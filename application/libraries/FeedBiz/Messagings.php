<?php

require_once(dirname(__FILE__).'/../tools.php');
require_once(dirname(__FILE__).'/../db.php');
require_once(dirname(__FILE__).'/../UserInfo/configuration.php');
require_once dirname(__FILE__).'/../FeedBiz/Marketplaces.php';
require_once dirname(__FILE__).'/../../../assets/apps/FeedbizImport/log/RecordProcess.php';
require_once dirname(__FILE__).'/../Mirakl/functions/mirakl.messages.php';

class Messagings {

    const TABLE = 'mp_messaging';
    const PERIOD = 200000; //86400;
    const SYS_EMAIL_LIMIT = 15;
    public $error = array();
    public $db;
    public $path_pdf;
    public $path_mail;
    public $template;
    public $mail_provider = null;
    public $provider_name = null;
    public $hostname = null;
    public $login = null;
    public $password = null;
    public $transport = array();
    public $mailer = array();
    public $swift_message = array();

    public static $limit_field = 'mail_maximum';

    public static $template_list = array(
        'mail_invoice' => array(
            'main' => array('invoice'),
        ),
        'mail_review' => array(
            'main' => array('review_incentive'),
        ),
        'customer_thread' => array(
            'main' => array('invoice', 'review_incentive'), //reply_msg
        ),
        'reply_template' => array('reply_msg'),
    );

    public static $templates = array(
        'mail_invoice' => array("{customer_name}", "{order_id}", "{order_date}", "{shop_name}", "{marketplace}"),
        'mail_review' => array("{customer_name}", "{shop_name}", "{marketplace}", "{review_url_html}")
    );

    public static $order_status = array(
        'main' => array('Shipped', 'Delivered'),
    );

    public static $messaging_fields = array(
        'main' => array(
            'mail_invoice' => array('mail_invoice_active', 'mail_invoice_order_state', 'mail_invoice_template', 'mail_invoice_additionnal',
                'mail_invoice_mail_from', 'mail_invoice_port', 'mail_invoice_encryption', 'mail_invoice_password', 'mail_invoice_smtp_server'),
            'mail_review' => array('mail_review_active', 'mail_review_order_state', 'mail_review_template', 'mail_review_delay',
                'mail_review_mail_from', 'mail_review_port', 'mail_review_encryption', 'mail_review_password', 'mail_review_smtp_server'),
            'customer_thread' => array('customer_thread_active', 'customer_thread_template', 'customer_thread_mail_provider', 'customer_thread_login', 'customer_thread_password'),
            'reply_template' => array('reply_template_active', 'reply_template_template'),
        ),
    );

    public static $review_url = array(
        'amazon'        => 'https://www.%s/gp/feedback/',
        'ebay'          => 'http://feedback.%s/ws/eBayISAPI.dll?LeaveFeedback2',
        'darty'             => '',
        'carrefour'         => '',
        'auchan'            => '',
        'comptoirsante'     => '',
        'delamaison'        => '',
        'elcorteingles'     => '',
        'menlook'           => '',
        'naturedecouvertes' => '',
        'privalia'          => '',
        'rueducommerce'     => '',
        'thebeautyst'       => '',
        'truffaut'          => '',
    );

    public static $preg_match_order_id = array(
        'amazon'            => '/([0-9]{3}-[0-9]{7}-[0-9]{7})/i', //'/\(([A-Z])\w+\s:\s([0-9]{3}-[0-9]{7}-[0-9]{7})\)/i',
        'ebay'              => '/([0-9]{12}-[0-9]{13})/',        
    );

    public static $invoice_subjects = array(
        'fr' => 'Facture pour votre commande',
        'en' => 'Invoice for your order',
        'de' => 'Rechnung f&uuml;r Ihre Bestellung',
        'it' => 'Fattura per il vostro ordine',
        'es' => 'Factura de su pedido',
        'mx' => 'Invoice for your order',
        'ca' => 'Invoice for your order'
    );

    public static $review_subjects = array(
        'fr' => 'Evaluation concernant votre commande N&ordm;',
        'en' => 'Seller Rating for your order No.',
        'de' => 'Bewertung Ihrer Bestellung Nr.',
        'it' => 'Valutazione del Suo ordine N&ordm;',
        'es' => 'Evaluaci&oacute;n relativa a su pedido N&ordm;',
        'mx' => 'Seller Rating for your order No.',
        'ca' => 'Seller Rating for your order No.'
    );

    public static $email_providers = array(
        'gmail' => 'Google Mail (gmail.com)'
    );

    public static $email_hostnames = array(
        'gmail' => '{imap.gmail.com:993/imap/ssl}'
    );

    public function __construct($user_name, $debug = false) {

        $this->db = new Db($user_name);
        $this->user = $user_name;
        $this->debug = $debug;

        $this->path_pdf = USERDATA_PATH.$this->user.'/email/pdf/';
        $this->path_html = USERDATA_PATH.$this->user.'/email/html/';
        $this->path_mail = dirname(__FILE__).'/../../../messaging_templates/';

        $this->prefix_order = 'orders_';

        $this->countries = Marketplaces::getCountries();
        $this->marketplaces = Marketplaces::get_marketplaces();

        $this->batch_id = uniqid();

        if ($this->debug) {
            @ini_set('display_errors', 'on');
            @error_reporting(E_ALL | E_STRICT);
        } else {
            @ini_set('display_errors', 'off');
            @error_reporting(E_ALL | E_STRICT);
        }
    }

    public function get_messagings($id_shop = null, $marketplace = null, $id_country = null, $action = null) {

        $content = $where = array();
        $table = self::TABLE;

        if (isset($id_country) && !empty($id_country))
            $where['id_country'] = $id_country;

        if (isset($marketplace) && !empty($marketplace))
            $where['marketplace'] = $marketplace;

        if (isset($id_shop) && !empty($id_shop))
            $where['id_shop'] = $id_shop;

        $this->db->from($table);

        if (!empty($where)) {
            $this->db->where($where);
        }

        if (isset($action) && !empty($action))
            $this->db->where_like(array('field_name' => "$action"), ' AND ');

        $result = $this->db->db_array_query($this->db->query);

        foreach ($result as $value) {

            $key = $value['id_country'];
            if ($value['id_marketplace'] == 0 && isset($value['region']) && !empty($value['region'])) {
                $key = $value['region'];
                $content[$value['marketplace']][$key]['id_lang'] = $value['id_country'];
            }

            $content[$value['marketplace']][$key][$value['field_name']] = $value['field_value'];
        }

        return $content;
    }

    public function get_messaging($id_shop = null, $marketplace = null, $id_country = null, $action = null) {

        $content = $where = array();
        $table = self::TABLE;

        if (isset($id_country) && !empty($id_country))
            $where['id_country'] = $id_country;

        if (isset($marketplace) && !empty($marketplace))
            $where['marketplace'] = $marketplace;

        if (isset($id_shop) && !empty($id_shop))
            $where['id_shop'] = $id_shop;

        $this->db->from($table);

        if (!empty($where)) {
            $this->db->where($where);
        }

        if (isset($action) && !empty($action))
            $this->db->where_like(array('field_name' => "$action"), ' AND ');

        $result = $this->db->db_array_query($this->db->query);

        foreach ($result as $value) {
            $content[$value['field_name']] = $value['field_value'];
        }

        return $content;
    }

    public function save_messaging($action, $data) {
        if (!empty($data)) {
            $sql = '';
            $table = self::TABLE;
            $messaging_fields = isset(self::$messaging_fields['main'][$action]) ? self::$messaging_fields['main'][$action] : array();

            if (!isset($data[$action.'_active'])) {
                $data[$action.'_active'] = false;
            }
           
            // set email limit
            if(isset($data[Messagings::$limit_field])) {
                $save_data = array();
                $save_data['id_shop'] = (int) $data['id_shop'];
                $save_data['id_marketplace'] = 0;
                $save_data['marketplace'] = 'main';
                $save_data['id_country'] = 0;
                $save_data['field_name'] = Messagings::$limit_field;
                $save_data['field_value'] = trim($data[Messagings::$limit_field]);
                $save_data['date_upd'] = date('Y-m-d H:i:s');
                $sql .= $this->db->replace_string($table, $save_data);
            }
            
            foreach ($data as $fields => $val) {
                if (in_array($fields, $messaging_fields)) {
                    $save_data = array(
                        'id_shop' => (int) $data['id_shop'],
                        'id_marketplace' => (int) $data['id_marketplace'],
                        'marketplace' => $data['marketplace'],
                        'id_country' => (int) $data['id_country'],
                        'field_name' => $fields,
                        'field_value' => trim($val),
                        'date_upd' => date('Y-m-d H:i:s'),
                    );
                    if ($action == 'reply_template') {
                        $save_data['region'] = $data['lang'];
                    }
                    $sql .= $this->db->replace_string($table, $save_data);
                }
            }
            return $this->db->db_exec($sql);
        }
        return (true);
    }

    public function create_table($table_name) {
        if ($this->db->db_table_exists($table_name) == false) {
            $sql = "CREATE TABLE $table_name (
		    id_shop int(11) NOT NULL,
		    id_marketplace int(11) NOT NULL,		    
		    marketplace varchar(64) NOT NULL,
                    id_country int(11) NOT NULL,
                    region VARCHAR(8),
                    field_name VARCHAR(64),
                    field_value TEXT,
                    date_upd DATETIME,
		    PRIMARY KEY (id_shop, marketplace, id_country, field_name)
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
            if (!$this->db->db_exec($sql, false))
                return false;

            if (!$this->db->fieldExists("{$this->prefix_order}orders", 'flag_mail_review')) {
                $this->db->db_exec("ALTER IGNORE TABLE {$this->prefix_order}orders ADD `flag_mail_review` TINYINT(2) NULL DEFAULT NULL AFTER `flag_canceled_status`");
                $this->db->db_exec("UPDATE {$this->prefix_order}orders SET `flag_mail_review` = 1 ; ");
            }
            if (!$this->db->fieldExists("{$this->prefix_order}orders", 'flag_mail_invoice')) {
                $this->db->db_exec("ALTER IGNORE TABLE {$this->prefix_order}orders ADD `flag_mail_invoice` TINYINT(2) NULL DEFAULT NULL AFTER `flag_canceled_status`");
                $this->db->db_exec("UPDATE {$this->prefix_order}orders SET `flag_mail_invoice` = 1 ; ");
            }
        }
        
        if ($this->db->db_table_exists($table_name.'_history') == false) {
            $sql = "CREATE TABLE ".$table_name."_history (
                    field_name VARCHAR(64),
                    field_value TEXT,
                    date_upd DATETIME		    
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
            if (!$this->db->db_exec($sql, false))
                return false;
        }

        return true;
    }

    public function getOrderToSendMessage($action, $allow_marketplace=null) {

        $limit = $this->get_maximum_send_mail();
        $limit = $limit>self::SYS_EMAIL_LIMIT?self::SYS_EMAIL_LIMIT:$limit;
        if(isset($limit) && $limit ==0) // when sent orders is over limit // limit =0
            return ;
        
        if (!$this->db->fieldExists("{$this->prefix_order}orders", 'flag_'.$action)) {
            $this->db->db_exec("ALTER IGNORE TABLE {$this->prefix_order}orders ADD `flag_$action` TINYINT(2) NULL DEFAULT NULL AFTER `flag_canceled_status`");
        }

        // only some marketplace
        $list_marletplace = array();
        if(isset($allow_marketplace) && !empty($allow_marketplace)){
            foreach ($allow_marketplace as $id_marketplace => $marketplace){
                $list_marletplace[] = $id_marketplace;
            }
        }
              
        // select orders where flag = null And status = mail_invoice_order_state and sent to shop and invoice_no is not null
        $sql = "SELECT o.id_orders as id_order,
                o.id_marketplace_order_ref as id_marketplace_order_ref,                
                o.id_shop as id_shop,
                o.site as site,
                o.id_marketplace as id_marketplace,
                o.sales_channel as sales_channel,
                o.order_date as order_date,
                o.order_status as order_status,
                o.flag_$action as flag_$action,
                ob.`name` as customer_name,
                ob.`email` as customer_email,
                oi.invoice_no as invoice_no,               
                oi.seller_order_id as seller_order_id,
                osm.sub_marketplace as id_sub_marketplace
                FROM {$this->prefix_order}orders o 
                JOIN {$this->prefix_order}order_invoice oi ON oi.id_orders = o.id_orders 
                JOIN {$this->prefix_order}order_buyer ob ON ob.id_orders = o.id_orders AND ob.site = o.site
                LEFT JOIN {$this->prefix_order}order_sub_marketplace osm ON osm.id_orders = o.id_orders AND osm.id_country = o.site AND osm.id_shop = o.id_shop
                WHERE o.order_status IN ('".implode("', '", self::$order_status['main'])."') ";

        if ($this->debug && isset($_GET['id_order'])) {
            $sql.= " AND o.id_orders = ".$_GET['id_order']." ";
        } else {
            $sql.= " AND o.status = 1 AND (o.flag_$action IS NULL OR o.flag_$action != 1) AND oi.invoice_no IS NOT NULL AND oi.seller_order_id IS NOT NULL ";
        }

        if(!empty($list_marletplace)){
            $sql.= " AND o.id_marketplace IN (".implode(", ", $list_marletplace).") ";
        }
//        $sql .= " ORDER BY o.id_marketplace asc,o.site asc ";
        if(isset($limit) && !empty($limit))
            $sql .= " LIMIT $limit ";

        if ($this->debug){
            echo '<br/>';
            echo $sql;
            echo '<br/>';
        }
        
        return $this->db->db_query_str($sql);
    }

    public function flagOrderMessage($action, $orders, $flag = 1) {

        $sql = '';
        if (!empty($orders)) {
            $field = 'flag_'.$action;
            foreach ($orders as $id_order => $order){
                $sql .= "UPDATE {$this->prefix_order}orders SET $field = $flag WHERE id_orders = $id_order ; ";
            }
            $data = array(
                'field_name'=>'sent_mail',
                'field_value'=>count($orders),
                'date_upd'=>date('Y-m-d H:i:s')
            );
            if ($this->debug) {
                echo '<pre>'.print_r($sql, true).'</pre>';
                $sql = $this->db->replace_string(self::TABLE."_history", $data);
                echo '<pre>'.print_r($sql, true).'</pre>';
                return true;
            } else {
                //file_put_contents(dirname(__FILE__).'/../../../assets/apps/users/'.$this->user.'/amazon/flagOrderMessage_'.$id_order.'.txt', $sql);
                $exec = $this->db->db_exec($sql,true,true,true);
                if($exec){
                    $sql = $this->db->replace_string(self::TABLE."_history", $data);
                    $this->db->db_exec($sql);
                }
                return $exec;
            }
        }
    }

    public function CustomerThreadEmail($user_id, $user_config = null, $data = null) {

        if (!isset($user_config))
            $user_config = new UserConfiguration();

        // get language
        $language = $user_config->getUserDefaultLaguage($user_id);
        load('messaging', $language);

        // get user data
        if (!isset($data))
            $data = $user_config->check_site_verify($user_id);

        $shop = $data['feed_shop_info'];
        $shop_name = $shop['name'];

        // start message
        if (!$this->debug) {
            $type = 'Customer Thread Email';
            $report_process = new report_process($this->user, $this->batch_id, l('Customer Thread Email'), true, false, true);
            if ($report_process->has_other_running($type)) {
                return (false);
            }
            $report_process->set_process_type($type);
            $report_process->set_popup_display(true);
            $report_process->set_priority(1);
            $report_process->set_shop_name($shop_name);

            $message = new stdClass();
            $message->message = l('Processing')."..";
            $report_process->set_status_msg($message);
        }

        // check shop messaging url
        if (isset($shop['url']['messaging']) && !empty($shop['url']['messaging'])) {
            // parameter to set url
            $argv_param = array(
                'base_url' => $data['feed_biz']['base_url'],
                'token' => $data['feed_token'],
                'url' => $shop['url']['messaging'],
            );

            // send to shop
            $messaging_url = $user_config->prepare_argv($argv_param);

            if ($this->debug) {
                echo basename(__FILE__).'#'.__LINE__;
                echo '<pre>'.print_r($messaging_url, true).'</pre>';
            }

            $return_messaging = curl_get_contents($messaging_url);

            if ($this->debug) {
                echo basename(__FILE__).'#'.__LINE__;
                echo '<pre>'.print_r($return_messaging, true).'</pre>';
            }

            if (isset($return_messaging) && !empty($return_messaging) && count($return_messaging) > 0) {

                if (isset($report_process)) {
                    $message->message = l('Successful').".";
                    $message->status = 'Success';
                    $report_process->set_status_msg($message);
                }
            } else {
                if ($this->debug) {
                    echo basename(__FILE__).'#'.__LINE__;
                    echo '<pre>No return message</pre>';
                }
            }
        } else {
            if ($this->debug) {
                echo basename(__FILE__).'#'.__LINE__;
                echo '<pre>Missing : shop messaging url </pre>';
            }
        }

        if (!empty($this->error)) {
            if (isset($report_process)) {
                $message->error = implode(",<br/>", $this->error);
                if ($this->debug) {
                    echo "<pre>\n";
                    printf('%s - %s::%s - line #%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                    echo "</pre>\n";
                    echo "<pre>\n";
                    print_r($message->error, true);
                    echo "</pre>\n";
                }
                $this->proc_rep->set_error_msg($message);
            }
        }

        if (isset($report_process)) {
            $report_process->finish_task(0); //stop progress
        }

        return (true);
    }

    public function getCustomerThreadEmail($params = null) {

        ob_start();

        // Mirakl Exception
        $mirakl_message = new MiraklMessages($this->user, $this->debug);

        if(isset($this->marketplaces[$mirakl_message->getIdMarketplace()]['submarketplace']))
            $mirakl_marketplace = array_map('strtolower', $this->marketplaces[$mirakl_message->getIdMarketplace()]['submarketplace']);

        // get all parameters
        if (!isset($params))
            $params = $this->get_messagings();

        $messages = array();
        foreach ($params as $marketplace => $param) {

            if($marketplace == 'main')
                continue;

            if ($this->debug){
                echo ('<p>------------</p><b>'.$marketplace.'</b><p>------------</p>');
            }
            
            // Mirakl
            if (isset($mirakl_marketplace) && in_array($marketplace, $mirakl_marketplace)) {
                $start_date = date('Y-m-d\TH:i:s\Z', time() - self::PERIOD);

                if ($this->debug){
                    echo ($marketplace.' start date : ' . $start_date . '<br/>');
                }

                foreach ($param as $id_country => $pa) {

                    $iso_code = isset($this->countries[$id_country]['iso_code']) ? $this->countries[$id_country]['iso_code'] : null;
                    $language_code = isset($this->countries[$id_country]['language_code']) ? $this->countries[$id_country]['language_code'] : null;

                    if (!array_key_exists('customer_thread_active', $pa) || !(bool) $pa['customer_thread_active']) {
                        if ($this->debug){
                            echo ($marketplace. ' : is inactive customer thread.<br/>');
                        }
                        continue;
                    }
                    
                    $sub_marketplace = array_search($marketplace, $mirakl_marketplace);
                    
                    $params = array(
                        'sub_marketplace' => $sub_marketplace,
                        'id_country' => $id_country,
                        'marketplace' => $marketplace,
                        'start_date' => $start_date
                    );
                    
                    $mails = $mirakl_message->getMessage($params);
                    ksort($mails);

                    if ($this->debug) {
                        echo "<pre>\n";
                        print_r($mails, true);
                        echo "</pre>\n";
                    }

                    if ($mails) {
                        $mails['iso_code'] = $language_code;
                        $messages[$marketplace][$iso_code] = $mails;
                    }
                }                
                
            } else {

                foreach ($param as $id_country => $pa) {

                    $iso_code = isset($this->countries[$id_country]['iso_code']) ? $this->countries[$id_country]['iso_code'] : null;
                    $language_code = isset($this->countries[$id_country]['language_code']) ? $this->countries[$id_country]['language_code'] : null;

                    if (!array_key_exists('customer_thread_active', $pa) || !(bool) $pa['customer_thread_active']) {
                        if ($this->debug)
                            echo ($marketplace. ' : Imap messaging is inactive<br/>');
                        continue;
                    }
                    if (!array_key_exists('customer_thread_mail_provider', $pa) || !strlen($pa['customer_thread_mail_provider'])) {
                        if ($this->debug)
                            echo ($marketplace. ' : Mail provider is not yet configured<br/>');
                        continue;
                    }

                    if (!array_key_exists($pa['customer_thread_mail_provider'], Messagings::$email_providers)) {
                        if ($this->debug)
                            echo ($marketplace. ' : Mail provider is not yet implemented<br/>');
                        continue;
                    }

                    if (!array_key_exists('customer_thread_login', $pa) || !strlen($pa['customer_thread_login'])) {
                        if ($this->debug)
                            echo ($marketplace. ' : Login is not yet configured<br/>');
                        continue;
                    }

                    if (!array_key_exists('customer_thread_password', $pa) || !strlen($pa['customer_thread_password'])) {
                        if ($this->debug)
                            echo ($marketplace. ' : Password is not yet configured<br/>');
                        continue;
                    }

                    $this->mail_provider = $pa['customer_thread_mail_provider'];
                    $this->provider_name = Messagings::$email_providers[$pa['customer_thread_mail_provider']];
                    $this->login = $pa['customer_thread_login'];
                    $this->password = $pa['customer_thread_password'];
                    $label = sprintf('%s-%s', ucfirst(strtolower($marketplace)), ucfirst($iso_code));

                    $this->hostname = Messagings::$email_hostnames[$pa['customer_thread_mail_provider']].$label;

                    if ($this->debug) {
                        echo "<pre>\n";
                        printf('%s - %s::%s - line #%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                        printf('Mail Provider: %s'."\n", $this->mail_provider);
                        printf('Provider Name: %s'."\n", $this->provider_name);
                        printf('Hostname: %s'."\n", $this->hostname);
                        printf('Login: %s'."\n", $this->login);
                        printf('Password: %s'."\n", $this->password);
                        printf('Password Length: %s'."\n", strlen($this->password));
                        echo "</pre>\n";
                    }

                    $mails = $this->grabEmails($marketplace);
                    ksort($mails);

                    if ($this->debug) {
                        echo "<pre>\n";
                        print_r($mails, true);
                        echo "</pre>\n";
                    }

                    if ($mails) {
                        $mails['iso_code'] = $language_code;
                        $messages[$marketplace][$iso_code] = $mails;
                    }
                }
            }
        }
        
        if ($this->debug) { exit; }

        $outBuffer = ob_get_contents();

        if (isset($outBuffer) && !empty($outBuffer)) {
            $messages['error'] = $outBuffer;
        }

        ob_end_clean();

        return $messages;
    }

    public function overrideCustomerThreadEmail($id_lang, $marketplace = null) {

        require_once dirname(__FILE__).'/../FeedBiz.php';

        $Feedbiz = new FeedBiz($this->user);
        $defaultShop = $Feedbiz->getDefaultShop($this->user);

        if (!isset($defaultShop['id_shop']) || empty($defaultShop['id_shop'])) {
            if ($this->debug) {
                printf('%s:#%d Missing shop default'."<br />\n", basename(__FILE__), __LINE__);
            }
            return (false);
        }

        $language = $Feedbiz->getLanguage($defaultShop['id_shop']);

        //get lang by id
        $lang = null;
        if (isset($language[$id_lang]['iso_code']) && !empty($language[$id_lang]['iso_code'])) {
            $lang = $language[$id_lang]['iso_code'];
        } else {
            $defaultLang = $Feedbiz->getLanguageDefault($defaultShop['id_shop']);
            foreach ($defaultLang as $id_lang_default => $default_lang)
                $lang = isset($default_lang['iso_code']) ? $default_lang['iso_code'] : null;
        }

        if (!isset($lang) || empty($lang)) {
            if ($this->debug) {
                printf('%s:#%d Missing Language Code for ID: %s'."<br />\n", basename(__FILE__), __LINE__, $id_lang);
            }
            return (false);
        }

        // get template from
        $template = null;

        $params = $this->get_messaging($defaultShop['id_shop'], 0, $id_lang, 'reply_template');

        if (isset($params['reply_template_active']) && $params['reply_template_active']) {
            if (isset($params['reply_template_template']) && !empty($params['reply_template_template']))
                $template = $params['reply_template_template'];

            $template_file_html = sprintf('%s/%s/%s/%s.html', $this->path_html, (isset($marketplace) ? $marketplace : 'main'), $lang, $template);

            if (!file_exists($template_file_html)) {
                $template_file_html = sprintf('%s%s/%s.html', $this->path_mail, $lang, $template);
            }
            if (!file_exists($template_file_html)) {
                if ($this->debug) {
                    printf('%s:#%d Template file doesn\'t exists for this lang: %s'."<br />\n", basename(__FILE__), __LINE__, $lang);
                }
                return (false);
            }
            $html = file_get_contents($template_file_html);
        }

        return (array('html' => $html, 'txt' => ''));
    }

    private function grabEmails($marketplace = null) {

        $hostname = $this->hostname;
        $username = $this->login;
        $password = $this->password;

        $criteria = 'SINCE "'.date('d-M-Y', time() - self::PERIOD).'"';

        $inbox = @imap_open($hostname, $username, $password);

        if (!$inbox) {
            $error = @imap_last_error();
            $this->error[] = l('Cannot connect to IMAP serveur').': '.$error;

            if ($this->debug) {
                echo "<pre>\n";
                printf('%s - %s::%s - line #%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                printf('Cannot connect to IMAP serveur: '.$error);
                echo "</pre>\n";
            }

            return (false);
        } else {

            $emails = @imap_search($inbox, $criteria);

            $messages = array();

            if (is_array($emails) && count($emails)) {
                rsort($emails);

                foreach ($emails as $email_number) {

                    $overviews = @imap_fetch_overview($inbox, $email_number, 0);
                    $overview = reset($overviews);

                    $structure = @imap_fetchstructure($inbox, $email_number);
                    if (!isset($structure->parts) || !is_array($structure->parts)) {
                        continue;
                    }

                    $part = reset($structure->parts);

                    $message = @imap_fetchbody($inbox, $email_number, 1);
                    $subject = $overview->subject;
                    $message = @imap_qprint($message);

                    mb_internal_encoding('UTF-8');
                    $subject = str_replace("_", " ", mb_decode_mimeheader($subject));

                    $key = ($email_number+1).'';
                    $messages[$key]['subject'] = (string) $subject;
                    $messages[$key]['from'] = (string) $overview->from;
                    $messages[$key]['date'] = (string) $overview->date;
                    $messages[$key]['seen'] = $overview->seen;
                    $messages[$key]['message_id'] = (string) $overview->message_id;
                    $messages[$key]['body'] = (string) strip_tags($message);

                    // subject exception ;
                    if (isset(self::$preg_match_order_id[strtolower($marketplace)])) {
                        $match_ok = preg_match(self::$preg_match_order_id[strtolower($marketplace)], $subject, $matches);
                        // Message related to an order
                        if ($match_ok && is_array($matches) && count($matches)) {
                            $messages[$key]['mp_order_id'] = end($matches);
                        }
                    }

                    if (!$overview->seen) {
                        if ($this->debug) {
                            echo '<pre>';
                            printf('Date: %s', $overview->date);
                            printf('Subject: %s', $subject);
                            echo str_repeat('-', 160)."\n";
                            echo '</pre>';
                        }
                    }
                }
            }
        }

        if ($this->debug) {
            echo "<pre>\n";
            printf('%s - %s::%s - line #%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
            printf('Messages: %s', print_r($messages, true));
            echo "</pre>\n";
        }

        @imap_close($inbox);

        return($messages);
    }

    public function sendReviewIncentive($user_id, $params = null, $user_config = null, $data = null, $allow_marketplace = null) {

        if (!isset($user_config))
            $user_config = new UserConfiguration();

        // get language
        $language = $user_config->getUserDefaultLaguage($user_id);
        load('messaging', $language);

        // get user data
        if (!isset($data))
            $data = $user_config->check_site_verify($user_id);

        $shop = $data['feed_shop_info'];
        $shop_name = $shop['name'];
        $default_mail_from = $data['user_email'];
        
        // get review parameter
        if (!isset($params))
            $params = $this->get_messagings();

        // get order to send message
        $order_list = $this->getOrderToSendMessage('mail_review', $allow_marketplace);
        
        if ($this->debug) {
            echo basename(__FILE__).'#'.__LINE__;
            echo '<pre>'.print_r($order_list, true).'</pre>';
        }

        $marketplaces_domain = Marketplaces::getDomains();
        $flagged_order = array();

        // start message
        if (!$this->debug) {
            $type = 'Send mail review';
            $report_process = new report_process($this->user, $this->batch_id, l('Sending mail review'), true, true, true, $type);
            if ($report_process->has_other_running($type)) {
                return (false);
            }
            $report_process->set_process_type($type);
            $report_process->set_priority(1);
            $report_process->set_shop_name($shop_name);

            $loop = 1; // start progress
            $report_process->set_max_min_task(sizeof($order_list), 1);
        }

        if(isset($order_list) && !empty($order_list)){
            foreach ($order_list as $order) {

                if (isset($report_process)) {
                    $report_process->set_running_task($loop);
                    $loop++;
                }

                // skip non feedbiz order id
                if (!isset($order['id_order']) || empty($order['id_order']))
                    continue;

                // skip non seller order id
                if (!isset($order['seller_order_id']) || empty($order['seller_order_id']))
                    continue;

                // skip non invoice id
                if (!isset($order['invoice_no']) || empty($order['invoice_no']))
                    continue;

                // skip flagged email invooice
                if (isset($order['flag_mail_review']) && $order['flag_mail_review'])
                    continue;

                // get marketplace name
                $sales_channel = explode('.', $order['sales_channel']);
                $marketplace_name = isset($sales_channel[0]) ? $sales_channel[0] : $order['sales_channel'];

                if (isset($this->marketplaces[$order['id_marketplace']]['submarketplace'][$order['id_sub_marketplace']])) {
                    $marketplace_name = $this->marketplaces[$order['id_marketplace']]['submarketplace'][$order['id_sub_marketplace']];
                } elseif (isset($this->marketplaces[$order['id_marketplace']]['name'])) {
                    $marketplace_name = $this->marketplaces[$order['id_marketplace']]['name'];
                }

                $marketplace = strtolower($marketplace_name);

                $param = isset($params[$marketplace][$order['site']]) ? $params[$marketplace][$order['site']] : null;

                // check is mail_invoice active ?
                if (isset($param['mail_review_active']) && $param['mail_review_active']) {

                    // check mail invoice template ?
                    if (isset($param['mail_review_template']) && !empty($param['mail_review_template'])) {

                        // check order state ?
                        if (isset($param['mail_review_order_state']) && !empty($param['mail_review_order_state'])) {

                            $order_state = $param['mail_review_order_state'];
                            $order_delay = isset($param['mail_review_delay']) && !empty($param['mail_review_delay']) ? $param['mail_review_delay'] : false;
                            $template = $param['mail_review_template'];

                            $smtp_params = array(
                                'smtp_server' => isset($param['mail_review_smtp_server']) && !empty($param['mail_review_smtp_server']) ? $param['mail_review_smtp_server'] : null,
                                'port' => isset($param['mail_review_smtp_server']) && !empty($param['mail_review_port']) ? $param['mail_review_port'] : null,
                                'encryption' => isset($param['mail_review_encryption']) && !empty($param['mail_review_encryption']) ? $param['mail_review_encryption'] : null,
                                'password' => isset($param['mail_review_password']) && !empty($param['mail_review_password']) ? $param['mail_review_password'] : null,
                            );

                            //if order have shipping number changed order_status to Shipped
                            if($order_state == 'Delivered'){
                                if($this->get_order_delivered($order['id_order'])){
                                    $order['order_status'] = 'Delivered';
                                }
                            }

                            if ($order_state != $order['order_status'])
                                continue;

                            #get lang by site id
                            if (isset($this->countries[$order['site']]['language_code']) && !empty($this->countries[$order['site']]['language_code'])) {
                                $lang = $this->countries[$order['site']]['language_code'];
                            } else {
                                $lang = 'en';
                            }

                            if (isset($marketplaces_domain[$order['id_marketplace']][$order['site']]['domain_offer_sub_pkg'])) {
                                $domain = $marketplaces_domain[$order['id_marketplace']][$order['site']]['domain_offer_sub_pkg'];
                            }

                            #send invoice to customer#
                            $dateOrdered = date('Y-m-d', strtotime($order['order_date']));
                            $dateCurrent = date('Y-m-d');

                            if ((int) $order_delay && getWorkingDays($dateOrdered, $dateCurrent) >= (int) $order_delay) {
                                if ($this->debug) {
                                    printf('%s:#%d Out of delay: created on %s - sent on %s'."<br />\n", basename(__FILE__), __LINE__, $dateOrdered, $dateCurrent);
                                }
                                continue;
                            }

                            $template_file = sprintf('%s%s/%s.html', $this->path_mail, $lang, $template);

                            if (!file_exists($template_file)) {

                                #get iso_code by site id
                                if (isset($this->countries[$order['site']]['iso_code']) && !empty($this->countries[$order['site']]['iso_code'])) {
                                    $iso_code = $this->countries[$order['site']]['iso_code'];
                                } else {
                                    $iso_code = 'en';
                                }

                                $template_file = sprintf('%s/%s/%s/%s.html', $this->path_html, $marketplace, $iso_code, $template);
                            }

                            if (!file_exists($template_file)) {
                                if ($this->debug) {
                                    printf('%s:#%d Template file doesn\'t exists for this lang: %s'."<br />\n", basename(__FILE__), __LINE__, $lang);
                                }

                                continue;
                            }

                            if (isset(self::$review_subjects[$lang])) {
                                $title = self::$review_subjects[$lang];
                            } else {
                                $title = self::$review_subjects['en'];
                            }

                            $email_subject = sprintf('%s %s', html_entity_decode($title, ENT_COMPAT, 'UTF-8'), $order['id_marketplace_order_ref']);

                            // Send mail review only it have customer email
                            if(isset($order['customer_email']) && !empty($order['customer_email'])){
                                $email_to = $order['customer_email'];
                                $email_from = isset($param['mail_review_mail_from']) ? trim($param['mail_review_mail_from']) : $default_mail_from;

                                $template_html = file_get_contents($template_file);
                                $marketplace_domain = self::goToSellerReviewPage($marketplace, $domain);

                                // Send mail review only it have review URL
                                if($marketplace_domain) {
                                    $reviewPage = sprintf('<a href="%s" title="%s">%s</a>', $marketplace_domain, $email_subject, $marketplace_domain);
                                    $customer_name = htmlentities($order['customer_name'], ENT_COMPAT, 'UTF-8');
                                    $template_keys = self::$templates['mail_review'];
                                    $template_vars = array($customer_name, $shop_name, $marketplace_name, $reviewPage);

                                    $content = str_replace($template_keys, $template_vars, $template_html);

                                    if ($this->send_mail($email_to, $email_subject, $email_from, $shop_name, $content, array(), $smtp_params)) {
                                        $flagged_order[$order['id_order']] = true;
                                        $this->flagOrderMessage('mail_review', $flagged_order);
                                        unset($flagged_order[$order['id_order']]);
                                    } /*else {
                                        file_put_contents(dirname(__FILE__).'/../../../assets/apps/users/'.$this->user.'/amazon/send_mail_'.$order['id_order'].'.txt', 'true');
                                    }*/
                                }
                            }
                        }
                    }
                }
            }
        }
        
        //$this->flagOrderMessage('mail_review', $flagged_order);

        if (isset($report_process)) {
            $report_process->finish_task(0); //stop progress
        }

        return (true);
    }

    public function sendInvoice($user_id, $params = null, $user_config = null, $data = null, $allow_marketplace = null) {

        if (!isset($user_config))
            $user_config = new UserConfiguration();

        // get language
        $language = $user_config->getUserDefaultLaguage($user_id);
        load('messaging', $language);

        // get user data
        if (!isset($data))
            $data = $user_config->check_site_verify($user_id);

        $shop = $data['feed_shop_info'];
        $shop_name = $shop['name'];
        $default_mail_from = $data['user_email'];

        // check invoice url
        if (isset($shop['url']['invoice']) && !empty($shop['url']['invoice']))
        {
            // get invoice parameter
            if (!isset($params))
                $params = $this->get_messagings();

            // get order to send message
            $order_list = $this->getOrderToSendMessage('mail_invoice', $allow_marketplace);

            if ($this->debug) {
                echo basename(__FILE__).'#'.__LINE__;
                echo '<pre>'.print_r($order_list, true).'</pre>';
            }

            // parameter to set url
            $argv_param = array(
                'base_url' => $data['feed_biz']['base_url'],
                'token' => $data['feed_token'],
                'url' => $shop['url']['invoice'],
            );

            $flagged_order = array();

            // start message
            if (!$this->debug) {
                $type = 'Send mail invoice';
                $report_process = new report_process($this->user, $this->batch_id, l('Sending mail invoice'), true, true, true, $type);
                if ($report_process->has_other_running($type)) {
                    return (false);
                }
                $report_process->set_process_type($type);
                $report_process->set_priority(1);
                $report_process->set_shop_name($shop_name);

                $loop = 1; // start progress
                $report_process->set_max_min_task(sizeof($order_list), 1);
            }
            
            if(isset($order_list) && !empty($order_list)){
                foreach ($order_list as $order) {

                    if (isset($report_process)) {
                        $report_process->set_running_task($loop);
                        $loop++;
                    }

                    // skip non feedbiz order id
                    if (!isset($order['id_order']) || empty($order['id_order'])){
                        if ($this->debug) {
                            echo basename(__FILE__).'#'.__LINE__;
                            echo '<pre>'.print_r('skip non feedbiz order id', true).'</pre>';
                        }
                        continue;
                    }

                    // skip non seller order id
                    if (!isset($order['seller_order_id']) || empty($order['seller_order_id'])){
                        if ($this->debug) {
                            echo basename(__FILE__).'#'.__LINE__;
                            echo '<pre>'.print_r('skip non seller order id', true).'</pre>';
                        }
                        continue;
                    }

                    // skip non invoice id
                    if (!isset($order['invoice_no']) || empty($order['invoice_no'])){
                        if ($this->debug) {
                            echo basename(__FILE__).'#'.__LINE__;
                            echo '<pre>'.print_r('skip non invoice id', true).'</pre>';
                        }
                        continue;
                    }

                    // skip flagged email invooice
                    if (isset($order['flag_mail_invoice']) && $order['flag_mail_invoice']){
                        if ($this->debug) {
                            echo basename(__FILE__).'#'.__LINE__;
                            echo '<pre>'.print_r('skip flagged email invoice', true).'</pre>';
                        }
                        continue;
                    }
                    
                    // get marketplace name
                    $sales_channel = explode('.', $order['sales_channel']);
                    $marketplace_name = isset($sales_channel[0]) ? $sales_channel[0] : $order['sales_channel'];

                    if (isset($this->marketplaces[$order['id_marketplace']]['submarketplace'][$order['id_sub_marketplace']])) {
                        $marketplace_name = $this->marketplaces[$order['id_marketplace']]['submarketplace'][$order['id_sub_marketplace']];
                    } elseif (isset($this->marketplaces[$order['id_marketplace']]['name'])) {
                        $marketplace_name = $this->marketplaces[$order['id_marketplace']]['name'];
                    }

                    $marketplace = strtolower($marketplace_name);
                    $param = isset($params[$marketplace][$order['site']]) ? $params[$marketplace][$order['site']] : null;

                    // check is mail_invoice active ?
                    if (isset($param['mail_invoice_active']) && $param['mail_invoice_active']) {
                        // check mail invoice template ?
                        if (isset($param['mail_invoice_template']) && !empty($param['mail_invoice_template'])) {
                            // check order state ?
                            if (isset($param['mail_invoice_order_state']) && !empty($param['mail_invoice_order_state'])) {

                                $template = $param['mail_invoice_template'];
                                $additionals = isset($param['mail_invoice_additionnal']) ? array($param['mail_invoice_additionnal']) : array();
                                $order_state = $param['mail_invoice_order_state'];
                                $smtp_params = array(
                                    'smtp_server' => isset($param['mail_invoice_smtp_server']) && !empty($param['mail_invoice_smtp_server']) ? $param['mail_invoice_smtp_server'] : null,
                                    'port' => isset($param['mail_invoice_smtp_server']) && !empty($param['mail_invoice_port']) ? $param['mail_invoice_port'] : null,
                                    'encryption' => isset($param['mail_invoice_encryption']) && !empty($param['mail_invoice_encryption']) ? $param['mail_invoice_encryption'] : null,
                                    'password' => isset($param['mail_invoice_password']) && !empty($param['mail_invoice_password']) ? $param['mail_invoice_password'] : null,
                                );

                                //if order have shipping number changed order_status to Shipped
                                if($order_state == 'Delivered'){
                                    if($this->get_order_delivered($order['id_order'])){
                                        $order['order_status'] = 'Delivered';
                                    }
                                }

                                if ($order_state != $order['order_status'])
                                    continue;

                                $argv_param['id_order'] = $order['seller_order_id'];
                                $invoice_url = $user_config->prepare_argv($argv_param);

                                if ($this->debug) {
                                    echo basename(__FILE__).'#'.__LINE__;
                                    echo '<pre>'.print_r($invoice_url, true).'</pre>';
                                }

                                $invoice = curl_get_contents($invoice_url);

                                if ($this->debug) {
                                    echo basename(__FILE__).'#'.__LINE__;
                                    echo '<pre>'.print_r(strlen($invoice), true).'</pre>';
                                }

                                if (strlen($invoice) <= 0) {
                                    continue;
                                }

                                #get lang by site id
                                if (isset($this->countries[$order['site']]['language_code']) && !empty($this->countries[$order['site']]['language_code'])) {
                                    $lang = $this->countries[$order['site']]['language_code'];
                                } else {
                                    $lang = 'en';
                                }

                                #send invoice to customer#
                                $template_file = sprintf('%s%s/%s.html', $this->path_mail, $lang, $template);

                                if (!file_exists($template_file)) {

                                    #get iso_code by site id
                                    if (isset($this->countries[$order['site']]['iso_code']) && !empty($this->countries[$order['site']]['iso_code'])) {
                                        $iso_code = $this->countries[$order['site']]['iso_code'];
                                    } else {
                                        $iso_code = 'en';
                                    }

                                    $template_file = sprintf('%s/%s/%s/%s.html', $this->path_html, $marketplace, $iso_code, $template);
                                }

                                if (!file_exists($template_file)) {
                                    if ($this->debug) {
                                        printf('%s:#%d Template file doesn\'t exists for this lang: %s'."<br />\n", basename(__FILE__), __LINE__, $lang);
                                    }
                                    continue;
                                }

                                $customer_name = htmlentities($order['customer_name'], ENT_COMPAT, 'UTF-8');
                                $template_html = file_get_contents($template_file);
                                $template_keys = self::$templates['mail_invoice'];

                                // fba_multichannel :
                                $template_vars = array($customer_name, $order['id_marketplace_order_ref'], $order['order_date'], $shop_name, $marketplace_name);
                                $content = str_replace($template_keys, $template_vars, $template_html);

                                $title = self::$invoice_subjects[$lang];
                                $email_subject = sprintf('%s %s', html_entity_decode($title, ENT_COMPAT, 'UTF-8'), $order['id_marketplace_order_ref']);

                                if(isset($order['customer_email']) && !empty($order['customer_email'])){

                                    $email_to = $order['customer_email'];
                                    $email_from = isset($param['mail_invoice_mail_from']) ? trim($param['mail_invoice_mail_from']) : $default_mail_from;

                                    $file_attachement = array();
                                    $file_attachement[0]['content'] = $invoice;
                                    $file_attachement[0]['name'] = $order['invoice_no'].'.pdf';
                                    $file_attachement[0]['mime'] = 'application/pdf';

                                    if (isset($additionals)) {
                                        $i = 1;
                                        foreach ($additionals as $additional) {

                                            $addtional_path = $this->path_pdf.strtolower($marketplace).'/'.$lang.'/'.$additional;

                                            if ($this->debug) {
                                                printf('%s:#%d path additional: %s'."<br />\n", basename(__FILE__), __LINE__, $addtional_path);
                                            }

                                            if (file_exists($addtional_path)) {
                                                $file_attachement[$i]['content'] = file_get_contents($addtional_path);
                                                $file_attachement[$i]['name'] = $additional;
                                                $file_attachement[$i]['mime'] = 'application/pdf';
                                                $i++;
                                            }
                                        }
                                    }
                                }

                                if ($this->send_mail($email_to, $email_subject, $email_from, $shop_name, $content, $file_attachement, $smtp_params)) {
                                    $flagged_order[$order['id_order']] = true;
                                    $this->flagOrderMessage('mail_invoice', $flagged_order);
                                    unset($flagged_order[$order['id_order']]);
                                } /*else {
                                    file_put_contents(dirname(__FILE__).'/../../../assets/apps/users/'.$this->user.'/amazon/send_mail_'.$order['id_order'].'.txt', 'true');
                                }*/
                            }
                        }
                    }
                }
            }

            //$this->flagOrderMessage('mail_invoice', $flagged_order);

            if (isset($report_process)) {
                $report_process->finish_task(0); //stop progress
            }
            
        }
        
        return (true);
    }

    public function send_mail($to_email, $subject, $setFromEmail, $setFromName, $content, $additional = array(), $smtp_params = array()) {

        if ($this->debug && (!isset($_GET['id_order']) || !$_GET['id_order'])){
            echo '<pre>SENT TO : '.print_r($to_email, true).'</pre>';
            return (true);
        }

        require_once (dirname(__FILE__).'/../Swift/swift_required.php');

        if (strlen($content) > 50) {

            if ($this->debug) {
                echo basename(__FILE__).'#'.__LINE__;
                echo '<pre>'.print_r($smtp_params, true).'</pre>';
            }

            $key = md5($setFromEmail.$smtp_params['smtp_server']);
            
            if(!isset($this->transport[$key])){
                if (isset($smtp_params['smtp_server']) && isset($smtp_params['port']) && isset($smtp_params['encryption']) && isset($smtp_params['password'])) {
                    $this->transport[$key] = Swift_SmtpTransport::newInstance($smtp_params['smtp_server'], (int) $smtp_params['port'], $smtp_params['encryption'])
                            ->setUsername($setFromEmail)
                            ->setPassword($smtp_params['password']);
                } else {
                    $this->transport[$key] =  Swift_SmtpTransport::newInstance();
                }
            }

            if(!isset($this->mailer[$key])){
                $this->mailer[$key] = Swift_Mailer::newInstance($this->transport[$key]);
            }
            if(!isset($this->swift_message[$key])){
                $this->swift_message[$key] = Swift_Message::newInstance();
            }
            $this->swift_message[$key]->setSubject($subject);
            $this->swift_message[$key]->setFrom(array($setFromEmail => $setFromName));
            $this->swift_message[$key]->setBody($content, 'text/html');

            if ($this->debug)
                $to_email = 'praew@common-services.com';

            $this->swift_message[$key]->setTo(array($to_email));
            $this->swift_message[$key]->setBcc(array('praew@common-services.com'));

            $attached = array();
            foreach ($additional as $attachment) {
                if (isset($attachment['content']) && isset($attachment['name']) && isset($attachment['mime'])){

                    $attached_content = new Swift_Attachment($attachment['content'], $attachment['name'], $attachment['mime']);
                    $this->swift_message[$key]->attach($attached_content);

                    $attached[] = $attached_content;
                }
            }

            sleep(60);
            try{
                $sent = $this->mailer[$key]->send($this->swift_message[$key]);
            } catch (Exception $ex) {
                file_put_contents(dirname(__FILE__).'/../../../assets/apps/users/'.$this->user.'/amazon/send_mail.txt', print_r($ex, true), FILE_APPEND);
            }
            
            foreach ($attached as $att){
                $this->swift_message[$key]->detach($att);
            }

            return isset($sent)?(bool) $sent:false;
        }
        return (false);
    }

    public function get_order_delivered($id_order){

        if (!isset($id_order) || empty($id_order))
            return false;

        $this->db->from($this->prefix_order.'order_shipping');
        $this->db->where(array('id_orders' => (int) $id_order));
        $result = $this->db->db_array_query($this->db->query);

        if(isset($result[0]['tracking_number']) && !empty($result[0]['tracking_number']))
            return true;

        return false;
    }

    public function get_maximum_send_mail(){

        $limit = 0;
        $mail_maximum = null;

        // get limit
        $table = self::TABLE;
        $where = array('field_name' => Messagings::$limit_field);
        $this->db->from($table);
        $this->db->where($where);
        $result = $this->db->db_array_query($this->db->query);

        if(isset($result[0]['field_name']) && $result[0]['field_name'] == Messagings::$limit_field)
            $mail_maximum = (int) $result[0]['field_value'];

        // get sent email count in 1 hour
        $sql = "SELECT * FROM ".self::TABLE."_history "
            . "WHERE field_name = 'sent_mail' "
            . "AND HOUR(date_upd)>=".(int)date("H",time())." "
            . "AND date_upd LIKE '".date("Y-m-d",time())."%' "
            . "ORDER BY date_upd DESC";
        $count_result = $this->db->db_array_query($sql);

        foreach ($count_result as $count){
            $limit += $count['field_value'];
        }

        if ($this->debug){
            if($mail_maximum){
                echo '<pre><b>Maximum Mail :</b></br/>' . print_r($mail_maximum, true).'</pre>';
                echo '<pre><b>SENT MAIL :</b></br/>' . print_r($limit, true).'</pre>';
                echo '<pre><b>LIMIT :</b></br/>' . print_r(($mail_maximum - $limit), true).'</pre>';
            } else {
                echo '<pre><b>Maximum Mail :</b></br/>' . print_r($mail_maximum, true).'</pre>';
            }
        }
        
        return isset($mail_maximum) ? (int)($mail_maximum - $limit) : $mail_maximum;
    }

    public static function goToSellerReviewPage($marketplace, $domain) {
        $marketplace = strtolower($marketplace);
        if (isset(self::$review_url[$marketplace])) {
            $url_review = sprintf(self::$review_url[$marketplace], $domain);
            return $url_review;
        } else {
            return (false);
        }
    }

}
