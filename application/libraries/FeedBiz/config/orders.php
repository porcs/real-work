<?php

require_once(dirname(__FILE__) . '/../../db.php');
require_once(dirname(__FILE__) . '/../orders/ObjectModelOrder.php');
require_once(dirname(__FILE__) . '/../orders/OrderLog.php');
require_once(dirname(__FILE__) . '/../orders/OrderShop.php');
require_once(dirname(__FILE__) . '/../orders/OrderBuyers.php');
require_once(dirname(__FILE__) . '/../orders/OrderCurrency.php');
require_once(dirname(__FILE__) . '/../orders/OrderTaxes.php');
require_once(dirname(__FILE__) . '/../orders/OrderInvoices.php');
require_once(dirname(__FILE__) . '/../orders/OrderItems.php');
require_once(dirname(__FILE__) . '/../orders/OrderItemsAttribute.php');
require_once(dirname(__FILE__) . '/../orders/OrderLanguage.php');
require_once(dirname(__FILE__) . '/../orders/OrderPayments.php');
require_once(dirname(__FILE__) . '/../orders/OrderSeller.php');
require_once(dirname(__FILE__) . '/../orders/OrderShippings.php');
require_once(dirname(__FILE__) . '/../orders/OrderShop.php');
require_once(dirname(__FILE__) . '/../orders/OrderStatus.php');
require_once(dirname(__FILE__) . '/../orders/Orders.php');
require_once(dirname(__FILE__) . '/../../tools.php');