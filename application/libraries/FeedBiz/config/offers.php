<?php

require_once(dirname(__FILE__) . '/../../db.php');
require_once(dirname(__FILE__) . '/../offers/ObjectModel.php');
require_once(dirname(__FILE__) . '/../offers/Product.php');
require_once(dirname(__FILE__) . '/../offers/ProductAttribute.php');
require_once(dirname(__FILE__) . '/../offers/ProductSale.php');
require_once(dirname(__FILE__) . '/../offers/Carrier.php');
require_once(dirname(__FILE__) . '/../offers/Category.php');
require_once(dirname(__FILE__) . '/../offers/Conditions.php');
require_once(dirname(__FILE__) . '/../offers/Currency.php');
require_once(dirname(__FILE__) . '/../offers/Language.php');
require_once(dirname(__FILE__) . '/../offers/Manufacturer.php');
require_once(dirname(__FILE__) . '/../offers/Tax.php');
require_once(dirname(__FILE__) . '/../offers/Supplier.php');
require_once(dirname(__FILE__) . '/../offers/Profile.php');
require_once(dirname(__FILE__) . '/../offers/Price.php');
require_once(dirname(__FILE__) . '/../offers/Rule.php');
require_once(dirname(__FILE__) . '/../offers/OTP.php');
require_once(dirname(__FILE__) . '/../offers/Shop.php');