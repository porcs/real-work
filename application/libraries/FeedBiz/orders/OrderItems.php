<?php

class OrderItems extends ObjectModelOrder {
        public $id_orders;
        public $id_order_items;
        public $id_marketplace_order_item_ref;
        public $id_shop;
        public $id_product;
        public $reference;
        public $id_marketplace_product;
        public $id_product_attribute;
        public $product_name;
        public $quentity;
        public $quentity_in_stock;
        public $quentity_refunded;
        public $product_weight;
        public $product_price;
        public $shipping_price;
        public $tax_rate;
        public $unit_price_tax_incl;
        public $unit_price_tax_excl;
        public $total_price_tax_incl;
        public $total_price_tax_excl;
        public $total_shipping_price_tax_incl;
        public $total_shipping_price_tax_excl;
        public $id_condition;
        public $promotion;
        public $message;
        public $id_status;

        public static $definition = array(
                'table'     => 'order_items',
                'fields'    => array(
                    'id_orders'                     =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_order_items'                =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_marketplace_order_item_ref' =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_shop'                       =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_product'                    =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'reference'                     =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
                    'id_marketplace_product'        =>  array('type' => 'varchar', 'required' => false, 'size' => 32),
                    'id_product_attribute'          =>  array('type' => 'int', 'required' => false, 'size' => 10),
                    'product_name'                  =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
                    'quantity'                      =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'quantity_in_stock'             =>  array('type' => 'int', 'required' => false, 'size' => 10),
                    'quantity_refunded'             =>  array('type' => 'int', 'required' => false, 'size' => 10),
                    'product_weight'                =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'product_price'                 =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'shipping_price'                =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'tax_rate'                      =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'unit_price_tax_incl'           =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'unit_price_tax_excl'           =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'total_price_tax_incl'          =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'total_price_tax_excl'          =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'total_shipping_price_tax_incl'     =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'total_shipping_price_tax_excl'     =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'id_condition'                  =>  array('type' => 'int', 'required' => false),
                    'promotion'                     =>  array('type' => 'varchar', 'required' => false, 'size' => 32),
                    'message'                       =>  array('type' => 'text', 'required' => false),
                    'id_status'                     =>  array('type' => 'int', 'required' => false ),
                ),
        );
                
	public static function getOrderItemReferenceById($id_order=null) {
            
                if (!isset($id_order) || empty($id_order))
                        return NULL;

		$order_items = array();
                $_db = ObjectModelOrder::$db;
                $_db->from('order_items');
		$_db->where( array('id_orders' => (int)$id_order) );
                
		$result = $_db->db_query_fetch($_db->query);

		foreach ( $result as $key => $set ) {
		    $key = $set['id_marketplace_order_item_ref'];
		    $order_items[$key]  = $set;
		    $order_items[$key]['attributes']  = OrderItemsAttribute::getOrderItemsAttributeById($set['id_order_items']);
		}
                
                return $order_items;
        }
	
        public static function getOrderItemById($id_order = null) {
		
		$order_items = array();
                if (!isset($id_order) || empty($id_order))
                        return NULL;

                $_db = ObjectModelOrder::$db;
                $_db->from('order_items');
                
                if ( $id_order ) {
                        $_db->where( array('id_orders' => (int)$id_order) );
                        $_db->where( array('quantity' => 0), ">" );
                }
                
                $result = $_db->db_query_fetch($_db->query);
  
                foreach ( $result as $key => $set ) {
		    
		    $key = $set['id_order_items'];
		    $order_items[$key] = $set;
                    $order_items[$key]['attributes']  = OrderItemsAttribute::getOrderItemsAttributeById($set['id_order_items']);
		    
                }
                
                return $result;
        }
                
        public function getOrderItem($id_orders = null) {
            
                $_db = ObjectModelOrder::$db;
                $_db->from('order_items');
                
                if ( $id_orders ) {
                        $_db->where( array('id_orders' => (int)$id_orders) );
                        $_db->where( array('quantity' => 0), ">" );
                }
                 $result = $_db->db_query_fetch($_db->query);
                 return $result;
        }
	
        public function getOrderItems($id_shop, $send_order_list=array()) {
	    
		foreach ($send_order_list as $order){
		    $list_id_orders[] = $order['id_orders'];
		}
		
		$order_items = array();
                $_db = ObjectModelOrder::$db;
                $_db->from('order_items');                
		$_db->where( array('id_shop' => (int)$id_shop) );
		$_db->where( array('id_orders' => "(".implode(', ',$list_id_orders).")" ), "IN", "AND" );
		
                 $result = $_db->db_query_fetch($_db->query);
		 
		 foreach ($result as $order_item){
		     $order_items[$order_item['id_orders']][$order_item['id_order_items']] = $order_item['id_order_items'];
		 }
                 return $order_items;
        }
	
}