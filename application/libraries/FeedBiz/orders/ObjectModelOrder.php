<?php
require_once(dirname(__FILE__) . '/../../db.php');
abstract class ObjectModelOrder {
    
    protected $id;
    protected static $user;
    public static $definition = array();
    protected $def;
    protected static $db = false;
    protected $_db = null;
    
    public static $database = 'orders';
    public $id_shop= '';
    
    public function __construct($user, $id = null, $id_shop = null, $database = null ) {  
        
            if (!isset($user) && !strlen($user))
                    return (false);        
            
            ObjectModelOrder::$user = $user;
            $class = get_class($this);
            if (!$class)
                    return (false);        

            $this->def = $class::$definition;
            if(isset($database) && $database != null && !empty($database))
                    $database_name = $database;
            else
                    $database_name = ObjectModelOrder::$database;        
            
            ObjectModelOrder::$db = new Db($user, $database_name );
            $this->_db = ObjectModelOrder::$db;

            if(!$this->_db)
                    return (false);
        
            if ($id) {
                    $this->id = (int)$id;
                    $where = array();
                    if ( isset($this->def) && is_array($this->def) && count($this->def) ) {
                            if (!(isset($this->def['table']) && strlen($this->def['table'])))
                                    return (false);

                        $this->_db->from($this->def['table'], 'a');

                        if (!(isset($this->def['primary'][0]) && strlen($this->def['primary'][0])))
                                return (false);
                        
                        $where['a.' . $this->def['primary'][0]] = $this->id;
                        
                        if ($id_shop) 
                                $where['a.id_shop'] = (int)$this->id_shop;

                        $this->_db->where( $where );

                        $object_datas = $this->_db->db_array_query($this->_db->query);

                        if (is_array($object_datas) && count($object_datas))
                                foreach ($object_datas as $values)
                                        foreach ($values as $key => $value)
                                            if (array_key_exists($key, $this)) {
                                                    $this->{$key} = $value;
                                            }

                        foreach ($this->def['fields'] as $fields_key => $fields )
                        {
                            if($fields['type'] == 'int' || $fields['type'] == 'tinyint' || $fields['type'] == 'INTEGER')
                                settype($this->{$fields_key}, "integer");

                            if($fields['type'] == 'decimal')
                                settype($this->{$fields_key}, "float");
                        }
                    }
            }
            unset($this->def);
            unset($this->_db);
    }
    
    public function get_tmpcode($table, $column, $id)
    {
        $tmp = '';
        $this->_db->select(array('tmpcode'));
        $this->_db->from($table);
        $this->_db->where(array($column => $id));
        
        $result = $this->_db->db_array_query($this->_db->query);
        
        if (isset($result) && !empty($result))
            foreach ($result as $value)
                $tmp = $value['tmpcode'];
        
        return $tmp;
    }
    
    
    public static function get_marketplace_by_id($id) {
	
	$name = '';
	$mysql_db = new ci_db_connect();  
        $query = $mysql_db->select_query("SELECT name_offer_pkg FROM offer_packages WHERE id_offer_pkg = ".(int)$id.";" );
	
        while($row = $mysql_db->fetch($query)){ 
	    $name = $row['name_offer_pkg'];
	}
	
        return $name;
    }
    
    public static function get_region_by_id_site($id_marketplace, $id) {
	
	$mysql_db = new ci_db_connect();  
	$where = 'id_offer_sub_pkg = ' . $id;
	
	switch ($id_marketplace){
	    // eBay
	    case '3' : $where = "id_offer_sub_pkg = (SELECT id_offer_sub_pkg FROM offer_price_packages WHERE id_site_ebay = $id)";
	}
	    
        $query = $mysql_db->select_query("SELECT * FROM offer_sub_packages WHERE $where ;" );
	
        while($row = $mysql_db->fetch($query)){ 
	    if(isset($row['iso_code']) && !empty($row['iso_code'])){
		return  $row['iso_code'];
	    } else {
		return  $row['title_offer_sub_pkg'];
	    }
	}
    }

    public static function get_order_cancel_reason() {
        $reasons = array();
	$mysql_db = new ci_db_connect();
        $query = $mysql_db->select_query("SELECT * FROM order_cancel_reasons ;" );

        while($row = $mysql_db->fetch($query)){
            foreach ($row as $key => $data){
                if(!is_int($key)){
                    $reasons[$row['marketplace']][$row['reason_key']][$key] = $data;
                }
            }
        }
        return $reasons ;

    }
}
