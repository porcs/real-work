<?php

class OrderShop extends ObjectModelOrder {
    
        public static $definition = array(
                'table'     => 'shop',
                'primary' => array('id_shop'),
                'fields'    => array(
                        'id_shop'       =>  array('type' => 'INTEGER', 'required' => true,  'primary_key' => true),
                        'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 64 ),
                        'is_default'    =>  array('type' => 'int', 'required' => false, 'size' => 1),
                        'active'        =>  array('type' => 'int', 'required' => false, 'size' => 1),
                        'description'   =>  array('type' => 'text', 'required' => false),
                        'date_add'      =>  array('type' => 'datetime', 'required' => true ),
                ),
        );
        
        public static function getDefaultShop() {
            
                $shops = array();

                $_db = new Db( ObjectModelOrder::$user, 'products');
                $_db->from('shop');
                $_db->where(array('is_default' => 1));

                $result = $_db->db_array_query($_db->query);

                foreach ($result as $key => $shop)
                        foreach ($shop as $skey => $svalue)
                                foreach (self::$definition['fields'] as $fkey => $fvalue)
                                        if($skey == $fkey)
                                                $shops[$fkey] = $shop[$fkey];
                return $shops;
        }
   
}