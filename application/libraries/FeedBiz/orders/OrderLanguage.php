<?php

class OrderLanguage extends ObjectModelOrder
{
    public static $definition = array(
        'table'     => 'language',
        'primary'   => array('id_lang'),
        'fields'    => array(
            'id_lang'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'      =>  array('type' => 'varchar', 'required' => true, 'size' => 32),
            'iso_code'  =>  array('type' => 'varchar', 'required' => true, 'size' => 2),
            'is_default'    =>  array('type' => 'tinyint', 'required' => true, 'size' => 1 ),
        ),
    );
  
    public static function getLanguageDefault($id_shop)
    {
        $languages = array();
        
        $_db = new Db( ObjectModelOrder::$user, 'products');
        $_db->from('language');
        $_db->where(array('is_default' => 1, 'id_shop' => (int)$id_shop));
        
        $result = $_db->db_array_query($_db->query);
        foreach ($result as $language)
        {
            $languages['id_lang'] = $language['id_lang'];
            $languages['name'] = $language['name'];
            $languages['iso_code'] = $language['iso_code'];
        }
            
        return $languages;
    }
  
    public static function getIsoCodeById($id_lang)
    {
        $languages = array();
        
        $_db = new Db( ObjectModelOrder::$user, 'products');
        $_db->from('language');
        $_db->where(array('id_lang' => (int)$id_lang));
        
        $result = $_db->db_array_query($_db->query);
        foreach ($result as $language) {
            $languages = $language['iso_code'];
        }
            
        return $languages;
    }
    
    public static function getLanguages()
    {        
	$languages = array();
        $_db = new Db( ObjectModelOrder::$user, 'products');
        $_db->from('language');
        
        $result = $_db->db_array_query($_db->query);
        
	foreach ($result as $language) {
	    $languages[$language['id_lang']] = $language;
        }
	
	return $languages ;
    }
    
    
}