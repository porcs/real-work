<?php

class OrderPayments extends ObjectModelOrder {
    
        public $id_orders;
        public $id_order_items;
        public $payment_method;
        public $payment_status;
        public $id_currency;
        public $Amount;
        public $external_transaction_id;

        public static $definition = array(
                'table'     => 'order_payment',
                'primary'   => array('id_orders','id_order_items'),
                'unique'        => array('order_payment' => array('id_orders','id_order_items')),
                'fields'    => array(
                    'id_orders'                 =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_order_items'            =>  array('type' => 'int', 'required' => false, 'size' => 10),
                    'payment_method'            =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'payment_status'            =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'id_currency'               =>  array('type' => 'int', 'required' => false, 'size' => 10),
                    'Amount'                    =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'external_transaction_id'   =>  array('type' => 'varchar', 'required' => false, 'size' => 64),
                ),
        );
    
        public static function getOrderPaymentById($id_order = null) {
                if (!isset($id_order) || empty($id_order))
                        return NULL;

                $_db = ObjectModelOrder::$db;
                $_db->from('order_payment');
                if ( $id_order )
                        $_db->where( array('id_orders' => (int)$id_order) );
                
                $result = $_db->db_query_fetch($_db->query);
                $result = current($result);

                foreach (self::$definition['fields'] as $fields_key => $fields ) {
                        if($fields['type'] == 'int' || $fields['type'] == 'tinyint' || $fields['type'] == 'INTEGER')
                                settype($result[$fields_key], "integer");

                        if($fields['type'] == 'float') { 
                                settype($result[$fields_key], "float");
                        }
                }
                           
                unset($result['id_orders'], $result['id_order_items']);
                return $result;
        }
        
        public function getOrderPayment($id_orders = null) {
                
                $_db = ObjectModelOrder::$db;

                $_db->from('order_payment');
                if ( $id_orders )
                        $_db->where( array('id_orders' => (int)$id_orders) );                
                
                $result = $_db->db_query_fetch($_db->query);
                return current($result);
        }
}