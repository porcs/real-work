<?php

class CurrencyOrder extends ObjectModelOrder {
    
        public static $definition = array(
                'table'     => 'shop',
                'primary' => array('id_shop'),
                'fields'    => array(
                        'id_currency'   =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
                        'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 64 ),
                        'iso_code'      =>  array('type' => 'varchar', 'required' => false, 'size' => 64 ),
                        'name'          =>  array('type' => 'varchar', 'required' => false, 'size' => 64 ),
                        'date_add'      =>  array('type' => 'date', 'required' => false),
                ),
        );
        
        public function getCurrency( $id_currency, $id_shop ) {
            
                if ( !isset( $id_currency ) || empty( $id_currency ) || !isset( $id_shop ) || empty( $id_shop ) ) {
                        return NULL;
                }                
                
                $_db = new Db( ObjectModelOrder::$user, 'products');
                $_db->from('currency');
                $_db->where(array('id_shop' => $id_shop, 'id_currency' => $id_currency));
                
                $result = $_db->db_query_fetch($_db->query);
                return $result;
        }
   
}