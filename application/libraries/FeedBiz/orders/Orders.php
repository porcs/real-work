<?php

require_once(dirname(__FILE__) . '/../config/stock.php');
require_once(dirname(__FILE__) . '/../Marketplaces.php');

class Orders extends ObjectModelOrder {
        
        public $id_orders; 
        public $id_marketplace;
        public $id_marketplace_order_ref;
        public $marketplace_order_number;
        public $sales_channel;
        public $id_shop; 
        public $id_lang; 
        public $site; 
        public $order_type;
        public $order_status;
        public $payment_method; 
        public $total_discount; 
        public $total_amount;
        public $total_paid;
        public $total_shipping;
        public $order_date; 
        public $shipping_date;
        public $latest_shipping_date;
        public $delivery_date;
        public $latest_delivery_date;
        public $purchase_date; 
        public $affiliate_id; 
        public $commission; 
        public $ip;
        public $gift_message; 
        public $gift_amount;
        public $comment;
        public $status;
        public $date_add;
        public $id_currency;
        public $stock;
	
        const LIMIT_CHART_TOP_NUM = 4; 
        private static $exceptions_shop_order = ' and o.id_marketplace != 0 ';//' and o.id_marketplace_order_ref != 0 '; // exclude shop orders  id_marketplace_order_ref = 0
	
        public static $definition = array(
            'table'     => 'orders',
            'primary'   => array('id_orders'),
            'fields'    => array(
                                'id_orders' => array('type' => 'int', 'required' => true, 'size' => 10),
                                'id_marketplace' => array('type' => 'int', 'required' => true, 'size' => 10),
                                'id_marketplace_order_ref'=> array('type' => 'varchar', 'required' => true, 'size' => 64),
                                'marketplace_order_number'=> array('type' => 'varchar', 'required' => false, 'size' => 64),
                                'sales_channel' => array('type' => 'varchar', 'required' => true, 'size' => 64),
                                'id_shop' => array('type' => 'int', 'required' => true, 'size' => 10),
                                'site' => array('type' => 'varchar', 'required' => true, 'size' => 128),
                                'id_lang' => array('type' => 'int', 'required' => false, 'size' => 10),
                                'order_type'=> array('type' => 'varchar', 'required' => true, 'size' => 64),
                                'order_status'=> array('type' => 'varchar', 'required' => false, 'size' => 64),
                                'payment_method' => array('type' => 'varchar', 'required' => false, 'size' => 64),
                                'id_currency' => array('type' => 'varchar', 'required' => false, 'size' => 64),
                                'total_discount' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                                'total_amount'=> array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                                'total_paid'=> array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                                'total_shipping'=> array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                                'order_date' => array('type' => 'datetime', 'required' => false ),
                                'shipping_date' => array('type' => 'datetime', 'required' => false ),
                                'latest_shipping_date' => array('type' => 'datetime', 'required' => false ),
                                'delivery_date' => array('type' => 'datetime', 'required' => false ),
                                'latest_delivery_date' => array('type' => 'datetime', 'required' => false ),
                                'purchase_date' => array('type' => 'datetime', 'required' => false ),
                                'affiliate_id' => array('type' => 'int', 'required' => false, 'size' => 10),
                                'commission' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                                'ip'=> array('type' => 'varchar', 'required' => true, 'size' => 15 ),
                                'gift_message' => array('type' => 'text', 'required' => false),
                                'gift_amount'=> array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                                'comment'=> array('type' => 'text', 'required' => false),
                                'status'=> array('type' => 'tinyint', 'required' => false),
                                'date_add'=> array('type' => 'datetime', 'required' => false ),
				'shipping_status'=> array('type' => 'tinyint', 'required' => false),
				'flag_error_email'=> array('type' => 'tinyint', 'required' => false),
                                'flag_canceled_status'=> array('type' => 'tinyint', 'required' => false),
                            ),
        );

        public function __construct( $user, $id_order = null, $debug = null ) {
		
		$this->_user = $user;
                parent::__construct( $user, (int)$id_order );
                
		$this->debug = $debug;
		
                if ($this->id) {
                        
                        // send language iso_code to module
                        $this->language = OrderLanguage::getIsoCodeById($this->id_lang); 
                        
                        // send currency iso_code to module
                        $this->currency = $this->id_currency; 
                        unset( $this->id_currency ); 
                        
			// buyer & shipping
			$OrderBuyers = OrderBuyers::getOrderBuyerById((int)$this->id);			
                        $this->buyer = $OrderBuyers['buyer'];                        
                        $this->shipping = $OrderBuyers['shipping'];       
			                     
                        $this->carrier = OrderShippings::getOrderShippingById((int)$this->id);
                        $this->payment = OrderPayments::getOrderPaymentById((int)$this->id);
                        $this->invoices = OrderInvoices::getOrderInvoiceById((int)$this->id);
                        $this->item = OrderItems::getOrderItemById((int)$this->id);   
                }  
                
                $this->stock = new StockMovement($user);
        }
	
	public function importOrders( $orders ) {
    
                if(!isset($orders) || empty($orders))
                    return array('pass'=>false,'output'=>'empty order') ;

                if(!is_array($orders)){ // convert to array
                    $orders = xmlToarray($orders);
                }

                require_once(dirname(__FILE__) . '/../../FeedBiz.php');
                $feedbiz = new FeedBiz(array($this->_user));

                $sorder = $uorder = $order_item = $order_item_attr = $invoice = $payment = $order_list = array();
                $total_discount = $total_shipping = $total_amount = $gift_amount = $total_quantity = $total_tax_rate = $total_shipping_tax = 0;
                $message = $sql = $gift_message = null;
                $today_date = date("Y-m-d H:i:s");

                // shop detail
                $default_shop = $feedbiz->getDefaultShop($this->_user);
                if($orders['IdShop'] <> 1 && isset($default_shop['id_shop'])){
                    $orders['IdShop'] = $default_shop['id_shop'];
                }
                $id_shop = isset($orders['IdShop']) ? (int) $orders['IdShop'] : (isset($default_shop['id_shop']) ? $default_shop['id_shop'] : null) ;
                $shop_name =  isset($orders['ShopName']) ? (string) $orders['ShopName'] : (isset($default_shop['name']) ? $default_shop['name'] : null) ;

                if(!isset($id_shop) || empty($id_shop))
                    return array('pass'=>false,'output'=>'Shop are required') ;

                $order_status = isset($orders['Info']['OrderStatus']) ? (string) $orders['Info']['OrderStatus'] : 'Unshipped';
                $order_type = isset($orders['Info']['OrderType']) ? (string) $orders['Info']['OrderType'] : 'Standard';
                $sales_channel = isset($orders['Info']['SaleChannel']) ? (string) $orders['Info']['SaleChannel'] : $shop_name ;
                $id_marketplace = isset($sales_channel) ? (int) $feedbiz->get_marketplace_by_name($sales_channel) : 0;

                if(!isset($orders['LanguageCode']) || empty($orders['LanguageCode']))
                    return array('pass'=>false,'output'=>'Missing Language Code') ;

                $language  = $feedbiz->checkLanguage((string) $orders['LanguageCode'], $id_shop);

                $site = (int) Marketplaces::getIdCountryByIso((string) $orders['LanguageCode']);
                $id_lang = isset($orders['LanguageId']) ? (int) $orders['LanguageId'] : (isset($language['id_lang']) ? (int) $language['id_lang'] : null) ;

                // Order Reference
                $order_id = (string) $orders['References']['Reference']; // Marketplace order id reference
                $seller_order_id = isset($orders['References']['Id']) ? (int) $orders['References']['Id'] : null;
                $invoice_number = isset($orders['References']['InvoiceNumber']) ? (string) $orders['References']['InvoiceNumber'] : null;

                // check order exits by seller order id
                if(isset($seller_order_id) && !empty($seller_order_id)) {
                    $check_order = $this->getOrderBySellerOrderId($seller_order_id);
                    if(!empty($check_order)){
                        if($this->debug) {
                            echo sprintf('%s: %s::%s(%d) - Order %s , already exist, ', basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__,print_r($check_order, true));
                        }
                        return array('pass'=>false,'output'=>sprintf('Order already exist, Seller Order id : %s', $seller_order_id)) ;
                    }
                }

                // check order exits by order reference
                $check_order = $this->getOrderByMPID($order_id);

                if(!empty($check_order)){
                    if($this->debug) {
                        echo sprintf('%s: %s::%s(%d) - Order %s , already exist, ', basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__,print_r($check_order, true));
                    }
                    return array('pass'=>false,'output'=>sprintf('Order already exist, Order Reference : %s', $order_id)) ;
                }

                $id_currency = (int) $orders['CurrencyId'] ; // find currency id by code
                $currency = (string) $orders['CurrencyCode'] ;
                $total_paid = (float) $orders['Total']['Paid']['Amount'];

                //Save Order//
                $sorder['id_marketplace_order_ref'] = $order_id;
                $sorder['sales_channel'] = $sales_channel;
                $sorder['id_shop'] = (int) $id_shop;
                $sorder['site'] = $site;
                $sorder['id_marketplace'] = $id_marketplace;
                $sorder['id_lang'] = (int) $id_lang;
                $sorder['id_currency'] = $currency;
                $sorder['order_type'] = $order_type;
                $sorder['order_status'] = $order_status;
                $sorder['payment_method'] = (string) $orders['Info']['Payment'];
                $sorder['purchase_date'] = (string) $orders['Billing']['DateAdd'];
                $sorder['order_date'] = (string) $orders['Created'];
                $sorder['shipping_date'] = isset($orders['Delivery']['ShippingDate']) ? (string) $orders['Delivery']['ShippingDate'] : null ;
                $sorder['delivery_date'] = isset($orders['Delivery']['DeliveryDate']) ? (string) $orders['Delivery']['DeliveryDate'] : null ;
                $sorder['total_paid'] = $total_paid;
                $sorder['date_add'] = $today_date;
                $sorder['ip'] = $seller_order_id;

                // marketplace_order_number 5/6/2015
                $sorder['marketplace_order_number'] = $order_id;
                
                if (!$id_order = $this->saveOrder('orders', $sorder)) {
                    return array('pass'=>false,'output'=>sprintf('Can not save order id : %s', print_r($sorder, true))) ;
                }

                //Item
                if (isset($orders['Products']) && !empty($orders['Products'])) {

                    foreach ($orders['Products'] as $item) {

                        $id_item = (int) $item['id_order_detail'];

                        // Check SKU
                        if (!isset($item['References']['ProductId']) || empty($item['References']['ProductId'])) {
                            continue;
                        }

                        $id_product = (int) $item['References']['ProductId'];
                        $id_product_attribute =  isset($item['References']['ProductAttributeId']) ? (int)$item['References']['ProductAttributeId'] : null;

                        // Product
                        if (!$product = new Product($this->_user, $id_product)) {
                            continue;
                        }

                        $order_item['id_orders'] = $id_order;
                        $order_item['reference'] = (string) $item['References']['ProductReference'];
                        $order_item['id_shop'] = (int) $id_shop;
                        $order_item['id_product'] = $id_product;
                        $order_item['id_product_attribute'] = $id_product_attribute;
                        $order_item['product_name'] = (string) $item['References']['ProductName'];
                        $order_item['quantity'] = (int) $item['Quantity']['Ordered'];
                        $order_item['id_marketplace_order_item_ref'] = $id_item;
                        $order_item['id_marketplace_product'] = (string) $item['References']['ProductReference'];
                        $total_quantity = $total_quantity + $order_item['quantity'];

                        $order_item['quantity_in_stock'] = (int) $item['Quantity']['InStock'];

                        // Product weight
                        $product_weight = isset($product->weight['value']) ? $product->weight['value'] : null;
                        if (isset($product->combination[$id_product_attribute]['weight']['value']))
                            $product_weight = $product->combination[$id_product_attribute]['weight']['value'];
                        $order_item['product_weight'] = $product_weight;

                        // Product price
                        $product_price = $product->price;
                        if (isset($product->combination[$id_product_attribute]))
                            if (isset($product->combination[$id_product_attribute]['price']))
                                $product_price = $product->combination[$id_product_attribute]['price'];
                        $order_item['product_price'] = $product_price;


                        // Shipping
                        $order_item['shipping_price'] = (float) $item['Shipping']['Price'];
                        $total_shipping = ( $total_shipping + $order_item['shipping_price'] );

                        // Tax
                        $order_item['tax_rate'] = (float) $item['Taxes']['Rate'];
                        $total_tax_rate = $total_tax_rate + $order_item['tax_rate'];
                        $total_shipping_tax = $total_shipping_tax + (float) $item['Taxes']['Rate'];

                        // Before VAT
                        $order_item['unit_price_tax_excl'] = (float) $item['Prices']['UnitTaxExcl'];
                        $order_item['total_price_tax_excl'] = (float) $item['Prices']['TotalTaxExcl'];
                        $order_item['total_shipping_price_tax_excl'] = (float) $item['Shipping']['TotalTaxExcl'];

                        // After VAT
                        $order_item['unit_price_tax_incl'] = (float) $item['Prices']['UnitTaxIncl'];
                        $order_item['total_price_tax_incl'] = (float) $item['Prices']['TotalTaxIncl'];
                        $order_item['total_shipping_price_tax_incl'] = (float) $item['Prices']['TotalTaxIncl'];

                        // Condition
                        $order_item['id_condition'] = $product->id_condition;

                        // Discount
                        if (isset($item['Discount']['AmountTaxIncl']) && $item['Discount']['AmountTaxIncl'] > 0)
                            $total_discount = ( $total_discount + (float) $item['Discount']['AmountTaxIncl'] );

                        // Total Amount
                        $total_amount = $total_amount + $order_item['total_price_tax_incl'];

                        if (!$id_order_item = $this->saveOrder('order_items', $order_item)) {
                            continue;
                        }

                        //Order item attribute
                        if (isset($product->combination[$id_product_attribute]['attributes'])) {
                            foreach ($product->combination[$id_product_attribute]['attributes'] as $attr_key => $attr_value) {
                                $order_item_attr['id_order_items'] = $id_order_item;
                                $order_item_attr['id_attribute_group'] = $attr_key;

                                foreach ($product->combination[$id_product_attribute]['attributes'][$attr_key]['value'] as $id_attribute => $attr_val) {
                                    $order_item_attr['id_attribute'] = $id_attribute;
                                }
                                $order_item_attr['id_shop'] = (int) $id_shop;
                                $sql .= $this->saveOrder('order_items_attribute', $order_item_attr, true);
                            }
                        }

                        // payment
                        $payment['id_orders'] = $id_order;
                        $payment['id_order_items'] = $id_order_item;
                        $payment['payment_method'] = isset($orders['Info']['Payment']) ? $orders['Info']['Payment'] : null;
                        $payment['payment_status'] = $order_status;
                        $payment['id_currency'] = $id_currency;
                        $payment['Amount'] = (float) $item['Prices']['TotalTaxIncl'];

                        $sql .= $this->saveOrder('order_payment', $payment, true);

                    }
                }

                //Gift Message
                if(isset($orders['Info']['Gift']['value']) && $orders['Info']['Gift']['value'] == "Yes")
                    $gift_message = (string) $orders['Info']['Gift']['Message'];

                //check empty Order skip
                if (empty($orders['Products']) || empty($order_item)) {

                    // Update Order Status
                    $eorder = array(
                        'status' => 'error',
                        'comment' => 'Product items mismatch'
                    );
                    $where = array(
                        'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
                        'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
                        'id_marketplace' => array('operation' => '=', 'value' =>  $id_marketplace ),
                        'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
                    );

                    $e_sql = $this->saveOrder('orders', $eorder, false, 'update_string', $where);
                }
                
                // Buyer
                if (isset($orders['Customer'])) {
                    $buyer = array();
                    $buyer['id_orders'] = $id_order;
                    $buyer['site'] = $site;
                    $buyer['id_buyer_ref'] = $orders['Customer']['id'];
                    $buyer['email'] = (string) $orders['Customer']['Email'];
                    $buyer['name'] = (string) $orders['Customer']['Firstname'] . ' ' . (string) $orders['Customer']['Lastname'];
                    $buyer['address_name'] = (string) $orders['Delivery']['Firstname'].' '.(string) $orders['Delivery']['Lastname'].' '.(string) $orders['Delivery']['Company'] ;
                    $buyer['id_address_ref'] = isset($orders['Delivery']['Alias']) ? (string) $orders['Delivery']['Alias'] : null ;
                    $buyer['address1'] = (string) $orders['Delivery']['Address1'];
                    $buyer['address2'] = (string) $orders['Delivery']['Address2'];
                    $buyer['city'] = (string) $orders['Delivery']['City'];
                    $buyer['state_region'] = isset($orders['Delivery']['state']) && !empty($orders['Delivery']['state']) ?
                                            (string) $orders['Delivery']['state'] : strtoupper((string) $orders['LanguageCode'] );
                    $buyer['postal_code'] = (string) $orders['Delivery']['Postcode'];
                    $buyer['country_code'] = (string) $orders['Delivery']['country'];
                    $buyer['phone'] = (string) $orders['Delivery']['Phone'];

                    $sql .= $this->saveOrder('order_buyer', $buyer, true);
                }

                // Shipping
                $shipping = array();
                $shipping['id_orders'] = $id_order;
                $shipping['shipment_service'] = trim((string) $orders['Carrier']['Name']);
                $shipping['shipping_services_level'] = isset($orders['Carrier']['ShipmentServiceLevel']) ? trim((string)$orders['Carrier']['ShipmentServiceLevel']) : 'Standard';
                $shipping['id_carrier'] = (int) $orders['Carrier']['id'];

                $sql .= $this->saveOrder('order_shipping', $shipping, true);

                // Invoice
                $invoice['id_orders'] = $id_order;
                $invoice['seller_order_id'] = $seller_order_id;
                $invoice['invoice_no'] = $invoice_number;
                $invoice['total_discount_tax_incl'] = $total_discount;
                $invoice['total_paid_tax_incl'] = $total_paid;
                $invoice['total_products'] = $total_quantity;
                $invoice['total_shipping_tax_incl'] = $total_shipping;
                $invoice['date_add'] = $today_date;

                $sql .= $this->saveOrder('order_invoice', $invoice, true);

                // Taxes
                $taxes['id_orders'] = $id_order;
                $taxes['tax_amount'] = $total_tax_rate;
                $taxes['tax_on_shipping_amount'] = $total_shipping_tax;

                $sql .= $this->saveOrder('order_taxes', $taxes, true);

                // Update Order
                $uorder['total_shipping'] = $total_shipping;
                $uorder['total_discount'] = $total_discount;
                $uorder['total_amount'] = $total_amount;
                $uorder['gift_message'] = $gift_message;
                $uorder['gift_amount'] = $gift_amount;
                $uorder['comment'] = $message;
                $uorder['status'] = 1; // sent

                $where = array(
                    'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
                    'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
                    'id_marketplace' => array('operation' => '=', 'value' =>  $id_marketplace ),
                    'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
                );

                $sql .= $this->saveOrder('orders', $uorder, true, 'update_string', $where);

                //saveOrder($table, $data, $return_sql=false, $type='insert_string', $where=array(), $only_exec=false, $no_prefix=false)
                if (!$this->saveOrder('multi', $sql, false, '', array(), true)) {

                    array('pass'=>false,'output'=>sprintf('Can not save order item : ', print_r($sql, true))) ;

                    // Update Order Status
                    $eorder = array(
                        'status' => 'error',
                        'comment' => 'Unable to save order items'
                    );

                    $where = array(
                        'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
                        'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
                        'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
                    );

                    $e_sql = $this->saveOrder('orders', $eorder, false, 'update_string', $where);

                }

                return array('pass'=>true, 'id_order' => $id_order, 'id_shop' => $id_shop, 'shop_name' => $shop_name, 'site' => $site);
	    
	}
	
	public function updateOrders( $id_order, $orders ) {
		
		if(!isset($orders) || empty($orders))
		    return array('pass'=>false,'output'=>'empty order') ;
		
		require_once(dirname(__FILE__) . '/../config/products.php');
		
		$uorder = $order_item = $order_item_attr = $invoice = $payment = $order_list = array();
		$total_discount = $total_shipping = $total_amount = $gift_amount = $total_quantity = $total_tax_rate = $total_shipping_tax = 0;
		$message = $sql = $gift_message = null;
		$today_date = date("Y-m-d H:i:s");
		$order_status = 'Unshipped';
		$order_type = 'Standard';
		$id_marketplace = 0;
		
		//Params
		$id_shop = (int) $orders['IdShop'];
		$shop_name = (string) $orders['ShopName'];
		$site = (int) Marketplaces::getIdCountryByIso((string) $orders['LanguageCode']);		
		$id_lang = (int) $orders['LanguageId'];

		// Amazon Order Id
		$order_id = (string) $orders->References->Reference;       
		$seller_order_id = (int) $orders->References->Id;       
		$invoice_number = (string) $orders->References->InvoiceNumber;    // 26/01/2016
		
		$id_currency = (int) $orders['CurrencyId'] ;
		$currency = (string) $orders['CurrencyCode'] ;
		$total_paid = (float) $orders->Total->Paid->Amount;
		$sales_channel = (string) $orders['ShopName'] ;

		//Save Order//        
		$uorder['id_marketplace_order_ref'] = $order_id;
		$uorder['sales_channel'] = $sales_channel;
		$uorder['id_shop'] = $id_shop;
		$uorder['site'] = $site;
		$uorder['id_marketplace'] = $id_marketplace;
		$uorder['id_lang'] = (int) $id_lang;		
		$uorder['id_currency'] = $currency;
		$uorder['order_type'] = $order_type;
		$uorder['order_status'] = $order_status;
		$uorder['payment_method'] = (string) $orders->Info->Payment;
		$uorder['purchase_date'] = (string) $orders->Billing->DateAdd;
		$uorder['order_date'] = (string) $orders['Created'];
		$uorder['shipping_date'] = null;
		$uorder['delivery_date'] = null;
		$uorder['total_paid'] = $total_paid;
		$uorder['date_add'] = $today_date;
		$uorder['ip'] = $seller_order_id;
		$uorder['marketplace_order_number'] = $order_id;
		
		/*$where_order = array(
		    'id_orders'=> array('operation' => '=', 'value' => $id_order),
		    'site'=>array('operation' => '=', 'value' => $site),
		    'id_shop'=>array('operation' => '=', 'value' => $id_shop),
		);
		
		if (!$this->saveOrder('orders', $sorder, false, 'update_string', $where_order)) {			   
		    return array('pass'=>false,'output'=>sprintf('Can not update order id : %s', print_r($sorder, true))) ;
		}*/
	   
		//Item 		
		if (isset($orders->Products->Product) && !empty($orders->Products->Product)) {
		    
		    $items = OrderItems::getOrderItemReferenceById((int) $id_order);
		    		    
		    foreach ($orders->Products->Product as $item) {			
			
			$id_item = (int) $item['id_order_detail'];			
			
			// Check SKU
			if (!isset($item->References->ProductId) || empty($item->References->ProductId)) {			  
			    continue;
			}
			
			$id_product = (int) $item->References->ProductId;
			$id_product_attribute =  isset($item->References->ProductAttributeId) ? (int)$item->References->ProductAttributeId : null;
			
			// Product
			if (!$product = new Product($this->_user, $id_product)) {			  
			    continue;
			}
			
			$order_item['id_orders'] = $id_order;
			$order_item['reference'] = (string) $item->References->ProductReference;
			$order_item['id_shop'] = (int) $id_shop;
			$order_item['id_product'] = $id_product;
			$order_item['id_product_attribute'] = $id_product_attribute;
			$order_item['product_name'] = (string) $item->References->ProductName;
			$order_item['quantity'] = (int) $item->Quantity->Ordered;
			$order_item['id_marketplace_order_item_ref'] = $id_item;
			$order_item['id_marketplace_product'] = (string) $item->References->ProductReference;
			$total_quantity = $total_quantity + $order_item['quantity'];

			// Product quantity						
			$order_item['quantity_in_stock'] = (int) $item->Quantity->InStock;

			// Product weight
			$product_weight = isset($product->weight['value']) ? $product->weight['value'] : null;
			if (isset($product->combination[$id_product_attribute]['weight']['value']))
			    $product_weight = $product->combination[$id_product_attribute]['weight']['value'];			
			$order_item['product_weight'] = $product_weight;
			
			// Product price
			$product_price = $product->price;
			if (isset($product->combination[$id_product_attribute]))
			    if (isset($product->combination[$id_product_attribute]['price']))
				$product_price = $product->combination[$id_product_attribute]['price'];
			$order_item['product_price'] = $product_price;


			// Shipping 
			$order_item['shipping_price'] = (float) $item->Shipping->Price;
			$total_shipping = ( $total_shipping + $order_item['shipping_price'] );
						
			// Tax
			$order_item['tax_rate'] = (float) $item->Taxes->Rate;
			$total_tax_rate = $total_tax_rate + $order_item['tax_rate'];
			$total_shipping_tax = $total_shipping_tax + (float) $item->Taxes->Rate;

			// Before VAT
			$order_item['unit_price_tax_excl'] = (float) $item->Prices->UnitTaxExcl;
			$order_item['total_price_tax_excl'] = (float) $item->Prices->TotalTaxExcl;
			$order_item['total_shipping_price_tax_excl'] = (float) $item->Shipping->TotalTaxExcl;

			// After VAT
			$order_item['unit_price_tax_incl'] = (float) $item->Prices->UnitTaxIncl;
			$order_item['total_price_tax_incl'] = (float) $item->Prices->TotalTaxIncl;
			$order_item['total_shipping_price_tax_incl'] = (float) $item->Prices->TotalTaxIncl;

			// Condition
			$order_item['id_condition'] = $product->id_condition;

			// Discount
			if (isset($item->Discount->AmountTaxIncl) && $item->Discount->AmountTaxIncl > 0)
			    $total_discount = ( $total_discount + (float) $item->Discount->AmountTaxIncl );

			// Total Amount
			$total_amount = $total_amount + $order_item['total_price_tax_incl'];
			
			if (!isset($items[$id_item]['id_order_items']) || !$items[$id_item]['id_order_items'] || empty($items[$id_item]['id_order_items'])){
			    // save order items
			    $id_order_item = $this->saveOrder('order_items', $order_item, false);
			
			} else {	  
			    
			    // update order items
			    $id_order_item = $items[$id_item]['id_order_items'];

			    $where_order_item = array(
				'id_orders'=> array('operation' => '=', 'value' => $id_order),
				'id_marketplace_order_item_ref'=>array('operation' => '=', 'value' => $id_item),
				'id_shop'=>array('operation' => '=', 'value' => $id_shop),
			    );

			    $sql .= $this->saveOrder('order_items', $order_item, true, 'update_string', $where_order_item);
			}
			
			//Order item attribute
			if (isset($product->combination[$id_product_attribute]['attributes'])) {
			    foreach ($product->combination[$id_product_attribute]['attributes'] as $attr_key => $attr_value) {
				$order_item_attr['id_attribute_group'] = $attr_key;

				foreach ($product->combination[$id_product_attribute]['attributes'][$attr_key]['value'] as $id_attribute => $attr_val) {
				    $order_item_attr['id_attribute'] = $id_attribute;
				}
				
				$where = array(
				    'id_order_items'=> array('operation' => '=', 'value' => $id_order_item),
				    'id_shop'=>array('operation' => '=', 'value' => $id_shop),
				);
				
				//($table, $data, $return_sql=false, $type='insert_string', $where=array(), $only_exec=false, $no_prefix=false)
				$sql .= $this->saveOrder('order_items_attribute', $order_item_attr, true, 'update_string', $where);
			    }
			}

			// payment
			$payment['payment_method'] = isset($orders->Info->Payment) ? $orders->Info->Payment : null;
			$payment['payment_status'] = $order_status;
			$payment['id_currency'] = $id_currency;
			$payment['Amount'] = (float) $item->Prices->TotalTaxIncl;
			
			$where = array(
			    'id_orders'=> array('operation' => '=', 'value' => $id_order),
			    'id_order_items'=> array('operation' => '=', 'value' => $id_order_item),
			);
			
			$sql .= $this->saveOrder('order_payment', $payment, true, 'update_string', $where);
		    }
		}
		
		//Gift Message
		if(isset($orders->Info->Gift['value']) && $orders->Info->Gift['value'] == "Yes")
		    $gift_message = (string) $orders->Info->Gift->Message;

		// Buyer
		if (isset($orders->Customer)) {		    
		    $buyer = array();
		    $buyer['site'] = $site;
		    $buyer['id_buyer_ref'] = $orders->Customer['id'];
		    $buyer['email'] = (string) $orders->Customer->Email;
		    $buyer['name'] = (string) $orders->Customer->Firstname . ' ' . (string) $orders->Customer->Lastname;
		    $buyer['address_name'] = (string) $orders->Delivery->Firstname . ' ' . (string) $orders->Delivery->Lastname . ' ' . (string) $orders->Delivery->Company ;
		    $buyer['address1'] = (string) $orders->Delivery->Alias . ' ' . (string) $orders->Delivery->Address1;
		    $buyer['address2'] = (string) $orders->Delivery->Address2;
		    $buyer['city'] = (string) $orders->Delivery->City;
		    $buyer['state_region'] = isset($orders->Delivery['state']) && !empty($orders->Delivery['state']) ? 
					    (string) $orders->Delivery['state'] : strtoupper((string) $orders['LanguageCode'] );
		    $buyer['postal_code'] = (string) $orders->Delivery->Postcode;
		    $buyer['country_code'] = (string) $orders->Delivery['country'];
		    $buyer['phone'] = (string) $orders->Delivery->Phone;
		    
		    $where = array(
			'id_orders'=> array('operation' => '=', 'value' => $id_order), 
			'site'=> array('operation' => '=', 'value' => $site)
		    );
		    $sql .= $this->saveOrder('order_buyer', $buyer, true, 'update_string', $where);
		}

		$where = array(
			'id_orders'=> array('operation' => '=', 'value' => $id_order), 
		);
		
		// Shipping        
		$shipping = array();		
		$shipping['shipment_service'] = trim((string) $orders->Carrier->Name);
		$shipping['shipping_services_level'] = 'Standard'; //trim((string) $$orders->Carrier->ShipmentServiceLevelCategory);
		$shipping['id_carrier'] = (int)  $orders->Carrier['id'];
		
		$sql .= $this->saveOrder('order_shipping', $shipping, true, 'update_string', $where);
		
		// Invoice
		$invoice['seller_order_id'] = $seller_order_id;
		$invoice['invoice_no'] = $invoice_number;
		$invoice['total_discount_tax_incl'] = $total_discount;
		$invoice['total_paid_tax_incl'] = $total_paid;
		$invoice['total_products'] = $total_quantity;
		$invoice['total_shipping_tax_incl'] = $total_shipping;
		$invoice['date_add'] = $today_date;
		
		$sql .= $this->saveOrder('order_invoice', $invoice, true, 'update_string', $where);
		
		// Taxes		
		$taxes['tax_amount'] = $total_tax_rate;
		$taxes['tax_on_shipping_amount'] = $total_shipping_tax;

		$sql .= $this->saveOrder('order_taxes', $taxes, true, 'update_string', $where);
		
		// Update Order
		$uorder['total_shipping'] = $total_shipping;
		$uorder['total_discount'] = $total_discount;
		$uorder['total_amount'] = $total_amount;
		$uorder['gift_message'] = $gift_message;
		$uorder['gift_amount'] = $gift_amount;
		$uorder['comment'] = $message;
		$uorder['status'] = 1; // sent

		$where_order = array(
		    'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
		    'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
		    'id_marketplace' => array('operation' => '=', 'value' =>  $id_marketplace ),
		    'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
		);
		
		$sql .= $this->saveOrder('orders', $uorder, true, 'update_string', $where_order);
				
		//saveOrder($table, $data, $return_sql=false, $type='insert_string', $where=array(), $only_exec=false, $no_prefix=false) 
		if (!$this->saveOrder('multi', $sql, false, '', array(), true)) {
		    
		    array('pass'=>false,'output'=>sprintf('Can not update order attribute : ', print_r($sql, true))) ;
		    
		    // Update Order Status
		    $eorder = array(
			'status' => 'error',
			'comment' => 'Unable to save order items'
		    );
		    
		    $where = array(
			'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
			'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
			'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
		    );
		    
		    $this->saveOrder('orders', $eorder, false, 'update_string', $where);		 
		    
		}
		
		return array('pass'=>true, 'id_order' => $id_order, 'id_shop' => $id_shop, 'shop_name' => $shop_name, 'site' => $site);
	    
	}
	
	public function saveOrder($table, $data, $return_sql=false, $type='insert_string', $where=array(), $only_exec=false, $no_prefix=false) {
	    
	    $db = ObjectModelOrder::$db;
	  
	    $transaction=true;
	    $multi_query=false;
	    
	    if($type!='insert_string') 
		$multi_query = true;
		    
	    $direct=false;
	    $commit=true;
	    
	    if($only_exec){
		if(!$this->debug) {
		    $multi_query = true;
		    return $db->db_exec($data, $transaction, $multi_query, $direct, $commit);
		}
		
		return $data;
	    }
	    
	    $id = false;
	    $sql = $db->$type($table, $data, !empty($where)? $where : $no_prefix);
	   
	    if($this->debug) {
		
		printf("$table");
		var_dump($sql);	
		
		if(!$return_sql) {
		    return rand(1, 100);
		} else {
		    return $sql;
		}
		
	    } else {        
		//  $transaction = true, $multi_query=true, $direct=false, $commit=true
		if(!$return_sql) {                     
		    if ($db->db_exec($sql, $transaction, $multi_query, $direct, $commit)) 
		    {
			if( $type == 'insert_string' ) 
			{
			    $id = $db->db_last_insert_rowid();
                            
			} else {
			    $id = 1;
			}
		    } else {
			if($this->debug) {
			    var_dump($db->_error());	
			}
		    }
		} else {
		    return $sql;
		}
	    }
	  
	    return $id;
	}
	
        public function exportOrders( $sales_channal = null, $id_shop = null, $site = null, $id_lang = null ) {
            
                $orders_list = array();
                $where = array();
                $_db = ObjectModelOrder::$db;
                $_db->select(array('id_orders'));
                $_db->from('orders');
                if ( $id_lang  )
                        $where['id_lang'] = (int)$id_lang;
                
                if ( $id_shop  )
                        $where['id_shop'] = (int)$id_shop;
                
                if ( $site  )
                        $where['site'] = $site;
                
                if ( $sales_channal  )
                        $where['sales_channel'] = $sales_channal;
                
                if ( !empty($where) ) 
                        $_db->where( $where );
                
                $_db->orderBy('id_orders');

                $result = $_db->db_array_query($_db->query);
		
                foreach ($result as $order_data) {
		    
			$order = $this->getOrdersListById( $this->_user , (int)$order_data['id_orders'] ) ; 
                        $orders_list[] = get_object_vars($order);
                }

                return $orders_list;
        }
        
	public function getOrdersListById( $user , $id_order ) {
	    
                if ( !isset( $user ) || empty( $user ) || !isset( $id_order ) || empty( $id_order ) )
                        return false;
                
		$Channel_name_exception = array('2' => 'Amazon');
                //check stock movement
                $Stock = new StockMovement($user);
                $inStock = $Stock->checkOrderInStockMvt($id_order);
                        
                // aleardy decrease stock
                if($inStock){
                    $ForceImport = 1;
		    
                // not decrease stock
                } else {
                    $ForceImport = 0;
                }
		
                $orders = array();
                $order_list = new Orders( $user, $id_order );
		
		if(isset($order_list) && !empty($order_list)){
		    		    
		    $sales_channel = $order_list->sales_channel;
		    
		    // Sale Channel Rule
		    if(isset($order_list->id_marketplace) && isset($order_list->site))
		    {
			// 1 .get Marketplace name by id marketplace, if not Mirakl can not get marketplace.
                        if($order_list->id_marketplace != Marketplaces::ID_GENERAL){
                            $sales_channel = ucfirst(strtolower(ObjectModelOrder::get_marketplace_by_id($order_list->id_marketplace)));
                        }
                        
			// 2. get Region by id_site
			$sales_channel .= ' - ' . strtoupper(ObjectModelOrder::get_region_by_id_site($order_list->id_marketplace, $order_list->site));

			// 3. if not Amazon And Mirakl added Payment
			if(!isset($Channel_name_exception[$order_list->id_marketplace]) && $order_list->id_marketplace != Marketplaces::ID_GENERAL){
			    $sales_channel .= ' - ' . ucfirst(strtolower($order_list->payment_method));
			}
		    }

		    $orders['Order:SalesChannel'] = $sales_channel;
		    
		    $orders['Order:SalesChannelID'] = $order_list->id_marketplace;
		    
		    // Multichannel
		    $mp_order = $this->getMultichannelOrderById((int) $order_list->id_shop, $id_order);
                    $orders['Order:Multichannel'] = '';
		    if(isset($mp_order['mp_channel'])){
			$orders['Order:Multichannel'] = $mp_order['mp_channel'];
                        if(isset($mp_order['is_prime']) && $mp_order['is_prime'])
                            $orders['Order:isPrime'] = $mp_order['is_prime'];
		    }
		    
		    $orders['Order:ShopID'] = (int) $order_list->id_shop;
		    $orders['Order:Language'] = $order_list->language;	
		    $orders['Order:LanguageID'] = (int) $order_list->id_lang;	
		    $orders['Order:Currency'] = $order_list->currency;	
		    
		    $orders['Order']['Status'] = $order_list->order_status;	
		    $orders['Order']['Type'] = $order_list->order_type;	
		    
		    // References
		    $orders['Order']['References']['Id'] = (int) $order_list->id_orders;
		    $orders['Order']['References']['ChannelId'] = (int) $order_list->id_marketplace;
		    $orders['Order']['References']['MPReference'] = $order_list->id_marketplace_order_ref;
		    $orders['Order']['References']['MPNumber'] = $order_list->marketplace_order_number;	
		    
		    //Total
		    $orders['Order']['Total']['Amount'] = (float) $order_list->total_amount;	
		    $orders['Order']['Total']['Shipping'] = (float) $order_list->total_shipping;	
		    $orders['Order']['Total']['Discount'] = (float) $order_list->total_discount;	
		    $orders['Order']['Total']['Paid'] = (float) $order_list->total_paid;	
		    
		    // Date
		    $orders['Order']['Date']['OrderDate'] = $order_list->order_date;
		    $orders['Order']['Date']['ShippingDate'] = $order_list->shipping_date;
		    $orders['Order']['Date']['LatestShipDate'] = $order_list->latest_shipping_date;
		    $orders['Order']['Date']['DeliveryDate'] = $order_list->delivery_date;
		    $orders['Order']['Date']['LatestDeliveryDate'] = $order_list->latest_delivery_date;
		    $orders['Order']['Date']['PurchaseDate'] = $order_list->purchase_date;
		    // Gift
		    $orders['Order']['Gift']['GiftMessage'] = $order_list->gift_message;
		    $orders['Order']['Gift']['GiftAmount'] = $order_list->gift_amount;
		    
		    // Buyer
		    $orders['Order']['Buyer:ID'] = $order_list->buyer['id_buyer'];
		    $orders['Order']['Buyer:ReferenceID'] = $order_list->buyer['id_buyer_ref'];
		    $orders['Order']['Buyer']['Email'] = $order_list->buyer['email'];
		    $orders['Order']['Buyer']['FirstName'] = $order_list->buyer['firstname'];
		    $orders['Order']['Buyer']['Lastname'] = $order_list->buyer['lastname'];		    

		    // shipping
		    $orders['Order']['Shipping:ID'] = $order_list->buyer['id_buyer'];
		    $orders['Order']['Shipping:ReferenceID'] = $order_list->buyer['id_address_ref'];
		    $orders['Order']['Shipping']["Name"]['Firstname'] = $order_list->buyer['address_firstname'];
		    $orders['Order']['Shipping']["Name"]['Lastname'] = $order_list->buyer['address_lastname'];
		    $orders['Order']['Shipping']["Name"]['Company'] = $order_list->buyer['company'];
		    $orders['Order']['Shipping']['Address']['Address1'] = $order_list->buyer['address1'];
		    $orders['Order']['Shipping']['Address']['Address2'] = $order_list->buyer['address2'];
		    $orders['Order']['Shipping']['Address']['StateRegion'] = $order_list->buyer['state_region'];
		    $orders['Order']['Shipping']['Address']['City'] = $order_list->buyer['city'];
		    $orders['Order']['Shipping']['Address']['District'] = $order_list->buyer['district'];
		    $orders['Order']['Shipping']['Address']['CountryCode'] = $order_list->buyer['country_code'];
		    $orders['Order']['Shipping']['Address']['CountryName'] = $order_list->buyer['country_name'];
		    $orders['Order']['Shipping']['Address']['PostalCode'] = $order_list->buyer['postal_code'];
		    $orders['Order']['Shipping']['Address']['Phone'] = $order_list->buyer['phone'];
		    
		    // carrier
		    $orders['Order']['Carrier:ID'] = $order_list->carrier['id_carrier'];
		    $orders['Order']['Carrier']['ShipmentService'] = $order_list->carrier['shipment_service'];
		    $orders['Order']['Carrier']['Weight'] = (float) $order_list->carrier['weight'];
		    $orders['Order']['Carrier']['TrackingNumber'] = $order_list->carrier['tracking_number'];
		    $orders['Order']['Carrier']['ShippingServicesCost'] = $order_list->carrier['shipping_services_cost'];
                    $orders['Order']['Carrier']['ShippingServicesLevel'] = $order_list->carrier['shipping_services_level'];
		    
		    // Payment
		    $orders['Order']['Payment']['PaymentMethod'] = isset($order_list->payment['payment_method']) ? $order_list->payment['payment_method'] : '';
		    $orders['Order']['Payment']['PaymentStatus'] = isset($order_list->payment['payment_status']) ? $order_list->payment['payment_status'] : '';
		    $orders['Order']['Payment']['CurrencyID'] = (int) $order_list->payment['id_currency'];
		    $orders['Order']['Payment']['Amount'] = (float) $order_list->payment['Amount'];
		    $orders['Order']['Payment']['ExternalTransactionID'] = isset($order_list->payment['external_transaction_id']) ? $order_list->payment['external_transaction_id'] : '';

		    // Invoices
		    $orders['Order']['Invoices:ID'] = $order_list->invoices['id_invoice'];
		    $orders['Order']['Invoices:InvoiceNo'] = $order_list->invoices['invoice_no'];
		    $orders['Order']['Invoices:SellerOrderId'] = $order_list->invoices['seller_order_id'];
		    $orders['Order']['Invoices']['Total']['Discount']['TaxExcl'] = (float) $order_list->invoices['total_discount_tax_excl'];
		    $orders['Order']['Invoices']['Total']['Discount']['TaxIncl'] = (float) $order_list->invoices['total_discount_tax_incl'];
		    $orders['Order']['Invoices']['Total']['Paid']['TaxExcl'] = (float) $order_list->invoices['total_paid_tax_excl'];
		    $orders['Order']['Invoices']['Total']['Paid']['TaxIncl'] = (float) $order_list->invoices['total_paid_tax_incl'];
		    $orders['Order']['Invoices']['Total']['Shipping']['TaxExcl'] = (float) $order_list->invoices['total_shipping_tax_excl'];
		    $orders['Order']['Invoices']['Total']['Shipping']['TaxIncl'] = (float) $order_list->invoices['total_shipping_tax_incl'];
		    $orders['Order']['Invoices']['Total']['Products'] = (float) $order_list->invoices['total_products'];
		    $orders['Order']['Invoices']['note'] = $order_list->invoices['note'];		    
		    
		    // Items		    
		    if(isset($order_list->item)) {
			foreach($order_list->item as $keys => $order) {
			    
			    $keys = $order['id_order_items'];
			    
			    //$orders['Order']['Items'][$keys]['ItemID'] = $order['id_order_items'];	
			    $orders['Order']['Items'][$keys]['MarketplaceItemID'] = $order['id_marketplace_order_item_ref'];	
			    $orders['Order']['Items'][$keys]['MarketplaceProductID'] = $order['id_marketplace_product'];	
			    $orders['Order']['Items'][$keys]['Product']['Reference'] = $order['reference'];	
			    $orders['Order']['Items'][$keys]['Product']['ID'] = (int) $order['id_product'];	
			    $orders['Order']['Items'][$keys]['Product']['AttributeID'] = (int) $order['id_product_attribute'];	
			    $orders['Order']['Items'][$keys]['Product']['Name'] = $order['product_name'];	
			    $orders['Order']['Items'][$keys]['Product']['Weight'] = (float) $order['product_weight'];
			    $orders['Order']['Items'][$keys]['Quantity']['Ordered'] = (int) $order['quantity'];	
			    $orders['Order']['Items'][$keys]['Quantity']['InStock'] = (int) $order['quantity_in_stock'];	
			    $orders['Order']['Items'][$keys]['Quantity']['Refunded'] = (int) $order['quantity_refunded'];	
			    $orders['Order']['Items'][$keys]['Price']['Product'] = (float) $order['product_price'];
			    $orders['Order']['Items'][$keys]['Price']['Shipping'] = (float) $order['shipping_price'];
			    $orders['Order']['Items'][$keys]['Price']['TaxRate'] = (float) $order['tax_rate'];
			    $orders['Order']['Items'][$keys]['Price']['PerUnit']['TaxIncl'] = (float) $order['unit_price_tax_incl'];
			    $orders['Order']['Items'][$keys]['Price']['PerUnit']['TaxExcl'] = (float) $order['unit_price_tax_excl'];
			    $orders['Order']['Items'][$keys]['Total']['Price']['TaxIncl'] = (float) $order['total_price_tax_incl'];
			    $orders['Order']['Items'][$keys]['Total']['Price']['TaxExcl'] = (float) $order['total_price_tax_excl'];
			    $orders['Order']['Items'][$keys]['Total']['Shipping']['TaxIncl'] = (float) $order['total_shipping_price_tax_incl'];
			    $orders['Order']['Items'][$keys]['Total']['Shipping']['TaxExcl'] = (float) $order['total_shipping_price_tax_excl'];
			    //$orders['Order']['Items'][$keys]['id_condition'] = $order['id_condition'];
			    $orders['Order']['Items'][$keys]['promotion'] = $order['promotion'];
			    $orders['Order']['Items'][$keys]['message'] = $order['message'];
			    //$orders['Order']['Items'][$keys]['Status'] = $order['id_status'];
			    //$orders['Order']['Items'][$keys]['id_tax'] = $order['id_tax'];
			    //$orders['Order']['Items'][$keys]['attributes'] = $order['attributes'];
			}
		    }
		    
		    /*foreach($order_list as $keys => $order) {
			
			if($keys == 'item') {
			    
			    foreach ($order as $key => $order_items){
				unset($order_items['id_orders']);
				foreach ($order_items as $key_item => $order_item){
				
				    $key_item = str_replace(" ", "", ucwords(str_replace("_", " ", $key_item)));
				    $orders['Order']['Items'][($key+1)][$key_item] = $order_item;
				    
				}
			    }
			    
			} else if(is_array($order)){
			    
			    foreach ($order as $key => $order_detail){
				
				    $key = str_replace(" ", "", ucwords(str_replace("_", " ", $key)));
				    $orders['Order'][ucfirst($keys)][$key] = $order_detail;

			    }
			}
		    }*/
		    
		    // Info
		    $orders['Order']['Info']['AffiliateID'] = $order_list->affiliate_id;
		    $orders['Order']['Info']['Commission'] = $order_list->commission;
		    $orders['Order']['Info']['IP'] = $order_list->ip;
		    $orders['Order']['Info']['Comment'] = $order_list->comment;
		    $orders['Order']['Info']['Status'] = $order_list->status;
		    $orders['Order']['Info']['Date'] = $order_list->date_add;
		}
		
                $orders['Order']['Info']['ForceImport'] = $ForceImport;
                
                if($ForceImport == 1 && isset($inStock)) {
                    $orders['Order']['Info']['StockMovementDate'] = $inStock;
                }		

                return ($orders);
        }
	
        public function getOrders($sales_channal, $id_shop, $site, $id_lang) {
            
                $orders = array();
                $_db = ObjectModelOrder::$db;

                $sql = "SELECT "
                        . "o.id_marketplace_order_ref as id_marketplace_order_ref, "
                        . "o.id_marketplace as id_marketplace, "
                        . "o.site as site, "
                        . "o.payment_method as payment_method, "
                        . "o.total_paid as total_paid, "
                        . "o.order_date as order_date, "
                        . "ob.id_buyer as id_buyer, "
                        . "ob.email as email "
                        . "FROM {$_db->prefix_table}orders o "
                        . "LEFT JOIN {$_db->prefix_table}order_buyer ob ON (m.id_orders = n.id_orders) "
                        . "WHERE o.sales_channal = " . $sales_channal . " "
                        . "AND o.id_shop = " . $id_shop . " "
                        . "AND o.site = " . $site . " "
                        . "AND o.id_lang = " . $id_lang . " "
                        . self::$exceptions_shop_order ;

                $result = $_db->db_query_str($sql);

                foreach ($result as $order) {
                        $orders['id_marketplace'] = $order['id_marketplace'];
                        $orders['id_marketplace_order_ref'] = $order['id_marketplace_order_ref'];
                        $orders['site'] = $order['site'];
                        $orders['payment_method'] = $order['payment_method'];
                        $orders['total_paid'] = $order['total_paid'];
                        $orders['order_date'] = $order['order_date'];
                        $orders['id_buyer'] = $order['id_buyer'];
                        $orders['email'] = $order['email'];
                }

                return $orders;
        }
        
        public function getOrderBySellerOrderId($seller_order_id, $order_id=null) {

                if(isset($order_id) && !empty($order_id))
                      $id_orders = ' AND id_orders = '. $order_id;

                $orders = array();
                $_db = ObjectModelOrder::$db;
                $sql = "SELECT * FROM {$_db->prefix_table}order_invoice WHERE seller_order_id = '" . $seller_order_id . "' $id_orders; ";
                $result = $_db->db_query_str($sql);
                foreach ($result as $order) {
			$orders['id_orders'] = $order['id_orders'];
			$orders['seller_order_id'] = $order['seller_order_id'];
			$orders['invoice_no'] = $order['invoice_no'];
                }

                return $orders;
        }
        
        public function getOrderByMPID($id_marketplace_order_ref) {
            
                $orders = array();
                $_db = ObjectModelOrder::$db;

                $sql = "SELECT * FROM {$_db->prefix_table}orders WHERE id_marketplace_order_ref = '" . $id_marketplace_order_ref . "'; ";

                $result = $_db->db_query_str($sql);

                foreach ($result as $order) {
                        $orders['id_order'] = $order['id_orders'];
                        $orders['id_marketplace'] = $order['id_marketplace'];
                        $orders['id_marketplace_order_ref'] = $order['id_marketplace_order_ref'];
                        $orders['site'] = $order['site'];
                        $orders['id_shop'] = $order['id_shop'];
                        $orders['order_date'] = $order['order_date'];
                        $orders['sales_channel'] = $order['sales_channel'];
                        $orders['status'] = $order['status'];
                }

                return $orders;
        }
        
        public function getOrderByID($id_orders) {
            
                $orders = array();
                $_db = ObjectModelOrder::$db;

                $sql = "SELECT * FROM {$_db->prefix_table}orders WHERE id_orders = " . (int)$id_orders . "; ";

                $result = $_db->db_query_str($sql);

                foreach ($result as $order) {
                        $orders['id_marketplace'] = $order['id_marketplace'];
                        $orders['id_marketplace_order_ref'] = $order['id_marketplace_order_ref'];
                        $orders['site'] = $order['site'];
                        $orders['id_shop'] = $order['id_shop'];
                        $orders['order_date'] = $order['order_date'];
                        $orders['sales_channel'] = $order['sales_channel'];
                        $orders['status'] = $order['status'];
                }

                return $orders;
        }
        
        public function getOrdersToSend($id_shop, $id_marketplace, $site, $sub_marketplace = null) {
            
                $yesturday = date('Y-m-d H:i:s', strtotime("yesterday"));
//                $yesterday = date('Y-m-d H:i:s', strtotime("-3 days"));  
                $today = date('Y-m-d H:i:s');
                
                $_db = ObjectModelOrder::$db;
                $where = '';
                $condition_sub_marketplace = '';
                $exceptions = '';
                
                if ( !empty($id_marketplace) && $id_marketplace == 3 ) {
                        $where = " AND o.order_status IN('Completed', 'Shipped') "
				. "AND (o.purchase_date IS NOT NULL "
				. "AND date(o.purchase_date) NOT IN ('', '0000-00-00 00:00:00')) ";
                }
                
                // conditions in sub_marketplace edit::07/03/2016
                if(isset($sub_marketplace) && !empty($sub_marketplace)){
                    $condition_sub_marketplace = " LEFT JOIN {$_db->prefix_table}order_sub_marketplace SM ON o.id_orders = SM.id_orders ";
                    $where = "AND SM.sub_marketplace = $sub_marketplace ";
                }else{
                    $exceptions = self::$exceptions_shop_order;
                }
                
                $sql = "SELECT o.id_orders, o.site, o.id_marketplace_order_ref, o.id_marketplace FROM {$_db->prefix_table}orders o $condition_sub_marketplace
                        WHERE o.id_shop = ".$id_shop." AND o.id_marketplace = ".$id_marketplace." AND o.site = ".$site." 
                        AND (o.status IS NULL OR o.status != 1) AND (o.date_add >= '$yesturday' AND o.date_add <= '$today') $where $exceptions ; ";

                $result = $_db->db_query_str($sql);
                
                return $result;
        }
        
        public function getOrdersNotInList($id_shop, $list_orders) {
            
                $_db = ObjectModelOrder::$db;
                
                if(!empty($list_orders)){
                    $sql = "SELECT o.id_orders, o.site, o.id_marketplace, o.order_status FROM {$_db->prefix_table}orders o "
			    . "WHERE o.id_shop = ".$id_shop." "
			    . "AND o.id_orders NOT IN ( $list_orders ) "
			    . "AND o.order_date > '".date('Y-m-d H:i:s', strtotime('now - 15 days'))."' "
			    . "AND o.order_status NOT IN ('Active', 'Cancelled', 'Shipped') ".self::$exceptions_shop_order."; ";
                } else {
                    $sql = "SELECT o.id_orders, o.site, o.id_marketplace, o.order_status "
			    . "FROM {$_db->prefix_table}orders o "
			    . "WHERE o.id_shop = ".$id_shop." "
			    . "AND o.order_date > '".date('Y-m-d H:i:s', strtotime('now - 15 days'))."' "
			    . "AND o.order_status NOT IN ('Active', 'Cancelled', 'Shipped') ".self::$exceptions_shop_order."; ";
                }
		
		if($this->debug) {
		    echo '<pre>'.__FUNCTION__ .' (#' . __LINE__ . ')</pre>'; 
		    echo '<pre>' . $sql . '</pre>'; 
		}

                $result = $_db->db_query_str($sql);
                return $result;
        }
        
        public function updateOrderStatus($id_order , $status, $id_invoice, $order_number=null){
        
            $_db = ObjectModelOrder::$db;
            $description = array();
            $description['status'] = $status;
            $description['comment'] = '';

            $where = array(
                'id_orders'=>  array(
                    'value' => $id_order,
                    'operation' => '=',
                )
            );
	    
	    $invoice = array();
	    $invoice['invoice_no'] = $id_invoice;
	    
	    if(isset($order_number) && !empty($order_number))
		$invoice['seller_order_id'] = $order_number;
		
            //update
            $sql = $_db->update_string('orders', $description, $where);
            $sql .= $_db->update_string('order_invoice', $invoice, $where);

            if(! $_db->db_exec($sql))
                return false;

            return true;
        }
        
        public function updateOrderErrorStatus($id_order , $status, $comment){
        
            $_db = ObjectModelOrder::$db;
            $description = array();
            $description['status'] = $status;
            $description['comment'] = $comment;

            $where = array(
                'id_orders'=>  array(
                    'value' => $id_order,
                    'operation' => '=',
                )
            );

            //update
            $sql = $_db->update_string('orders', $description, $where);

            if(! $_db->db_exec($sql))
                return false;

            return true;
        }
	
	public function getOrderErrorStatus($id_shop){
	    
	    $date = date('Y-m-d H:i:s', strtotime('-15 days'));
	    
            $_db = ObjectModelOrder::$db;
	    $sql = "SELECT o.id_orders, o.order_status, o.site, o.id_marketplace_order_ref, o.id_marketplace, o.comment, o.order_date "
                . "FROM {$_db->prefix_table}orders o "
		    . "WHERE "
                    . "( "
                        . "o.id_shop = ".$id_shop." "
                        . "AND o.status = 0 "
                        . "AND (o.date_add >= '$date') "
                        . "AND ( flag_error_email = 0 OR flag_error_email IS NULL ) "
                        . "AND ( o.comment !='' AND o.comment is not null ) "
                    . ") AND ( "
                        . " CASE "
                            . "WHEN o.comment LIKE '%waiting order items%' THEN o.date_add <= '".date('Y-m-d H:i:s', strtotime('-5 hour'))."' "
                            . "WHEN o.comment LIKE '%Products do not match%' THEN o.date_add <= '".date('Y-m-d H:i:s', strtotime('-5 hour'))."' "
                            . "WHEN o.comment LIKE '%Product items mismatch%' THEN o.date_add <= '".date('Y-m-d H:i:s', strtotime('-5 hour'))."' "
                            . "ELSE (o.date_add >= '$date') "
                        . "END "
                    . ") ; ";
	    
	    $result = $_db->db_query_str($sql);
	   
            return $result;
        }
	
	public function updateFlagCancelledStatus($id_order, $flagged=1){
        
            $_db = ObjectModelOrder::$db;
            $description = array();
            $description['flag_canceled_status'] = $flagged;

            $where = array(
                'id_orders'=>  array(
                    'value' => $id_order,
                    'operation' => '=',
                )
            );

            //update
            $sql = $_db->update_string('orders', $description, $where);
	    
            if(! $_db->db_exec($sql))
                return false;

            return true;
        }
	
	public function updateFlagErrorEmail($id_order , $flagged=1){
        
            $_db = ObjectModelOrder::$db;
            $description = array();
            $description['flag_error_email'] = $flagged;

            $where = array(
                'id_orders'=>  array(
                    'value' => $id_order,
                    'operation' => '=',
                )
            );

            //update
            $sql = $_db->update_string('orders', $description, $where);

            if(! $_db->db_exec($sql))
                return false;

            return true;
        }
	
        /* stockMovement : updates stock on Merchant site and keeps order log. */
        public function stockMovement($stockmovement_url, $fbtoken, $id_shop, $batch_id, $id_marketplace, $site, $imported_order, $marketplace = null, $is_addStock=false, $action_type=null) {
            $return_list = array();
            if ($imported_order && $stockmovement_url) {

                $_db = ObjectModelOrder::$db;
                
                foreach ($imported_order as $order_id => $order) {

                    if(isset($order['action_type']) && $order['action_type']){
                        $action_type = $order['action_type'] ;
                    }

                    if(isset($order['site']) && !empty($order['site'])){
                        $site = $order['site'];
                    } 

                    foreach ($order ['items'] as $item) {
                        
                        $message = $error_code = $marketplace = $error_code = '';
                        
                        if(!isset($item['id_product'])){
                            //Log
                            $message = 'This order has no items. ' ;
                        } else {
                            
                            if($item['id_product'] == 0){
                                continue;
                            }
                            
                            // check stock movement
                            $inStock = $this->stock->checkOrderInStockMvt($order_id, $item['id_product'], $item['id_product_attribute']);

			    if($this->debug) {
				echo $order_id . ' has set stock movement : - ' . ($inStock ? "$inStock" : "false");
			    }
			    
                            // aleardy decrease stock
                            if($inStock){
				
                                $message = 'This order has set stock movement on : ' . $inStock;

                                if(isset($action_type)) {
                                    continue;
                                }
                            // not decrease stock
                            } else {

                                // Determine Software type and prepare URL
                                $conj = strpos($stockmovement_url, '?') !== FALSE ? '&' : '?';
                                $preparedURL = $stockmovement_url . $conj . sprintf('fbtoken=%s&fbproduct=%s&fbcombination=%s&fbsold=%s', 
                                                $fbtoken, intval($item['id_product']), intval($item['id_product_attribute']), intval($item['quantity']));
                                
				if(!$this->debug) {
				    
				    if(function_exists('fbiz_get_contents')){
                                        $return = fbiz_get_contents($preparedURL, array('curl_method'=>1));
                                    }else{
                                        $return = @file_get_contents($preparedURL);
                                    }
				    if(!$return || empty($return)) {
                                        if(isset($action_type) && isset($item['reference'])){
                                            $return_list['fail'][$action_type][$order_id][$item['reference']] = $item;
                                        }
					$message = 'Update stock movement fail, can not connect to Shop.';

				    } else {

					$return_dom = new SimpleXMLElement($return);

					// add log stock movement
					if($return_dom->Status->Code == 1 && !$is_addStock) {
					    $data_offer = $item;

					    if(isset($order['source']) && !empty($order['source'])){
						$marketplace = $order['source'];
					    } else if(isset($marketplace) && !empty($marketplace)) {
						$marketplace = $marketplace;
					    }
					    
					    // log to stock movement
					    $this->stock->stockMovementProcess($data_offer, $id_shop, $item['time'], StockMovement::import_order, $marketplace, $order_id);

                                            $error_code = $return_dom->Status->Code;
                                            $message = 'StockMovement : ' . $return_dom->Status->Message;

					} else {
                                            
                                            $error_code = $return_dom->Status->Code;
                                            $message = 'Update stock movement fail, ' . $return_dom->Status->Message;
                                        }
				    }
				    
				} else {
				    echo '<pre>' . __FUNCTION__ .'#('.__LINE__.') : ' . ($preparedURL) . '</pre>' ; 
				}				
                            }
                        }

                        // keep log
                        $data_insert = array(
                            'id_shop' => (int)$id_shop,
                            'id_marketplace' => (int)$id_marketplace,
                            'site' => $site,
                            'batch_id' => $batch_id,
                            'error_code' => $error_code,
                            'order_id' => $order_id,
                            'id_product' => (int)$item['id_product'],
                            'id_product_attribute' => (int)$item['id_product_attribute'],
                            'message' => $message,
                            'quantity' => (int)$item['quantity'],
                            'action_type' => isset($action_type) ? $action_type : StockMovement::import_order,
                            'date_add' => date('Y-m-d H:i:s')
                        );

                        $sql = $_db->insert_string('offers_stock_mvt_logs', $data_insert, true);

                        if(!$this->debug) {
                            $_db->db_exec($sql);
                        } else {
                           echo '<pre>' . __FUNCTION__ .'#('.__LINE__.') : ' . ($sql) . '</pre>' ;
                        }
                    }
                }
            }
            
            return (isset($return_list) && !empty($return_list)) ? $return_list : true;
        }
	
	public function order_log($id_shop, $id_marketplace, $site, $batch_id, $order_id, $error_code, $message, $time=null){
	    
	    $_db = ObjectModelOrder::$db;
	    
	    // keep log
	    $data_insert = array(
		'id_shop' => $id_shop,
		'id_marketplace' => $id_marketplace,
		'site' => $site,
		'batch_id' => $batch_id,
		'request_id' => $order_id,
		'error_code' => $error_code,
		'message' => $message,
		'date_add' => isset($time) ? $time : date('Y-m-d H:i:s')
	    );
	    
	    $sql = $_db->insert_string('log', $data_insert);

	    if(!$this->debug) {

		if (!$_db->db_exec($sql)) {
		    return false;
		}

	    } else {

	       echo '<pre>' . __FUNCTION__ .'#('.__LINE__.') : ' . ($sql) . '</pre>' ;  

	    }
	    
	    return true ;
	}
	
	public function send_orders($url, $id_order) {
            
            $error = false;
            $output = '';	    
            $timeout = 180;
	    $ctx = stream_context_create(array(
                'http' => array(
                    'timeout' => $timeout
                    )
                )
            );
            $page='';
            if(function_exists('fbiz_get_contents')){
                $page = fbiz_get_contents($url,$timeout);
            }else{
                $page = file_get_contents($url, 0, $ctx);
            }
            if (!$page = @simplexml_load_string($page, 'SimpleXMLElement', LIBXML_NOCDATA)) {
                return array('pass' => false, 'output' => 'Failed to open stream from module. HTTP request failed! HTTP/1.0 500 Internal Server Error');
            }	 
	    
            $invoice = $order_number = '';
            
            if (isset($page->Status->Code)) {
                if (strval($page->Status->Code) == 1) {
                    $invoice = $page->Status->InvoiceNumber;
		    $order_number = isset($page->Status->OrderNumber) ? strval($page->Status->OrderNumber) : '';
                    $result = $this->updateOrderStatus($id_order, '1', $invoice, $order_number);
                    if ($result) {
                        $output = strval($page->Status->Message);
                    } else {
                        $error = true;
                        $output = 'Update Status Error';
                    }
                    
                } else if (strval($page->Status->Code) == -1) {
                    
                    $error = true;
                    $output_array = array();
		    
                    if(isset($page->Status->Message) && !empty($page->Status->Message)) {
                    	$output_array[] = strval($page->Status->Message);
                    }
                    if(isset($page->Status->Output) && !empty($page->Status->Output)) {
                    	$output_array[] = strval($page->Status->Output);
                    }
                    if (isset($page->Status->Error) && !empty($page->Status->Error)) {
                    	$output_array[] = strval($page->Status->Error);
                    }
		    
                    $this->updateOrderErrorStatus($id_order, '0', implode(', ', $output_array));	
		    
		    if(!empty($output_array))
			$output = implode(', ', $output_array);
                    
                } else {

                    $error = true;
                    $output_array = array();
                    if(isset($page->Status->Message) && !empty($page->Status->Message)) {
                        $output_array[] = strval($page->Status->Message);
                    } 
                    if(isset($page->Status->Output) && !empty($page->Status->Output)) {
                        $output_array[] = strval($page->Status->Output);
                    } 
                    if (isset($page->Status->Error) && !empty($page->Status->Error)) {
                        $output_array[] = strval($page->Status->Error);
                    }
		    
                    $this->updateOrderErrorStatus($id_order, '0', implode(', ', $output_array));
		    
                     if(!empty($output_array))
			$output = implode(', ', $output_array);
                }
            } else {
                $error = true;
                $output =  'Send Error';
            }
	    
            return array('pass' => $error ? false : true, 'output' => $output, 'invoice' => $invoice, 'order_number' => $order_number);
        }
        
        public function getNumberOrderInRange(){
            
                $_db = ObjectModelOrder::$db;

                $gr = array('month'=>30,'week'=>7,'day'=>1);
                $output = array();
                
                foreach($gr as $key=>$dd){
 
                    $sql = "SELECT o.sales_channel as name, o.id_marketplace as id, count(oi.id_orders) as ct from {$_db->prefix_table}orders o
			    inner join {$_db->prefix_table}order_items oi on oi.id_orders = o.id_orders
			    where o.order_date >= DATE_SUB(NOW(), INTERVAL {$dd} DAY) and o.status != 'error' ".self::$exceptions_shop_order." 
			    group by o.sales_channel order by o.order_date asc, count(oi.id_orders) asc ";
 
                    $result = $_db->db_query_str($sql);
                    
                    $output[$key]=array();
                    
                    foreach($result as $keyr=>$r){
                        if($keyr < self::LIMIT_CHART_TOP_NUM){
                            $output['label'][$r['name']] = $r['name'];
                            $output[$key]['data'][$r['name']] = $r['ct'];
                        }else{
                            if(!isset($output[$key]['data']['Other'])) $output[$key]['data']['Other'] = 0;
                            $output['label']['Other'] = 'Other';
                            $output[$key]['data']['Other'] += $r['ct']; 
                        } 
                    }
                    if(isset($output[$key]['data']['Other'])){
                    $output[$key]['data']['Other'] = isset($output[$key]['data']['Other']) ? (string) $output[$key]['data']['Other'] : '';
                    }
                }
                
                if(isset($output['label'] ))
                    foreach($output['label'] as $k =>$v){
                        foreach($gr as $key=>$dd){
                             if(!isset($output[$key]['data'][$k])) {
                                 $output[$key]['data'][$k] = 0;
                             }
                        }
                    }
                
                return $output;
        }
        
        public function getOrdersDataGraph(){
            
            $_db = ObjectModelOrder::$db;
            $ml = array(2 => "Amazon", 3 => "Ebay");
            $rd = array('month'=>30*8,'week'=>7*10,'day'=>1*25);
            $gr = array('month'=>'%Y %m','week'=>'%U','day'=>'%m %d');
            $ord = array('month'=>'%Y %m','week'=>'%Y %m %d','day'=>'%Y %m %d');
            $dpa = array('month'=>'%Y %m','week'=>'%m w%U','day'=>'%m %d');
            
            $output = array();
            foreach($rd as $key => $dd){
                $grp = $gr[$key];
                $dp = $dpa[$key];
                $ord_v = $ord[$key];
 
                $dy = $rd[$key]; 
                    $sql = "SELECT  DATE_FORMAT ( date_add,'{$dp}') as lb ,DATE_FORMAT ( date_add,'%d') as d,DATE_FORMAT ( date_add,'%m') as m ,"
			    . "DATE_FORMAT ( date_add,'%Y') as y,id_marketplace as id , count(id_orders) as ct "
                            . " from {$_db->prefix_table}orders o where order_date >= DATE_SUB(NOW(), INTERVAL {$dy} DAY)   and status != 'error' ".self::$exceptions_shop_order 
                            . " group by id_marketplace,DATE_FORMAT ( date_add,'{$grp}')   "
			    . " order by DATE_FORMAT ( date_add,'{$ord_v}') asc";
 
                    $result = $_db->db_query_str($sql);
                     
                    $output[$key]=array();
                    $idl=array();
                    $i=0;
                    foreach($result as $r){
                        if(!isset($idl[$r['lb']])){
                            $idl[$r['lb']] = $i;$i++;
                        } 
                        $output[$key]['label'][$idl[$r['lb']]] = $r['lb'];
                        if(isset($ml[$r['id']])){
                        $output[$key]['data'][$ml[$r['id']]][$idl[$r['lb']]] = $r['ct'];
                        }
                        
                    } 
                    foreach($ml as $l){
                        if(!isset($output[$key]['data'][$l])){
                            continue;
                        }
                        if(!isset($output[$key]['data'][$l])){
                            $output[$key]['data'][$l] = array();
                        }
                        if(isset($output[$key]['label'] ))
                        foreach($output[$key]['label'] as $k => $v){
                            if(!isset($output[$key]['data'][$l][$k])){
                                $output[$key]['data'][$l][$k] = 0;
                            }
                        }     
                    }
                } 
                return $output;
        }
        
        public function getOrderCanceled(){
            
            $orders = array();            
            $_db = ObjectModelOrder::$db;
            
            $sql = "SELECT o.id_orders, o.id_marketplace, o.id_marketplace_order_ref, o.site, o.id_shop, o.sales_channel, "
		. "CASE WHEN oi.seller_order_id IS NOT NULL THEN oi.seller_order_id END AS seller_order_id "
		. "FROM {$_db->prefix_table}orders o "
		. "LEFT JOIN {$_db->prefix_table}order_invoice oi ON oi.id_orders = o.id_orders "		
		. "WHERE o.order_status = 'Canceled' AND o.status = 1 AND o.date_add > '".date('Y-m-d H:i:s', strtotime('now - 30 days'))."' "
		. "AND (o.flag_canceled_status IS NULL OR o.flag_canceled_status = '' OR o.flag_canceled_status = 0 ) ; ";
            
            $result = $_db->db_query_str($sql);
            foreach ($result as $order) {
                $oid = $order['id_orders'];
                $orders[$oid]['id_orders'] = $order['id_orders'];
                $orders[$oid]['id_marketplace'] = $order['id_marketplace'];
                $orders[$oid]['id_marketplace_order_ref'] = $order['id_marketplace_order_ref'];
                $orders[$oid]['site'] = $order['site'];
                $orders[$oid]['id_shop'] = $order['id_shop'];
                $orders[$oid]['sales_channel'] = $order['sales_channel'];
                $orders[$oid]['seller_order_id'] = $order['seller_order_id']; //2016-01-14                
            }

            return $orders;
        }

        public function updateOrderCanceled($fbtoken, $url, $orders, $batch_id, $by_seller_order_id=false, $debug=false, $allow_send_to_shop=true){
             
            if (!empty($orders) && !empty($url)) {
                
                $_db = ObjectModelOrder::$db;                
                
                // 1. update status
                foreach ($orders as $id_orders) {

                    //$order = $this->getOrderByID($id_orders);

                    //if(isset($order['status']) && $order['status'] == 1){
                        
                        $update = "UPDATE {$_db->prefix_table}orders SET order_status = 'Canceled', date_add = '" . date('Y-m-d H:i:s') . "' "
                                . "WHERE id_orders = " . (int)$id_orders . "; ";
                        if($debug){
                            echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                            echo 'update sql : ' . ($update) . '';
                        } else {
                            $_db->db_exec($update);
                        }

                    /*} else {

                        if($debug){
                            echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                            echo 'Order : ' . $id_orders . ' Not sent to Shop.';
                        } 
                    }*/
                }                

		// only allow send to shop
		if($allow_send_to_shop)
		{
		    // 2. getOrderCanceled                
		    $cancaled_orders = $this->getOrderCanceled();

		    if ($debug) {
			echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
			echo 'getOrderCanceled : <pre>' . print_r($cancaled_orders, true) . '</pre>';
		    }

		    if(!empty($cancaled_orders)) {

			//get cancaled orders and send status
			foreach ($cancaled_orders as $order_detail) {

			    if(empty($order_detail['id_orders'])){
				continue;
			    }                        

			    $output = '';
			    $conj = strpos($url, '?') !== FALSE ? '&' : '?';

			    if($by_seller_order_id) { // when send cancel order by seller order id

				if(isset($order_detail['seller_order_id'])) {
				    $preparedURL = $url . $conj . sprintf('fbtoken=%s&seller_order_id=%s', $fbtoken, intval($order_detail['seller_order_id']));
				} else {
				    $output = 'Missing seller order id.';
				}

			    } else {
				$preparedURL = $url . $conj . sprintf('fbtoken=%s&fborder=%s', $fbtoken, $order_detail['id_marketplace_order_ref']);
			    }

			    // 3. update Canceled order 
			    if ($debug){

				echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
				echo 'preparedURL : <pre>' . print_r($preparedURL, true) . '</pre>';

			    } else {

                                if(function_exists('fbiz_get_contents')){
                                    $return = fbiz_get_contents($preparedURL, array('curl_method'=>1));
                                }else{
                                    $return = @file_get_contents($preparedURL);
                                }
                                
				if (!$page = simplexml_load_string($return, 'SimpleXMLElement', LIBXML_NOCDATA)) {

				    $output = 'Failed to open stream from module. HTTP request failed! HTTP/1.0 500 Internal Server Error';

				} else {

				    if (isset($page->Status->Code)) {
					if (strval($page->Status->Code) == 1) {

					   $output = strval($page->Status->Message);    

					   // update flag canceled status // to avoid duplacate sent order cancel //2016-01-27
					   $this->updateFlagCancelledStatus($order_detail['id_orders'] , 1);

					} else if (strval($page->Status->Code) == -1) {

					    $output = strval($page->Status->Output);
                                            
                                            // The order has already been assigned this status.
                                            $this->updateFlagCancelledStatus($order_detail['id_orders'] , 1);

					} else {

					    if(isset($page->Status->Output)) {
						$output = strval($page->Status->Output);
					    } else if (isset($page->Status->Error)) {
						$output = strval($page->Status->Error);
					    } else {
						$output = 'Save error';
					    } 
					}
				    } else {
					$output =  'Send Error';
				    }
				}

				// 4. log
				$data_insert = array(
				    'id_shop' => $order_detail['id_shop'],
				    'id_marketplace' => $order_detail['id_marketplace'],
				    'site' => $order_detail['site'],
				    'batch_id' => $batch_id,
				    'request_id' => $order_detail['id_orders'],
				    'error_code' => $order_detail['id_marketplace_order_ref'],
				    'message' => $output,
				    'date_add' => date('Y-m-d H:i:s')
				);

				$sql_insert = $_db->insert_string('log', $data_insert);
                                $_db->db_exec($sql_insert);
			    }

			}
		    }
		}
            }
	    
            return true;
        }
	
	public function getMultichannelOrderById($id_shop, $id_order) {
	    	    
	    $_db = ObjectModelOrder::$db;   
	    
	    $sql  = 'SELECT * '  ;  
	    $sql .= 'FROM mp_multichannel mp ';
	    $sql .= ' WHERE mp.id_shop = ' . (int) $id_shop .' '
		  . ' AND mp.id_orders = '. (int) $id_order.' ; '; 

	    $result = $_db->db_query_str($sql . "; ");   
	
	    return isset($result[0]) ? $result[0] : null ;
	}
	
	public function getMultichannelOrder($id_shop, $start = null, $limit = null, $order_by = null, $search = null, $num_rows = false, $marketplaces = array()) {
	    	    
	    $amazon_id = 2;
	    $ebay_id = 3;
	    
	    $_db = ObjectModelOrder::$db;   
	    
	    $orders = array();
            $oi_id_orders = array();
            if(isset($search) && !empty($search)){
                $sql = "select oi.id_orders from ".$_db->prefix_table."order_invoice oi where oi.seller_order_id like '%" . $search . "%' "
                    . " OR oi.total_products like '%" . $search . "%'   group by oi.id_orders";
                $result = $_db->db_query_str($sql);
                foreach($result as $r){
                    $oi_id_orders[$r['id_orders']]=$r['id_orders'];
                }
            }

            $sql  = 'SELECT o.id_orders as "id_orders", o.payment_method as "payment_method", o.total_paid as "total_paid", '
		  . 'o.order_date as "order_date" , mp.id_country as "id_country", mp.mp_channel_status as "mp_channel_status",'
		  . ' o.id_marketplace_order_ref as "id_marketplace_order_ref", o.id_lang as "id_lang", o.site as "site", o.id_marketplace as "id_marketplace", '
                  . 'o.comment as "comment", o.sales_channel as "sales_channel" ' ;
	    $sql .= 'FROM '.$_db->prefix_table.'orders o'		  
		  . ' LEFT JOIN mp_multichannel mp ON mp.id_orders = o.id_orders AND mp.id_shop = o.id_shop  '
		  . ' JOIN '.$_db->prefix_table.'order_items oit ON oit.id_orders = o.id_orders AND oit.id_shop = o.id_shop  '
		  . ' LEFT JOIN amazon_product_option apo ON apo.id_shop = oit.id_shop AND apo.id_product = oit.id_product '
		    . 'AND apo.id_product_attribute = oit.id_product_attribute AND apo.sku = oit.reference AND (apo.id_country = o.id_lang OR apo.id_country = o.site) ' ;
	    $sql .= ' WHERE o.id_shop = ' . (int) $id_shop .' '
		  . ' AND ( CASE 
			WHEN mp.id_orders IS NULL THEN o.order_status NOT IN ("Shipped", "Canceled")
			WHEN mp.id_orders IS NOT NULL THEN mp.mp_channel = "MAFN" 
			END  ) 
                      AND ( CASE
                            WHEN o.id_marketplace = '.$ebay_id.'
                            THEN (o.order_status IN ("Completed") AND (o.purchase_date IS NOT NULL AND date(o.purchase_date) NOT IN ("", "0000-00-00 00:00:00")))
                            ELSE o.order_status IS NOT NULL
                      END ) '
		  . ' AND payment_method NOT LIKE "Amazon%" AND id_marketplace <> '.$amazon_id // exception id from shop
                  . ' AND IF(mp.id_orders IS NULL , apo.fba, o.id_orders) IS NOT NULL ';

	    if(isset($search) && !empty($search)){
		$sql .= "AND ( "
			. "o.id_orders like '%" . $search . "%' OR "
			. "o.payment_method like '%" . $search . "%' OR "
			. "o.total_paid like '%" . $search . "%' OR "
			. "o.order_date like '%" . $search . "%' OR "
			. "o.id_marketplace_order_ref like '%" . $search . "%' OR "
			. "mp.mp_channel_status like '%" . $search . "%'  ";
                if(!empty($oi_id_orders)){ //search from orders_order_invoice
                    $sql .=  " OR o.id_orders in ('".implode("','",$oi_id_orders)."') ";
                }
		$sql .=  ") ";
	    }

	    $sql .= ' GROUP BY o.id_orders ';
	    
	    if(isset($order_by) && !empty($order_by) && !$num_rows){
		$sql .= "ORDER BY $order_by ";
	    }	   
	    
	    if($num_rows) {

		$result = $_db->db_sqlit_query($sql);
		$row = $_db->db_num_rows($result);
		return ($row);

	    } else {
            
		if(isset($start) && isset($limit) && !$num_rows){
		    $sql .= "LIMIT $limit OFFSET $start ";
		}
		$languages = OrderLanguage::getLanguages();
		$countries = Marketplaces::getCountries();
		
		require_once(dirname(__FILE__) . '/../../Amazon/classes/amazon.userdata.php');
				
		$result = $_db->db_query_str($sql . "; ");   
		$id = 0;
                $order_id_list=array();
                foreach ($result as $order) {
                    $order_id_list[$order['id_orders']] = $order['id_orders'];
                }
                $oi_orders_list = array();
                if(!empty($order_id_list)){
                    $sql = "select oi.id_orders,oi.seller_order_id as seller_order_id , oi.total_products as total_products "
                        . " from ".$_db->prefix_table."order_invoice oi "
                        . " where oi.id_orders in ('".implode("','",$order_id_list)."')   group by oi.id_orders";
                    $resultb = $_db->db_query_str($sql);
                    foreach($resultb as $r){
                        $oi_orders_list[$r['id_orders']]=$r ;
                    }
                }
		
		foreach ($result as $order) {
		   
                    $order['seller_order_id'] = isset($oi_orders_list[$order['id_orders']])?$oi_orders_list[$order['id_orders']]['seller_order_id']:'';
                    $order['total_products'] = isset($oi_orders_list[$order['id_orders']])?$oi_orders_list[$order['id_orders']]['total_products']:'';
		    
		    $orders[$id]['id_orders'] = $order['id_orders'];
		    $orders[$id]['seller_order_id'] = $order['seller_order_id'];
		    $orders[$id]['id_marketplace_order_ref'] = $order['id_marketplace_order_ref'];
		    $orders[$id]['sales_channel'] = $order['sales_channel'] ;
		    $orders[$id]['payment_method'] = $order['payment_method'];
		    $orders[$id]['mp_channel'] = isset($marketplaces['Amazon'][$order['id_country']]) ? ucwords($marketplaces['Amazon'][$order['id_country']]) : '' ;
		    $orders[$id]['mp_channel_status'] = isset($order['mp_channel_status']) ?  ($order['mp_channel_status']) : '';   
		    $orders[$id]['total_paid'] = number_format($order['total_paid'], 2);
		    $orders[$id]['order_date'] = $order['order_date'];
		    $orders[$id]['language'] = isset($languages[$order['id_lang']]['iso_code']) ? strtoupper($languages[$order['id_lang']]['iso_code']) : '' ; 
		    $orders[$id]['comment'] = $order['comment'];
		    
		    /*Amazon FBA*/		    
		    $data = AmazonUserData::getMasterFBA(ObjectModelOrder::$user, $order['site'], $id_shop);
		    
		    if(!empty($data)) {
			$orders[$id]['id_country'] = $data['id_country']; 
			$orders[$id]['fba_multichannel_auto'] = (bool)$data['fba_multichannel_auto']; 
			$orders[$id]['country'] = isset($countries[$data['id_country']]['title_offer_sub_pkg']) ? 
                        str_replace(" ", "_", $countries[$data['id_country']]['title_offer_sub_pkg']) : '' ;
		    }
		    
		    switch ($order['id_marketplace'])
		    {
			case $ebay_id :
			    
			    $total_product = 0;
			    $order_items = OrderItems::getOrderItemById((int)$order['id_orders']);
			    foreach ($order_items as $items){
				$total_product = $total_product + $items['quantity'];
			    }
			    $orders[$id]['total_products'] = $total_product;
			    break;
			
			default : 
			    $orders[$id]['total_products'] = $order['total_products'];
			    break;
		    }
		    
		    $id++;

		}  
	    }

	    return($orders);
	}

}

