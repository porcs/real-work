<?php

class OrderBuyers extends ObjectModelOrder {
        public $id_orders; 
        public $id_buyer; 
        public $id_buyer_ref; 
        public $site; 
        public $email; 
        public $name; 
        public $id_address_ref; 
        public $address1; 
        public $address2; 
        public $city; 
        public $district; 
        public $state_region; 
        public $country_code; 
        public $country_name; 
        public $postal_code; 
        public $phone; 
        public $security_code_token; 

        public static $definition = array(
                'table'     => 'order_buyer',
                'primary'   => array('id_orders', 'id_buyer'),
                'unique'    => array('order_buyer' => array('id_orders', 'id_buyer')),
                'fields'    => array(
                    'id_orders'         =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_buyer'          =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_buyer_ref'      =>  array('type' => 'varchar', 'required' => true, 'size' => 255),
                    'site'              =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
                    'email'             =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'name'              =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'id_address_ref'    =>  array('type' => 'varchar', 'required' => false, 'size' => 32),
                    'address1'          =>  array('type' => 'text', 'required' => false),
                    'address2'          =>  array('type' => 'text', 'required' => false, 'size' => 255),
                    'city'              =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'district'          =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'state_region'      =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'country_code'      =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'country_name'      =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'postal_code'       =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'phone'             =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                    'security_code_token' =>  array('type' => 'varchar', 'required' => false, 'size' => 128),
                    'address_name'          =>  array('type' => 'text', 'required' => false)
                ),
        );
        
        public function changeOrderAddressName($id_orders, $name) {
                $_db = ObjectModelOrder::$db;
                $sql = "UPDATE {$_db->prefix_table}order_buyer SET address_name = '".$name."' WHERE id_orders = $id_orders ; ";
                return $_db->db_exec($sql);
        }

        public function changeOrderAddress($id_orders, $data) {
                $_db = ObjectModelOrder::$db;
                $sql = "UPDATE {$_db->prefix_table}order_buyer SET ";
                foreach ($data as $field => $value){
                    $sql .= " $field = '$value',";
                }
                $sql = trim($sql, ",");
                $sql .= " WHERE id_orders = $id_orders ;";
                return $_db->db_exec($sql);
        }
        
	public static function getOrderBuyerById( $id_order = null ) {
            
                if (!isset($id_order) || empty($id_order))
                        return NULL;

                $Order_Buyer = array();
                $_db = ObjectModelOrder::$db;
                $_db->from('order_buyer');
                if ( $id_order ){
                    $_db->where( array('id_orders' => (int)$id_order) );
                }
                
                $result = $_db->db_query_fetch($_db->query);                
                if(isset($result)){
                    foreach ($result as $keys => $OrderBuyers){
			
			// Charset conversion
			/*foreach ($OrderBuyers as $order_key => $item) {
				if (!is_object($OrderBuyers[$order_key]) && !is_array($OrderBuyers[$order_key]) && strval($OrderBuyers[$order_key]))
					//if (mb_detect_encoding(strval($OrderBuyers[$order_key])) != 'UTF-8')
						$OrderBuyers[$order_key] = iconv("ISO-8859-15", "UTF-8//TRANSLIT", $item);
			}*/

			/*** support feed.biz module version < 1.1.18 ***/
			    $Order_Buyer['buyer']['id_buyer'] = htmlspecialchars_decode($OrderBuyers['id_buyer'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['id_buyer_ref'] = htmlspecialchars_decode($OrderBuyers['id_buyer_ref'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['site'] = htmlspecialchars_decode($OrderBuyers['site'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['email'] = htmlspecialchars_decode($OrderBuyers['email'], ENT_QUOTES); 

			    // name
			    $Order_Buyer['buyer']['name'] = htmlspecialchars_decode($OrderBuyers['address_name'], ENT_QUOTES); 
			    $cus_fullname_split = self::getCustomerName ( $Order_Buyer['buyer']['name'] );
			    $Order_Buyer['buyer']['firstname'] = $cus_fullname_split ['firstname'];
			    $Order_Buyer['buyer']['lastname'] = $cus_fullname_split ['lastname'];  

			    // address name
			    $Order_Buyer['buyer']['id_address_ref'] = htmlspecialchars_decode($OrderBuyers['id_address_ref'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['address_name'] = htmlspecialchars_decode($OrderBuyers['address_name'], ENT_QUOTES); 
			    if(!empty($Order_Buyer['buyer']['address_name'])) {
				$cus_fullname_split = self::getCustomerName ( $Order_Buyer['buyer']['address_name'] );
				$Order_Buyer['buyer']['address_firstname'] = $cus_fullname_split ['firstname'];
				$Order_Buyer['buyer']['address_lastname'] = $cus_fullname_split ['lastname'];
				$Order_Buyer['buyer']['company'] = $cus_fullname_split ['company'];
			    }

			    // Address
			    $Order_Buyer['buyer']['address1'] = htmlspecialchars_decode($OrderBuyers['address1'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['address2'] = htmlspecialchars_decode($OrderBuyers['address2'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['state_region'] = htmlspecialchars_decode($OrderBuyers['state_region'], ENT_QUOTES); 

			    if (empty($Order_Buyer['buyer']['address1'])) {
				$Order_Buyer['buyer']['address1'] = pregAddress($Order_Buyer['buyer']['address2']);
				$Order_Buyer['buyer']['address2'] = isset($Order_Buyer['buyer']['state_region']) ? (string)self::_filter($Order_Buyer['buyer']['state_region']) : null;
			    } else {
				$Order_Buyer['buyer']['address1'] = pregAddress($Order_Buyer['buyer']['address1']); 
				$Order_Buyer['buyer']['address2'] = pregAddress($Order_Buyer['buyer']['address2']); 
			    }

			    $Order_Buyer['buyer']['city'] = pregCityName(htmlspecialchars_decode($OrderBuyers['city'], ENT_QUOTES));
			    $Order_Buyer['buyer']['district'] = htmlspecialchars_decode($OrderBuyers['district'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['country_code'] = htmlspecialchars_decode($OrderBuyers['country_code'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['country_name'] = htmlspecialchars_decode($OrderBuyers['country_name'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['postal_code'] = htmlspecialchars_decode($OrderBuyers['postal_code'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['phone'] = htmlspecialchars_decode($OrderBuyers['phone'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['security_code_token'] = htmlspecialchars_decode($OrderBuyers['security_code_token'], ENT_QUOTES); 
			
			/*** support feed.biz module version >= 1.1.18 ***/
			    $Order_Buyer['buyer']['id_buyer'] = htmlspecialchars_decode($OrderBuyers['id_buyer'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['id_buyer_ref'] = htmlspecialchars_decode($OrderBuyers['id_buyer_ref'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['site'] = htmlspecialchars_decode($OrderBuyers['site'], ENT_QUOTES); 
			    $Order_Buyer['buyer']['email'] = htmlspecialchars_decode($OrderBuyers['email'], ENT_QUOTES); 

			    // name
			    $Order_Buyer['buyer']['name'] = htmlspecialchars_decode($OrderBuyers['address_name'], ENT_QUOTES); 
			    $cus_fullname_split = self::getCustomerName ( $Order_Buyer['buyer']['name'] );
			    $Order_Buyer['buyer']['firstname'] = $cus_fullname_split ['firstname'];
			    $Order_Buyer['buyer']['lastname'] = $cus_fullname_split ['lastname'];  

			    // address name
			    $Order_Buyer['shipping']['id_address_ref'] = htmlspecialchars_decode($OrderBuyers['id_address_ref'], ENT_QUOTES); 
			    $Order_Buyer['shipping']['name'] = htmlspecialchars_decode($OrderBuyers['address_name'], ENT_QUOTES); 
			    if(!empty($Order_Buyer['shipping']['name'])) {
				$cus_fullname_split = self::getCustomerName ( $Order_Buyer['shipping']['name'] );
				$Order_Buyer['shipping']['firstname'] = $cus_fullname_split ['firstname'];
				$Order_Buyer['shipping']['lastname'] = $cus_fullname_split ['lastname'];
				$Order_Buyer['shipping']['company'] = $cus_fullname_split ['company'];
			    }

			    // Address
			    $Order_Buyer['shipping']['address1'] = htmlspecialchars_decode($OrderBuyers['address1'], ENT_QUOTES); 
			    $Order_Buyer['shipping']['address2'] = htmlspecialchars_decode($OrderBuyers['address2'], ENT_QUOTES); 
			    $Order_Buyer['shipping']['state_region'] = htmlspecialchars_decode($OrderBuyers['state_region'], ENT_QUOTES); 

			    if (empty($Order_Buyer['shipping']['address1'])) {
				$Order_Buyer['shipping']['address1'] = pregAddress($Order_Buyer['shipping']['address2']);
				$Order_Buyer['shipping']['address2'] = isset($Order_Buyer['shipping']['state_region']) ? (string)self::_filter($Order_Buyer['shipping']['state_region']) : null;
			    } else {
				$Order_Buyer['shipping']['address1'] = pregAddress($Order_Buyer['shipping']['address1']); 
				$Order_Buyer['shipping']['address2'] = pregAddress($Order_Buyer['shipping']['address2']); 
			    }

			    $Order_Buyer['shipping']['city'] = pregCityName(htmlspecialchars_decode($OrderBuyers['city'], ENT_QUOTES));
			    $Order_Buyer['shipping']['district'] = htmlspecialchars_decode($OrderBuyers['district'], ENT_QUOTES); 
			    $Order_Buyer['shipping']['country_code'] = htmlspecialchars_decode($OrderBuyers['country_code'], ENT_QUOTES); 
			    $Order_Buyer['shipping']['country_name'] = htmlspecialchars_decode($OrderBuyers['country_name'], ENT_QUOTES); 
			    $Order_Buyer['shipping']['postal_code'] = htmlspecialchars_decode($OrderBuyers['postal_code'], ENT_QUOTES); 
			    $Order_Buyer['shipping']['phone'] = htmlspecialchars_decode($OrderBuyers['phone'], ENT_QUOTES); 
			    $Order_Buyer['shipping']['security_code_token'] = htmlspecialchars_decode($OrderBuyers['security_code_token'], ENT_QUOTES); 
                    }
                }       
                
                unset($Order_Buyer['id_orders']);
             
                return $Order_Buyer;
        }	
        
        public function getOrderBuyer() {
            
                $Order_Buyer = array();
                $_db = ObjectModelOrder::$db;
                $_db->from('order_buyer');
                $result = $_db->db_query_fetch($_db->query);
                
                if(isset($result)){
                    foreach ($result as $keys => $OrderBuyers){
                        foreach ($OrderBuyers as $key => $Buyers){
                           $Order_Buyer[$keys][$key] = htmlspecialchars_decode($Buyers, ENT_QUOTES);                         
                            switch ($key){
                                case 'name': 
                                    $cus_fullname_split = self::getCustomerName ( $Order_Buyer[$keys][$key]);
                                    $Order_Buyer[$keys]['firstname'] = $cus_fullname_split ['firstname'];
                                    $Order_Buyer[$keys]['lastname'] = $cus_fullname_split ['lastname'];                                    
                                break;
                                case 'address_name':
                                    $cus_fullname_split = self::getCustomerName ( $Order_Buyer[$keys][$key] );
                                    $Order_Buyer[$keys]['address_firstname'] = $cus_fullname_split ['firstname'];
                                    $Order_Buyer[$keys]['address_lastname'] = $cus_fullname_split ['lastname'];
                                    $Order_Buyer[$keys]['company'] = $cus_fullname_split ['company'];
                                break;
                                case 'city' : $Order_Buyer[$keys][$key] = pregCityName($Order_Buyer[$keys][$key]); break;
                                case 'address1' : 
				case 'address2' : $Order_Buyer[$keys][$keys] = pregAddress($Order_Buyer[$keys][$keys]); break;
                            } 
                        }
                        unset($Order_Buyer[$keys]['id_orders']);
                    }
                }                
                return $Order_Buyer;
        }        
        
        public static function getCustomerName($fullname)
        {
		$result = array();
		$result['company'] = '';
		
		$fullname = str_replace(array('<','>','{','}'), array('(',')','(',')'), $fullname);
		$fullname = self::_filter($fullname);

		if (preg_match('/(c\/o\b)/i', $fullname)) // case: John Doe c/o Apple Inc.
		{
		    if ($parts = preg_split('/(c\/o\b)/i', $fullname))
		    {
			if (count($parts) > 1)
			{
			    $result['company'] = trim($parts[1]);
			    $fullname = trim(reset($parts));
			}
			else $fullname = trim(reset($result));
		    }
		    $var = mb_substr($fullname, 0, 64);
		}
		elseif (preg_match('/,|\//', $fullname))
		{
		    
		    $parts = preg_split('/,|\//', $fullname);
		    $var = trim(mb_substr(array_shift($parts), 0, 32));
		    
		    $result['company'] = trim(mb_substr(@implode('/', $parts), 0, 32));
		     // company invalid characters : <>:=#{}
		} else {
		    $var = mb_substr($fullname, 0, 64);
		}

		$var = mb_ereg_replace('[0-9!<>,;?=+()@#"Â°{}_$%:]', '', $var);

		$reverse_fullname = self::mb_strrev($var);
		$name1 = trim(self::mb_strrev(substr($reverse_fullname, mb_strpos($reverse_fullname, ' '))));
		$name2 = trim(self::mb_strrev(substr($reverse_fullname, 0, mb_strpos($reverse_fullname, ' '))));
		
		if(strlen($name1) > 32){
		    $name = str_split($name1, 32);
		    $name1 = mb_substr($name[0], 0, 32);
		    $name2 = mb_substr($name[1], 0, 32);
		}

		if (empty($name1) && empty($name2)) {
		    $name1 = 'unknown';
		    $name2 = 'unknown';
		} elseif (empty($name1)) {
		    $name1 = $name2;
		} elseif (empty($name2)) {
		    $name2 = $name1;
		}

		if($name1 == $name2){
		    if(mb_strpos($name1, ' ')){
			$full_name = self::mb_strrev($name1);
			$name1 = trim(self::mb_strrev(substr($full_name, mb_strpos($full_name, ' '))));
			$name2 = trim(self::mb_strrev(substr($full_name, 0, mb_strpos($full_name, ' '))));
		    }
		}
		
		$result['firstname'] = ucfirst($name1);
		$result['lastname'] = ucfirst($name2);

		return ($result);
        }    

        public static function mb_strrev($str, $encoding='UTF-8')
        {
            return mb_convert_encoding( strrev( mb_convert_encoding($str, 'UTF-16BE', $encoding) ), $encoding, 'UTF-16LE');
        }

        public static function _filter($text) {
            if (!self::isJapanese($text)) {
                $text = mb_convert_encoding($text, 'HTML-ENTITIES', 'UTF-8');

                $searches = array('&szlig;', '&(..)lig;', '&([aouAOU])uml;', '&(.)[^;]*;');
                $replacements = array('ss', '\\1', '\\1' . 'e', '\\1');

                foreach ($searches as $key => $search)
                    $text = mb_ereg_replace($search, $replacements[$key], $text);
            }

            $text = str_replace('_', '/', $text);
            $text = mb_ereg_replace('[\x00-\x1F\x21-\x2C\x3A-\x3F\x5B-\x60\x7B-\x7F\x2E\x2F]]', '', $text); // remove non printable
            $text = mb_ereg_replace('[!<>?=+@{}_$%]*$', '', $text); // remove chars rejected by Validate class

            return $text;
        }

        //http://stackoverflow.com/questions/2856942/how-to-check-if-the-word-is-japanese-or-english-using-php
        public static function isJapanese($word) {
            return preg_match('/[\x{4E00}-\x{9FBF}\x{3040}-\x{309F}\x{30A0}-\x{30FF}]/u', $word);
        }

}