<?php

class OrderInvoices extends ObjectModelOrder {
    
        public $id_orders;
        public $id_invoice;
        public $total_discount_tax_excl;
        public $total_discount_tax_incl;
        public $total_paid_tax_excl;
        public $total_paid_tax_incl;
        public $total_products;
        public $total_shipping_tax_excl;
        public $total_shipping_tax_incl;
        public $note;
        public $date_add;

        public static $definition = array(
                'table'     => 'order_invoice',
                'fields'    => array(
                    'id_orders'                 =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_invoice'                =>  array('type' => 'int', 'required' => false, 'size' => 10),
                    'seller_order_id'           =>  array('type' => 'VARCHAR', 'required' => false, 'size' => 40),
                    'total_discount_tax_excl'   =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'total_discount_tax_incl'   =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'total_paid_tax_excl'       =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'total_paid_tax_incl'       =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'total_products'            =>  array('type' => 'int', 'required' => false, 'size' => 100),
                    'total_shipping_tax_excl'   =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'total_shipping_tax_incl'   =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
                    'note'                      =>  array('type' => 'text', 'required' => false),
                    'date_add'                  =>  array('type' => 'datetime', 'required' => false),
		    'invoice_no'                =>  array('type' => 'VARCHAR', 'size' => 64, 'required' => false),
                ),
        );
        
        public static function getOrderInvoiceById($id_order = null) {
                if (!isset($id_order) || empty($id_order))
                        return NULL;

                $_db = ObjectModelOrder::$db;
                $_db->from('order_invoice');
                if ( $id_order )
                        $_db->where( array('id_orders' => (int)$id_order) );
                
                $result = $_db->db_query_fetch($_db->query);
                $result = current($result);

                unset($result['id_orders']);
                return $result;
        }
    
        public function getOrderInvoice($id_orders = null) {
                
                $_db = ObjectModelOrder::$db;

                $_db->from('order_invoice');
                if ( $id_orders )
                        $_db->where( array('id_orders' => (int)$id_orders) );                
                
                $result = $_db->db_query_fetch($_db->query);
                return current($result);
        }
        
        public static function getOrderInvoiceBySellerOrderId($id_order_sellers = null) {
                if (!isset($id_order_sellers) || empty($id_order_sellers))
                        return NULL;

                $_db = ObjectModelOrder::$db;

                $sql = "SELECT 
                        oi.id_orders AS id_orders,
                        oi.seller_order_id as seller_order_id,
                        o.id_marketplace_order_ref as id_marketplace_order_ref
                        FROM {$_db->prefix_table}order_invoice oi
                        LEFT JOIN orders_orders o ON (o.id_orders = oi.id_orders)";
                $sql .= !empty($id_order_sellers) && is_array($id_order_sellers) ? 
                       " WHERE oi.seller_order_id IN ('".implode("', '", $id_order_sellers)."') " : "";
                
                $prepared_result = array();
                $result = $_db->db_query_str($sql); 

                foreach($result as $result_ele){
                    $prepared_result[$result_ele['seller_order_id']] = $result_ele['id_marketplace_order_ref'];
                }

                return $prepared_result;
        }
}