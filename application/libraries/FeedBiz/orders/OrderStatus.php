<?php

class OrderStatus extends ObjectModelOrder {
    
        public $id_status;
        public $id_lang;
        public $name;
        public $date_add;

        public static $definition = array(
            'table'     => 'order_status',
            'primary'   => array('id_status'),
            'unique'        => array('order_status' => array('id_status')),
            'fields'    => array(
                'id_status'     =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'          =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
                'date_add'      =>  array('type' => 'datetime', 'required' => false),
            ),
        );
    
        public function getOrderStatus($id_orders = null) {
                
                $_db = ObjectModelOrder::$db;

                $_db->from('order_status');
                if ( $id_orders )
                        $_db->where( array('id_orders' => (int)$id_orders) );                
                
                $result = $_db->db_query_fetch($_db->query);
                return current($result);
        }
}