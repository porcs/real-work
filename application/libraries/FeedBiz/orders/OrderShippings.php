<?php

class OrderShippings extends ObjectModelOrder {

    public $id_orders;
    public $shipment_service;
    public $weight;
    public $tracking_number;
    public $shipping_services_cost;
    public $shipping_services_level;
    public $id_carrier;
    public static $definition = array(
        'table' => 'order_shipping',
        'primary' => array('id_orders'),
        'unique' => array('order_shipping' => array('id_orders')),
        'fields' => array(
            'id_orders' => array('type' => 'int', 'required' => true, 'size' => 10),
            'shipment_service' => array('type' => 'varchar', 'required' => false, 'size' => 255),
            'weight' => array('type' => 'float', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'tracking_number' => array('type' => 'varchar', 'required' => false, 'size' => 255),
            'shipping_services_cost' => array('type' => 'float', 'required' => false, 'size' => '20,6'),
            'shipping_services_level' => array('type' => 'varchar', 'required' => false, 'size' => 255),
            'id_carrier' => array('type' => 'int', 'required' => false, 'size' => 10),
        ),
    );

    public static function getOrderShippingById($id_order = null) {
        if (!isset($id_order) || empty($id_order))
            return NULL;

        $_db = ObjectModelOrder::$db;
        $_db->from('order_shipping');
        if ($id_order)
            $_db->where(array('id_orders' => (int) $id_order));

        $result = $_db->db_query_fetch($_db->query);
        $result = current($result);

        foreach (self::$definition['fields'] as $fields_key => $fields) 
	{
	    if(isset($result[$fields_key])) 
	    {
		if ($fields['type'] == 'int' || $fields['type'] == 'tinyint' || $fields['type'] == 'INTEGER') 
		{
		    settype($result[$fields_key], "integer");
		}

		if ($fields['type'] == 'float')
		{
		    settype($result[$fields_key], "float");
		}
	    }
        }
	
        unset($result['id_orders']);
        return $result;
    }

    public function getOrderShipping($id_orders = null) {
        
        $_db = ObjectModelOrder::$db;
        $_db->from('order_shipping');
        if ($id_orders)
            $_db->where(array('id_orders' => (int) $id_orders));

        $result = $_db->db_query_fetch($_db->query);
        return current($result);
    }

    public function getOrdersNoTracking($id_arr) {
       
        $_db = ObjectModelOrder::$db;

        $sql = "SELECT 
                    o.id_orders as id_orders,
                    o.id_marketplace_order_ref as id_marketplace_order_ref,
                    o.sales_channel as sales_channel,
                    o.id_shop as id_shop,
                    o.site as site
                FROM {$_db->prefix_table}orders o
                JOIN {$_db->prefix_table}order_shipping os ON o.id_orders = os.id_orders 
                WHERE (os.tracking_number IS NULL OR os.tracking_number = '') ";
        $sql .= $id_arr && is_array($id_arr) ? " AND o.id_marketplace_order_ref IN ('" . implode("', '", $id_arr) . "') " : "";

        $prepared_result = array();
        $result = $_db->db_query_str($sql); 
        foreach($result as $result_ele){
        	$prepared_result[$result_ele['id_orders']] = $result_ele;
        }
        return $prepared_result;
    }

    public function getOrdersForShip() {
        
        $_db = ObjectModelOrder::$db;

        $sql = "SELECT o.id_orders as id_orders, 
                o.id_marketplace_order_ref as id_marketplace_order_ref, 
                o.sales_channel as sales_channel, 
                o.id_shop as id_shop, 
                o.site as site, 
                o.id_marketplace as id_marketplace, 
                o.order_status as order_status, 
                o.shipping_date as shipping_date, 
                oi.id_marketplace_order_item_ref as id_marketplace_order_item_ref,
                oi.id_product as id_product,
                oi.product_name as product_name,
                os.tracking_number as tracking_number,
                os.id_carrier as id_carrier,
                o.shipping_date as shipping_date
                FROM {$_db->prefix_table}orders o 
                JOIN {$_db->prefix_table}order_items oi ON o.id_orders = oi.id_orders 
                JOIN {$_db->prefix_table}order_shipping os on o.id_orders = os.id_orders
                WHERE (os.tracking_number IS NOT NULL AND os.tracking_number != '') AND IFNULL(o.shipping_status, 0) != 1 ";

        $result = $_db->db_query_str($sql);
        $prepared_result = array();
        foreach ($result as $result_ele) {
            $prepared_result[$result_ele['sales_channel']][$result_ele['site']][$result_ele['id_orders']] = $result_ele;
        }
        return $prepared_result;
    }
    
    public function updateOrderTracking($orders) {
        
        $_db = ObjectModelOrder::$db;
        $sql = '';
	
        $arr = array();
        foreach ($orders as $order_element) {
            $arr[] = $order_element['MPOrderID'];
        }
	
        $sql = "SELECT os.id_orders ,o.id_marketplace_order_ref FROM {$_db->prefix_table}order_shipping os
                        LEFT JOIN {$_db->prefix_table}orders o ON o.id_orders = os.id_orders 
                        WHERE o.id_marketplace_order_ref in ('".implode("' ,'",$arr)."') 
                        AND (os.tracking_number IS NULL OR os.tracking_number = '') ";
        $list_id = $_db->db_query_str($sql);  
            
        $arr_id = array();
        foreach ($list_id as $or) {
            $arr_id[$or['id_marketplace_order_ref']] = $or['id_orders'];
        }  
	
        $sql='';
        foreach ($orders as $order_element) {
	    
            if(!isset($arr_id[$order_element['MPOrderID']]))
		continue;

            $update_carrier_id = ''; // to update id_carrier when shop changed carrier ; // 12/04/2016
            
            if(isset($order_element['CarrierID']) && !empty($order_element['CarrierID']) && $order_element['CarrierID']){
                $update_carrier_id = ", id_carrier = " . (int)$order_element['CarrierID']. " ";
            }
            
            //$date = date("c", strtotime($order_element['ShippingDate']));
            $date = date("Y-m-d H:i:s", strtotime($order_element['ShippingDate']));
            
            $sql .= "UPDATE {$_db->prefix_table}orders SET shipping_date = '" . $date . "',  
                    order_status = 'Shipped' 
                    WHERE id_orders  = '".$arr_id[$order_element['MPOrderID']]."'; ";
            
            $sql .= "UPDATE {$_db->prefix_table}order_shipping SET tracking_number = '" . $order_element['ShippingNumber'] . "' $update_carrier_id 
                    WHERE id_orders  = '".$arr_id[$order_element['MPOrderID']]."';  ";
                    
        }        	
	
        if(! $_db->db_exec($sql,false,true,true))
		return false;
	
        return true;        
    }    
    
    public function updateOrderTrackingByIdOrders($orders, $fbtoken=null, $url=null, $batchID=null, $debug=false) { //08/12/2015
        
        $_db = ObjectModelOrder::$db;       
	
        $sql='';
        foreach ($orders as $order_element) {
	   
            if(!isset($order_element['OrderID']))
		continue;
           
	    // Update shipping Number to Shop
	    if(isset($fbtoken) && isset($url)){
		
		if(!isset($order_element['SellerOrderId']))
		    continue;
		
		if(!isset($order_element['ShippingNumber']))
		    continue;
		
		if(!isset($order_element['CarrierName']))
		    continue;
		
		$output = '';
		$conj = strpos($url, '?') !== FALSE ? '&' : '?';
		
		$preparedURL = $url . $conj . sprintf('fbtoken=%s&seller_order_id=%s&tracking_number=%s&carrier_code=%s', 
			$fbtoken, 
			intval($order_element['SellerOrderId']),
			strval($order_element['ShippingNumber']),
			rawurlencode(strval($order_element['CarrierName']))			
		);

		// 3. update Canceled order 
		if ($debug){

		    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		    echo 'preparedURL : <pre>' . print_r($preparedURL, true) . '</pre>';

		} else {
                    if(function_exists('fbiz_get_contents')){
                        $return = fbiz_get_contents($preparedURL, array('curl_method'=>1));
                    }else{
                        $return = @file_get_contents($preparedURL);
                    }
		    if (!$page = simplexml_load_string($return, 'SimpleXMLElement', LIBXML_NOCDATA)) {

			$output = 'Failed to open stream from module. HTTP request failed! HTTP/1.0 500 Internal Server Error';

		    } else {

			if (isset($page->Status->Code)) {
			    if (strval($page->Status->Code) == 1) {

			       $output = strval($page->Status->Message);                   

			    } else if (strval($page->Status->Code) == -1) { 

				$output = strval($page->Status->Output);

			    } else {

				if(isset($page->Status->Output)) {
				    $output = strval($page->Status->Output);
				} else if (isset($page->Status->Error)) {
				    $output = strval($page->Status->Error);
				} else {
				    $output = 'Send error';
				} 
			    }
			} else {
			    $output =  'Send Error';
			}
		    }
		}
		
		// 4. log
		/*$data_insert = array(
		    'id_shop' => $order_element['id_shop'],
		    'id_marketplace' => $order_element['id_marketplace'],
		    'site' => $order_element['site'],
		    'batch_id' => $batchID,
		    'request_id' => $order_element['OrderID'],
		    'error_code' => $order_element['SellerOrderId'],
		    'message' => 'Update Order Tracking : ' . $output,
		    'date_add' => date('Y-m-d H:i:s')
		);
		
		$sql .= $_db->insert_string('log', $data_insert);*/
		
	    }
	    
            $sql .= "UPDATE {$_db->prefix_table}orders SET shipping_date = '" . $order_element['ShippingDate'] . "', order_status = 'Shipped', shipping_status = '1' 
                    WHERE id_orders  = ".$order_element['OrderID']." ; ";
            
            $sql .= "UPDATE {$_db->prefix_table}order_shipping SET tracking_number = '" . $order_element['ShippingNumber'] . "' " ;
		
	    if(isset($order_element['CarrierName']) && !empty($order_element['CarrierName']))
		$sql .= " , shipping_services_cost = '".$order_element['CarrierName']."' ";
		
	    $sql .= " WHERE id_orders = ".$order_element['OrderID'].";  ";
            
        }        	
	if(!$debug){
	    if(!$exec = $_db->db_exec($sql,false,true,true)){
		    return false;
	    }
	} else {
	    echo $sql;
	}
	
        return true;        
    }
    
    public function updateShippedStatus($orders, $where = 'id_orders'){
        
        if(trim($where) == null ) $where = 'id_orders';
        
    	$_db = ObjectModelOrder::$db;
    	$sql = "UPDATE {$_db->prefix_table}orders SET shipping_status = '1' WHERE $where IN ('".implode("', '", $orders)."') ; ";
    	
    	if(! $_db->db_exec($sql)) return false;
    	return true;
    }
}
