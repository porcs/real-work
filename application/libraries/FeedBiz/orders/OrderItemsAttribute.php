<?php

class OrderItemsAttribute extends ObjectModelOrder {
    
        public $id_order_items;
        public $id_attribute_group;
        public $id_attribute;
        public $id_shop;

        public static $definition = array(
            'table'     => 'order_items_attribute',
            'fields'    => array(
                    'id_order_items'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_attribute_group'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_attribute'          =>  array('type' => 'int', 'required' => true, 'size' => 10),
                    'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10),
            ),
        );
        
        public static function getOrderItemsAttributeById($id_order = null) {         
                if (!isset($id_order) || empty($id_order))
                        return NULL;

                $_db = ObjectModelOrder::$db;
                $_db->select(array('id_attribute_group', 'id_attribute'));
                $_db->from('order_items_attribute');
                if ( $id_order )
                        $_db->where( array('id_order_items' => (int)$id_order) );
                
                $result = $_db->db_query_fetch($_db->query);
                return $result;
        }
        
        
        public function getOrderItemsAttribute($id_orders = null) {
                
                $_db = ObjectModelOrder::$db;

                $_db->from('order_items_attribute');
                if ( $id_orders )
                        $_db->where( array('id_order_items' => (int)$id_orders) );
                
                $result = $_db->db_query_fetch($_db->query);
                return $result;
        }
}