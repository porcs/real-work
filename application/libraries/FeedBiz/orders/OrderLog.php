<?php

class OrderLog extends ObjectModelOrder
{
    public static $definition = array(
        'table'     => 'log',
        'fields'    => array(
            'batch_id'          =>  array('type' => 'varchar', 'required' => false, 'size' => 32),
            'request_id'        =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'error_code'        =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'message'           =>  array('type' => 'text'),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
            'id_shop'           =>  array('type' => 'int', 'required' => false ),
            'id_marketplace'    =>  array('type' => 'int', 'required' => true ),
            'site'              =>  array('type' => 'int', 'required' => false ),
        ),
    );

    public function __construct( $user ) {
    	parent::__construct( $user );
    }
    
    public function insertLog($logs) {
    	$sql = '';
    	$fields = array_keys(self::$definition['fields']);
    	$_db = ObjectModelOrder::$db;
    	foreach($logs as $log){
			$prepared_val = array();
    		foreach ($fields as $field){
    			$prepared_val[] = isset($log[$field]) ? $_db->escape_str($log[$field]) : '';
    		}
    		
    		$sql .= 'INSERT INTO '.$_db->prefix_table.self::$definition['table'].' ('.implode(', ', $fields).') VALUES (';
    		$sql .= "'".implode("', '", $prepared_val)."'";
    		$sql .= ');';
    	}
    	
    	if($sql){	    	
	    	if(! $_db->db_exec($sql))
	    		return false;
    	}
    	return true;
    }
}