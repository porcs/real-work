<?php

require_once(dirname(__FILE__) . '/products/Shop.php');
require_once(dirname(__FILE__) . '/products/Language.php');

class ImportLog {
    /* Build object */
    private static $flag_offers = null;
    private static $flag_offers_log = null;
    public function __construct($user) {
        $this->user = $user;
        if ( isset($this->user) && !empty($this->user)){
            $this->db = new Db($this->user, 'log');
            $this->offer = new Db($this->user, 'offers');
            $this->products = new Db($this->user, 'products');
        }
    }

    public function get_pid() {
        
        $pid = '';
        $_db = $this->db;
        $_db->from('server_log');
        $result = $_db->db_array_query($_db->query);

        if (isset($result) && !empty($result))
            foreach ($result as $value)
                $pid = $value['pid'];

        return $pid;
    }

    public function get_import_log($limit=0, $start = null, $order_by = null, $keywords = null, $num_rows = false) {
        
        if($num_rows){
            $sql  = "SELECT 1 FROM log_log WHERE no_total >= 0   ";
        }else{
            $sql  = "SELECT * FROM log_log WHERE no_total >= 0   ";
        } 
        if(isset($keywords) && !empty($keywords)){
                $keywords = $this->db->escape_str($keywords);
                $sql  .= "AND ((batch_id LIKE '%" . $keywords . "%' ) or "
                        . "(source LIKE '%" . $keywords . "%' ) or"
                        . "(shop LIKE '%" . $keywords . "%' ) or"
                        . "(type LIKE '%" . $keywords . "%' ))";
        }
        if(isset($order_by) && !empty($order_by)){
                $order_by = $this->db->escape_str($order_by);
                $sql  .= "ORDER BY " . $order_by ." ";
        }  else {
                $sql  .= "ORDER BY datetime DESC ";            
        }
        //echo $start.' -> '.$limit;
        if(!$num_rows){
        if(isset($limit) && !empty($limit)){
            $limit = (int)$limit;
            if(isset($start) && !empty($start)){
                $start = (int)$start;
                $sql  .= "LIMIT " . $start . "," . $limit;
            }  else {
                $sql  .= "LIMIT " . $limit;
            }
        } 
        }
        
        $values = array();
        $_db = $this->db; 
        $result = $_db->db_query_str($sql); 
        if($num_rows){
            return sizeof($result);
        } 
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $values[$key]['id'] = $value['id'];
                $values[$key]['batch_id'] = $value['batch_id'];
                $values[$key]['shop'] = $value['shop'];
                $values[$key]['source'] = $value['source'];
                $values[$key]['type'] = $value['type'];
                $values[$key]['no_total'] = $value['no_total'];
                $values[$key]['no_success'] = $value['no_success'];
                $values[$key]['no_warning'] = $value['no_warning'];
                $values[$key]['no_error'] = $value['no_error'];
                $values[$key]['datetime'] = $value['datetime'];
                if($value['type']=='Product'){
                    $values[$key]['download']='1';
                }else if($value['type']=='Offer'){
                    $values[$key]['download']='2';
                }else{
                    $values[$key]['download']='0';
                }  
            }
        }

        return $values;
    }

    public function get_last_import_log($type, $shop) {
        
        $values = array();
        $_db = $this->db;
        $_db->from('log');
        $_db->where(array('type' => $type, 'shop' => $shop));
        $_db->where_select(array('datetime' => '(SELECT max(datetime) FROM '.$_db->prefix_table.'log WHERE type = "' . $type . '" AND shop = "' . $shop . '")'), 'IN');

        $result = $_db->db_array_query($_db->query);

        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $values['id'] = $value['id'];
                $values['batch_id'] = $value['batch_id'];
                $values['shop'] = $value['shop'];
                $values['source'] = $value['source'];
                $values['type'] = $value['type'];
                $values['no_total'] = $value['no_total'];
                $values['no_success'] = $value['no_success'];
                $values['no_warning'] = $value['no_warning'];
                $values['no_error'] = $value['no_error'];
                $values['datetime'] = $value['datetime'];
            }
        }

        return $values;
    }

    public function download_log($type, $data = null) {
        
        $list = array();
        $_db = $this->db;
        $_db->from('message_log');

        if (isset($data) && !empty($data)) {
        	$where = array();
        	$where_like = array();
        
        	if (is_array($data))
        		foreach ($data as $key => $value) {
        			if (isset($value) && !empty($value))
        				if (is_integer($value))
        					$where[$key] = $value;
        				else
        					$where_like[$key] = $value;
        		} else
        			$where['batch_id'] = $data;
        
        		if (isset($where) && !empty($where))
        			$_db->where($where);
        
        		if (isset($where_like) && !empty($where_like))
        			$_db->where_like($where_like);
        }
        
        $_db->orderBy('id_shop');
        $_db->orderBy('id_product');
        $result = $_db->db_array_query($_db->query);

        $list['header']['header']['id_log'] = "ID Log";
        $list['header']['header']['shop'] = "Shop Name";
        $list['header']['header']['id_product'] = "ID Products";
        $list['header']['header']['message'] = "Message";
        $list['header']['header']['date_add'] = "Date";

        if (isset($result) && !empty($result)) {
            foreach ($result as $product) {
                $shopname = Shop::getShopNameById($product['id_shop']);

                $list[$product['id_product']][$product['id_log']]['id_log'] = $product['id_log'];
                $list[$product['id_product']][$product['id_log']]['shop'] = $shopname;
                $list[$product['id_product']][$product['id_log']]['id_product'] = $product['id_product'];
                $list[$product['id_product']][$product['id_log']]['message'] = $product['message'];
                $list[$product['id_product']][$product['id_log']]['date_add'] = $product['date_add'];
            }
        }

        ob_start();
        ob_get_clean();

        $report_id = isset($data) && !empty($data) && is_string($data) ? $data : date('Ymd_His');
        $filename =  $report_id . '_' . $type. '_' . 'report.csv';

        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");

        $h = fopen('php://output', 'w');

        foreach ($list as $datas)
            foreach ($datas as $data)
                fputcsv($h, $data, ';');
    }
    
    public static function update_flag_offers($id_marketplace, $ext, $id_shop, $id_product, $flag = 0,$get_sql = true){
        
        if(!isset($id_marketplace) || !isset($ext) || !isset($id_shop) || !isset($id_product))
            return;

        if($get_sql){
            return "\n".'  REPLACE INTO offers_flag_update (id_marketplace, id_site, id_shop, id_product, flag) '
            . 'VALUES ('.$id_marketplace.',"'.$ext.'",'.$id_shop.','.$id_product.','.$flag.') ; '."\n"
            . 'INSERT INTO offers_flag_update_log (id_marketplace, id_site, id_shop, id_product, action_type, flag, date_add) '
            . 'VALUES ('.$id_marketplace.',"'.$ext.'",'.$id_shop.','.$id_product.',"update_offer",'.$flag.', "'.date('Y-m-d H:i:s').'") ; ';
        }else{
            if(empty(self::$flag_offers)){
                self::$flag_offers = array();
                self::$flag_offers_log = array();
            }
            self::$flag_offers[] = '('.$id_marketplace.',"'.$ext.'",'.$id_shop.','.$id_product.','.$flag.')';
            self::$flag_offers_log[] = '('.$id_marketplace.',"'.$ext.'",'.$id_shop.','.$id_product.',"update_offer",'.$flag.', "'.date('Y-m-d H:i:s').'")';
        }
        return '';
    }
    public static function get_all_flag_offers(){
        if(empty(self::$flag_offers)){
            return '';
        }
        $sep_no_row = 500;
        $sql='';
        $offers = self::$flag_offers;
        $prefix = 'REPLACE  INTO offers_flag_update (id_marketplace, id_site, id_shop, id_product, flag) VALUES ';
        if(!empty($offers)){ 
            if(sizeof($offers)>$sep_no_row){
                $set_list = array();
                foreach($offers as $i => $v){
                    $set_no = floor($i/$sep_no_row);
                    $set_list[$set_no][] = $v;
                    unset($offers[$i]);
                }
                foreach($set_list as $v){
                    $sql .= $prefix.implode(', ', $v) . "; \n";
                }
            }else{
                $sql .= $prefix.implode(', ', $offers) . "; \n";
            }
        }

        $offers = self::$flag_offers_log;
        $prefix = 'INSERT INTO offers_flag_update_log (id_marketplace, id_site, id_shop, id_product, action_type, flag, date_add) VALUES ';
        if(!empty($offers)){
            if(sizeof($offers)>$sep_no_row){
                $set_list = array();
                foreach($offers as $i => $v){
                    $set_no = floor($i/$sep_no_row);
                    $set_list[$set_no][] = $v;
                    unset($offers[$i]);
                }
                foreach($set_list as $v){
                    $sql .= $prefix.implode(', ', $v) . "; \n";
                }
            }else{
                $sql .= $prefix.implode(', ', $offers) . "; \n";
            }
        }
        return $sql;
        
    }

    
    public static function update_flag_offers_general($id_marketplace, $ext, $sub_marketplace, $id_shop, $id_product, $flag = 0){
        
        if(!isset($id_marketplace) || !isset($ext) || !isset($sub_marketplace) || !isset($id_shop) || !isset($id_product))
            return;
        if(!empty($id_product) && is_array($id_product)){
            $output = '';
            $list = array();
            $prefix = 'REPLACE INTO offers_flag_update (id_marketplace, id_site, sub_marketplace, id_shop, id_product, flag) values ';
            foreach($id_product as $id){
                $list[] = "('{$id_marketplace}','{$ext}', '{$sub_marketplace}', '{$id_shop}','{$id}','{$flag}')";
                if(sizeof($list)>=2000){
                   $output.=$prefix.implode(',' ,$list).";\n";
                   unset($list);
                }
            }
            if(!empty($list)){
                $output.=$prefix.implode(',' ,$list).";\n";
            }  
            return $output;
        }else{
            return ' REPLACE INTO offers_flag_update (id_marketplace, id_site, sub_marketplace, id_shop, id_product, flag) '
        . 'VALUES ('.$id_marketplace.',"'.$ext.'", "'.$sub_marketplace.'", '.$id_shop.','.$id_product.','.$flag.') ; ';
        }
    }

    /* Message Log */
    public function get_log($id_shop = null, $keywords = null, $order_by = null, $limit = null, $start = null, $num_rows = false, $type = 'product',$ignore_case=false)
    {
        //$column = '';
        $list = $where = $sort = array();
        $_db = $this->db;
        if(!$_db->db_table_exists('message_log'))return $list;
        $_db->from('message_log');
        
        if(isset($keywords) && !empty($keywords)){
            
            $where_like = $where_or= array();
            $where_like['message'] = $keywords;
            $where_like['batch_id'] = $keywords;
            $where_like['id_product'] = $keywords;
            $_db->where_like($where_like,true);
            
            $product_list = $this->getProducListtIdByName($keywords, $id_shop);
            
            if(!empty($product_list)){
                $list_id_product = implode(', ', $product_list['id_product']);
                $list_id_product_attribute = implode(', ', $product_list['id_product_attribute']);
                
                if(!empty($list_id_product)){
                    $where_in['id_product'] = '(' . $list_id_product . ')';
                }
                if(!empty($list_id_product_attribute)){
                    $where_in['id_product_attribute'] = '(' . $list_id_product_attribute . ')';
                }
                $_db->where( $where_in , 'IN', 'OR' );
            }
            
            
            if($keywords == "Warning" || $keywords == "warning"){
                $where_or['severity'] = 1;
            }
            if($keywords == "Error" || $keywords == "error"){
                $where_or['severity'] = 2;
            }
            if(!empty($where_or)){
                $_db->where($where_or , '=', 'OR' );
            }
        }
        
        if(isset($id_shop) && !empty($id_shop)){
            $where['id_shop'] = (int)$id_shop; 
        }
        $where['product_type'] = $type;
        if($ignore_case){
            $where_not_in_ignore=array('error_code'=>'(' .implode(',',array('7','8')).')');
            $_db->where( $where_not_in_ignore , 'NOT IN'  );
        }
        
        if(!empty($where)) {
            $_db->where( $where );
        }
        
        if(isset($start) && !empty($start)) {
            $_db->limit($limit, $start); 
        } else {
            $_db->limit($limit); 
        }

        if(isset($order_by) && !empty($order_by)){
            
             foreach($order_by as $order_sort){
                switch ($order_sort['field']){
                    case 'id_product' : $sort[] = $order_sort['field'] . ' ' . $order_sort['dir'] ; break;                 
                    case 'message' : $sort[] = $order_sort['field'] . ' ' . $order_sort['dir'] ; break;
                }
            }
            
            $sort_by = implode(', ', $sort);
            if(strlen($sort_by)){
                $_db->orderBy($sort_by);
            }
        }  
        if($num_rows){    
            
            $result =  $_db->db_sqlit_query($_db->query_string($_db->query));
            $row =  $_db->db_num_rows($result);
            
            return $row;
            
        } else {
            
            $shop = new Shop($this->user);
            $result = $_db->db_array_query($_db->query);
            foreach ($result as $product)
            {
                $id_product_attribute = null;
                
                if($product['id_product_attribute'] != 0){
                    $id_product_attribute = $product['id_product_attribute'];
                }
                
                $name = $this->getProductNameById((int)$product['id_product'], (int)$product['id_shop'], $id_product_attribute);                
                $shopname = $shop->getShopNameById($product['id_shop']);
                
                $list[$product['id_log']]['id_log'] = $product['id_log'];
                $list[$product['id_log']]['batch_id'] = $product['batch_id'];
                $list[$product['id_log']]['id_shop'] = $product['id_shop'];
                $list[$product['id_log']]['name'] = $name;
                $list[$product['id_log']]['shop'] = $shopname;
                $list[$product['id_log']]['severity'] =  $product['severity'];
                $list[$product['id_log']]['id_product'] = $product['id_product'];
                $list[$product['id_log']]['id_product_attribute'] = $product['id_product_attribute']; 
                $list[$product['id_log']]['error_code'] = $product['error_code'];
                $list[$product['id_log']]['message'] = $product['message'];
                $list[$product['id_log']]['date_add'] = $product['date_add'];

            }
            
            return $list;
        }
        
    }
    
    public function get_log_dashboard($num_product=0, $id_shop = null/*, $type = 'product'*/){
        
        if($num_product==0){
            return array();
        }
        $_db = $this->db;
        if(!$_db->db_table_exists('message_log')) return array();        
        
        $sql = 'SELECT Count(id_log) AS num, message as message, severity as severity
                FROM (select log_message_log.* FROM log_message_log 
                INNER JOIN products_product ON log_message_log.id_product = products_product.id_product 
                INNER JOIN products_category ON products_product.id_category_default = products_category.id_category 
                WHERE log_message_log.id_product_attribute = 0 group by log_message_log.id_product,log_message_log.id_product_attribute,message,error_code)
                as '.$_db->prefix_table.'message_log , products_shop s
                WHERE id_product_attribute = 0 and  '.$_db->prefix_table.'message_log.id_shop = s.id_shop and s.is_default=1
                GROUP BY '.$_db->prefix_table.'message_log.message 
                ORDER BY Count('.$_db->prefix_table.'message_log.id_log) DESC LIMIT 2 ';
                    
        $out = $_db->db_query_str($sql);
        $i=0;
        foreach($out as $k=>$o){
            $out[$i]['percent'] = ($o['num']/$num_product)*100;
                    $i++;
        }
        $sql = "select p.id_product_attribute from products_product_attribute p , products_shop s where p.id_shop = s.id_shop and s.is_default=1 group by id_product_attribute";
        $outx = $_db->db_query_str($sql);
        $attr_prod_num = sizeof($outx);
        $sql = 'SELECT Count(id_log) AS num, message as message, severity as severity
                FROM (select log_message_log.* FROM log_message_log
                INNER JOIN products_product ON log_message_log.id_product = products_product.id_product
                INNER JOIN products_category ON products_product.id_category_default = products_category.id_category
                WHERE log_message_log.id_product_attribute <> 0 group by log_message_log.id_product,log_message_log.id_product_attribute,message,error_code)  as '.$_db->prefix_table.'message_log , products_shop s
                WHERE id_product_attribute <> 0 and  '.$_db->prefix_table.'message_log.id_shop = s.id_shop and s.is_default=1  
                GROUP BY '.$_db->prefix_table.'message_log.message 
                ORDER BY Count('.$_db->prefix_table.'message_log.id_log) DESC LIMIT 1 ';
        
        $out2 = $_db->db_query_str($sql);
        foreach($out2 as $k=>$o){ 
            $out[$i] = $o;
            $out[$i]['percent'] = ($o['num']/$attr_prod_num)*100;
            $i++;
        }
        return $out;
    }
    
    public function getProductNameById($id_product, $id_shop, $id_product_attribute = null) {

        if (!isset($id_product) || empty($id_product) || !isset($id_shop) || empty($id_shop))
            return;

        $_db = $this->products;

        $lang =  Language::getLanguageDefault($id_shop); 
        $lang = array_keys($lang);
        $id_lang = $lang[0];

        $sql = "SELECT                          
            agl.name as agl_name, 
            a.name as a_name, 
            pl.name as product_name
            FROM ".$_db->prefix_table."product p 
            left join ".$_db->prefix_table."product_attribute pa on pa.id_product = p.id_product and   pa.id_shop = p.id_shop    
            LEFT JOIN ".$_db->prefix_table."product_lang pl ON p.id_product = pl.id_product AND p.id_shop = pl.id_shop
            LEFT JOIN ".$_db->prefix_table."product_attribute_combination pac ON pa.id_product_attribute = pac.id_product_attribute AND pa.id_shop = pac.id_shop
            LEFT JOIN ".$_db->prefix_table."attribute_group_lang agl ON (pac.id_attribute_group = agl.id_attribute_group AND pac.id_shop = agl.id_shop) AND agl.id_lang = " . (int) $id_lang . "
            LEFT JOIN ".$_db->prefix_table."attribute_lang a ON (pac.id_attribute_group = a.id_attribute_group AND pac.id_attribute = a.id_attribute AND pac.id_shop = a.id_shop) AND a.id_lang = " . (int) $id_lang . "
            WHERE p.id_product = " . $id_product . " AND p.id_shop = " . $id_shop . " ";

        if(isset($id_product_attribute) && !empty($id_product_attribute)){
            $sql .= " AND pa.id_product_attribute = " . $id_product_attribute . " ";
        }

        if (isset($id_lang) && !empty($id_lang)) {
            $sql .= "   AND pl.id_lang = " . (int) $id_lang . " ";
        }

        $sql .= " ; ";      
        $result = $_db->db_query_str($sql);
        $name = '';
        $attribute = array();

        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {

                //product_name
                $product_name = $value['product_name'];                                       

                if(isset($value['agl_name']) && !empty($value['agl_name'])){

                    $attribute[$key] = $value['agl_name'];

                    if(isset($value['a_name']) && !empty($value['a_name'])){
                        $attribute[$key] = $value['agl_name'] . ' ' . $value['a_name'];
                    }
                }
            }

            $name = $product_name;
            if (isset($attribute) && !empty($attribute) && isset($id_product_attribute) && !empty($id_product_attribute))
                $name .= ' - ' . implode(', ', $attribute);
        }

        return $name;
    }
    
    public function getProducListtIdByName($name, $id_shop) {

        if (!isset($name) || empty($name) || !isset($id_shop) || empty($id_shop))
            return;

        $_db = $this->products;
        
        $search = '';
        $language = new Language($this->user);
        $lang =  Language::getLanguageDefault($id_shop); 
        $lang = array_keys($lang);
        $id_lang = $lang[0];
        
        $names = explode(' - ', $name);
        
        if (isset($names[1])){            
            $attributes = explode(',', $names[1]);
            foreach ($attributes as $attr){
                $attrs = explode(' ', trim($attr));
                if(strlen($search) > 1) {
                    $search .= " OR ";
                }
                
                $search .= " pl.name like '%" . $names[0] . "%' ";
                
                if(!empty($attrs[0])){
                    $search .= " AND agl.name like '%" . $attrs[0] . "%' ";
                }
                
                if (isset($attrs[1])){ 
                    $search .= " AND a.name like '%" . $attrs[1] . "%' ";
                }
            }                       
        } else {
            $search .= " pl.name like '%" . $names[0] . "%' ";
            
            $attrs = explode(' ', trim($names[0]));

            if(!empty($attrs[0])){
                $search .= " OR agl.name like '%" . $attrs[0] . "%' ";
            }

            if (isset($attrs[1])){ 
                $search .= " OR a.name like '%" . $attrs[1] . "%' ";
            }
        }
        
        $sql = "SELECT                          
            pa.id_product as id_product, 
            pa.id_product_attribute as id_product_attribute
            FROM ".$_db->prefix_table."product_attribute pa     
            LEFT JOIN ".$_db->prefix_table."product_lang pl ON pa.id_product = pl.id_product AND pa.id_shop = pl.id_shop
            LEFT JOIN ".$_db->prefix_table."product_attribute_combination pac ON pa.id_product_attribute = pac.id_product_attribute AND pa.id_shop = pac.id_shop
            LEFT JOIN ".$_db->prefix_table."attribute_group_lang agl ON (pac.id_attribute_group = agl.id_attribute_group AND pac.id_shop = agl.id_shop)
            LEFT JOIN ".$_db->prefix_table."attribute_lang a ON (pac.id_attribute_group = a.id_attribute_group AND pac.id_attribute = a.id_attribute AND pac.id_shop = a.id_shop)
            WHERE pa.id_shop = " . $id_shop . " AND agl.id_lang = $id_lang AND a.id_lang = $id_lang 
            AND ($search) 
            GROUP BY pa.id_product,	pa.id_product_attribute; ";
       
        $result = $_db->db_query_str($sql);
        $products = $history = array();

        if (isset($result) && !empty($result)) {
            $products['id_product'] = array();
            $products['id_product_attribute'] = array();
            foreach ($result as $key => $value) {
                if(!isset($history['id_product'][$value['id_product']])){
                    array_push($products['id_product'], $value['id_product']);
                }
                
                if(!isset($history['id_product_attribute'][$value['id_product_attribute']])){
                    array_push($products['id_product_attribute'], $value['id_product_attribute']);
                }
                
                $history['id_product'][$value['id_product']] = true;
                $history['id_product_attribute'][$value['id_product_attribute']] = true;
            }
        }

        return $products;
    }
    
    public function get_product_update_log($id_shop, $date){
        
        $out = array();
        $product_update_status = array('instock', 'active', 'reprice');
        $_db = $this->db;
        if(!$_db->db_table_exists('product_update_log')) return array();
        
        $sql = "SELECT id_product, id_product_attribute
                FROM {$_db->prefix_table}product_update_log
                WHERE product_update_status IN ('".  implode("', '", $product_update_status)."')                
                AND id_shop = $id_shop AND date_add LIKE '%$date%'";
        
        $result = $_db->db_query_str($sql);
        foreach($result as $log){
            $out[$log['id_product']]['id_product'] = $log['id_product'];
            $out[$log['id_product']]['id_product_attribute'] = $log['id_product_attribute'];
        }
        
        return $out;
    }
}
