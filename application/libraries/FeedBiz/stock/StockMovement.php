<?php

class StockMovement
{
    const update_offer = 'update_offer';
    const import_order = 'import_order';
    const order_prefix = 'orders_';
    const product_prefix = 'products_';

    public function __construct( $user, $debug=false )
    {        
        $this->db = new Db($user, 'offers');
        $this->user = $user;
        $this->debug = $debug;
    }
    
    public function createTableStock(){
        
        $sql = "CREATE TABLE {$this->db->prefix_table}stock ("
                . "id_stock INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, "
                . "id_shop INTEGER NOT NULL , "
                . "id_product INTEGER, "
                . "id_product_attribute INTEGER, "
                . "name varchar(128) , "
                . "reference varchar(64) , "
                . "sku varchar(64) , "
                . "ean13 varchar(13) , "
                . "upc varchar(12) , "
                . "date_add datetime NOT NULL );" ;
        
        if(!$this->db->db_exec($sql,false))
            return false;
        
        return true;
    }
    
    public function createTableStockMovement(){
        
        $sql = "CREATE TABLE {$this->db->prefix_table}stock_mvt (
                id_stock_mvt INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, 
                id_stock INTEGER NOT NULL , 
                id_shop INTEGER NOT NULL , 
                action varchar(64) , 
                type varchar(64) , 
                source varchar(64) , 
                id_ref INTEGER, 
                quantity int(10) NOT NULL , 
                movement_qty int(10) NOT NULL , 
                balance_qty int(10) NOT NULL , 
                date_add datetime NOT NULL );";
        
        if(!$this->db->db_exec($sql,false))
            return false;
        
        $sql = "CREATE TABLE `{$this->db->prefix_table}stock_mvt_logs` (
                `id_log`  int(11) NOT NULL AUTO_INCREMENT ,
                `id_shop`  int(10) NULL ,
                `id_marketplace`  int(10) NULL ,
                `site`  int(10) NULL ,
                `batch_id`  varchar(32) NULL ,
                `error_code`  int(4) NULL ,
                `order_id`  varchar(32) NULL ,
                `id_product`  int(11) NULL ,
                `id_product_attribute`  int(11) NULL ,
                `message`  text NULL ,
                `quantity`  int(10) NULL ,
                `action_type`  varchar(32) NULL ,
                `comments`  text NULL ,
                `date_add`  datetime NULL ,
                PRIMARY KEY (`id_log`)
                );";
        
        if(!$this->db->db_exec($sql,false))
            return false;
        
        return true;
    }

    // to get stock movement from logs
    public function getstockMovementLogs($id_shop, $date_from = null, $date_to = null, $sku = null, $action_type = null){

        $logs = array();
        $where_date_from =  $where_date_to = $where_action_type = $where_id_product = $where_id_product_attribute = '';

        if(isset($date_from) && !empty($date_from)) {
            $where_date_from = " AND sml.date_add >= '".date('Y-m-d H:i:s', strtotime($date_from))."' " ;
        }

        if(isset($date_to) && !empty($date_to)) {
            $where_date_to = " AND sml.date_add <= '".date('Y-m-d H:i:s', strtotime($date_to))."' " ;
        }

        if(isset($action_type) && !empty($action_type))
        {
            switch ($action_type){
                case 'reserved' :
                    $where_action_type = " AND sml.action_type IN ('remove_cart', 'add_cart') "; break;
                default :
                    $where_action_type = " AND sml.action_type IN ('$action_type') "; break;
                }
        }

        if(isset($sku) && !empty($sku)){

            $sql_products = "SELECT p.id_product, pa.id_product_attribute FROM ".self::product_prefix."product p "
            . " LEFT JOIN ".self::product_prefix."product_attribute pa ON pa.id_product = p.id_product AND pa.id_shop = p.id_shop "
            . " WHERE p.id_shop = ".(int)$id_shop." AND (pa.reference = '$sku' OR p.reference = '$sku') ";
            $result_products = $this->db->db_query_str($sql_products);

            if(isset($result_products) && !empty($result_products)){
                $list_id_product = array();
                $list_id_product_attribute = array();
                foreach($result_products as $products){
                    array_push($list_id_product, (int)$products['id_product']);
                    if(isset($products['id_product_attribute'])){
                        array_push($list_id_product_attribute, (int)$products['id_product_attribute']);
                    }
                }
                $where_id_product = " AND sml.id_product IN (" .implode(", ",$list_id_product).") " ;

                if(count($list_id_product_attribute) > 0)
                    $where_id_product_attribute = " AND sml.id_product_attribute IN (" .implode(", ",$list_id_product_attribute).") " ;
            }
        }
        
        // get product list
        $sql = "SELECT SUM(sml.quantity) as summary, sml.*,
                CASE
                    WHEN pa.reference IS NOT NULL THEN pa.reference
                    WHEN p.reference IS NOT NULL THEN p.reference
                END AS reference FROM {$this->db->prefix_table}stock_mvt_logs sml "
            . "LEFT JOIN ".self::product_prefix."product p ON p.id_product = sml.id_product AND p.id_shop = sml.id_shop "
            . "LEFT JOIN ".self::product_prefix."product_attribute pa ON pa.id_product=sml.id_product AND pa.id_product_attribute = sml.id_product_attribute AND pa.id_shop = sml.id_shop "
            . "WHERE sml.id_shop = ".(int)$id_shop." $where_action_type $where_date_from $where_date_to $where_id_product $where_id_product_attribute "
            . "GROUP BY reference, sml.order_id "
            . "ORDER BY sml.date_add, reference, sml.order_id DESC ; ";

        $result = $this->db->db_query_str($sql);

        foreach ($result as $key => $log){
            if($log['summary'] <= 0) continue;
            
            $key = (int)($key + 1);
            $logs[$key]['date_add'] = $log['date_add'];
            $logs[$key]['sku'] = $log['reference'];
            $logs[$key]['id_product'] = (int)$log['id_product'];
            $logs[$key]['id_product_attribute'] = (int)$log['id_product_attribute'];
            $logs[$key]['order_id'] = $log['order_id'];
            $logs[$key]['quantity'] = $log['summary']*(-1);//($log['quantity'] * -1);
            $logs[$key]['message'] = $log['message'];
            $logs[$key]['action_type'] = $log['action_type'];
        }

        return $logs;
    }
    
    public function getstockMovementReport($id_shop, $start = null, $limit = null, $order_by = null, $search = null, $numrow = false){
        
        // get product list
        $sql = "SELECT * FROM {$this->db->prefix_table}stock s "
                . "LEFT JOIN {$this->db->prefix_table}stock_mvt smvt ON s.id_stock = smvt.id_stock AND s.id_shop = smvt.id_shop "
                . "WHERE s.id_shop = ".(int)$id_shop." and smvt.id_shop = ".(int)$id_shop." /*AND source NOT IN ('FBA')*/ ";
                
        if(isset($search) && !empty($search)){
            $sql .= "AND ( "
                    . "s.id_product like '%" . $search . "%' OR "
                    . "s.id_product_attribute like '%" . $search . "%' OR "
                    . "s.reference like '%" . $search . "%' "
                    . ") ";
        }
        
        if(isset($order_by) && !empty($order_by)){
            $sql .= "ORDER BY $order_by ";
        }
        if(isset($start) && isset($limit)){
            $sql .= "LIMIT $limit OFFSET $start ";
        }
        $sql .= " ; " ;
	
        if($numrow) {
            $result =  $this->db->db_sqlit_query($sql);
            $row =  $this->db->db_num_rows($result);
            return $row;
        }
        
        $result = $this->db->db_query_str($sql);
        return $result;
    }
    
    public function getStock($id_shop, $start = null, $limit = null, $order_by = null, $search = null){
        
        $sql =  "SELECT * FROM {$this->db->prefix_table}stock WHERE id_shop = ".(int)$id_shop." " ;
        
        if(isset($search) && !empty($search)){
            $sql .= "AND ( "
                    . "id_product like '%" . $search . "%' OR "
                    . "id_product_attribute like '%" . $search . "%' OR "
                    . "reference like '%" . $search . "%' "
                    . ") ";
        }
        if(isset($order_by) && !empty($order_by)){
            $sql .= "ORDER BY $order_by ";
        }
        if(isset($start) && isset($limit)){
            $sql .= "LIMIT $limit OFFSET $start ";
        }
        $sql .= " ; " ;
        
        $result = $this->db->db_query_str($sql);
        return $result;
        
    }
    
    public function getOrderIdStock($id_shop, $type = StockMovement::import_order){
        
        $list = array();        
        $sql =  "SELECT id_ref FROM {$this->db->prefix_table}stock_mvt WHERE id_shop = ".(int)$id_shop." AND date_add > '".date('Y-m-d H:i:s', strtotime('now - 15 days'))."' " ;
        
        if(isset($type) && !empty($type)){
            $sql .= "AND type = '" . $type . "' ";
        }
        
        $sql .= "ORDER BY id_ref ASC ; " ;
        
        $result = $this->db->db_query_str($sql);
        
	if($this->debug) {
	    echo '<pre>'.__FUNCTION__ .' (#' . __LINE__ . ')</pre>'; 
	    echo '<pre>' . $sql . '</pre>'; 
	}
	
        if(!empty($result)) {
            foreach ($result as $row){
                array_push($list, $row['id_ref']);
            }
        }
        
        return $list;
        
    }
    
    public function stockMovementProcess($offer, $id_shop, $time, $type = null, $source = null, $id_ref = null){
        
        if(!isset($offer['id_product']) || !isset($offer['quantity']))
            return;
        
        $id_product = $offer['id_product'];
        $id_product_attribute = isset($offer['id_product_attribute']) ? $offer['id_product_attribute'] : null;
        $quantity = $offer['quantity'];
        
        $qty = array();
        
        if($type == self::update_offer) {
                    
            //check stock movement
            $product_qty = $this->checkStockMovement($id_product, $id_product_attribute, $id_shop, $quantity);
	    
            if($product_qty) {

                // Add to stock
                if($id_stock = $this->addStock($offer, $id_shop, $time)) {
                    
                    //1-3 = -2
                    $usage_qty = ((int)$product_qty - (int)$quantity);
                    $qty['quantity']       = (int)$product_qty ;
                    $qty['balance_qty']    = (int)($quantity);
                    $qty['movement_qty']   = str_replace(array('-','+'), '', trim($usage_qty)); 

                    if($quantity < $product_qty)
                        $action = 'decrease';
                    else
                        $action = 'increase';

                    // Add to stock movement
                    $this->addStockMovement($id_shop, $id_stock, $time, $action, $qty, $type, $source, $id_ref);

                }
            }
                    
        } else if($type == self::import_order) {

            // Add to stock
            if($id_stock = $this->addStock($offer, $id_shop, $time)) {
                
                $qty['quantity']       = (int)$offer['quantity_in_stock'] ;
                $qty['movement_qty']   = $quantity;
                $qty['balance_qty']    = ($source=='FBA') ? (int)$offer['quantity_in_stock'] : ((int)$offer['quantity_in_stock'] - (int)$quantity) ;
                $action = 'decrease';
                
                // Add to stock movement
                $this->addStockMovement($id_shop, $id_stock, $time, $action, $qty, $type, $source, $id_ref);

                // update quantity to products_product and offers_product
                if(isset($source) && $source != 'FBA') {

                    if(isset($id_product_attribute) && $id_product_attribute > 0){

                        // Update product combination offers : qty.
                        $update_offers = array(
                            'quantity' => $qty['balance_qty'],
                        );
                        $where_offers = array(
                            'id_product' => array('operation' => '=', 'value' => (int) $id_product),
                            'id_product_attribute' => array('operation' => '=', 'value' => (int) $id_product_attribute),
                            'id_shop' => array('operation' => '=', 'value' => (int)$id_shop),
                        );

                        $updateProductOptions = $this->db->update_string('product_attribute', $update_offers, $where_offers);
                        //$updateProductOptions .= $this->db->update_string(self::product_prefix.'product_attribute', $update_offers, $where_offers);

                    } else {

                        // Update offers : qty.
                        $update_offers = array(
                            'quantity' => $qty['balance_qty'],
                        );
                        $where_offers = array(
                           'id_product' => array('operation' => '=', 'value' => (int) $id_product),
                           'id_shop' => array('operation' => '=', 'value' => (int)$id_shop),
                       );

                        $updateProductOptions = $this->db->update_string('product', $update_offers, $where_offers);
                        //$updateProductOptions .= $this->db->update_string(AmazonDatabase::product_prefix.'product', $update_offers, $where_offers);
                    }

                    $this->db->db_exec($updateProductOptions);
                }
            }

        } else {
	     //check stock movement
            $product_qty = $this->checkStockMovement($id_product, $id_product_attribute, $id_shop, $quantity);

            if($product_qty) {

                // Add to stock
                if($id_stock = $this->addStock($offer, $id_shop, $time)) {
                    
                    //1-3 = -2
                    $usage_qty = ((int)$product_qty - (int)$quantity);
                    $qty['quantity']       = (int)$product_qty ;
                    $qty['balance_qty']    = (int)($quantity);
                    $qty['movement_qty']   = str_replace(array('-','+'), '', trim($usage_qty)); 

                    if($quantity < $product_qty)
                        $action = 'decrease';
                    else
                        $action = 'increase';

                    // Add to stock movement
                    $this->addStockMovement($id_shop, $id_stock, $time, $action, $qty, $type, $source, $id_ref);

                }
            }
	}
    }

    private function addStockMovement($id_shop, $id_stock, $time, $action, $quantity = array(), $type = null, $source = null, $id_ref = null){
        
        $data = array();
        $data['id_stock']       = $id_stock;
        $data['id_shop']        = $id_shop;
        $data['action']         = $action;
        $data['type']           = $type;
        $data['source']         = $source;
        $data['id_ref']         = $id_ref;
        $data['quantity']       = isset($quantity['quantity']) ? $quantity['quantity'] : 0;
        $data['movement_qty']      = isset($quantity['movement_qty']) ? $quantity['movement_qty'] : 0;
        $data['balance_qty']    = isset($quantity['balance_qty']) ? $quantity['balance_qty'] : 0;
        $data['date_add']       = $time;
         
        $sql = $this->db->insert_string('stock_mvt', $data);
        
        if($this->db->db_exec($sql))
            return true;

        return false;
    }

    private function addStock($offer, $id_shop, $time){
        
        $data = array();
        $data['id_shop']               = (int)$id_shop;
        $data['id_product']            = (int)$offer['id_product'];
        $data['id_product_attribute']  = isset($offer['id_product_attribute']) ? (int)$offer['id_product_attribute'] : null ;
        $data['reference']             = isset($offer['reference']) ? $offer['reference'] : null ;
        $data['sku']                   = isset($offer['sku']) ? $offer['sku'] : null ;
        $data['ean13']                 = isset($offer['ean13']) ? $offer['ean13'] : null;
        $data['upc']                   = isset($offer['upc']) ? $offer['upc'] : null;
        $data['date_add']              = $time;
      
        $sql =  "SELECT id_stock FROM {$this->db->prefix_table}stock
                WHERE id_product = ".(int)$data['id_product']."
                AND id_product_attribute = ".(int)$data['id_product_attribute']."
                AND id_shop = ".(int)$id_shop." ; " ;
        
        $result = $this->db->db_query_str($sql);
        
        if(isset($result[0]['id_stock']) && !empty($result[0]['id_stock'])) {
            
            return (int)$result[0]['id_stock'];
            
        } else {
            
            $sql = $this->db->insert_string('stock', $data);

            if($this->db->db_exec($sql, false, false)) {
                return $this->db->db_last_insert_rowid() ;
            }
        }

        return false;
    }
    
    private function checkStockMovement($id_product, $id_product_attribute, $id_shop, $quantity){
         
        if(isset($id_product_attribute) && !empty($id_product_attribute)) {
            $sql =  "SELECT CASE WHEN pa.quantity IS NOT NULL THEN pa.quantity ELSE p.quantity END AS quantity 
                    FROM {$this->db->prefix_table}product p 
                    LEFT JOIN {$this->db->prefix_table}product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                    WHERE p.id_product = ".(int)$id_product." AND pa.id_product_attribute = ".(int)$id_product_attribute." AND p.id_shop = ".(int)$id_shop." ; " ;
                    
        } else {
            $sql =  "SELECT quantity FROM {$this->db->prefix_table}product WHERE id_product = ".(int)$id_product." AND id_shop = ".(int)$id_shop." ;" ;
        }
        
        $result = $this->db->db_query_str($sql);
            
        if(isset($result[0]) && !empty($result[0])) {
            
            $product_qty = $result[0]['quantity'];
            
            if($quantity != $product_qty)
                return $product_qty;
        }
                
        return false;
        
    }
    
    public function checkOrderInStockMvt($id_order, $id_product = null, $id_product_attribute = null){
        
        $where = "";
        $sql = "SELECT smvt.date_add as date_add FROM {$this->db->prefix_table}stock_mvt smvt " ;
        
        if(isset($id_product) && !empty($id_product)){
            $sql .= " LEFT JOIN {$this->db->prefix_table}stock st ON smvt.id_stock = st.id_stock AND smvt.id_shop = st.id_shop ";
            $where = " AND st.id_product = ".(int)$id_product." ";
            
            if(isset($id_product_attribute) && !empty($id_product_attribute)){
            	$where .= " AND st.id_product_attribute = ".(int)$id_product_attribute."  ";
            }
        }
        
        $sql .= " WHERE smvt.type = '".StockMovement::import_order."' AND smvt.id_ref = ".(int)$id_order." " . $where ." ;";
	
        if($this->debug) {
	    echo '<pre>'.__FUNCTION__ .' (#' . __LINE__ . ')</pre>'; 
	    echo '<pre>' . $sql . '</pre>'; 
	}
        
        $result = $this->db->db_query_str($sql);
	
        if(isset($result[0]['date_add']) && $result[0]['date_add']) {
            return $result[0]['date_add'];
        }
                
        return false;
    }
    
    public function getstockCartReport($id_shop, $start = null, $limit = null, $search = null, $numrow = false){
        
	// Amazon
	require_once(dirname(__FILE__) . '/../../Amazon/classes/amazon.order.php');
	
	$amazon_order_sql_1 = '';
	
	$amzon_id = Amazon_Order::$MarketplaceID;
	$ebay_id = 3;
	$exclude_from_order =  '0, ' . $amzon_id;
	
	// get amazon order list
	if($this->db->db_table_exists(Amazon_Order::$m_pref."amazon_orders", true)){
	    
	    $amazon_order_sql_1 = " UNION " ;
	    $amazon_order_sql_1 .= "SELECT "
		    . " '2' as id_marketplace, "
		    . " amoi.sku as reference, "
		    . " amoi.id_product as id_product, "
		    . " amoi.id_product_attribute as id_product_attribute,"
		    . " amo.id_country as site,"
		    . " amo.mp_order_id as id_marketplace_order_ref,"
		    . " amoi.quantity as quantity,"
		    . " amo.order_date as order_date,"
		    . " amoi.date_upd as date_upd"
	    . " FROM ".Amazon_Order::$m_pref."amazon_orders amo "
	    . "LEFT JOIN ".Amazon_Order::$m_pref."amazon_orders_items amoi ON amoi.mp_order_id = amo.mp_order_id AND amoi.id_shop = amo.id_shop AND amoi.id_country=amo.id_country "
	    . "WHERE amo.id_shop = " . (int) $id_shop . " "
	    . "AND amoi.id_shop = " . (int) $id_shop . " ";

	    $amazon_order_sql_1 .= "AND amo.mp_status = '" . Amazon_Order::PENDING . "' AND mp_channel = 'MFN' ";

	    if(isset($search) && !empty($search)){
		$amazon_order_sql_1 .= "AND ( "
		    . "amo.mp_order_id like '%" . $search . "%' OR "
		    . "amoi.mp_order_id like '%" . $search . "%' OR "
		    . "amoi.sku like '%" . $search . "%' OR "
		    . "amoi.id_product like '%" . $search . "%' OR "
		    . "amoi.id_product_attribute like '%" . $search . "%' OR "
		    . "amoi.quantity like '%" . $search . "%' OR "
		    . "amo.price like '%" . $search . "%' ";
		$amazon_order_sql_1 .=  ") ";
	    }       

	}
	
	// get eBat site
	require_once(dirname(__FILE__) . '/../../UserInfo/configuration.php');
	$UserInfo = new UserConfiguration();
	
	$eBay_site = $UserInfo->getEbaySiteDictionary();
	
        // get order list
        $sql = "SELECT "
		    . "o.id_marketplace as id_marketplace, "
		    . "oi.reference as reference, "
		    . "oi.id_product as id_product, "
		    . "oi.id_product_attribute as id_product_attribute,"
		    . "o.site as site,"
		    . "o.id_marketplace_order_ref as id_marketplace_order_ref,"
		    . "oi.quantity as quantity,"
		    . "o.order_date as order_date,"
		    . "o.date_add as date_upd"
		. " FROM ". self::order_prefix . "orders o "
                . "LEFT JOIN ". self::order_prefix . "order_items oi ON oi.id_orders = o.id_orders AND oi.id_shop = o.id_shop "
                . "WHERE oi.id_shop = ".(int)$id_shop." AND o.id_shop = ".(int)$id_shop." AND (oi.reference IS NOT NULL AND oi.reference != '' ) "
                . "AND CURDATE() - date(o.order_date) IN (0, 1, 2, 3, 4) "
		. "AND o.id_marketplace NOT IN ($exclude_from_order) " ;
		    
	// eBay Status = active(Pending)
	$sql .=  "AND CASE WHEN o.id_marketplace = $ebay_id THEN o.order_status = 'active' END ";
	$sql .=  "AND CASE WHEN o.id_marketplace = $ebay_id THEN o.payment_method NOT LIKE 'None' END ";
                
        if(isset($search) && !empty($search)){
	     $sql .= "AND ( "
		. "o.id_marketplace_order_ref like '%" . $search . "%' OR "
		. "oi.reference like '%" . $search . "%' OR "
		. "oi.id_product like '%" . $search . "%' OR "
		. "oi.id_product_attribute like '%" . $search . "%' OR "
		. "oi.quantity like '%" . $search . "%' OR "
		. "o.total_amount like '%" . $search . "%' ";
            $sql .=  ") ";
	    
        }
	$sql .= $amazon_order_sql_1;
	
	$sql .= "ORDER BY date_upd DESC, id_product ASC, id_product_attribute ASC ";

        if(isset($start) && isset($limit)){
            $sql .= "LIMIT $limit OFFSET $start ";
        }
	
        $sql .= " ; " ;
	
	if($numrow) {
	    
	    $result =  $this->db->db_sqlit_query($sql);
            $row =  $this->db->db_num_rows($result);
            return $row;
	    
	} else {
	    
	    require_once(dirname(__FILE__) . '/../../Marketplaces/classes/marketplaces.tools.php');
	    
	    $result = $this->db->db_query_str($sql);
	    $orders = array();
	    
	    foreach ($result as $key => $order) {
		
		$id =$key;
		
		if($order['id_marketplace'] == $ebay_id){
		    
		    $country_name = isset($eBay_site[$order['site']]['name']) ? ' - ' . $eBay_site[$order['site']]['name'] : '';
		    
		} else {
		    
		    $country = MarketplacesTools::getCountry($order['site']);
		    $country_name = isset($country['name']) ? ' - ' . $country['name'] : '';
		}
		
		$text = '';
                $text .= ' (' .$order['id_product'];
                $text .= isset($order['id_product_attribute']) ? '/' . $order['id_product_attribute'] .')' : ')';
		
                if(isset( $order['reference']))  
		{
		    $orders[$id]['reference'] = 'Reference : ' . $order['reference'] . $text;
		} else {
		    $orders[$id]['reference'] = $text;
		}
		
		$orders[$id]['marketplace'] = MarketplacesTools::getMarketplaceByID($order['id_marketplace']). $country_name;
		$orders[$id]['sku'] = $order['reference'];
		$orders[$id]['id_product'] = $order['id_product'];
		$orders[$id]['id_product_attribute'] = $order['id_product_attribute'];
		//$orders[$id]['site'] = isset($country['name']) ? $country['name'] : $order['site'];
		$orders[$id]['detail'] = $order['id_marketplace_order_ref'];
		$orders[$id]['id_marketplace_order_ref'] = $order['id_marketplace_order_ref'];
		$orders[$id]['quantity'] = $order['quantity'];
		$orders[$id]['order_date'] = date( 'Y-m-d H:i:s' , strtotime($order['order_date']));
		$orders[$id]['date_upd'] = date( 'Y-m-d H:i:s' , strtotime($order['date_upd']));
	    }
	    
	    return $orders;
	}
	
    }
    
}