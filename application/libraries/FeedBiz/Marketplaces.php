<?php

require_once(dirname(__FILE__).'/../../../libraries/ci_db_connect.php');
require_once(dirname(__FILE__).'/../../../application/libraries/Amazon/classes/amazon.userdata.php');

class Marketplaces {  
    
    const GENERAL = 'General';
    const AMAZON = 'Amazon';
    const EBAY = 'eBay';
    const ID_GENERAL = 6;


    public function __construct() {
         $this->db = new ci_db_connect();
    }
    
    public function configuration($id_user) {
        
        $data = array();
   
        $offerPackage = $this->getMarketplaceOffer();
        $offerUserPackage = $this->getUserPackageID($id_user, true);
        
        if ($offerPackage) {
            foreach ($offerPackage as $field) {
				
                if ($field['submenu_offer_pkg'] == 1) {
                    $region = $this->getMarketplaceRegion();

                    foreach ($region as $reg) {
                        $subOfferQuery = $this->getSubMarketplaceOffer($field['id_offer_pkg'], $reg['id_region']);
			
                        if (isset($subOfferQuery) && !empty($subOfferQuery) && $subOfferQuery != '') {

                            foreach ($subOfferQuery as $key => $field2) {

				if(!isset($offerUserPackage[$field2['id_offer_pkg']][$field2['id_offer_sub_pkg']]))
				    continue;
				
				if(!isset($field2['iso_code']))
				    continue;
				
                                if($field['name_offer_pkg'] != self::GENERAL){
				    $data[$field['name_offer_pkg']][$field2['iso_code']]['title'] = $field2['title_offer_sub_pkg'];
				    $data[$field['name_offer_pkg']][$field2['iso_code']]['currency'] = $field2['currency_offer_price_pkg'];
				    $data[$field['name_offer_pkg']][$field2['iso_code']]['ext'] = $field2['ext_offer_sub_pkg'];
				    $data[$field['name_offer_pkg']][$field2['iso_code']]['domain'] = $field2['domain_offer_sub_pkg'];
				    $data[$field['name_offer_pkg']][$field2['iso_code']]['id_country'] = (int) $field2['id_offer_sub_pkg'];
				    
				    if($field['name_offer_pkg'] == Marketplaces::AMAZON){
					$fba = AmazonUserData::getFBA($id_user, $field2['ext_offer_sub_pkg']);					
					
					if(isset($fba) && !empty($fba)) {
					    
					    $data[$field['name_offer_pkg']][$field2['iso_code']]['fba:active'] = 'true';
					    
					    if(isset($fba['fba_multichannel']) && $fba['fba_multichannel'])
						$data[$field['name_offer_pkg']][$field2['iso_code']]['fba:multichannel'] = 'true';
					    else
						$data[$field['name_offer_pkg']][$field2['iso_code']]['fba:multichannel'] = 'false';
						
					    if(isset($fba['fba_master_platform']) && $fba['fba_master_platform'])
						$data[$field['name_offer_pkg']][$field2['iso_code']]['fba:is_master_platform'] = 'true';
					   
					}
				    }
                                } 
				else {
                                    $data[self::GENERAL][$field2['comments']][$field2['iso_code']]['title'] = $field2['title_offer_sub_pkg'];
				    $data[self::GENERAL][$field2['comments']][$field2['iso_code']]['currency'] = $field2['currency_offer_price_pkg'];
				    $data[self::GENERAL][$field2['comments']][$field2['iso_code']]['ext'] = $field2['ext_offer_sub_pkg'];
				    $data[self::GENERAL][$field2['comments']][$field2['iso_code']]['domain'] = $field2['domain_offer_sub_pkg'];
				    $data[self::GENERAL][$field2['comments']][$field2['iso_code']]['id_country'] = $field2['id_offer_sub_pkg'];
                                }
                            }
                        }
                    }
                }
            }
        }       
        
	return $data;
    }

    public function messaging($user) {
        require_once(dirname(__FILE__) . '/Messagings.php');
        $data = array();
        $messagings = new Messagings($user);
        $reply_template = $messagings->get_messagings(null, 0, null, 'reply_template');
        foreach ($reply_template as $marketplace => $templates){
            foreach ($templates as $region => $template){
                $data[$region .':active'] = $template['reply_template_active'] ? 'true' : 'false';
                $data[$region .':id_lang'] = $template['id_lang'];
            }
        }
	return $data;
    }

    public function getMarketplaceOffer($name = null)
    {
        $sql = "SELECT * FROM offer_packages ";
        if(isset($name) && !empty($name)) {
            $name = $this->db->escape_str($name);
             $sql .="WHERE name_offer_pkg = '$name' ";
        } else {
            $sql .=" WHERE is_marketplace = 1 AND status_offer_pkg = 'active' ";
        }
        $sql .= "ORDER BY sort_offer_pkg ASC";
        
	$query = $this->db->select_query($sql);
        $data = array();
        
        while($row = $this->db->fetch($query)){ 
	    $data[$row['id_offer_pkg']]['id_offer_pkg'] = $row['id_offer_pkg'];
	    $data[$row['id_offer_pkg']]['name_offer_pkg'] = $row['name_offer_pkg'];
	    $data[$row['id_offer_pkg']]['sort_offer_pkg'] = $row['sort_offer_pkg'];
	    $data[$row['id_offer_pkg']]['submenu_offer_pkg'] = $row['submenu_offer_pkg'];
	    $data[$row['id_offer_pkg']]['status_offer_pkg'] = $row['status_offer_pkg'];
	    $data[$row['id_offer_pkg']]['is_marketplace'] = $row['is_marketplace'];
        }
	
        return $data;
    }
    
    public function getUserPackageID($id_user,$sub=false)
    {
        $id_user = (int)$id_user;
        $sql = "SELECT 
		    user_package_details.id_package as id_package,
		    offer_price_packages.id_offer_pkg as id_offer_pkg,
		    offer_price_packages.id_region as id_region,
		    offer_price_packages.id_offer_sub_pkg as id_offer_sub_pkg 
		FROM user_package_details 
		LEFT OUTER JOIN offer_price_packages ON (user_package_details.id_package = offer_price_packages.id_offer_price_pkg)
		WHERE offer_price_packages.offer_pkg_type <> '3' AND user_package_details.id_users = '".$id_user."' " ;
		
        if(!$sub) {
            $sql .= "AND offer_price_packages.id_offer_sub_pkg IS NULL ";
	} 
	
        $sql .= "ORDER BY offer_price_packages.id_region ASC,offer_price_packages.id_offer_pkg ASC,offer_price_packages.id_offer_sub_pkg ASC";
	$query = $this->db->select_query($sql);
        $data = array();
       
        while($row = $this->db->fetch($query)){ 
	    $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['id_package'] = $row['id_package'];
	    $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['id_offer_pkg'] = $row['id_offer_pkg'];
	    $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['id_region'] = $row['id_region'];
	    $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['id_offer_sub_pkg'] = $row['id_offer_sub_pkg'];
        }
	
        return $data;
    }
    
    public function getMarketplaceRegion($id_region = null)
    {
        $sql = "SELECT * FROM region ";
	
        if(isset($id_region) && !empty($id_region)){
            $sql .= "WHERE id_region = '$id_region' ";
	}
        
        $sql .= "ORDER BY name_region ASC";
        
	$query = $this->db->select_query($sql);
        $data = array();
        
        while($row = $this->db->fetch($query)){ 
	    $data[$row['id_region']]['id_region'] = $row['id_region'];
	    $data[$row['id_region']]['name_region'] = $row['name_region'];
	    $data[$row['id_region']]['map_x_region'] = $row['map_x_region'];
	    $data[$row['id_region']]['map_y_region'] = $row['map_y_region'];
	    $data[$row['id_region']]['status_region'] = $row['status_region'];
	    $data[$row['id_region']]['sort_by'] = $row['sort_by'];
        }
	
        return $data;
    }
    
    public function getSubMarketplaceOffer($id_offer_pkg, $id_region = null)
    {
        $region='';
        $id_offer_pkg = (int)$id_offer_pkg;
        if( isset($id_region) && $id_region != null ){
            $id_region = $this->db->escape_str($id_region);
            $region = " AND opp.id_region = '".$id_region."' ";
        }
        
        $sql = "SELECT opp.*, sub.*,sub.iso_code as pkg_iso_code FROM offer_sub_packages sub ";
        $sql .= "LEFT JOIN offer_price_packages opp ON (sub.id_offer_sub_pkg = opp.id_offer_sub_pkg) and opp.offer_pkg_type != '3'";
        $sql .= "WHERE opp.id_offer_pkg = '{$id_offer_pkg}' ". $region ." AND sub.sort_offer_sub_pkg != 0 AND opp.status_offer_price_pkg = 'active' ";
         
        $query = $this->db->select_query($sql);
        $data = array();
	
        while($row = $this->db->fetch($query)){ 
	    $data[$row['id_offer_sub_pkg']]['id_offer_sub_pkg'] = $row['id_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['sort_offer_sub_pkg'] = $row['sort_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['title_offer_sub_pkg'] = $row['title_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['value_offer_sub_pkg'] = $row['value_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['icon_offer_sub_pkg'] = $row['icon_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['input_offer_sub_pkg'] = $row['input_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['status_offer_sub_pkg'] = $row['status_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['iso_code'] = $row['pkg_iso_code'];
	    $data[$row['id_offer_sub_pkg']]['id_offer_price_pkg'] = $row['id_offer_price_pkg'];
	    $data[$row['id_offer_sub_pkg']]['id_region'] = $row['id_region'];
	    $data[$row['id_offer_sub_pkg']]['id_offer_pkg'] = $row['id_offer_pkg'];
	    $data[$row['id_offer_sub_pkg']]['amt_offer_price_pkg'] = $row['amt_offer_price_pkg'];
	    $data[$row['id_offer_sub_pkg']]['currency_offer_price_pkg'] = $row['currency_offer_price_pkg'];
	    $data[$row['id_offer_sub_pkg']]['priod_offer_price_pkg'] = $row['priod_offer_price_pkg'];
	    $data[$row['id_offer_sub_pkg']]['element_offer_sub_pkg'] = $row['element_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['ext_offer_sub_pkg'] = $row['ext_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['domain_offer_sub_pkg'] = $row['domain_offer_sub_pkg'];
	    $data[$row['id_offer_sub_pkg']]['status_offer_price_pkg'] = $row['status_offer_price_pkg'];
	    $data[$row['id_offer_sub_pkg']]['offer_pkg_type'] = $row['offer_pkg_type'];
	    $data[$row['id_offer_sub_pkg']]['comments'] = $row['comments'];
	    $data[$row['id_offer_sub_pkg']]['id_site_ebay'] = $row['id_site_ebay'];
	    $data[$row['id_offer_sub_pkg']]['sub_marketplace'] = $row['sub_marketplace'];
        }
	
        return $data;
    }
    
    public static function getCountries()
    {
	$countries = array();
	$db = new ci_db_connect();
	
        $sql = "SELECT * FROM offer_sub_packages ;";               
	$query = $db->select_query($sql);
        
        while($row = $db->fetch($query)){ 
	    $countries[$row['id_offer_sub_pkg']] = $row;
        }
	
	return $countries;
    }
    
    public static function getIdCountryByIso($iso_code)
    {
	$db = new ci_db_connect();
	
        $sql = "SELECT * FROM offer_sub_packages WHERE iso_code = '$iso_code' ;";               
	$query = $db->select_query($sql);
        
        while($row = $db->fetch($query)){ 
	    return $row['id_offer_sub_pkg'];	    
        }
	
    }
    
    public static function getExtCountryByIso($iso_code)
    {
	$db = new ci_db_connect();
	
        $sql = "SELECT * FROM offer_sub_packages WHERE iso_code = '$iso_code' ;";               
	$query = $db->select_query($sql);
        
        while($row = $db->fetch($query)){ 
	    return $row['id_offer_sub_pkg'];	    
        }
	
    }

    public static function extToId($ext) {

        $ext = ltrim($ext, ".");
        switch ($ext) {
            case 'co.uk' :
                return ('uk');
            case 'com' :
                return ('us');
            case 'mx' :
            case 'com_mx' :
            case 'com.mx' :
                return ('mx');
            default:
                return ($ext);
        }
    }

    public static function getDomains() {

        $mysql_db = new ci_db_connect();
        $sql = "SELECT * FROM offer_price_packages WHERE status_offer_price_pkg = 'active';";

        $query = $mysql_db->select_query($sql);
        $data = array();

        while($row = $mysql_db->fetch($query)){
	    $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['id_region'] = $row['id_region'];
	    $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['ext_offer_sub_pkg'] = $row['ext_offer_sub_pkg'];
	    $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['domain_offer_sub_pkg'] = $row['domain_offer_sub_pkg'];
        }

        return $data;
    }

    public static function get_marketplaces() {

	$name = array();
	$mysql_db = new ci_db_connect();
        $query = $mysql_db->select_query("SELECT * FROM offer_packages WHERE is_marketplace = 1 ;" );

        while($row = $mysql_db->fetch($query)){
	    $name[$row['id_offer_pkg']]['name'] = $row['name_offer_pkg'];

            if($sub_marketplaces = self::get_sub_marketplaces($row['id_offer_pkg'])){
                foreach ($sub_marketplaces as $id_sub_marketplace => $sub_marketplace){
                    $name[$row['id_offer_pkg']]['submarketplace'][$id_sub_marketplace] = $sub_marketplace;
                }
            }
	}
      
        return $name;
    }

    public static function get_sub_marketplaces($id_marketplace) {

	$name = array();
	$mysql_db = new ci_db_connect();
        $query = $mysql_db->select_query("SELECT * FROM offer_sub_marketplace WHERE id_marketplace = $id_marketplace ;" );

        while($row = $mysql_db->fetch($query)){
	    $name[$row['id_offer_sub_marketplace']] = $row['name_marketplace'];
	}

        return $name;
    }
}
