<?php

class Eucurrency
{
    private $xmlUrl = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
    private $xml;
    public $eurCurrency = array();
    
    public function __construct()
    {
        try
        {		    
		$filename = dirname(__FILE__) . '/../../assets/apps/fifo_dir/eurofxref-daily.xml';
		
		$this->xml = @simplexml_load_file($this->xmlUrl);

		if(!empty($this->xml))
		{
			file_put_contents($filename, $this->xml->saveXML());

		} else {

			$this->xml = @file_get_contents($filename);
			$this->xml = new SimpleXMLElement($this->xml);
		}		

		foreach($this->xml->Cube->Cube->Cube as $rate)
		{
			$this->eurCurrency[trim($rate['currency'])] = trim($rate['rate']);
		}   
	    
        }
	catch(Exception $ex)
        {
		echo $ex->getMessage();
        }
	
    }
    
    public function doConvert($amount,$currency)
    {
        if($currency=='EUR') return $amount;
        return $amount * $this->eurCurrency[$currency];
    }
    
    public function doDiffConvert($amount,$from_currency='EUR',$to_currency='EUR')
    {
        $from_currency = strtoupper($from_currency);
        $to_currency = strtoupper($to_currency);
        if($from_currency=='EUR' && $to_currency=='EUR')
            return $amount;
        if((!isset($this->eurCurrency[$from_currency]) && $from_currency !='EUR') || (!isset($this->eurCurrency[$to_currency]) && $to_currency !='EUR') )
            return $amount; 
        $eur_temp = $from_currency == 'EUR' ? $amount : ($amount/$this->eurCurrency[$from_currency]);
        return  $to_currency == 'EUR' ? $eur_temp :($eur_temp * $this->eurCurrency[$to_currency]);
    }
}