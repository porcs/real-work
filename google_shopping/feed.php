<?php if( !defined('BASEPATH') ) define('BASEPATH', '');

chdir(dirname(__FILE__));

$test = isset($_GET['test']) ? (bool) $_GET['test'] : false ;

if($test) {
    require_once(dirname(__FILE__) . '/../application/libraries/GoogleShopping/functions/google.feed-praew-test.php');
} else {
    require_once(dirname(__FILE__) . '/../application/libraries/GoogleShopping/functions/google.feed.php');
}
require_once(dirname(__FILE__) . '/../application/libraries/UserInfo/configuration.php');

$debug = isset($_GET['debug']) ? (bool) $_GET['debug'] : false ;
$token = $_GET['token'];

$export_file = isset($_GET['export_file']) ? true : false ;

if($export_file){
    $file = "google_merchant_feed.xml";
    $quoted = sprintf('"%s"', addcslashes(basename($file), '"\\'));
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . $quoted);
    header('Content-Transfer-Encoding: binary');
    header('Connection: Keep-Alive');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public'); 
}

if(!empty($token)) 
{
    $data = explode('|', base64_decode(($token)));

    if(!empty($data))
    {
	if(isset($data[0]) && isset($data[1]) && isset($data[2]) && isset($data[3]) && isset($data[4]))
	{
            $user_id = $data[0];
            $user_code = $data[1];
            $id_shop = $data[2];
            $id_country = $data[3];
            $id_marketplace = $data[4];

	    // find user data
	    $UserInfo = new UserConfiguration();
            $user_data = $UserInfo->getUserById($user_id);

            // don't debug, it can hack.
            /*if($debug){
                echo ' --- Params ---- ';
                var_dump(array(
                    'user_code' => $user_code,
                    'id_customer' => $user_id,
                    'id_shop' => $id_shop,
                    'id_country' => $id_country,
                    'id_marketplace' => $id_marketplace)
                    );
            }*/

            if(isset($user_data['user_code']) && md5($user_data['user_code']) == $user_code) {

                $user_name = $user_data['user_name'];
                $id_customer = $user_data['id_customer'];

                /*if($debug){
                    var_dump(array('user_name' => $user_name, 'user_code' => $user_data['user_code']));
                }*/

                $google_feed = new GoogleFeed($user_name, $debug, $export_file);
                $google_feed->getXmlFeedByArray($id_shop, $id_country, $id_customer);
            }
	    
	}
    }
}
