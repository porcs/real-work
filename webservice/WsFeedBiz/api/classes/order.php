<?php

use Swagger\Annotations as SWG;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="http://localhost/WsFeedBiz/api",
 *   resourcePath="/WsFeedBiz",
 *   description="Operations from Feedbiz",
 *   produces="['application/json','application/xml']"
 * )
 * 
 */
class Order extends Entity {
    
    /**
     * Initialize arguments and allowed operations/methods
     * for this class
     * @param array $arguments
     */
    public function __construct() {
        
        parent::__construct();
        
	Entity::$_allowedCDATAs = true;
		
        $this->_allowedOperations = array(
                                        'getOrders'=>array('GET'),
                                        'setOrders'=>array('POST'),
                                        'updateOrder'=>array('GET'),
                                        'cancelOrder'=>array('GET')
                                    );
    }
    
    /** @SWG\Api(
     *   path="/Order/getOrders",
     *   @SWG\Operation(
     *     method="GET",
     *     summary="Get current active orders",
     *     notes="Returns orders (if found)",
     *     type="string",
     *     nickname="getOrders",
     *     @SWG\Parameter(
     *        name="login",
     *        description="Username of the credentials to be authenticated",
     *        paramType="header",
     *        required=true,
     *        type="string"
     *     ),
     *     @SWG\Parameter(
     *        name="token",
     *        description="token of the credentials to be authenticated",
     *        paramType="header",
     *        required=true,
     *        type="string"
     *     )
     *   )
     * )
     * @return array
     */
    public function getOrders( $user, $id_order){
        $feedbiz = $this->getFeedBizInstance($user); // need to first line
        $order = $feedbiz->exportOrderList($user, $id_order);
        $return = array('Orders' => array("return" => $order) );
	
    	if(defined('DEBUG_MODE') && DEBUG_MODE){
            echo 'DIR_SERVER:'.DIR_SERVER.'<br/>';
            echo 'USER:'.$user.'|ORDER:'.$id_order;
            echo '<pre>'.print_r($return, true).'</pre>';
            exit;
        }
        return $this->getSuccess($return);
    }
    
    public function setOrders($user, $orders=null){
        
        $feedbiz = $this->getFeedBizInstance($user); // need to first line
	
	$pass = 'false' ;	
	
	//$orders = new SimpleXMLElement(file_get_contents('D:\project_file\ChaussMoi\order-55131.xml'));
	//$orders = new SimpleXMLElement(urldecode($orders)) ;
        $orders   = simplexml_load_string(urldecode($orders), 'SimpleXMLElement', LIBXML_NOCDATA);
	
	// set to order table
	$order = $feedbiz->setOrder($user, $orders, false);
	
	if(isset($order['pass']) && $order['pass']){
	    
	    // Hook
	    require_once(BASEPATH.'/../libraries/Hooks.php');
	    
	    $hook = new Hooks();
	    $hook->_call_hook('save_orders', array(
		'user_name' => $user,
		'id_shop' => $order['id_shop'],
		'site' => $order['site'], 
		'id_order' => $order['id_order']
	    ));
	    
	    $pass = 'true';
	    $message = 'import success';
	    
	} else {
	    
	    if($orders instanceof SimpleXMLElement) {
		
		// log orders
		$logs[] = array(
		    'batch_id'		=> (string) $orders->References->Id,
		    'request_id'	=> (int) $orders->References->Id,
		    'error_code'	=>  1,
		    'message'		=>  isset($order['output']) ? $order['output'] : 'Import order unsuccessful',
		    'date_add'		=>  date('Y-m-d H:i:s'),
		    'id_shop'		=>  (int) $orders['IdShop'],
		    'id_marketplace'	=>  0,
		    'site'		=> (string) $orders['LanguageCode']
		);
		
		$feedbiz->insertLog($user, $logs);
		$message = isset($order['output']) ? $order['output'] : 'Import order unsuccessful';
	    }
	}
	
	$output = array();
	$output['EndNewVersion'] = true;
	$output['pass'] = $pass;
	$output['output'] = $message;
		
        $return = array('Orders' => array("return" => $output) );
	
    	if(defined('DEBUG_MODE') && DEBUG_MODE){
    		echo 'DIR_SERVER:'.DIR_SERVER.'<br/>';
			echo 'USER:'.$user;
			echo '<pre>'.print_r($return, true).'</pre>';
			exit;
		}
        
        return $this->getSuccess($return);
    }
    
    public function updateOrder($user, $seller_order_id){

        $feedbiz = $this->getFeedBizInstance($user); // need to first line
        $order = $feedbiz->updateOrder($user, $seller_order_id) ;

	$output = array();
	$output['pass'] = $order['pass'];
	$output['output'] = $order['message'];

        $return = array('Orders' => array("return" => $output) );

    	if(defined('DEBUG_MODE') && DEBUG_MODE){
    		echo 'DIR_SERVER:'.DIR_SERVER.'<br/>';
			echo 'USER:'.$user;
			echo '<pre>'.print_r($return, true).'</pre>';
			exit;
		}

        return $this->getSuccess($return);
    }

    public function cancelOrder($user, $seller_order_id=null, $order_id=null, $reason_id=null){

        $feedbiz = $this->getFeedBizInstance($user); // need to first line
	$pass = 'false' ;
        $output = '';
        
        if(isset($seller_order_id) && !empty($seller_order_id)) {

            $seller_order_id = isset($seller_order_id) ? $seller_order_id : null;

            //1. get order by seller id
            $chk_order = $feedbiz->getOrdersBySellerOrderID($user , $seller_order_id, $order_id);

            if(isset($chk_order['id_orders']) && !empty($chk_order['id_orders'])) {

                $id_order = (int) $chk_order['id_orders'];

                //2. get order from db
                $order_list = new Orders($user, $id_order);

                if(isset($order_list->status) && $order_list->status) { //Allow only order which sent to Shop

                    //3. update status to canceled
                    $order_cancelled[] = $id_order;
                    $result = $feedbiz->updateOrderCanceled($user, '', 'url', $order_cancelled, '', false, false, false);

                    //4. find reson key by reason_id
                    //get marketplace by id
                    $marketplace = $feedbiz::get_marketplace_by_id($order_list->id_marketplace);
                    $reasons = $feedbiz::get_order_cancel_reason();
                    $reason = isset($reasons[$marketplace][$reason_id]['reason_key']) ? $reasons[$marketplace][$reason_id]['reason_key'] : null;

                    if(!isset($reason) || empty($reason)) {
                        
                        // log order have no reason
                        $output = 'Seller Order : '.$seller_order_id.' have cancelled with no reason';
                        $request_id = $id_order;
                        $error_code = 1;

                    } else {

                        if($result){
                            
                            $pass = 'true' ;
                            //5. save reson
                            $result = $feedbiz->updateOrderErrorStatus($user, $id_order , 1, $reason); // update reason to comment
                            $output = 'Seller Order : '.$seller_order_id.' have cancelled ['.$reason.']';
                            $request_id = $id_order;
                            $error_code = 0;

                            //5. update cancelled to Marketplace
                            if($result){

                                // Hook
                                require_once(BASEPATH.'/../libraries/Hooks.php');
                                $hook = new Hooks();
                                $hook->_call_hook('cancel_orders', array(
                                    'user_name' => $user,
                                    'id_order' => $id_order,
                                    'id_marketplace' => $order_list->id_marketplace,
                                    'id_shop' => $order_list->id_shop,
                                    'site' => $order_list->site,
                                ));
                            }
                        } else {
                            // log order save reason unsuccessful
                            $output = 'Seller Order : '.$seller_order_id.' save reason unsuccessful';
                            $request_id = $id_order;
                            $error_code = 1;
                        }
                    }
                } else {
                    // log order didn't send to Shop
                    $output = 'Seller Order : '.$seller_order_id.' does not send to Shop';
                    $request_id = $id_order;
                    $error_code = 1;
                }
              
            } else {
                // log order not exist in database
                $output = 'Seller Order : '.$seller_order_id.' does not exist in Feed.biz';
                $error_code = 1;
            }
        }

        // save logs
        $logs[] = array(
            'id_shop'           =>  1,
            'request_id'        =>  isset($request_id) ? $request_id : null,
            'error_code'        =>  $error_code,
            'message'           =>  $output,
            'date_add'          =>  date('Y-m-d H:i:s'),
        );
        $feedbiz->insertLog($user, $logs);
        $message = isset($output) ? $output : 'Cancel Order unsuccessful';
        
        $return = array('Order' => array("return" => array('pass'=>$pass, 'output'=>$message)) );

    	if(defined('DEBUG_MODE') && DEBUG_MODE){
            echo 'DIR_SERVER:'.DIR_SERVER.'<br/>';
            echo 'USER:'.$user;
            echo '<pre>'.print_r($return, true).'</pre>';
            exit;
        }

        return $this->getSuccess($return);
    }
}