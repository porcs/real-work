<?php

abstract class Entity {
    
    public static $_allowedCDATAs  = false;
    
    protected $_allowedOperations  = array();
    protected $_name;
      
    public function __construct() {
       
        $this->_name = get_class($this);
        
    }
    /**
    * Return allowed operations for this class
    * @return type
    */
    public function getAllowedOperations(){
        return $this->_allowedOperations;
    }
    
    protected function getError($code, $msg){	
        /*return array('Response' => array($this->_name =>
                                                array("return" => $msg)
                                        ),
                     'status' => $code
                   );*/
        return array('Response' => $msg, 'status' => $code);
    }
    
    protected function getSuccess($return){
        /*return array('Response' => array($this->_name =>
                                                     array("return" => $return),
                                            ),
                     'status' => 200
                   );*/
        return array('Response' => $return, 'status' => 200);
    }    
    
    protected function getStockMovementInstance($user){

        require_once(dirname(__FILE__) . '/../../../../application/libraries/db.php');
        require_once(dirname(__FILE__) . '/../../../../application/libraries/FeedBiz/stock/StockMovement.php');
        
        $stock_movement = new StockMovement($user);
        
        return $stock_movement;
    }    

    protected function getFeedBizInstance($user){

        require_once(dirname(__FILE__) . '/../../../../application/libraries/FeedBiz_Orders.php');

        $feedbiz = new FeedBiz_Orders(array($user));

        return $feedbiz;
    }
    
    protected function getMarketplaceInstance(){
        
        require_once(dirname(__FILE__) . '/../../../../application/libraries/FeedBiz/Marketplaces.php');
        
        $marketplace = new Marketplaces();
        
        return $marketplace;
    }    
    
    protected function getOffersOptionsInstance($user, $id_marketplace=null, $id_country=null){
	
        require_once(dirname(__FILE__) . '/../../../../application/libraries/FeedBiz/config/products.php');
        
        $MarketplaceProductOption = new MarketplaceProductOption($user, null, null, null, null, $id_country, $id_marketplace);
        
        return $MarketplaceProductOption;
    }   
    
    protected function getCustomerMessagesInstance($user, $debug=false){

        require_once(dirname(__FILE__) . '/../../../../application/libraries/FeedBiz/Messagings.php');

        $messagings = new Messagings($user, $debug);

        return $messagings;
    }

    protected function getDomainsInstance(){

        return file_get_contents(dirname(__FILE__) . '/../../../../assets/apps/users/json/feedbiz_domains.json');

    }

    protected function getAmazonDbInstance($user, $debug=false){

        require_once(dirname(__FILE__) . '/../../../../application/libraries/Amazon/classes/amazon.database.php');
        require_once(dirname(__FILE__) . '/../../../../application/libraries/Amazon/functions/amazon.fba.stock.php');
        $amazonDb = new AmazonDatabase($user, $debug);
        return $amazonDb;

    }

    protected function getAmazonMWSInstance($user, $id_shop, $id_country, $debug=false){
        
        require_once(dirname(__FILE__) . '/../../../../application/libraries/Amazon/classes/amazon.webservice.php');
        
        //3. get user info
        $info = array(
            'user_name' => $user,
            'site' => $id_country,
            'id_shop' => $id_shop,
        );
        $user_info = AmazonUserData::get($info);
        
        $auth = array(
            'MerchantID' => trim($user_info['merchant_id']),
            'AWSAccessKeyID' => trim($user_info['aws_id']),
            'SecretKey' => trim($user_info['secret_key']),
            'lang' => ltrim($user_info['ext'], "."),
        );
        if(isset($user_info['auth_token']) && !empty($user_info['auth_token'])){
            $auth['auth_token'] = trim($user_info['auth_token']);
        }

        $marketPlace = array(
            'Currency' => $user_info['currency'],
            'Country' => ltrim($user_info['ext'], "."),
        );

        // 4. connect Amazon Webservices
        $amazon_mws = new Amazon_WebService($auth, $marketPlace, null, $debug);
        
        return $amazon_mws;

    }
}