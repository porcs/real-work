<?php

use Swagger\Annotations as SWG;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="http://localhost/WsFeedBiz/api",
 *   resourcePath="/WsFeedBiz",
 *   description="Operations from Feedbiz",
 *   produces="['application/json','application/xml']"
 * )
 * 
 */
class CustomerMessages extends Entity {
    
    /**
     * Initialize arguments and allowed operations/methods
     * for this class
     * @param array $arguments
     */
    public function __construct() {
        
        parent::__construct();
        	
        Entity::$_allowedCDATAs = true;
        $this->_allowedOperations = array(
                                        'getCustomerMessages'=>array('GET'),
                                        'getTemplates'=>array('GET')
                                    );
    }    
   
    public function getCustomerMessages($user, $debug=false){
	
        $messaging = $this->getCustomerMessagesInstance($user, $debug);
	
        $customer_messages = $messaging->getCustomerThreadEmail();
	
	$return = array('CustomerMessages' => array("return" => $customer_messages) );
	
    	if(defined('DEBUG_MODE') && DEBUG_MODE){
	    echo 'DIR_SERVER:'.DIR_SERVER.'<br/>';	   
	    echo '<pre>'.print_r($return, true).'</pre>';
	    exit;
	}
        
        return $this->getSuccess($return);
    }

    public function getTemplates($user, $id_lang, $template){

        $messaging = $this->getCustomerMessagesInstance($user);

        switch ($template){
            case 'reply_msg' :
                $template_message = $messaging->overrideCustomerThreadEmail($id_lang);
                break;
        }

        if(isset($template_message) && !empty($template_message)){
            $return = array('Template' => array("return" => $template_message));
        } else {
             $return = array();
        }

    	if(defined('DEBUG_MODE') && DEBUG_MODE){
	    echo 'DIR_SERVER:'.DIR_SERVER.'<br/>';
	    echo '<pre>'.print_r($return, true).'</pre>';
	    exit;
	}

        return $this->getSuccess($return);
    }
    
}