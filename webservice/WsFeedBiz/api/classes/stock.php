<?php

use Swagger\Annotations as SWG;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="http://localhost/WsFeedBiz/api",
 *   resourcePath="/WsFeedBiz",
 *   description="Operations from Feedbiz",
 *   produces="['application/json','application/xml']"
 * )
 * 
 */
class Stock extends Entity {
    
    /**
     * Initialize arguments and allowed operations/methods
     * for this class
     * @param array $arguments
     */
    public function __construct() {
        
        parent::__construct();
        
	Entity::$_allowedCDATAs = true;
		
        $this->_allowedOperations = array(
                                        'getReservedReports'=>array('GET'),
                                        'stockmovementFba'=>array('GET'),
                                    );
    }          

    public function getReservedReports($user, $date_from = null, $date_to = null, $sku = null){
        
        $stockMovement = $this->getStockMovementInstance($user); // need to first line
        $id_shop = 1; // mark to implement (default id_shop)
        $action_type = 'reserved' ; // only reserved product
        $logs = $stockMovement->getstockMovementLogs($id_shop, $date_from, $date_to, $sku, $action_type);

        $return = array('Logs' => $logs);

        return $this->getSuccess($return);
    }

    public function stockmovementFba($user, $id_shop, $id_country){

        $amazonMWS = $this->getAmazonDbInstance($user); // need to first line
        
        $list_sku = $amazonMWS->get_fba_flag($id_shop, $id_country, AmazonFBAStock::PROCESS_KEY, AmazonFBAStock::FBA_STOCK_SYNCH, null, 1);
        $list = array();
        foreach ($list_sku as $k => $sku) {
                $key =  $k+1;
		$list[$key]['fbproduct'] = (int) $sku['id_product'];
		$list[$key]['fbcombination'] = (int) $sku['id_product_attribute'];
		$list[$key]['sku'] = $sku['sku'];
		$list[$key]['fbstock'] = (int) $sku['quantity'];
		$list[$key]['fbstock_fba'] = (int) $sku['quantity_fba'];
		//$list[$key]['fbsold'] = (string)($sku['delta']*-1);
        }
        
        $return = array('ListStock' => $list);

        return $this->getSuccess($return);
    }
}