<?php

use Swagger\Annotations as SWG;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="http://localhost/WsFeedBiz/api",
 *   resourcePath="/WsFeedBiz",
 *   description="Operations from Feedbiz",
 *   produces="['application/json','application/xml']"
 * )
 * 
 */
class OffersOptions extends Entity {
    
    /**
     * Initialize arguments and allowed operations/methods
     * for this class
     * @param array $arguments
     */
    public function __construct() {
        
        parent::__construct();
        	
        $this->_allowedOperations = array(
                                        'getUpdateOffersOptions'=>array('GET')
                                    );
    }    
   
    public function getUpdateOffersOptions($user, $id_marketplace, $id_shop, $id_country){
	
        $OffersOptions = $this->getOffersOptionsInstance($user, $id_marketplace, $id_country);
        
	$debug = false;
	
        $offers_options = $OffersOptions->get_offers_options_to_update_shop($id_shop, $debug);
	
	$return = array('OffersOptions' => array("return" => $offers_options) );
	
    	if(defined('DEBUG_MODE') && DEBUG_MODE){
	    echo 'DIR_SERVER:'.DIR_SERVER.'<br/>';	   
	    echo '<pre>'.print_r($return, true).'</pre>';
	    exit;
	}
        
        return $this->getSuccess($return);
    }
    
    
}