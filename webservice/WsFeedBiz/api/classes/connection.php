<?php

use Swagger\Annotations as SWG;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="http://localhost/WsFeedBiz/api",
 *   resourcePath="/WsFeedBiz",
 *   description="Operations from Feedbiz",
 *   produces="['application/json','application/xml']"
 * )
 * 
 */
class Connection extends Entity {
    
    /**
     * Initialize arguments and allowed operations/methods
     * for this class
     * @param array $arguments
     */
    public function __construct() {
        
        parent::__construct();
        	
        $this->_allowedOperations = array(
                                        'getDomains'=>array('GET')
                                    );
    }
    
    /** @SWG\Api(
     *   path="/Order/getOrders",
     *   @SWG\Operation(
     *     method="GET",
     *     summary="Get current active orders",
     *     notes="Returns orders (if found)",
     *     type="string",
     *     nickname="getOrders",
     *     @SWG\Parameter(
     *        name="login",
     *        description="Username of the credentials to be authenticated",
     *        paramType="header",
     *        required=true,
     *        type="string"
     *     ),
     *     @SWG\Parameter(
     *        name="token",
     *        description="token of the credentials to be authenticated",
     *        paramType="header",
     *        required=true,
     *        type="string"
     *     )
     *   )
     * )
     * @return array
     */
    public function getDomains(){
	
        $domains = $this->getDomainsInstance();
        
	$return = array('Domains' => array("return" => $domains));
	
    	if(defined('DEBUG_MODE') && DEBUG_MODE){
	    echo 'DIR_SERVER:'.DIR_SERVER.'<br/>';	   
	    echo '<pre>'.print_r($return, true).'</pre>';
	    exit;
	}
        
        return $this->getSuccess($return);
    }
    
    
}