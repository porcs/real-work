<?php

use Swagger\Annotations as SWG;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   basePath="http://localhost/WsFeedBiz/api",
 *   resourcePath="/WsFeedBiz",
 *   description="Operations from Feedbiz",
 *   produces="['application/json','application/xml']"
 * )
 * 
 */
class Marketplace extends Entity {
    
    /**
     * Initialize arguments and allowed operations/methods
     * for this class
     * @param array $arguments
     */
    public function __construct() {
        
        parent::__construct();
        	
        $this->_allowedOperations = array(
                                        'getConfigurations'=>array('GET')
                                    );
    }
    
    /** @SWG\Api(
     *   path="/Order/getOrders",
     *   @SWG\Operation(
     *     method="GET",
     *     summary="Get current active orders",
     *     notes="Returns orders (if found)",
     *     type="string",
     *     nickname="getOrders",
     *     @SWG\Parameter(
     *        name="login",
     *        description="Username of the credentials to be authenticated",
     *        paramType="header",
     *        required=true,
     *        type="string"
     *     ),
     *     @SWG\Parameter(
     *        name="token",
     *        description="token of the credentials to be authenticated",
     *        paramType="header",
     *        required=true,
     *        type="string"
     *     )
     *   )
     * )
     * @return array
     */
    public function getConfigurations($user, $id_user){

        $this->getFeedBizInstance($user);
        $marketplace = $this->getMarketplaceInstance();

        $configuration = $marketplace->configuration($id_user);
        $messaging = $marketplace->messaging($user);
        
        $cancel_reasons = FeedBiz_Orders::get_order_cancel_reason();
        $OrderCancelReason = array();
        foreach ($cancel_reasons as $marketplace => $cancel_reason){
            $i = 0;
            foreach ($cancel_reason as $reason){
                $OrderCancelReason[$marketplace]['Reasons:'.$reason['id_reason']]['Item:id'] = $reason['id_reason'];
                $OrderCancelReason[$marketplace]['Reasons:'.$reason['id_reason']]['Item']['Key'] = $reason['reason_key'];
                $OrderCancelReason[$marketplace]['Reasons:'.$reason['id_reason']]['Item']['Text'] = $reason['reason'];
                $i++;
            }
        }
	$return = array('Marketplace' => array("return" => $configuration), 'Messaging' => array("return" => $messaging), 'OrderCancelReason' => array("return" => $OrderCancelReason) );
    	if(defined('DEBUG_MODE') && DEBUG_MODE){
	    echo 'DIR_SERVER:'.DIR_SERVER.'<br/>';	   
	    echo '<pre>'.print_r($return, true).'</pre>';
	    exit;
	}
        
        return $this->getSuccess($return);
    }
    
    
}