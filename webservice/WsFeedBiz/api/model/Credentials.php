<?php 

class Credentials {
    
    private $db;
    private $class;
    private $id;
    private $token;
    private $otp;
    
    private static $otp_require = array(
	'order' => array(
	    'getorders' => 1,
	),
        'connection' => array(
	    'getdomains' => 1,
	)
    );
    
    public function __construct($class_name = null, $arguments = null, $class_method = null)
    {
        $this->db       = new ci_db_connect();
        $this->class    = $class_name;
        $this->id       = isset($arguments['id_order']) ? $arguments['id_order'] : null;
        $this->token    = isset($arguments['token']) ? $arguments['token'] : null;
        $this->otp      = isset($arguments['otp']) ? $arguments['otp'] : null;
	
	if(isset($class_method))
	    $this->class_method    = $class_method;
	
    }
    
    public function getLogin() {	
        return $this->login;
    }
    
    public function getUserID() {	
        return $this->id_user;
    }

    public function setUserID($id) {
        $this->id_user = $id;
    }
    
    public function setLogin($login) {
        $this->login = $login;	 
    }
    
    private static function isExpired($timestamp, $expiration = 3600)
    {
        $now = time() ;
	
        if ( ! $timestamp )
            return(true) ;
        
        if ( $now - $timestamp > $expiration )
            return(true) ;
        
        return(false) ;
    }
    
    private function checkOTP()
    {
        //select otp
        $add = '';
        if(isset($this->id))
            $add  = ' AND request_id = "'.$this->id.'" ';
       
        $sql = 'SELECT token,date_add FROM token WHERE user_code = "' . $this->token . '" AND source = "'.$this->class.'" '.$add.' order by date_add desc  limit 1';

        $result = $this->db->select_query($sql . "; ");
        $row = $this->db->fetch($result);
        
        //check time 
        if(self::isExpired(strtotime($row['date_add']), 3600)) // 15 minute
        {
	    $message = 'Request expired';
	    if(defined('DEBUG_MODE') && DEBUG_MODE){
		    $current = new DateTime();
		    $message .= '[SQL:'.$sql.'|DB:'.$row['date_add'].'|CURRENT:'.$current->format('Y-m-d H:i:s').']';
	    }
            return array('error' => $message);
        }
        else
        {
            //check token
            if($this->otp != md5(md5($row['token'])))
            {
                return array('error' => 'Wrong OTP!');
            }
            else
            {                
                return(true);
            }                
        }
        return false;
    }
    
    public function isAuthorized()
    {
        $otp_result = true;
        if(isset(self::$otp_require[$this->class][$this->class_method]))
	{
            $otp_result = $this->checkOTP();
	}

        if(isset($otp_result['error']) || $otp_result == false)
        {
            return $otp_result;
        }
        else 
        {
            $row = null;
            $sql = 'SELECT id,user_code,user_name FROM users WHERE user_code = "' . $this->token . '"';
            $result = $this->db->select_query($sql);
            while ($row = $this->db->fetch($result))
            {          
                $this->setLogin($row['user_name']);
                $this->setUserID($row['id']);
                return true;
            }      
        }
        
        return array('error' => 'Unauthorized access');
    }
}