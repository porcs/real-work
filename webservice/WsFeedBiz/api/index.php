<?php

define('_API_REQUEST_',true);

require_once 'controllers/apiController.php';

/*
 *
 * *************************************************************
 * * ***********************************************************
 *           ON ERROR, this structure is returned:
 *                          XML
 * <response>
 *      <error></error>
 * </response>
 * 
 * 
 * *************************************************************
 * * ***********************************************************
 *            ON SUCCESS, this struture is returned
 *                           XML
 * GET: for example getting a list of entities (product, orders)
 * <response>
 *      <entity_name>
 *          <property1></property1>
 *          <property2></property2>
 *          <property3></property3>
 *      </entity_name>
 *      <entity_name>
 *          <property1></property1>
 *          <property2></property2>
 *          <property3></property3>
 *      </entity_name>
 * </response>
 */

$api = new ApiController($_SERVER, $_GET, $_POST);
$api->processRequest();