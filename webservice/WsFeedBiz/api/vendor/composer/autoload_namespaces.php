<?php

// autoload_namespaces.php generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Swagger' => $vendorDir . '/zircote/swagger-php/library',
    'Doctrine\\Common\\Lexer\\' => $vendorDir . '/doctrine/lexer/lib',
    'Doctrine\\Common\\Annotations\\' => $vendorDir . '/doctrine/annotations/lib',
);
