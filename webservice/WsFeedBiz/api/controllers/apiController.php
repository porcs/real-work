<?php

require_once dirname(__FILE__).'/../../../../libraries/ci_db_connect.php';
require_once dirname(__FILE__).'/../model/Credentials.php';
require_once dirname(__FILE__).'/../classes/entity.php';

class ApiController {
    
    private $_method;
    private $_entity;
    private $_entity_operation;
    private $_arguments;
    private $_req_server;
    private $_req_get;
    private $_req_post;
    
    private $_http_status_codes = array(
                                    100 => 'HTTP/1.1 100 Continue',
                                    101 => 'HTTP/1.1 101 Switching Protocols',
                                    200 => 'HTTP/1.1 200 OK',
                                    201 => 'HTTP/1.1 201 Created',
                                    202 => 'HTTP/1.1 202 Accepted',
                                    203 => 'HTTP/1.1 203 Non-Authoritative Information',
                                    204 => 'HTTP/1.1 204 No Content',
                                    205 => 'HTTP/1.1 205 Reset Content',
                                    206 => 'HTTP/1.1 206 Partial Content',
                                    300 => 'HTTP/1.1 300 Multiple Choices',
                                    301 => 'HTTP/1.1 301 Moved Permanently',
                                    302 => 'HTTP/1.1 302 Found',
                                    303 => 'HTTP/1.1 303 See Other',
                                    304 => 'HTTP/1.1 304 Not Modified',
                                    305 => 'HTTP/1.1 305 Use Proxy',
                                    307 => 'HTTP/1.1 307 Temporary Redirect',
                                    400 => 'HTTP/1.1 400 Bad Request',
                                    401 => 'HTTP/1.1 401 Unauthorized',
                                    402 => 'HTTP/1.1 402 Payment Required',
                                    403 => 'HTTP/1.1 403 Forbidden',
                                    404 => 'HTTP/1.1 404 Not Found',
                                    405 => 'HTTP/1.1 405 Method Not Allowed',
                                    406 => 'HTTP/1.1 406 Not Acceptable',
                                    407 => 'HTTP/1.1 407 Proxy Authentication Required',
                                    408 => 'HTTP/1.1 408 Request Time-out',
                                    409 => 'HTTP/1.1 409 Conflict',
                                    410 => 'HTTP/1.1 410 Gone',
                                    411 => 'HTTP/1.1 411 Length Required',
                                    412 => 'HTTP/1.1 412 Precondition Failed',
                                    413 => 'HTTP/1.1 413 Request Entity Too Large',
                                    414 => 'HTTP/1.1 414 Request-URI Too Large',
                                    415 => 'HTTP/1.1 415 Unsupported Media Type',
                                    416 => 'HTTP/1.1 416 Requested Range Not Satisfiable',
                                    417 => 'HTTP/1.1 417 Expectation Failed',
                                    500 => 'HTTP/1.1 500 Internal Server Error',
                                    501 => 'HTTP/1.1 501 Not Implemented',
                                    502 => 'HTTP/1.1 502 Bad Gateway',
                                    503 => 'HTTP/1.1 503 Service Unavailable',
                                    504 => 'HTTP/1.1 504 Gateway Time-out',
                                    505 => 'HTTP/1.1 505 HTTP Version Not Supported',
                                );
    /**
     * Initialize properties values
     * Allow for CORS, assemble and pre-process the data
     */
    public function __construct($server, $get, $post) {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        
        $this->_entity = null;
        $this->_entity_operation = null;
        
        $this->_arguments = '';
        $this->_req_server = $server;
        $this->_req_get = $get;
        $this->_req_post = $post; 
        $this->_method = $this->_req_server['REQUEST_METHOD'];
        
        //for example: "/WsRestful/api/"
        $api_uri = str_replace("index.php", "", $this->_req_server["SCRIPT_NAME"]); 
        //remove $api_uri from request: "/WsRestful/api/helloworld/is_authorized?login=test&token=123456789"
        $request_uri = str_replace($api_uri, "", $this->_req_server["REQUEST_URI"]); 
        //remove query arguments: "?login=test&token=123456789"
        $query = stripos($request_uri, "?") !== FALSE ? (stripos($request_uri, "?")) : strlen($request_uri); //position of "?" in "GET" requests
        $params = explode("/", substr($request_uri, 0, $query));
        
	foreach ($params as $param){
	    if(!empty($param) && (!isset($this->_entity) || empty($this->_entity))){
		$this->_entity = strtolower ($this->cleanEntityName($param));		
	    }
	    if(!empty($param) && isset($this->_entity) && !empty($this->_entity)){
		$this->_entity_operation = strtolower ($this->cleanEntityName($param));
	    }	    
	}
       
    }

    /**
     * Proceess service requests
     * @param array $server
     * @param array $get
     * @param array $post
     */
    public function processRequest() {
        
        /**
         * GET requests are easy to detect, but DELETE and PUT requests are hidden 
         * inside a POST request through the use of the HTTP_X_HTTP_METHOD header
         */
	$server = $_SERVER;
        if ($this->_method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $server)) {
            if ($server['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->_method = 'DELETE';
            } else if ($server['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->_method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        
        switch ($this->_method) {
            case 'GET':
                $this->_arguments = $this->_req_get;
                $this->performOperation();
                break;
            case 'HEAD':
                $this->_arguments = $this->_req_get;
                $this->performOperation();
                break;
            case 'POST':
                $this->_arguments = $this->_req_post;
                $this->performOperation();
                break;
            case 'PUT':
                parse_str(file_get_contents('php://input'), $this->_arguments);
                $this->performOperation();
                break;
            case 'DELETE':
                parse_str(file_get_contents('php://input'), $this->_arguments);
                $this->performOperation();
                break;
            default:
                echo "not defined";
        }
    }
    
    private function authenticateRequest($format, $class_name, $class_method=null){
        
        $c = new Credentials($class_name, $this->_arguments, $class_method);       
        $auth = $c->isAuthorized();
        //var_dump($auth);
        if(isset($auth['error'])){
            $response = array();
            $response['status'] = 401;
            $response['Response'] = array('error'=>$auth['error']);
            $this->setApiResponse($format, $response);
        }
        
        $this->_user = $c->getLogin();        
        $this->_id_user = $c->getUserID();        
    }

    /**
     * Perform operation according to parameters set, and defined HTTP method
     */
    protected function performOperation() {
        
        $args = $this->_arguments;
        
        $class_name = $this->_entity;
        $class_method = $this->_entity_operation;
        $format = 'xml'; //default output format
        $http_accept = isset($this->_req_server["HTTP_ACCEPT"]) ? $this->_req_server["HTTP_ACCEPT"] : 'xml';
        
        if(stripos($http_accept,'application/json') !== FALSE){
            $format = 'json';
        }
        
        //Verifies if access is authorized
        $this->authenticateRequest($format, $class_name, $class_method);

        //set username after authenticate
        $args['user'] = $this->_user;
        $args['id_user'] = $this->_id_user;
        
        //get the name of the Entity/Class affected
        if(!$class_name){
            $response['status'] = 406;
            $response['Response'] = array('error'=>'No Entity was set');
            $this->setApiResponse($format, $response);
        }
        
        //get the name of the operation to execute
        if(!$class_method){
            $response['status'] = 406;
            $response['Response'] = array('error'=>'No Operation was set');
            $this->setApiResponse($format, $response);
        }
           
        if($class_name == "connection" && $class_method == "checkconnection")
        {
            $response = array('Response' => array($class_name =>
                                                     array("return" => "true"),
                                            ),
                     'status' => 200
                   );
        }
	else if($class_name == "ping"){
	    $domain = $this->_req_server['HTTP_HOST'];
	    $response = array('Response' => array('primary_ip' => gethostbyname($domain)),
                     'status' => 200
                   );
	}
        else
        {
            $response = $this->executeClassMethod($class_name, $class_method, $args);
        }
        
        //echo '<pre>' . print_r($response, true) . '</pre>';
        $this->setApiResponse($format, $response);
        
    }
    
    /**
     * Di¡ynamic execution of a Class' method
     * @param string $class_name
     * @param string $class_method
     * @param array $args
     */
    private function executeClassMethod($class_name, $class_method, array $args){
        	
        $dir = dirname(__FILE__).'/../classes/' ;
        $class_file = null;
        $response = array('status'=>200, 'Response'=>array());
        
        if($class_name && strlen($class_name) >1){
            //all file names must be in lowercase
            $class_file = $dir . strtolower($class_name) . '.php';
        }else{
            $response['status'] = 400;
            $response['Response'] = array('error'=>'Entity name was not received');
            return $response;
        }
        
        if(is_file($class_file)){
            require_once $class_file;
            if(!class_exists($class_name)){
                $response['status'] = 400;
                $response['Response'] = array('error'=>'Entity is not defined');
                return $response;
            }
            
            $refClass = new ReflectionClass($class_name);
            $class_name = $refClass->name; //to set teh exact class name
            $class_instance = $refClass->newInstance();
            //get class public methods
            $refClassMethods = $refClass->getMethods(ReflectionMethod::IS_PUBLIC);
            $refMethodFound = false;
            foreach($refClassMethods as $method){
                if(strtolower($method->name) == strtolower($class_method)){
                    $refMethod = $method;
                    $class_method = $refMethod->name;
                    $refMethodFound = true;
                    break;
                }
            }
            
            if(!$refMethodFound){
                $response['status'] = 400;
                $response['Response'] = array('error'=>'Operation ' . $class_method . ' does not exist for ' . $class_name);
                return $response;
            }
            try{
                $allowed = $class_instance->getAllowedOperations();
                if(!isset($allowed[$class_method])){
                    $response['status'] = 400;
                    $response['Response'] = array('error'=>'No Access method is defined for operation "' . $class_method . '" (e.g. GET, PUT, DELETE, POST)');
                    return $response;
                }
                if(!in_array($this->_method, $allowed[$class_method])){
                    $response['status'] = 400;
                    $response['Response'] = array('error'=>'HTTP Method ' . $this->_method . ' is not allowed for operation ' . 
                                                   $class_method . ' on ' . $class_name);
                    return $response;
                }
                //set all arguments names to lowercase for comparison
                $args_lower = array();
                foreach($args as $key=>$value){
                    $args_lower[strtolower($key)] = $value;
                }
                //Verify defined function parameters
                $arguments = array();
                
                foreach($refMethod->getParameters() as $parameter){
                    $name = strtolower($parameter->name);
                    
                    //If defined parameter is defined, its value is added and process continues with next parameter
                    if(isset($args_lower[$name]) && strlen($args_lower[$name]) > 0){
                        $arguments[] = $args_lower[$name];
                        continue;
                    }else{
                        $refParameter = new ReflectionParameter(array($refClass->name, $refMethod->name), $name );
                        //if parameter is optional default value is set
                        if($refParameter->isOptional() && $refParameter->isDefaultValueAvailable()){
                            $arguments[] = $refParameter->getDefaultValue();
                            continue;
                        }
                        
                        $response['status'] = 400;
                        $response['Response'] = array('error'=>'A valid value set for parameter: "' . $parameter->name . '".');
                        return $response;
                    }
                }
		//var_dump($class_instance, $class_method, $arguments); exit;
                $response = call_user_func_array(array($class_instance, $class_method), $arguments);
            }catch(Exception $e){
                $response['status'] = 400;
                $response['Response'] = array('error'=>$e->getMessage());
                return $response;
            }
            
        }else{
             
            $response['status'] = 404;
            $response['Response'] = array('error'=>'Entity ' . $class_name . ' does not exist');
        }
        
        return $response;
    }

    /**
     * Deliver HTTP Response
     * @param string $format The desired HTTP response content type: [json, html, xml]
     * @param string $api_response The desired HTTP response data
     * @return void
     * */
    protected function setApiResponse($format, $response) {
       
        
        // Set HTTP Response
        header($this->_http_status_codes[$response['status']]);
        $content = '';

        // Process different content types
        if (strcasecmp($format, 'json') == 0) {
            // Set HTTP Response Content Type
            header('Content-Type: application/json; charset=utf-8');
            // Format data into a JSON response
            $content = json_encode($response);
        } elseif (strcasecmp($format, 'xml') == 0) {
            // Set HTTP Response Content Type
            header('Content-Type: application/xml; charset=utf-8');
            // Format data into an XML response (This is only good at handling string data, not arrays)
            $dom =  new DOMDocument();
            $r = $dom->createElement('Response');
            $dom->appendChild($r);
            $source = isset($response['Response']) ? $response['Response'] : array();
            $this->arrayToXML($source,$r);
            
            $content = $dom->saveXML();
        } else {
            // Set HTTP Response Content Type (This is only good at handling string data, not arrays)
            header('Content-Type: text/html; charset=utf-8');
            // Deliver formatted data
            $content = $response['data'];
        }

        echo $content;
        die;
    }
    
    /**
     * transforms an array structure to DOM 
     * @param array $source
     * @param DOMElement $dom
     */
    private function arrayToXML(array $source, DOMElement &$dom){
	 //exit;
        foreach($source as $key=>$value){	    
            
	    $attributeName = '';
	    $attributevalue = 0;
	    
	    if(is_numeric($key)) {
		$tagName = "item";
		$attributeName = 'ID';
		$attributevalue = $key;
	    } else {
		$tagName = $key;        
	    }
	    
	    if(strpos($tagName, ':')) {  // 11/12/2015
		$Names = explode(':', $tagName);
		$tagName = isset($Names[0])? $Names[0] : $tagName;
		
		if(isset($Names[1]))
		    $attributeName = isset($Names[1])? $Names[1] : $tagName;	
	    }
	     
	    if($key != 'EndNewVersion') // exception for send orders only
	    {
		//FIX FOR TESTING, AS ARRAY HAS INTEGERS AS INDEXES
		if(!isset($history[$tagName][$attributevalue]) && isset($tagName) && !empty($tagName))
		{
		    $node = $dom->ownerDocument->createElement($tagName);		 
		}
		$history[$tagName][$attributevalue] = true;	    

		if(is_array($value)){

		    if($attributevalue != 0)
			    $node->setAttribute($attributeName, $attributevalue);

		    $this->arrayToXML($value, $node);
		}else{
                    $value = utf8_encode(($value));
		    if(isset($attributeName) && !empty($attributeName)) { // 11/12/2015
			if($attributevalue != 0) {
			    $node->setAttribute($attributeName, $attributevalue);
			} else {
			    $node->setAttribute($attributeName, $value);
			}
		    } else{
			if(!is_int($value) && !is_float($value) && !is_integer($value) && Entity::$_allowedCDATAs  ) {
			    $node->appendChild($dom->ownerDocument->createCDATASection($value));
			} else {
			    $node->nodeValue = htmlspecialchars($value);
			}

		    }
		}

		$dom->appendChild($node);
            }
	    
	    // use until user not use old version
	    if($key == 'EndNewVersion')  {
		Entity::$_allowedCDATAs = false;
	    }
	    
        }
    }
    
    /**
     * Prevents injection, used when evaluating a name for including a file
     * @param string $entity name of the entity to be included
     * @return string type Entity name after replaced non allowed characters
     */
    protected function cleanEntityName($entity) {
        //return preg_replace("\.*\s*/*\\*", "", $entity);
        return preg_replace("/[^a-zA-Z0-9\_]*/", "", $entity);
    }
    
}