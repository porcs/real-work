<?php

function misc_parsestring($text,$allowchr='1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
	if(empty($allowchr))
		$allowchr = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	if(empty($text)) return FALSE;
	$size = strlen($text);
	for($i=0; $i < $size; $i++) {
		$tmpchr = substr($text, $i , 1);
		if(strpos($allowchr,$tmpchr) === FALSE) 
			return FALSE;
	}
	return TRUE;
}

function game_authen($username,$password)
{
	global $_CONFIG;

	$login_flag = false;
	$user_no = '';
	$result = odbc_exec($_CONFIG['mssql']['connection'],'SELECT TOP 1 UserNum,UserPass FROM ' . $_CONFIG['mssql']['account_db_name'] . '.dbo.UserInfo WHERE UserName = \'' . $username . '\'');
	if(odbc_num_rows($result) == 1)
	{
		odbc_fetch_row($result);
		$row = array();
		$row['UserNum'] = odbc_result($result, 'UserNum');
		$row['UserPass'] = odbc_result($result, 'UserPass');
		if($_CONFIG['mssql']['use_md5'] == true)
		{
			$password = strtoupper(substr(md5($password),0,19));
		}
		if(strcmp($password,$row['UserPass']) == 0)
		{
			$login_flag = true;
			$user_no = $row['UserNum'];
		}
	}
	return array('flag'=>$login_flag,'id'=>$user_no);
}

function refill_countcards($query)
{
	global $_CONFIG;

	$result = odbc_exec($_CONFIG['mssql']['connection'],'SELECT COUNT(*) AS count FROM ' . $_CONFIG['mssql']['account_db_name'] . '.dbo.truemoney ' . $query);
	$row = odbc_fetch_array($result);
	return $row['count'];
}

function refill_getcards($user_no,$count=20)
{
	global $_CONFIG;

	$cards = array();
	$fields = array();
	//card status : 0=waiting , 1=succeed , 2=succeed(cash was added) , 3=already used , 4=incorrect password , 5=truemovecard
	$result = odbc_exec($_CONFIG['mssql']['connection'],'SELECT TOP ' . $count . ' * FROM ' . $_CONFIG['mssql']['account_db_name'] . '.dbo.truemoney WHERE user_no = ' . $user_no . ' ORDER BY card_id DESC');
	for($i = 1;$i <= odbc_num_fields($result);$i++)
    {
        $fields[$i] = odbc_field_name($result,$i);
    }
	while($row = odbc_fetch_row($result))
	{
		$row = array();
		foreach($fields as $name)
		{
			$row[$name] = odbc_result($result, $name);
		}
		$cards[] = $row;
	}
	return $cards;
}

function refill_sendcard($user_no,$password)
{
	global $_CONFIG;

	$curl = curl_init('https://203.146.127.112/tmpay.net/TPG/backend.php?merchant_id=' .$_CONFIG['tmpay']['merchant_id'] . '&password=' . $password . '&resp_url=' . $_CONFIG['tmpay']['resp_url']);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curl, CURLOPT_TIMEOUT, 10);
	curl_setopt($curl, CURLOPT_HEADER, FALSE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	$curl_content = curl_exec($curl);
	if($curl_content === false)
	{
		die(curl_errno($curl) . ':' . curl_error($curl));
	}
	curl_close($curl);
	if(strpos($curl_content,'SUCCEED') !== FALSE)
	{
		odbc_exec($_CONFIG['mssql']['connection'],'INSERT INTO ' . $_CONFIG['mssql']['account_db_name'] . '.dbo.truemoney (password,user_no,amount,status,added_time) VALUES (\'' . $password . '\',' . $user_no . ',0,0,GETDATE())');
		return TRUE;
	}
	else return $curl_content;
}

?>