<?php

header('P3P: CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
header('Content-Type: text/html; charset=utf-8');

ini_set('mssql.datetimeconvert', 0);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<table border="1" cellspacing="0" cellpadding="10">
  <tr>
    <td align="center"><strong>Card ID</strong></td>
    <td align="center"><strong>Password</strong></td>
    <td align="center"><strong>ID</strong></td>
    <td align="center"><strong>Amount</strong></td>
    <td align="center"><strong>Status</strong></td>
	<td align="center"><strong>Datetime</strong></td>
  </tr>
<?php

include_once('config.php');
include_once('functions.php');

/* connect to mssql server */
$_CONFIG['mssql']['connection'] = odbc_connect('Driver={SQL Server};Server=' . $_CONFIG['mssql']['host'] . ';Database=' . $_CONFIG['mssql']['account_db_name']  . ';' , $_CONFIG['mssql']['username'],$_CONFIG['mssql']['password']) or die('MSSQL Connection Error');

$result = odbc_exec($_CONFIG['mssql']['connection'],'SELECT TOP 100 * FROM ' . $_CONFIG['mssql']['account_db_name'] . '.dbo.truemoney ORDER BY card_id DESC');
while($row = odbc_fetch_object($result))
{
	$account_result = odbc_exec($_CONFIG['mssql']['connection'],'SELECT UserName FROM ' . $_CONFIG['mssql']['account_db_name'] . '.dbo.UserInfo WHERE UserNum=' . $row->user_no);
	$account_row = odbc_fetch_object($account_result);
	echo '
		  <tr>
		<td>' . $row->card_id . '</td>
		<td>' . substr($row->password,0,10) . 'xxxx</td>
		<td>' . $account_row->UserName . '</td>
		<td>' . $_CONFIG['tmpay']['amount'][$row->amount] . '</td>
		<td>' . $_CONFIG['tmpay']['card_status'][$row->status] . '</td>
		<td>' . $row->added_time . '</td>
	  </tr>
	';
}

?>
</table>
</body>
</html>