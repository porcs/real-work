<?php

// IP ของฐานข้อมูล MSSQL
$_CONFIG['mssql']['host'] = '127.0.0.1';
// Username ของฐานข้อมูล
$_CONFIG['mssql']['username'] = 'sa';
// Password ของฐานข้อมูล
$_CONFIG['mssql']['password'] = '1234';
// ชื่อ Database ที่เก็บข้อมูล ID ของเกม
$_CONFIG['mssql']['account_db_name'] = 'RanUser';
// มีการเข้ารหัสผ่านแบบ MD5 หรือไม่ (true = ใช่ , false = ไม่ใช่)
$_CONFIG['mssql']['use_md5'] = false;

// IP ของ TMPAY.NET ที่อนุญาติให้รับส่งข้อมูลบัตรเงินสด (ไม่ควรแก้ไข)
$_CONFIG['tmpay']['access_ip'] = '203.146.127.112';
// รหัสร้านค้า ของบัญชี TMPAY.NET
$_CONFIG['tmpay']['merchant_id'] = 'NO15110611';
// URL ที่ได้ติดตั้งไฟล์ tmpay.php
$_CONFIG['tmpay']['resp_url'] = 'http://127.0.0.1/topup/tmpay.php';

// มูลค่าบัตรเงินสด (ไม่ควรแก้ไข)
$_CONFIG['tmpay']['amount'][0] = 0;
$_CONFIG['tmpay']['amount'][1] = 50;
$_CONFIG['tmpay']['amount'][2] = 90;
$_CONFIG['tmpay']['amount'][3] = 150;
$_CONFIG['tmpay']['amount'][4] = 300;
$_CONFIG['tmpay']['amount'][5] = 500;
$_CONFIG['tmpay']['amount'][6] = 1000;

// อัตรา Point ที่จะได้รับ (เทียบกับมูลค่าบัตรเงินสด)
$_CONFIG['tmpay']['cash_amount'][0] = 0;
$_CONFIG['tmpay']['cash_amount'][1] = 200;
$_CONFIG['tmpay']['cash_amount'][2] = 360;
$_CONFIG['tmpay']['cash_amount'][3] = 600;
$_CONFIG['tmpay']['cash_amount'][4] = 1250;
$_CONFIG['tmpay']['cash_amount'][5] = 2100;
$_CONFIG['tmpay']['cash_amount'][6] = 4200;

$_CONFIG['tmpay']['card_status'][0] = 'รอการตรวจสอบ';
$_CONFIG['tmpay']['card_status'][1] = '<font color="green">ผ่าน</font>';
$_CONFIG['tmpay']['card_status'][3] = '<font color="red">ถูกใช้ไปแล้ว</font>';
$_CONFIG['tmpay']['card_status'][4] = '<font color="red">รหัสไม่ถูกต้อง</font>';
$_CONFIG['tmpay']['card_status'][5] = '<font color="red">บัตรทรูมูฟ</font>';

?>