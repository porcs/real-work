<?php

header('P3P: CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
header('Content-Type: text/html; charset=utf-8');

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

ini_set('mssql.datetimeconvert', 0);

error_reporting(0);
session_start();

function redirect($url)
{
	return '<script type="text/javascript">
	setTimeout(function () {
	   window.location.href = "' . $url . '";
	}, 4000);
	</script>
	<html><head><meta http-equiv="refresh" content="4;URL=' . $url . '"></head></html>
	(กรุณารอสักครู่ เรากำลังพาท่านไป ... หรือ <a href="' . $url . '"><b>คลิ๊กที่นี่</b></a>)';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<title>RAN REFILL CENTER</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
}
body {
	background-image: url(images/bg.jpg);
	background-repeat: repeat-x;
	background-color: #013183;
	text-align: center;
}
.style1 {
	color: #666666;
	font-weight: bold;
}
.style2 {color: #666666}
a:link {
	color: #FFFFFF;
}
a:visited {
	color: #FFFFFF;
}
a:hover {
	color: #FFFFFF;
}
a:active {
	color: #FFFFFF;
}
.hd {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	color: #FFF;
}
.se {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	color: #FFF;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
history.forward();

function disableback() {
	if(window.history.forward(1) != null)
		window.history.forward(1);
}
//-->
</script>
</head>

<body onLoad="disableback();">
<table width="600" border="0" align="center" cellpadding="0" cellspacing="2">
  <tr>
    <td height="45" align="center" background="images/topbaras_fill.gif"><span class="style1">RAN REFILL CENTER</span></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td><?php

include_once('config.php');
include_once('functions.php');

/* connect to mssql server */
$_CONFIG['mssql']['connection'] = odbc_connect('Driver={SQL Server};Server=' . $_CONFIG['mssql']['host'] . ';Database=' . $_CONFIG['mssql']['account_db_name']  . ';' , $_CONFIG['mssql']['username'],$_CONFIG['mssql']['password']) or die('MSSQL Connection Error');
odbc_exec($_CONFIG['mssql']['connection'],'SELECT 1 FROM ' . $_CONFIG['mssql']['account_db_name'] . '.dbo.truemoney') or die('Table truemoney could\'nt be reached');

if(function_exists('curl_init') == false)
{
	die('cURL extension must be enabled');
}

if(isset($_GET['logout']))
{
	session_unset();
}

if(empty($_SESSION['user_no']))
{
	if(isset($_POST['username']) && misc_parsestring($_POST['username']) && isset($_POST['password']))
	{
		sleep(3);
		$_POST['username'] = strtolower($_POST['username']);
		$login_info = game_authen($_POST['username'],$_POST['password']);
		if($login_info['flag'] == true)
		{
			$_SESSION['user_no'] = $login_info['id'];
			echo '
			<table border="0" align="center" cellpadding="5" cellspacing="2">
			  <tr>
				<td>Login สำเร็จ<br />
				' . redirect('./?' . session_id() . '-' . mt_rand()) . '</td>
			  </tr>
			</table>';
		}
		else
		{
			echo '
			<table border="0" align="center" cellpadding="5" cellspacing="2">
			  <tr>
					<td>Login ล้มเหลว เนื่องจาก Username หรือ Password ไม่ถูกต้อง<br />
					' . redirect('./?' . session_id() . '-' . mt_rand()) . '</td>
			  </tr>
			</table>';
		}
	}
	else
	{
		echo '
		<form id="form1" name="form1" method="post" action="./?' . session_id() . '-' . mt_rand() . '">
		  <table border="0" align="center" cellpadding="5" cellspacing="2">
			<tr>
			  <td colspan="2" align="center">กรุณา Login ด้วย ID ที่ใช้เข้าสู่เกม</td>
			</tr>
			<tr>
			  <td align="right">ID</td>
			  <td align="left"><input name="username" type="text" id="username" size="15" maxlength="30" /></td>
			</tr>
			<tr>
			  <td align="right">Password</td>
			  <td align="left"><input name="password" type="password" id="password" size="15" maxlength="30" /></td>
			</tr>
			<tr>
			  <td colspan="2" align="center"><input type="submit" name="button" id="button" value="Login" /></td>
			</tr>
		  </table>
		</form>';
	}
}
else
{
	if(isset($_POST['truemoney_password']) && isset($_SESSION['can_refill']) && isset($_POST['encode_hash']) && $_POST['encode_hash'] == md5($_SESSION['can_refill']))
	{
		$_SESSION['can_refill'] = unserialize($_SESSION['can_refill']);
		foreach($_SESSION['can_refill'] as $digit=>$char)
		{
			$_POST['truemoney_password'] = str_replace($char,$digit,$_POST['truemoney_password']);
		}
		unset($_SESSION['can_refill']);
		if (misc_parsestring($_POST['truemoney_password'],'0123456789') == FALSE || strlen($_POST['truemoney_password']) != 14)
		{
			echo '
			<table border="0" align="center" cellpadding="5" cellspacing="2">
			  <tr>
				<td>รหัสบัตรเงินสดที่ระบุมีรูปแบบที่ไม่ถูกต้อง<br />
				' . redirect('./?' . session_id() . '-' . mt_rand()) . '</td>
			  </tr>
			</table>';
		}
		else if (refill_countcards('WHERE password = \'' . $_POST['truemoney_password'] . '\' AND (status = 0 OR status = 1)') == 1)
		{
			echo '
			<table border="0" align="center" cellpadding="5" cellspacing="2">
			  <tr>
				<td>รหัสบัตรเงินสดที่ระบุ ถูกใช้งานไปแล้ว<br />
				' . redirect('./?' . session_id() . '-' . mt_rand()) . '</td>
			  </tr>
			</table>';
		}
		else if (refill_countcards('WHERE user_no = ' . $_SESSION['user_no'] . ' AND status = 0') >= 3)
		{
			echo '
			<table border="0" align="center" cellpadding="5" cellspacing="2">
			  <tr>
				<td>ท่านยังมีรหัสบัตรเงินสดที่รอการตรวจสอบอยู่<br />
				' . redirect('./?' . session_id() . '-' . mt_rand()) . '</td>
			  </tr>
			</table>';
		}
		else if (refill_countcards('WHERE user_no = ' . $_SESSION['user_no'] . ' AND status > 1 AND added_time > (GETDATE()-1)') >= 3)
		{
			echo '
			<table border="0" align="center" cellpadding="5" cellspacing="2">
			  <tr>
				<td>ท่านเติมเงินผิดหลายครั้ง ระบบระงับการเติมเงินเป็นเวลา 24 ชั่วโมง<br />
				' . redirect('./?' . session_id() . '-' . mt_rand()) . '</td>
			  </tr>
			</table>';
		}
		else
		{
			if(($tmpay_ret = refill_sendcard($_SESSION['user_no'],$_POST['truemoney_password'])) !== TRUE)
			{
				echo '
				<table border="0" align="center" cellpadding="5" cellspacing="2">
				  <tr>
					<td>ขออภัย ขณะนี้ระบบ TMPAY.NET ขัดข้อง กรุณาติดต่อเจ้าหน้าที่ (Error: ' . $tmpay_ret . ')<br />
					' . redirect('./?' . session_id() . '-' . mt_rand()) . '</td>
				  </tr>
				</table>';
			}
			else
			{
				echo '
				<table border="0" align="center" cellpadding="5" cellspacing="2">
				  <tr>
					<td>ได้รับข้อมูลบัตรเงินสดเรียบร้อย กรุณารอการตรวจสอบจากระบบ<br />
					' . redirect('./?' . session_id() . '-' . mt_rand()) . '</td>
				  </tr>
				</table>';
			}
		}
	}
	else
	{
		$array_desc = str_split('ABCDEFGHIJ');
		shuffle($array_desc);
		$_SESSION['can_refill'] = serialize($array_desc);
		echo '
		<script type="text/javascript">
		function encode_tmnc()
		{
			var temp = document.getElementById("truemoney_password_tmp").value;
		';
		foreach($array_desc as $digit=>$char)
		{
			echo '
			while(temp.indexOf(\'' . $digit . '\')!=-1) { temp = temp.replace(\'' . $digit . '\',\'' . $char . '\'); }';
		}
		echo '
			document.getElementById("truemoney_password").value = temp;
			document.getElementById("truemoney_password_tmp").value = "";
		}
		</script>';
		$cards = refill_getcards($_SESSION['user_no'],20);
		if(empty($cards) == false)
		{
			echo '
			<table border="0" align="center" cellpadding="5" cellspacing="2">
			  <tr>
				<td align="center" bgcolor="#333333"><strong>รหัสบัตรเงินสด</strong></td>
				<td align="center" bgcolor="#333333"><strong>มูลค่า</strong></td>
				<td align="center" bgcolor="#333333"><strong>สถานะ</strong></td>
				<td align="center" bgcolor="#333333"><strong>เวลาที่เพิ่มเข้าระบบ</strong></td>
			  </tr>';
			 foreach($cards as $val)
			{
				echo '
				  <tr>
					<td align="center">' . $val['password'] . '</td>
					<td align="center">' . $_CONFIG['tmpay']['amount'][$val['amount']] . ' บาท</td>
					<td align="center">' . $_CONFIG['tmpay']['card_status'][$val['status']] . '</td>
					<td align="center">' . $val['added_time'] . '</td>
				  </tr>';
			}
			echo '		 </table><br />';
		}
		$result = odbc_exec($_CONFIG['mssql']['connection'],'SELECT TOP 1 UserPoint FROM ' . $_CONFIG['mssql']['account_db_name'] . '.dbo.UserInfo WHERE UserNum = ' . $_SESSION['user_no']);
		odbc_fetch_row($result);
		$point = odbc_result($result, 'UserPoint');
		echo '
		 <form id="form1" name="form1" method="post" action="./?' . session_id() . '-' . mt_rand() . '" onsubmit="encode_tmnc();">
		  <table border="0" align="center" cellpadding="5" cellspacing="2">
			<tr>
			  <td colspan="2" align="center">ID ของคุณมี Point คงเหลือ <b>' . number_format($point) . '</b> Point</td>
			</tr>
			<tr>
			  <td align="right">รหัสบัตรเงินสด</td>
			  <td align="left"><input name="truemoney_password_tmp" type="text" id="truemoney_password_tmp" size="20" maxlength="14" /><input name="truemoney_password" type="hidden" id="truemoney_password" />
			  <input name="encode_hash" type="hidden" id="encode_hash" value="' . md5($_SESSION['can_refill']) . '" /></td>
			</tr>
			<tr>
			  <td colspan="2" align="left">- เฉพาะบัตรทรูมันนี่เท่านั้น บัตรทรูมูฟไม่สามารถใช้ได้<br />
				- Point จะเข้าสู่ ID ของท่านทันที หลังจากระบบอนุมัติ<br />
				- ใช้เวลาตรวจสอบประมาณ 1-10 นาที<br />
				- หากเกิดข้อผิดพลาดใดๆ กรุณาแจ้งเจ้าหน้าที่</td>
			</tr>
			<tr>
			  <td colspan="2" align="center"><input type="submit" name="button" id="button" value="Refill" /></td>
			</tr>
			<tr>
			  <td colspan="2" align="center">หากเลิกใช้งาน กรุณา <b><a href="./?logout=' . session_id() . '">ออกจากระบบ</a></b></td>
			</tr>
		  </table>
		</form>';
	}
}

odbc_close($_CONFIG['mssql']['connection']);

?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="45" align="center" background="images/topbaras_fill.gif"><span class="style2">Refill Center System (Powered by <strong><a href="http://www.tmpay.net/" target="_blank"><span class="style2">TMPAY.NET</span></a></strong>)</span></td>
  </tr>
</table>
<br />
<table width="600" border="0" align="center">
  <tr>
    <th scope="col"><table width="600" border="0" align="center">
      <tr>
        <td align="center" valign="top" bgcolor="#FF6699" class="hd">อัตราการเติมเงิน</td>
      </tr>
      <tr>
        <td align="center" valign="top" bgcolor="#0066FF"><strong class="hd">เติมเงินผ่าน 7-11 นำ Barcode ไปให้ 7-11 ยิง (สอบถามGMเพิ่มเติม)</strong></td>
      </tr>
      <tr>
        <td align="center"><table width="327" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="120" align="right"><table width="117" border="1" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center"><a href="images/7-11.png" target="_blank"><img src="images/7-11.png" width="200" /></a></td>
              </tr>
              <tr>
                <td align="center"><span class="hd">คลิ้กที่ภาพเพื่อขยาย</span></td>
              </tr>
            </table></td>
            <td width="168"><table width="210" border="1" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center"><span class="hd">จำนวนเงิน</span></td>
                <td align="center"><span class="hd">Pointที่ไดรับ้</span></td>
                <td align="center" bgcolor="#FF6600"><span class="hd">อัตราการคูณ</span></td>
              </tr>
              <tr>
                <td align="center"><span class="hd">500</span></td>
                <td align="center">1,750</td>
                <td align="center" bgcolor="#FF6600">3.5</td>
              </tr>
              <tr>
                <td align="center">1,000</td>
                <td align="center">4,000</td>
                <td align="center" bgcolor="#FF6600">4</td>
              </tr>
            </table></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align="center" valign="top" bgcolor="#FF0000" class="hd">เติมเงิน True Money</td>
      </tr>
      <tr>
        <td align="center" valign="top"><table width="250" border="1" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center" valign="top"><span class="hd">ราคาบัตรทรูมันนี่</span></td>
            <td align="center" valign="top"><span class="hd">Pointที่ได้รับ</span></td>
            <td align="center" valign="top" bgcolor="#FF6600"><span class="hd">อัตราการคูณ</span></td>
          </tr>
          <tr>
            <td align="center" valign="top">50</td>
            <td align="center" valign="top">150</td>
            <td align="center" valign="top" bgcolor="#FF6600">3</td>
          </tr>
          <tr>
            <td align="center" valign="top">90</td>
            <td align="center" valign="top">270</td>
            <td align="center" valign="top" bgcolor="#FF6600">3</td>
          </tr>
          <tr>
            <td align="center" valign="top">150</td>
            <td align="center" valign="top">450</td>
            <td align="center" valign="top" bgcolor="#FF6600">3</td>
          </tr>
          <tr>
            <td align="center" valign="top">300</td>
            <td align="center" valign="top">900</td>
            <td align="center" valign="top" bgcolor="#FF6600">3</td>
          </tr>
          <tr>
            <td align="center" valign="top">500</td>
            <td align="center" valign="top">1,500</td>
            <td align="center" valign="top" bgcolor="#FF6600">3</td>
          </tr>
          <tr>
            <td align="center" valign="top">1000</td>
            <td align="center" valign="top">3,000</td>
            <td align="center" valign="top" bgcolor="#FF6600">3</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" valign="top">&nbsp;</td>
      </tr>
    </table></th>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>