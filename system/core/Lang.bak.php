<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Language Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Language
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/language.html
 */
class CI_Lang {

	/**
	 * List of translations
	 *
	 * @var array
	 */
	var $language	= array();
	/**
	 * List of loaded language files
	 *
	 * @var array
	 */
	var $is_loaded	= array();
        public $language_by_file = array();

	/**
	 * Constructor
	 *
	 * @access	public
	 */
	function __construct()
	{
		log_message('debug', "Language Class Initialized");
	}

	// --------------------------------------------------------------------

	/**
	 * Load a language file
	 *
	 * @access	public
	 * @param	mixed	the name of the language file to be loaded. Can be an array
	 * @param	string	the language (english, etc.)
	 * @param	bool	return loaded array of translations
	 * @param 	bool	add suffix to $langfile
	 * @param 	string	alternative path to look for language file
	 * @return	mixed
	 */
	function load($langfile = '', $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '')
	{
		$langfile = str_replace('.php', '', $langfile);
                $index_file = $langfile;
		if ($add_suffix == TRUE)
		{
			$langfile = str_replace('_lang.', '', $langfile).'_lang';
		}
               
		$langfile .= '.php';

		if (in_array($langfile, $this->is_loaded, TRUE))
		{
			return;
		}

		$config =& get_config();

		if ($idiom == '')
		{
			$deft_lang = ( ! isset($config['language'])) ? 'english' : $config['language'];
			$idiom = ($deft_lang == '') ? 'english' : $deft_lang;
		}

		// Determine where the language file is and load it
		if ($alt_path != '' && file_exists($alt_path.'language/'.$idiom.'/'.$langfile))
		{
			include($alt_path.'language/'.$idiom.'/'.$langfile);
		}
		else
		{
			$found = FALSE;

			foreach (get_instance()->load->get_package_paths(TRUE) as $package_path)
			{
				if (file_exists($package_path.'language/'.$idiom.'/'.$langfile))
				{
					include($package_path.'language/'.$idiom.'/'.$langfile);
					$found = TRUE;
					break;
				}
			}

			if ($found !== TRUE)
			{
				show_error('Unable to load the requested language file: language/'.$idiom.'/'.$langfile);
			}
		}


		if ( ! isset($lang))
		{
			log_message('error', 'Language file contains no data: language/'.$idiom.'/'.$langfile);
			return;
		}

		if ($return == TRUE)
		{
			return $lang;
		}

		$this->is_loaded[] = $langfile;
		$this->language = array_merge($this->language, $lang);
                if(!isset($this->language_by_file[$langfile])) $this->language_by_file[$langfile] = array();
                $this->language_by_file[$index_file] = array_merge($this->language_by_file[$langfile], $lang);
		unset($lang);

		log_message('debug', 'Language file loaded: language/'.$idiom.'/'.$langfile);
		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Fetch a single line of text from the language array
	 *
	 * @access	public
	 * @param	string	$line	the language line
	 * @return	string
	 */
        function check_file_line($file,$line){
            return isset($this->language_by_file[$file][$line]);
        }
	function line($line = '',$file='')
	{       if(!empty($file)){
                    $value = ($line == '' OR !isset($this->language_by_file[$file]) OR ! isset($this->language_by_file[$file][$line])) ? FALSE : $this->language[$line];
                }else{
                    $value = ($line == '' OR ! isset($this->language[$line])) ? FALSE : $this->language[$line];
                }

		// Because killer robots like unicorns!
		if ($value === FALSE)
		{
                     $url = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
                     $ref='';
		     log_message('error', 'Could not find the language line "'.$line.'" @ '.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
                     if(isset($_SERVER["HTTP_REFERER"])){
                         $ref = $_SERVER["HTTP_HOST"] .$_SERVER["HTTP_REFERER"];
                     log_message('error','Referer From : '.$_SERVER["HTTP_HOST"] .$_SERVER["HTTP_REFERER"]) ;
                     }
                     
                     if(!empty($line) && !empty($url)){
                         $ci = get_instance();
                         $session_key = date("Y-m-d H:i:s");
                         $data = array('id_key'=>md5($line.$url), 'missing_word'=>$line,'url'=>$url,'ref'=>$ref,'last_update'=>$session_key);
                         if(!empty($file)){
                             $data['target_file'] = $file.'_lang.php'; 
                         }
                         $insert_query = $ci->db->insert_string('crowdin_missing_word', $data);
                          $insert_query = str_replace('INSERT INTO','REPLACE INTO',$insert_query);
                         $ci->db->query($insert_query);
                     } 

		}else{    
                    $url =  $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
                    $url = str_replace('//','/',$url);
                    $ref=''; 
                    if(isset($_SERVER["HTTP_REFERER"])){
                        $ref =  $_SERVER["HTTP_REFERER"]; 
                    }
                        $ci = get_instance();
                        $session_key = date("Y-m-d H:i:s");
                        $data = array('id_key'=>md5($line.$url), 'key_full'=>$line,'value'=>$value,'url'=>$url,'ref'=>$ref,'last_update'=>$session_key);
                        $insert_query = $ci->db->insert_string('crowdin_path_of_word', $data);
                        $insert_query = str_replace('INSERT INTO','REPLACE INTO',$insert_query);
                        $ci->db->query($insert_query);
                }

		return $value;
	}

}
// END Language Class

/* End of file Lang.php */
/* Location: ./system/core/Lang.php */
