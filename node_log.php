<pre style="width: 100%; word-break: break-word;">
<?php
$limit = 300;
$refresh = 15;
if(isset($_REQUEST['limit']) && is_numeric($_REQUEST['limit'])){
    $limit = $_REQUEST['limit'];
}
if(isset($_REQUEST['refresh']) && is_numeric($_REQUEST['refresh'])){
    $refresh = $_REQUEST['refresh'];
}
$fl = fopen("./assets/apps/node.log", "r");
for($x_pos = 0, $ln = 0, $output = array(); fseek($fl, $x_pos, SEEK_END) !== -1; $x_pos--) {
    $char = fgetc($fl);
    if ($char === "\n") {
        // analyse completed line $output[$ln] if need be
        $ln++;
        continue;
        }
    $output[$ln] = strip_tags($char . ((array_key_exists($ln, $output)) ? $output[$ln] : ''));
    if($ln>=$limit)break;
    }
fclose($fl);
print_r($output);
?>
</pre>
<meta http-equiv="refresh" content="<?php echo $refresh;?>" />