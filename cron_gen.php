<?php

ini_set('display_errors',1);
error_reporting(E_ALL);
$system_path = 'system';
define('BASEPATH', str_replace("\\", "/", $system_path));
include(dirname(__FILE__) . '/../application/config/config.php');
include(dirname(__FILE__) . '/../application/config/database.php'); 
require_once(dirname(__FILE__) . '/../application/libraries/Ebay/ObjectBiz.php');
$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.');
 
$update_file = isset($_REQUEST['update_file'])?true:false;

 



class GENCRON{
    const FREETAIL = 0;
    public function __construct() {
        global $dbx,$update_file;
        $this->app_path = "  php  ".DIR_SERVER."/assets/apps/";  
        $this->cmd_padd = "";
        $this->mode_fix = true;
        $this->cronj_list = array();  
        $this->db = $dbx;
        $this->update_file = $update_file;
        $this->crond_import_feed_url =   "import_feed_cron.php";
        $this->crond_import_product_url = "import_product_cron.php";
        $this->crond_import_offer_url =   "import_offer_cron.php";
        $this->crond_import_order_url =  "import_shop_order_cron.php"; 
        $this->crond_import_stock_url = "stock_movement_cron.php"; 
        
        $this->crond_update_product_amazon_url = "amazon_update_products_cron.php";
        $this->crond_update_offer_amazon_url = "amazon_update_offers_cron.php";
        $this->crond_update_product_ebay_url = "ebay_export_product_cron.php";
        $this->crond_update_offer_ebay_url = "ebay_export_offer_cron.php";
        
        $this->crond_delete_product_amazon_url = "amazon_delete_product_cron.php";
        $this->crond_delete_product_ebay_url = "ebay_delete_product_cron.php";
        $this->crond_sync_product_ebay_url ="ebay_sync_product_cron.php";
        
        $this->crond_update_order_amazon_url = "amazon_update_orders_cron.php";
        $this->crond_update_order_ebay_url = "ebay_update_orders_cron.php";
        $this->crond_get_order_amazon_url = "amazon_get_orders_cron.php";
        $this->crond_get_order_ebay_url = "ebay_get_orders_cron.php";
        $this->crond_send_order_amazon_url = "send_orders_cron.php";
        $this->crond_send_order_ebay_url = "send_orders_cron.php";
        
        
        
        
        $this->file_crontab = DIR_SERVER . '/assets/apps/users/cron/user_crontab.cron';
        // $this->frequency_feed = '3';//mins
        $this->frequency_feed_feed = '1441'; // mins = 1 day
        $this->frequency_feed_offer = '31'; // mins
        $this->frequency_feed_order = '46'; // mins
        $this->frequency_update_product_amazon = '721';
        $this->frequency_update_offer_amazon = '91';
        $this->frequency_update_product_ebay = '61';
        $this->frequency_update_offer_ebay = '91';
        $this->frequency_update_order_amazon = '1441';
        $this->frequency_update_order_ebay = '1441';
        $this->frequency_get_order_amazon = '61';
        $this->frequency_get_order_ebay = '61';
        $this->frequency_send_order_amazon = '61';
        $this->frequency_send_order_ebay = '61';
        $this->frequency_delete_product_amazon = '721';
        $this->frequency_delete_product_ebay = '721';
        $this->frequency_feed_stock = '30'; 
        $this->frequency_sync_product_ebay = '721';
        
        $this->fix_time_feed_feed = '0 0 * * *'; // mins = 1 day
        $this->fix_time_feed_offer = '15,45 * * * *'; // mins
        $this->fix_time_feed_order = '25 * * * *'; // mins
        $this->fix_time_feed_stock = '5,35 * * * *'; // mins
        $this->fix_time_update_product_amazon = '55  0,12 * * *';
        $this->fix_time_update_offer_amazon = '20,50 * * * *';
        $this->fix_time_update_product_ebay = '30 5 * * *';
        $this->fix_time_update_offer_ebay = '20 * * * *';
        $this->fix_time_update_order_amazon = '20 2 * * *';
        $this->fix_time_update_order_ebay = '20 2 * * *';
        $this->fix_time_get_order_amazon = '1,30 * * * *'; 
        $this->fix_time_get_order_ebay = '5,35 * * * *';
        $this->fix_time_send_order_amazon = '10,40 * * * *';
        $this->fix_time_send_order_ebay = '10,40 * * * *';
        $this->fix_time_delete_product_amazon = '35 * * * *'; 
        $this->fix_time_delete_product_ebay = '30 4 * * *';
        $this->fix_time_sync_product_ebay = '55 5 * * *';
        
        $this->cronj_list = array();
        $this->spread = 4;
        $this->cron_user_list = array();
        $this->cron_user_id_list = array();
        $this->shop_name = array();
        $this->file_crontab = DIR_SERVER . '/assets/apps/users/cron/user_crontab.cron';
        
        $this->ebay_avail_site = array();
        $users = $this->get_user_conj_feed_active();
        $avail_users = array(); 
        $this->cron_user_list = $users;
        $this->imported_feed = $this->check_history_key_all('import_feed');
        foreach($users as $u){
            if(!isset($this->imported_feed[$u['id']])) continue;
            if (self::FREETAIL > 0) {
                $expire = $this->get_base_pk_zone_not_expire($u['id']);
                if(empty($expire)){
                    $create_user_date = strtotime($u['user_created_on']);
                    $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                    if($create_user_date - $expired <= 0){
                        continue;
                    }
                } 
            } 
            $this->cron_user_id_list[] = $u['id'];
            $this->shop_name[$u['id']] = str_replace(' ','_',$this->get_current_shop_name($u['id']));;
            $this->user_name[$u['id']] = $u['user_name'];
//            $this->feedbiz = new FeedBiz(array($u['user_name']));
            $this->feedbiz[$u['user_name']] = new ObjectBiz(array($u['user_name']));
            $db_id_shop = $this->feedbiz[$u['user_name']]->getDefaultShop($u['user_name']);
             
            $this->shop_id[$u['id']] = isset($db_id_shop['id_shop'])?$db_id_shop['id_shop']:0;
        }
        
        $this->spread = ceil(((sizeof($users) * 3) / 60) + 2); 
        $this->amazon_user_list = $this->getUserUpdateMarketCron('amazon');
        $this->ebay_user_list = $this->getUserUpdateMarketCron('ebay');
    }
    
    public function cronj_gen_tasks_file() {
        $this->cronj_list = array();
        // call list
        $this->cronj_gen_import_direct('feed');
        $this->cronj_gen_market_update_order('amazon','get');
        $this->cronj_gen_market_update_order('ebay','get');
        $this->cronj_gen_market_update_order('amazon','send');
        $this->cronj_gen_market_update_order('ebay','send'); 
        
        $this->cronj_gen_import_direct('stock');
        
        
        $this->cronj_gen_import_direct('offer');
        $this->cronj_gen_import_direct('order');
        $this->cronj_gen_market_update('amazon','product');
        $this->cronj_gen_market_update('amazon','offer');
        $this->cronj_gen_market_update('ebay','product','sync');
        $this->cronj_gen_market_update('ebay','product','delete');
        $this->cronj_gen_market_update('ebay','product');
        $this->cronj_gen_market_update('ebay','offer');
        
        
        
//        $this->cronj_gen_market_update('amazon','product','delete');
        
        
        // generate additional crontab file
        $output = '';
        
        foreach ($this->cronj_list as $id => $d) {
            
            $output .= "#UID $id \n";
            foreach ($d as $l) {
                if($id==9 && strpos($l,'amazon_get_orders_cron.php')===false)$l='#'.$l;
//                if($id==83 && strpos($l,'import_offer_cron.php')!==false)$l='#'.$l;
//                if( strpos($l,'ebay')!==false)$l='#'.$l;
//                if(!in_array($id,array(9,83,82)))$l='#'.$l;
//                if(in_array($id,array(83,82)) && !( strpos($l,'ebay_export_offer_cron.php')!==false || strpos($l,'ebay_sync_product_cron.php')!==false || strpos($l,'ebay_delete_product_cron.php')!==false || strpos($l,'ebay_export_product_cron.php')!==false  ) ) $l='#'.$l;
                $output .= "$l \n";
            }
        }
        $dir = dirname($this->file_crontab);
        if (!file_exists($dir)) {
            $old = umask(0);
            mkdir($dir, 0775, true);
            umask($old);
        }
//        echo $this->file_crontab;
        if (file_exists($this->file_crontab))
            unlink($this->file_crontab);
//        file_put_contents($this->file_crontab, $output); 
       
        $file = fopen($this->file_crontab,"w");
        fwrite($file,$output);
        fclose($file);
        if(!$this->update_file){
        exec('php '.DIR_SERVER.'/assets/apps/cronman/import_cronjob.php');
        }
    }
    
    public function getUserUpdateMarketCron($market = 'amazon'){
        if($market=='amazon'){
            if(!isset($this->amazon_runned_users)){
                $sql = "select user_id as id from histories where history_action like 'export_amazon%_synchronize' group by user_id ";
                $q = mysqli_query($this->db,$sql);
                    $id = array();
                    while($r = mysqli_fetch_assoc($q)){
                        $id[] =$r['id'];
                    }
                $add = "'".implode("','",$id)."'";
                $this->amazon_runned_users = $add;
            }
            $sql = "SELECT
                            ac.id_customer,
                            ac.user_name,
                            ac.id_shop,
                            ac.ext,ac.id_country,ac.cron_send_orders, 
                            u.user_created_on
                           
                    FROM
                            amazon_configuration ac 
                    INNER JOIN users u ON u.id = ac.id_customer and u.id in ($this->amazon_runned_users)
                    INNER JOIN offer_price_packages op ON op.ext_offer_sub_pkg = ac.ext
                    INNER JOIN user_package_details up ON up.id_users = u.id 
                    AND up.id_package = op.id_offer_price_pkg
                    WHERE
                            ac.active = '1' /*and ac.allow_automatic_offer_creation = '1'*/
                    GROUP BY
                            ac.id_customer,
                            id_country,
                            id_shop ";
             
        }else if($market=='ebay'){
            $sql = "SELECT
                    c.id_customer,
                    u.user_name,
                    offer_price_packages.ext_offer_sub_pkg as ext,
                    offer_price_packages.domain_offer_sub_pkg as domain,
                    offer_price_packages.id_site_ebay as id_site
                    from configuration c ,users u  
                    INNER JOIN user_package_details ON u.id = user_package_details.id_users
                    INNER JOIN offer_price_packages ON user_package_details.id_package = offer_price_packages.id_offer_price_pkg and offer_price_packages.id_offer_pkg = '3'
                    where c.name = 'EBAY_TOKEN' and u.id = c.id_customer
                    ";
        } 
        $q = mysqli_query($this->db,$sql);
        $out = array();
        while($r = mysqli_fetch_assoc($q)){
            $out[] =$r;
        }
        return $out;
    }
    
    public function getUserUpdateOrderMarketCron($market = 'amazon',$t_action='get'){
        if($market=='amazon'){ 
            if(!isset($this->amazon_runned_users)){
                $sql = "select user_id as id from histories where history_action like 'export_amazon%_synchronize' group by user_id ";
                $q = mysqli_query($this->db,$sql);
                    $id = array();
                    while($r = mysqli_fetch_assoc($q)){
                        $id[] =$r['id'];
                    }
                $add = "'".implode("','",$id)."'";
                $this->amazon_runned_users = $add;
            }
            $add = "'".implode("','",$id)."'";
            $sql = "SELECT
                            ac.id_customer,
                            ac.user_name,
                            ac.id_shop,
                            ac.ext,ac.id_country,ac.cron_send_orders, 
                            u.user_created_on
                           
                    FROM
                            amazon_configuration ac 
                    INNER JOIN users u ON u.id = ac.id_customer and u.id in ($this->amazon_runned_users)
                    INNER JOIN offer_price_packages op ON op.ext_offer_sub_pkg = ac.ext
                    INNER JOIN user_package_details up ON up.id_users = u.id 
                    AND up.id_package = op.id_offer_price_pkg
                    WHERE
                            ac.active = '1' /*and ac.allow_automatic_offer_creation = '1'*/
                    GROUP BY
                            ac.id_customer,
                            id_country,
                            id_shop ";
             
           
        }else if($market=='ebay'){
            $sql = "SELECT
                    c.id_customer,
                    u.user_name,
                    offer_price_packages.ext_offer_sub_pkg as ext,
                    offer_price_packages.domain_offer_sub_pkg as domain,
                    offer_price_packages.id_site_ebay as id_site
                    from configuration c ,users u  
                    INNER JOIN user_package_details ON u.id = user_package_details.id_users
                    INNER JOIN offer_price_packages ON user_package_details.id_package = offer_price_packages.id_offer_price_pkg and offer_price_packages.id_offer_pkg = '3'
                    where c.name = 'EBAY_TOKEN' and u.id = c.id_customer
                    ";
        } 
        $q = mysqli_query($this->db,$sql);
        $out = array();
        while($r = mysqli_fetch_assoc($q)){
            $out[] =$r;
        }
        return $out;
    }
     
    public function get_base_pk_zone_not_expire($id_user){
        $return = array(); 
        $add = ''; 
        if(strtolower($id_user) != 'all'){
            $add=" b.id_users = '$id_user' and "; 
        }
        $sql = "select bd.id_bill_detail,bd.id_package,id_users from billings_detail bd , billings b  , offer_packages opk  ,offer_price_packages pk "
                . "left join offer_sub_packages s on s.id_offer_sub_pkg = pk.id_offer_sub_pkg "
                 . "where $add b.id_billing = bd.id_billing and status_billing = 'active' "
                 . "and end_date_bill_detail >= '".date('Y-m-d')."'  "
                 . "and pk.id_offer_price_pkg = bd.id_package and opk.id_offer_pkg = pk.id_offer_pkg and pk.offer_pkg_type = 9  "
                 . "order by opk.id_offer_pkg asc,pk.id_offer_price_pkg asc";
       
        $q = mysqli_query($this->db,$sql);
        $return = array();
        while($v = mysqli_fetch_assoc($q)){
             $bdt_id = $v['id_bill_detail'];
             $pk_id = $v['id_package'];
             $u_id = $v['id_users'];
             $sql = "select bd.id_bill_detail from billings_detail bd , billings b "
                    . " where  b.id_billing = bd.id_billing and status_billing = 'active' "
                    . "and bd.id_package = '$pk_id' and b.id_users = '$u_id' order by bd.id_bill_detail desc limit 1";
             
             $q2 = mysqli_query($this->db,$sql);
             $return = array();
             $ch = mysqli_fetch_assoc($q2);
             
             if($bdt_id == $ch['id_bill_detail']){
                $return = $v;break;
             }
         }
         
         return $return;
    }
    public function get_pk_zone_purchased($id_user, $site_name, $ext){
        $return = array();
//        $base = $this->get_base_pk_zone_not_expire($id_user);
//        if(!empty($base))$return['base'] = $base;
        $add = '';
        
        if(strtolower($id_user) != 'all'){
            $add=" b.id_users = '$id_user' and "; 
        } 
        
         
        
        $sql = "SELECT bd.id_bill_detail,bd.id_package,id_users FROM billings_detail bd, billings b, offer_packages opk, 	
	offer_price_packages pk
        LEFT JOIN offer_sub_packages s ON s.id_offer_sub_pkg = pk.id_offer_sub_pkg
        join offer_price_packages tpk  on tpk.ext_offer_sub_pkg LIKE '$ext' and pk.offer_pkg_type = '4' 
        WHERE 	$add
          b.id_billing = bd.id_billing
        AND status_billing = 'active'
        AND (tpk.id_offer_price_pkg = bd.id_package || pk.id_offer_price_pkg = bd.id_package)
        AND opk.id_offer_pkg = tpk.id_offer_pkg
        AND opk.id_offer_pkg = pk.id_offer_pkg
        AND opk.name_offer_pkg LIKE '$site_name' 
        AND tpk.id_region = pk.id_region   group by opk.id_offer_pkg ASC, pk.id_offer_price_pkg ASC ORDER BY opk.id_offer_pkg ASC, pk.id_offer_price_pkg ASC";
        
        $q = mysqli_query($this->db,$sql);
       
        $r = mysqli_fetch_assoc($q);
        if(!empty($r))$return['pk'] = $r;
        return $return;
    }
    public function get_pk_not_expire($id_user=0, $site_name=null, $ext=null){
        
        $add=$add2=$add3=$add4= '';
        
        if(strtolower($id_user) != 'all'){
            $add=" b.id_users = '$id_user' and "; 
        } 
        
        if($site_name !== null){
            $add2 = " and opk.name_offer_pkg like '$site_name' ";
        }
        if($ext !== null){
            $add3 = " and pk.ext_offer_sub_pkg like '$ext' ";
        }
        
        $sql = "select bd.id_bill_detail,bd.id_package,id_users from billings_detail bd , billings b $add2, offer_packages opk  ,offer_price_packages pk "
                . "left join offer_sub_packages s on s.id_offer_sub_pkg = pk.id_offer_sub_pkg "
                 . "where $add b.id_billing = bd.id_billing and status_billing = 'active' "
                 . "and end_date_bill_detail >= '".date('Y-m-d')."'  "
                 . "and pk.id_offer_price_pkg = bd.id_package and opk.id_offer_pkg = pk.id_offer_pkg and pk.offer_pkg_type <>2 $add3 $add4"
                 . "order by opk.id_offer_pkg asc,pk.id_offer_price_pkg asc";
       
        $q = mysqli_query($this->db,$sql);
        $return = array();
        while($v = mysqli_fetch_assoc($q)){
             $bdt_id = $v['id_bill_detail'];
             $pk_id = $v['id_package'];
             $u_id = $v['id_users'];
             $sql = "select bd.id_bill_detail from billings_detail bd , billings b "
                    . " where  b.id_billing = bd.id_billing and status_billing = 'active' "
                    . "and bd.id_package = '$pk_id' and b.id_users = '$u_id' order by bd.id_bill_detail desc limit 1";
             
             $q2 = mysqli_query($this->db,$sql);
             $return = array();
             $ch = mysqli_fetch_assoc($q2);
             
             if($bdt_id == $ch['id_bill_detail']){
                $return = $v;break;
             }
         }
         
         return $return;
    }
    
    public function cronj_gen_market_update($market = 'amazon',$type='offer',$t_action='update'){
        $action = $this->app_path;
        $url = 'crond_'.$t_action.'_'.$type.'_' . $market . '_url';
        $padd=$this->cmd_padd;
        $run = true;
        $avail_users = array(); 
        
        if($market=='amazon'){
            $market_id = 2;
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;
            foreach($user_list as $u){
                $user_id = $u['id_customer'];
                if(!isset($this->user_name[$user_id]))continue;
//                if(!in_array($user_id,$this->cron_user_id_list))continue;
                $user_name = $u['user_name']; 
                $shop_id = $u['id_shop']; 
                $db_id_shop = $this->shop_id[$user_id];
                if($shop_id != $db_id_shop) continue;
                $ext = $u['ext'];
                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext);
                    if(empty($expire)){
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if($create_user_date - $expired <= 0){
                            continue;
                        }
                    } 
                }
                
                $shop_name = $this->shop_name[$user_id];
                
                $freq = "frequency_".$t_action."_".$type."_" . $market;
                $k = $t_action =='update'?'synchronize':'delete';
                $key = "export_amazon{$ext}_{$k}_".$type;
                $run= ($t_action =='delete')?false:$run;
                
                if($this->mode_fix){
                    
                    $ft_txt = "fix_time_".$t_action."_".$type."_" . $market;
                    $txt_time = $this->$ft_txt; 
                }else{
                $next_time = $this->get_next_action($user_id, $key, $this->$freq);
//                 $h = date('G', $next_time);
//                $m = 1 * date('i', $next_time);
//                echo $user_id.' '.$key.' '.$next_time.' '.$this->$freq.' '.$h.'-'.$m.'<br>';
                $txt_time = $this->loadBalanceCronTask($next_time,$run);
                }
                if(!$run){$txt_time = '#'.$txt_time;}
                $uri =  "".$this->$url." $user_id $user_name $ext $shop_id $shop_name $market_id";
                $this->cronj_list [$user_id] [] = $txt_time . $action . $uri.$padd;
            }
            
            
        }else if($market=='ebay'){
            $market_id = 3;
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;
             
            foreach($user_list as $u){
                
                $user_id = $u['id_customer']; 
                if(!isset($this->user_name[$user_id]))continue;
//                if(!in_array($user_id,$this->cron_user_id_list)){   continue;}
                $user_name = $u['user_name']; 
//                $this->feedbiz = new ObjectBiz(array($user_name));
//                $db_id_shop = $this->feedbiz->getDefaultShop($user_name);
                $db_id_shop = $this->shop_id[$user_id];
                $id_shop = $db_id_shop;
                
                  
                if(!isset($this->ebay_avail_site[$user_id]))
                    $this->ebay_avail_site[$user_id] = $this->feedbiz->getEbayAvailSite($user_name);
                $id_site = $u['id_site'];
                  
                if(!($user_id=='38'&&$id_site=='0'))
                if(!in_array($id_site, $this->ebay_avail_site[$user_id])){continue;}
                
                $run=false;
                if(in_array($user_id,array(38,53,26,82,83))){//||(in_array($user_id,array(53))&&$type=='proudct')){
                    $run = true; 
                } 
                $ext = $u['ext'];
                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext);
                    if(empty($expire)){
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if($create_user_date - $expired <= 0){
                            continue;
                        }
                    } 
                }
                $shop_name = $this->shop_name[$user_id];
                $domain = $u['domain'];
                $freq = "frequency_".$t_action."_".$type."_" . $market;
                $key = "export_{$domain}";
                
                if($this->mode_fix){
                    $ft_txt = "fix_time_".$t_action."_".$type."_" . $market;
                     
                    $txt_time = $this->$ft_txt; 
                }else{
                    $next_time = $this->get_next_action($user_id, $key, $this->$freq);
                    $txt_time = $this->loadBalanceCronTask($next_time,$run); 
                }
                if(!$run){$txt_time = '#'.$txt_time;}
                $uri =  "".$this->$url." $user_id $user_name $id_site $domain $shop_name $market_id";
                $this->cronj_list [$user_id] [] = $txt_time . $action . $uri.$padd;
            }
            
        }
        
    }
    
    
    public function cronj_gen_market_update_order($market = 'amazon',$t_action='get'){
        $action = $this->app_path;
        $url = 'crond_'.$t_action.'_order_' . $market . '_url';
        $padd=$this->cmd_padd;
        
        $avail_users = array(); 
        $run = true;
        
        
        if($market=='amazon'){
            $market_id = 2;
//            $user_list = $this->getUserUpdateOrderMarketCron($market,$t_action);
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;
            foreach($user_list as $u){
                $user_id = $u['id_customer'];
                $cron_status = isset($u['cron_send_orders'])?(int)$u['cron_send_orders']:1;
                if(!isset($this->user_name[$user_id]))continue;
//                if(!in_array($user_id,$this->cron_user_id_list))continue;
                $user_name = $u['user_name']; 
                $shop_id = $u['id_shop'];
                $db_id_shop = $this->shop_id[$user_id];
                if($shop_id != $db_id_shop) continue;
                $ext = $u['ext'];
                $id_site = $u['id_country'];
                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext);
                    if(empty($expire)){
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if($create_user_date - $expired <= 0){
                            continue;
                        }
                    } 
                }
                
                $shop_name = $this->shop_name[$user_id];
                
                $freq = "frequency_".$t_action."_order_" . $market;
                $key = "export_amazon{$ext}_{$t_action}_order"; 
                
                if($this->mode_fix){
                    $ft_txt = "fix_time_".$t_action."_order_" . $market;
                    $txt_time = $this->$ft_txt; 
                }else{
                $next_time = $this->get_next_action($user_id, $key, $this->$freq);
//                 $h = date('G', $next_time);
//                $m = 1 * date('i', $next_time);
//                echo $user_id.' '.$key.' '.$next_time.' '.$this->$freq.' '.$h.'-'.$m.'<br>';
                $txt_time = $this->loadBalanceCronTask($next_time,$run);
                } 
                if(!$run){$txt_time = '#'.$txt_time;}
                if(in_array($t_action,array('send','stock'))){
                    $uri =  "".$this->$url." $user_id $user_name $market_id $id_site $shop_id $shop_name $cron_status"; 
                }else{
                    $uri =  "".$this->$url." $user_id $user_name $ext $shop_id $shop_name $market_id $cron_status"; 
                }
               $this->cronj_list [$user_id] [] = $txt_time . $action . $uri.$padd;
            }
            
            
        }else if($market=='ebay'){
            if($t_action=='send'){
                $run = false;
            }
            $market_id = 3;
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;
             
                
            foreach($user_list as $u){
                
                $user_id = $u['id_customer']; 
                if(!isset($this->user_name[$user_id]))continue;
//                if(!in_array($user_id,$this->cron_user_id_list)){   continue;}
                $user_name = $u['user_name']; 
                
                $db_id_shop = $this->shop_id[$user_id];
 
                if(!isset($this->ebay_avail_site[$user_id])){ 
                    if(isset($this->feedbiz[$user_name])){
                        $this->ebay_avail_site[$user_id] = $this->feedbiz[$user_name]->getEbayAvailSite($user_name);
                    }
                }
                    
                $id_site = $u['id_site'];
                
                if(!in_array($id_site, $this->ebay_avail_site[$user_id])){continue;}
                
                
                $ext = $u['ext'];
                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext);
                    if(empty($expire)){
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if($create_user_date - $expired <= 0){
                            continue;
                        }
                    } 
                }
                $shop_id = $db_id_shop ;
                $shop_name = $this->shop_name[$user_id];
                $domain = $u['domain'];
                $freq = "frequency_".$t_action."_order_" . $market;
                $key = "ebay_{$t_action}_order_{$domain}";
                $run=true;
                $cron_status = $this->get_ebay_cron_status($user_id,$id_site);
                if($this->mode_fix){
                    $ft_txt = "fix_time_".$t_action."_order_" . $market;
                    $txt_time = $this->$ft_txt;
                }else{
                $next_time = $this->get_next_action($user_id, $key, $this->$freq);
                $txt_time = $this->loadBalanceCronTask($next_time,$run); 
                }
                $run= ($t_action =='stock')?false:$run;
                if(!$run){$txt_time = '#'.$txt_time;}
                if(in_array($t_action,array('send','stock'))){
                    $uri =  "".$this->$url." $user_id $user_name $market_id $id_site $shop_id $shop_name $cron_status"; 
                }else{
                    $uri =  "".$this->$url." $user_id $user_name $id_site $domain $shop_name $market_id $cron_status"; 
                }
                
                $this->cronj_list [$user_id] [] = $txt_time . $action . $uri.$padd;
            }
            
        }
        
    }
    function get_ebay_cron_status($id_customer,$id_site){
        $sql = "select value from ebay_configuration where id_customer='$id_customer' and id_site = '$id_site' and name = 'EBAY_CRON_ORDER' ";
        $q = mysqli_query($this->db,$sql);
        if(mysqli_num_rows($q)>0){
             $data = mysqli_fetch_assoc($q);
             return $data['value'];
        }else{
            return '1';
        }
    }
    function get_user_conj_feed_active(){
        $sql = "select id,value,user_name from users u inner join configuration c on u.id = c.id_customer where u.user_status = '1' and user_cronj_status = '1' and c.name = 'FEED_BIZ' order by u.id asc";
        
        $q = mysqli_query($this->db,$sql);
        $out = array();
        while($r = mysqli_fetch_assoc($q)){
            
            $val = unserialize(base64_decode($r['value']));
            if(!(isset($val['verified'])&& $val['verified']!='')){
               continue;
            }
            $out[]=$r;
        } 
        return $out;
    } 
     public function cronj_gen_import_direct($type = 'offer') {
        $action = $this->app_path;
        $padd=$this->cmd_padd;//"> /dev/null 2>&1";
        $url = 'crond_import_' . $type . '_url';
        $arr = $this->get_cron_time_user_import($type);
        $run = true;
        foreach ($arr as $u) {
            
            $id = $u ['id']; 
            $txt_time = $u ['cron_time'];
            $time = $u ['time'];
            if(!$run){$txt_time = '#'.$txt_time;}
//            $add= '';
//            if($type=='stock'){
                $add = $this->shop_id[$id];
//                if($add==0)return;
//            }
            $uri =  "".$this->$url." $id ".$add;//.urlencode($this->my_feed_model->get_json_for_import($id))."'"; 
            $this->cronj_list [$id] [] = $txt_time . $action . $uri.$padd;
            
        }
    }
    function check_history_key_all($key='import_'){
        $sql="select user_id as id from histories where    history_action = '{$key}' group by user_id order by user_id asc  ";
//        $sql="select user_id as id from histories where    history_action like '{$key}%' group by user_id order by user_id asc  ";
        $q = mysqli_query($this->db,$sql);
        $out = array();
        while($d = mysqli_fetch_assoc($q)){
            $out[$d['id']] = true;
        }
        return $out;
    }
     function check_history_key($id,$key='import_'){
        $sql="select id from histories where user_id = '$id' and history_action like '{$key}%' order by history_date_time desc limit 1";
        $q = mysqli_query($this->db,$sql);
        if ( mysqli_num_rows($q) > 0){
            return TRUE;
        } 
            return false;
         
        
    }
    function get_current_shop_name($id){
        $sql = "select value from configuration where name in ('FEED_SHOP_INFO') and id_customer = '$id' limit 1";
        $q = mysqli_query($this->db,$sql);
        if(mysqli_num_rows($q)> 0){
            $d = mysqli_fetch_assoc($q);
            $v = unserialize(base64_decode($d['value']));
            if(isset($v['name']))
            return $v['name'];
        }
        
        return false;
    }
    function get_general($id_customer=null) 
    {
        $mainconfiguration = array();
        $sql = "select name,value ,config_date from configuration where name in ('FEED_SHOP_INFO') and id_customer = '$id_customer' limit 1 ";
        $q = mysqli_query($this->db,$sql);
        while($row = mysqli_fetch_assoc($q))
        {
            $mainconfiguration[$row['name']]['value'] = $row['value'];
            $mainconfiguration[$row['name']]['config_date'] = $row['config_date'];
        }
        
        return $mainconfiguration;
    }
    
    public function get_cron_time_user_import($type = 'feed') {
        $users = $this->cron_user_list;
        $avail_users = array();
        
        
        foreach ($users as $u) {
            $id = $u ['id'];
            $run=TRUE;
            // $verified = $this->my_feed_model->check_verified_import($id);
            
            if(!isset($this->imported_feed[$id])){
                continue;
            }
            $freq = "frequency_feed_" . $type;
            $key = 'import_';
            if ($type == 'order') {
                $config_data = $this->get_general($id);
                if (isset($config_data ['FEED_SHOP_INFO'])) {
                    $feed = unserialize(base64_decode($config_data ['FEED_SHOP_INFO'] ['value']));
                    if (!isset($feed ['url'] ['shippedorders'])) {
                        continue;
                    }
                } else {
                    continue;
                }
                $key .= $type;
                $run=TRUE;
            }elseif($type=='feed'){
                $key .= $type;
                $run=TRUE;
            }else{
                
                $run=TRUE;
            }
            if($this->mode_fix){
                $ft_txt = 'fix_time_feed_'.$type;
//                if($type=='feed'){
//                    $txt = '0 0 * * * ';
//                }else if($type=='offer'){
//                    $txt = '5,35 * * * * ';
//                }else if($type=='order'){
//                    $txt = '7 * * * * ';
//                }
                $txt = $this->$ft_txt;
                $next_time = '';
            }else{
                $next_time = $this->get_next_action($id, $key, $this->$freq); 
                $txt =  $this->loadBalanceCronTask($next_time,$run);
            }
            if(!$run){$txt = '#'.$txt;}
            $avail_users [] = array(
                'id' => $id,
                'cron_time' => $txt,
                'time' => $next_time
            );
        }
        return $avail_users;
    }
    
    function get_next_action($id,$key = 'import_',$min = 15){
        if($key == 'import_'){
            $sql = "select history_date_time from histories where user_id = '$id' and history_action like '{$key}%' and history_action not like 'import_order' order by history_date_time desc limit 1";
        }else{
            $sql = "select history_date_time from histories where user_id = '$id' and history_action like '{$key}%' order by history_date_time desc limit 1";
        }
        
        //echo $sql.'<br>';
        $q = mysqli_query($this->db,$sql);
        $min_no=2;
        if ( mysqli_num_rows($q) > 0){
            $d = mysqli_fetch_assoc($q); 
         
            $time =  strtotime($d['history_date_time']. " +{$min} minutes") ;
            $cur_time = strtotime("-2 minutes");
            $run_time = $time; 
//            echo $this->db->last_query().'<br>';
//            echo $key.' '.$cur_time.' '.date('r',$cur_time) . ' '. $time.' '.date('r',$time).' '.$q[0]['history_date_time']. " +{$min} minutes".'<br>';
            if($cur_time > $time){
                
;                $run_time = strtotime("+{$min_no} minutes"); 
            }
        }else{
//            echo '2x<br>';
            $run_time = strtotime("+{$min_no} mins");
        }
        return $run_time;
    }
    
    public function loadBalanceCronTask($next_time,$run=true){
        $spread = $this->spread;
            $h = date('G', $next_time);
                $m = 1 * date('i', $next_time);
                $loop_end = false;
                while (!$loop_end) {
                    if (isset($this->list_tasks [$h . "_" . $m]) && $this->list_tasks [$h . "_" . $m] > $spread) {
                        $next_time += 61;
                        $h = date('G', $next_time);
                        $m = 1 * date('i', $next_time);
                    } else {
                        $loop_end = true;
                        if($run){
                            if (isset($this->list_tasks [$h . "_" . $m])) {
                                $this->list_tasks [$h . "_" . $m] ++;
                            } else {
                                $this->list_tasks [$h . "_" . $m] = 1;
                            }
                        }
                        break;
                    }
                }

            return ($run?'':'#')."$m $h * * * ";
    }
}        
 
$gc = new GENCRON();
if(isset($_REQUEST['test'])){
    $out = $gc->get_pk_zone_purchased(307,'Amazon','.de');
    echo '<pre>';
    if(!empty($out)){
        echo ' not empty ';
        print_r($out);
    }  else {
        print_r($out);    
    }
    echo '</pre>';
}else{
    $gc->cronj_gen_tasks_file();
}
?>