<?php
include(dirname(__FILE__) . '/../libraries/include_lib_so_lite.php');
if (strtoupper(substr(PHP_OS, 0, 3))!== 'WIN') {
    exec("ps aux | grep cron_gen.php|grep -v grep | grep -v ' /bin/sh'", $output, $result);
    $count=0;
    foreach ($output as $line) {
        if (strpos($line, "cron_gen.php")) {
            $count++;
        }
    }
    if ($count>1) {
        die('duplicate calling'.print_r($output, true));
    }
}
ini_set('display_errors', 1);
error_reporting(E_ALL|E_STRICT);
$system_path = 'system';
define('BASEPATH', str_replace("\\", "/", $system_path));
define('USER_DB_PREFIX', 'fb_');
include(dirname(__FILE__) . '/../application/config/config.php');
include(dirname(__FILE__) . '/../application/config/database.php');
require_once(dirname(__FILE__) . '/../application/libraries/Ebay/ObjectBiz.php');
$dbx = mysqli_connect($db['default']['hostname'], $db['default']['username'], $db['default']['password']) or die('Could not connect to server.');
mysqli_select_db($dbx, $db['default']['database']) or die('Could not select database.');
$dbu = mysqli_connect($db['backend']['hostname'], $db['backend']['username'], $db['backend']['password']) or die('Could not connect to server.');

$update_file = isset($_REQUEST['update_file'])?true:false;
$host_code = isset($config['host_code'])?$config['host_code']:'front-host';
$host_frontend=false;
if(strpos($host_code,'front') !==false){
    $host_frontend=true;
}
$host_split=true;

if (!function_exists('cli_log_exception')) {
    function cli_log_exception($severity, $message, $filepath, $line)
    {
        global $config,$argv,$levels,$last_query;
        $host = str_replace(array("https://", "http://", '/'), '', $config['base_url']);
        $call_file = isset($argv[0])?"(".$argv[0].")":'';
        if(!empty($argv)){
        $call = implode(' ', $argv);
        }else{
            $call='on frontend';
        }
        $add_query = !empty($last_query)?$last_query:'';
        $severity = (! isset($levels[$severity])) ? $severity : $levels[$severity];


        $key = md5($severity.'  --> '.$message. ' '.$filepath.' '.$line ."\n");
        if (!isset($levels['key'])) {
            $levels['key'] = array();
        }
        if (isset($levels['key'][$key])) {
            return;
        }
        $levels['key'][$key] = $key;
//                $his_dir = dirname(__FILE__).'/mail_history';
//                if(!file_exists($his_dir)){
//                    mkdir($his_dir,777,true);
//                }
//                $his_file_path = $his_dir.'/'.$key;
//                if(file_exists($his_file_path)){
//                   return true;
//                }
//                file_put_contents($his_file_path,'');
                $mail_content ='Error on '.$host.' @ PHP Cli '.$call_file;
        $subject = $mail_content ;
        $mail_content .="\n".'Server Time : '.date('c')."\n";
        $mail_content .=  "\n".$severity.'  --> '.$message. ' '.$filepath.' '.$line ."\n";
        $mail_content .= 'Command : '.$call."\n";
//                $mail_content .= print_r($levels,true);
                if (!empty($add_query)) {
                    $mail_content .= 'Last query : '.$add_query."\n";
                }

        $send_to=array();
        $send_to[]='palm@common-services.com';
        $e = new Exception;
        $trace = $e->getTraceAsString();
        $mail_content .= "\n\n".$trace."\n\n";
//                log_message('error',$trace) ;
                require_once dirname(__FILE__) . '/../application/libraries/Swift/swift_required.php';
        try {
            $transport = Swift_SmtpTransport::newInstance();
            $mailer = Swift_Mailer::newInstance($transport);
            $swift_message = Swift_Message::newInstance();
            $swift_message->setSubject($subject)
                            ->setFrom(array('support@feed.biz' => ''.$host.''))
                            ->setTo($send_to)
                            ->setBody('<pre>'.$mail_content.'</pre>', 'text/html');
            $out = $mailer->send($swift_message) ;
        } catch (Exception $ex) {
        }
        return true;
    }
    set_error_handler('cli_log_exception');
}


class GENCRON
{
    const FREETAIL = 0;

    public function __construct()
    {
        global $dbx,$update_file,$backend_db_connector,$dbu;
        $this->app_path = "  php  ".DIR_SERVER."/assets/apps/";
        $this->cron_serial_path = "  php -n ".DIR_SERVER."/cron_serial/";
        $this->cmd_padd = "";
        $this->mode_fix = true;
        $this->cronj_list = array();
        $this->cronj_serial_list = array();
        $this->db = $dbx;
        $this->dbu = $dbu;
        $this->update_file = $update_file;
        $server_busy = false;
        $this->server_busy = $server_busy;
        $this->file_crontab = USERDATA_PATH.'cron/user_crontab.cron';
        $this->deny_txt = '#access deny# ';
        $this->init_cron();

        $this->cronj_list = array();
        $this->spread = 4;
        $this->cron_user_list = array();
        $this->cron_user_id_list = array();
        $this->shop_name = array();
        $this->user_time_zone = array();

        $this->ebay_avail_site = array();
        $users = $this->get_user_conj_feed_active();
        $avail_users = array();
        $this->cron_user_list = $users;
        $this->imported_feed = $this->check_history_key_all('import_feed');
        $ebay_avail_path = dirname(__FILE__).'/ebay_avail_list.json';
        $ebay_avail_json = '';
        if (file_exists($ebay_avail_path)) {
            if ((filemtime($ebay_avail_path) >= time() - 3600)) {
                $ebay_avail_json = file_get_contents($ebay_avail_path);
                $this->ebay_avail_site = json_decode($ebay_avail_json, true);
            }
        }

        foreach ($users as $u) {
            if (!isset($this->imported_feed[$u['id']])) {
                continue;
            }
            if (self::FREETAIL > 0) {
                $expire = $this->get_base_pk_zone_not_expire($u['id']);
                if (empty($expire)) {
                    $create_user_date = strtotime($u['user_created_on']);
                    $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                    if ($create_user_date - $expired <= 0) {
                        continue;
                    }
                }
            }
            if (isset($u['user_time_zone'])) {
                $this->user_time_zone[$u['id']] = $u['user_time_zone']*1;
            }
            $this->feedbiz = new ObjectBiz(array($u['user_name']));
            $db_id_shop = $this->feedbiz->getDefaultShop($u['user_name']);
            if (empty($db_id_shop)||$db_id_shop==0) {
                continue;
            }
            $this->cron_user_id_list[] = $u['id'];
            $this->shop_name[$u['id']] = str_replace(' ', '_', $this->get_current_shop_name($u['id']));
            ;
            $this->user_name[$u['id']] = $u['user_name'];
//            $this->feedbiz = new FeedBiz(array($u['user_name']));
            if (!isset($this->ebay_avail_site[$u['id']])) {
//                $out = $this->feedbiz->getEbayAvailSite($u['user_name']);
                $out = $this->check_ebay_export_past($u['user_name']);
                $this->ebay_avail_site[$u['id']] = $out;
            }

            $this->shop_id[$u['id']] = isset($db_id_shop['id_shop'])?$db_id_shop['id_shop']:0;
            unset($this->feedbiz);
            unset($backend_db_connector);
        }

        $ebay_new_json = json_encode($this->ebay_avail_site);
        if (strcmp($ebay_avail_json, $ebay_new_json) !== 0) {
            file_put_contents($ebay_avail_path, $ebay_new_json);
        }

        $this->spread = ceil(((sizeof($users) * 3) / 60) + 2);
        $this->amazon_user_list = $this->getUserUpdateMarketCron('amazon');
        $this->ebay_user_list = $this->getUserUpdateMarketCron('ebay');
        $this->mirakl_user_list = $this->getUserUpdateMarketCron('mirakl');
        $this->cronjob_custom_time = $this->get_all_custom_time();
        $this->amazon_fba_master = $this->getFbaMaster('amazon');
        $this->amazon_fba_user = $this->getFbaUserCron('amazon');
        $this->amazon_log_display_user = $this->getFbaRepricingUserCron('amazon');
        $this->messaging_active = $this->getMessagingActive();
    }
    private function init_cron()
    {
        global $host_frontend,$host_split;
        if($host_frontend){
            if($host_split){
                $sql = "select * from cron_tasks where cron_shop in (1,2) and cron_status in (1,3) order by sort_by asc";
            }else{
                $sql = "select * from cron_tasks where cron_status in (1,3) order by sort_by asc";
            }

        }else{
            if($host_split){
                $sql = "select * from cron_tasks where cron_shop in (0,2) and cron_status in (1,3) order by sort_by asc";
            }else{
                $sql = "select * from cron_tasks where cron_shop in (2) and cron_status in (1,3) order by sort_by asc";
            }

        }
        $q = mysqli_query($this->db, $sql);
        $this->cron_info_list = array();
        $this->cron_id_list = array();
        while ($r = mysqli_fetch_assoc($q)) {
            $this->cron_info_list[$r['cron_id']] = $r;
            $type = $r['cron_type'];
            $action = str_replace(' ', '_', $r['cron_action']);
            $single = $r['cron_for_all_site'];
            $market = $r['cron_marketplace'];
            $time = $this->server_busy?$r['cron_busy_time']:$r['cron_time'];
            $time = empty($time)?$r['cron_time']:$time;
            $offen = $r['cron_offen_minute'];
            $file = $r['cron_file'];
            $this->cron_serial_mode_list[$file] = isset($r['cron_serial_mode'])?$r['cron_serial_mode']:0;

            if ($single) {
                if(!empty($market)){
                    $file_var = "crond_{$action}_{$type}_{$market}_url";
                    $offen_var = "frequency_{$action}_{$type}_{$market}";
                    $cron_time_var = "fix_time_{$action}_{$type}_{$market}";
                }else{
                $file_var = "crond_{$action}_{$type}_url";
                $offen_var = "frequency_feed_{$type}";
                $cron_time_var = "fix_time_feed_{$type}";
                }
            } else {
                $file_var = "crond_{$action}_{$type}_{$market}_url";
                $offen_var = "frequency_{$action}_{$type}_{$market}";
                $cron_time_var = "fix_time_{$action}_{$type}_{$market}";
            }
//           echo $file_var.' '.$offen_var.' '.$cron_time_var.'<br>';
           $this->$file_var = $file;
            $this->$offen_var = $offen;
            $this->$cron_time_var = $time;
            $this->cron_id_list[$file]=$r['cron_id'];
        }

        $sql = "select * from cronjob_status cs , cron_tasks c where  status=0 and  c.cron_id = cs.cron_id and c.cron_status = 1   ";
        $q = mysqli_query($this->db, $sql);
        $this->cron_deny_list = array();
        while ($r = mysqli_fetch_assoc($q)) {
            $this->cron_deny_list[] = $r;
            $type = $r['cron_type'];
            $action = str_replace(' ', '_', $r['cron_action']);
            $single = $r['cron_for_all_site'];
            $market = $r['cron_marketplace'];
            $time = $this->server_busy?$r['cron_busy_time']:$r['cron_time'];
            $time = empty($time)?$r['cron_time']:$time;
            $offen = $r['cron_offen_minute'];
            $file = $r['cron_file'];
            $all_user = $r['cron_for_all_user'];
            $user_id = $r['id_customer'];
            $rel_cron_id = $r['rel_cron_id'];
            $site = empty($r['site'])?'':strtolower($r['site']);
            $ext=$site.$r['ext'];
            $this->cron_id_list[$file]=$r['cron_id'];
            $status = $r['status']==0?true:false;
            if ($single) {
                if (!isset($this->import_direct_deny_user)) {
                    $this->import_direct_deny_user=array();
                }
                $this->import_direct_deny_user[$type][$user_id]=$status;
            } else {
                if (strpos($type, 'order') !== false) {
                    if (!isset($this->market_update_order_deny_user)) {
                        $this->market_update_order_deny_user=array();
                    }
                    $this->market_update_order_deny_user[$market][$action][$user_id][$ext]=$status;
                } else {
                    if (!isset($this->market_update_deny_user)) {
                        $this->market_update_deny_user=array();
                    }
                    $this->market_update_deny_user[$market][$type][$action][$user_id][$ext]=$status;
                }
            }
            if (!empty($rel_cron_id)) {
                if($host_frontend){
                    if($host_split){
                        $sql = "select * from  cron_tasks c where  cron_shop in (1,2) and c.cron_id in (".$rel_cron_id.") and c.cron_status = 3   ";
                    }else{
                        $sql = "select * from  cron_tasks c where   c.cron_id in (".$rel_cron_id.") and c.cron_status = 3   ";
                    }
                }else{
                    if($host_split){
                        $sql = "select * from  cron_tasks c where  cron_shop in (0,2) and c.cron_id in (".$rel_cron_id.") and c.cron_status = 3   ";
                    }else{
                        $sql = "select * from  cron_tasks c where  cron_shop in (2) and  c.cron_id in (".$rel_cron_id.") and c.cron_status = 3   ";
                    }
                }
//                $sql = "select * from  cron_tasks c where   c.cron_id in (".$rel_cron_id.") and c.cron_status = 3   ";
                $qb= mysqli_query($this->db, $sql);

                while ($r = mysqli_fetch_assoc($qb)) {
                    $this->cron_deny_list[] = $r;
                    $type = $r['cron_type'];
                    $action = str_replace(' ', '_', $r['cron_action']);
                    $single = $r['cron_for_all_site'];
                    $market = $r['cron_marketplace'];
                    $time = $this->server_busy?$r['cron_busy_time']:$r['cron_time'];
                    $time = empty($time)?$r['cron_time']:$time;
                    $offen = $r['cron_offen_minute'];
                    $file = $r['cron_file'];
                    $all_user = $r['cron_for_all_user'];
                    $this->cron_id_list[$file]=$r['cron_id'] ;
                    if ($single) {
                        if (!isset($this->import_direct_deny_user)) {
                            $this->import_direct_deny_user=array();
                        }
                        $this->import_direct_deny_user[$type][$user_id]=true;
                    } else {
                        if (strpos($type, 'order') !== false) {
                            if (!isset($this->market_update_order_deny_user)) {
                                $this->market_update_order_deny_user=array();
                            }
                            $this->market_update_order_deny_user[$market][$action][$user_id][$ext]=true;
                        } else {
                            if (!isset($this->market_update_deny_user)) {
                                $this->market_update_deny_user=array();
                            }
                            $this->market_update_deny_user[$market][$type][$action][$user_id][$ext]=true;
                        }
                    }
                }
            }
        }
    }

    public function cronj_gen_tasks_file()
    {
        $this->cronj_list = array();
        // call list
        if (isset($this->cron_info_list)) {
            foreach ($this->cron_info_list as $r) {
                $cron_serial = empty($r['cron_serial'])||((isset($r['cron_serial'])&&$r['cron_serial']==0))?false:true;
                $target_serial = empty($r['target_cron_serial'])||((isset($r['target_cron_serial'])&&$r['target_cron_serial']==0))?false:$r['target_cron_serial'];
                $type = $r['cron_type'];
                $action = str_replace(' ', '_', $r['cron_action']);
                $single = $r['cron_for_all_site'];
                $market = $r['cron_marketplace'];
                $time = $this->server_busy?$r['cron_busy_time']:$r['cron_time'];
                $time = empty($time)?$r['cron_time']:$time;
                $offen = $r['cron_offen_minute'];
                $param_type = isset($r['param_type'])?$r['param_type']:0;
                $param_value = !empty($r['add_param_value'])?$r['add_param_value']:'';
                $check_master = !empty($r['check_master'])?true:false;
                $file = $r['cron_file'];
                $all_user = $r['cron_for_all_user'];
//                $cron_serial=false;
                if ($all_user) {
                    $ak = 'All users';
                    if (!isset($this->cronj_list[$ak])) {
                        $this->cronj_list[$ak]=array();
                    }
                    $this->cronj_list[$ak][] =  $time.' '.$this->app_path.$file;
                } else {
                    if ($single) {
                        if(!empty($market)){
                            $this->cronj_gen_market_direct($market,$action, $type, $param_type,$cron_serial);
                        }else{
                            $this->cronj_gen_import_direct($action, $type, $param_type,$cron_serial,$target_serial);
                        }
                    } else {
                        if (strpos($type, 'order') !== false) {
                            $this->cronj_gen_market_update_order($market, $action, $cron_serial, $param_type,$param_value,$check_master);
                        } else {
                            $this->cronj_gen_market_update($market, $type, $action, $cron_serial, $param_type,$param_value,$check_master);
                        }
                    }
                }
            }
        }
//        $this->cronj_gen_import_direct('feed');
//        $this->cronj_gen_market_update_order('amazon','get');
//        $this->cronj_gen_market_update_order('ebay','get');
//        $this->cronj_gen_market_update_order('amazon','send');
//        $this->cronj_gen_market_update_order('ebay','send');
//
//        $this->cronj_gen_import_direct('stock');
//
//
//        $this->cronj_gen_import_direct('offer');
//        $this->cronj_gen_import_direct('order');
//        $this->cronj_gen_market_update('amazon','product');
//        $this->cronj_gen_market_update('amazon','offer');
//        $this->cronj_gen_market_update('ebay','product','sync');
//        $this->cronj_gen_market_update('ebay','product','delete');
//        $this->cronj_gen_market_update('ebay','product');
//        $this->cronj_gen_market_update('ebay','offer');



//        $this->cronj_gen_market_update('amazon','product','delete');


        // generate additional crontab file
        $output = '';
//        print_r($this->user_time_zone);


        foreach ($this->cronj_list as $id => $d) {
            $output .= "#UID $id \n";
            if (isset($this->cronj_serial_list[$id])) {
                $dx=$this->cronj_serial_list[$id];
                foreach ($dx as $file => $list) {
                    $f_re = str_replace('.php', '', $file);
                    $gen_file_name = 'cron_u'.$id.'_'.$f_re.'.txt';
                    $time='';

                    if ($id==69 || $id==76|| $id==35) {
                        foreach ($list as $k=>$v) {
                            $list[$k]['deny'] = 1;
                        }
                    }
                    if ($id!=26) {
                        foreach ($list as $k=>$v) {
                            $l = $list[$k]['cmd'];
                            if (strpos($l, 'check')!==false) {
                                $list[$k]['deny'] = 1;
                            }
                        }
                    }
//                    if($id==26){
//                        foreach($list as $k=>$v){
//                            $l = $list[$k]['cmd'];
//                            if( strpos($l,'amazon_update_products_cron.php')!==false && strpos($l,'.es')!==false ){
//                                $list[$k]['deny'] = 1;
//                            }
//                        }
//
//
//                    }
                    if (empty($list[0]['time'])) {
                        continue;
                    }

                    $time = $list[0]['time'];
                    if (isset($this->user_time_zone[$id]) && $this->user_time_zone[$id] != 0) {
                        $cron_time = trim($time);
                        $time = $this->get_new_cron_time($cron_time, $this->user_time_zone[$id]);
                    }
                    $path = $this->cron_serial_path .$gen_file_name;

                    if(isset($list[0]['src'])){
                        $serial_mode = isset($this->cron_serial_mode_list[$list[0]['src']])?$this->cron_serial_mode_list[$list[0]['src']]:0;
                    }else{
                        $serial_mode=0;
                    }
//                    print_r(array($serial_mode,$list[0]['src']));
                    $this->gen_serial_cron_files($path, $list,$serial_mode);
                    $output .= "$time $path\n";
                }
            }
            foreach ($d as $l) {
                if ($id==69 || $id==76 || $id==35 || ($id!=26 && strpos($l, 'check')!==false) /*|| ($id==26  && strpos($l,'amazon_update_products_cron.php')!==false && strpos($l,'.es')!==false )*/) {
                    $l=$this->deny_txt.$l;
                }

//
//                if($id==83 && strpos($l,'ebay_export_product_cron.php')!==false)$l='#'.$l;
//                if( strpos($l,'ebay')!==false)$l='#'.$l;
//                if(!in_array($id,array(9,83,82)))$l='#'.$l;
//                if(in_array($id,array(83,82)) && !( strpos($l,'ebay_export_offer_cron.php')!==false || strpos($l,'ebay_sync_product_cron.php')!==false || strpos($l,'ebay_delete_product_cron.php')!==false || strpos($l,'ebay_export_product_cron.php')!==false  ) ) $l='#'.$l;
                if (isset($l[0]) && $l[0] =='#') {
                    //$output .= "$l \n";
                } elseif (isset($this->user_time_zone[$id]) && $this->user_time_zone[$id] != 0) {
                    $tmp = explode(' php ', $l);
                    $cron_time = trim($tmp[0]);
                    $new_cron_time = $this->get_new_cron_time($cron_time, $this->user_time_zone[$id]);
                    $l =  str_replace($cron_time, $new_cron_time, $l);
                } else {
                    //$output .= "$l \n";
                }
                $output .= "$l \n";
            }
        }
        $dir = dirname($this->file_crontab);
        if (!file_exists($dir)) {
            $old = umask(0);
            mkdir($dir, 0775, true);
            umask($old);
        }
//        echo $this->file_crontab;
        if (file_exists($this->file_crontab)) {
            unlink($this->file_crontab);
        }
//        file_put_contents($this->file_crontab, $output);
//       echo $output;
        $file = fopen($this->file_crontab, "w");
        fwrite($file, $output);
        fclose($file);
        if (!$this->update_file) {
            exec('php '.DIR_SERVER.'/assets/apps/cronman/import_cronjob.php');
        }
    }
    public function gen_serial_cron_files($path, $list,$serial_mode=0)
    {
        if (empty($path) || empty($list)) {
            return;
        }
        $path = trim(str_replace(' php ', '', $path));
        $the_list = '';
        foreach ($list as $l) {
            $c = trim($l['cmd']);
            $deny = '';
            if (!empty($l['deny'])) {
                $deny = '//'.$this->deny_txt;
            }
            $the_list .= '                '.trim($deny.'$cmd_list[] = '."'{$c}'". ";")."\n";
        }
        $file_name = str_replace(DIR_SERVER."/cron_serial/", '', $path);
        if($serial_mode==0){
        $str = '<?php

        ob_start();
         passthru("ps aux | grep php | grep '.$file_name.'  | grep -v \'/bin/sh\'  | grep -v \'sh -c\'|grep -v \'grep\' ");
        if(sizeof(explode("\n",trim(ob_get_clean())))>1){
            exit("dup");
        }

                $cmd_list = array();
'.$the_list.'

                foreach($cmd_list as $cmd){
                    exec("{$cmd}");
                }
                ?>';
        }else if(!empty($serial_mode))  {
        $str = '<?php



                $cmd_list = array();
'.$the_list.'

                foreach($cmd_list as $cmd){
                    exec(\'bash -c "exec nohup setsid \'.$cmd.\' > /dev/null 2>&1 &  "\');
                    sleep('.$serial_mode.');
                }
                ?>';
        }
        $path = trim(str_replace('-n','',$path));
        if (!file_exists($path)) {
            file_put_contents($path, $str);
            if (php_sapi_name() == "cli") {
                exec('sudo chmod 775 '.$path.' && sudo chown ubuntu:deploy '.$path);
            } else {
                exec('chmod 777 '.$path);
            }
        } else {
            file_put_contents($path, $str);
            if (php_sapi_name() == "cli") {
                exec('sudo chmod 775 '.$path.' && sudo chown ubuntu:deploy '.$path);
            }
        }
    }
    public function getFbaMaster($market = 'amazon')
    {
        $reg = array();
        if ($market=='amazon') {
            $sql = "select id_customer,id_shop,region , ext from amazon_fba where fba_master_platform = 1 group by id_customer,id_shop,region ";
            $q = mysqli_query($this->db, $sql);
            while ($r = mysqli_fetch_assoc($q)) {
                $reg[$r['id_customer']][$r['id_shop']][$r['region']] =$r['ext'];
            }
        }
        return $reg;
    }
    public function getFbaUserCron($market = 'amazon')
    {
        $reg = array();
        if ($market=='amazon') {
            $txt = "{$market}_runned_users";
            if (!isset($this->$txt)) {
                $this->getUserUpdateMarketCron($market);
            }
            $avail_run = explode(',', $this->$txt);
            $avail_list = array();
            foreach ($avail_run as $v) {
                $id = trim($v, "'");
                if (empty($this->shop_id[$id])) {
                    continue;
                }
                $shop_id = $this->shop_id[$id];
                $avail_list[] = "('{$id}','{$shop_id}')";
            }
            if (empty($avail_list)) {
                return $reg;
            }

            $sql = "select id_customer ,id_shop,id_country,region , ext from amazon_fba where (id_customer,id_shop) in (".implode(',', $avail_list).") and fba_master_platform = 1 group by id_customer,region ";
            $q = mysqli_query($this->db, $sql);
            while ($r = mysqli_fetch_assoc($q)) {
                $reg[$r['id_customer']][$r['region']] =$r;
            }
        }
        return $reg;
    }

    public function getFbaRepricingUserCron($market = 'amazon')
    {
        $reg = array();
        if ($market=='amazon') {
            $txt = "{$market}_runned_users";
            if (!isset($this->$txt)) {
                $this->getUserUpdateMarketCron($market);
            }
            $avail_run = explode(',', $this->$txt);
            $avail_list = array();
            foreach ($avail_run as $v) {
                $id = trim($v, "'");
                if (empty($this->shop_id[$id])) {
                    continue;
                }
                $shop_id = $this->shop_id[$id];
//                $avail_list[] = "('{$id}','{$shop_id}')";
                $u_name = $this->user_name[$id];

                $sql = "select field_name,id_country from ".USER_DB_PREFIX.$u_name.".amazon_log_display where id_shop= '$shop_id'  and menu = 'features' and  field_toggle = 1 and field_disabled = 0 and field_name in ('repricing','fba')";

                $q = mysqli_query($this->dbu, $sql);
                if ($q) {
                    while ($r = mysqli_fetch_assoc($q)) {
                        $reg[$id][$r['id_country']][$r['field_name']] =true;
                    }
                } else {
                    //                    echo $sql."\n";
                }
            }

            return $reg;
        }
        return $reg;
    }

    public function getMessagingActive()
    {
        $reg = array();

            foreach ($this->cron_user_list as $v) {
                $id = $v['id'];
                if (empty($this->shop_id[$id])) {
                    continue;
                }
                $shop_id = $this->shop_id[$id];
//                $avail_list[] = "('{$id}','{$shop_id}')";
                $u_name = $this->user_name[$id];

                $sql = "SELECT
id_shop
FROM
".USER_DB_PREFIX.$u_name.".mp_messaging
where field_name in ('mail_invoice_active','mail_review_active','customer_thread_active')
group by id_shop
";

                $q = mysqli_query($this->dbu, $sql);
                if ($q) {
                    while ($r = mysqli_fetch_assoc($q)) {
                        if($shop_id==$r['id_shop']){
                            $reg[$id]=true;
                        }
                    }
                } else {
                    //                    echo $sql."\n";
                }
            }
            return $reg;
    }

    public function getUserUpdateMarketCron($market = 'amazon')
    {
        if ($market=='amazon') {
            if (!isset($this->amazon_runned_users)) {
                $sql = "select user_id as id from histories where history_action like 'amazon%_synchronize' or history_action like 'amazon%_create' group by user_id ";
                $q = mysqli_query($this->db, $sql);
                $id = array();
                while ($r = mysqli_fetch_assoc($q)) {
                    $id[] =$r['id'];
                }
                $add = "'".implode("','", $id)."'";
                $this->amazon_runned_users = $add;
            }
            $sql = "SELECT
                            ac.id_customer,
                            ac.user_name,
                            ac.id_shop,ac.id_region,r.name_region as region,
                            ac.ext,ac.id_country,ac.cron_send_orders,
                            u.user_created_on

                    FROM
                            amazon_configuration ac
                    INNER JOIN users u ON u.id = ac.id_customer and u.id in ($this->amazon_runned_users)
                    INNER JOIN offer_price_packages op ON op.ext_offer_sub_pkg = ac.ext
                    INNER JOIN user_package_details up ON up.id_users = u.id
                    AND up.id_package = op.id_offer_price_pkg
                    LEFT JOIN  region r on ac.id_region = r.id_region
                    WHERE
                            ac.active = '1'  and op.id_offer_pkg = '2' /*and ac.allow_automatic_offer_creation = '1'*/
                    GROUP BY
                            ac.id_customer,
                            id_country,
                            id_shop ";
        } elseif ($market=='mirakl') {
            if (!isset($this->mirakl_runned_users)) {
                $sql = "select user_id as id from histories where history_action like '%_mirakl_%_.%'  group by user_id  ";
                $q = mysqli_query($this->db, $sql);
                $id = array();
                while ($r = mysqli_fetch_assoc($q)) {
                    $id[] =$r['id'];
                }
                $add = "'".implode("','", $id)."'";
                $this->mirakl_runned_users = $add;
            }
            $sql = "SELECT
                            ac.id_customer,
                            ac.user_name,
                            ac.id_shop,
                            ac.ext,ac.id_country,
                            u.user_created_on,op.sub_marketplace,ac.id_country

                    FROM
                            mirakl_configuration ac
                    INNER JOIN users u ON u.id = ac.id_customer and u.id in ($this->mirakl_runned_users)
                    INNER JOIN offer_price_packages op ON op.ext_offer_sub_pkg = ac.ext
                    INNER JOIN user_package_details up ON up.id_users = u.id
                    AND up.id_package = op.id_offer_price_pkg
                    WHERE
                            ac.active = '1'  and op.id_offer_pkg = '6'
                    GROUP BY
                            ac.id_customer,
                            id_country,
                            id_shop ";
        } elseif ($market=='ebay') {
            $sql = "SELECT
                    c.id_customer,
                    u.user_name,
                    offer_price_packages.ext_offer_sub_pkg as ext,
                    offer_price_packages.domain_offer_sub_pkg as domain,
                    offer_price_packages.id_site_ebay as id_site
                    from configuration c ,users u
                    INNER JOIN user_package_details ON u.id = user_package_details.id_users
                    INNER JOIN offer_price_packages ON user_package_details.id_package = offer_price_packages.id_offer_price_pkg and offer_price_packages.id_offer_pkg = '3'
                    where c.name = 'EBAY_TOKEN' and u.id = c.id_customer  and offer_price_packages.id_offer_pkg = '3'
                    ";
        }
        $q = mysqli_query($this->db, $sql);
        $out = array();
        if (!$q) {
            echo '<br>'.$sql.'<br><br>';
        }
        while ($r = mysqli_fetch_assoc($q)) {
            $out[] =$r;
        }
        return $out;
    }

//    public function getUserUpdateOrderMarketCron($market = 'amazon',$t_action='get'){
//        if($market=='amazon'){
//            if(!isset($this->amazon_runned_users)){
//                $sql = "select user_id as id from histories where history_action like 'amazon%_synchronize' group by user_id ";
//                $q = mysqli_query($this->db,$sql);
//                    $id = array();
//                    while($r = mysqli_fetch_assoc($q)){
//                        $id[] =$r['id'];
//                    }
//                $add = "'".implode("','",$id)."'";
//                $this->amazon_runned_users = $add;
//            }
//            $add = "'".implode("','",$id)."'";
//            $sql = "SELECT
//                            ac.id_customer,
//                            ac.user_name,
//                            ac.id_shop,
//                            ac.ext,ac.id_country,ac.cron_send_orders,
//                            u.user_created_on
//
//                    FROM
//                            amazon_configuration ac
//                    INNER JOIN users u ON u.id = ac.id_customer and u.id in ($this->amazon_runned_users)
//                    INNER JOIN offer_price_packages op ON op.ext_offer_sub_pkg = ac.ext
//                    INNER JOIN user_package_details up ON up.id_users = u.id
//                    AND up.id_package = op.id_offer_price_pkg
//                    WHERE
//                            ac.active = '1' /*and ac.allow_automatic_offer_creation = '1'*/
//                    GROUP BY
//                            ac.id_customer,
//                            id_country,
//                            id_shop ";
//
//
//        }else if($market=='ebay'){
//            $sql = "SELECT
//                    c.id_customer,
//                    u.user_name,
//                    offer_price_packages.ext_offer_sub_pkg as ext,
//                    offer_price_packages.domain_offer_sub_pkg as domain,
//                    offer_price_packages.id_site_ebay as id_site
//                    from configuration c ,users u
//                    INNER JOIN user_package_details ON u.id = user_package_details.id_users
//                    INNER JOIN offer_price_packages ON user_package_details.id_package = offer_price_packages.id_offer_price_pkg and offer_price_packages.id_offer_pkg = '3'
//                    where c.name = 'EBAY_TOKEN' and u.id = c.id_customer
//                    ";
//        }
//        $q = mysqli_query($this->db,$sql);
//        $out = array();
//        while($r = mysqli_fetch_assoc($q)){
//            $out[] =$r;
//        }
//        return $out;
//    }

    public function get_base_pk_zone_not_expire($id_user)
    {
        $return = array();
        $add = '';
        if (strtolower($id_user) != 'all') {
            $add=" b.id_users = '$id_user' and ";
        }
        $sql = "select bd.id_bill_detail,bd.id_package,id_users from billings_detail bd , billings b  , offer_packages opk  ,offer_price_packages pk "
                . "left join offer_sub_packages s on s.id_offer_sub_pkg = pk.id_offer_sub_pkg "
                 . "where $add b.id_billing = bd.id_billing and status_billing = 'active' "
                 . "and end_date_bill_detail >= '".date('Y-m-d')."'  "
                 . "and pk.id_offer_price_pkg = bd.id_package and opk.id_offer_pkg = pk.id_offer_pkg and pk.offer_pkg_type = 9  "
                 . "order by opk.id_offer_pkg asc,pk.id_offer_price_pkg asc";

        $q = mysqli_query($this->db, $sql);
        $return = array();
        while ($v = mysqli_fetch_assoc($q)) {
            $bdt_id = $v['id_bill_detail'];
            $pk_id = $v['id_package'];
            $u_id = $v['id_users'];
            $sql = "select bd.id_bill_detail from billings_detail bd , billings b "
                    . " where  b.id_billing = bd.id_billing and status_billing = 'active' "
                    . "and bd.id_package = '$pk_id' and b.id_users = '$u_id' order by bd.id_bill_detail desc limit 1";

            $q2 = mysqli_query($this->db, $sql);
            $return = array();
            $ch = mysqli_fetch_assoc($q2);

            if ($bdt_id == $ch['id_bill_detail']) {
                $return = $v;
                break;
            }
        }

        return $return;
    }
    public function get_pk_zone_purchased($id_user, $site_name, $ext, $sub_market_id = 0)
    {
        $return = array();
//        $base = $this->get_base_pk_zone_not_expire($id_user);
//        if(!empty($base))$return['base'] = $base;
        $add = '';

        if (strtolower($id_user) != 'all') {
            $add=" b.id_users = '$id_user' and ";
        }
        if (!empty($sub_market_id) && $sub_market_id!=0) {
            $add .= " tpk.sub_marketplace = '$sub_market_id' and ";
        }


        $sql = "SELECT bd.id_bill_detail,bd.id_package,id_users FROM billings_detail bd, billings b, offer_packages opk,
	offer_price_packages pk
        LEFT JOIN offer_sub_packages s ON s.id_offer_sub_pkg = pk.id_offer_sub_pkg
        join offer_price_packages tpk  on tpk.ext_offer_sub_pkg LIKE '$ext' and pk.offer_pkg_type = '4'
        WHERE 	$add
          b.id_billing = bd.id_billing
        AND status_billing = 'active'
        AND (tpk.id_offer_price_pkg = bd.id_package || pk.id_offer_price_pkg = bd.id_package)
        AND opk.id_offer_pkg = tpk.id_offer_pkg
        AND opk.id_offer_pkg = pk.id_offer_pkg
        AND opk.name_offer_pkg LIKE '$site_name'
        AND tpk.id_region = pk.id_region   group by opk.id_offer_pkg ASC, pk.id_offer_price_pkg ASC ORDER BY opk.id_offer_pkg ASC, pk.id_offer_price_pkg ASC";

        $q = mysqli_query($this->db, $sql);

        $r = mysqli_fetch_assoc($q);
        if (!empty($r)) {
            $return['pk'] = $r;
        }
        return $return;
    }
    public function get_pk_not_expire($id_user=0, $site_name=null, $ext=null)
    {
        $add=$add2=$add3=$add4= '';

        if (strtolower($id_user) != 'all') {
            $add=" b.id_users = '$id_user' and ";
        }

        if ($site_name !== null) {
            $add2 = " and opk.name_offer_pkg like '$site_name' ";
        }
        if ($ext !== null) {
            $add3 = " and pk.ext_offer_sub_pkg like '$ext' ";
        }

        $sql = "select bd.id_bill_detail,bd.id_package,id_users from billings_detail bd , billings b $add2, offer_packages opk  ,offer_price_packages pk "
                . "left join offer_sub_packages s on s.id_offer_sub_pkg = pk.id_offer_sub_pkg "
                 . "where $add b.id_billing = bd.id_billing and status_billing = 'active' "
                 . "and end_date_bill_detail >= '".date('Y-m-d')."'  "
                 . "and pk.id_offer_price_pkg = bd.id_package and opk.id_offer_pkg = pk.id_offer_pkg and pk.offer_pkg_type <>2 $add3 $add4"
                 . "order by opk.id_offer_pkg asc,pk.id_offer_price_pkg asc";

        $q = mysqli_query($this->db, $sql);
        $return = array();
        while ($v = mysqli_fetch_assoc($q)) {
            $bdt_id = $v['id_bill_detail'];
            $pk_id = $v['id_package'];
            $u_id = $v['id_users'];
            $sql = "select bd.id_bill_detail from billings_detail bd , billings b "
                    . " where  b.id_billing = bd.id_billing and status_billing = 'active' "
                    . "and bd.id_package = '$pk_id' and b.id_users = '$u_id' order by bd.id_bill_detail desc limit 1";

            $q2 = mysqli_query($this->db, $sql);
            $return = array();
            $ch = mysqli_fetch_assoc($q2);

            if ($bdt_id == $ch['id_bill_detail']) {
                $return = $v;
                break;
            }
        }

        return $return;
    }

    public function cronj_gen_market_update($market = 'amazon', $type='offer', $t_action='update', $cron_serial=false, $param_type=0,$param_value='',$check_master=false)
    {
        $action = $this->app_path;
        $url = 'crond_'.$t_action.'_'.$type.'_' . $market . '_url';
        $padd=$this->cmd_padd;
        $run = true;
        $avail_users = array();
        $cron_id = $this->cron_id_list[$this->$url];
        $cron_info = $this->cron_info_list[$cron_id];
        if ($market=='amazon') {
            $market_id = 2;
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;
            foreach ($user_list as $u) {
                $deny='';
                $ext = $u['ext'];
                $region_name = $u['region'];
                $id_country = $u['id_country'];
                if ($type=='fba' || $type=='repricing') {
                    if (empty($this->amazon_log_display_user[$u['id_customer']][$id_country][$type])) {
                        continue;
                    }
                }
                if ($type=='fba') {
                    if (empty($this->amazon_fba_user[$u['id_customer']][$region_name])) {
                        continue;
                    }
                    $fba = $this->amazon_fba_user[$u['id_customer']][$region_name];
                    if($check_master && $fba['ext'] != $ext){
                        continue;
                    }
                }

//                if(!isset($this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$ext]) || $this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$ext]==true){
//                    $deny=$this->deny_txt;
//                }
                if (isset($this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$ext]) && $this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$ext] == true) {
                    $deny=$this->deny_txt;
                }
                $user_id = $u['id_customer'];
                if (!isset($this->user_name[$user_id])) {
                    continue;
                }
//                if(!in_array($user_id,$this->cron_user_id_list))continue;
                $user_name = $u['user_name'];
                $shop_id = $u['id_shop'];
                $db_id_shop = $this->shop_id[$user_id];
                if ($shop_id != $db_id_shop) {
                    continue;
                }

                if (isset($cron_info['master_fba_run_only']) && $cron_info['master_fba_run_only']==1) {
                    if (isset($this->amazon_fba_master[$user_id][$shop_id][$region_name]) && $this->amazon_fba_master[$user_id][$shop_id][$region_name] != $ext) {
                        continue;
                    }
                }

                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext);
                    if (empty($expire)) {
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if ($create_user_date - $expired <= 0) {
                            continue;
                        }
                    }
                }

                $shop_name = $this->shop_name[$user_id];

                $freq = "frequency_".$t_action."_".$type."_" . $market;
                $k = $t_action =='update'?'synchronize':'delete';
                $key = "amazon{$ext}_{$k}_".$type;
                $run= ($t_action =='delete')?false:$run;

                if ($this->mode_fix) {
                    $ft_txt = "fix_time_".$t_action."_".$type."_" . $market;
                    $txt_time = $this->$ft_txt;
                } else {
                    $next_time = $this->get_next_action($user_id, $key, $this->$freq);
//                 $h = date('G', $next_time);
//                $m = 1 * date('i', $next_time);
//                echo $user_id.' '.$key.' '.$next_time.' '.$this->$freq.' '.$h.'-'.$m.'<br>';
                $txt_time = $this->loadBalanceCronTask($next_time, $run);
                }
                $txt_time = isset($this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]])?"{$this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]]}":$txt_time;
                if (!$run) {
                    $txt_time = '#'.$txt_time;
                }
                $uri =  "".$this->$url." $user_id $user_name $ext $shop_id $shop_name $market_id";
                if(!empty($param_value)){
                    $uri .= ' '.$param_value;
                }
                if (!$cron_serial) {
                    $this->cronj_list [$user_id] [] = $deny.$txt_time . $action . $uri.$padd;
                } else {
                    $this->cronj_serial_list[$user_id][str_replace(array(' ','-',"'",'"'), '_',  strtolower($region_name)).'_'.$this->$url][]
                        = array('time'=>$txt_time,'src'=>$this->$url,'cmd'=>$action . $uri.$padd,'deny'=>$deny);
                }
            }
        } elseif ($market=='mirakl') {
            $market_id = 6;
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;
            foreach ($user_list as $u) {
                $deny='';
                $ext = $u['ext'];
//                $sub_market_name = $u['marketplace'];
                $id_sub_market = $u['sub_marketplace'];
                $id_country = $u['id_country'];
//                $ext_t = strtolower($sub_market_name).$ext;
                if (isset($this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$id_sub_market.$ext])) {
                    $deny=$this->deny_txt;
                }
                $user_id = $u['id_customer'];
                if (!isset($this->user_name[$user_id])) {
                    continue;
                }
//                if(!in_array($user_id,$this->cron_user_id_list))continue;
                $user_name = $u['user_name'];
                $shop_id = $u['id_shop'];
                $db_id_shop = $this->shop_id[$user_id];
                if ($shop_id != $db_id_shop) {
                    continue;
                }

                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext, $id_sub_market);
                    if (empty($expire)) {
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if ($create_user_date - $expired <= 0) {
                            continue;
                        }
                    }
                }

                $shop_name = $this->shop_name[$user_id];

                $freq = "frequency_".$t_action."_".$type."_" . $market;
                $k = $t_action =='update'?'synchronize':'delete';
                $key = "export_{$type}_mirakl_{$id_sub_market}_{$ext}";
//                $run= ($t_action =='delete')?false:$run;

                if ($this->mode_fix) {
                    $ft_txt = "fix_time_".$t_action."_".$type."_" . $market;
                    $txt_time = $this->$ft_txt;
                } else {
                    $next_time = $this->get_next_action($user_id, $key, $this->$freq);
//                 $h = date('G', $next_time);
//                $m = 1 * date('i', $next_time);
//                echo $user_id.' '.$key.' '.$next_time.' '.$this->$freq.' '.$h.'-'.$m.'<br>';
                $txt_time = $this->loadBalanceCronTask($next_time, $run);
                }
                $txt_time = isset($this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]])?"{$this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]]}":$txt_time;
                if (!$run) {
                    $txt_time = '#'.$txt_time;
                }
//                $uri =  "".$this->$url." $user_id $user_name $ext $shop_id $shop_name $sub_market_name $id_sub_market $id_country";
                $uri =  "".$this->$url." $user_id $shop_id $id_sub_market $id_country  ";
                if (!$cron_serial) {
                    $this->cronj_list [$user_id] [] = $deny.$txt_time . $action . $uri.$padd;
                } else {
                    $this->cronj_serial_list[$user_id][$this->$url][]
                        = array('time'=>$txt_time,'src'=>$this->$url,'cmd'=>$action . $uri.$padd,'deny'=>$deny);
                }
            }
        } elseif ($market=='ebay') {
            $market_id = 3;
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;

            foreach ($user_list as $u) {
                $deny='';
                $ext = $u['ext'];
                if (isset($this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$ext])) {
                    $deny=$this->deny_txt;
                }
                $user_id = $u['id_customer'];
                if (!isset($this->user_name[$user_id])) {
                    continue;
                }
//                if(!in_array($user_id,$this->cron_user_id_list)){   continue;}
                $user_name = $u['user_name'];
//                $this->feedbiz = new ObjectBiz(array($user_name));
//                $db_id_shop = $this->feedbiz->getDefaultShop($user_name);
                $db_id_shop = $this->shop_id[$user_id];
                $id_shop = $db_id_shop;


//                if(!isset($this->ebay_avail_site[$user_id]))
//                    $this->ebay_avail_site[$user_id] = $this->feedbiz[$user_name]->getEbayAvailSite($user_name);
                $id_site = $u['id_site'];

                if (!($user_id=='38'&&$id_site=='0')) {
                    if (!in_array($id_site, $this->ebay_avail_site[$user_id])) {
                        continue;
                    }
                }

//                $run=false;
//                if(in_array($user_id,array(38,53,26,82,83))){//||(in_array($user_id,array(53))&&$type=='proudct')){
                    $run = true;
//                }

                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext);
                    if (empty($expire)) {
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if ($create_user_date - $expired <= 0) {
                            continue;
                        }
                    }
                }
                $shop_name = $this->shop_name[$user_id];
                $domain = $u['domain'];
                $freq = "frequency_".$t_action."_".$type."_" . $market;
                $key = "export_{$domain}";

                if ($this->mode_fix) {
                    $ft_txt = "fix_time_".$t_action."_".$type."_" . $market;

                    $txt_time = $this->$ft_txt;
                } else {
                    $next_time = $this->get_next_action($user_id, $key, $this->$freq);
                    $txt_time = $this->loadBalanceCronTask($next_time, $run);
                }
                $txt_time = isset($this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]])?"{$this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]]}":$txt_time;
                if (!$run) {
                    $txt_time = '#'.$txt_time;
                }
                $uri =  "".$this->$url." $user_id $user_name $id_site $domain $shop_name $market_id";
                if (!$cron_serial) {
                    $this->cronj_list [$user_id] [] = $deny.$txt_time . $action . $uri.$padd;
                } else {
                    $this->cronj_serial_list[$user_id][$this->$url][]
                        = array('time'=>$txt_time,'src'=>$this->$url,'cmd'=>$action . $uri.$padd,'deny'=>$deny);
                }
            }
        }
    }


    public function cronj_gen_market_update_order($market = 'amazon', $t_action='get', $cron_serial=false, $param_type=0,$param_value='',$check_master=false)
    {
        $action = $this->app_path;

        $url = 'crond_'.$t_action.'_order_' . $market . '_url';
        $padd=$this->cmd_padd;

        $avail_users = array();
        $run = true;


        if ($market=='amazon') {
            $market_id = 2;
//            $user_list = $this->getUserUpdateOrderMarketCron($market,$t_action);
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;
            foreach ($user_list as $u) {
                $deny='';
                $ext = $u['ext'];

                $region_name = $u['region'];
                $id_country = $u['id_country'];
                if (strpos($t_action, 'fba') !==false) {
                    if (empty($this->amazon_log_display_user[$u['id_customer']][$id_country]['fba'])) {
                        continue;
                    }
                }
                if (strpos($t_action, 'fba') !==false) {
                    if (empty($this->amazon_fba_user[$u['id_customer']][$region_name])) {
                        continue;
                    }
                }

                if (isset($this->market_update_order_deny_user[$market][$t_action][$u['id_customer']][$ext]) && $this->market_update_order_deny_user[$market][$t_action][$u['id_customer']][$ext]==true) {
                    $deny=$this->deny_txt;
                }
//                if(!isset($this->market_update_order_deny_user[$market][$t_action][$u['id_customer']][$ext]) || ($this->market_update_order_deny_user[$market][$t_action][$u['id_customer']][$ext]) == true ) $deny=$this->deny_txt;

                $user_id = $u['id_customer'];
                //$cron_status = isset($u['cron_send_orders'])?(int)$u['cron_send_orders']:1;
                $cron_status =  1;
                if (!isset($this->user_name[$user_id])) {
                    continue;
                }
//                if(!in_array($user_id,$this->cron_user_id_list))continue;
                $user_name = $u['user_name'];
                $shop_id = $u['id_shop'];
                $db_id_shop = $this->shop_id[$user_id];
                if ($shop_id != $db_id_shop) {
                    continue;
                }

                $id_site = $u['id_country'];
                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext);
                    if (empty($expire)) {
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if ($create_user_date - $expired <= 0) {
                            continue;
                        }
                    }
                }

                $shop_name = $this->shop_name[$user_id];

                $freq = "frequency_".$t_action."_order_" . $market;
                $key = "amazon{$ext}_{$t_action}_order";

                if ($this->mode_fix) {
                    $ft_txt = "fix_time_".$t_action."_order_" . $market;
                    $txt_time = $this->$ft_txt;
                } else {
                    $next_time = $this->get_next_action($user_id, $key, $this->$freq);
//                 $h = date('G', $next_time);
//                $m = 1 * date('i', $next_time);
//                echo $user_id.' '.$key.' '.$next_time.' '.$this->$freq.' '.$h.'-'.$m.'<br>';
                $txt_time = $this->loadBalanceCronTask($next_time, $run);
                }
                $txt_time = isset($this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]])?"{$this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]]}":$txt_time;
                if (!$run) {
                    $txt_time = '#'.$txt_time;
                }
                if (in_array($t_action, array('send', 'stock'))) {
                    $uri =  "".$this->$url." $user_id $user_name $market_id $id_site $shop_id $shop_name $cron_status $ext";
                } else {
                    $uri =  "".$this->$url." $user_id $user_name $ext $shop_id $shop_name $market_id $cron_status";
                }
//                if ($param_type==1) {
//                    $uri.= ' recheck';
//                } elseif ($param_type==2) {
//                    $uri.= ' doublecheck';
//                }
                if(!empty($param_value)){
                    $uri .= ' '.$param_value;
                }

                if (!$cron_serial) {
                    $this->cronj_list [$user_id] [] = $deny.$txt_time . $action . $uri.$padd;
                } else {
                    $this->cronj_serial_list[$user_id][$this->$url][]
                        = array('time'=>$txt_time,'src'=>$this->$url,'cmd'=>$action . $uri.$padd,'deny'=>$deny);
                }
            }
        } elseif ($market=='mirakl') {
            $market_id = 6;
//            $user_list = $this->getUserUpdateOrderMarketCron($market,$t_action);
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;
            foreach ($user_list as $u) {
                $deny='';
                $ext = $u['ext'];
//                $sub_market_name = $u['marketplace'];
                $id_sub_market = $u['sub_marketplace'];
                $id_country = $u['id_country'];
                if (isset($this->market_update_order_deny_user[$market][$t_action][$u['id_customer']][$id_sub_market.$ext])) {
                    $deny=$this->deny_txt;
                }
                $user_id = $u['id_customer'];
                //$cron_status = isset($u['cron_send_orders'])?(int)$u['cron_send_orders']:1;
                $cron_status =  1;
                if (!isset($this->user_name[$user_id])) {
                    continue;
                }
//                if(!in_array($user_id,$this->cron_user_id_list))continue;
                $user_name = $u['user_name'];
                $shop_id = $u['id_shop'];
                $db_id_shop = $this->shop_id[$user_id];
                if ($shop_id != $db_id_shop) {
                    continue;
                }

                $id_site = $u['id_country'];
                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext, $id_sub_market);
                    if (empty($expire)) {
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if ($create_user_date - $expired <= 0) {
                            continue;
                        }
                    }
                }

                $shop_name = $this->shop_name[$user_id];

                $freq = "frequency_".$t_action."_order_" . $market;
//                $key = "export_mirakl{$ext}_{$t_action}_order";
                $type='order';
                $key = "export_{$type}_mirakl_{$id_sub_market}_{$ext}";


                if ($this->mode_fix) {
                    $ft_txt = "fix_time_".$t_action."_order_" . $market;
                    $txt_time = $this->$ft_txt;
                } else {
                    $next_time = $this->get_next_action($user_id, $key, $this->$freq);
//                 $h = date('G', $next_time);
//                $m = 1 * date('i', $next_time);
//                echo $user_id.' '.$key.' '.$next_time.' '.$this->$freq.' '.$h.'-'.$m.'<br>';
                $txt_time = $this->loadBalanceCronTask($next_time, $run);
                }
                $txt_time = isset($this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]])?"{$this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]]}":$txt_time;
                if (!$run) {
                    $txt_time = '#'.$txt_time;
                }
//                if(in_array($t_action,array('send','stock'))){
//                    $uri =  "".$this->$url." $user_id $user_name $ext $shop_id $shop_name $sub_market_name $id_sub_market $id_country";
//                }else{
//                    $uri =  "".$this->$url."  $user_id $user_name $ext $shop_id $shop_name $sub_market_name $id_sub_market $id_country";
//
//                }
                //                $uri =  "".$this->$url." $user_id $user_name $ext $shop_id $shop_name $sub_market_name $id_sub_market $id_country";
//               $uri =  "".$this->$url." $user_id $shop_id $id_sub_market $id_country  ";

//               if(in_array($t_action,array('send' ))){
//                    $uri =  "".$this->$url." $user_id $user_name $market_id $id_site $shop_id $shop_name $cron_status $ext $id_sub_market";
//                }else{
//                    $uri =  "".$this->$url." $user_id $user_name $ext $shop_id $shop_name $market_id $cron_status";
//                }
                if (in_array($t_action, array('send' ))) {
                    $uri =  "".$this->$url." $user_id $user_name $market_id $id_site $shop_id $shop_name $cron_status $ext $id_sub_market";
                } else {
                    $uri =  "".$this->$url." $user_id $shop_id $id_sub_market $id_country  ";
                }

                if (!$cron_serial) {
                    $this->cronj_list [$user_id] [] = $deny.$txt_time . $action . $uri.$padd;
                } else {
                    $this->cronj_serial_list[$user_id][$this->$url][]
                        = array('time'=>$txt_time,'src'=>$this->$url,'cmd'=>$action . $uri.$padd,'deny'=>$deny);
                }
            }
        } elseif ($market=='ebay') {
            if ($t_action=='send') {
                $run = false;
            }
            $market_id = 3;
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;


            foreach ($user_list as $u) {
                $deny='';
                $ext = $u['ext'];
                if (isset($this->market_update_order_deny_user[$market][$t_action][$u['id_customer']][$ext])) {
                    $deny=$this->deny_txt;
                }
                $user_id = $u['id_customer'];
                if (!isset($this->user_name[$user_id])) {
                    continue;
                }
//                if(!in_array($user_id,$this->cron_user_id_list)){   continue;}
                $user_name = $u['user_name'];

                $db_id_shop = $this->shop_id[$user_id];

//                if(!isset($this->ebay_avail_site[$user_id])){
//                    if(isset($this->feedbiz[$user_name])){
//                        $this->ebay_avail_site[$user_id] = $this->feedbiz[$user_name]->getEbayAvailSite($user_name);
//                    }
//                }

                $id_site = $u['id_site'];

                if (!in_array($id_site, $this->ebay_avail_site[$user_id])) {
                    continue;
                }



                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext);
                    if (empty($expire)) {
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if ($create_user_date - $expired <= 0) {
                            continue;
                        }
                    }
                }
                $shop_id = $db_id_shop ;
                $shop_name = $this->shop_name[$user_id];
                $domain = $u['domain'];
                $freq = "frequency_".$t_action."_order_" . $market;
                $key = "ebay_{$t_action}_order_{$domain}";
                $run=true;
                $cron_status = $this->get_ebay_cron_status($user_id, $id_site);
                if ($this->mode_fix) {
                    $ft_txt = "fix_time_".$t_action."_order_" . $market;
                    $txt_time = $this->$ft_txt;
                } else {
                    $next_time = $this->get_next_action($user_id, $key, $this->$freq);
                    $txt_time = $this->loadBalanceCronTask($next_time, $run);
                }
                $run= ($t_action =='stock')?false:$run;
                $txt_time = isset($this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]])?"{$this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]]}":$txt_time;
                if (!$run) {
                    $txt_time = '#'.$txt_time;
                }
                if (in_array($t_action, array('send', 'stock'))) {
                    $uri =  "".$this->$url." $user_id $user_name $market_id $id_site $shop_id $shop_name $cron_status";
                } else {
                    $uri =  "".$this->$url." $user_id $user_name $id_site $domain $shop_name $market_id $cron_status $shop_id";
                }
                if (!$cron_serial) {
                    $this->cronj_list [$user_id] [] = $deny.$txt_time . $action . $uri.$padd;
                } else {
                    $this->cronj_serial_list[$user_id][$this->$url][]
                        = array('time'=>$txt_time,'src'=>$this->$url,'cmd'=>$action . $uri.$padd,'deny'=>$deny);
                }
            }
        }
    }
    public function get_ebay_cron_status($id_customer, $id_site)
    {
        $sql = "select value from ebay_configuration where id_customer='$id_customer' and id_site = '$id_site' and name = 'EBAY_CRON_ORDER' ";
        $q = mysqli_query($this->db, $sql);
        if (mysqli_num_rows($q)>0) {
            $data = mysqli_fetch_assoc($q);
            return $data['value'];
        } else {
            return '1';
        }
    }
    public function get_user_conj_feed_active()
    {
        $sql = "select id,value,user_name,user_time_zone from users u inner join configuration c on u.id = c.id_customer where u.user_status = '1' and user_cronj_status = '1' and c.name = 'FEED_BIZ' order by u.id asc";

        $q = mysqli_query($this->db, $sql);
        $out = array();
        while ($r = mysqli_fetch_assoc($q)) {
            $val = unserialize(base64_decode($r['value']));
            if (!(isset($val['verified'])&& $val['verified']!='')) {
                continue;
            }
            $out[]=$r;
        }
        return $out;
    }
    public function cronj_gen_import_direct($action_cmd="import", $type = 'offer', $param_type=0,$cron_serial=false,$target_serial=false)
    {
        $action = $this->app_path;
        $padd=$this->cmd_padd;//"> /dev/null 2>&1";
        $url = 'crond_'.$action_cmd.'_' . $type . '_url';

        $arr = $this->get_cron_time_user_import($type);
        $run = true;
        foreach ($arr as $u) {
            $deny='';
            if (isset($this->import_direct_deny_user[$type][$u['id']])) {
                $deny=$this->deny_txt;
            }
            $id = $u ['id'];
            if($type=='messaging'){
                if(!isset($this->messaging_active[$id])){
                    continue;
                }
            }
            $user_name = $u['user_name'];
            if (!isset($this->shop_id[$id])) {
                continue;
            }
            $txt_time = $u ['cron_time'];
            $time = $u ['time'];
            $txt_time = isset($this->cronjob_custom_time[$id][$this->cron_id_list[$this->$url]])?"{$this->cronjob_custom_time[$id][$this->cron_id_list[$this->$url]]}":$txt_time;
            if (!$run) {
                $txt_time = '#'.$txt_time;
            }
//            $add= '';
//            if($type=='stock'){
                $add = $this->shop_id[$id];
//                if($add==0)return;
//            }
            switch ($param_type) {
                case 0:
                    break;
                case 1:
                    $add="{$user_name} ";
                    break;
                default:
                    break;
            }
            $uri =  "".$this->$url." $id ".$add;//.urlencode($this->my_feed_model->get_json_for_import($id))."'";

            if($cron_serial && $target_serial!==false){
                $target = $this->cron_info_list[$target_serial];
                $urlx = $target['cron_file'];
                $this->cronj_serial_list[$id][$urlx][]
                        = array('time'=>'','cmd'=>$action . $uri.$padd,'deny'=>$deny);
            }elseif($cron_serial!==false && $target_serial===false){
                $urlx = $this->$url;
                $this->cronj_serial_list[$id][$urlx][]
                        = array('time'=>$txt_time ,'cmd'=>$action . $uri.$padd,'deny'=>$deny);
            }else{
                $this->cronj_list [$id] [] = $deny.$txt_time . $action . $uri.$padd;
            }
        }
    }
    public function cronj_gen_market_direct($market,$t_action="import", $type = 'offer', $param_type=0,$cron_serial=false){
        $action = $this->app_path;
        $url = 'crond_'.$t_action.'_'.$type.'_' . $market . '_url';
        $padd=$this->cmd_padd;
        $run = true;
        $avail_users = array();
        $cron_id = $this->cron_id_list[$this->$url];
        $cron_info = $this->cron_info_list[$cron_id];
        if ($market=='amazon') {
            $market_id = 2;
            $ul = "{$market}_user_list";
            $user_list = $this->$ul;
            $runned = array();
            foreach ($user_list as $u) {
                if(isset($runned[$u['id_customer']]))continue;
                $deny='';
                $ext = $u['ext'];
                $region_name = $u['region'];
                $id_country = $u['id_country'];
                if ($type=='fba' || $type=='repricing') {
                    if (empty($this->amazon_log_display_user[$u['id_customer']][$id_country][$type])) {
                        continue;
                    }
                }
                if ($type=='fba') {
                    if (empty($this->amazon_fba_user[$u['id_customer']][$region_name])) {
                        continue;
                    }
                }

//                if(!isset($this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$ext]) || $this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$ext]==true){
//                    $deny=$this->deny_txt;
//                }
//                if (isset($this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$ext]) && $this->market_update_deny_user[$market][$type][$t_action][$u['id_customer']][$ext] == true) {
//                    $deny=$this->deny_txt;
//                }
                $user_id = $u['id_customer'];
                if (!isset($this->user_name[$user_id])) {
                    continue;
                }
//                if(!in_array($user_id,$this->cron_user_id_list))continue;
                $user_name = $u['user_name'];
                $shop_id = $u['id_shop'];
                $db_id_shop = $this->shop_id[$user_id];
                if ($shop_id != $db_id_shop) {
                    continue;
                }
//                print_r($u);
//                echo($url);
//                print_r($this);
                if (isset($cron_info['master_fba_run_only']) && $cron_info['master_fba_run_only']==1) {
                    if (isset($this->amazon_fba_master[$user_id][$shop_id][$region_name]) && $this->amazon_fba_master[$user_id][$shop_id][$region_name] != $ext) {
                        continue;
                    }
                }

                if (self::FREETAIL > 0) {
                    $expire = $this->get_pk_zone_purchased($user_id, $market, $ext);
                    if (empty($expire)) {
                        $create_user_date = strtotime($u['user_created_on']);
                        $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL . ' month ago')));
                        if ($create_user_date - $expired <= 0) {
                            continue;
                        }
                    }
                }

                $shop_name = $this->shop_name[$user_id];

                $freq = "frequency_".$t_action."_".$type."_" . $market;
                $k = $t_action =='update'?'synchronize':'delete';
                $key = "amazon{$ext}_{$k}_".$type;
                $run= ($t_action =='delete')?false:$run;

                if ($this->mode_fix) {
                    $ft_txt = "fix_time_".$t_action."_".$type."_" . $market;
                    $txt_time = $this->$ft_txt;
                } else {
                    $next_time = $this->get_next_action($user_id, $key, $this->$freq);
//                 $h = date('G', $next_time);
//                $m = 1 * date('i', $next_time);
//                echo $user_id.' '.$key.' '.$next_time.' '.$this->$freq.' '.$h.'-'.$m.'<br>';
                $txt_time = $this->loadBalanceCronTask($next_time, $run);
                }
                $txt_time = isset($this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]])?"{$this->cronjob_custom_time[$user_id][$this->cron_id_list[$this->$url]]}":$txt_time;
                if (!$run) {
                    $txt_time = '#'.$txt_time;
                }
                $uri =  "".$this->$url." $user_id $user_name   $shop_id $shop_name $market_id";


                if($cron_serial){
                    $urlx = $this->$url;
                    $this->cronj_serial_list[$user_id][$urlx][]
                            = array('time'=>'','cmd'=>$action . $uri.$padd,'deny'=>$deny);
                }else{
                    $this->cronj_list [$user_id] [] = $deny.$txt_time . $action . $uri.$padd;
                }


//                $this->cronj_list [$user_id] [] = $deny.$txt_time . $action . $uri.$padd;
//                echo $deny.$txt_time . $action . $uri.$padd;exit;
                $runned[$u['id_customer']] = true;
            }
        }
    }
    public function check_ebay_export_past($u_name){
        $sql = "SELECT id_site from ".USER_DB_PREFIX.$u_name.".ebay_product_details , ".USER_DB_PREFIX.$u_name.".products_shop as shop where ebay_product_details.id_shop = shop.id_shop and shop.is_default = '1' and  /*ebay_product_details.is_enabled ='1' and*/  ebay_product_details.export_pass = '1' group by ebay_product_details.id_site";
        $q = mysqli_query($this->dbu, $sql);
        $out=array();
        if(!is_bool($q)){
            while ($d = mysqli_fetch_assoc($q)) {
                $out[] = $d['id_site'];
            }
        }
        return $out;
    }
    public function check_history_key_all($key='import_')
    {
        $sql="select user_id as id from histories where history_action = '{$key}' group by user_id order by user_id asc  ";
//        $sql="select user_id as id from histories where    history_action like '{$key}%' group by user_id order by user_id asc  ";
        $q = mysqli_query($this->db, $sql);
        $out = array();
        while ($d = mysqli_fetch_assoc($q)) {
            $out[$d['id']] = true;
        }
        return $out;
    }
    public function check_history_key($id, $key='import_')
    {
        $sql="select id from histories where user_id = '$id' and history_action like '{$key}%' order by history_date_time desc limit 1";
        $q = mysqli_query($this->db, $sql);
        if (mysqli_num_rows($q) > 0) {
            return true;
        }
        return false;
    }
    public function get_current_shop_name($id)
    {
        $sql = "select value from configuration where name in ('FEED_SHOP_INFO') and id_customer = '$id' limit 1";
        $q = mysqli_query($this->db, $sql);
        if (mysqli_num_rows($q)> 0) {
            $d = mysqli_fetch_assoc($q);
            $v = unserialize(base64_decode($d['value']));
            if (isset($v['name'])) {
                return $v['name'];
            }
        }

        return false;
    }
    public function get_general($id_customer=null)
    {
        $mainconfiguration = array();
        $sql = "select name,value ,config_date from configuration where name in ('FEED_SHOP_INFO') and id_customer = '$id_customer' limit 1 ";
        $q = mysqli_query($this->db, $sql);
        while ($row = mysqli_fetch_assoc($q)) {
            $mainconfiguration[$row['name']]['value'] = $row['value'];
            $mainconfiguration[$row['name']]['config_date'] = $row['config_date'];
        }

        return $mainconfiguration;
    }

    public function get_cron_time_user_import($type = 'feed')
    {
        $users = $this->cron_user_list;
        $avail_users = array();


        foreach ($users as $u) {
            $id = $u ['id'];
            $run=true;
            // $verified = $this->my_feed_model->check_verified_import($id);

            if (!isset($this->imported_feed[$id])) {
                continue;
            }
            $freq = "frequency_feed_" . $type;
            $key = 'import_';
            if ($type == 'order') {
                $config_data = $this->get_general($id);
                if (isset($config_data ['FEED_SHOP_INFO'])) {
                    $feed = unserialize(base64_decode($config_data ['FEED_SHOP_INFO'] ['value']));
                    if (!isset($feed ['url'] ['shippedorders'])) {
                        continue;
                    }
                } else {
                    continue;
                }
                $key .= $type;
                $run=true;
            } elseif ($type=='feed') {
                $key .= $type;
                $run=true;
            } else {
                $run=true;
            }
            if ($this->mode_fix) {
                $ft_txt = 'fix_time_feed_'.$type;
//                if($type=='feed'){
//                    $txt = '0 0 * * * ';
//                }else if($type=='offer'){
//                    $txt = '5,35 * * * * ';
//                }else if($type=='order'){
//                    $txt = '7 * * * * ';
//                }
                $txt = $this->$ft_txt;
                $next_time = '';
            } else {
                $next_time = $this->get_next_action($id, $key, $this->$freq);
                $txt =  $this->loadBalanceCronTask($next_time, $run);
            }
            if (!$run) {
                $txt = '#'.$txt;
            }
            $avail_users [] = array(
                'id' => $id,
                'user_name'=>$u['user_name'],
                'cron_time' => $txt,
                'time' => $next_time
            );
        }
        return $avail_users;
    }

    public function get_next_action($id, $key = 'import_', $min = 15)
    {
        if ($key == 'import_') {
            $sql = "select history_date_time from histories where user_id = '$id' and history_action like '{$key}%' and history_action not like 'import_order' order by history_date_time desc limit 1";
        } else {
            $sql = "select history_date_time from histories where user_id = '$id' and history_action like '{$key}%' order by history_date_time desc limit 1";
        }

        //echo $sql.'<br>';
        $q = mysqli_query($this->db, $sql);
        $min_no=2;
        if (mysqli_num_rows($q) > 0) {
            $d = mysqli_fetch_assoc($q);

            $time =  strtotime($d['history_date_time']. " +{$min} minutes") ;
            $cur_time = strtotime("-2 minutes");
            $run_time = $time;
//            echo $this->db->last_query().'<br>';
//            echo $key.' '.$cur_time.' '.date('r',$cur_time) . ' '. $time.' '.date('r',$time).' '.$q[0]['history_date_time']. " +{$min} minutes".'<br>';
            if ($cur_time > $time) {
                ;
                $run_time = strtotime("+{$min_no} minutes");
            }
        } else {
            //            echo '2x<br>';
            $run_time = strtotime("+{$min_no} mins");
        }
        return $run_time;
    }

    public function loadBalanceCronTask($next_time, $run=true)
    {
        $spread = $this->spread;
        $h = date('G', $next_time);
        $m = 1 * date('i', $next_time);
        $loop_end = false;
        while (!$loop_end) {
            if (isset($this->list_tasks [$h . "_" . $m]) && $this->list_tasks [$h . "_" . $m] > $spread) {
                $next_time += 61;
                $h = date('G', $next_time);
                $m = 1 * date('i', $next_time);
            } else {
                $loop_end = true;
                if ($run) {
                    if (isset($this->list_tasks [$h . "_" . $m])) {
                        $this->list_tasks [$h . "_" . $m] ++;
                    } else {
                        $this->list_tasks [$h . "_" . $m] = 1;
                    }
                }
                break;
            }
        }

        return ($run?'':'#')."$m $h * * * ";
    }
    public function get_all_custom_time()
    {
        $result = array();
        $sql="select * from cronjob_custom_time where status = 1";
        $q = mysqli_query($this->db, $sql);
        while ($d = mysqli_fetch_assoc($q)) {
            $result[$d['user_id']][$d['cronjob_id']] = $d['custom_time'];
        }
        return $result;
    }

    public function get_new_cron_time($cron_time, $time_zone)
    {
        $list = explode(' ', str_replace('  ', ' ', $cron_time));
        $min = $list[0];
        $hour = $list[1];
        $day = $list[2];
        $month = $list[3];
        $week = $list[4];
        if ($time_zone<0) {
            $time_zone = 24+$time_zone;
        }
        if (strpos($hour, '*')===false) {
            if (is_numeric($hour)) {
                $list[1] = ($hour+$time_zone) % 24;
            } elseif (strpos($hour, ',')) {
                $tmpx = explode(',', $hour);
                $h_list = array();
                foreach ($tmpx as $t) {
                    if (strpos($t, '-')) {
                        $tmp = explode('-', $t);
                        for ($i=$tmp[0];$i<=$tmp[1];$i++) {
                            $t = ($i+$time_zone)%24;
                            $h_list[$t] = $t;
                        }
                    } elseif (is_numeric($t)) {
                        $t = ($t+$time_zone)%24;
                        $h_list[$t] = $t;
                    }
                }
                ksort($h_list);
                $h_list = array_values($h_list);
                $rh_list =array();
                $pos = array();
                foreach ($h_list as $k=>$h) {
                    if (!isset($pos[0])) {
                        $pos[0] = $h;
                    } else {
                        $pos[1] = $h;
                    }

                    if (isset($h_list[$k+1])) {
                        if ($h_list[$k+1]==$h+1) {
                            continue;
                        } else {
                            if (!isset($pos[1])) {
                                $rh_list[] = $pos[0];
                                unset($pos);
                                continue;
                            } else {
                                $rh_list[] = $pos[0].'-'.$pos[1];
                                unset($pos);
                                continue;
                            }
                        }
                    } else {
                        if (!isset($pos[1])) {
                            $rh_list[] = $pos[0];
                            unset($pos);
                            continue;
                        } else {
                            $rh_list[] = $pos[0].'-'.$pos[1];
                            unset($pos);
                            continue;
                        }
                    }
                }

                $ox = implode(',', $rh_list);
                $list[1] = empty($ox)?0:$ox;
            } elseif (strpos($hour, '-')) {
                $tmp = explode('-', $hour);
                $h_list = array();
                for ($i=$tmp[0];$i<=$tmp[1];$i++) {
                    $t = ($i+$time_zone)%24;
                    $h_list[$t] = $t;
                }
                ksort($h_list);
                $h_list = array_values($h_list);
                $rh_list =array();
                $pos = array();
                foreach ($h_list as $k=>$h) {
                    if (!isset($pos[0])) {
                        $pos[0] = $h;
                    } else {
                        $pos[1] = $h;
                    }

                    if (isset($h_list[$k+1])) {
                        if ($h_list[$k+1]==$h+1) {
                            continue;
                        } else {
                            if (!isset($pos[1])) {
                                $rh_list[] = $pos[0];
                                unset($pos);
                                continue;
                            } else {
                                $rh_list[] = $pos[0].'-'.$pos[1];
                                unset($pos);
                                continue;
                            }
                        }
                    } else {
                        if (!isset($pos[1])) {
                            $rh_list[] = $pos[0];
                            unset($pos);
                            continue;
                        } else {
                            $rh_list[] = $pos[0].'-'.$pos[1];
                            unset($pos);
                            continue;
                        }
                    }
                }
                $ox = implode(',', $rh_list);
                $list[1] = empty($ox)?0:$ox;
            }
        }
        return implode(' ', $list);
    }
}

$gc = new GENCRON();
if (isset($_REQUEST['test'])) {
    $out = $gc->get_pk_zone_purchased(307, 'Amazon', '.de');
    echo '<pre>';
    if (!empty($out)) {
        echo ' not empty ';
        print_r($out);
    } else {
        print_r($out);
    }
    echo '</pre>';
} else {
    $gc->cronj_gen_tasks_file();
}
