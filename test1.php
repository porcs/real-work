<?php
//
date_default_timezone_set('Etc/GMT+2');
//date_default_timezone_set('Asia/Bangkok');
$dateWithTimeZone = date('Y-m-d H:i:s');
echo $dateWithTimeZone.'<br>';
$time = strtotime($dateWithTimeZone);
$dateInLocal = date('Y-m-d H:i:s', $time);
echo 'LOCAL: '.$dateInLocal.'<br>';

date_default_timezone_set('UTC');

$time = strtotime($dateWithTimeZone);
$dateInLocal = date('Y-m-d H:i:s', $time);
echo 'UTC: '.$dateInLocal.'<br>';

date_default_timezone_set('Etc/GMT+2');
$time = strtotime($dateWithTimeZone);
$dateInLocal = gmdate('Y-m-d H:i:s', $time);
echo 'GMT+2: '.$dateInLocal.'<br>';

$time = strtotime($dateWithTimeZone.' UTC');
$dateInLocal = gmdate('Y-m-d H:i:s', $time);
echo 'UTC: '.$dateInLocal.'<br>';
