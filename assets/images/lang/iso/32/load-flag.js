var fs = require('fs'),
    request = require('request');

var download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);

    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

var flat_name = ['abkhazia',
'afghanistan',
'aland',
'albania',
'algeria',
'american_samoa',
'andorra',
'angola',
'anguilla',
'antarctica',
'antigua_and_barbuda',
'argentina',
'armenia',
'aruba',
'australia',
'austria',
'azerbaijan',
'bahamas',
'bahrain',
'bangladesh',
'barbados',
'basque_country',
'belarus',
'belgium',
'belize',
'benin',
'bermuda',
'bhutan',
'bolivia',
'bosnia_and_herzegovina',
'botswana',
'brazil',
'british_antarctic_territory',
'british_virgin_islands',
'brunei',
'bulgaria',
'burkina_faso',
'burundi',
'cambodia',
'cameroon',
'canada',
'canary_islands',
'cape_verde',
'catalonia',
'cayman_islands',
'central_african_republic',
'chad',
'chile',
'china',
'christmas_island',
'cocos_keeling_islands',
'colombia',
'commonwealth',
'comoros',
'cook_islands',
'costa_rica',
'cote_divoire',
'croatia',
'cuba',
'curacao',
'cyprus',
'czech_republic',
'democratic_republic_of_the_congo',
'denmark',
'djibouti',
'dominica',
'dominican_republic',
'east_timor',
'ecuador',
'egypt',
'el_salvador',
'england',
'equatorial_guinea',
'eritrea',
'estonia',
'ethiopia',
'european_union',
'falkland_islands',
'faroes',
'fiji',
'finland',
'france',
'french_polynesia',
'french_southern_territories',
'gabon',
'gambia',
'georgia',
'germany',
'ghana',
'gibraltar',
'gosquared',
'greece',
'greenland',
'grenada',
'guam',
'guatemala',
'guernsey',
'guinea_bissau',
'guinea',
'guyana',
'haiti',
'honduras',
'hong_kong',
'hungary',
'iceland',
'india',
'indonesia',
'iran',
'iraq',
'ireland',
'isle_of_man',
'israel',
'italy',
'jamaica',
'japan',
'jersey',
'jordan',
'kazakhstan',
'kenya',
'kiribati',
'kosovo',
'kuwait',
'kyrgyzstan',
'laos',
'latvia',
'lebanon',
'lesotho',
'liberia',
'libya',
'liechtenstein',
'lithuania',
'luxembourg',
'macau',
'macedonia',
'madagascar',
'malawi',
'malaysia',
'maldives',
'mali',
'malta',
'mars',
'marshall_islands',
'martinique',
'mauritania',
'mauritius',
'mayotte',
'mexico',
'micronesia',
'moldova',
'monaco',
'mongolia',
'montenegro',
'montserrat',
'morocco',
'mozambique',
'myanmar',
'nato',
'nagorno_karabakh',
'namibia',
'nauru',
'nepal',
'netherlands_antilles',
'netherlands',
'new_caledonia',
'new_zealand',
'nicaragua',
'niger',
'nigeria',
'niue',
'norfolk_island',
'north_korea',
'northern_cyprus',
'northern_mariana_islands',
'norway',
'olympics',
'oman',
'pakistan',
'palau',
'palestine',
'panama',
'papua_new_guinea',
'paraguay',
'peru',
'philippines',
'pitcairn_islands',
'poland',
'portugal',
'puerto_rico',
'qatar',
'red_cross',
'republic_of_the_congo',
'reunion',
'romania',
'russia',
'rwanda',
'saint_barthelemy',
'saint_helena',
'saint_kitts_and_nevis',
'saint_lucia',
'saint_martin',
'saint_vincent_and_the_grenadines',
'samoa',
'san_marino',
'sao_tome_and_principe',
'saudi_arabia',
'scotland',
'senegal',
'serbia',
'seychelles',
'sierra_leone',
'singapore',
'sint_maarten',
'slovakia',
'slovenia',
'solomon_islands',
'somalia',
'somaliland',
'south_africa',
'south_georgia_and_the_south_sandwich_islands',
'south_korea',
'south_ossetia',
'south_sudan',
'spain',
'sri_lanka',
'sudan',
'suriname',
'swaziland',
'sweden',
'switzerland',
'syria',
'taiwan',
'tajikistan',
'tanzania',
'thailand',
'togo',
'tokelau',
'tonga',
'trinidad_and_tobago',
'tunisia',
'turkey',
'turkmenistan',
'turks_and_caicos_islands',
'tuvalu',
'us_virgin_islands',
'uganda',
'ukraine',
'united_arab_emirates',
'united_kingdom',
'united_nations',
'united_states',
'unknown',
'uruguay',
'uzbekistan',
'vanuatu',
'vatican_city',
'venezuela',
'vietnam',
'wales',
'wallis_and_futuna',
'western_sahara',
'yemen',
'zambia',
'zimbabwe']

for(var i=0; i<flat_name.length; i++)
download('http://az414932.vo.msecnd.net/images/icons/flags/fjf/'+flat_name[i]+'.png', flat_name[i].substr(0, 2)+'.png', function(){
  console.log('done');
});