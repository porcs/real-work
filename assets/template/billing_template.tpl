<div style="margin: 0px 0px 30px 0px;">
    <div style="float: left;text-align: left;width: 230px;">
        <img src="assets/images/logo_count.png" width="140">
    </div>
    <div style="float: right;text-align: right;font-family: Verdana, sans-serif;">
        <div style="margin: 0px 0px 5px 0px;"><span style="font-weight: bold;">Billing No:</span> {#billID}</div>
        <div><span style="font-weight: bold;">Date:</span> {#billDate}</div>
    </div>
</div>
<div style="line-height: 17px;margin: 0px 0px 20px 0px;font-family: Verdana, sans-serif;">
    <div style="font-weight: bold;">Billing Address</div>
    {#billAddress}
</div>
<div style="margin: 0px 0px 5px 0px;">
    <div style="float: left;text-align: left;background-color: #464646;width: 30px;color: #FFF;font-weight: bold;padding: 0px 5px;line-height: 40px;font-family: Verdana, sans-serif;">
        &nbsp;
    </div>
    <div style="float: left;text-align: left;background-color: #464646;width: 440px;color: #FFF;font-weight: bold;padding: 0px 5px;line-height: 40px;font-family: Verdana, sans-serif;">
        Package Descriptions
    </div>
    <div style="float: left;text-align: left;background-color: #464646;width: 130px;color: #FFF;font-weight: bold;padding: 0px 5px;line-height: 40px;font-family: Verdana, sans-serif;">
        Package Option
    </div>
    <div style="float: left;text-align: left;background-color: #464646;width: 70px;color: #FFF;font-weight: bold;padding: 0px 5px;line-height: 40px;font-family: Verdana, sans-serif;">
        Amount
    </div>
</div>
{#billDetail}
<div style="margin: 20px 13px 0px 0px;">
    <div style="text-align: right;font-family: Verdana, sans-serif;">
        <span style="font-weight: bold;">Total Amount:</span> {#totalAmount}
    </div>
</div>