<?php

/* Update every day */

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.validValues.update.php';

class AmazonUpdateValidValues extends UpdateValidValues
{
    public function __construct() {
        parent::__construct();
    }
    
    public function Dispatch(){
        
        $error = '';
        $output = array();
        
	if(!$output['ValidValues'] = $this->InstallValidValues()){
            $error .= "Install Valid Values Error";
        }
        if(!$output['ShippingMethods'] = $this->InstallShippingMethods()){
            $error .= "Install Shipping Methods Error";
        }
        if(!$output['FieldSettings'] = $this->InstallFieldSettings()){
            $error .= 'Install Field Settings Error';
        }
        if(!$output['FieldTranslations'] = $this->InstallFieldTranslations()){
            $error .= "Install Field Translations Error";
        }
        echo json_encode(
            array(
                'error' => $error,
                'output' => $output,
                'pass' => (strlen($error) > 0) ? true : false,
            )
        );

        exit;
    }    
            
}

$update_validvalue = new AmazonUpdateValidValues();
$update_validvalue->Dispatch();