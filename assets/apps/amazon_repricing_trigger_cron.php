<?php 

define('BASEPATH', '');
chdir(dirname(__FILE__)); 

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.config.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');
require_once(dirname(__FILE__) . '/../../application/libraries/UserInfo/configuration.php');

//https://qa.feed.biz/assets/apps/amazon_repricing_trigger_cron.php?debug=1&id_customer=26&user_name=u00000000000026&id_shop=1&shop_name=FOXCHIP

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;

if($debug) {

    $info = array(
	'user_id' => $_GET['id_customer'],
	'user_name' => $_GET['user_name'],
	'id_shop' => $_GET['id_shop'],
	'shop_name' => $_GET['shop_name'],
	'id_marketplace' => 2,
	'next_time' => 1440,
	'owner' => 'system',
	'action' => 'fba_report',
    );

} else {

    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]==''
        || !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !isset($argv[5]) || $argv[5]==''){
        die( 'No input data!');
    }

    //Get argv from url
    $info = array(
        'user_id' => $argv[1],
        'user_name' => $argv[2],
        'id_shop' => $argv[3],
        'shop_name' => str_replace('_',' ',$argv[4]),
        'id_marketplace' => $argv[5],
        'next_time' => 1440,
        'owner' => 'system',
        'action' => 'trigger_repricing',
    );
}
    
if(!isset($info) || empty($info) ){

    $info['countries'] = $info['shop_name'];

    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    $id_shop = $info['id_shop'];
    $shop_id_list = array($id_shop);

    $userInfo = new UserConfiguration();
    $amazon_database = new AmazonDatabase($info['user_name'], $debug);
    $cron_running = $userInfo->getAmazonAllCron($info['user_id'], $shop_id_list, true);

    if($debug) {
        //echo '<br/> <b>Cron</b> : <pre>' . print_r($cron_running, true) . '</pre> <br/>';
    }

    // find checked region
    foreach($cron_running as $running_process) {
        if(isset($running_process['deny']['trigger_repricing']) && $running_process['deny']['trigger_repricing'] == false){

            $info['ext'] = $running_process['ext'];
            $info['countries'] = $running_process['countries'];
            $id_country = $running_process['id_country'];

            $features = $amazon_database->get_configuration_features($id_country, $info['id_shop']);

            if($debug) {
                echo '<br/>---------------------------------------------------------------------------------------------------------';
                echo '<br/> <b>'.$running_process['ext'].'</b> : <br/>';
                echo '- use repricing all offers<br/>';
                //echo '<br/> <b>Features</b> : <pre>' . print_r($features, true) . '</pre><br/>';
            }

            if(isset($features['repricing']) && $features['repricing']) {

                if($debug) {
                    echo '- active repricing<br/>';
                    echo '<br/> ---------- <b>query</b> ---------- <br/>';
                }
                
                // Set history log
                AmazonHistoryLog::set($info);
                // trigger repricing for all offers
                $amazon_database->update_repricing_product_update_log($info['id_shop'], $id_country);
                echo '\n <b>Trigger repricing Success ext</b>  : ' . $running_process['ext'] . '\n';

            } else {
                if($debug) {
                    echo '- inactive repricing<br/>';
                }
            }
         }
    }
}