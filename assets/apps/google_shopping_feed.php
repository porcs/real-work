<?php if( !defined('BASEPATH') ) define('BASEPATH', '');

chdir(dirname(__FILE__));
require_once(dirname(__FILE__) . '/../../application/libraries/Marketplaces/classes/marketplaces.configuration.php');
require_once(dirname(__FILE__) . '/../../application/libraries/GoogleShopping/classes/google.config.php');
require_once(dirname(__FILE__) . '/../../application/libraries/GoogleShopping/functions/google.feed.php');

class Google_feed {
    protected $user_name;
    protected $id_shop;
    protected $id_mode = 1;
    protected $marketplace_name;
    protected $id_country;
    protected $id_marketplace;
    protected $id_lang;
    
    public static $google_field = array(
        'id'                        => array('tag' => 'g:id'),
        'title'                     => array('tag' => 'title'),
        'description'               => array('tag' => 'description'),
        'google_product_category'   => array('tag' => 'g:google_product_category'),
        'product_type'              => array('tag' => 'g:product_type'),
        'link'                      => array('tag' => 'link'),
        'mobile_link'               => array('tag' => 'g:mobile_link'),
        'image_link'                => array('tag' => 'g:image_link'),
        'additional_image_link'     => array('tag' => 'g:additional_image_link'),
        'condition'                 => array('tag' => 'g:condition'),
        'gtin'                      => array('tag' => 'g:gtin'),
        'mpn'                       => array('tag' => 'g:mpn'),
        'brand'                     => array('tag' => 'g:brand'),
        'identifier_exists'         => array('tag' => 'g:identifier_exists'),
        'availability'              => array('tag' => 'g:availability'),
        'availability_date'         => array('tag' => 'g:availability_date'),
        'price'                     => array('tag' => 'g:price'),
        'sale_price'                => array('tag' => 'g:sale_price'),
        'sale_price_effective_date' => array('tag' => 'g:sale_price_effective_date'),
        'color'                     => array('tag' => 'g:color'),
        'gender'                    => array('tag' => 'g:gender'),
        'age_group'                 => array('tag' => 'g:age_group'),
        'material'                  => array('tag' => 'g:material'),
        'pattern'                   => array('tag' => 'g:pattern'),
        'size'                      => array('tag' => 'g:size'),
        'size_type'                 => array('tag' => 'g:size_type'),
        'size_system'               => array('tag' => 'g:size_system'),
        'custom_label_0'            => array('tag' => 'g:custom_label_0'),
        'custom_label_1'            => array('tag' => 'g:custom_label_1'),
        'custom_label_2'            => array('tag' => 'g:custom_label_2'),
        'custom_label_3'            => array('tag' => 'g:custom_label_3'),
        'custom_label_4'            => array('tag' => 'g:custom_label_4'),
        'multipack'                 => array('tag' => 'g:multipack'),
        'is_bundle'                 => array('tag' => 'g:is_bundle'),
        'adult'                     => array('tag' => 'g:adult'),
        'adwords_redirect'          => array('tag' => 'g:adwords_redirect'),
        'expiration_date'           => array('tag' => 'g:expiration_date'),
        'energy_efficiency_class'   => array('tag' => 'g:energy_efficiency_class'),
        'unit_pricing_measure'      => array('tag' => 'g:unit_pricing_measure'),
        'unit_pricing_base_measure' => array('tag' => 'g:unit_pricing_base_measure'),
        'shipping_weight'           => array('tag' => 'g:shipping_weight'),
        'shipping_label'            => array('tag' => 'g:shipping_label'),
    );
    
    public function __construct() {
        $this->marketplace_prefix = _PREFIX_;
        $this->marketplace_name = _MARKETPLACE_NAME_;
        $this->user_name = $_GET['user_name'];
        $this->id_shop = $_GET['id_shop'];
        $this->id_country = $_GET['id_country'];
        $this->id_marketplace = $_GET['id_marketplace'];
    }

    public function get_google_types($value = 'text') {
        switch ($value['hidden']) {
            case 'select' :
                $type = 'item_field';
            break;
            case 'select|select' :
                $type = 'item_field';
            break;
            case 'input' :
                $type = 'text';
            break;
            case 'select|input' :
                $type = 'text';
            break;
            default :
                $type = 'text';
            break;
        }
        return $type;
    }

    public function Dispatch() {
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        $this->feedbiz = new FeedBiz(array($this->user_name));
        $this->data['marketplace_option'] = array();
        $this->data['profiles'] = $configuration->getProfiles($this->id_shop, $this->id_country);
        $this->data['models'] = $configuration->getModels($this->id_shop, $this->id_country);
        $this->data['configuration'] = $configuration->getParameters($this->id_shop, $this->id_country);
        $this->data['category'] = $configuration->getSelectedCategories($this->id_shop, $this->id_country);
        $this->data['condition'] = $this->feedbiz->getConditionMapping($this->user_name, $this->id_shop, $this->id_marketplace);
        $this->data['products_option'] = $configuration->getProductOption($this->id_shop, $this->id_country);
        $this->data['marketplace_option'] = $this->feedbiz->marketplace_getProductOption($this->id_shop, null, null, null, null, null, $this->id_country, $this->id_marketplace);
        if ( !empty($this->data['products_option']) ) {
            $this->data['marketplace_option'] = $this->data['marketplace_option'] + $this->data['products_option'];
        }
        $item_mapping = $profile = $conditions = array();
        $lang_default = $this->feedbiz->getLanguageDefault($this->id_shop);
        $this->id_lang = '';
        if ( !empty($lang_default) ) {
            $lang_default = current($lang_default);
            $this->id_lang = $lang_default['id_lang'];
            $this->product_iso_code = $lang_default['iso_code'];
        }

        $DefaultShop = $this->feedbiz->getDefaultShop($this->user_name);
        if ( !empty($DefaultShop) ) {
            $this->id_shop = $DefaultShop['id_shop'];
            $this->shop_default = $DefaultShop['name'];
        }
            
        if ( !empty($this->data['condition']) ) {
            foreach ( $this->data['condition'] as $condition ) {
                $conditions[$condition['txt']] = $condition['condition_value'];
            }
        }

        if ( !empty($this->data['category']) ) {
            foreach ( $this->data['category'] as $category ) {
                $categories[$category['id_category']] = $category['id_profile'];
            }
        }

        if ( !empty($this->data['profiles']) ) {
            foreach ( $this->data['profiles'] as $id_profile => &$profile ) {
                if ( !empty($profile['id_model']) && !empty($this->data['models']) && array_key_exists($profile['id_model'], $this->data['models']) ) {
                    $id_model = $profile['id_model'];
                    $grouping = array();
                    foreach ( $this->data['models'][$id_model] as $model_key => &$model ) {
                        switch ($model_key) {
                            case 'product_universe' : 
                                if ( !empty($model) ) {
                                    $item_mapping['item_mapping']['google_product_category'] = array(
                                        'type'      => 'text',
                                        'value'     => $model,
                                        'tag'       => 'g:google_product_category',
                                    );
                                }
                            break;
                            case 'product_type' : 
                                if ( is_array($model) && array_key_exists($model_key, self::$google_field) ) {
                                    $item_mapping['item_mapping']['product_type'] = array(
                                        'type'      => 'array_text',
                                        'value'     => serialize($model),
                                        'tag'       => 'g:product_type',
                                    );
                                }
                            break;
                            case 'variation_theme' :
                            case 'specific_fields' :
                            case 'custom_attributes' :
                                foreach ( $model as $item_key => $item ) {
                                    if ( $item_key == 'mobile_link' && !empty($item['value']) ) {
                                        $item_mapping['item_mapping'][$item_key] = array(
                                            'type'      => 'item_field',
                                            'value'     => 'link',
                                            'tag'       => 'link',
                                        );
                                        continue;
                                    }
                                    if ( $item_key == 'additional_image_link' && !empty($item['value']) ) {
                                        $item_mapping['item_mapping'][$item_key] = array(
                                            'type'      => 'item_field',
                                            'value'     => 'images',
                                            'tag'       => self::$google_field[$item_key]['tag'],
                                        );
                                        continue;
                                    }
                                    if ( is_array($item) && array_key_exists($item_key, self::$google_field) && !empty($item) ) {
                                        if ( !empty($item['grouping']) ) {
                                            $grouping[$item_key] = 'on';
                                        }
                                        $item_mapping['item_mapping'][$item_key] = array(
                                            'type'      => $this->get_google_types($item),
                                            'value'     => $item['value'],
                                            'tag'       => self::$google_field[$item_key]['tag'],
                                        );
                                    }
                                    
                                }
                            break;
                            default :
                                $item_mapping['item_mapping']['id'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'id_product',
                                    'tag'       => 'g:id',
                                );
                                $item_mapping['item_mapping']['title'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'name',
                                    'tag'       => 'title',
                                );
                                $item_mapping['item_mapping']['description'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'description',
                                    'tag'       => 'description',
                                );
                                $item_mapping['item_mapping']['link'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'link',
                                    'tag'       => 'link',
                                );
                                $item_mapping['item_mapping']['image_link'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'images',
                                    'tag'       => 'g:image_link',
                                );
                                $item_mapping['item_mapping']['condition'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'condition',
                                    'tag'       => 'g:condition',
                                );
                                $item_mapping['item_mapping']['price'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'price',
                                    'tag'       => 'g:price',
                                );
                                $item_mapping['item_mapping']['availability'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'active',
                                    'tag'       => 'g:availability',
                                );
                                $item_mapping['item_mapping']['availability_date'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'available_date',
                                    'tag'       => 'g:availability_date',
                                );
                                $item_mapping['item_mapping']['unit_pricing_measure'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'unit_price',
                                    'tag'       => 'g:unit_pricing_measure',
                                );
                                $item_mapping['item_mapping']['unit_pricing_base_measure'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'unity',
                                    'tag'       => 'g:unit_pricing_base_measure',
                                );
                                $item_mapping['item_mapping']['brand'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'manufacturer',
                                    'tag'       => 'g:brand',
                                );
                            break;
                        }
                    }
                    $profile['data'][$profile['id_model']] = array(
                        'name'      => $profile['name'],
                        'group'     => !empty($grouping) ? 'Yes' : 'No',
                        'shipping'  => !empty($this->data['configuration']['export_shipping']) ? 'Yes': 'No',
                    );
                    if ( !empty($grouping) ) {
                        $profile['data'][$profile['id_model']]['grouping'] = $grouping;
                    }
                    if ( !empty($item_mapping) ) {
                        $profile['data'][$profile['id_model']]['item_mapping'] = $item_mapping['item_mapping'];
                    }
                }
            }
        }   
	
        $google_feed = new GoogleFeed(array($this->user_name));
        $xml = $google_feed->getXmlFeedByArray($this->user_name, $this->id_shop, $this->id_lang, $this->id_mode, $this->product_iso_code, $this->data['profiles'], $conditions, $categories);
        $this->dir_name = dirname(__FILE__) . '/users/' .$this->user_name. '/google/';
        if(!is_dir($this->dir_name)) :
                $old = umask(0); 
                mkdir($this->dir_name, 0777, true);
                umask($old); 
        endif;
        $xml->preserveWhitespace = false;
        $xml->formatOutput = true;
        $xml->save($this->dir_name.'gmc_export_' .$this->product_iso_code. '.xml');
    }
}


$google_feed = new Google_feed();
$google_feed->Dispatch();

