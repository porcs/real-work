<?php
        define ( 'BASEPATH', '' );
        chdir ( dirname ( __FILE__ ) );
	require_once(dirname(__FILE__) . '/FeedbizImport/config/products.php');
        require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');

	class garbage_db extends ObjectModel {
            public $user = null;
                public function __construct($user = null) {
                        parent::__construct($user);
                        
                        $this->user = !empty($user) ? $user: '';
                }

                public function garbage_delete_db() {
                        $oldmask = umask(0);
                        $conn_id = $this->conn_id;
                        $sql = $error = '';

                        if(!$conn_id)
                                return false;
                                
                        $result_database = $this->db_exec("BEGIN;DELETE FROM ebay_statistics WHERE date_add < date(julianday(date('now')) - 15);DELETE FROM ebay_product_details WHERE export_pass != 1 and date_add < date(julianday(date('now')) - 15);COMMIT;");

                        if(!$result_database)
                           $error = sprintf('Delete database error, %s', $this->user) ;

                        if(isset($error) || !empty($error) || $error == null)
                                $output = sprintf('Delete database success, %s', $this->user) ;

                        $json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $output) ) ;
                    
                        return $json;
                }

	}
        
        if (! isset ( $argv [1] ) || $argv [1] == '' || ! is_numeric ( $argv [1] ) || ! isset ( $argv [2] ) || $argv [2] == '' ) {
                die ( 'No input data!' );
        }

        $user_id = $argv [1];
        $user_name = $argv [2];
        $shop_name = '';
        
        $error  = null ;
        $output = null;
        $type = 'delete_statistic';
        $fail               = 1;
        $batch_id = !empty($product->batch_id) ? $product->batch_id : 'check exist ebay running';
        
        $rep = new report_process($user_name, $batch_id, "Delete statistic", true, true, true, "delete_statistic");
        if($rep->has_other_running($type)){
            die();
        }
 
        $rep->set_process_type($type); 
        $rep->set_priority(1); 
        $rep->set_shop_name($shop_name); 

        
        $garbage_db = new garbage_db(($user_name));
        $json = $garbage_db->garbage_delete_db();
        
        ( !empty($rep) ) ? $rep->finish_task($fail) : $rep->finish_task(0);

