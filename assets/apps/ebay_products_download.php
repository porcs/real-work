<?php
    require dirname(__FILE__). "/../../application/libraries/Ebay/ebayExport.php";
    $error  = null ;
    $output = null;

    $data                       = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $userdata                   = json_decode($data);
    $user                       = str_replace(" ", "_", $userdata->username);
    $user_id                    = $userdata->userid;
    $language                   = $userdata->language;
    $site                       = $userdata->site;
    $date_to                    = $userdata->date;
   
    if(isset($error) || !empty($error) || $error == null)
        $output = sprintf('User %s successfully created', $user) ;

    $product                    = new ebayExport(array($user, $user_id, isset($language) ? $language : 'en', isset($site) ? $site : 0));
    $result                     = $product->export_download(date("Y-m-d", strtotime(isset($date_to) ? "$date_to" : "now")));
    $json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
    echo $json;