<?php

require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.scheme.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.history.log.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/functions/mirakl.orders.accept.php';

$debug = false;

if (!$debug) {
    if (!isset($argv[1]) || $argv[1] == '' || !is_numeric($argv[1]) ||
            !isset($argv[2]) || $argv[2] == '' || !is_numeric($argv[2]) ||
            !isset($argv[3]) || $argv[3] == '' || !is_numeric($argv[3]) ||
            !isset($argv[4]) || $argv[4] == '' || !is_numeric($argv[4])
    ) {
        die('No input data!');
    }
} else {
    $argv[1] = 56;
    $argv[2] = 1;
    $argv[3] = 13;
    $argv[4] = 10;
}

// init var
$id_user = $argv[1];
$id_shop = $argv[2];
$sub_marketplace = $argv[3];
$id_country = $argv[4];

// init params
$order_id = '';

// get data
$mirakl_scheme = new MiraklScheme();
$shop_info = $mirakl_scheme->getShopInfo($id_user);
$get_config = $mirakl_scheme->getMiraklConfiguration($id_user, $sub_marketplace, $id_country, $id_shop);

$params = array();
$params['order_id'] = $order_id;
$params['accept_order'] = true;

$mirakl_function = new MiraklOrderAccept((object) $get_config, $debug, true);
$results = $mirakl_function->acceptOrder($params);


$log_history = array(
    'user_id' => $id_user,
    'ext' => $get_config['ext'],
    'shop_name' => isset($shop_info['name']) ? $shop_info['name'] : '',
    'countries' => $get_config['countries'],
    'sub_marketplace' => $sub_marketplace,
    'next_time' => 60,
    'owner' => 'system',
    'action' => MiraklHistoryLog::ACTION_TYPE_ACCEPT_ORDER,
    'transaction' => $results['status'] // check status in woking for check next time.
);

// Set history log
MiraklHistoryLog::set($log_history);

// response to node
//echo json_encode($results);
