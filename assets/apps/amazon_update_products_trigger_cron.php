<?php 

define('BASEPATH', '');
chdir(dirname(__FILE__)); 

require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.feeds.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;

if($debug) {

    $info = array(
	'user_id' => $_GET['id_customer'],
	'user_name' => $_GET['user_name'],
	'ext' => $_GET['ext'],
	'id_shop' => $_GET['id_shop'],
	'shop_name' => $_GET['shop_name'],
	'id_marketplace' => $_GET['id_marketplace'],
        'next_time' => 60*24,
        'owner' => 'system',
        'action' => 'synchronize_product',
    );

    if(isset($_GET['remove_debug']) && $_GET['remove_debug']){
        $debug = false;
    }

} else {

    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]==''
        || !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
        || !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]==''){
        die( 'No input data!');
    }

    // Get argv from url
    $info = array(
        'user_id' => $argv[1],
        'user_name' => $argv[2],
        'ext' => $argv[3],
        'id_shop' => $argv[4],
        'shop_name' => str_replace('_',' ',$argv[5]),
        'id_marketplace' => $argv[6],
        'next_time' => 60*24,
        'owner' => 'system',
        'action' => 'synchronize_product',
    );

}

// Get userdata fromdatabase
$data = AmazonUserData::get($info);
$info['countries'] = $data['countries'];
// Set history log
AmazonHistoryLog::set($info);

if(!isset($data) || empty($data) ){
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {

    // check amazon status
    $amazonDatabase = new AmazonDatabase($info['user_name']);
    $status = $amazonDatabase->get_amazon_status($data['id_country'], $data['id_shop'], 'update_product', true, null, 1);
    if (isset($status) && count($status) > 0 && !empty($status)) {
        # Delete status
        if(!$debug) {
            $amazonDatabase->delete_amazon_status($data['id_country'], $data['id_shop'], 'update_product');
        }

        $data['mode'] = AmazonDatabase::SYNC;
        $data['update_type'] = 'update_product_flag'; //'product';
        $object = json_decode(json_encode($data), FALSE);

        // Synchronize Amazon
        $object->creation = false;
        $amazon_feeds = new AmazonFeeds($object, AmazonDatabase::SYNC, $debug, true);
        $amazon_feeds->FeedsData();

    } else {
        $json = json_encode( array('error' => false, 'pass' => true, 'output' => 'No trigger update product') ) ;
        echo $json ; exit;
    }
}