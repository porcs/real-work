<?php
define ( 'BASEPATH', '' );
// ini_set('display_errors',1);
// ini_set('display_startup_errors',1);
// error_reporting(-1);
chdir ( dirname ( __FILE__ ) );
require_once (dirname ( __FILE__ ) . '/../../application/config/config.php');
require_once (dirname ( __FILE__ ) . '/../../libraries/ci_db_connect.php');
require_once (dirname ( __FILE__ ) . '/../../application/libraries/FeedBiz.php');
require_once (dirname ( __FILE__ ) . '/../../application/libraries/FeedBiz_Orders.php');
require_once (dirname ( __FILE__ ) . '/../../application/libraries/Ebay/ebayorder.php');
// if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])){
// die( 'No input data!');
// }
// $user_id = $argv[1];
class LoadMPOrderCron {
	var $mysqlPortal = null;
	var $user_id = 0;
	var $user_name = '';
	var $user_code = '';
	var $shop_id = 0;
	public function __construct($user_id) {
		$this->user_id = $user_id;
		$this->mysqlPortal = new MysqlPortal ();
		$user = $this->mysqlPortal->getUsers ( $this->user_id );
		$this->user_name = $user ['user_name'];
		$this->user_code = $user ['user_code'];
		$FeedBiz = new FeedBiz ( array (
				$this->user_id 
		) );
		$shops = $FeedBiz->getDefaultShop ( $this->user_name );
		foreach ( $shops as $shop ) {
			$this->shop_id = $shop ['id_shop'];
		}
	}
	public function dispatch() {
		$marketplace_array = $this->mysqlPortal->getMarketplaces ( $this->user_id );
		// MARKETPLACE API
		foreach ( $marketplace_array as $mp_id => $mp_detail ) {
			switch ($mp_id) {
				case 3 : // EBAY
					$param = array (
							$this->user_name,
							$this->user_id,
							$mp_detail ['id_site_ebay'] 
					);
					$ebayorder = new ebayorder ( $param );
					$data_from = date ( 'Y-m-d', strtotime ( '-60 days' ) );
					$data_to = date ( 'Y-m-d', strtotime ( '+2 days' ) );
					// API
					$parser = $ebayorder->get_orders ( $data_from, $data_to, 60 );
					$imported_order = $ebayorder->get_imported_orders ();
					// LOG
					if ($imported_order) {
						$config_data = $this->mysqlPortal->getConfiguration ( $this->user_id );
						$feed = unserialize ( base64_decode ( $config_data ['FEED_SHOP_INFO'] ) );
						$batch_id = substr ( str_replace ( '.', '', microtime ( true ) ), 0, 13 );
						$configuration = $config_data ['EBAY_USER'];
						$ebay_user = $configuration [0]->value;
						$feedBiz_Orders = new FeedBiz_Orders ( array (
								$this->user_name 
						) );
						$feedBiz_Orders->stockMovement ( $this->user_name, $feed ['url'] ['stockmovement'], $this->user_code, $this->shop_id, $batch_id, 3, $mp_detail ['id_site_ebay'], $imported_order );
					}
					break;
			}
		}
	}
}

class MysqlPortal {
	var $ci_db_connect = null;
	public function __construct() {
		$this->ci_db_connect = new ci_db_connect ();
	}
	public function getUsers($user_id) {
		$user_sql = "select id as user_id, user_name, user_code from users where id = '$user_id'";
		$user_query = $this->ci_db_connect->select_query ( $user_sql );
		$user_row = $this->ci_db_connect->fetch ( $user_query );
		return $user_row;
	}
	public function getMarketplaces($user_id) {
		$marketplace_array = array ();
		$marketplace_sql = "select p.id_offer_pkg as id_marketplace, p.id_site_ebay
						from user_package_details u
						join offer_price_packages p on u.id_package = p.id_offer_price_pkg
						where u.id_users = '" . $user_id . "'";
		$marketplace_query = $this->ci_db_connect->select_query ( $marketplace_sql );
		while ( $row = $this->ci_db_connect->fetch ( $marketplace_query ) ) {
			$marketplace_array [$row ['id_marketplace']] = $row;
		}
		return $marketplace_array;
	}
	function getConfiguration($user_id) {
		$mainconfiguration = array ();
		$configuration_sql = "select * from configuration where id_customer = '" . intval ( $user_id ) . "' and name IN ('FEED_BIZ', 'FEED_MODE','FEED_SHOP_INFO','FIRST_CONF', 'EBAY_USER')";
		$configuration_query = $this->ci_db_connect->select_query ( $configuration_sql );
		while ( $row = $this->ci_db_connect->fetch ( $configuration_query ) ) {
			$mainconfiguration [$row ['name']] = $row ['value'];
		}
		return $mainconfiguration;
	}
}

$user_id = 35;
$object = new LoadMPOrderCron ( $user_id );
$object->dispatch ();