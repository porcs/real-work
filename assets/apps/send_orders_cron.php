<?php

define('BASEPATH', '');
chdir(dirname(__FILE__));

if (!isset($argv[1]) || $argv[1] == '' || !is_numeric($argv[1]) || !isset($argv[2]) || $argv[2] == '' || !isset($argv[3]) || $argv[3] == '' || !is_numeric($argv[3]) || !isset($argv[4]) || $argv[4] == '' || !is_numeric($argv[4]) || !isset($argv[5]) || $argv[5] == '' || !is_numeric($argv[5]) || !isset($argv[6]) || $argv[6] == '' || !isset($argv[7]) || $argv[7] == '' || !is_numeric($argv[7])
) {
    //argv from Ajax
    $argvs = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($argv[1])), null, 'UTF-8');
    $datas = json_decode($argvs);
    if (isset($datas->data) && !empty($datas->data)) {
        $data_decode = json_decode($datas->data);
        $data['id_user'] = $data_decode->id_user;
        $data['user_name'] = $data_decode->user_name;
        $data['id_marketplace'] = $data_decode->id_marketplace;
        $data['site'] = $data_decode->site;
        $data['id_shop'] = $data_decode->id_shop;
        $data['shop_name'] = $data_decode->shop_name;
        $data['cron_send_orders'] = $data_decode->cron_send_orders;
    } else {
        echo json_encode('No input data!');
        die('No input data!');
    }
} else {
    //argv from php
    $data['id_user'] = $argv[1];
    $data['user_name'] = $argv[2];
    $data['id_marketplace'] = $argv[3];
    $data['site'] = $argv[4];
    $data['id_shop'] = $argv[5];
    $data['shop_name'] = $argv[6];
    $data['cron_send_orders'] = $argv[7];
    $data['sub_marketplace'] = !empty($argv[9]) ? $argv[9] : ''; // 9 for param sub_marketplace
}

if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
    $str = implode(' ', $argv);
    ob_start();
    $cmd = "ps aux | grep php | grep '{$str}'  | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' ";
    passthru($cmd);
    if (sizeof(explode("\n", trim(ob_get_clean()))) > 1) {
        exit("dup");
    }
}

if (!isset($data) || empty($data)) {
    $json = json_encode(array('error' => true, 'pass' => false, 'output' => 'Access Denided.'));
    echo $json;
    exit;
} else {
    require_once dirname(__FILE__).'/../../application/libraries/FeedBiz/config/orders.php';
    require_once dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php';
    require_once dirname(__FILE__).'/../../application/libraries/UserInfo/feedbiz_otp.php';
    require_once dirname(__FILE__).'/../../application/libraries/Amazon/functions/amazon.status.orders.php';
    require_once(dirname(__FILE__).'/FeedbizImport/log/RecordProcess.php');
    require_once(dirname(__FILE__).'/../../application/libraries/tools.php');
    require_once(dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.database.php');
    require_once(dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.parameter.php');

    $id_user = $data['id_user'];
    $id_shop = $data['id_shop'];
    $site = $data['site'];
    $id_marketplace = $data['id_marketplace'];
    $user_name = $data['user_name'];
    $shop = $data['shop_name'];
    $cron_send_orders = $data['cron_send_orders'];
    $sub_marketplace = isset($data['sub_marketplace']) && !empty($data['sub_marketplace']) ? $data['sub_marketplace'] : ''; // for mirakl.

    $amazon_id_marketplace = Amazon_Order::$MarketplaceID;
    $eBay_id_marketplace = 3;
    $mirakl_id_marketplace = MiraklParameter::MARKETPLACE_ID; // 6
    $log_detail = array();

    $history = array();
    $config = new UserConfiguration();
    $OTP = new FeedbizOTP();
    $batch_id = uniqid();
    $date = date('Y-m-d H:i:s');
    $loop = 1;
    $next_time = 20;
    $shop_name = str_replace('_', ' ', $data['shop_name']);

    $userdata = $config->getShopInfo($id_user);
    $Language = $config->getUserDefaultLaguage($id_user);
    $Marketplace = $config->getMargetplaceById($id_marketplace, $site);
    $user_data = $config->getUserById($id_user);

    if ($id_marketplace == $mirakl_id_marketplace && !empty($sub_marketplace)) {
        $type = "send_mirakl_{$sub_marketplace}_{$data['site']}_orders";
    } else {
        $type = "send_{$data['site']}_orders";
    }

    $config->record_history($id_user, $shop_name, $type, $next_time);

    // Load lange file
    load('orders', $Language);

    if (isset($userdata) && !empty($userdata)) {
        $process_title = l('Exporting your orders to ').$shop.'...';
        $process_type = 'Export orders';

        $proc_rep = new report_process($user_name, $batch_id, $process_title, false, true);
        $proc_rep->set_process_type($process_type);

        $token = $config->getUserCode($id_user);

        $Orders = new Orders($user_name);
        $OrderItems = new OrderItems($user_name);
        $amazon_database = new Amazon_Order($user_name);

        // declare mirakl db.
        $mirakl_database = new MiraklDatabase($user_name);

        // Get all send orders
        $send_order_list = $Orders->getOrdersToSend($id_shop, $id_marketplace, $site, $sub_marketplace); // $sub_marketplace for mirakl.
        $send_order_item_list = $OrderItems->getOrderItems($id_shop, $send_order_list);

        if (isset($send_order_list) && !empty($send_order_list)) {
            if ($loop == 1) {
                $proc_rep->set_max_min_task(sizeof($send_order_list), 1);
            }

            foreach ($send_order_list as $key => $order) {
                $proc_rep->set_running_task($loop);
                $id_orders = $order['id_orders'];

                if (!isset($send_order_item_list[$id_orders]) ||
                        empty($send_order_item_list[$id_orders])) {
                    // When marketplace is Amazon : go to get order item again
                    if ($id_marketplace == $amazon_id_marketplace) {
                        $amazon_database->saveOrderMissingItems($id_shop, $id_orders, $order['id_marketplace_order_ref']);
                    }
                    $Orders->updateOrderErrorStatus($id_orders, '0', 'waiting order items.');

                    continue;
                }

                $id_site = $order['site'];
                $token = str_replace(" ", "_", $token);

                // History of Marketplace
                $history['process'][$id_orders] = true;

                // generate_otp
                $order_otp = $OTP->generate_otp('order', $id_orders, $token);

                $params = array(
                    'base_url' => $userdata['base_url'],
                    'url' => $userdata['order_url'],
                    'token' => $token,
                    'id_order' => $id_orders,
                    'otp' => $order_otp,
                );

                // get url 
                $url = $config->prepare_argv($params);

                // send order cron
                $result = $Orders->send_orders($url, $id_orders, true);

                // log error
                switch ($id_marketplace) {

                    //Log Amazon
                    case $amazon_id_marketplace :
                        $history['error'][$id_orders] = true;
                        if ($result['pass'] == false) {
                            $error_data = array(
                                'batch_id' => $batch_id,
                                'id_shop' => $id_shop,
                                'id_country' => $id_site,
                                'action_process' => Amazon_Order::$Export_Orders,
                                'action_type' => 'error',
                                'message' => '#'.$id_orders.' - '.$result['output'],
                                'date_add' => $date,
                            );
                            $amazon_database->save_validation_log($error_data);
                        } else {
                            if (isset($order['id_marketplace_order_ref']) &&
                                    isset($result['order_number']) &&
                                    !empty($order['id_marketplace_order_ref']) &&
                                    !empty($result['order_number'])) {
                                Amazon_Order::$orders[$id_orders]['AmazonOrderID'] = $order['id_marketplace_order_ref'];
                                Amazon_Order::$orders[$id_orders]['MerchantOrderID'] = $result['order_number'];
                            }
                        }
                        break;

                    case $mirakl_id_marketplace :
                        $history['error'][$id_orders] = true;
                        
                        $log_detail[$key]['reference'] = $id_orders;
                        $log_detail[$key]['message_code'] = '';
                        $log_detail[$key]['message'] = "# $id_orders : {$result['output']} ";

                        break;
                }

                // delete otp
                $OTP->del_otp('order', $id_orders, $token);

                $loop ++;
            }

            // Amazon
            if ($id_marketplace == $amazon_id_marketplace) {

                // Update History Log for Amazon
                $no_process = isset($history['process']) ? sizeof($history['process']) : 0;
                $no_error = isset($history['error']) ? sizeof($history['error']) : 0;
                $no_success = ($no_process > 0) ? $no_process - $no_error : 0;

                $log_data = array(
                    'batch_id' => $batch_id,
                    'id_shop' => $id_shop,
                    'id_country' => $site,
                    'action_type' => Amazon_Order::$Export_Orders,
                    'no_process' => $no_process,
                    'no_success' => $no_success,
                    'no_error' => $no_error,
                    'is_cron' => 1,
                    'date_upd' => $date,
                    'detail' => isset($history['error']) ? base64_encode(serialize($history['error'])) : '',
                );

                $amazon_database->update_log($log_data);

                // Hook 
                require_once('./../../libraries/Hooks.php');

                $hook = new Hooks();
                $hook->_call_hook('send_orders_cron', array(
                    'user_name' => $user_name,
                    'id_shop' => $id_shop,
                    'site' => $site,
                    'orders' => Amazon_Order::$orders
                ));
            }


            // Mirakl
            if ($id_marketplace == $mirakl_id_marketplace) {
                // Update History Log for Mirakl
                $no_process = isset($history['process']) ? sizeof($history['process']) : 0;
                $no_error = isset($history['error']) ? sizeof($history['error']) : 0;
                $no_success = ($no_process > 0) ? $no_process - $no_error : 0;

                $log_data = array(
                    'sub_marketplace' => $sub_marketplace,
                    'id_country' => $site,
                    'id_shop' => $id_shop,
                    'batch_id' => $batch_id,
                    'feed_type' => MiraklParameter::FEED_TYPE_ORDER,
                    'action_type' => MiraklParameter::ACTION_TYPE_ORDER,
                    'count_process' => $no_process,
                    'count_send' => 0,
                    'count_success' => $no_success,
                    'count_error' => $no_error,
                    'count_skipped' => 0,
                    'count_warning' => 0,
                    'is_cron' => 1,
                    'detail' => isset($history['error']) ? base64_encode(serialize($history['error'])) : '',
                    'date_add' => date('Y-m-d H:i:s'),
                    'date_upd' => date('Y-m-d H:i:s')
                );
                
                $id_log = $mirakl_database->updateMiraklLog($log_data, $log_detail);
            }

            $proc_rep->set_status_msg(l('COMPLETED'));
        } else {
            $proc_rep->set_error_msg(l('EMPTY ORDERS'));
        }

        $proc_rep->finish_task();
        echo json_encode(array('status' => 'success'));

        // mail to customer
        // get error order
        $error_orders = array();
        $error_order_list = $Orders->getOrderErrorStatus($id_shop);

        foreach ($error_order_list as $error_order) {
            // set flag send mail
            if ($Orders->updateFlagErrorEmail($error_order['id_orders'])) {
                $error_order['comment'] = l($error_order['comment']);
                $error_orders[$error_order['id_orders']] = $error_order;
                $error_orders[$error_order['id_orders']]['marketplace'] = $config->getMargetplaceById($error_order['id_marketplace'], $error_order['site']);
            }
        }

        notify_by_email($user_data, $error_orders);
    } else {
        echo json_encode(array('status' => 'error',
            'process' => l('EMPTY ORDERS'),
            'message' => 'Wrong user.'));
        die();
    }
}

function notify_by_email($user_data, $order_data) {
    if (!empty($order_data)) {
        require_once(dirname(__FILE__).'/../../application/libraries/Smarty/Smarty.class.php');
        require_once(dirname(__FILE__).'/../../application/libraries/Smarty.php');
        require_once(dirname(__FILE__).'/../../application/libraries/Swift/swift_required.php');
        require_once(dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');

        $smarty = new Smarty();
        $smarty->compile_dir = dirname(__FILE__)."/../../views/templates_c";
        $smarty->template_dir = dirname(__FILE__)."/../../views/templates";

        if (!isset($config['base_url'])) {
            include(dirname(__FILE__).'/../../application/config/config.php');
        }

        $style_img = 'border: 0 !important; display: block !important; outline: none !important; padding: 5px 5px;';
        $base_url = $config['base_url'];
        $site_name = 'Feed.biz';
        $user_email = $user_data['user_email'];
        $user_first_name = $user_data['user_first_name'];
        $user_las_name = $user_data['user_las_name'];

        $data = array(
            'base_url' => $base_url,
            'logo' => '<img src="'.$base_url.'/nblk/images/feedbiz_logob.png" alt="feed.biz" style="'.$style_img.'" />',
            'title' => l('Dear').' '.$user_first_name.' '.$user_las_name,
            'message' => l('Here is the order error list').' : ',
            'message_detail' => $order_data,
            'thanks' => l('Thanks'),
            'footer1' => l('This e-mail was sent to'),
            'footer2' => l('Please note that you must use an email to access to Feed.biz management system.'),
            'footer3' => l('All right reserved.'),
        );

        $smarty->assign($data);
        $html = $smarty->fetch(dirname(__FILE__).'/../../application/views/templates/email_template/notify_import_order_error.tpl');

        if (strlen($html) > 50) {
            $transport = Swift_SmtpTransport::newInstance();
            $mailer = Swift_Mailer::newInstance($transport);
            $swift_message = Swift_Message::newInstance();
            $swift_message->setSubject($site_name.' : '.l('Import Order Error'));
            $swift_message->setFrom(array('support@feed.biz' => $site_name));
            $swift_message->setBody($html, 'text/html');
            $swift_message->setTo(array($user_email));
            $swift_message->setBcc(
                    array('praew@common-services.com',
                        'por@common-services.com',
                        'palm@common-services.com')
            );
            $mailer->send($swift_message);
        }
    }
}
