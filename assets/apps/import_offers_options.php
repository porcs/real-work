<?php

require_once dirname(__FILE__) . '/../../application/libraries/FeedBiz.php';
require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');
ini_set("auto_detect_line_endings", "1");
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

//$argv[1] = urlencode(json_encode(array('data' =>json_encode(array('user_name'=>"u00000000000400","id_shop"=>"1","shopname"=>"Torne_Valley","id_country"=>"3","marketplace_id"=>"2")))));

if(!isset($argv[1]) || $argv[1]==''){
    die( 'No input data!');
}

$argv_data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
$datas = json_decode($argv_data);

class ImportProductOption 
{        
    public function Dispatch($user_name, $id_shop, $shopname, $id_country, $marketplace_id, $debug = false){
               
        $product_list = new MarketplaceProductOption($user_name, null, null, null, null, $id_country, $marketplace_id);
        
        /* process report conf */
        $batchID = uniqid();
        $process_title = 'Import Offers Options ';
        $process_type = 'Import offers options';
	
	if(!$debug) {
	    $proc_rep = new report_process($user_name, $batchID, $process_title, false, true);
	    $proc_rep->set_process_type($process_type);
	    $proc_rep->set_shop($id_shop, $shopname);
	}
        $upload_path = USERDATA_PATH . $user_name .'/process/';
        $file_name = 'offers_options'. ( ( isset($marketplace_id) && !empty($marketplace_id) ) ? '_'.$marketplace_id : '' ) 
			. ( ( isset($id_country) && !empty($id_country) ) ? '_'.$id_country : '' ). '.csv';                 
        
        if(!file_exists($upload_path . $file_name)){
            $error = 'File does not exits : ' .$upload_path . $file_name ;
	    if(!$debug) {
		$proc_rep->set_error_msg($error);
		$proc_rep->finish_task();
	    }
            echo json_encode(array('error' => $error,'pass' =>  false,'output' => '')); 
            exit;
        }

        $path = $upload_path . $file_name; 
	
        $file = file($path);
	
	if(!$debug) 
	    $proc_rep->set_max_min_task(count($file), 0);
	
        $datalist = $head = array();

        $delimiter = ',';
        $array_header = array();
        $array_header['id_product'] = "id_product";
        $array_header['id_product_attribute'] = "id_product_attribute";
        $array_header['SKU'] = "sku";
        $array_header['Price_Override'] = "price";
        $array_header['Disable'] = "disable";
        $array_header['Force_in_Stock'] = "force";
        $array_header['Do_not_export_price'] = "no_price_export";
        $array_header['Do_not_export_quantity'] = "no_quantity_export";
        $array_header['Gift_wrap'] = "gift_wrap";
        $array_header['Gift_message'] = "gift_message";
        $array_header['ASIN'] = "asin1";
        $array_header['FBA_Value'] = "fba_value";
        $array_header['Latency'] = "latency";
        $array_header['Shipping'] = "shipping";
        $array_header['Shipping_Type'] = "shipping_type";
        $array_header['Condition_note'] = "text";
        
        if ($path) {
            $fp = fopen($path, 'r');
            $buf = fread($fp, 1000);
            fclose($fp);
        } else {
            $error = 'Import product option fail : ' . $path;
	    
	    if(!$debug) {
		$proc_rep->set_error_msg($error);
		$proc_rep->finish_task();
	    }
	    
            echo json_encode(array('error' => $error,'pass' =>  false,'output' => '')); 
            exit;
        }
        
        $count1 = count(explode(',', $buf));
        $count2 = count(explode(';', $buf));
        if ($count2 > $count1){
            $delimiter = ';';
        }
                
        $fp = fopen($path, 'r');
        $i = 0;
        $list_sku = '' ;
	
        while ($data = fgetcsv($fp, 1000, $delimiter)) {  
	   
            if (!is_array($data)){
                $error = 'Data is incorrect';
		
		if(!$debug) {
		    $proc_rep->set_error_msg($error);
		    $proc_rep->finish_task();
		}
		
                echo json_encode(array('error' => $error,'pass' =>  false,'output' => '')); 
                exit;
            }
            
            if($i == 0){               
                foreach ($data as $khead => $vhead) {
                    if (!empty($vhead)) {
			$vhead = str_replace(" ", "_", $vhead);
                        $head[$khead] = strtolower(isset($array_header[$vhead]) ? $array_header[$vhead] : $vhead);
                    }
                }		
		
            } else {               
		
                foreach ($data as $khead => $vhead) {
                    if (!empty($vhead) && isset($head[$khead])) {
                        $datalist[$i][$head[$khead]] = trim($vhead);
                    }
                }	
		
		if(strlen($list_sku) <= 0) {
		    $list_sku .= (isset($datalist[$i]['sku']) ? "'" .$datalist[$i]['sku']."'"  : '');
		} else {
		    $list_sku .= isset($datalist[$i]['sku']) ? ", '" . $datalist[$i]['sku'] ."'" : '';
		}
		
            }
	    
            $i++;
	    
            if(!$debug) 
		$proc_rep->set_running_task($i);
	    
        } 
	
	// check product in list
	if(!empty($datalist) && !in_array('id_product', $head)){
	    
	    if(!isset($head['id_product']) && !isset($head['id_product_attribute'])){    

		$products = $product_list->getProductBySKU($id_shop, $list_sku, true);  

	    }
	    
	    foreach ($datalist as $k => $data_list) {
		
		if(!isset($data_list['sku'])) {
		    unset ($datalist[$k]); 
		    continue;
		}
		
		$product = isset($products[$data_list['sku']]) && !empty($products[$data_list['sku']]) ? $products[$data_list['sku']] : null;
		
		if(!isset($product['id_product']))  {
		    unset ($datalist[$k]); 
		    continue;
		}
		
		$datalist[$i]['id_product'] = $product['id_product'];
		$datalist[$i]['id_product_attribute'] = isset($product['id_product_attribute']) ? $product['id_product_attribute'] : null;
	    }
	}
	 
        if(!empty($datalist)){
	    
            $result = $product_list->product_options_upload($datalist, $id_shop);
        }
	
        if(!$debug){
	    $proc_rep->finish_task();
	}
	
        echo json_encode(array('error' => '', 'pass' =>  true, 'output' => isset($result) ? $result : 'Import Offers Options Success')); 
        exit;
    }
}

$debug = false;
$update = new ImportProductOption();
$user_data = json_decode($datas->data);

$id_country = isset($user_data->id_country) ? $user_data->id_country : null;
$marketplace_id = isset($user_data->marketplace_id) ? $user_data->marketplace_id : null;
$update->Dispatch($user_data->user_name, $user_data->id_shop, $user_data->shopname, $id_country, $marketplace_id, $debug);