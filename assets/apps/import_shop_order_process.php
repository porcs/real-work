<?php
	define('BASEPATH', '');
    if(!isset($argv[1]) || $argv[1]==''){
        die( 'No input data!');
    }
    $input = json_decode(rawurldecode(($argv[1])),true);
    $input = json_decode($input['data'], true);
    
    if(!isset($input['xml_url']) || empty($input['xml_url']) || !isset($input['user_name']) || empty($input['user_name'])){
        die('Data input is broken');
    }
    $user_name = $input['user_name'];
    $id_user = $input['user_id'];
    include (dirname(__FILE__). "/../../application/config/config.php");
    require_once DIR_SERVER . '/application/libraries/FeedBiz_Orders.php';
    require_once DIR_SERVER . '/application/libraries/Ebay/ObjectBiz.php';
    require_once DIR_SERVER . '/application/libraries/Ebay/ebayshipment.php';        
    require_once DIR_SERVER . '/application/libraries/Amazon/functions/amazon.status.orders.php';
    include_once (dirname(__FILE__). "/../../libraries/feedbiz_override_tools.php");

    $ebayObjectBiz = new ObjectBiz(array($user_name));
    $feedBizOrder = new FeedBiz_Orders(array($user_name));
    $ebayCarriers = $ebayObjectBiz->getAllCarriers();
    //INITIAL END

    //retrieve setting + connector
    $orders = array();
    $order_ids = array();
    
//    $xml_string = file_get_contents($input['xml_url']); 
    if(function_exists('fbiz_get_contents')){
        $xml_string = fbiz_get_contents($input['xml_url']); 
    }else{
        $xml_string = file_get_contents($input['xml_url']); 
    }
                // validate xml
    libxml_use_internal_errors(true);
    $xml_doc = simplexml_load_string($xml_string);

    // load dom
    if ($xml_doc) {
        $xml_dom = new SimpleXMLElement($xml_string);
        foreach ($xml_dom->Orders->Order as $orderDOM) {
            $orders [strval($orderDOM->MPOrderID)] = array(
                'MPOrderID' => strval($orderDOM->MPOrderID),
                'CarrierID' => strval($orderDOM->CarrierID),
                'CarrierName' => strval($orderDOM->CarrierName),
                'ShippingNumber' => strval($orderDOM->ShippingNumber),
                'ShippingDate' => strval($orderDOM->ShippingDate)
            );
            $order_ids [] = strval($orderDOM->MPOrderID);
        }                    

        if (!empty($orders)) {               
            $ordersNoTracking = $feedBizOrder->getOrdersNoTracking($user_name, $order_ids);

            // SQLITE
            $feedBizOrder->updateOrderTracking($user_name, $orders);
            // LOG
            $current = new DateTime();
            $logs = array();
            foreach($ordersNoTracking as $oid=>$orderNoTracking){
            		$sale_channel = strtolower($orderNoTracking['sales_channel']);
            		$id_marketplace = 0;
            		if(strpos($sale_channel, 'ebay') !== FALSE)
            			$id_marketplace = 3;
            		if(strpos($sale_channel, 'amazon') !== FALSE)
            			$id_marketplace = 4;
            		
                    $logs[] = array('batch_id'=>$orderNoTracking['id_marketplace_order_ref'],
                                                    'request_id'=>$orderNoTracking['id_orders'],
                                                    'error_code'=>0,
                                                    'message'=>'Update tracking number: '.$orders[$oid]['ShippingNumber'].' Shipped Date: '.$orders[$oid]['ShippingDate'],
                                                    'date_add'=>$current->format('Y-m-d H:i:s'),
                    								'id_shop'=>$orderNoTracking['id_shop'],
                    								'id_marketplace'=>$id_marketplace,
                    								'site'=>$orderNoTracking['site']
                    );
            }
            $feedBizOrder->insertLog($user_name, $logs);
            // LOG END
            //SQLITE END
        }
    }
    
    
    
       //MP
        $current = new DateTime();
        $raworders = $feedBizOrder->getOrdersForShip($user_name);
        $logs = array();
        $done = array();
        $orderMPMapping = array();
        $shipments = array();
        foreach ($raworders as $channel=>$sites) {
        	//EBAY
        	if (strpos(strtolower($channel), 'ebay') !== FALSE) {
        		foreach($sites as $site_id => $site_orders){
        			foreach($site_orders as $order){
        				$id_shop = $order['id_shop'];
        				$id_carrier = $order['id_carrier'];
        				$shipments[] = array('OrderID' => $order['id_marketplace_order_ref'],
        				// 	                        					'OrderLineItemID' => $order['id_marketplace_order_ref'],
        						'ShipmentTrackingNumber' => $order['tracking_number'],
        						'ShippedTime' => $order['shipping_date'],
        						'ShippingCarrierUsed' => isset($ebayCarriers[$id_shop]) && isset($ebayCarriers[$id_shop][$id_carrier]) ? $ebayCarriers[$id_shop][$id_carrier]['name'] : 'Feedbiz'
        				);
        				$orderMPMapping[$order['id_marketplace_order_ref']] = $order;
        			}
        			 
        			//API
        			if($shipments){
	        			$ebayshipment = new ebayshipment(array($user_name, $id_user, $site_id));
	        			$ebayshipment->export_shipment($shipments);
	        			$ebayshipment->export_bulk();
	        			$rs = $ebayshipment->export_download();
	        			 
	        			//Response
	        			if($rs){
	        				foreach($rs as $orderResponse){
	        					$Ack = strval($orderResponse->Ack);
	        					$OrderID = strval($orderResponse->OrderID);
	        					$raworder = isset($orderMPMapping[$OrderID]) ? $orderMPMapping[$OrderID] : null;
	        					$id_shop = $raworder['id_shop'];
	        					$id_carrier = $raworder['id_carrier'];
	        					$carrier_name = isset($ebayCarriers[$id_shop]) && isset($ebayCarriers[$id_shop][$id_carrier]) ? $ebayCarriers[$id_shop][$id_carrier]['name'] : 'Feedbiz';
	        					
	        					$sale_channel = strtolower($raworder['sales_channel']);
	        					$id_marketplace = 0;
	        					if(strpos($sale_channel, 'ebay') !== FALSE)
	        						$id_marketplace = 3;
	        					if(strpos($sale_channel, 'amazon') !== FALSE)
	        						$id_marketplace = 4;
	        						
	        					if($raworder){
	        						if(strtolower($Ack) == 'success'){
	        							$logs[] = array('batch_id'=>$OrderID,
	        									'request_id'=>intval($orderMPMapping[$OrderID]['id_orders']),
	        									'error_code'=>0,
	        									'message'=>'Ebay SetShipmentTrackingInfo: '.$raworder['tracking_number'].' Shipped Date: '.$raworder['shipping_date'],
	        									'date_add'=>$current->format('Y-m-d H:i:s'),
                    							'id_shop'=>$raworder['id_shop'],
                    							'id_marketplace'=>$id_marketplace,
                    							'site'=>$raworder['site']
	        							);
	        							$done[] = intval($orderMPMapping[$OrderID]['id_orders']);
	        						}
	        						else{
	        							$error_messages = array();
	        							foreach($orderResponse->Errors as $errDOM)
	        							{
	        								$error_messages[] = strval($errDOM->ErrorCode).': '.strval($errDOM->LongMessage);
	        							}
	        							$logs[] = array('batch_id'=>$OrderID,
	        									'request_id'=>intval($orderMPMapping[$OrderID]['id_orders']),
	        									'error_code'=>1,
	        									'message'=>'Ebay SetShipmentTrackingInfo: '.implode('<br/>', $error_messages),
	        									'date_add'=>$current->format('Y-m-d H:i:s'),
                    							'id_shop'=>$raworder['id_shop'],
                    							'id_marketplace'=>$id_marketplace,
                    							'site'=>$raworder['site']
	        							);
	        						}
	        					}
	        				}
	        			}
        			}
        		}
        	}
        }
        
        //SQLITE
        $feedBizOrder->insertLog($user_name, $logs);
        $feedBizOrder->updateShippedStatus($user_name, $done);
        //SQLITE END
        //MP END
    echo json_encode(array('result'=>'success'));
    