<?php

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

class AmazonOrderAcknowledge {

    const CANCELLED = 1;

    public function __construct( $user ) {
	$this->user = $user ;
    }

    public function Dispatch($action, $id_order, $id_marketplace, $id_shop, $site){

	switch ($action) {
	    case AmazonOrderAcknowledge::CANCELLED :                
		$file = dirname(__FILE__) . "/amazon_order_acknowledge_cancelled.php";
                $url = "php {$file} {$this->user} {$id_order} {$id_marketplace} {$id_shop} {$site}";
                exec('bash -c "exec nohup setsid '.$url.' > /dev/null 2>&1 &  "');                
                file_put_contents(dirname(__FILE__) .'/users/'.$this->user.'/amazon/AmazonOrderAcknowledge_cancelled.txt', print_r(array('url'=>$url), true), FILE_APPEND);
		break;
	}

    }

}