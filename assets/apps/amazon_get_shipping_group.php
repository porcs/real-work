<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.reports.php';

$debug = false;

$data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
$datas = json_decode($data);
$users = json_decode($datas->data);


$amazon_reports = new AmazonReports($users, $debug);
$amazon_reports->getShippingGroups();