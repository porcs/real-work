<?php
    header('Content-Type: text/html; charset=utf-8');
    require_once(dirname(__FILE__) . '/FeedbizImport/config/offers.php');

    $error  = null ;
    $output = null;
    
//    $time = time();
//    $argv[1] = json_encode(array("username" => "Test"));

    $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $userdata = json_decode($data);
    $userdata = json_decode($userdata->data);
    
    if(!isset($userdata->username) || empty($userdata->username))
    {
        echo json_encode( array('error' => 'Username wrong') ) ; 
        exit;
    } 
    
    $user = str_replace(" ", "_", $userdata->username);
    
    //////Offers//////
    $offers_database = ObjectModel::createDatabase($user);

    if(isset($offers_database['error']))
    {
       $error = $offers_database['error'];
    }   
    else 
    {
        $offers_table_name = array(
            'Product',
            'ProductAttribute',
            'ProductSale',    
            'Category', 
            'Carrier', 
            'Conditions', 
            'Currency',
            'Manufacturer',
            'Profile', 
            'Price',
            'Rule',
            'Supplier',
            'Tax',
            'OTP',
            'Shop',
            'Log',
        ); 
        foreach ($offers_table_name as $offer_table_name)
            if(method_exists($offer_table_name, 'createTable'))
            {
                $offer_class = new $offer_table_name($user);
                $result = $offer_class->createTable();
                
                if(strlen($result))
                   $error .= $result;
            }
            else
            {
                $error .= sprintf('Table %s - method doesn\'t exists. ', $table_name) ;
            }   

       //create category selected table
        $category = new Category($user);
        if(!$category->createCategorySelectedTable())
            $error .= sprintf('Table category_selected - can not create') ;
        
        //Create Rule_item
        $rules = new Rule($user);
        if(!$rules->createTableItems())
            $error .= sprintf('Table Rule Item - can not create') ;
        
        //Create Profile_item
        $profiles = new Profile($user);
        if(!$profiles->createTableItems())
            $error .= sprintf('Table Profile Item - can not create') ;
        
        $log = new Log($user);
        if(!$log->createTableFlag())
            $error .= sprintf('Table Log Item - can not create') ;
        
        //create token
        $token = new OTP($user);
        if(!$token->generate_token())
            $error .= sprintf('Token - can not generate') ;
        
        // stock 
        $stock = new StockMovement($user);
        if(!$stock->createTableStock())
            $error .= sprintf('Table Stock - can not create') ;
        if(!$stock->createTableStockMovement())
            $error .= sprintf('Table Stock Movement - can not create') ;
    }
    
    if(isset($error) || !empty($error) || $error == null)
        $output = sprintf('User %s successfully created offers', $user) ;

    $json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $output) ) ;

    echo $json ;