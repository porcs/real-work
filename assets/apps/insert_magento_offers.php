<?php
//header("Content-type: text/xml");
require_once(dirname(__FILE__) . '/FeedbizImport/config/offers.php');
 
$error  = null ;
$output = null ;
$shopname = null;
$id_shop = null;
$shop_type = 'OpenCart';  
$feed_type = 'Offer';
$process_title =  'Importing your offers';
$process_type =  'import offer';
    $arr_setting = array(
//        'Attributes'      => 'Attribute',
        'Carriers'        => 'Carrier',
        'Categories'      => 'Category',
        'Conditions'      => 'Conditions',
        'Currencies'      => 'Currency',
//        'Features'        => 'Feature',
//        'Language'        => 'Language',
        'Manufacturers'   => 'Manufacturer',
        'Suppliers'       => 'Supplier',
        'Taxes'           => 'Tax',
//        'Units'           => 'Unit',
        //'Products'        => 'Product',
    );
    $arr_content = array(
        'Offers'        => 'Product',
    );
//$argv[1] = json_encode(
//    array(
//        'data' => array(
//            'username' => "100007406051767",
//            'token' => 'b4f43307f0f88694e0eb0df6acf558a0',
//            'gdata' => array(
//                'fileurl' => "http://192.168.1.10/opencart1564/index.php",
//                'ws_token'   => "465683344",
//        )
//    )
//));

$data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($argv[1])),null,'UTF-8');
$datas = json_decode($data); 
//$userdata = json_decode($datas->data);
$userdata = $datas;
$user = str_replace(" ", "_", $userdata->username);
$time = date("Y-m-d H:i:s");  

if( isset($user) && !empty($user) )
{
//    $otp = new OTP($user);
//    $otp_result = $otp->check_token($userdata->token);
//
//    if( !$otp_result && $userdata->token !='')
//    {
//        $error = sprintf('token_wrong') ;
//        $output = $userdata;
//    }
//    else
//    {
        if( !empty($userdata->gdata->fileurl))
        {
            $Convert = new ConvertXMLToFeedBizXML($user); 
            
            //get setting xml
            $xml = $Convert->getSetting($userdata->gdata->setting_fileurl,$user);
            if( !isset($xml) || empty($xml) || (isset($xml['error']) && $xml['error']==true))
            {
                if($xml['error']){$error =  $xml['message'];}
                else{ $error = sprintf('can_not_load_setting_data', $user) ;}
            }else{
                
                $arr_class_name = $arr_setting;
                
                $oXml = simplexml_load_string($xml['xml']);

                if(!isset($oXml) || empty($oXml) || !$oXml)
                    $error .=  sprintf('Data feed wrong.') ;
                
                if(isset($oXml->Status->Code) && $oXml->Status->Code==0 ){
                    $error .=  sprintf('Access Denied.') ;
                }
                
                if (isset($oXml->Status->Code) && $oXml->Status->Code == 01 && !empty($oXml->Status->Message)) {
                    $error = sprintf('11-00003');
                }
                
                //shop name
                $shopname = $oXml['ShopName'];
                if($error === null){
                    if( isset($shopname) && !empty($shopname))
                    {
                        $shop = new Shop($user);
                        $id_shop = $shop->importData($shopname, $time, $userdata->gdata->fileurl);
                        $table_list = array();
                                
                        foreach ($arr_class_name as $xml_key => $class_name)
                        {
                            if(method_exists($class_name, 'importData'))
                            {
                                $class = new $class_name($user);

                                if(isset($oXml->$xml_key))
                                {
                                    $oXml->$xml_key->id_shop = $id_shop;
                                    $oXml->$xml_key->shopname = $shopname;
                                    $oXml->$xml_key->source = $shop_type;

                                    $result = $class->import($oXml->$xml_key, $time);

                                    if(isset($result['error']))
                                        $error .= $result['error'] ;  
                                    
                                    //List table for delete product not use
                                    if(isset($result['table_list']))
                                        $table_list[] = $result['table_list'];
                                }
                                else
                                    $error .= sprintf('No data export %s. ', $class_name) ;
                            }
                            else
                            {
                                $error .= sprintf('Data table %s - Cannot be imported. ', $class_name) ;
                            }   
                        }

                        if(!strlen($error))
                            $output = sprintf('Import successful. Time : ' . $time) ;
                    }
                    else 
                    {
                        $error = 'Incorrect Shop Name.';
                    }
                }

            }
            if($error !== null || $shopname === null || $id_shop === null){
                $json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $output, 'shopname' => (string)$shopname, 'id_shop' => (int)$id_shop) ) ;
                echo $json ;exit;
            }
            //import log for global var
            $import_log = new ImportLog($user);
            $no_total = 0;
            $no_success = 0;
            $no_error = 0;
            $no_warning = 0;
            $batchID = uniqid();
            $source=null;
            //get product xml
            $status = null;
            $loop_at=0;
             /* process report conf */ 
            $proc_rep = new report_process($user,$batchID,$process_title,false,true);
            $proc_rep->set_process_type($process_type); 
            $proc_rep->set_shop($id_shop,$shopname);
            /* process report conf end */
            $no_current_id = 0;
            do{
                
                $xml = $Convert->getSingleContent($userdata->gdata->fileurl,$user);
                //print_r($xml); 
                   //echo rawurlencode( print_r( $xml,true));
                if( !isset($xml) || empty($xml) || (isset($xml['error']) && $xml['error']==true))
                {
                    if($xml['error']){$error =  $xml['message'];}
                    else{ $error = sprintf('can_not_load_data', $user) ;}
                    $status=0;
                }
                else
                {
                    if($no_current_id == $xml['current'] &&  $xml['total'] != 0  ){
                        $error = sprintf('duplicate_load_data', $user) ;
                        break;
                    }else{
                        $no_current_id = $xml['current'];
                    }
                    $status=$xml['status'];
                    $arr_class_name = $arr_content;

                    $oXml = simplexml_load_string($xml['xml']);

                    if(!isset($oXml) || empty($oXml) || !$oXml)
                        $error .=  sprintf('Data feed wrong.') ;

                    if(isset($shopname) && !empty($shopname))
                    {
                        foreach ($arr_class_name as $xml_key => $class_name)
                        {
                            if(method_exists($class_name, 'importData'))
                            {
                                $class = new $class_name($user);

                                if(isset($oXml->$xml_key))
                                {
                                    $oXml->$xml_key->id_shop = $id_shop;
                                    $oXml->$xml_key->shopname = $shopname;
                                    $oXml->$xml_key->source = $shop_type;

                                    $result = $class->import($oXml->$xml_key, $time);

                                    if(isset($result['error']))
                                        $error .= $result['error'] ;  
                                    
                                    //List table for delete product not use
                                    if(isset($result['table_list']))
                                        $table_list[] = $result['table_list'];
                                }
                                else
                                    $error .= sprintf('No data export %s. ', $class_name) ;
                            }
                            else
                            {
                                $error .= sprintf('Data table %s - Cannot be imported. ', $class_name) ;
                            }   
                        }

                        if(!strlen($error))
                            $output = sprintf('Import successful. Time : ' . $time) ;
                    }
                    else 
                    {
                        $error = 'Incorrect Shop Name.';
                    }
                    
                    /* process report conf */
                    if($loop_at==0){$proc_rep->set_max_min_task($xml['max'],$xml['min']);}
                    $proc_rep->set_running_task($xml['current']);
                    /* process report conf */
                }
                
                $loop_at++;
              
            }while($status == -1);
            
//            if($loop_at>0){
                        $data= array(
                            'batch_id'      => $batchID,  
                            'shop'          => ObjectModel::escape_str($shopname),  
                            'source'        => ObjectModel::escape_str($source), 
                            'type'          => $feed_type, 
                            'no_total'      => (int)$no_total,  
                            'no_success'    => (int)$no_success,  
                            'no_warning'    => (int)$no_warning, 
                            'no_error'      => (int)$no_error, 
                            'datetime'      => $time, 
                        );

                        $import_log->insert_log($data);
                        /* process report conf */
                        if($error!=''){
                            $proc_rep->set_error_msg($error);
                        }else{
                            $proc_rep->finish_task();
                        }
                        /* process report conf */
//            }
            
            ////Delete product not use
            if(isset($table_list) && !empty($table_list) && isset($id_shop) && !empty($id_shop))
            {
                $product_list = new Product($user);
                $product_list->truncate_products($table_list, $time, $id_shop);
            }
        }
        else 
        {
            $error = 'Incorrect Urls.';
        }
//    }
}
else
{
    $error = 'username_wrong';
}
//print_r($table_list);
$json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $output, 'shopname' => (string)$shopname, 'id_shop' => (int)$id_shop) ) ;
echo $json ;
