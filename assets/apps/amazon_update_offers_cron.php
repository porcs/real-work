<?php 

define('BASEPATH', '');
chdir(dirname(__FILE__)); 

require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.feeds.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]=='' 
    || !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
    || !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]==''){
    die( 'No input data!');
}

// Get argv from url
$info = array(
    'user_id' => $argv[1],
    'user_name' => $argv[2],
    'ext' => $argv[3],
    'id_shop' => $argv[4],
    'shop_name' => str_replace('_',' ',$argv[5]),
    'id_marketplace' => $argv[6],
    'next_time' => 20,
    'owner' => 'system',
    'action' => 'synchronize_offer',
);

// Get userdata fromdatabase
$data = AmazonUserData::get($info);

if(!isset($data) || empty($data) ){
    
    $info['countries'] = $info['ext'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    $info['countries'] = $data['countries']; 
    $data['mode'] = AmazonDatabase::SYNC;
    $data['update_type'] = 'offer';
    $object = json_decode(json_encode($data), FALSE);

    // Synchronize Amazon
    $object->creation = false;
    $amazon_feeds = new AmazonFeeds($object, AmazonDatabase::SYNC, false, true);
    
    // Check status
    $status = $amazon_feeds->amazonDatabase->get_amazon_status($data['id_country'], $data['id_shop'], 'recreate', true );
    
    if (isset($status) && count($status) > 0 && !empty($status)) {
        
        $info['action'] = 'recreate';
        
        // Set history log
        AmazonHistoryLog::set($info);
        
        $user_id = $info['user_id']; 
        $user_name = $info['user_name']; 
        $ext = $info['ext'];  
        $shop_id = $info['id_shop']; 
        $shop_name = $info['shop_name']; 
        $market_id = $info['id_marketplace']; 
        
        # Delete recreate product
        $delete_url = "php amazon_delete_products_cron.php {$user_id} {$user_name} {$ext} {$shop_id} {$shop_name} {$market_id}"; 
        echo $delete_url."\n"; 
        exec($delete_url);
        
        # Create product
        $create_url = "php amazon_create_cron.php {$user_id} {$user_name} {$ext} {$shop_id} {$shop_name} {$market_id}";
        echo $create_url."\n"; 
        exec($create_url);
        
    } else {
        
        // Set history log
        AmazonHistoryLog::set($info);
        
        $amazon_feeds->FeedsData();
    }
}