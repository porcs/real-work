<?php
if(!defined('BASEPATH')){
    define('BASEPATH', '');
}
chdir(dirname(__FILE__));
require_once  (dirname(__FILE__).'/../../libraries/ci_db_connect.php');
require_once  (dirname(__FILE__). "/../../application/libraries/Ebay/ebayexport.php");
require_once  (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
require_once  (dirname(__FILE__).'/../../application/libraries/Smarty/Smarty.class.php');
require_once  (dirname(__FILE__).'/../../application/libraries/Smarty.php');
if ( file_exists(dirname(__FILE__).'/../../application/libraries/Amazon/functions/amazon.message.summary.php')) {
        require_once  (dirname(__FILE__).'/../../application/libraries/Amazon/functions/amazon.message.summary.php');
}
require_once  (dirname(__FILE__).'/../../application/libraries/FeedBiz.php');

class export_monitoring {
    
        protected $allCronUserSites = array();
	protected $siteDictionary = array();
	protected $uinfo = null;
	protected $users = null;
	protected $user_name = null;
	protected $id_customer = null;
	protected $id_site = null;
	protected $user_email = null;
	protected $first_name = null;
	protected $last_name = null;
	protected $debug = false;
        protected $read_user = array();
        protected $base_url = DIR_DOMAIN;
        protected $dir_server = DIR_SERVER;
        protected $site_config = null;
        protected $site_name = 'Feed.biz';
	
	function __construct(){
                
                $this->ci_db_connect = new ci_db_connect ();		
		
                $this->getCustomerSite();
		
		$this->allCronUserSites = $this->getAllCronUserSites();
		$this->uinfo = new UserConfiguration();
		$this->users = $this->uinfo->getAllUser();
		$this->siteDictionary = $this->uinfo->getEbaySiteDictionary();
		$this->debug = isset($_GET['debug']) && $_GET['debug'] == 1; 
		
                $this->_smarty                      = new Smarty();
                $this->_smarty->compile_dir         = dirname(__FILE__) . "/../../views/templates_c";
                $this->_smarty->template_dir        = dirname(__FILE__) . "/../../views/templates";
		include (dirname(__FILE__). "/../../application/config/config.php");
                $this->site_config = $config; 
                unset($config);
                if(isset($this->site_config['site_name'])){
                    $this->site_name = $this->site_config['site_name'];
                }
	}
        
	public function getUserReports(){	    
	    return $this->uinfo->getAllUsers();
	}

	public function getCustomerSite(){
		$sql	= "SELECT id_customer, id_site FROM ebay_configuration Where `name` = 'EBAY_SITE_ID'";
                $query	= $this->ci_db_connect->select_query($sql);
		
                while ( list($id_customer, $id_site) = $this->ci_db_connect->fetch($query) ) :	 
		    $this->customer_site[$id_customer][]    = $id_site;
                endwhile;
	}
		
	public function geteBayMessage() {
                if ( !isset($this->user_name) && empty($this->user_name) || !isset($this->id_customer) && empty($this->id_customer) || !isset($this->site) && empty($this->site) ) {
                        return null;
                }
                $result = array();
                
                $param = array($this->user_name, $this->id_customer, $this->id_site);
                $ebayexport = new ebayexport($param);
                $result = $ebayexport->products_monitoring($this->user_name);
                $this->read_user[$this->user_name][$this->marketplace] = 1;
                return $result;
        }
        
        public function getAmazonMessage($date) {             
                if (!isset($this->user_name) && empty($this->user_name)) {
                        return null;
                }
                $result = array();
                if ( file_exists(dirname(__FILE__).'/../../application/libraries/Amazon/functions/amazon.message.summary.php') ) {
                        $amazon = new AmazonMessageSummary($this->user_name, $this->debug);
			
                        // Summary Feed
                        $SummaryFeed = $amazon->getSummaryFeed($date);
                        if( !empty($SummaryFeed) && ($SummaryFeed != false))
                            $result['SummaryFeed'] = $SummaryFeed;
			
                        // Summary Error Resolution
                        $SummaryValidate = $amazon->getSummaryValidate($date);
                        if( !empty($SummaryValidate))
                            $result['SummaryValidate'] = $SummaryValidate;
			
                        $this->read_user[$this->user_name][$this->marketplace] = 1;
                }
		
                if(!empty($result)) return $result; 
                else return false; 
        }
        
        function sendMessage($html = null) {
                if ( !isset($html) && empty($html) ) {
                        return null;
                }
                $first   = $this->_smarty->fetch(dirname(__FILE__) . '/../../application/views/templates/email/english/export_monitor_header.tpl');
                if($this->debug){
			echo $first.$html;
		}
		else{
			$transport = Swift_SmtpTransport::newInstance();
			$mailer = Swift_Mailer::newInstance($transport);
			$swift_message = Swift_Message::newInstance();
			$swift_message->setSubject('Feed.biz : Ebay Product Upload Monitoring');
			$swift_message->setFrom(array('support@feed.biz' => 'feed.biz'));
			$swift_message->setBody($first.$html, 'text/html');
			$swift_message->setTo(array('tok@common-services.com', 'por@common-services.com', 'olivier@common-services.com' ));
			$mailer->send($swift_message);
		}
        }
	
	function getAllCronUserSites(){
		$matches = array();
		$cron_contents = file_get_contents(dirname(__FILE__)."/users/cron/user_crontab.cron", "r");
		$cron_lines = explode("\n", $cron_contents);
		$ebay_export_product = 'ebay_export_product_cron.php';
		foreach($cron_lines as $cron_line){
			//Find Ebay Export Product
			if(strpos($cron_line, '#') === 0 ||
					strpos($cron_line, $ebay_export_product) < 1){
				continue;
			}
			//String processing
			$cron_line = substr($cron_line, strpos($cron_line, $ebay_export_product) + strlen($ebay_export_product));
			$cron_line = trim($cron_line);
			$cron_line_elements = explode(' ', $cron_line);
			$matches[$cron_line_elements[0]][$cron_line_elements[2]] = 1;
		}
		return $matches;
	}
        
	function generateEbayTemplates($result = null, $sum = null, $message = null,$language='english') {
                if ( !isset($result) && empty($result) || !isset($sum) && empty($sum) || !isset($message) && empty($message) ) {
                        return '';
                }
                $html = '';
                if (!empty($this->base_url))
                        $base_url = isset($this->base_url) && !empty($this->base_url) ? $this->base_url : $this->dir_server;
                elseif (!empty($this->dir_server))
                        $base_url = $this->dir_server;

                $logo                   = '<img src="'.$base_url.'/nblk/images/feedbiz_logob.png" alt="feed.biz" style="border: 0 !important; display: block !important; outline: none !important; padding: 5px 5px;" />';
                $ebaylogo               = '<img height=22 src="'.$base_url.'/assets/images/EBay_logo.svg-Custom.png" alt="eBay" style="margin-left: 10px" >';
                $this->dir_name         = USERDATA_PATH.$this->user_name."/ebay/product";

                $feed = array();
                $feed['country']        = $this->siteDictionary[$this->id_site]['name'];
                $feed['market_place']   = "eBay";

                $assign                 = array(
                        'logo'          => $logo, 
                        'shopname'      => $this->shop_name, 
                        'ebaylogo'      => $ebaylogo, 
                        'fname'         => $this->first_name, 
                        'lname'         => $this->last_name,
                        'feed'          => $feed,
                        'date'          => date('Y-m-d', strtotime('-1 days')),
                        'results'       => $result,
                        'sum'           => $sum,
                        'message'           => $message,
                );
                $this->_smarty->assign($assign);
            
		$html  = $this->_smarty->fetch(dirname(__FILE__) . '/../../application/views/templates/email/english/export_monitor.tpl');
		return $html;
	}
        
	function generateAmazonTemplates($marketplace, $result, $date = null,$language='english') {
                         
                if (!empty($this->base_url))
                        $base_url = isset($this->base_url) && !empty($this->base_url) ? $this->base_url : $this->dir_server;
                elseif (!empty($this->dir_server))
                        $base_url = $this->dir_server;
                
                $marketplacelogo = '';
                $feed = array();
                //$feed['country']        = $this->siteDictionary[$this->id_site]['name'];
                $feed['market_place']   = $marketplace;
                $feed['shopname']   = $marketplace;
                $style_img = 'border: 0 !important; display: block !important; outline: none !important; padding: 5px 5px;';
                
                switch ($marketplace){
                        case 'Amazon': $marketplacelogo = 'Amazon'; break;
                }
                
                $assign = array(
                    'logo'                  => '<img src="'.$base_url.'/nblk/images/feedbiz_logob.png" alt="feed.biz" style="'.$style_img.'" />', 
                    'marketplace_logo'      => '<img src="'.$base_url.'/nblk/images/logo-amazon.gif" alt="Amazon" />',
                    'shopname'              => $this->shop_name, 
                    'id_marketplace'        => $this->id_marketplace, 
                    'marketplace'           => $this->marketplace, 
                    'date'                  => isset($date) ? $date : date('Y-m-d',strtotime("-1 days")), 
                    'fname'                 => $this->first_name, 
                    'lname'                 => $this->last_name,
                    'feed'                  => $feed,
                    'results'               => $result,
                    'site'                  => $this->site,
                );
                
                $this->_smarty->assign($assign);
		$html  = $this->_smarty->fetch(dirname(__FILE__) . '/../../application/views/templates/email/english/export_monitor_body.tpl');
		return $html;
	}
}
