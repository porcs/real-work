<?php

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;
if($debug) {
    $info = array(
        'user_id'        => $_GET['user_id'] ? $_GET['user_id'] : 342,
        'user_name'      => $_GET['user_name'] ? $_GET['user_name'] : 'u00000000000342',
        'site_id'        => $_GET['site_id'] ? $_GET['site_id'] : 71,
        'site_domain'    => $_GET['site_domain'] ? $_GET['site_domain'] : 'ebay.fr',
        'id_shop'        => $_GET['id_shop'] ? $_GET['id_shop'] : 1,
        'shop_name'      => $_GET['shop_name'] ? $_GET['shop_name'] : 'Eclairage Design',
        'id_marketplace' => $_GET['id_marketplace'] ? $_GET['id_marketplace'] : 3,
    );
} else {
    if(  !isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])
      || !isset($argv[2]) || $argv[2]==''
      || !isset($argv[3]) || $argv[3]=='' || !is_numeric($argv[3])
      || !isset($argv[4]) || $argv[4]==''
      || !isset($argv[5]) || $argv[5]==''
      || !isset($argv[6]) || $argv[6]==''){
        die( 'No input data!');
    }
}

$user_id = $debug ? $info['user_id'] : $argv[1];
$user_name = $debug ? $info['user_name'] : $argv[2];
$site_id = $debug ? $info['site_id'] : $argv[3];
$site_domain = $debug ? $info['site_domain'] : $argv[4];
$shop_name = $debug ? $info['shop_name'] : str_replace('_', ' ', $argv[5]);
$id_marketplace = $debug ? $info['id_marketplace'] : $argv[6];
$next_time = 60;
$ext = str_replace('ebay','', $site_domain);