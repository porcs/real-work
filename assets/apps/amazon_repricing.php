<?php 

define('BASEPATH', '');
chdir(dirname(__FILE__)); 

require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.repricing.automaton.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

if(!isset($argv[1]) || $argv[1]==''){
    die( 'No input data!');
}

$data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');

$datas = json_decode($data);
$info = json_decode($datas->data, true);

$Shop = AmazonUserData::getShopInfo($info['id_customer']);
$mode = isset($info['mode']) ? $info['mode'] : null;

$info['user_id'] = $info['id_customer'];
$info['shop_name'] = isset($Shop['name']) ? str_replace('_',' ',$Shop['name']) : $info['id_shop'];
$info['next_time'] = 60;
$info['owner'] = isset($info['user_name']) ? $info['user_name'] : $info['id_customer'];
$info['action'] = 'repricing_' . $mode;

// Set history log
 AmazonHistoryLog::set($info);   
    
if(!isset($info) || empty($info)){
        
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else { 
    
    $debug = false;
    $cron = false;   
    
    $object = json_decode(json_encode($info));
    
    $amazon_repricing = new AmazonRepricingAutomaton($object, $debug, $cron);
    $amazon_repricing->Dispatch($mode);
}
