<?php

/* Update every day */

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.validValues.update.php';

class AmazonDownloadFlatFile extends UpdateValidValues
{
    public function __construct() {
        parent::__construct();
    }
    
    public function Dispatch(){ 
        
        $error = '';
        $output = array();
        
        if(!$output['DownloadFlatFile'] =  $this->DownloadFlatFile()){
            $error .= 'Download Flat File Error';
        }
        
        echo json_encode(
            array(
                'error' => $error,
                'output' => $output,
                'pass' => (strlen($error) > 0) ? true : false,
            )
        );

        exit;
    }            
}

$update_validvalue = new AmazonDownloadFlatFile();
$update_validvalue->Dispatch();