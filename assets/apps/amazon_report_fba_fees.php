<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.reports.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

//https://qa.feed.biz/assets/apps/amazon_report_fba_fees.php?debug=1&id_customer=26&user_name=u00000000000026&ext=.es&id_shop=1&shop_name=FOXCHIP

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;
   
if($debug) {
   
    $info = array(
	'user_id' => $_GET['id_customer'],
	'user_name' => $_GET['user_name'],
	'ext' => $_GET['ext'],
	'id_shop' => $_GET['id_shop'],
	'shop_name' => $_GET['shop_name'],
	'next_time' => 1440,
	'owner' => 'system',
	'action' => 'fba_report',
    );
    
} else {
    
    $argv_1 = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $infos = json_decode($argv_1);

    $user_info = json_decode($infos->data, true);

    // Get userdata fromdatabase
    $info = array(
	'user_id' => $user_info['id_customer'],
	'user_name' => $user_info['user_name'],
	'ext' => $user_info['ext'],
	'id_shop' => $user_info['id_shop'],
	'shop_name' => $user_info['shop_name'],
	'next_time' => 1440,
	'owner' => 'system',
	'action' => 'fba_report',
    );
    
}

// Get userdata fromdatabase
$data = AmazonUserData::get($info, false, true);

if(!isset($data) || empty($data) ){
    
    $info['countries'] = $info['ext'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    // skip un - master platform 
    /*if(!isset($data['fba_master_platform']) || empty($data['fba_master_platform']) || !$data['fba_master_platform']){

	$json = json_encode( array('error' => false, 'pass' => true, 'output' => 'we will run fba report for master plateform only. ' . $data['countries'] . ' is not Master Platform') ) ;
	echo $json ; exit;

    }*/
    
    $data['shop_name'] = $info['shop_name'];
    $info['countries'] = $data['countries'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $users = json_decode(json_encode($data), false);
    
    if(!isset($user_info['datefrom']) || !isset($user_info['dateto'])){
	
	$datefrom = date('d-m-Y',strtotime('-3 day'));
	$dateto = date('d-m-Y');
	
    } else {
	
	$datefrom = $user_info['datefrom'];
	$dateto = $user_info['dateto'];
	
    }
    
    $amazon_reports = new AmazonReports($users, $debug);
    $amazon_reports->getReportFulfillmentFees($datefrom, $dateto);
    
}