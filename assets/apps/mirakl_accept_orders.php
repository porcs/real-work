<?php

require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.scheme.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.history.log.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/functions/mirakl.orders.accept.php';

$debug = false;

if (!$debug) {
    $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($argv[1])), null, 'UTF-8');
    $datas = json_decode($data);
    $users = json_decode($datas->data);
} else {
    $users = new stdClass;
    $users->id_user = '56';
    $users->id_shop = '1';
    $users->sub_marketplace = '13';
    $users->id_country = '10';
    $users->order_id = '';
}

if (!isset($users) || empty($users)) {
    echo json_encode(array('status' => 1, 'message' => 'Access Denided'));
    return;
}

// init var
$id_user = $users->id_user;
$id_shop = $users->id_shop;
$sub_marketplace = $users->sub_marketplace;
$id_country = $users->id_country;

// init params
$order_id = isset($users->order_id) && !empty($users->order_id) ? $users->order_id : '';

// get data
$mirakl_scheme = new MiraklScheme();
$shop_info = $mirakl_scheme->getShopInfo($id_user);
$get_config = $mirakl_scheme->getMiraklConfiguration($id_user, $sub_marketplace, $id_country, $id_shop);

$params = array();
$params['order_id'] = $order_id;
$params['is_lookup'] = empty($order_id) ? true : false;
//print_r($params); exit;
$mirakl_function = new MiraklOrderAccept($get_config, false);

//$results = $mirakl_function->acceptOrder($params);
$results = $mirakl_function->processOrder($params);

//echo '<pre>'; print_r($results); exit;

$log_history = array(
    'user_id' => $id_user,
    'ext' => $get_config['ext'],
    'shop_name' => isset($shop_info['name']) ? $shop_info['name'] : '',
    'countries' => $get_config['countries'],
    'sub_marketplace' => $sub_marketplace,
    'next_time' => 60,
    'owner' => 'user',
    'action' => MiraklHistoryLog::ACTION_TYPE_ACCEPT_ORDER,
    'transaction' => $results['status'] // check status in woking for check next time.
);

// Set history log
MiraklHistoryLog::set($log_history);

// response to node
echo json_encode($results);
