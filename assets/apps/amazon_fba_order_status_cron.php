<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.fba.orders.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]=='' 
    || !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4])
    || !isset($argv[5]) || $argv[5]=='' || !isset($argv[6]) || $argv[6]=='' || !is_numeric($argv[6]) ){
    die( 'No input data!');
}

// Get argv from url
$info = array(
    'user_id' => $argv[1],
    'user_name' => $argv[2],
    'ext' => $argv[3],
    'id_shop' => $argv[4],
    'shop_name' => str_replace('_',' ',$argv[5]),
    'id_marketplace' => $argv[6],
    'next_time' => 60,
    'owner' => 'system',
    'action' => 'fba_order_status',
);

// Get userdata fromdatabase
$data = AmazonUserData::get($info, false, true);

if( (!isset($data) || empty($data)) ){
    
    $info['countries'] = $info['ext'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {

    // skip un - master platform 
    if(!isset($data['fba_master_platform']) || empty($data['fba_master_platform']) || !$data['fba_master_platform']){

	$json = json_encode( array('error' => false, 'pass' => true, 'output' => 'we will run fba manager for master plateform only.') ) ;
	echo $json ; exit;

    }

    $data['shop_name'] = $info['shop_name'];
    $info['countries'] = $data['countries'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $users = json_decode(json_encode($data), false);
    $cron = true;
    $debug = false;
    
    $amazon_stocks = new AmazonFBAOrder($users, $cron, $debug);
    $amazon_stocks->Dispatch(AmazonFBAOrder::fba_status);
    
}