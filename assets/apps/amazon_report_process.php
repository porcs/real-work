<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.reports.php';

$debug = false;

if(!$debug)
{
    $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $datas = json_decode($data);
    $users = json_decode($datas->data);
}
else
{
    $data = '{"id_customer":"26","user_name":"u00000000000026","user":{"email":"i_am_praew@hotmail.com","firstname":"Praew","lastname":"Fah"},"id_country":"10","ext":".com","countries":"United State","currency":"USD","merchant_id":"AMXABK87V96I9","aws_id":"AKIAIM3RCTF2EJ4G4VHA","secret_key":"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","auth_token":"amzn.mws.e8a704f7-f68c-8188-edd9-95ed8f39c6ec","id_mode":"1","id_shop":"1","id_lang":"8","iso_code":"en","id_region":"us","allow_automatic_offer_creation":false,"synchronize_quantity":false,"synchronize_price":false,"synchronization_field":"ean13","active":true,"creation":true,"id_marketplace":2,"mode":"1","language":"english"}';
    $users = json_decode($data);
}

$amazon_reports = new AmazonReports($users, $debug);
$amazon_reports->getReport();