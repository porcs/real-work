<?php

require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.scheme.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.history.log.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/functions/mirakl.products.create.php';

$debug = false;

if (!$debug) {
    $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($argv[1])), null, 'UTF-8');
    $datas = json_decode($data);
    $users = json_decode($datas->data);
} else {
    $users = new stdClass;
    $users->id_user = '56';
    $users->id_shop = '1';
    $users->sub_marketplace = '13';
    $users->id_country = '10';
    $users->in_stock = 0;
    $users->send_mirakl = 0;
}

if (!isset($users) || empty($users)) {
    echo json_encode(array('status' => 1, 'message' => 'Access Denided'));
    return;
}

// init var
$id_user = $users->id_user;
$id_shop = $users->id_shop;
$sub_marketplace = $users->sub_marketplace;
$id_country = $users->id_country;

// init params
$in_stock = $users->in_stock;
$send_mirakl = $users->send_mirakl;

// get data
$mirakl_scheme = new MiraklScheme();
$shop_info = $mirakl_scheme->getShopInfo($id_user);
$get_config = $mirakl_scheme->getMiraklConfiguration($id_user, $sub_marketplace, $id_country, $id_shop);

$params = array();
$params['in_stock'] = $in_stock;
$params['send_mirakl'] = $send_mirakl;
//echo '<pre>'; print_r($params); exit;

$mirakl_function = new MiraklProductCreate($get_config, false);
$results = $mirakl_function->processProduct($params);
//$results = $mirakl_function->productCreate($params, false);

$log_history = array(
    'user_id' => $id_user,
    'ext' => $get_config['ext'],
    'shop_name' => isset($shop_info['name']) ? $shop_info['name'] : '',
    'countries' => $get_config['countries'],
    'sub_marketplace' => $sub_marketplace,
    'next_time' => 60,
    'owner' => 'user',
    'action' => MiraklHistoryLog::ACTION_TYPE_EXPORT_PRODUCT,
    'transaction' => $results['status'] // check status in woking for check next time.
);

// Set history log
MiraklHistoryLog::set($log_history);

// response to node
echo json_encode($results);
