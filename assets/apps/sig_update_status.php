<?php
define('_SKIP_NEWRELIC_',true);
require_once(dirname(__FILE__) . '/FeedbizImport/config/config.php'); 
require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');

//ini_set('display_errors',1);
if(isset($argv[1]) && $argv[1]!=''){
$user = $argv[1];
}else{
    die("need user id to access\n");
} 
if(isset($argv[1]) && $argv[1]=='view_system_process'  ){
    $score=0;
    $head = '';
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//        echo 'This is a server using Windows!';
        $cmd = ' tasklist /SVC | find "php.exe" ';
        $score=1;
    } else {
        $cmd = 'ps -eo pid,pcpu,pmem,vsz,start_time,time,etime,cmd  | head -n1';
        ob_start();
        passthru($cmd ); 
        $head = ob_get_contents();
        ob_end_clean();
//        echo 'This is a server not using Windows!';
        $cmd = " ps -eo pid,pcpu,pmem,vsz,start_time,time,etime,cmd  | grep php | grep -v 'php-fpm'| grep -v 'php sig_update_status.php' | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' ";
    }
    ob_start();
    passthru($cmd ); 
    $outt = ob_get_contents();
    ob_end_clean();
//    $cmd = " ps aux | grep bin/php | grep -v '/bin/sh' |grep -v 'grep' ";
      
    $add = 0 ;
    if(trim($outt) != ''){
        $add=$score;
    }
    if(function_exists('sys_getloadavg')){
    echo '<pre>';
    $cmd = "sudo find /var/spool/postfix/deferred -type f | wc -l";
    ob_start();
    passthru($cmd ); 
    $out = ob_get_contents();
    ob_end_clean();
    echo "\n\nMail queue : ".$out;

    $cmd = "df -h | head -n2";
    ob_start();
    passthru($cmd );
    $out = ob_get_contents();
    ob_end_clean();
    echo "\nDisk space : \n".$out;
    
    $cpu = sys_getloadavg();
    
    echo "\nCPU last usage :  ";
   $stat1 = file('/proc/stat'); 
    usleep(100000); 
    $stat2 = file('/proc/stat'); 
    $info1 = explode(" ", preg_replace("!cpu +!", "", $stat1[0])); 
    $info2 = explode(" ", preg_replace("!cpu +!", "", $stat2[0])); 
    $dif = array(); 
    $dif['user'] = $info2[0] - $info1[0]; 
    $dif['nice'] = $info2[1] - $info1[1]; 
    $dif['sys'] = $info2[2] - $info1[2]; 
    $dif['idle'] = $info2[3] - $info1[3]; 
    $total = array_sum($dif); 
    $total = $total==0?1:$total;
    $cpu = array(); 
    foreach($dif as $x=>$y) $cpu[$x] = round($y / $total * 100, 1);
    $out = "User:{$cpu['user']} Nice:{$cpu['nice']} Sys:{$cpu['sys']} Idle:{$cpu['idle']}";
    echo "\n".$out."\n-------------------------------------\n\n";
    $cmd = "sudo free ";
    ob_start();
    passthru($cmd ); 
    $out = ob_get_contents();
    ob_end_clean();
    echo "System resource : \n".$out."\n-------------------------------------\n\n";
    }
    if(!empty($outt)){
        echo $head."";
        print_r($outt);
    }
     
    echo '</pre>';
    echo '<meta http-equiv="refresh" content="10" />';
    exit;
}
if(isset($argv[1]) && $argv[1]=='register_backend_server'  ){
    require_once(dirname(__FILE__) . '/../../libraries/cron_server_report.php'); 
    $obj = new feedbiz_server_backend(null,true);
    exit;
}
if(isset($argv[1]) && $argv[1]=='backend_update_status'  ){
    require_once(dirname(__FILE__) . '/../../libraries/cron_server_report.php'); 
    $obj = new feedbiz_server_backend();
    
    $score=0;
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') { 
        $cmd = ' tasklist /SVC | find "php.exe" ';
        $score=1;
    } else { 
        $cmd = " ps aux | grep php | grep -v 'php-fpm'| grep -v 'php sig_update_status.php' | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' ";
    }
    ob_start();
    passthru($cmd ); 
    $out = ob_get_contents();
    ob_end_clean();  
    $add = 0 ;
    if(trim($out) != ''){
        $add=$score;
    }
    $out.='';
    $no = $add+(int)substr_count( $out, "\n" );
//    $str = strtok($out, "\n");
    $str = $out;
    $obj->update_server_load($no,$str);
    
    exit;
    
}

//if(isset($argv[1]) && $argv[1]=='system_num_process'  ){
//    
//    $score=0;
//    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
////        echo 'This is a server using Windows!';
//        $cmd = ' tasklist /SVC | find "php.exe" ';
//        $score=1;
//    } else {
////        echo 'This is a server not using Windows!';
//        $cmd = " ps aux | grep php | grep -v 'php-fpm'| grep -v 'php sig_update_status.php' | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' ";
//    }
//    ob_start();
//    passthru($cmd ); 
//    $out = ob_get_contents();
//    ob_end_clean();
////    $cmd = " ps aux | grep bin/php | grep -v '/bin/sh' |grep -v 'grep' ";
//      
//    $add = 0 ;
//    if(trim($out) != ''){
//        $add=$score;
//    }
//    $no = $add+(int)substr_count( $out, "\n" );
////    if( isset($argv[2]) && $argv[2]=='all_infos'){
//    
//        die(json_encode(array( 'proc'=>$out,'no'=>$no)));
////    }else{
////        die((int)substr_count( $out, "\n" ));
////    }
//    exit;
//}
$proc = new result_process($user,false); 

if(isset($argv[2]) && $argv[2]=='remove'){ 
    $proc->clear_process();
    exit;
}
ob_start();
 passthru("ps aux | grep php | grep ".__FILE__." | grep {$user} | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' ");
 $out = ob_get_contents();
 ob_end_clean();
$ox = explode("\n",$out);

if(sizeof($ox)>3){ 
    $delay = 4;
$timeout = 8;
$output=array();
$output['delay']=$delay*4000;
$output['timeout']=$timeout*2000; 
echo  json_encode($output);
exit;
}
$res = $proc->get_running_process();

$output = array();
$delay = 4;
$timeout = 8;
if(sizeof($res)>0){
foreach($res as $p){
        if(empty($p)||is_array($p))continue;
	$r= json_decode($p); 
        if(!isset($r->pid)||$r->pid==null)continue;
        
        $title = $r->msg_title;
        $del = $r->delay;
        $tim = $r->timeout;
        $ex = explode(' ',$r->type);
        $key = str_replace(' ','_', strtolower($r->type));
//        if( empty($r->type) && empty($r->msg))continue;
//        if((empty($r->msg_percent) || round($r->msg_percent,2) == 0) && empty($key))continue;
        
        if($key==''&&$title!=''){
            continue;
//            $key = strtolower($title);
//            $key = trim($key,'.');
//            $key = trim($key);
//            $key = str_replace(array(',','"',"'",'?','/','#','@','!','$','%','^','&',' '),'_',$key);
        }elseif($key==''&&$title==''){
            $key = md5(uniqid());
        }
        if( is_string($r->shop_name)){
            $r->shop_name = str_replace('%20',' ',$r->shop_name);
        }else{
            $index='0';
            if(isset($r->shop_name->$index) && !empty($r->shop_name->$index)){
                $r->shop_name->$index = str_replace('%20',' ',$r->shop_name->$index);
            }
        }
        $output['proc'][$key] = array(
            'time' => $r->at_time,
            'bid'=>$r->batch_id,
            'pid'=>$r->pid,
            'msg'=>$title,
            'name'=> $r->shop_name ,
            'sid'=>$r->shop_id,
            'action'=>$ex[0],
            'marketplace' => str_replace('%20',' ',$r->marketplace),
            'process'=>$r->type,
            'proc_msg'=>$r->msg,
            'err_msg'=>$r->error_msg,
            'err_status'=>$r->error_status,
            'priority'=>$r->priority,
            'progress'=>round($r->msg_percent,2),
            'popup_display'=>$r->popup_display,
            'status'=>$r->task_status,
            'comments' => $r->comments,
                
                );
        $delay = $delay<$del?$del:$delay;
        $timeout = $timeout<$tim?$tim:$timeout;
}
}
if(sizeof($res)>0 && isset($output['proc'])){
$output['delay']=$delay*4000;
$output['timeout']=$timeout*2000;
}
echo  json_encode($output);

exit;
?>
