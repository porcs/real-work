<?php

define('BASEPATH', '');
chdir(dirname(__FILE__));
require_once dirname(__FILE__) . '/../../libraries/newrelic_report.php';
require_once dirname(__FILE__) . '/../../application/libraries/FeedBiz/Messagings.php';

//http://ndb.feed.dev/assets/apps/messaging_cron.php?debug=1&user_id=328&id_order=

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;
$pass =true;
$output = array();

if($debug){
    $user_id = $_GET['user_id'];
} else {
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])){
        die( 'No input data!');
    }
    $user_id = $argv[1];
}

## 1. get user information ##
$user_config = new UserConfiguration();
$data = $user_config->check_site_verify($user_id);

if($debug){
    //echo '<br/><b>user information</b><pre>' . print_r($data, true) . '</pre>';
}

## 2. check shop verify ##
if(!isset($data['feed_shop_info']) || empty($data['feed_shop_info'])){
    echo json_encode(array('pass'=>'false',"output"=>"User:".$data['user_name']." did not Verify Shop"));
    die();
}

$shop = $data['feed_shop_info'];

## 3. check url verify ##
if(!isset($data['feed_biz']['verified']) || $data['feed_biz']['verified']!='verified'){
    echo json_encode(array('pass'=>'false',"output"=>"User:".$data['user_name']." did not Verify url"));
    die();
}

## 4. record history ##
$user_name = $data['user_name'];
$id_user = $data['user_id'];
$type = 'send_messaging_orders';
$next_time = 60; //mins
$shop_name = $shop['name']; 
$batch_id = uniqid();

$user_config->record_history($user_id, $shop_name, $type, $next_time);

$feedBizMessaging = new Messagings($user_name, $debug);
$params = $feedBizMessaging->get_messagings();
$allow_marketplace = $marketplace_list = array();

// Find marketplace ID
$marketplaces = new Marketplaces();
foreach ($marketplaces->getMarketplaceOffer() as $m){
    $marketplace_list[strtolower($m['name_offer_pkg'])] = $m['id_offer_pkg'];
    if(isset($m['sub_marketplaces'])){
        foreach ($m['sub_marketplaces'] as $sub_marketplaces){
            $marketplace_list[strtolower($sub_marketplaces)] = $m['id_offer_pkg'];
        }
    }
}


// Find Allow marketplace ID
foreach ($params as $mk => $param){
    foreach ($param as $site => $p){
        if(isset($marketplace_list[$mk]) && isset($p['mail_invoice_active']) && $p['mail_invoice_active'] == 1){
            $id_marketplace = $marketplace_list[$mk];
            $allow_marketplace[$id_marketplace][$site] = true;
        }
    }
}
if($debug){
    //echo '<br/><b>Allow Marketplace</b><pre>' . print_r($allow_marketplace, true) . '</pre>';
}

//////// send invoice //////
$resultSendInvoice = $feedBizMessaging->sendInvoice($id_user, $params, $user_config, $data, $allow_marketplace);
if($resultSendInvoice)
    $output[] = "Send Invoice Success";

//////// send review //////
$resultSendReviewIncentive = $feedBizMessaging->sendReviewIncentive($id_user, $params, $user_config, $data, $allow_marketplace);
if($resultSendReviewIncentive)
    $output[] = "Send Review Incentive Success";

//////// Customer Thread Email////// CustomerThreadEmail($user_id, $user_config = null, $data = null)
$resultCustomerThreadEmail = $feedBizMessaging->customerThreadEmail($id_user, $user_config, $data);
if($resultCustomerThreadEmail)
    $output[] = "Customer Thread Email Success";

echo json_encode(array('pass'=>$pass, "output"=>"User:".$data['user_name'].", " .implode(", ", $output)));
exit;