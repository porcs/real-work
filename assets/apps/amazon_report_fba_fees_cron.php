<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.reports.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

//https://qa.feed.biz/assets/apps/amazon_report_fba_fees_cron.php?debug=1&id_customer=26&user_name=u00000000000026&ext=.es&id_shop=1&shop_name=FOXCHIP

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;
   
if($debug) {
   
    $info = array(
	'user_id' => $_GET['id_customer'],
	'user_name' => $_GET['user_name'],
	'ext' => $_GET['ext'],
	'id_shop' => $_GET['id_shop'],
	'shop_name' => $_GET['shop_name'],
	'next_time' => 1440,
	'owner' => 'system',
	'action' => 'fba_report',
    );
    
} else {
    
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]=='' 
	|| !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
	|| !isset($argv[5]) || $argv[5]==''){
	die( 'No input data!');
    }
    // Get argv from url
    $info = array(
	'user_id' => $argv[1],
	'user_name' => $argv[2],
	'ext' => $argv[3],
	'id_shop' => $argv[4],
	'shop_name' => $argv[5],
	'next_time' => 1440,
	'owner' => 'system',
	'action' => 'fba_report',
	//'region' => $user_info['region'],
	//'id_marketplace' => $user_info['id_marketplace'],
    );
}

// Get userdata fromdatabase
$data = AmazonUserData::get($info, false, true);

if(!isset($data) || empty($data) ){
    
    $info['countries'] = $info['ext'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    // skip un - master platform 
    /*if(!isset($data['fba_master_platform']) || empty($data['fba_master_platform']) || !$data['fba_master_platform']){

	$json = json_encode( array('error' => false, 'pass' => true, 'output' => 'we will run fba report for master plateform only. ' . $data['countries'] . ' is not Master Platform') ) ;
	echo $json ; exit;

    }*/
    
    $data['shop_name'] = $info['shop_name'];
    $info['countries'] = $data['countries'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $users = json_decode(json_encode($data), false);
    
    $datefrom = date('d-m-Y',strtotime('-3 day'));
    $dateto = date('d-m-Y');
    
    $amazon_reports = new AmazonReports($users, $debug);
    $amazon_reports->getReportFulfillmentFees($datefrom, $dateto);
    
}