<?php 

define('BASEPATH', '');
chdir(dirname(__FILE__)); 

require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.repricing.automaton.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]=='' 
    || !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
    || !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]==''){
    die( 'No input data!');
}

// Get argv from url
$info = array(
    'user_id' => $argv[1],
    'user_name' => $argv[2],
    'ext' => $argv[3],
    'id_shop' => $argv[4],
    'shop_name' => str_replace('_',' ',$argv[5]),
    'id_marketplace' => $argv[6],
    'next_time' => 20,
    'owner' => 'system',
    'action' => 'repricing_export',
);

$info['countries'] = $info['ext'];

// Set history log
AmazonHistoryLog::set($info);
    
if(!isset($info) || empty($info) ){    
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    $info['id_user'] = $info['user_id'];    
   
    $debug = false;
    $cron = true;
    $object = json_decode(json_encode($info), FALSE);
    
    $amazon_repricing = new AmazonRepricingAutomaton($object, $debug, $cron);
    $amazon_repricing->Dispatch(AmazonRepricingAutomaton::EXPORT);
}