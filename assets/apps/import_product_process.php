<?php
    if(!isset($argv[1]) || $argv[1]==''){
        die( 'No input data!');
    }
    $input = json_decode(rawurldecode(($argv[1])));
    $input = (json_decode($input->data,true)); 
    if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){
        die('This site was not verified.');
    }
    $shop = $input['feed_shop_info'];
    $shop_type = strtolower($shop['software']); 
    
    //prepare data products
    function prepare_argv ($input,$type = 'products',$shop_type=''){ 
        $add = '&';
        $url_feed = $input['feed_shop_info']['url'][$type];
        
        if(strpos($url_feed,'?') === false){
            $add='?';
        }
        //$input['feed_token']='805070727';
        $add_url = $add."fbtoken=".$input['feed_token'];
        
        
         return json_encode(
                    array(
                        'username' =>$input['feed_biz']['username'],
                        //'token' => '2f3ec004209a653d20ed4e90a4fc59b5',
                        'gdata' => array(
                            'fileurl' => $input['feed_shop_info']['url'][$type].$add_url,
                            'setting_fileurl' => $input['feed_shop_info']['url']['settings'].$add_url,
                            'ws_token'   => $input['feed_token'],
                        )
                )); 
    } 
    
    //echo prepare_argv_products($input);
    //print_r(json_decode(prepare_argv_products($input)));
    //$shop_type = 'opencart'; 
    $out= array();
    switch($shop_type){
        case 'prestashop': 
            $res = exec('php insert_prestashop_products.php '.rawurlencode(prepare_argv($input))); 
            $out[] = json_decode($res,true);  
            echo json_encode($out); 
        break;
        case 'opencart': 
            $res = exec('php insert_opencart_products.php '.rawurlencode(prepare_argv($input))); 
            $out[] = json_decode($res,true);  
            echo json_encode($out);  
        break;
        case 'gmerchant':
            $res = exec('php insert_gmerchant_products.php '.rawurlencode(prepare_argv($input)));
            $out = json_decode($res);
            if($out->pass){
                $res = exec('php insert_gmerchant_offers.php '.rawurlencode(prepare_argv($input,'offers')));
            }
        break;
        case 'magento':
            $res = exec('php insert_magento_products.php '.rawurlencode(prepare_argv($input,'products',$shop_type))); 
            $out[] = json_decode($res,true);  
            echo json_encode($out); 
        break;
        case 'woocommerce': 
            $res = exec('php insert_woocommerce_products.php '.rawurlencode(prepare_argv($input)));
            $out[] = json_decode($res,true);  
        
            echo json_encode($out);
        break;
        case 'shopify': 
            $res = exec('php insert_shopify_products.php '.rawurlencode(prepare_argv($input)));
            $out[] = json_decode($res,true);  
        
            echo json_encode($out);
        break;
        default://other cms
            
        break;
    }
    