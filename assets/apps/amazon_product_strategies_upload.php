<?php

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.database.php';
require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

if(!isset($argv[1]) || $argv[1]==''){
    die( 'No input data!');
}

//$argv[1] = urlencode(json_encode(array('data' =>json_encode(array('user_name'=>"u00000000000026","id_shop"=>"1","shopname"=>"FOXCHIP","id_country"=>"10")))));
$argv_data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
$datas = json_decode($argv_data);

class ImportProductStrategies 
{        
    public function Dispatch($user_name, $id_shop, $shopname, $id_country, $debug = false){
               
        $amazon_database = new AmazonDatabase($user_name);
        
        $batchID = uniqid();
        $process_title = 'Import Product Strategies ';
        $process_type = 'Import Product Strategies';
	
	if(!$debug){
	    $proc_rep = new report_process($user_name, $batchID, $process_title, false, true);
	    $proc_rep->set_process_type($process_type);
	    $proc_rep->set_shop($id_shop, $shopname);
	}
	
        $upload_path = USERDATA_PATH . $user_name .'/process/';
        $file_name = 'product_strategies.csv';      
        
        if(!file_exists($upload_path . $file_name)){
            $error = 'File does not exits : ' .$upload_path . $file_name ;
            $proc_rep->set_error_msg($error);
            $proc_rep->finish_task();
            echo json_encode(array('error' => $error,'pass' =>  false,'output' => '')); 
            exit;
        }

        $path = $upload_path . $file_name; 
        
        $file = file($path);
	
	if(!$debug)
	    $proc_rep->set_max_min_task(count($file), 0);
        
        $datalist = $head = array();
        $delimiter = ',';
        $array_header = array();
        $array_header['SKU'] = "sku";
        $array_header['Minimum_Price'] = "minimum_price";
        $array_header['Maximum_Price'] = "target_price";       
        $array_header['id_product'] = "id_product";    
        $array_header['id_product_attribute'] = "id_product_attribute";    
        $array_header['asin'] = "asin";    
        $array_header['actual_price'] = "actual_price";    
        $array_header['gap'] = "gap";    
        
        if ($path) {
            $fp = fopen($path, 'r');
            $buf = fread($fp, 1000);
            fclose($fp);
        } else {
            $error = 'Import Product Strategies fail : ' . $path;
	    
	    if(!$debug) {
		$proc_rep->set_error_msg($error);
		$proc_rep->finish_task();
	    }
	    
            echo json_encode(array('error' => $error,'pass' =>  false,'output' => '')); 
            exit;
        }

        $count1 = count(explode(',', $buf));
        $count2 = count(explode(';', $buf));
        if ($count2 > $count1){
            $delimiter = ';';
        }

        $fp = fopen($path, 'r');
        $i = 0;

        while ($data = fgetcsv($fp, 0, $delimiter)) {  
            
            if (!is_array($data)){
		
                $error = 'Data is incorrect';
		
		if(!$debug) {
		    $proc_rep->set_error_msg($error);
		    $proc_rep->finish_task();
		}
		
                echo json_encode(array('error' => $error,'pass' =>  false,'output' => '')); 
                exit;
            }
            if($i == 0){
                foreach ($data as $khead => $vhead) {
                    if (!empty($vhead)) {
                        $vhead = str_replace(" ", "_", $vhead);
                        $head[$khead] = strtolower(isset($array_header[$vhead]) ? $array_header[$vhead] : $vhead);
                    }
                }
            } else {
                
                foreach ($data as $khead => $vhead) {
                    if (!empty($vhead)) {
                        $datalist[$i][$head[$khead]] = trim($vhead);
                    }
                }
                
                if(!empty($datalist[$i]) && !in_array('id_product', $head) && !in_array('id_product_attribute', $head)){
                    if(!isset($datalist[$i]['sku']))continue; 
                    $product = $amazon_database->feedbiz->getProductBySKU($id_shop, $datalist[$i]['sku']);
                    if(!isset($product['id_product'])) continue;
                    $datalist[$i]['id_product'] = $product['id_product'];
                    $datalist[$i]['id_product_attribute'] = $product['id_product_attribute'];
                }
            }
            $i++;
        } 
        
        if(!empty($datalist)){
            $result = $amazon_database->productStrategiesUpload($datalist, $id_shop, $id_country);
        }
	
	if(!$debug) {
	    $proc_rep->finish_task();
	}
	
        echo json_encode(array('error' => '', 'pass' =>  true, 'output' => isset($result) ? $result : 'Import Offers Options Success')); 
        exit;
    }
}

$debug = false;

$update = new ImportProductStrategies();
$user_data = json_decode($datas->data);
$id_country = isset($user_data->id_country) ? $user_data->id_country : null;
$update->Dispatch($user_data->user_name, $user_data->id_shop, $user_data->shopname, $id_country, $debug);