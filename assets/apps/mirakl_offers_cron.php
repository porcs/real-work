<?php

require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.scheme.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.history.log.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/functions/mirakl.products.update.php';

$debug = false;

if (!isset($argv[1]) || $argv[1] == '' || !is_numeric($argv[1]) ||
        !isset($argv[2]) || $argv[2] == '' || !is_numeric($argv[2]) ||
        !isset($argv[3]) || $argv[3] == '' || !is_numeric($argv[3]) ||
        !isset($argv[4]) || $argv[4] == '' || !is_numeric($argv[4])
) {
    die('No input data!');
}

//$argv[1] = 56;
//$argv[2] = 1;
//$argv[3] = 13;
//$argv[4] = 10;

// init var
$id_user = $argv[1];
$id_shop = $argv[2];
$sub_marketplace = $argv[3];
$id_country = $argv[4];

// init params
$export_all = 0; // no export offers all
$purge = 0; // mode normal
$send_mirakl = 1; // send to mirakl

// times
$next_time = 60;

// get data
$mirakl_scheme = new MiraklScheme();
$shop_info = $mirakl_scheme->getShopInfo($id_user);
$get_config = $mirakl_scheme->getMiraklConfiguration($id_user, $sub_marketplace, $id_country, $id_shop);


/* set file to update export all */
$folder_timers = USERDATA_PATH.$get_config['user_name'].'/mirakl/times/';

if (!file_exists($folder_timers)) {
    mkdir($folder_timers, 0777);
    chmod($folder_timers, 0777);
}

$file_name = 'offer_next_time_'.$id_shop.'_'.$sub_marketplace.'_'.$id_country;
$file = $folder_timers.$file_name.'.txt';

$readtime = file_exists($file) ? file_get_contents($file) : 0;
$readtime = $readtime + $next_time;

file_put_contents($file, $readtime);

if (file_exists($file)) {
    $perms = (int) substr(sprintf('%o', fileperms($file)), -4);
    if ($perms != 777 && $perms != 666) {
        chmod($file, 0777);
    }
}

if($readtime > (1440 * 7)){ // 1440 * 7; // 7/day
    $export_all = 1;
    file_put_contents($file, 0);
}
/* end set file */

$params = array();
$params['export_all'] = $export_all;
$params['purge'] = $purge;
$params['send_mirakl'] = $send_mirakl;

$mirakl_function = new MiraklProductUpdate($get_config, true);
//$results = $mirakl_function->productUpdate($params, false);
$results = $mirakl_function->processProduct($params, false);

$log_history = array(
    'user_id' => $id_user,
    'ext' => $get_config['ext'],
    'shop_name' => isset($shop_info['name']) ? $shop_info['name'] : '',
    'countries' => $get_config['countries'],
    'sub_marketplace' => $sub_marketplace,
    'next_time' => $next_time,
    'owner' => 'system',
    'action' => MiraklHistoryLog::ACTION_TYPE_EXPORT_OFFER,
    'transaction' => $results['status'] // check status in woking for check next time.
);

// Set history log
MiraklHistoryLog::set($log_history);

// response to node
//echo json_encode($results);
