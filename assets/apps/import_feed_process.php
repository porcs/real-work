<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
    if(!isset($argv[1]) || $argv[1]==''){
        die( 'No input data!');
    }

//$argv[1]=urlencode('{"feed_biz":{"base_url":"http:\/\/dev.prestashop-less.biz\/modules\/feedbiz\/functions\/connector.php","username":"u00000000000035","verified":"verified","user_id":"35"},"feed_shop_info":{"name":"Prestashop_G","software":"Prestashop","version":"1.6.0.9","url":{"settings":"http:\/\/dev.prestashop-less.biz\/modules\/feedbiz\/functions\/settings.php","products":"http:\/\/dev.prestashop-less.biz\/modules\/feedbiz\/functions\/products.php","offers":"http:\/\/dev.prestashop-less.biz\/modules\/feedbiz\/functions\/offers.php","stockmovement":"http:\/\/dev.prestashop-less.biz\/modules\/feedbiz\/functions\/stockmovement.php","orderimport":"http:\/\/dev.prestashop-less.biz\/modules\/feedbiz\/functions\/orders_import.php"}},"feed_token":"ef8b1678cc8b78801d49cd8a483d0b94"}');
    $input = json_decode(rawurldecode(($argv[1])));
    $input = (json_decode($input->data,true)); 
//    $input = (json_decode($input->data,true)); 
    if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){
        die('This site was not verified.');
    }
    $shop = $input['feed_shop_info'];
    $shop_type = strtolower($shop['software']); 
    
    //prepare data products
    function prepare_argv ($input,$type = 'products',$shop_type=''){ 
        $add = '&';
        $url_feed = $input['feed_shop_info']['url'][$type];
        
		if(strpos($url_feed,'?') === false){
            $add='?';
        }
        //$input['feed_token']='805070727';
        $add_url = $add."fbtoken=".$input['feed_token'];
        
        
         return json_encode(
                    array(
                        'username' =>$input['feed_biz']['username'],
                        'from_task' =>1,
                        //'token' => '2f3ec004209a653d20ed4e90a4fc59b5',
                        'gdata' => array(
                            'fileurl' => $input['feed_shop_info']['url'][$type].$add_url,
                            'setting_fileurl' => $input['feed_shop_info']['url']['settings'].$add_url,
                            'ws_token'   => $input['feed_token'],
                        )
                )); 
    } 
    
    //echo prepare_argv_products($input);
    //print_r(json_decode(prepare_argv_products($input)));
    //$shop_type = 'opencart'; 
    $out = array();
    switch($shop_type){
        case 'prestashop': 
            $res = exec('php insert_prestashop_products.php '.rawurlencode(prepare_argv($input)));
            $out[] = json_decode($res,true);  
//             $out = $res.' Empty Val  php insert_prestashop_products.php '.rawurlencode(prepare_argv($input));
//             echo json_encode(json_encode($out)); exit;
            if($out[0]['pass']==true){ 
                $res = exec('php insert_prestashop_offers.php '.rawurlencode(prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);  
            } 
            echo json_encode($out); 
        break;
        case 'opencart': 
            $res = exec('php insert_opencart_products.php '.rawurlencode(prepare_argv($input)));
            $out[] = json_decode($res,true);  
            if($out[0]['pass']==true){  
                $res = exec('php insert_opencart_offers.php '.rawurlencode(prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);  
            }
            echo json_encode($out);
        break;
        case 'gmerchant':
            $res = exec('php insert_gmerchant_products.php '.rawurlencode(prepare_argv($input)));
            $out = json_decode($res);
            if($out->pass){
                $res = exec('php insert_gmerchant_offers.php '.rawurlencode(prepare_argv($input,'offers')));
            }
        break;
        case 'magento':
            $res = exec('php insert_magento_products.php '.rawurlencode(prepare_argv($input,'products',$shop_type))); 
//            print_r($input);
//            print_r(prepare_argv($input,'products',$shop_type));
//            echo $shop_type;
            $out[] = json_decode($res,true);  
            if($out[0]['pass']==true){  
                $res = exec('php insert_magento_offers.php '.rawurlencode(prepare_argv($input,'offers',$shop_type)));
                $out[] = json_decode($res,true);  
            }
            echo json_encode($out);
        break;
        case 'woocommerce': 
            $res = exec('php insert_woocommerce_products.php '.rawurlencode(prepare_argv($input)));
            $out[] = json_decode($res,true);  
            if($out[0]['pass']==true){  
                $res = exec('php insert_woocommerce_offers.php '.rawurlencode(prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);  
            }
            echo json_encode($out);
        break;
        case 'shopify': 
            $res = exec('php insert_shopify_products.php '.rawurlencode(prepare_argv($input)));
            $out[] = json_decode($res,true);  
            if($out[0]['pass']==true){  
                $res = exec('php insert_shopify_offers.php '.rawurlencode(prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);  
            }
            echo json_encode($out);
        break;
        default://other cms
            
        break;
    }
    
    set_shop_detail($input);
    
    function set_shop_detail($info){
        if(isset($info) && !empty($info)){
            require_once(dirname(__FILE__) . '/../../application/config/config.php');
            require_once(dirname(__FILE__) . '/../../application/libraries/db.php');
            require_once(dirname(__FILE__) . '/FeedbizImport/products/ObjectModel.php');
            require_once dirname(__FILE__) . '/FeedbizImport/products/Shop.php';
            
            $user = $info['feed_biz']['username'];
            $shop_name = str_replace('_',' ', $info['feed_shop_info']['name']);
            $shop_info = base64_encode(str_replace('_',' ',serialize($info['feed_shop_info'])));
            $shop_feed = base64_encode(serialize($info['feed_biz']));
            
            $data = base64_encode(serialize(array('FEED_SHOP_INFO' => $shop_info, 'FEED_BIZ' => $shop_feed)));
                        
	    $Shop = new Shop($user);
	    
            //feed_shop_info
            $shop = $Shop->updateShopDetail($user, $shop_name, $data, 'products');
            
            if($shop)
                $Shop->updateShopDetail($user, $shop_name, $data, 'offers');
        }
    }