<?php

define('BASEPATH', '');
chdir(dirname(__FILE__)); 
require_once  (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
//require_once  (dirname(__FILE__).'/../../libraries/ci_db_connect.php');

if ( isset($_GET['debug'])) {
    $user_id = $_GET['user_id'];
    $uri = isset($_GET['uri']) ? 1 : 0;
    $uinfo = new UserConfiguration(); 
    $input = $uinfo->check_site_verify($user_id); 

    $shop = $input['feed_shop_info']; 

    if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){
        die();
    }else if( !isset($shop['url']['shippedorders'])){
        die();
    }

    $shippedorder_url = $shop['url']['shippedorders'];
    $conj = strpos($shippedorder_url, '?') !== FALSE ? '&' : '?'; 
    $input['xml_url'] =$shippedorder_url . $conj . 'fbtoken=' . $input['feed_token'];

    if(!isset($input['xml_url']) || empty($input['xml_url']) || !isset($input['user_name']) || empty($input['user_name'])){
        die('Data input is broken');
    }

    $user_name = $input['user_name'];
    $id_user = $input['user_id'];
    $type = 'import_order'; 
    $next_time = 60; //mins
    $shop_name = $shop['name']; 

    include (dirname(__FILE__). "/../../application/config/config.php");
    require_once DIR_SERVER . '/application/libraries/FeedBiz_Orders.php';
    require_once DIR_SERVER . '/application/libraries/Ebay/ObjectBiz.php';
    require_once DIR_SERVER . '/application/libraries/Ebay/ebayshipment.php';        
    require_once DIR_SERVER . '/application/libraries/Amazon/functions/amazon.status.orders.php';
    require_once (dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php'); 
    $ebayObjectBiz = new ObjectBiz(array($user_name));
    $feedBizOrder = new FeedBiz_Orders(array($user_name));
    $ebayCarriers = $ebayObjectBiz->getAllCarriers();
    //INITIAL END

    //retrieve setting + connector
    $orders = array();
    $order_ids = array();
    if ($uri) {
        $input['xml_url'] = $input['xml_url'].'&debug=1';
        echo "url ".$input['xml_url']."\n";
    }
    
    if(function_exists('fbiz_get_contents')){
        $xml_string = fbiz_get_contents($input['xml_url']); 
    }else{
        $xml_string = file_get_contents($input['xml_url']); 
    }
    if ($uri) {
        echo "<pre>", print_r($xml_string, true), "</pre>"."<br>";
    }

    // validate xml
    libxml_use_internal_errors(true);
    $xml_doc = simplexml_load_string($xml_string);
    $shop_id_list = array();

    if(empty($xml_doc)){
        sleep(5);
    //    echo "\n ---Reload XML--- \n";
        if(function_exists('fbiz_get_contents')){
            $xml_string = fbiz_get_contents($input['xml_url']);  
        }else{
            $xml_string = file_get_contents($input['xml_url']); 
        }
        $xml_doc = simplexml_load_string($xml_string);
    }


    // 1. LOAD DOM AND UPDATE ORDER TRACKING
    if ($xml_doc) {

        $xml_dom = new SimpleXMLElement($xml_string);
        echo "<pre>", print_r($xml_dom, true), "</pre>"."<br>";
        foreach ($xml_dom->Orders->Order as $orderDOM) {
            $seller_id = $orderDOM->attributes()->{'ID'};
            if ( empty($orderDOM->MPOrderID) && !empty($seller_id)) {
                $seller_ids[] = strval($seller_id);
                $seller_datas[strval($seller_id)] = array(
                    'CarrierID' => strval($orderDOM->CarrierID),
                    'CarrierName' => strval($orderDOM->CarrierName),
                    'ShippingNumber' => strval($orderDOM->ShippingNumber),
                    'ShippingDate' => strval($orderDOM->ShippingDate)
                );
            }
            $orders [strval($orderDOM->MPOrderID)] = array(
                'MPOrderID' => strval($orderDOM->MPOrderID),
                'CarrierID' => strval($orderDOM->CarrierID),
                'CarrierName' => strval($orderDOM->CarrierName),
                'ShippingNumber' => strval($orderDOM->ShippingNumber),
                'ShippingDate' => strval($orderDOM->ShippingDate)
            );
            $order_ids [] = strval($orderDOM->MPOrderID);
        }
        
        if ( !empty($seller_ids)) {
            $resultSellerIDs = $feedBizOrder->findOrdersFromSellerOrderId(
                $user_name, $seller_ids
            );
            
            foreach ( $resultSellerIDs as $sellerID => $order ) {
                $seller_datas[$sellerID]['MPOrderID'] = $order;

                $orders[$order] = $seller_datas[$sellerID];
            }
            unset($seller_datas);
        }
    file_put_contents('/var/www/backend/assets/apps/fifo_dir/test_import_order.txt',print_r(array($orders,date('c')),true),FILE_APPEND);                  
    echo "order list : ".sizeof($orders)."\n";
    echo "<pre>", print_r($orders, true), "</pre>"."<br>";
    }
}
//echo "\n-----------end shop order cron-------------\n";
