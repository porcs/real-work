<?php
define ( 'BASEPATH', '' );
// ini_set('display_errors',1);
// ini_set('display_startup_errors',1);
// error_reporting(-1);
chdir ( dirname ( __FILE__ ) );
require_once (dirname ( __FILE__ ) . '/../../application/config/config.php');
require_once (dirname ( __FILE__ ) . '/../../libraries/ci_db_connect.php');
require_once (dirname ( __FILE__ ) . '/../../application/libraries/FeedBiz.php');
require_once (dirname ( __FILE__ ) . '/../../application/libraries/FeedBiz_Orders.php');
require_once (dirname ( __FILE__ ) . '/../../application/libraries/Ebay/ebayorder_debug.php');
require_once (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
require_once (dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');

 $user_id = 53;
 $user_name = 'u00000000000053';
 $site_id = 71;
 $site_domain = 'ebay.fr';
 $shop_name = 'Eclairage_Design';
 $id_marketplace = 3;

$id_shop = 1;
$ext = str_replace('ebay','', $site_domain);

class LoadMPOrderCron {
	var $mysqlPortal = null;
	var $user_id = 0;
	var $user_name = '';
	var $user_code = '';
	var $shop_id = 0;
	var $id_marketplace = 0;
	var $site_id = 0;
	var $site_domain = '';
	var $shop_name = '';
        var $next_time = 20;
        
	public function __construct($user_id, $id_marketplace, $site_id, $site_domain, $shop_name, $id_shop) {
		$this->user_id = $user_id;
		$user = $this->mysqlPortal->getUsers ( $this->user_id );
		$this->user_name = $user ['user_name'];
		$this->user_code = $user ['user_code'];
		$FeedBiz = new FeedBiz ( array (
				$this->user_name 
		) );
		if ( empty($id_shop) ) {
                        $shops = $FeedBiz->getDefaultShop ( $this->user_name );
                        $this->shop_id = $shops ['id_shop'];
                }
                else {
                        $this->shop_id = $id_shop;
                }
		$this->site_id = $site_id;
		$this->site_domain = $site_domain;
		$this->shop_name = $shop_name;
		$this->id_marketplace = $id_marketplace;
	}
	public function dispatch() {
		if ($this->id_marketplace == 3) {
			$param = array (
					$this->user_name,
					$this->user_id,
					$this->site_id 
			);
			$ebayorder = new ebayorder ( $param );
			$data_from = date ( 'Y-m-d', strtotime ( '-60 days' ) );
			$data_to = date ( 'Y-m-d', strtotime ( '+2 days' ) );
			// EBAYAPI
			$parser = $ebayorder->get_orders ( $data_from, $data_to, 60 );
		}
	}
}
class MysqlPortal {
	var $ci_db_connect = null;
	public function __construct() {
		$this->ci_db_connect = new ci_db_connect ();
	}
	public function getUsers($user_id) {
		$user_sql = "select id as user_id, user_name, user_code from users where id = '$user_id'";
		$user_query = $this->ci_db_connect->select_query ( $user_sql );
		$user_row = $this->ci_db_connect->fetch ( $user_query );
		return $user_row;
	}
	function getConfiguration($user_id) {
		$mainconfiguration = array ();
		$configuration_sql = "select * from configuration where id_customer = '" . intval ( $user_id ) . "' and name IN ('FEED_BIZ', 'FEED_MODE','FEED_SHOP_INFO','FIRST_CONF', 'EBAY_USER')";
		$configuration_query = $this->ci_db_connect->select_query ( $configuration_sql );
		while ( $row = $this->ci_db_connect->fetch ( $configuration_query ) ) {
			$mainconfiguration [$row ['name']] = $row ['value'];
		}
		return $mainconfiguration;
	}
	function logCron($data) {
		$this->ci_db_connect->add_db ( 'histories', $data );
	}
}

$object = new LoadMPOrderCron ( $user_id, $id_marketplace, $site_id, $site_domain, $shop_name, $id_shop );
$object->dispatch ();