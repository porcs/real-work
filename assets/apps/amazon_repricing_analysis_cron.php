<?php 

define('BASEPATH', '');
chdir(dirname(__FILE__)); 

require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.repricing.automaton.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

//https://qa.feed.biz/assets/apps/amazon_repricing_analysis_cron.php?debug=1&id_customer=26&user_name=u00000000000026&ext=.es&id_shop=1&shop_name=FOXCHIP

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;
   
if($debug) {
   
    $info = array(
	'user_id' => $_GET['id_customer'],
	'user_name' => $_GET['user_name'],
	'ext' => $_GET['ext'],
	'id_shop' => $_GET['id_shop'],
	'shop_name' => $_GET['shop_name'],
	'id_marketplace' => 2,
	'next_time' => 1440,
	'owner' => 'system',
	'action' => 'fba_report',
    );
    
} else {
    
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]=='' 
	|| !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
	|| !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]==''){
	die( 'No input data!');
    }

    // Get argv from url
    $info = array(
	'user_id' => $argv[1],
	'user_name' => $argv[2],
	'ext' => $argv[3],
	'id_shop' => $argv[4],
	'shop_name' => str_replace('_',' ',$argv[5]),
	'id_marketplace' => $argv[6],
	'next_time' => 2,
	'owner' => 'system',
	'action' => 'repricing_analysis',
    );
    
}

$info['countries'] = $info['ext'];

// Set history log
AmazonHistoryLog::set($info);
    
if(!isset($info) || empty($info) ){
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    $info['id_user'] = $info['user_id'];
    $cron = true;
    $object = json_decode(json_encode($info), FALSE);
    
    $amazon_repricing = new AmazonRepricingAutomaton($object, $debug, $cron);
    $amazon_repricing->Dispatch(AmazonRepricingAutomaton::REPRICE);
}
