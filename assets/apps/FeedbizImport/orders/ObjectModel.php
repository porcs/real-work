<?php

abstract class ObjectModel
{
    private $user;
    private $path;
    public static $database = 'orders'; 
    public $prefix_table = 'orders_';
    public $engine = " ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ";
    
    /* Build object */
    public function __construct( $user, $id = null, $connect = true)
    {
        $this->user = $user;
        $this->path = USERDATA_PATH . $user . '/' . ObjectModel::$database . '.db';
       
        if ($id)
            $this->id = (int)$id;
         
         if($connect) $this->db_connect($this->path);

    }
    
    public static function createDatabase($user) 
    {
        $path   = DIR_SERVER . '/assets/apps';
        $result = array();
        
        if ( !is_writable( $path . '/users') )
        {
            $result['error'] = sprintf('Directory %s is not writeable', $path) ; 
        }
        else
        {
            $error = '';
            $oldmask = umask(0); 

            $conn_id = new Db($user,ObjectModel::$database,false  );
             
            if( $conn_id->db_status  )
                $result['error'] = $error;
            umask($oldmask);
        }
        
        return $result;
    }
        
    public function createTable()
    {   
        $result = '';
        $class = get_called_class();
        $data =  $class::$definition;        
        
        $lang = false;
        $loop = 1 ;
        
        if (isset($data['lang']) || !empty($data['lang']))
            $lang = true;
        
        if ($lang)
            $loop = 2;

        for( $l=1; $l <= $loop; $l++ )
        {
            $query = '';
            $sql = array();
            $tablename = $data['table'];
            
            if($l == 2)
            {
               $tablename = $data['table'] . '_lang';
               $data['fields'] = $data['lang']['fields'];
            }
            else 
                $data['fields'] = $data['fields'];
            
            foreach ($data['fields'] as $key_field => $field)
            {
                $default = '';
                $size = '';
                
                if(!isset($field['required']))
                    $field['required'] = false;
                
                $required = ($field['required']) ? ' NOT NULL ' : '' ;
                $default_null =  (isset($field['default_null']) && !$field['required']) ? ' DEFAULT NULL' : '' ;
                $others =  ( isset($field['other']) && !empty($field['other']) ) ? ' ' . $field['other'] : '' ;
//                $primary_key =  (isset($field['primary_key'])) ? ' PRIMARY KEY AUTO_INCREMENT' : '' ;
                
                 
                    $primary_key = (isset($field['primary_key'])) ? '  AUTO_INCREMENT ' : '' ;
                
                
                if(!isset($field['default_null']))
                    $default =  isset($field['default']) ? " DEFAULT '" . $field['default'] . "'" : '' ;
                
                if(isset($field['size']))
                    $size = '(' . $field['size'] . ') ';
                $unique = ' ';
//                if(!empty($data['unique']) && in_array($key_field,$data['unique'])){
//                    $unique = ' UNIQUE';
//                }
                $sql[] = $key_field . ' ' . $field['type'] .  $size  . $required . $default_null . $default . $primary_key . $others .$unique;
            }
            
            $arr_key=array();
            if(isset($data['unique']))
            foreach($data['unique'] as $v){
                if(is_array($v)){
                    foreach($v as $datab){
                        $arr_key[$datab]=$datab;
                    }
                }else{
                    $arr_key[$v]=$v;
                }
            }
            if(isset($data['primary']))
            foreach($data['primary'] as $v){ 
                    $arr_key[$v]=$v; 
            } 
            if($l == 2){
                $arr_key['id_lang']='id_lang';
            }
            if(!empty($arr_key)){
                $sql[] = " PRIMARY KEY (".implode(',',$arr_key).")";
            } 
            
            $sqls = implode(', ', $sql);
            $query .= "CREATE TABLE " . $this->prefix_table . $tablename . "(" . $sqls ;
            
            $query .= ") {$this->engine};" ;
            $out = $this->db_exec($query,false);
        }

        if($out!== true)
            $result .= sprintf('Failed to create table %s  '."<br>$query<br>$out" , $class  ) ; 
         
        //Create Index
        if (isset($data['primary']) && !empty($data['primary']))
        {
            $out = $this->createIndex( $data['table'] . "Index", $data['table'], true, $data['primary']);
            if($out !== true)
                $result .= sprintf('Failed to create Index %s '."<br>$out<br>", $data['table'] . " Index" ) ;
        }
        
        return $result;
    }
   
    function createIndex($index_name, $table_name, $unique = false, $columns = array()) 
    {
        $query = '';
        $column_name = array();
        $type = ($unique) ? 'UNIQUE' : '' ; 
        foreach ($columns as $column)
            $column_name[] = "" . $column . "" ;
        
        $column_names = implode(', ', $column_name);
        $query = "CREATE " . $type . " INDEX " .  $index_name . " ON " . $this->prefix_table .$table_name . " (" . $column_names . ')' ;
        
         if(!$this->db_exec($query)){
            return $query; 
         }
         
        return true;
    }
    
    public function insert($table, $data, $time = NULL)
    {
        if (!$data)
            return false;
        
        // Check if $data is a list of row
        $current = current($data);
        if (!is_array($current) || isset($current['type']))
            $data = array($data);

        $keys = array();
        $values_stringified = array();
        foreach ($data as $row_data)
        {
            $values = array();
            foreach ($row_data as $key => $value)
            {
                if (isset($keys_stringified))
                {
                    // Check if row array mapping are the same
                    if (!in_array("$key", $keys))
                         return false;
                }
                else
                    $keys[] = "$key";
                        
                if($value === '' || is_null($value))
                    $value = 'NULL';
                else if(is_int($value))
                    $value = (int)$value;
                else if(is_float($value))
                    $value = (float)$value;
                else
                    $value = "'{$value}'";
                 
                $values[] = $value;
            } 
            
            if(isset($time) && !empty($time) && $time != NULL)
            {
                $keys[] = "date_add";
                $values[] = "'" . $time . "'";
            }
            
            $keys_stringified = implode(', ', $keys);
            $values_stringified[] = '('.implode(', ', $values).')';
        }
        
        $sql = 'REPLACE INTO '.$this->prefix_table .$table.' ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified) . '; ';
         
        return (string)$sql;
    }
    
    public function delete($table, $time, $id_shop)
    {
        $data = true;
        
        if (!$table || !$time)
            return false;

        $query =  "DELETE FROM " . $this->prefix_table .$table . " WHERE date_add < '" . $time . "' AND id_shop = '" . $id_shop . "' ; ";
        
        if(!$this->db_exec($query))
            return FALSE;
        
        return $data;
    }
    
    public function update($table, $data, $where = array(), $limit = 0)
    {
        if (!$data)
            return false;

        $sql = 'UPDATE '.$this->prefix_table .$table.' SET ';
        
        foreach ($data as $key => $value)
             $sql .= ($value === '' || is_null($value)) ? "$key = NULL," : "$key = '{$value}',";

        $sql = rtrim($sql, ',');
        
        if (!empty($where))
        {
            $sql .= ' WHERE ';
            
            $sql_where = array();
            foreach ($where as $key => $value)
            {
                
                $sql_where[] = $key . ' ' . $value['operation'] . ' ' . $value['value'];
            }
            
            $sql .= implode(' AND ', $sql_where);
        }
        
        if ($limit)
                $sql .= ' LIMIT '.(int)$limit;
        
        return (string)$sql . '; ';
    }
        
    public function add($query_string,$transaction=true,$multi_query=true)
    { 
        
        if(isset($query_string) && !empty($query_string))
            if(!$this->db_exec($query_string,$transaction,$multi_query))
                return false;
         
        return true;
    }
    
    public static function truncate_database($user)
    {
        $oldmask = umask(0); 
        $conn_id = new Db($this->user,ObjectModel::$database  );
        $sql = '';
        
        if(!$conn_id)
            return false;

        $exec = $conn_id->db_query_str("show tables;");
         foreach ($exec as $table)
            $sql .= "DELETE FROM " . $this->prefix_table .reset($table) . "; ";
        $conn_id->db_exec('BEGIN');
        
        if(!$conn_id->db_exec($sql))
            return false;
        
        $conn_id->db_exec('COMMIT');
        
        $token = new OTP($user);
        if(!$token->generate_token())
            return false;
        
        return true;
    }
    
    public function truncate_table($table, $where = null)
    {
        if(!$this->db_exec("DELETE FROM " . $this->prefix_table .$table . " " . isset($where)? $where : '' . "; "))
            return false;
        
        return true;
    }
    
    public function db_connect($location) 
    { 
        $this->conn_id = new Db($this->user,ObjectModel::$database,false); 
    } 

    public function db_exec($query,$transaction=false,$multi_query=true) 
    { 
        $error = '';
        if(!$query || empty($query) || !isset($query))
            return FALSE;

        $exec = $this->conn_id->db_exec($query,$transaction,$multi_query);
        if(!$exec)
            return $error;
            
        return $exec;
    } 
    
    public function db_query($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
        $exec = $this->conn_id->db_sqlit_query($query);

        return $exec;
    }
    
    public function db_query_str($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
        $exec = $this->conn_id->db_query_str($query);

        return $exec;
    }
    
    public function db_num_rows($result) 
    {
        if(!isset($result) || empty($result) || !$result)
            return FALSE;
        
        $exec = $this->conn_id->db_num_rows($result);
        
        return $exec;
    }
    
    public function db_last_insert_rowid() 
    {
        return $this->conn_id->db_last_insert_rowid();
    }
    
    public function db_close() 
    { 
        return $this->conn_id->db_close();
    }
    
    public function db_trans_begin()
    {
        $this->conn_id->db_exec('BEGIN');
        return TRUE;
    }
    
    public function db_trans_commit()
    {
        $this->conn_id->db_exec('COMMIT');
        return TRUE;
    }
    
    public static function escape_str($str)
    {
         if (is_array($str))
         {
             foreach ($str as $key => $val)
                 $str[$key] = $this->escape_str($val);
             return $str;
         }

         $str = Db::escape_string($str);
         return $str;
    }
    
    public static function append_simplexml($simplexml_to, $simplexml_from)
    {
        foreach ($simplexml_from->children() as $simplexml_child)
        {
            $simplexml_temp = $simplexml_to->addChild($simplexml_child->getName(), (string) $simplexml_child);
            foreach ($simplexml_child->attributes() as $attr_key => $attr_value)
            {
                $simplexml_temp->addAttribute($attr_key, $attr_value);
            }

            ObjectModel::append_simplexml($simplexml_temp, $simplexml_child);
        }
    }
    
}
