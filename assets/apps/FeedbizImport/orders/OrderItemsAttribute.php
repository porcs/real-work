<?php

class OrderItemsAttribute extends ObjectModel
{
    public $id_order_items;
    public $id_attribute_group;
    public $id_attribute;
    public $id_shop;
    
    public static $definition = array(
        'table'     => 'order_items_attribute',
        'fields'    => array(
            'id_order_items'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_attribute_group'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_attribute'          =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10),
        ),
    );
    
}