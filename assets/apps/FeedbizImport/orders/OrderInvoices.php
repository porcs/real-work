<?php

class OrderInvoices extends ObjectModel
{
    public $id_orders;
    public $id_invoice;
    public $total_discount_tax_excl;
    public $total_discount_tax_incl;
    public $total_paid_tax_excl;
    public $total_paid_tax_incl;
    public $total_products;
    public $total_shipping_tax_excl;
    public $total_shipping_tax_incl;
    public $note;
    public $date_add;
    
    public static $definition = array(
        'table'     => 'order_invoice',
        'primary'   => array('id_invoice'),
        'fields'    => array(
            'id_orders'                 =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_invoice'                =>  array('type' => 'INTEGER', 'required' => true, 'primary_key' => true),
	    'seller_order_id'           =>  array('type' => 'VARCHAR', 'required' => false, 'size' => 40),
            'total_discount_tax_excl'   =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
            'total_discount_tax_incl'   =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
            'total_paid_tax_excl'       =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
            'total_paid_tax_incl'       =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
            'total_products'            =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'total_shipping_tax_excl'   =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
            'total_shipping_tax_incl'   =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
            'note'                      =>  array('type' => 'text', 'required' => false),
            'date_add'                  =>  array('type' => 'datetime', 'required' => false),
            'invoice_no'                =>  array('type' => 'VARCHAR', 'size' => 64, 'required' => false),
        ),
    );
    
}