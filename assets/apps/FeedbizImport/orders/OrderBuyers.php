<?php

class OrderBuyers extends ObjectModel
{
    public $id_orders; 
    public $id_buyer; 
    public $id_buyer_ref; 
    public $site; 
    public $email; 
    public $name; 
    public $id_address_ref; 
    public $address1; 
    public $address2; 
    public $city; 
    public $district; 
    public $state_region; 
    public $country_code; 
    public $country_name; 
    public $postal_code; 
    public $phone; 
    public $security_code_token; 
    
    public static $definition = array(
        'table'     => 'order_buyer',
        'primary'   => array('id_buyer'),
        'fields'    => array(
            'id_orders'         =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_buyer'          => array('type' => 'INTEGER', 'required' => true, 'primary_key' => true),
            'id_buyer_ref'      =>  array('type' => 'varchar', 'required' => false, 'size' => 64),
            'site'              =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'email'             =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'name'              =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'id_address_ref'    =>  array('type' => 'varchar', 'required' => false, 'size' => 32),
            'address1'          =>  array('type' => 'text', 'required' => false),
            'address2'          =>  array('type' => 'text', 'required' => false, 'size' => 255),
            'city'              =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'district'          =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'state_region'      =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'country_code'      =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'country_name'      =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'postal_code'       =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'phone'             =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'security_code_token' =>  array('type' => 'varchar', 'required' => false, 'size' => 128),
            'address_name'          =>  array('type' => 'text', 'required' => false),
        ),
    );
    
}