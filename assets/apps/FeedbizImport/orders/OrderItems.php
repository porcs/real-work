<?php

class OrderItems extends ObjectModel
{
    public $id_orders;
    public $id_order_items;
    public $id_marketplace_order_item_ref;
    public $id_shop;
    public $id_product;
    public $reference;
    public $id_marketplace_product;
    public $id_product_attribute;
    public $product_name;
    public $quentity;
    public $quentity_in_stock;
    public $quentity_refunded;
    public $product_weight;
    public $product_price;
    public $shipping_price;
    public $tax_rate;
    public $unit_price_tax_incl;
    public $unit_price_tax_excl;
    public $total_price_tax_incl;
    public $total_price_tax_excl;
    public $total_shipping_price_tax_incl;
    public $total_shipping_price_tax_excl;
    public $id_condition;
    public $promotion;
    public $message;
    public $id_status;
    
    public static $definition = array(
        'table'     => 'order_items',
        'primary'   => array('id_order_items'),
        'fields'    => array(
            'id_orders'                     =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_order_items'                =>  array('type' => 'INTEGER', 'required' => true, 'primary_key' => true),
            'id_marketplace_order_item_ref' =>  array('type' => 'varchar', 'required' => true, 'size' => 32),
            'id_shop'                       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_product'                    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'reference'                     =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'id_marketplace_product'        =>  array('type' => 'varchar', 'required' => false, 'size' => 32),
            'id_product_attribute'          =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'product_name'                  =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'quantity'                      =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'quantity_in_stock'             =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'quantity_refunded'             =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'product_weight'                =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'product_price'                 =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'shipping_price'                =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'tax_rate'                      =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'unit_price_tax_incl'           =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'unit_price_tax_excl'           =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'total_price_tax_incl'          =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'total_price_tax_excl'          =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'total_shipping_price_tax_incl'     =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'total_shipping_price_tax_excl'     =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'id_condition'                  =>  array('type' => 'int', 'required' => false),
            'promotion'                     =>  array('type' => 'varchar', 'required' => false, 'size' => 32),
            'message'                       =>  array('type' => 'text', 'required' => false),
            'id_status'                     =>  array('type' => 'int', 'required' => false ),
            'id_tax'                        =>  array('type' => 'int', 'required' => false, 'size' => 10 )
        ),
    );
    
}