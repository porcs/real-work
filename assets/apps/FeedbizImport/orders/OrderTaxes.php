<?php

class OrderTaxes extends ObjectModel
{
    public $id_orders;
    public $tax_description;
    public $imposition;
    public $tax_amount;
    public $tax_on_sub_total_amount;
    public $tax_on_shipping_amount;
    
    public static $definition = array(
        'table'     => 'order_taxes',
        'fields'    => array(
            'id_orders'                 =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'tax_description'          =>  array('type' => 'text', 'required' => false),
            'imposition'                =>  array('type' => 'text', 'required' => false),
            'tax_amount'                =>  array('type' => 'float', 'required' => false, 'size' => "10,6"),
            'tax_on_sub_total_amount'   =>  array('type' => 'float', 'required' => false, 'size' => "10,6"),
            'tax_on_shipping_amount'    =>  array('type' => 'float', 'required' => false, 'size' => "10,6"),
        ),
    );
    
}