<?php

class OrderSeller extends ObjectModel
{
    public $id_seller;
    public $id_user;
    public $site;
    public $email;
    public $name;
    public $status;
    public $token;
    
    public static $definition = array(
        'table'     => 'order_seller',
        'primary'   => array('id_seller','id_user'),
        'unique'        => array('order_seller' => array('id_seller','id_user')),
        'fields'    => array(
            'id_seller' =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_user'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'site'      =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'email'     =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'name'      =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'status'    =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'token'     =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
        ),
    );
    
}