<?php

class OrderLog extends ObjectModel
{
    public static $definition = array(
        'table'     => 'log',
        'fields'    => array(
            'id_shop'           =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'id_marketplace'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'site'              =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'batch_id'          =>  array('type' => 'varchar', 'required' => false, 'size' => 32),
            'request_id'        =>  array('type' => 'text', 'required' => false, 'size' => 10 ),
            'error_code'        =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'quantity'          =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'message'           =>  array('type' => 'text'),
            'date_add'          =>  array('type' => 'datetime', 'required' => false ),
        ),
    );
}