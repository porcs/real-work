<?php

class Orders extends ObjectModel{
    
    public $id_orders; 
    public $id_marketplace_order_ref;
    public $marketplace_order_number;
    public $sales_channel;
    public $id_shop; 
    public $site; 
    public $id_marketplace; 
    public $id_lang; 
    public $order_type;
    public $order_status;
    public $payment_method; 
    public $total_discount; 
    public $total_amount;
    public $total_paid;
    public $total_shipping;
    public $order_date; 
    public $shipping_date; 
    public $latest_shipping_date;
    public $delivery_date; 
    public $latest_delivery_date;
    public $purchase_date; 
    public $affiliate_id; 
    public $commission; 
    public $ip;
    public $gift_message; 
    public $gift_amount;
    public $comment;
    public $status;
    public $date_add;
  
    public static $definition = array(
        'table'     => 'orders',
        'primary'   => array('id_orders'),
        'fields'    => array(
                            'id_orders' => array('type' => 'INTEGER', 'required' => true, 'primary_key' => true),
                            'id_marketplace_order_ref'=> array('type' => 'varchar', 'required' => true, 'size' => 64),
                            'marketplace_order_number'=> array('type' => 'varchar', 'required' => false, 'size' => 64),
                            'sales_channel' => array('type' => 'varchar', 'required' => true, 'size' => 64),
                            'id_shop' => array('type' => 'int', 'required' => true, 'size' => 10),
                            'site' => array('type' => 'varchar', 'required' => true, 'size' => 128),
                            'id_marketplace' => array('type' => 'int', 'required' => false, 'size' => 10),
                            'id_lang' => array('type' => 'int', 'required' => false, 'size' => 10),
                            'order_type'=> array('type' => 'varchar', 'required' => false, 'size' => 64),
                            'order_status'=> array('type' => 'varchar', 'required' => true, 'size' => 64),
                            'payment_method' => array('type' => 'varchar', 'required' => false, 'size' => 64),
                            'id_currency' => array('type' => 'varchar', 'required' => false, 'size' => 64),
                            'total_discount' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                            'total_amount'=> array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                            'total_paid'=> array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                            'total_shipping'=> array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                            'order_date' => array('type' => 'datetime', 'required' => false ),
                            'shipping_date' => array('type' => 'datetime', 'required' => false ),
                            'latest_shipping_date' => array('type' => 'datetime', 'required' => false ),
                            'delivery_date' => array('type' => 'datetime', 'required' => false ),
                            'latest_delivery_date' => array('type' => 'datetime', 'required' => false ),
                            'purchase_date' => array('type' => 'datetime', 'required' => false ),
                            'affiliate_id' => array('type' => 'int', 'required' => false, 'size' => 10),
                            'commission' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                            'ip'=> array('type' => 'varchar', 'required' => false, 'size' => 15 ),
                            'gift_message' => array('type' => 'text', 'required' => false),
                            'gift_amount'=> array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
                            'comment'=> array('type' => 'text', 'required' => false),
                            'status'=> array('type' => 'tinyint', 'required' => false),
                            'date_add'=> array('type' => 'datetime', 'required' => false ),
                            'shipping_status'=> array('type' => 'tinyint', 'required' => false),
                            'flag_error_email'=> array('type' => 'tinyint', 'required' => false),
                            'flag_canceled_status'=> array('type' => 'tinyint', 'required' => false),
                        ),
    );
    
    public function __construct( $user, $id_order = null )
    {
        parent::__construct( $user, (int)$id_order);
    }
    
    public function updateOrderStatus($id_order , $status){
        
        $description = array();
        $description['status'] = $status;
        $description['comment'] = '';

        $where = array(
            'id_orders'=>  array(
                'value' => $id_order,
                'operation' => '=',
            )
        );

        //update
        $sql = self::update('orders', $description, $where);
        
        if(!self::db_exec($sql))
            return false;
        
        return true;
    }
    
    public function create_mp_multichannel(){
            
	$sql =  'CREATE TABLE mp_multichannel (                 
		    id_orders int(11) NOT NULL,
		    seller_order_id VARCHAR(40),
		    id_shop int(11) NOT NULL,
		    id_country int(11) NOT NULL,
		    mp_shipping varchar(64),
		    mp_items varchar(16),
		    mp_channel varchar(16),
		    mp_channel_status varchar(32),
		    mp_order_id VARCHAR(24),
		    mp_status VARCHAR(24),
		    acknowledge_flag TINYINT,
		    date_add DATETIME,
		    PRIMARY KEY (id_orders ASC, id_shop ASC, id_country ASC)
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
	
	if(self::db_exec($sql))
	    return true;
	
       return false;
       
    }
    
}

