<?php

class OrderShippings extends ObjectModel
{
    public $id_orders;
    public $shipment_service;
    public $weight;
    public $tracking_number;
    public $shipping_services_cost;
    public $shipping_services_level;
    public $id_carrier;
    
    public static $definition = array(
        'table'     => 'order_shipping',
        'unique'        => array('order_shipping' => array('id_orders')),
        'fields'    => array(
            'id_orders'                 =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'shipment_service'          =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'weight'                    =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6'),
            'tracking_number'           =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'shipping_services_cost'    =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'shipping_services_level'   =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'id_carrier'                =>  array('type' => 'int', 'required' => false, 'size' => 10),
        ),
    );
    
}