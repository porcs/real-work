<?php

class OrderStatus extends ObjectModel
{
    public $id_status;
    public $id_lang;
    public $name;
    public $date_add;
    
    public static $definition = array(
        'table'     => 'order_status',
        'primary'   => array('id_status'),
        'fields'    => array(
            'id_status'     =>  array('type' => 'INTEGER', 'required' => true, 'primary_key' => true),
            'name'          =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'date_add'      =>  array('type' => 'datetime', 'required' => false),
        ),
    );
    
}