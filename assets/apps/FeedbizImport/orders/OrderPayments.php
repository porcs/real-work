<?php

class OrderPayments extends ObjectModel
{
    public $id_orders;
    public $id_order_items;
    public $payment_method;
    public $payment_status;
    public $id_currency;
    public $Amount;
    public $external_transaction_id;

    public static $definition = array(
        'table'     => 'order_payment',
        'primary'   => array('id_orders','id_order_items'),
        'unique'        => array('order_payment' => array('id_orders','id_order_items')),
        'fields'    => array(
            'id_orders'                 =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_order_items'            =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'payment_method'            =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'payment_status'            =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'id_currency'               =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'Amount'                    =>  array('type' => 'float', 'required' => false, 'size' => '20,6'),
            'external_transaction_id'   =>  array('type' => 'varchar', 'required' => false, 'size' => 64),
        ),
    );
    
}