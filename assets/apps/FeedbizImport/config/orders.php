<?php
if( ! ini_get('date.timezone') ){ date_default_timezone_set('UTC');} 
require_once(dirname(__FILE__) . '/config.php');
require_once(dirname(__FILE__) . '/../../../../application/libraries/db.php');
require_once(dirname(__FILE__) . '/../orders/ObjectModel.php');
require_once(dirname(__FILE__) . '/../orders/OrderLog.php');
require_once(dirname(__FILE__) . '/../orders/OrderBuyers.php');
require_once(dirname(__FILE__) . '/../orders/OrderInvoices.php');
require_once(dirname(__FILE__) . '/../orders/OrderTaxes.php');
require_once(dirname(__FILE__) . '/../orders/OrderItems.php');
require_once(dirname(__FILE__) . '/../orders/OrderItemsAttribute.php');
require_once(dirname(__FILE__) . '/../orders/OrderPayments.php');
require_once(dirname(__FILE__) . '/../orders/OrderSeller.php');
require_once(dirname(__FILE__) . '/../orders/OrderShippings.php');
require_once(dirname(__FILE__) . '/../orders/OrderStatus.php');
require_once(dirname(__FILE__) . '/../orders/Orders.php');