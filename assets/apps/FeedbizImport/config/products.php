<?php
if( ! ini_get('date.timezone') ){ date_default_timezone_set('UTC');}
require_once(dirname(__FILE__) . '/config.php');
require_once(dirname(__FILE__) . '/../../../../application/libraries/db.php');
require_once(dirname(__FILE__) . '/../products/ObjectModel.php');
require_once(dirname(__FILE__) . '/../products/Attribute.php');
require_once(dirname(__FILE__) . '/../products/AttributeGroup.php');
require_once(dirname(__FILE__) . '/../products/Carrier.php');
require_once(dirname(__FILE__) . '/../products/Category.php');
require_once(dirname(__FILE__) . '/../products/Conditions.php');
require_once(dirname(__FILE__) . '/../products/Currency.php');
require_once(dirname(__FILE__) . '/../products/Feature.php');
require_once(dirname(__FILE__) . '/../products/FeatureValue.php');
require_once(dirname(__FILE__) . '/../products/Manufacturer.php');
require_once(dirname(__FILE__) . '/../products/Language.php');
require_once(dirname(__FILE__) . '/../products/Product.php');
require_once(dirname(__FILE__) . '/../products/ProductAttribute.php');
require_once(dirname(__FILE__) . '/../products/ProductAttributeCombination.php');
require_once(dirname(__FILE__) . '/../products/ProductAttributeImage.php');
require_once(dirname(__FILE__) . '/../products/ProductAttributeUnit.php');
require_once(dirname(__FILE__) . '/../products/ProductCarrier.php');
require_once(dirname(__FILE__) . '/../products/ProductCategory.php');
require_once(dirname(__FILE__) . '/../products/ProductFeature.php');
require_once(dirname(__FILE__) . '/../products/ProductImage.php');
require_once(dirname(__FILE__) . '/../products/ProductSale.php');
require_once(dirname(__FILE__) . '/../products/ProductSupplier.php');
require_once(dirname(__FILE__) . '/../products/ProductTag.php');
require_once(dirname(__FILE__) . '/../products/Supplier.php');
require_once(dirname(__FILE__) . '/../products/Tag.php');
require_once(dirname(__FILE__) . '/../products/Tax.php');
require_once(dirname(__FILE__) . '/../products/Unit.php');
require_once(dirname(__FILE__) . '/../products/Mapping.php');
require_once(dirname(__FILE__) . '/../products/Filter.php');
require_once(dirname(__FILE__) . '/../products/ConvertXMLToFeedBizXML.php');
require_once(dirname(__FILE__) . '/../products/OTP.php');
require_once(dirname(__FILE__) . '/../products/Shop.php');
require_once(dirname(__FILE__) . '/../products/Log.php');
require_once(dirname(__FILE__) . '/../log/ImportLog.php');
require_once(dirname(__FILE__) . '/../log/RecordProcess.php');
require_once(dirname(__FILE__) . '/../products/MarketplaceProductOption.php');
require_once(dirname(__FILE__) . '/../products/ProductUrl.php'); // 2015-12-03
require_once(dirname(__FILE__) . '/../products/ProductAttributeUrl.php'); // 2015-12-03