<?php

class ConvertXMLToFeedBizXML extends ObjectModel
{
    
    public function gmerchant($data, $time)
    {   
        $Document = new DOMDocument();
        $Document->preserveWhiteSpace = true;
        $Document->formatOutput = true;
        $Document->encoding = 'utf-8';
        $Document->version = '1.0';
        $OfferPackage   = $Document->appendChild($Document->createElement('OfferPackage'));
        $Offers         = $OfferPackage->appendChild($Document->createElement('Offers'));
        $Categories     = $OfferPackage->appendChild($Document->createElement('Categories'));
        $Conditions     = $OfferPackage->appendChild($Document->createElement('Conditions'));
        $Currencies     = $OfferPackage->appendChild($Document->createElement('Currencies'));
        $Manufacturers  = $OfferPackage->appendChild($Document->createElement('Manufacturers'));
        
        $mno            = 0;
        $cno            = 1;
        $conno          = 0;
        $currno         = 0;
        
        $OfferDetails = array();
        $history = array();
        
        $time = strtotime($time);
        
        $gkey = 1;
        
        if( !empty($data->gdata->fileurl))
        {
            $get_xml = @file_get_contents($data->gdata->fileurl);
            $xml = str_replace('&acute;', '', $get_xml);          
            
            if( isset($xml) && !empty($xml) && $xml != null )
            { 
                $oXml = new SimpleXmlElement($xml);
                
                $OfferPackage->setAttribute('ShopName', $oXml->channel->title);

                $no = 0;
                
                //Currency
                $currno++;
                $Currencies->appendChild($currency_id = $Document->createElement('Currency', $data->gdata->curr_name));
                $currency_id->setAttribute('ID', $currno); 
                $currency_id->setAttribute('iso_code', $data->gdata->curr_iso);
                            
                //root category
                if(!isset($OfferDetails['Category']) || empty($OfferDetails['Category']))
                {
                    $OfferDetails['Category'] = $Categories->appendChild($category_id = $Document->createElement('Category'));
                    $category_id->setAttribute('ID', 1);
                    $category_id->setAttribute('parent', 'root');
                    $category_id->setAttribute('code', 1);
                }

                $OfferDetails['Category']->appendChild($Document->createCDATASection('Home')); 

                //Currency
                $OfferDetails['Currency'] = $Offers->appendChild($currency = $Document->createElement('Currency'));
                $currency->setAttribute('ID', $currno);

                foreach ($oXml->channel->item as $item)
                {                                           
                    $namespaces = $item->getNameSpaces(true);
                    $g = $item->children($namespaces["g"]);

                    if(isset($g->item_group_id) && !empty($g->item_group_id))
                        $id_product = str_replace(array("-","+"), "", filter_var($g->item_group_id, FILTER_SANITIZE_NUMBER_INT));
                    else
                        $id_product = str_replace(array("-","+"," ","_"), "", filter_var($g->id, FILTER_SANITIZE_NUMBER_INT));
                    
                    //Offer
                    if(!isset($OfferDetails[$id_product]['ID']) || empty($OfferDetails[$id_product]['ID']))
                    {   
                        $no++;
                        $OfferDetails[$id_product]['ID'] = $Offers->appendChild($product_no = $Document->createElement('Offer'));
                        $product_no->setAttribute('ProductIdRefference', $no);
                         
                        //Reference
                        if(isset($g->mpn) && !empty($g->mpn))
                            $product_no->setAttribute('ProductReference', trim($g->mpn));

                        if(isset($g->gtin) && !empty($g->gtin))
                            $product_no->setAttribute('ProductEan', trim($g->gtin));

                    }
                    
                    //Category
                    if(isset($g->product_type) && !empty($g->product_type))
                    {
                        foreach ($g->product_type as $cate)
                        {
                            $cate = ObjectModel::escape_str($cate);

                            if(strlen(strchr($cate,"&gt;")))
                                $categories = explode(" &gt; ", $cate);
                            else
                                $categories = explode(" > ", $cate);

                            foreach ($categories as $ckey => $category)
                            {
                                $category = trim($category);

                                if(!isset($history['Category'][$category][$gkey]) || !$history['Category'][$category][$gkey])
                                {
                                    $cno++;

                                    if((int)$ckey > 0)
                                    {
                                        $ckey_no = intval($ckey)-1;
                                        $parent  = array_search($categories[$ckey_no], $history['Category']['name']);
                                    }
                                    else 
                                        $parent = 1;

                                    if(!isset($history['Category'][$category][$gkey]) || empty($history['Category'][$category][$gkey]))
                                    {
                                        $OfferDetails[$id_product][$ckey]['Category'] = $Categories->appendChild($category_id = $Document->createElement('Category'));
                                        $category_id->setAttribute('ID', $cno);
                                        $category_id->setAttribute('parent', $parent);
                                        $category_id->setAttribute('code', $time);
                                        $history['Category']['name'][$cno] = $category;
                                    }

                                    $OfferDetails[$id_product][$ckey]['Category']->appendChild($Document->createCDATASection($category)); 

                                    $history['Category']['number'][$ckey] = $cno;
                                }
                                $history['Category'][$category][$gkey] = true;
                            }
                        }

                        //product
                        if(!isset($OfferDetails[$id_product]['Category']) || empty($OfferDetails[$id_product]['Category']))
                        {
                            $category = array_search(trim($categories[(sizeof($categories)-1)]), $history['Category']['name']);
                            $product_no->setAttribute('DefaultCategory', $category);
                        }
                    }
                    
                    //Price 
                    $price = $sale_price = 0;
                    $Discount = array();
                    
                    if(isset($g->price) && !empty($g->price))
                    {   
                        //BasePrice
                        $price = filter_var($g->price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION); 
                        
                        if(isset($g->item_group_id) && !empty($g->item_group_id))
                            $product_no->setAttribute('BasePrice', 0);
                        else
                            $product_no->setAttribute('BasePrice', $price);
                    }
                    
                    //DiscountList
                    if(isset($g->sale_price) && !empty($g->sale_price))
                    {
                        $sale_price = filter_var($g->sale_price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION); 

                        if($sale_price < $price)
                            $Discount = round( ($price -$sale_price) , 2);
                        
                        if($sale_price && $Discount)
                        {
                            $percent = round(($Discount/$price)*100);

                            if(!isset($OfferDetails[$id_product]['Discount']) || empty($OfferDetails[$id_product]['Discount']))
                            {
                                $OfferDetails[$id_product]['Discount'] = $OfferDetails[$id_product]['ID']->appendChild($Document->createElement('DiscountList'));

                                $OfferDetails[$id_product]['Discount']->appendChild($discount = $Document->createElement('Discount'));
                                $discount->setAttribute('DiscountValue', $percent);
                                $discount->setAttribute('DiscountType', 'percentage');
                            }
                        }

                        if(isset($g->sale_price_effective_date) && !empty($g->sale_price_effective_date))
                        {
                            $sale_date_range = explode("/", $g->sale_price_effective_date);
                            $startDate = date('c', strtotime($sale_date_range[0])) ;  
                            $toDate = date('c', strtotime($sale_date_range[1])) ;
                            
                            $discount->setAttribute('StartDate', $startDate);
                            $discount->setAttribute('EndDate', $toDate);
                        }
                    }
                    
                    //Manufacturer
                    if( isset($g->brand) && !empty($g->brand) && $g->brand != "")
                    {
                        
                        $manufacturer = (string)$g->brand;
                        
                        if(!isset($OfferDetails[$id_product]['Manufacturer']) || empty($OfferDetails[$id_product]['Manufacturer']))
                        {
                            if(!isset($history['manufactories'][$manufacturer]))
                            {
                                $mno++;
                                $OfferDetails[$id_product]['Manufacturer'] = $Manufacturers->appendChild($Manufacturer = $Document->createElement('Manufacturer'));
                                $OfferDetails[$id_product]['Manufacturer']->appendChild($Document->createCDATASection($g->brand)); 
                                $Manufacturer->setAttribute('ID', $mno); 
                                $Manufacturer->setAttribute('code', $time); 

                                $history['manufactory'][$mno] = $manufacturer;
                            }

                            $history['manufactories'][$manufacturer] = TRUE;
                        }
                        
                        //Product Manufacturer
                        $product_no->setAttribute('Manufacturer', array_search($manufacturer, $history['manufactory'])); 
                    }
                    
                    //Condition
                    if(!isset($OfferDetails[$id_product]['Condition']) || empty($OfferDetails[$id_product]['Condition']))
                    {
                        if(isset($g->condition) && !empty($g->condition))
                        {
                            $condition = str_replace("'", " ", trim($g->condition));   
                            if(!isset($history['conditions'][$condition]) || empty($history['conditions'][$condition]))
                            {
                                $conno++;
                                $OfferDetails[$id_product]['Condition'] = $Conditions->appendChild($cond_new_id = $Document->createElement('Condition'));
                                $OfferDetails[$id_product]['Condition']->appendChild($Document->createCDATASection($condition )); 
                                $cond_new_id->setAttribute('ID', $conno);

                                $history['condition'][$conno] = $condition;   
                            }

                            $product_no->setAttribute('ProductCondition', array_search($condition, $history['condition']));

                            $history['conditions'][$condition] = true;                               
                        }
                    }
                    
                    //ShippingPrice
                    if(isset($g->shipping->price) && !empty($g->shipping->price))
                    {
                        $shipping_price = filter_var($g->shipping->price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION); 
                        $product_no->setAttribute('ShippingPrice', $shipping_price);
                    }
                    
                    //ProductCondition
                    
                    if(isset($g->item_group_id) && !empty($g->item_group_id))
                    {
                        $id_item = str_replace(array("-","+"," ","_"), "", filter_var($g->id, FILTER_SANITIZE_NUMBER_INT));
                        
                        if(!isset($OfferDetails[$id_product]['Items']) || empty($OfferDetails[$id_product]['Items']))
                            $OfferDetails[$id_product]['Items'] = $OfferDetails[$id_product]['ID']->appendChild($Document->createElement('Items'));

                        $OfferDetails[$id_product]['Items']->appendChild( $item  = $Document->createElement('Item'));
                        $item->setAttribute('AttributeIdRefference', $id_item);
                        
                        //Reference
                        if(isset($g->mpn) && !empty($g->mpn))
                            $item->setAttribute('ProductReference', trim($g->mpn));

                        if(isset($g->gtin) && !empty($g->gtin))
                            $item->setAttribute('ProductEan', trim($g->gtin));
                        
                        $item->setAttribute('Stock', $g->quantity);
                        $item->setAttribute('AdditionalPrice', round($price, 2) );
                        $item->setAttribute('SalesReferencePrice', round($sale_price, 2) );
                       
                    }
                    
                } //end foreach ($oXml->channel->item as $item

            }
            else
                return sprintf('can_not_load_data') ;
        }
        else 
            return sprintf('incorrect_urls') ;
        
        if (isset($OfferDetails) && count($OfferDetails) > 0)
            return $Document->saveXML();
       
        return FALSE;
    }
       
    public function opencart($url,$setting_url, $token)
    {
        $return = array();
         
        $xmlstr = @file_get_contents($url );
        if( !isset($xmlstr) || empty($xmlstr) )
            return array('message' => sprintf('can_not_load_data'), 'error' => true);
        
        $oxml = simplexml_load_string($xmlstr);
        $status = (int)$oxml->Status->Code;
        
        if( !isset($status) || $status == 0 )
            return array('message' => $oxml->Status->Message, 'error' => true);  
         
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('empty_data'), 'error' => true);       
                
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('dom_error'), 'error' => true);
        
        if(isset($oxml->Offers))
            $odxmlPRD = dom_import_simplexml($oxml->Offers);

        while($status != 1)
        {
            $xmlstr = @file_get_contents($url );
            if( !isset($_xmlstr) || empty($_xmlstr) ){break;}
            $_oxml = simplexml_load_string($_xmlstr);
            $_xml = simplexml_load_string($_xmlstr);
            if( !isset($_oxml) || empty($_oxml) ){break;}
            
            $status = (int)$_oxml->Status->Code;
            
            if(isset($_xml->Offers))
                foreach($_xml->Offers->Offer as $offer)
                {
                    $odxmlChildPRD = dom_import_simplexml($offer);
                    $odxmlChildPRD = $odxmlPRD->ownerDocument->importNode($odxmlChildPRD, true);
                    $odxmlPRD->appendChild($odxmlChildPRD);
                }
        }
        
        //Setting
        $xmlsetting = @file_get_contents($setting_url );
        $setting = simplexml_load_string($xmlsetting);
        if( !isset($xmlsetting) || empty($xmlsetting) ){return array('message' => sprintf('can_not_load_setting_data'), 'error' => true);}
        if( !isset($setting) || empty($setting) ){return array('message' => sprintf('setting_dom_error'), 'error' => true);}
        
        if(isset($setting))
        {
            $odxml = dom_import_simplexml($oxml);
            foreach($setting  as $set_key => $set)
            {
                $odxmlSetting = dom_import_simplexml($set);
                $odxmlSetting = $odxml->ownerDocument->importNode($odxmlSetting, true);
                $odxml->appendChild($odxmlSetting);
            }
        }
        
        unset($oxml->Status);
        
        $return['xml'] = $oxml->saveXML();
        
        return $return;
    }
    
    public function prestashop($url,$setting_url, $token)
    {
        $return = array();
         
        $xmlstr = @file_get_contents($url );
        if( !isset($xmlstr) || empty($xmlstr) )
            return array('message' => sprintf('can_not_load_data'), 'error' => true);
        
        $oxml = simplexml_load_string($xmlstr);
        $status = (int)$oxml->Status->Code;
        
        if( !isset($status) || $status == 0 )
            return array('message' => $oxml->Status->Message, 'error' => true);  
         
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('empty_data'), 'error' => true);       
                
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('dom_error'), 'error' => true);
        
        if(isset($oxml->Offers))
            $odxmlPRD = dom_import_simplexml($oxml->Offers);

        while($status != 1)
        {
            $xmlstr = @file_get_contents($url );
            if( !isset($_xmlstr) || empty($_xmlstr) ){break;}
            $_oxml = simplexml_load_string($_xmlstr);
            $_xml = simplexml_load_string($_xmlstr);
            if( !isset($_oxml) || empty($_oxml) ){break;}
            
            $status = (int)$_oxml->Status->Code;
            
            if(isset($_xml->Offers))
                foreach($_xml->Offers->Offer as $offer)
                {
                    $odxmlChildPRD = dom_import_simplexml($offer);
                    $odxmlChildPRD = $odxmlPRD->ownerDocument->importNode($odxmlChildPRD, true);
                    $odxmlPRD->appendChild($odxmlChildPRD);
                }
        }
        
        //Setting
        $xmlsetting = @file_get_contents($setting_url );
        $setting = simplexml_load_string($xmlsetting);
        if( !isset($xmlsetting) || empty($xmlsetting) ){return array('message' => sprintf('can_not_load_setting_data'), 'error' => true);}
        if( !isset($setting) || empty($setting) ){return array('message' => sprintf('setting_dom_error'), 'error' => true);}
        
        if(isset($setting))
        {
            $odxml = dom_import_simplexml($oxml);
            foreach($setting  as $set_key => $set)
            {
                $odxmlSetting = dom_import_simplexml($set);
                $odxmlSetting = $odxml->ownerDocument->importNode($odxmlSetting, true);
                $odxml->appendChild($odxmlSetting);
            }
        }
        
        unset($oxml->Status);
        
        $return['xml'] = $oxml->saveXML();
        
        return $return;
    }
    
   
    public function getSingleContent($url ,$user){
        
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'timeout' => 600,   
            )
        ));
        
        $start_time = date('c');
//        $xmlstr = @file_get_contents($url,false,$context); 
//        $xmlstr = @$this->curl_get_contents($url); 
        if(function_exists('fbiz_get_contents')){
            $xmlstr = @fbiz_get_contents($url,  array('curl_method'=>1,'timeout'=>600));
        }else{
            $xmlstr = @file_get_contents($url,false,$context); 
        } 
//        $xmlstr=str_replace('Exception Caught: String could not be parsed as XML','',$xmlstr);
         
        if( !isset($xmlstr) || empty($xmlstr) ){
            $err_dir = dirname(__FILE__).'/../../users/'.$user.'/xml_err/';
            if(!file_exists($err_dir)){
                mkdir($err_dir,0777,true);
            }
            $return=array();
            $return['start_load'] = $start_time;
            $return['end_load'] = date('c');
            $return['xml'] = 'can_not_load_data'; 
            $fname = 'can_not_data_offer_'.date('Y_m_j').'.txt';
            $file_err_path = $err_dir.$fname;
            file_put_contents($file_err_path,print_r($return,true));
            return array('message' => sprintf('can_not_load_data'), 'error' => true);
        }
        
        $oxml = simplexml_load_string($xmlstr);
        
        $status = (int)$oxml->Status->Code;
        
        if(  !($oxml) ){
            $err_dir = dirname(__FILE__).'/../../users/'.$user.'/xml_err/';
            if(!file_exists($err_dir)){
                mkdir($err_dir,0777,true);
            }
            $return=array();
            $return['start_load'] = $start_time;
            $return['end_load'] = date('c');
            $return['xml'] = $xmlstr; 
            $fname = 'data_offer_'.date('Y_m_j').'.txt';
            $file_err_path = $err_dir.$fname;
            file_put_contents($file_err_path,print_r($return,true));
            return array('message' => sprintf('empty_data'), 'error' => true);       
        }
        if( !isset($status) || $status == '03' || $status == 0 )
            return array('message' => $oxml->Status->Message, 'error' => true);  
        
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('empty_data'), 'error' => true);       
                
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('dom_error'), 'error' => true);
          
         $return['status'] = (int)$oxml->Status->Code;
         $return['total'] = (int)$oxml->Status->ExportTotal;
         $return['current'] = (int)$oxml->Status->CurrentProductID;
         $return['min'] = (int)$oxml->Status->MinProductID;
         $return['max'] = (int)$oxml->Status->MaxProductID;
         $return['start_load'] = $start_time;
         $return['end_load'] = date('c');
         $return['xml'] = $oxml->saveXML(); 
         if($return['total']==0){
             $return['status'] = 1;
         }
          
        $save_path = dirname(__FILE__).'/../../users/'.$user.'/xml_log/';
        if(!file_exists($save_path)){
            mkdir($save_path, 0777, true);
        }
        if($user=='u00000000000026' && !isset($this->file_log_name)){
            $file_name = 'data_offer_'.date('Y_m_j_H_i').'.xml';
            $this->file_log_name = $file_name;
        }elseif($user=='u00000000000026' && isset($this->file_log_name)){
            $file_name = $this->file_log_name ;
        }else{
            $file_name = 'data_offer.xml';
        }
        $fp = fopen($save_path.$file_name, 'a');
        if($fp){
        fwrite($fp, print_r($return,true));
        fclose($fp); 
        }
        
         return $return;
    }
    
    public function getSetting($url ,$user){
         
        $return = array();
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'timeout' => 10000,   
            )
        ));
        
        $start_time = date('c'); 
        if(function_exists('fbiz_get_contents')){
            $xmlsetting = @fbiz_get_contents($url,  array('curl_method'=>1,'timeout'=>10000));
        }else{
            $xmlsetting = @file_get_contents($url,false,$context); 
        }
        
        if(!empty($xmlsetting)){
//            $xmlsetting=str_replace('Exception Caught: String could not be parsed as XML','',$xmlsetting);
            $xmlsetting=trim(str_replace('CURL error:(3)<url> malformed','', $xmlsetting));
        }
        $setting = simplexml_load_string($xmlsetting);
        if( !isset($xmlsetting) || empty($xmlsetting) ){return array('message' => sprintf('can_not_load_setting_data'), 'error' => true);}
        if( !isset($setting) || empty($setting) ){return array('message' => sprintf('setting_dom_error'), 'error' => true);}
        
        
        $return['xml'] = $setting->saveXML(); 
        $save_path = dirname(__FILE__).'/../../users/'.$user.'/xml_log/';
        if(!file_exists($save_path)){
            mkdir($save_path, 0777, true);
        }
        if(file_exists($save_path.'data_setting.xml'))
        unlink($save_path.'data_setting.xml');
        if(file_exists($save_path.'data_offer.xml'))
        unlink($save_path.'data_offer.xml');
        $fp = fopen($save_path.'data_setting.xml', 'a');
        if($fp){
            fwrite($fp, print_r($return,true));
            fclose($fp); 
        }
        return $return;
    }
    
    public function saveLanguage($language, $time)
    {
        if(!isset($language) || empty($language))
            return FALSE;
    
        $sql = self::insert('language', $language, $time);
        
        if(!self::add($sql))
            return FALSE;
        
        return TRUE;
        
    }
    
    public function curl_get_contents($url,array $post_data=array(),$verbose=false,$ref_url=false,$cookie_location=false,$return_transfer=true)
    {
	$return_val = false;
 
	$pointer = curl_init();
 
	curl_setopt($pointer, CURLOPT_URL, $url);
	curl_setopt($pointer, CURLOPT_TIMEOUT, 40);
	curl_setopt($pointer, CURLOPT_RETURNTRANSFER, $return_transfer);
	curl_setopt($pointer, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.28 Safari/534.10");
	curl_setopt($pointer, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($pointer, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($pointer, CURLOPT_HEADER, false);
	curl_setopt($pointer, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($pointer, CURLOPT_AUTOREFERER, true);
 
	if($cookie_location !== false)
	{
		curl_setopt($pointer, CURLOPT_COOKIEJAR, $cookie_location);
		curl_setopt($pointer, CURLOPT_COOKIEFILE, $cookie_location);
		curl_setopt($pointer, CURLOPT_COOKIE, session_name() . '=' . session_id());
	}
 
	if($verbose !== false)
	{
		$verbose_pointer = fopen($verbose,'w');
		curl_setopt($pointer, CURLOPT_VERBOSE, true);
		curl_setopt($pointer, CURLOPT_STDERR, $verbose_pointer);
	}
 
	if($ref_url !== false)
	{
	    curl_setopt($pointer, CURLOPT_REFERER, $ref_url);
	}
 
	if(count($post_data) > 0)
	{
	    curl_setopt($pointer, CURLOPT_POST, true);
	    curl_setopt($pointer, CURLOPT_POSTFIELDS, $post_data);
	}
 
	$return_val = curl_exec($pointer);
 
	$http_code = curl_getinfo($pointer, CURLINFO_HTTP_CODE);
 
	if($http_code == 404)
	{
		return false;
	}
 
	curl_close($pointer);
 
	unset($pointer);
 
	return $return_val;
    }
}
