<?php

class Price extends ObjectModel
{
    public static $definition = array(
        'table'     => 'profile_price',
        'unique'    => array('profile_price' => array('id_profile_price', 'id_shop')),
        'fields'    => array(
            'id_profile_price'  =>  array('type' => 'INTEGER', 'required' => false, 'primary_key' => true),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_mode'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'              =>  array('type' => 'text', 'required' => true ),
            'price_percentage'  =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'price_value'       =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'rounding'          =>  array('type' => 'int', 'required' => false, 'size' => '1' ),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
}