<?php

class Supplier extends ObjectModel
{   
    public static $definition = array(
        'table'     => 'supplier',
        'unique'    => array('supplier' => array('id_supplier', 'id_shop')),
        'fields'    => array(
            'id_supplier'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 64 ),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
    /* $return Query (string) */
    public function importData($data, $time)
    {        
        //Supplier
        if(isset($data) && !empty($data))
        {
            foreach($data as $SuppliersXml)
            {
                $sql = '';
                $id_shop = $SuppliersXml->id_shop;
                $supplier = array();
                
                foreach ($SuppliersXml->Supplier as $Suppliers)
                {
                    $supplier['id_supplier'] = (int)ObjectModel::escape_str($Suppliers['ID']);
                    $supplier['name'] = ObjectModel::escape_str($Suppliers);
                    $supplier['id_shop'] = (int)$id_shop;

                    $sql .= self::insert('supplier', $supplier, $time);
                   
                }
            }

            return $sql;
        }
        
        return '';
    }
    
    
}