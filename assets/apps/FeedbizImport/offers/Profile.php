<?php

class Profile extends ObjectModel
{
    public static $definition = array(
        'table'     => 'profile',
        'unique'    => array('profile' => array('id_profile', 'id_shop')),
        'fields'    => array(
            'id_profile'        =>  array('type' => 'INTEGER', 'required' => false, 'primary_key' => true),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_mode'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'is_default'        =>  array('type' => 'int', 'required' => true, 'size' => 1),
            'name'              =>  array('type' => 'text', 'required' => true ),
            'id_profile_price'  =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    public function createTableItems() 
    {
        $query = "CREATE TABLE  {$this->prefix_table}profile_item ( id_profile_item INTEGER PRIMARY KEY auto_increment , id_profile int(10), id_rule int(10), id_shop int(10), id_mode int(10)); " ;
            
        if(!$this->db_exec($query,false))
            return false;
        
        return true;
    }
}