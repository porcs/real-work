<?php

class Manufacturer extends ObjectModel
{
    public static $definition = array(
        'table'     => 'manufacturer',
        'unique'    => array('manufacturer' => array('id_manufacturer', 'id_shop')),
        'fields'    => array(
            'id_manufacturer'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'tmpcode'           =>  array('type' => 'int', 'required' => false, 'size' => 11),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    /*$return Query (string)*/
    public function importData($data, $time)
    {
        //Manufacturer
        foreach($data as $ManufacturersXml)
        {
            $sql = '';
            $id_shop = $ManufacturersXml->id_shop;
            $manufacturer = array();
            
            foreach ($ManufacturersXml->Manufacturer as $Manufacturers)
            {
                $manufacturer['id_manufacturer'] = (int)ObjectModel::escape_str($Manufacturers['ID']);
                $manufacturer['name'] = empty($Manufacturers)?'':ObjectModel::escape_str($Manufacturers);
                $manufacturer['id_shop'] = (int)$id_shop;
                
                $sql .= self::insert('manufacturer', $manufacturer, $time);
                                                
            }
        }
        
        return $sql;
    }
    
}