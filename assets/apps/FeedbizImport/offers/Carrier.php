<?php

class Carrier extends ObjectModel
{   
    public static $definition = array(
        'table'     => 'carrier',
        'primary'   => array('id_carrier', 'id_shop'),
        'unique'    => array('carrier' => array('id_carrier', 'id_shop')),
        'fields'    => array(
            'id_carrier'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_carrier_ref'    =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_tax'        =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
            'is_default'    =>  array('type' => 'int', 'required' => true, 'size' => 1),
        ),
    );
    
    /* $return Query (string) */
    public function importData($data, $time)
    {
        //Carrier
        if(isset($data) && !empty($data))
        {
            foreach($data as $CarriersXml)
            {
                $sql = '';
                $id_shop = $CarriersXml->id_shop;
                $carrier = array();
                
                foreach ($CarriersXml->Carrier as $Carriers)
                {
                    $carrier['id_carrier'] = (int)$Carriers['ID'];
                    $carrier['id_carrier_ref'] = isset($Carriers['ID_ref']) && !empty($Carriers['ID_ref']) ? (int)$Carriers['ID_ref'] : (int)$Carriers['ID'];
                    $carrier['id_shop'] = (int)$id_shop;
                    $carrier['name'] = ObjectModel::escape_str($Carriers->Name);
                    $carrier['is_default'] = isset($Carriers['Default']) ? intval($Carriers['Default']) : 0;
                    if(empty($carrier['name'])){continue;}
                    foreach ($Carriers->Tax as $carrier_tax_value)
                        $carrier['id_tax'] = (int)$carrier_tax_value['ID'];

                    $sql .= self::insert('carrier', $carrier, $time);
                }
            }
            return $sql;
        }
        
        return '';
    }
   
}