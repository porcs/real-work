<?php

class Shop extends ObjectModel
{
    public function __construct( $user, $id = null, $connect = true)
    {
        parent::__construct( $user, $id , $connect );  
        $this->user = $user;
        
    }
    
    public static $definition = array(
        'table'     => 'shop',
        'primary' => array('id_shop'),
        'fields'    => array(
            'id_shop'       =>  array('type' => 'INTEGER', 'required' => true),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 64 ),
            'is_default'    =>  array('type' => 'int', 'required' => false, 'size' => 1),
            'active'        =>  array('type' => 'int', 'required' => false, 'size' => 1),
            'description'   =>  array('type' => 'text', 'required' => false),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
  
    public function importData($shopname, $time, $details = NULL)
    {        
        $id_shop = null;
        
        if(isset($shopname) && !empty($shopname))
        {
            $shopname = ObjectModel::escape_str($shopname);
            $id_shop = $this->checkShopProducts($shopname);
            
            if( !isset($id_shop) || empty($id_shop) || $id_shop <= 0)
                return false;
            
          
            if($this->checkShopName($shopname) == 0)
            {
                $shop['id_shop']     = $id_shop;
                $shop['name']        = $shopname;
                $shop['active']      = 1;
                $shop['is_default']  = 1;
               
                if(isset($details) && !empty($details))
                    $shop['description']    = $details;
               
                $sql = self::insert(self::$definition['table'], $shop, $time);
                self::add($sql,false,false);
            }
            else 
            {
                $result = self::db_query_str("SELECT id_shop FROM {$this->prefix_table}shop WHERE name = '" . $shopname . "' ;");
            
                if($result)
                    foreach ($result as $shop)
                        $id_shop = $shop['id_shop'];
            }
            
            if(isset($id_shop) && !empty($id_shop))
                if($this->updateShopDefault($id_shop, 'products',$time, $details))
                    if($this->updateShopDefault($id_shop, 'offers',$time, $details))
                        return $id_shop;
        }
        
        return false;
        
    }
    
    public function checkShopName($shopname)
    { 
        if(isset($shopname) && !empty($shopname))
        {
            $sql = self::db_query("SELECT id_shop FROM {$this->prefix_table}shop WHERE name = '" . $shopname . "' ;");
            
            if($sql)
                return self::db_num_rows($sql);
            else
                return false;
        }
        
        return false;
    }
    
    public function checkShopProducts($shopname)
    { 
        
        if(isset($shopname) && !empty($shopname))
        {
            $db = new Db($this->user, 'products');
            $sql = $db->db_query_str("SELECT id_shop FROM products_shop WHERE name = '" . $shopname . "' ;");
            
            if($sql)
                foreach ($sql as $shop)
                    return $shop['id_shop'];
            else
                return false;
        }
        
        return false;
    }
    
    public function updateShopDefault($id_shop, $database, $time, $details = null)
    { 
        $return = false;
        if(isset($id_shop) && !empty($id_shop))
        {
            $db = new Db($this->user, $database);
            
             if(isset($details) && !empty($details))
                $shop_data['description']    = $details;
             
            $shop_data['is_default']  = 1;
            $shop_data['date_add']  = $time;
            $shop_where['id_shop'] = array('operation' => '=', 'value' => $id_shop);

            $shop_sql = self::update(self::$definition['table'], $shop_data, $shop_where);
            
            if($db->db_exec($shop_sql))
            {
                $updade_data['is_default']  = 0;
                $updade_where['id_shop'] = array('operation' => '!=', 'value' => $id_shop);
                $updade_sql = self::update(self::$definition['table'], $updade_data, $updade_where);
                
                if($db->db_exec($updade_sql)){
                    $return = true;
                }
            }
                
        }
        
        return $return;
    }
}