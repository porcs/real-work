<?php

class Product extends ObjectModel {

    private $offerUpdated_offerDB = array();
    private $prefix_products_database = 'products_';

    public function __construct($user, $id = null, $connect = true) {	
        parent::__construct($user, $id, $connect);
        $this->user = $user;
        $this->prod_conn_id = new Db($this->user, 'products');
        $this->id = $id;
        $this->connect = $connect;
    }

    public static $definition = array(
        'table' => 'product',
        'unique' => array('product' => array('id_product', 'id_shop')),
        'primary'   => array('id_product', 'id_shop'),
        'fields' => array(
            'id_product' => array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop' => array('type' => 'int', 'required' => true, 'size' => 10),
            'id_supplier' => array('type' => 'int', 'required' => false, 'size' => 10),
            'id_manufacturer' => array('type' => 'int', 'required' => false, 'size' => 10),
            'id_carrier_default' => array('type' => 'int', 'required' => false, 'size' => 10),
            'id_category_default' => array('type' => 'int', 'required' => false, 'size' => 10),
            'on_sale' => array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0'),
            'reference' => array('type' => 'varchar', 'required' => false, 'size' => 64),
            'sku' => array('type' => 'varchar', 'required' => false, 'size' => 64),
            'ean13' => array('type' => 'varchar', 'required' => false, 'size' => 13),
            'upc' => array('type' => 'varchar', 'required' => false, 'size' => 12),
            'ecotax' => array('type' => 'decimal', 'required' => false, 'size' => '17,6', 'default' => '0.000000'),
            'quantity' => array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0'),
            'wholesale_price' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'price' => array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000'),
            'id_currency' => array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0'),
            'id_condition' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'id_tax' => array('type' => 'int', 'required' => false, 'size' => 10),
            'shipping_cost' => array('type' => 'int', 'required' => false, 'size' => 10),
            'has_attribute' => array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0'),
            'date_add' => array('type' => 'datetime', 'required' => true),
            'date_upd' => array('type' => 'datetime', 'required' => false),
        ),
        'table_link' => array(
            '0' => array('name' => 'product_attribute', 'lang' => false),
            '1' => array('name' => 'product_sale', 'lang' => false),
        ),
    );

    public function updateMarketplaceFlag($id_product, $id_shop) {

        $query = "UPDATE {$this->prefix_table}flag_update SET flag = 1 WHERE id_product = '" . (int) $id_product . "' AND id_shop = '" . (int) $id_shop . "'; 
                  INSERT INTO {$this->prefix_table}flag_update_log (id_shop, id_product, action_type, flag, date_add) 
                  VALUES ('" . (int) $id_shop . "', '" . (int) $id_product . "', 'import_offer', 1, '" . date('Y-m-d H:i:s') . "'); ";
        return $query;
    }

    public function checkOfferUpdated($id_product, $id_shop, $last_update) {

        $query = "SELECT id_product FROM {$this->prefix_table}product "
        . "WHERE id_product = " . (int) $id_product . " and id_shop = " . (int) $id_shop . " and date_upd >= '$last_update' limit 1 ; ";
        $exec = $this->conn_id->db_sqlit_query($query);
        $row = $this->conn_id->db_num_rows($exec);
        return $row;
    }

    public function getOffersUpdated($id_shop, $id_product_from, $id_product_to) {
        
        $dateUPDList = array();
        
        $query = "SELECT id_product, price, quantity FROM {$this->prefix_table}product "
        . "WHERE id_shop = '" . $id_shop . "' AND id_product BETWEEN '" . $id_product_from . "' AND '" . $id_product_to . "'";
        $rows = $this->conn_id->db_query_string_fetch($query);
        foreach ($rows as $row) {
            $dateUPDList[$row['id_product']] = $row;
        }

        $query = "SELECT id_product, id_product_attribute, price, quantity FROM {$this->prefix_table}product_attribute "
        . "WHERE id_shop = '" . $id_shop . "' AND id_product BETWEEN '" . $id_product_from . "' AND '" . $id_product_to . "'";
        $rows = $this->conn_id->db_query_string_fetch($query);
        foreach ($rows as $row) {
            $dateUPDList[$row['id_product']]['combinations'][$row['id_product_attribute']] = $row;
        }

        $query = "SELECT id_product, date_from, date_to, reduction, reduction_type FROM {$this->prefix_table}product_sale "
        . "WHERE id_shop = '" . $id_shop . "' AND id_product BETWEEN '" . $id_product_from . "' AND '" . $id_product_to . "'";
        $rows = $this->conn_id->db_query_string_fetch($query);
        foreach ($rows as $row) {
            $dateUPDList[$row['id_product']]['sale'] = $row;
        }

        return $dateUPDList;
    }

    public function getProductRowByIdFromProductDB($id_product) {
        $query = "SELECT id_product FROM {$this->prefix_products_database}product WHERE id_product = " . $id_product;
        $exec = $this->prod_conn_id->db_sqlit_query($query);
        $row = $this->prod_conn_id->db_num_rows($exec);
        return $row;
    }

    public function getProductRowsByIdFromProductDB($id_shop, $id_product_from, $id_product_to) {
        
        $idList = array();
        
        $query = "SELECT id_product FROM {$this->prefix_products_database}product "
        . "WHERE id_shop = '" . $id_shop . "' AND id_product BETWEEN '" . $id_product_from . "' AND '" . $id_product_to . "'";
        
        $rows = $this->prod_conn_id->db_query_string_fetch($query);
        if (is_array($rows)) {
            foreach ($rows as $row) {
                $idList[$row['id_product']] = true;
            }
        }
        return $idList;
    }

    public function updateProductStatusToProductDB($id_product, $id_shop, $staus) {
        
        if(empty($id_product))return false;
        
        if(is_array($id_product)){
            $query = "UPDATE {$this->prefix_products_database}product SET active = " . (int) $staus . " "
                    . "WHERE id_shop = '" . $id_shop . "' AND id_product in (" . implode(',',$id_product)  . ");";
        }else{
            $query = "UPDATE {$this->prefix_products_database}product SET active = " . (int) $staus . " "
                    . "WHERE id_shop = '" . $id_shop . "' AND id_product = " . (int) $id_product . ";";
        }
        return $query;
    }

    public function isChanged($Offers) {
        //ID
        if (isset($Offers['ProductIdReference']) && !empty($Offers['ProductIdReference'])) {
            $id_product = (int) ObjectModel::escape_str($Offers['ProductIdReference']);
            if (!isset($this->offerUpdated_offerDB[$id_product])) {
                return true;
            }
        }

        //PRICE
        if (isset($Offers['BasePrice'])) {
            $price = (float) ObjectModel::escape_str($Offers['BasePrice']);
            if (!isset($this->offerUpdated_offerDB[$id_product]['price']) || 
                    (isset($this->offerUpdated_offerDB[$id_product]['price']) && $this->offerUpdated_offerDB[$id_product]['price'] != $price)) {
                return true;
            }
        }

        //STOCK
        if (isset($Offers['Stock'])) {
            $quantity = (int) ObjectModel::escape_str($Offers['Stock']);
            if ($this->offerUpdated_offerDB[$id_product]['quantity'] != $quantity) {
                return true;
            }
        }

        //HAS ATTRIBUTE
        if (isset($Offers->Items) && !empty($Offers->Items)) {
            $feedCombinationCount = 0;
            $matchedCombinationCount = 0;
            foreach ($Offers->Items->Item as $items) {
                $feedCombinationCount++;
                $id_product_attribute = (int) ObjectModel::escape_str($items['AttributeIdReference']);
                $item_price = (float) ObjectModel::escape_str($items['AdditionalPrice']);
                $item_quantity = (int) ObjectModel::escape_str($items['Stock']);

                //check price & Qty
                if (!isset($this->offerUpdated_offerDB[$id_product]['combinations']) ||
                        !isset($this->offerUpdated_offerDB[$id_product]['combinations'][$id_product_attribute]) ||
                        $this->offerUpdated_offerDB[$id_product]['combinations'][$id_product_attribute]['price'] != $item_price ||
                        $this->offerUpdated_offerDB[$id_product]['combinations'][$id_product_attribute]['quantity'] != $item_quantity) {
                    return true;
                } else {
                    //if all price & Qty have no change, check number of combination
                    $matchedCombinationCount++;
                }
            }

            if ($feedCombinationCount != $matchedCombinationCount || sizeof($this->offerUpdated_offerDB[$id_product]['combinations']) != $matchedCombinationCount) {
                return true;
            }
        }
        
        //SALES
        if(isset($Offers->DiscountList) && !empty($Offers->DiscountList)){
        	foreach($Offers->DiscountList->Discount as $Discount){
        		
        		if(!isset($this->offerUpdated_offerDB[$id_product]['sale']) ||
        				($this->offerUpdated_offerDB[$id_product]['sale']['date_from'] != $Discount['StartDate'] ||
        						$this->offerUpdated_offerDB[$id_product]['sale']['date_to'] != $Discount['EndDate'] ||
        						$this->offerUpdated_offerDB[$id_product]['sale']['reduction'] != $Discount['DiscountValue'] ||
        						$this->offerUpdated_offerDB[$id_product]['sale']['reduction_type'] != $Discount['DiscountType'])
        		){
        				
        			return true;
        		}
        	}
        }
        
        return 0;
    }

    public function importData($data, $time) {
       
        $return = $shopname = '';
        $log = new ImportLog($this->user);
        $stock = new StockMovement($this->user);

        global $no_total, $no_success, $no_error, $no_warning, $batchID, $source;
        
	$ProductOption = new MarketplaceOfferOption($this->user, $this->id, $this->connect);
	$countries = ObjectModel::get_countries();
	
        $warning = 1;
        $error = 2;
        $sql = $sql_offer = $sql_product = $sql_log = $sql_log_delete = "";
        $productID_productDB = $sql_log_error = $sql_log_success = $ProductOptions = array();
        $error_counter = array();
        $active_product=array();
        $arr_item=array();
        $arr_discount=array(); 
        $arr_offer=array(); 
        //Products
        foreach ($data as $OffersXml) {
            
            $id_currency = 0;
            $id_shop = $OffersXml->id_shop;
            $shopname = $OffersXml->shopname;
            $source = $OffersXml->source;

            $productID_productDB = $this->getProductRowsByIdFromProductDB(
                    $id_shop, 
                    $OffersXml->Offer[0]['ProductIdReference'], 
                    $OffersXml->Offer[sizeof($OffersXml->Offer) - 1]['ProductIdReference']
                    );
            $this->offerUpdated_offerDB = $this->getOffersUpdated(
                    $id_shop, 
		    $OffersXml->Offer[0]['ProductIdReference'], 
                    $OffersXml->Offer[sizeof($OffersXml->Offer) - 1]['ProductIdReference']
                    );
	    
	    // get all offer options from main & all marketplace
	    $this->offerOptionsUpdated = $ProductOption->get_product_option_by_ids(
                    $id_shop, 
		    $OffersXml->Offer[0]['ProductIdReference'], 
                    $OffersXml->Offer[sizeof($OffersXml->Offer) - 1]['ProductIdReference']
                    );
	    
	    // get all Product Strategy 
	    $this->productStrategUpdated = $ProductOption->get_product_strategies(
                    $id_shop, 
		    $OffersXml->Offer[0]['ProductIdReference'], 
                    $OffersXml->Offer[sizeof($OffersXml->Offer) - 1]['ProductIdReference']
                    );
	    
            if (isset($OffersXml->Currency['ID']) && !empty($OffersXml->Currency['ID'])) {
                $id_currency = (int) ObjectModel::escape_str($OffersXml->Currency['ID']);
            }

            foreach ($OffersXml->Offer as $Offers) {
                
                $flag = $this->isChanged($Offers);                
                $sql = '';                
                $offer = array();
                $id_product = 0;

                //ID
                if (isset($Offers['ProductIdReference']) && !empty($Offers['ProductIdReference'])) {
                    $id_product = (int) ObjectModel::escape_str($Offers['ProductIdReference']);
                    $offer['id_product'] = $id_product;
                    $offer['id_shop'] = (int) $id_shop;
                }

                //Check Products
                if (!isset($productID_productDB[$id_product])) {
                                      
                    if(!isset($sql_log_error['offer'][$error][9][$id_product][0])){                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $error,
                            'error_code' => 9,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );                        
                        $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);                        
                        $sql_log_error['offer'][$error][9][$id_product][0] = true;    
                        $error_counter[$id_product] = 'error';
                    }  
                } else {
                    
                    if(!isset($sql_log_success['offer'][$error][9][$id_product][0])){                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $error,
                            'error_code' => 9,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );                        
                        $sql_log_delete .=  $log->deltete_log($err, $id_shop);                        
                        $sql_log_success['offer'][$error][9][$id_product][0] = true;                        
                    } 
                }

                if (isset($Offers['Active'])) {
                    $active_product[(int) ObjectModel::escape_str($Offers['Active'])][]=$id_product;
                }

                $no_total++;

                if (!isset($id_product) || empty($id_product)){
                    if(!isset($sql_log_error['offer'][$error][1][$id_product][0])){                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $error,
                            'error_code' => 1,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );                        
                        $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);                        
                        $sql_log_error['offer'][$error][1][$id_product][0] = true;                        
                        $error_counter[$id_product] = 'error';
                    }                     
                } else {
                    if(!isset($sql_log_success['offer'][$error][1][$id_product][0])){                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $error,
                            'error_code' => 1,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );                        
                        $sql_log_delete .=  $log->deltete_log($err, $id_shop);                        
                        $sql_log_success['offer'][$error][1][$id_product][0] = true;
                    } 
                }

                //CARRIER
                if (isset($Offers['DefaultCarrier']) && !empty($Offers['DefaultCarrier'])) {
                    $id_carrier_default = (int) ObjectModel::escape_str($Offers['DefaultCarrier']);
                    $offer['id_carrier_default'] = $id_carrier_default;
                }

                //CATEGORY
                if (isset($Offers['DefaultCategory']) && !empty($Offers['DefaultCategory'])) {
                    $id_category_default = (int) ObjectModel::escape_str($Offers['DefaultCategory']);
                    $offer['id_category_default'] = $id_category_default;
                }

                //MANUFACTURER
                if (isset($Offers['Manufacturer']) && !empty($Offers['Manufacturer'])) {
                    $id_manufacturer = (int) ObjectModel::escape_str($Offers['Manufacturer']);
                    $offer['id_manufacturer'] = $id_manufacturer;
                }

                //SUPPLIER
                if (isset($Offers['Supplier']) && !empty($Offers['Supplier'])) {
                    $id_supplier = (int) ObjectModel::escape_str($Offers['Supplier']);
                    $offer['id_supplier'] = $id_supplier;
                }

                //Reference
                if (isset($Offers['ProductReference']) && !empty($Offers['ProductReference'])) {
                    $sku = ObjectModel::escape_str($Offers['ProductReference']);
                    $offer['reference'] = $sku;
                }

                //Sku
                if (isset($Offers['ProductSku']) && !empty($Offers['ProductSku'])) {
                    $sku = ObjectModel::escape_str($Offers['ProductSku']);
                    $offer['sku'] = $sku;
                }

                //EAN
                if (isset($Offers['ProductEan']) && !empty($Offers['ProductEan'])) {
                    $ean13 = ObjectModel::escape_str($Offers['ProductEan']);
                    $offer['ean13'] = $ean13;
                    if (!$log->EAN_UPC_Check($offer['ean13'])) {
                        if(!isset($sql_log_error['offer'][$warning][5][$id_product][0])){                                    
                            $err = array(
                                'product_type' => 'offer',
                                'severity' => $warning,
                                'error_code' => 5,
                                'id_product' => $id_product,
                                'id_product_attribute' => 0,
                                'message' => $offer['ean13'],
                            );                                
                            $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);                                
                            $sql_log_error['offer'][$warning][5][$id_product][0] = true;                                
                            $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                        }
                    } else {
                        if(!isset($sql_log_success['offer'][$warning][5][$id_product][0])){                                    
                            $err = array(
                                'product_type' => 'offer',
                                'severity' => $warning,
                                'error_code' => 5,
                                'id_product' => $id_product,
                                'id_product_attribute' => 0
                            );                                
                            $sql_log_delete .=  $log->deltete_log($err, $id_shop);                                
                            $sql_log_success['offer'][$warning][5][$id_product][0] = true;                                
                        } 
                    }
                } 
                
                //UPC
                if (isset($Offers['ProductUpc']) && !empty($Offers['ProductUpc'])) {
                    $upc = ObjectModel::escape_str($Offers['ProductUpc']);
                    $offer['upc'] = $upc;
                    if (!$log->EAN_UPC_Check($offer['upc'])) {
                        if(!isset($sql_log_error['offer'][$warning][5][$id_product][0])){                                    
                            $err = array(
                                'product_type' => 'offer',
                                'severity' => $warning,
                                'error_code' => 14,
                                'id_product' => $id_product,
                                'id_product_attribute' => 0,
                                'message' =>  $offer['upc'],
                            );                                
                            $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);                                
                            $sql_log_error['offer'][$warning][5][$id_product][0] = true;
                            $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                        }
                    } else {
                        if(!isset($sql_log_success['offer'][$warning][5][$id_product][0])){                                    
                            $err = array(
                                'product_type' => 'offer',
                                'severity' => $warning,
                                'error_code' => 5,
                                'id_product' => $id_product,
                                'id_product_attribute' => 0
                            );                                
                            $sql_log_delete .=  $log->deltete_log($err, $id_shop);                                
                            $sql_log_success['offer'][$warning][5][$id_product][0] = true;                                
                        } 
                    }
                }

                if ((!isset($offer['reference']) || empty($offer['reference']))){
                    if(!isset($sql_log_error['offer'][$warning][2][$id_product][0])){                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $warning,
                            'error_code' => 2,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );                        
                        $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);                        
                        $sql_log_error['offer'][$warning][2][$id_product][0] = true;                        
                        $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                    } 
                }else {    
                    if(!isset($sql_log_success['offer'][$warning][2][$id_product][0])){                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $warning,
                            'error_code' => 2,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );                        
                        $sql_log_delete .=  $log->deltete_log($err, $id_shop);                        
                        $sql_log_success['offer'][$warning][2][$id_product][0] = true;                        
                    } 
                }

                //ECOTAX
                if (isset($Offers['EcoPart']) && !empty($Offers['EcoPart'])) {
                    $ecotax = (float) ObjectModel::escape_str($Offers['EcoPart']);
                    $offer['ecotax'] = $ecotax;
                }

                //PRICE
                if (isset($Offers['BasePrice']) && !empty($Offers['BasePrice'])) {
                    $price = (float) ObjectModel::escape_str($Offers['BasePrice']);
                    $offer['price'] = $price;
                }
                
                // WHOLESALE_PRICE /* ADD : 16/07/2015 */
                if(isset($Offers['WholeSalePrice']) && !empty($Offers['WholeSalePrice'])){
                    $wholesale_price = (float)ObjectModel::escape_str($Offers['WholeSalePrice']);
                    $offer['wholesale_price'] = $wholesale_price;
                }

                //CURRENCY
                $offer['id_currency'] = $id_currency;

                //CONDITION
                if (isset($Offers['ProductCondition']) && !empty($Offers['ProductCondition'])) {
                    $condition = (int) ObjectModel::escape_str($Offers['ProductCondition']);
                    $offer['id_condition'] = $condition;
                }

                //TAX
                if (isset($Offers['Vat']) && !empty($Offers['Vat'])) {
                    $vat = (float) ObjectModel::escape_str($Offers['Vat']);
                    $offer['id_tax'] = $vat;
                }

                //SHIPPING COST
                if (isset($Offers['ShippingPrice']) && !empty($Offers['ShippingPrice'])) {
                    $shipping_price = (float) ObjectModel::escape_str($Offers['ShippingPrice']);
                    $offer['shipping_cost'] = $shipping_price;
                }

                //STOCK
                if (isset($Offers['Stock']) && !empty($Offers['Stock'])) {
                    $quantity = (int) ObjectModel::escape_str($Offers['Stock']);
                    $offer['quantity'] = $quantity;
                }
		
 		#OffersOptions 
		if(isset($Offers->OffersOptions) /*&& (!isset($Offers->Items) || ($Offers->Items->count() == 0))*/){
		    
		    foreach ($Offers->OffersOptions as $OffersOptions){ 
			
			foreach($OffersOptions as $OptionKeys => $OffersOption){
                            if(!empty($Offers->Items->Item)){
                                foreach ($Offers->Items->Item as $items) {
                                    if(isset($items->OffersOptions->$OptionKeys) && !empty($items->OffersOptions->$OptionKeys) ) {
                                        continue;
                                    } else {
                                        if(!isset($items->OffersOptions->$OptionKeys)){
                                           if(!empty($OptionKeys)){

                                                // check table exits
                                                if(!isset($history[$OptionKeys])){
                                                    $history[$OptionKeys] = $ProductOption->checke_table_exit($OptionKeys);
                                                }

                                                if(isset($history[$OptionKeys]) && $history[$OptionKeys]){

                                                    foreach ($OffersOption as $OfferOption){
                                                            $id_country = null;
                                                            if(isset($OfferOption['region'])){
                                                                $Offer_key = ObjectModel::escape_str($OfferOption['region']);
                                                                $id_country= (int) $countries[ObjectModel::escape_str($OfferOption['region'])]['id'];
                                                            }

                                                            if(isset($OfferOption['lang'])){
                                                                $Offer_key = ObjectModel::escape_str($OfferOption['lang']);
                                                                $id_lang= (int) ObjectModel::escape_str($OfferOption['lang']);
                                                            }

                                                            $OfferOption->id_product = $id_product;
                                                            $OfferOption->id_product_attribute = $items['AttributeIdReference'];
                                                            $OfferOption->sku = isset($items['AttributeReference']) ? $items['AttributeReference'] : null;
                                                            $OfferOption->id_shop = (int) $id_shop;
                                                            $OfferOption->id_country = $id_country;
                                                            $OfferOption->id_lang =  isset($id_lang) ? $id_lang : null;

                                                            $offerOptionsUpdated = null;
                                                            $marketplace = strtolower($OptionKeys);
                                                            $site = isset($id_country) ? $id_country : (isset($id_lang) ? $id_lang : null);

                                                            // get Current Offer Options
                                                            if(!empty($site) && isset($this->offerOptionsUpdated[$marketplace][$site][$id_product][$items['AttributeIdReference']])){
                                                                $offerOptionsUpdated['options'] =
                                                                        $this->offerOptionsUpdated[$marketplace][$site][$id_product][$items['AttributeIdReference']];
                                                            }

                                                            // get Current Product Strategy
                                                            if(!empty(trim($items['AttributeIdReference'])) && isset($this->productStrategUpdated[$marketplace][$site][$id_product][trim($items['AttributeIdReference'])])){
                                                                $offerOptionsUpdated['repricing'] =
                                                                        $this->productStrategUpdated[$marketplace][$site][$id_product][trim($items['AttributeIdReference'])];
                                                            }

                                                            $offers = $ProductOption->set_offers_options($OfferOption, $OptionKeys, $offerOptionsUpdated);

                                                            if(isset($offers['options']) && !empty($offers['options'])){
                                                                if(!isset($ProductOptions['options'][$OptionKeys])){
                                                                    $ProductOptions['options'][$OptionKeys] = array();
                                                                }
                                                                $ProductOptions['options'][$OptionKeys][] =  $offers['options'];
                                                            }

                                                            if(isset($offers['repricing']) && !empty($offers['repricing'])){
                                                                if(!isset($ProductOptions['repricing'][$OptionKeys])){
                                                                    $ProductOptions['repricing'][$OptionKeys] = array();
                                                                }
                                                                $ProductOptions['repricing'][$OptionKeys][] = $offers['repricing'];
                                                            }

                                                            // if data is changed : trigger repricing
                                                            if(isset($offers['trigger_repricing']) && !empty($offers['trigger_repricing'])){

                                                                if(!isset($ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'])){
                                                                    $ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'] = '';
                                                                }
                                                                if(!isset($ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'])){
                                                                    $ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'] = '';
                                                                }

                                                                $ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'].=
                                                                        ", ". $offers['trigger_repricing']['product_update_log'];
                                                                $ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'] .=
                                                                        ", ". $offers['trigger_repricing']['repricing_flag'];

                                                            }

                                                            // flag update offer when Offers Options have changed
                                                            if(isset($offers['flag_update']) && $offers['flag_update']){
                                                                 $flag = true;
                                                            }

                                                            unset($offers);
                                                            unset($OfferOption);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
			    if(!empty($OptionKeys)){

				// check table exits
				if(!isset($history[$OptionKeys])){
				    $history[$OptionKeys] = $ProductOption->checke_table_exit($OptionKeys);
				}

				if(isset($history[$OptionKeys]) && $history[$OptionKeys]){
				    foreach ($OffersOption as $OfferOption){
                                            $id_country = null;
					    if(isset($OfferOption['region'])){
						$Offer_key = ObjectModel::escape_str($OfferOption['region']);
						$id_country= (int) $countries[ObjectModel::escape_str($OfferOption['region'])]['id'];
					    }

					    if(isset($OfferOption['lang'])){
						$Offer_key = ObjectModel::escape_str($OfferOption['lang']);
						$id_lang= (int) ObjectModel::escape_str($OfferOption['lang']);
					    }

					    $OfferOption->id_product = $id_product;
					    $OfferOption->id_product_attribute = 0;
					    $OfferOption->sku = isset($offer['reference']) ? $offer['reference'] : null; 
					    $OfferOption->id_shop = (int) $id_shop;
					    $OfferOption->id_country = $id_country ;
					    $OfferOption->id_lang =  isset($id_lang) ? $id_lang : null;
					    
					    $offerOptionsUpdated = array();
					    $marketplace = strtolower($OptionKeys);
					    $site = isset($id_country) ? $id_country : (isset($id_lang) ? $id_lang : 0);
					    
					    // get Current Offer Options
					    if(isset($this->offerOptionsUpdated[$marketplace][$site][$id_product][0])){
						$offerOptionsUpdated['options'] = 
							$this->offerOptionsUpdated[$marketplace][$site][$id_product][0];
					    }

					    // get Current Product Strategy
					    if(isset($this->productStrategUpdated[$marketplace][$site][$id_product][0])){
						$offerOptionsUpdated['repricing'] = 
							$this->productStrategUpdated[$marketplace][$site][$id_product][0];
					    }
					    
					    $offers = $ProductOption->set_offers_options($OfferOption, $OptionKeys, $offerOptionsUpdated);
					    
					    if(isset($offers['options']) && !empty($offers['options'])){
						if(!isset($ProductOptions['options'][$OptionKeys])){
						    $ProductOptions['options'][$OptionKeys] = array();
						}
						$ProductOptions['options'][$OptionKeys][]=   $offers['options'];
					    }
					    
					    if(isset($offers['repricing']) && !empty($offers['repricing'])){
						if(!isset($ProductOptions['repricing'][$OptionKeys])){
						    $ProductOptions['repricing'][$OptionKeys] = array();
						}
						$ProductOptions['repricing'][$OptionKeys][] = $offers['repricing'];
					    }
					    
					    // if data is changed : trigger repricing
					    if(isset($offers['trigger_repricing']) && !empty($offers['trigger_repricing'])){
						
						if(!isset($ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'])){
						    $ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'] = '';
						} 
						if(!isset($ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'])){
						    $ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'] = '';
						} 
						
						$ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'] .= 
							 ", ". $offers['trigger_repricing']['product_update_log'];
						$ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'] .= 
							 ", ". $offers['trigger_repricing']['repricing_flag'];
						
					    }

                                            // flag update offer when Offers Options have changed
                                            if(isset($offers['flag_update']) && $offers['flag_update']){
                                                 $flag = true;
                                            }
                                            
					    unset($offers);
					    unset($OfferOption);
					    
				    }
				} 
			    }					
			}
		    }
		}		
		#OffersOptions 

                //HAS ATTRIBUTE
                if (isset($Offers->Items) && !empty($Offers->Items)) {
                    $offer['has_attribute'] = sizeof($Offers->Items->Item);
                    $item_quantity = 0;

                    foreach ($Offers->Items->Item as $items) { 
                        $item = array();

                        $item['id_product'] = $id_product;
                        $item['id_product_attribute'] = (int) ObjectModel::escape_str($items['AttributeIdReference']);
                        $item['id_shop'] = (int) $id_shop;

                        $item['reference'] = $item['sku'] = $item['ean13'] = $item['upc'] = '';

                        //Reference
                        if (isset($items['AttributeReference']) && !empty($items['AttributeReference'])) {
                            
                            $item['reference'] = ObjectModel::escape_str($items['AttributeReference']);
                            
                            if (!$log->ValidateSKU($item['reference'])) {
                                if(!isset($sql_log_error['offer'][$warning][3][$id_product][$item['id_product_attribute']])){
                                    
                                    $err = array(
                                        'product_type' => 'offer',
                                        'severity' => $warning,
                                        'error_code' => 3,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => $item['id_product_attribute'],
                                        'message' =>  $item['reference'],
                                    );
                                    
                                    $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);
                                    
                                    $sql_log_error['offer'][$warning][3][$id_product][$item['id_product_attribute']] = true;
                            		$error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                                }
                            } else {
                                if(!isset($sql_log_success['offer'][$warning][3][$id_product][$item['id_product_attribute']])){
                                    
                                    $err = array(
                                        'product_type' => 'offer',
                                        'severity' => $warning,
                                        'error_code' => 3,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => $item['id_product_attribute'],
                                    );
                                    
                                    $sql_log_delete .=  $log->deltete_log($err, $id_shop);
                                    
                                    $sql_log_success['offer'][$warning][3][$id_product][$item['id_product_attribute']] = true;
                                    
                                }
                            }
                            
                            if(!isset($sql_log_success['offer'][$warning][2][$id_product][$item['id_product_attribute']])){
                                    
                                $err = array(
                                    'product_type' => 'offer',
                                    'severity' => $warning,
                                    'error_code' => 2,
                                    'id_product' => $id_product,
                                    'id_product_attribute' => $item['id_product_attribute'],
                                );
                                
                                $sql_log_delete .=  $log->deltete_log($err, $id_shop);
                                
                                $sql_log_success['offer'][$warning][2][$id_product][$item['id_product_attribute']] = true;
                                
                            }
                            
                        } else {
                            if(!isset($sql_log_error['offer'][$warning][2][$id_product][$item['id_product_attribute']])){
                                    
                                $err = array(
                                    'product_type' => 'offer',
                                    'severity' => $warning,
                                    'error_code' => 2,
                                    'id_product' => $id_product,
                                    'id_product_attribute' => $item['id_product_attribute'],
                                );
                                
                                $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);
                                
                                $sql_log_error['offer'][$warning][2][$id_product][$item['id_product_attribute']] = true;
                            	$error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                            }
                        }

                        // EAN
                        if (isset($items['AttributeEAN']) && !empty($items['AttributeEAN'])) {
                            $item['ean13'] = ObjectModel::escape_str($items['AttributeEAN']);
                        }

                        // UPC
                        if (isset($items['AttributeUPC']) && !empty($items['AttributeUPC'])) {
                            $item['upc'] = ObjectModel::escape_str($items['AttributeUPC']);
                        }

                        if (empty($item['ean13']) && empty($item['upc'])) {
                            if(!isset($sql_log_error['offer'][$warning][4][$id_product][$item['id_product_attribute']])){
                                    
                                $err = array(
                                    'product_type' => 'offer',
                                    'severity' => $warning,
                                    'error_code' => 4,
                                    'id_product' => $id_product,
                                    'id_product_attribute' => $item['id_product_attribute'],
                                );
                                
                                $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);
                                
                                $sql_log_error['offer'][$warning][4][$id_product][$item['id_product_attribute']] = true;
                            	$error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                            }
                        } else {
                            if(!isset($sql_log_success['offer'][$warning][4][$id_product][$item['id_product_attribute']])){
                                    
                                $err = array(
                                    'product_type' => 'offer',
                                    'severity' => $warning,
                                    'error_code' => 4,
                                    'id_product' => $id_product,
                                    'id_product_attribute' => $item['id_product_attribute'],
                                );
                                
                                $sql_log_delete .=  $log->deltete_log($err, $id_shop);
                                
                                $sql_log_success['offer'][$warning][4][$id_product][$item['id_product_attribute']] = true;
                                
                            }
                        }

                        // WHOLESALE_PRICE /* ADD : 16/07/2015 */
                        if(isset($items['AdditionalWholeSalePrice']) && !empty($items['AdditionalWholeSalePrice'])){
                            $items_wholesale_price = (float)ObjectModel::escape_str($items['AdditionalWholeSalePrice']);
                            $item['wholesale_price'] = $items_wholesale_price;
                        }
                        
                        $item['price'] = (float) ObjectModel::escape_str($items['AdditionalPrice']);
                        $item['quantity'] = (int) ObjectModel::escape_str($items['Stock']);

                        //Table product attribute
                        $arr_item[]=$item;

                        $item_quantity = $item_quantity + $item['quantity'];

                        if (!$item['price'] || empty($item['price'])) {
                            if(!isset($sql_log_error['offer'][$error][10][$id_product][$item['id_product_attribute']])){
                                    
                                $err = array(
                                    'product_type' => 'offer',
                                    'severity' => $error,
                                    'error_code' => 10,
                                    'id_product' => $id_product,
                                    'id_product_attribute' => $item['id_product_attribute'],
                                );
                                
                                $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);
                                
                                $sql_log_error['offer'][$error][10][$id_product][$item['id_product_attribute']] = true;
                        		$error_counter[$id_product] = 'error';
                            }
                        } else {
                            if(!isset($sql_log_success['offer'][$error][10][$id_product][$item['id_product_attribute']])){
                                    
                                $err = array(
                                    'product_type' => 'offer',
                                    'severity' => $error,
                                    'error_code' => 10,
                                    'id_product' => $id_product,
                                    'id_product_attribute' => $item['id_product_attribute'],
                                );
                                
                                $sql_log_delete .=  $log->deltete_log($err, $id_shop);
                                
                                $sql_log_success['offer'][$error][10][$id_product][$item['id_product_attribute']] = true;
                                
                            }
                        }
						
			#OffersOptions
                        // 1. check item first : When childred has Offers Options, replace offer option in this attribute
 			if(isset($items->OffersOptions) && isset($item['id_product_attribute'])){
			    foreach ($items->OffersOptions as $OffersOptions){
				foreach($OffersOptions as $OptionKeys => $OffersOption){

				    if(!empty($OptionKeys)){

					// check table exits
					if(!isset($history[$OptionKeys])){
					    $history[$OptionKeys] = $ProductOption->checke_table_exit($OptionKeys);
					}

					if(isset($history[$OptionKeys]) && $history[$OptionKeys]){

					    foreach ($OffersOption as $OfferOption){
                                                    $id_country = null;
						    if(isset($OfferOption['region'])){
							$Offer_key = ObjectModel::escape_str($OfferOption['region']);
							$id_country= (int) $countries[ObjectModel::escape_str($OfferOption['region'])]['id'];
						    }

						    if(isset($OfferOption['lang'])){
							$Offer_key = ObjectModel::escape_str($OfferOption['lang']);
							$id_lang= (int) ObjectModel::escape_str($OfferOption['lang']);
						    }

						    $OfferOption->id_product = $id_product;
						    $OfferOption->id_product_attribute = $item['id_product_attribute'];
						    $OfferOption->sku = isset($item['reference']) ? $item['reference'] : null;
						    $OfferOption->id_shop = (int) $id_shop;
						    $OfferOption->id_country = $id_country;
						    $OfferOption->id_lang =  isset($id_lang) ? $id_lang : null;

						    $offerOptionsUpdated = null;
						    $marketplace = strtolower($OptionKeys);
						    $site = isset($id_country) ? $id_country : (isset($id_lang) ? $id_lang : null);

						    // get Current Offer Options
						    if(isset($this->offerOptionsUpdated[$marketplace][$site][$id_product][$item['id_product_attribute']])){
							$offerOptionsUpdated['options'] =
								$this->offerOptionsUpdated[$marketplace][$site][$id_product][$item['id_product_attribute']];
						    }

						    // get Current Product Strategy
						    if(isset($this->productStrategUpdated[$marketplace][$site][$id_product][$item['id_product_attribute']])){
							$offerOptionsUpdated['repricing'] =
								$this->productStrategUpdated[$marketplace][$site][$id_product][$item['id_product_attribute']];
						    }

						    $offers = $ProductOption->set_offers_options($OfferOption, $OptionKeys, $offerOptionsUpdated);

						    if(isset($offers['options']) && !empty($offers['options'])){
							if(!isset($ProductOptions['options'][$OptionKeys])){
							    $ProductOptions['options'][$OptionKeys] = array();
                                                        }
                                                        $ProductOptions['options'][$OptionKeys][]=   $offers['options'];
						    }

						    if(isset($offers['repricing']) && !empty($offers['repricing'])){
							if(!isset($ProductOptions['repricing'][$OptionKeys])){
							    $ProductOptions['repricing'][$OptionKeys] = array();
							}
							$ProductOptions['repricing'][$OptionKeys][] =  $offers['repricing'];
						    }

						    // if data is changed : trigger repricing
						    if(isset($offers['trigger_repricing']) && !empty($offers['trigger_repricing'])){

							if(!isset($ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'])){
							    $ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'] = '';
							}
							if(!isset($ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'])){
							    $ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'] = '';
							}

							$ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'].=
								", ". $offers['trigger_repricing']['product_update_log'];
							$ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'] .=
								", ". $offers['trigger_repricing']['repricing_flag'];

						    }

                                                    // flag update offer when Offers Options have changed
                                                    if(isset($offers['flag_update']) && $offers['flag_update']){
                                                         $flag = true;
                                                    }

						    unset($offers);
						    unset($OfferOption);
					    }
					}
				    }
				}
			    }
			}
			// 2. if item is empty : When the parent product has Offers Options, set the options to all id_product_tribute
 			else if(isset($Offers->OffersOptions) && isset($item['id_product_attribute'])){
			    foreach ($Offers->OffersOptions as $OffersOptions){
				foreach($OffersOptions as $OptionKeys => $OffersOption){

				    if(!empty($OptionKeys)){

					// check table exits
					if(!isset($history[$OptionKeys])){
					    $history[$OptionKeys] = $ProductOption->checke_table_exit($OptionKeys);
					}

					if(isset($history[$OptionKeys]) && $history[$OptionKeys]){
					    foreach ($OffersOption as $OfferOption){
                                                    $id_country = null;
						    if(isset($OfferOption['region'])){
							$Offer_key = ObjectModel::escape_str($OfferOption['region']);
							$id_country= (int) $countries[ObjectModel::escape_str($OfferOption['region'])]['id'];
						    }

						    if(isset($OfferOption['lang'])){
							$Offer_key = ObjectModel::escape_str($OfferOption['lang']);
							$id_lang= (int) ObjectModel::escape_str($OfferOption['lang']);
						    }

						    $OfferOption->id_product = $id_product;
						    $OfferOption->id_product_attribute = $item['id_product_attribute'];
						    $OfferOption->sku = isset($item['reference']) ? $item['reference'] : null;
						    $OfferOption->id_shop = (int) $id_shop;
						    $OfferOption->id_country = $id_country;
						    $OfferOption->id_lang =  isset($id_lang) ? $id_lang : null;
						    
						    // check : Has offer option ?
						    // 
						    $offerOptionsUpdated = null;
						    $marketplace = strtolower($OptionKeys);
						    $site = isset($id_country) ? $id_country : 0;
						    
						    // get Current Offer Options
						    if(isset($this->offerOptionsUpdated[$marketplace][$site][$id_product][$item['id_product_attribute']])){
							$offerOptionsUpdated['options'] = 
								$this->offerOptionsUpdated[$marketplace][$site][$id_product][$item['id_product_attribute']];
						    }

						    // get Current Product Strategy
						    if(isset($this->productStrategUpdated[$marketplace][$site][$id_product][$item['id_product_attribute']])){
							$offerOptionsUpdated['repricing'] = 
								$this->productStrategUpdated[$marketplace][$site][$id_product][$item['id_product_attribute']];
						    }
						    
						    $offers = $ProductOption->set_offers_options($OfferOption, $OptionKeys, $offerOptionsUpdated);

						    if(isset($offers['options']) && !empty($offers['options'])){
							if(!isset($ProductOptions['options'][$OptionKeys])){
							    $ProductOptions['options'][$OptionKeys] = array();
                                                        }
                                                        $ProductOptions['options'][$OptionKeys][]=   $offers['options'];
						    }

						    if(isset($offers['repricing']) && !empty($offers['repricing'])){
							if(!isset($ProductOptions['repricing'][$OptionKeys])){
							    $ProductOptions['repricing'][$OptionKeys] = array();
							}
							$ProductOptions['repricing'][$OptionKeys][]=  $offers['repricing'];
						    }

						    // if data is changed : trigger repricing
						    if(isset($offers['trigger_repricing']) && !empty($offers['trigger_repricing'])){

							if(!isset($ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'])){
							    $ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'] = '';
							} 
							if(!isset($ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'])){
							    $ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'] = '';
							} 

							$ProductOptions['trigger_repricing'][$OptionKeys]['product_update_log'].=
								 ", ". $offers['trigger_repricing']['product_update_log'];
							$ProductOptions['trigger_repricing'][$OptionKeys]['repricing_flag'] .= 
								 ", ". $offers['trigger_repricing']['repricing_flag'];

						    }

                                                    // flag update offer when Offers Options have changed
                                                    if(isset($offers['flag_update']) && $offers['flag_update']){
                                                         $flag = true;
                                                    }

						    unset($offers);
						    unset($OfferOption);
					    }
					} 
				    }					
				}
			    }
			}	    
			#OffersOptions

                        if ($flag == true) {
                            $data_offer = $item;
                            $stock->stockMovementProcess($data_offer, $id_shop, $time, StockMovement::update_offer, strval($shopname));
                            $sql_offer .= $this->updateMarketplaceFlag($id_product, $id_shop);
                        }

                    }

                    $offer['quantity'] = $item_quantity;
                } else {
                    if ($flag == true) {
                        $data_offer = $offer;
                        $data_offer['id_product_attribute'] = 0;
                        $stock->stockMovementProcess($data_offer, $id_shop, $time, StockMovement::update_offer, strval($shopname));
                        $sql_offer .= $this->updateMarketplaceFlag($id_product, $id_shop);
                    }
                }

                if (!isset($offer['quantity']) || empty($offer['quantity'])){
                    if(!isset($sql_log_error['offer'][$warning][7][$id_product][0])){
                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $warning,
                            'error_code' => 7,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );
                        
                        $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);
                        
                        $sql_log_error['offer'][$warning][7][$id_product][0] = true;
                        $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                    }
                } else {
                    if(!isset($sql_log_success['offer'][$warning][7][$id_product][0])){
                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $warning,
                            'error_code' => 7,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );
                        
                        $sql_log_delete .=  $log->deltete_log($err, $id_shop);
                        
                        $sql_log_success['offer'][$warning][7][$id_product][0] = true;
                        
                    }
                }

                if (!isset($Offers['Active']) || empty($Offers['Active']) || $Offers['Active'] == 0){
                    if(!isset($sql_log_error['offer'][$error][8][$id_product][0])){
                                
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $error,
                            'error_code' => 8,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );
                        
                        $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);
                        
                        $sql_log_error['offer'][$error][8][$id_product][0]= true;
                        $error_counter[$id_product] = 'error';
                    }
                } else {
                    if(!isset($sql_log_success['offer'][$error][8][$id_product][0])){
                                
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $error,
                            'error_code' => 8,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );
                        
                        $sql_log_delete .=  $log->deltete_log($err, $id_shop);
                        
                        $sql_log_success['offer'][$error][8][$id_product][0]= true;
                        
                    }
                }
		
                //SALE                
                if (isset($Offers->DiscountList)) {
                    $offer['on_sale'] = 1;
                    foreach ($Offers->DiscountList as $DiscountList){
                        if (isset($DiscountList->Discount)) {
                            foreach ($DiscountList->Discount as $discounts){
                                $discount = array();
                                $discount['id_product'] = $id_product;
                                $discount['date_from'] = ObjectModel::escape_str($discounts['StartDate']);
                                $discount['date_to'] = ObjectModel::escape_str($discounts['EndDate']);
                                $discount['id_shop'] = (int) $id_shop;
                                $discount['reduction'] = (float) ObjectModel::escape_str($discounts['DiscountValue']);
                                $discount['reduction_type'] = ObjectModel::escape_str($discounts['DiscountType']);

                                if(isset($discounts['Combination'])){
                                    $discount['id_product_attribute'] = (int)ObjectModel::escape_str($discounts['Combination']);
                                }
                                $arr_discount[]=$discount;
                            }
                        }
                    }
                } else {
                   if(!isset($arr_delete_sale)){
                       $arr_delete_sale=array();
                   }
                   $arr_delete_sale[] = $id_product;
                }
                
                //Table product
                
                $arr_offer[] = $offer;
                
                $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'success';      
                
            } //$Offers 

            $return .= $sql ;

            $error_counter_sumary = array_count_values($error_counter);
            $no_success = isset($error_counter_sumary['success']) ? $error_counter_sumary['success'] : 0;
            $no_error = isset($error_counter_sumary['error']) ? $error_counter_sumary['error'] : 0;
            $no_warning = isset($error_counter_sumary['warning']) ? $error_counter_sumary['warning'] : 0;
            
        }//$data
        
        $sql_offer .= self::insert(self::$definition['table_link'][0]['name'], $arr_item, $time)."";
        $sql_offer .= self::insert(self::$definition['table_link'][1]['name'], $arr_discount, $time);
        $sql_offer .= self::insert(self::$definition['table'], $arr_offer, $time);

        // Update Flag
        $return.=$sql_offer;
        
        // Update Product Data
        if(!empty($id_shop)){
            foreach($active_product as $k=>$v){
                $sql_product.= $this->updateProductStatusToProductDB($v, $id_shop, $k);
            }
        }
         
        $return.=$sql_product;	
	
	#offersoptions
	if(isset($ProductOptions['options']) && !empty($ProductOptions['options'])){
	    $return .= $ProductOption->save_offers_options($ProductOptions['options']);
	}
	if(isset($ProductOptions['repricing']) && !empty($ProductOptions['repricing'])){
	    $return .= $ProductOption->save_repricing($ProductOptions['repricing']);
	}
	if(isset($ProductOptions['trigger_repricing']) && !empty($ProductOptions['trigger_repricing'])){
	    $return .= $ProductOption->update_repricing_flag($ProductOptions['trigger_repricing']);
	}
	
	unset($ProductOptions);
	
        // Add Log  
        $sql_log = $log->get_all_message_log();
        $return.=$sql_log;
        $sql_log_delete = $log->get_all_sql_delete_log($id_shop);
        $sql_log_product_update = $log->get_all_product_update_log();
        $return.=$sql_log_delete.$sql_log_product_update;
          	
        return $return;
    }

}
