<?php

require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');

abstract class ObjectModel {

    protected $table;
    protected $fieldsRequired = array();
    private $user;
    private $path;
    public $prefix_table = 'offers_';
    private $product_path;
    public static $database = 'offers';
    public static $product_database = 'products';
    public $engine = " ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ";

    public function __construct($user, $id = null, $connect = true) {

        $this->user = $user;
        $this->path = USERDATA_PATH . $user . '/' . ObjectModel::$database . '.db';
        $this->product_path = USERDATA_PATH . $user . '/' . ObjectModel::$product_database . '.db';

        if ($id) {
            $this->id = (int) $id;
        }	
	
        /* Open connection SQLite user db */
        if ($connect){
            $this->db_connect($this->path);
            $this->db_product_connect($this->product_path);
        } 
    }

    public static function createDatabase($user) {
        $path = DIR_SERVER . '/assets/apps';
        $result = array();

        if (!is_writable($path . '/users')) {
            $result['error'] = sprintf('Directory %s is not writeable', $path);
        } else {
            $error = '';
            $oldmask = umask(0);
            $conn_id = new Db($user, ObjectModel::$database);

            umask($oldmask);
        }

        return $result;
    }

    public function createTable()
    {   
        $result = '';
        $class = get_called_class();
        $data = $class::$definition;

        $query = '';
        $lang = false;
        $loop = 1;

        if (isset($data['lang']) || !empty($data['lang'])){
            $lang = true;
        }

        if ($lang){
            $loop = 2;
        }

        for( $l=1; $l <= $loop; $l++ )
        {
            $query = '';
            $sql = array();
            $tablename = $data['table'];
            
            if($l == 2)
            {
               $tablename = $data['table'] . '_lang';
               $data['fields'] = $data['lang']['fields'];
            }
            else 
                $data['fields'] = $data['fields'];
            
            foreach ($data['fields'] as $key_field => $field)
            {
                $default = '';
                $size = '';
                
                if(!isset($field['required']))
                    $field['required'] = false;
                
                $required = ($field['required']) ? ' NOT NULL ' : '' ;
                $default_null =  (isset($field['default_null']) && !$field['required']) ? ' DEFAULT NULL' : '' ;
                $others =  ( isset($field['other']) && !empty($field['other']) ) ? ' ' . $field['other'] : '' ;
                
                 
                    $primary_key = (isset($field['primary_key'])) ? '  AUTO_INCREMENT ' : '' ;
                 
                
                if(!isset($field['default_null']))
                    $default =  isset($field['default']) ? " DEFAULT '" . $field['default'] . "'" : '' ;
                
                if(isset($field['size']))
                    $size = '(' . $field['size'] . ') ';
                $unique = ' ';
		
                $sql[] = $key_field . ' ' . $field['type'] .  $size  . $required . $default_null . $default . $primary_key . $others .$unique;
            }
            
            $arr_key=array();
            if(isset($data['unique']))
            foreach($data['unique'] as $v){
                if(is_array($v)){
                    foreach($v as $datab){
                        $arr_key[$datab]=$datab;
                    }
                }else{
                    $arr_key[$v]=$v;
                }
            }
            if(isset($data['primary']))
            foreach($data['primary'] as $v){ 
                    $arr_key[$v]=$v; 
            } 
            if($l == 2){
                $arr_key['id_lang']='id_lang';
            }
            if(!empty($arr_key)){
                $sql[] = " PRIMARY KEY (".implode(',',$arr_key).")";
            } 
            $sqls = implode(', ', $sql);
            $query .= "CREATE TABLE " . $this->prefix_table . $tablename . "(" . $sqls ;
            
            $query .= ") {$this->engine};" ;
            $out = $this->db_exec($query,false);
        }

        if($out!== true)
            $result .= sprintf('Failed to create table %s  '."<br>$query<br>$out" , $class  ) ; 

        if (isset($data['primary']) && !empty($data['primary']))
        {
            $out = $this->createIndex( $data['table'] . "Index", $data['table'], true, $data['primary']);
            if($out !== true)
                $result .= sprintf('Failed to create Index %s '."<br>$out<br>", $data['table'] . " Index" ) ;
        }
        
        return $result;
    }
   
    function createIndex($index_name, $table_name, $unique = false, $columns = array()) 
    {
        $query = '';
        $column_name = array();
        $type = ($unique) ? 'UNIQUE' : '' ; 
        foreach ($columns as $column)
            $column_name[] = "" . $column . "" ;
        
        $column_names = implode(', ', $column_name);
        $query = "CREATE " . $type . " INDEX " . $index_name . " ON " . $this->prefix_table .$table_name . " (" . $column_names . ')' ;
        
         if(!$this->db_exec($query)){
            return $query; 
         }
         
        return true;
    }

    public function import($data, $time,$status=1,$update_setting=false,$from_task=true){
        
        if (empty($data->id_shop)){
            return false;
        }
        
        $id_shop = $data->id_shop;
        $table_list = array();
        $class = get_called_class();

        //definition
        $definition = $class::$definition;

        if (isset($definition['table'])) {
            $table_list[] = $definition['table'];
        }

        if (isset($definition['lang'])){
            $table_list[] = $definition['table'] . '_lang';
        }

        //other table
        if (isset($definition['table_link'])) {
            foreach ($definition['table_link'] as $table_link) {
                $table_list[] = $table_link['name'];

                if ($table_link['lang']){
                    $table_list[] = $table_link['name'] . '_lang';
                }
            }
        }

        //connect
        $this->db_connect($this->path);        

        //Import data
        if (!empty($data) || isset($data)) {
            $result = array();

            // New class
            $theClass = new $class($this->user);
            $results = $theClass->importData($data, $time);

            if(is_array($results) && !empty($results)){
                foreach($results as $r){
                    if($class=="Product"){
                        $add = self::add($r,false,true,true); 
                    }else{
                    $add = self::add($r);
                    } 
                }
            }elseif(!is_array($results) ){
                $results = trim($results);
                if($class=="Product"){
                    if($status==1){ 
                        $add = self::add($results,false,true,true,true,!$from_task);
                        $this->forceRemoveOfferNotExistsItem();
                    }else{
                        $add = self::add($results,false,true,true,false); 
                    }
                    if(!$add){
                        
                    }
                }else{
                $add = self::add($results);
                }
            }
            
            if($update_setting){ 
               $this->db_product_connect($this->product_path);
               self::add($results,false,true,true,true,!$from_task,$this->product_conn_id);
            }

            //Commit transaction 
            if(isset($add) && !$add && !empty($results)){
                if($class=='Product' && $status==1){
//                    ob_start();
//                    var_dump($add);
//                    $add_var = ob_get_clean();
//                    file_put_contents('/var/www/backend/script/log/test_p2.txt', print_r(array(date('c'),$add_var,$class,$results),true),FILE_APPEND);
                    $result['error'] = sprintf('Import class %s error.', $class);
                }else if($class!='Product'){ 
                    $result['error'] = sprintf('Import class %s error.', $class);
                }
            } 

            if ($class == 'Category') {
                $Category = new Category($this->user);
                if (!$Category->updateTable($id_shop)) {
                    $result['error'] = sprintf('Category updateTable error. ');
                }
            }

            if ($class == 'Product') {
                $log = new Log($this->user);
                if (!$log->updateTable($id_shop, $time)) {
                    $result['error'] = sprintf('Log updateTable error. ');
                }
            }

            return $result;
        }

        //close
        $this->db_close();

        return false;
    }

    public function truncate_products($table_list, $time, $id_shop) {
        
        $result = array();
        if(!empty($table_list)&& is_array($table_list)){
            foreach ($table_list as $table) {
                foreach ($table as $t) {
                    if (!$this->delete($t, $time, $id_shop)) {
                        $result['error'] = sprintf('Delete table %s error. ', $t);
                        return $result;
                    }
                }
            }
        }

        return true;
    }

    public function insert($table, $data, $time = NULL)
    {
        if (!$data || empty($data))
            return false;

        // Check if $data is a list of row
        $current = current($data);
        if (!is_array($current) || isset($current['type']))
            $data = array($data);

        $keys = array();
        $values_stringified = array();
        foreach ($data as $row_data)
        {
            $values = array();
            foreach ($row_data as $key => $value)
            {
                if (isset($keys_stringified))
                {
                    // Check if row array mapping are the same
                    if (!in_array("$key", $keys))
                         return false;
                }
                else
                    $keys[$key] = "$key";

                if($value === '' || is_null($value))
                    $value = 'NULL';
                else if(is_int($value))
                    $value = (int)$value;
                else if(is_float($value))
                    $value = (float)$value;
                else
                    $value = "'{$value}'";

                $values[$key] = $value;
            }
            if(isset($time) && !empty($time) && $time != NULL)
            {
                $values['date_add'] = "'" . $time . "'";
            }

            $values_stringified[] = $values;
        }
        if(isset($time) && !empty($time) && $time != NULL)
        {
            $keys[] = "date_add";
        }

        $keys_stringified = implode(', ', $keys);

        foreach($values_stringified as $j => $values){
            $out=array();
            foreach($keys as $k){
                if(isset($values[$k])){
                $out[$k] = $values[$k];
                }else{
                    $out[$k] = 'NULL';
                }
            }
            $values_stringified[$j]= ' ('.implode(',',$out).') ';
        }
        $sep_no_row = 500;
        $sql='';
        if(sizeof($values_stringified)>$sep_no_row){
            $set_list = array();
            foreach($values_stringified as $i => $v){
                $set_no = floor($i/$sep_no_row);
                $set_list[$set_no][] = $v;
                unset($values_stringified[$i]);
            }
            foreach($set_list as $v){
                $sql .= 'REPLACE INTO '.$this->prefix_table .$table.' ('.$keys_stringified.') VALUES '.implode(', ', $v) . "; \n";
            }
        }else{
        $sql = 'REPLACE INTO '.$this->prefix_table .$table.' ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified) . "; \n";
        }

        return (string)$sql;
    }

    public function forceRemoveOfferNotExistsItem(){
        $sql = "DELETE offers_product_attribute FROM offers_product_attribute
            LEFT JOIN  products_product_attribute  ON offers_product_attribute.id_product = products_product_attribute.id_product
		AND offers_product_attribute.id_shop = products_product_attribute.id_shop
		AND offers_product_attribute.id_product_attribute = products_product_attribute.id_product_attribute
            WHERE products_product_attribute.reference IS NULL ";
        $this->db_exec($sql,false);

        $sql = "DELETE offers_product FROM offers_product
            LEFT JOIN  products_product  ON offers_product.id_product = products_product.id_product
		AND offers_product.id_shop = products_product.id_shop
            WHERE products_product.reference IS NULL ";
        $this->db_exec($sql,false);

        $sql="update  offers_product_attribute , products_product_attribute
            set offers_product_attribute.reference = products_product_attribute.reference
            where offers_product_attribute.id_product = products_product_attribute.id_product
		AND offers_product_attribute.id_shop = products_product_attribute.id_shop
		AND offers_product_attribute.id_product_attribute = products_product_attribute.id_product_attribute
                AND products_product_attribute.reference != offers_product_attribute.reference";
        $this->db_exec($sql,false);
        $sql="UPDATE offers_product, products_product
            SET offers_product.reference = products_product.reference
            WHERE offers_product.id_product = products_product.id_product
            AND offers_product.id_shop = products_product.id_shop
            AND products_product.reference != offers_product.reference";
        $this->db_exec($sql,false);
    }
    public function delete($table, $time, $id_shop) {

        if (!$table || !$time) {
            return false;
        }
        
        $query = "DELETE FROM " . $this->prefix_table .$table . " WHERE date_add < '" . $time . "' AND id_shop = " . (int)$id_shop . " ; ";
        if (!$this->db_exec($query)) {
            return FALSE;
        }

        return true;
    }

    public function update($table, $data, $where = array(), $limit = 0) {
        
        if (!$data) {
            return false;
        }

        $sql = 'UPDATE ' .$this->prefix_table . $table . ' SET ';

        foreach ($data as $key => $value) {
            $sql .= ($value === '' || is_null($value)) ? "$key = NULL," : "$key = '{$value}',";
        }

        $sql = rtrim($sql, ',');

        if (!empty($where)) {
            
            $sql .= ' WHERE ';
            $sql_where = array();
            foreach ($where as $key => $value) {
                $sql_where[] = $key . ' ' . $value['operation'] . ' ' . $value['value'];
            }

            $sql .= implode(' AND ', $sql_where);
        }

        if ($limit) {
            $sql .= ' LIMIT ' . (int) $limit;
        }

        return (string) $sql . '; ';
    }

    public function add($query_string,$transaction=true,$multi_query=true,$direct=false,$commit=true,$asyn=true,$conn=null)
    { 
        
        if (isset($query_string) && !empty($query_string)){
            if (!$this->db_exec($query_string,$transaction,$multi_query,$direct,$commit,$asyn,$conn)) {
                return false;
            }
        }
        return true;
    }

    public static function truncate_database($user) {
        $oldmask = umask(0);
        $conn_id = new Db($this->user, ObjectModel::$database);
        $sql = '';

        if (!$conn_id) {
            return false;
        }

        $exec = $conn_id->db_query_str("show tables");
        foreach ($exec as $table) {
            
            $sql .= "DELETE FROM " . $this->prefix_table .reset($table) . "; ";
        }
        $conn_id->db_exec('BEGIN'); 
        if (!$conn_id->db_exec($sql)) {
            return false;
        }

        $conn_id->db_exec('COMMIT');

        $token = new OTP($user);
        if (!$token->generate_token()) {
            return false;
        }

        return true;
    }

    public function truncate_table($table, $where = null) {
        
        if (!$this->db_exec("DELETE FROM " . $this->prefix_table .$table . " " . isset($where) ? $where : '' . "; ")){
            return false;
        }

        return true;
    }

    public function db_connect($location) {
        $error = '';
        $oldmask = umask(0);
        $this->conn_id = new Db($this->user, ObjectModel::$database, false);
        umask($oldmask);	
    }
    
    public function db_product_connect($location) {
        $error = '';
        $oldmask = umask(0);
        $this->product_conn_id = new Db($this->user, ObjectModel::$product_database, false);
        umask($oldmask);
    }
    
    public function db_table_exists($table, $no_prefix){
        return $this->conn_id->db_table_exists($table, $no_prefix);
    }

    public function db_exec($query,$transaction=false,$multi_query=true,$direct=false,$commit=true,$asyn=true,$conn=null) 
    { 
        $error = '';
        if(!$query || empty($query) || !isset($query)||empty($this->conn_id))
            return FALSE;

        if(empty($conn)){
            $conn = $this->conn_id;
        }

        $exec = $conn->db_exec($query,$transaction,$multi_query,$direct,$commit,$asyn); 

        if (!$exec){
            return $error;
        }
        
        return $exec;
    }

    public function db_query($query) {
        
        if (!isset($query) || empty($query) || !$query){
            return FALSE;
        }

        $exec = $this->conn_id->db_sqlit_query($query);
        return $exec;
    }

    public function db_query_str($query) {
        
        if (!isset($query) || empty($query) || !$query) {
            return FALSE;
        }

        $exec = $this->conn_id->db_query_str($query);
        return $exec;
    }

    public function db_num_rows($result) {
        
        if (!isset($result) || empty($result) || !$result) {
            return FALSE;
        }

        $exec = $this->conn_id->db_num_rows($result);
        return $exec;
    }

    public function db_last_insert_rowid() {
        return $this->conn_id->db_last_insert_rowid();
    }

    public function db_close() {
        return $this->conn_id->db_close();
    }

    public function db_trans_begin($conn=null) {
        if(empty($conn))$conn = $this->conn_id;
        $conn->db_exec('BEGIN');
        return TRUE;
    }

    public function db_trans_commit($conn=null) {
        if(empty($conn))$conn = $this->conn_id;
        $conn->db_exec('COMMIT');
        return TRUE;
    }

    public static function escape_str($str) {
        if (is_array($str)) {
            foreach ($str as $key => $val) {
                $str[$key] = $this->escape_str($val);
            }
            return $str;
        }

        $str = Db::escape_string($str);
        return $str;
    }

    public static function append_simplexml(&$simplexml_to, &$simplexml_from) {

        static $firstLoop = true;

        //Here adding attributes to parent            
        if ($firstLoop) {
            foreach ($simplexml_from->attributes() as $attr_key => $attr_value) {
                $simplexml_to->addAttribute($attr_key, $attr_value);
            }
        }

        foreach ($simplexml_from->children() as $simplexml_child) {
            $simplexml_temp = $simplexml_to->addChild($simplexml_child->getName(), (string) $simplexml_child);
            foreach ($simplexml_child->attributes() as $attr_key => $attr_value) {
                $simplexml_temp->addAttribute($attr_key, $attr_value);
            }
            $firstLoop = false;
            append_simplexml($simplexml_temp, $simplexml_child);
        }

        $firstLoop = true;
        unset($firstLoop);
    }
    
    public static function get_marketplace_by_id($id) {
	
	$name = '';
	$mysql_db = new ci_db_connect();  
        $query = $mysql_db->select_query("SELECT name_offer_pkg FROM offer_packages WHERE id_offer_pkg = ".(int)$id.";" );
	
        while($row = $mysql_db->fetch($query)){ 
	    $name = $row['name_offer_pkg'];
	}
	
        return $name;
    }
    
    public static function get_countries() {
	
	$countries = array();
	$mysql_db = new ci_db_connect();  
        $query = $mysql_db->select_query("SELECT * FROM offer_sub_packages WHERE iso_code IS NOT NULL;" );
	
        while($row = $mysql_db->fetch($query)){ 
	    $countries[$row['iso_code']]['id'] = $row['id_offer_sub_pkg'];
	    $countries[$row['iso_code']]['name'] = $row['title_offer_sub_pkg'];
	    $countries[$row['iso_code']]['iso_code'] = $row['iso_code'];
	}
	
        return $countries;
    }
}
