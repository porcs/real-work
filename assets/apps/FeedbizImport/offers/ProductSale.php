<?php

class ProductSale extends ObjectModel
{
    public static $definition = array(
        'table'     => 'product_sale',
        'promary'   => array('id_product','id_product_attribute', 'date_from', 'date_to', 'id_shop'),
        'unique'    => array('product_sale' => array('id_product','id_product_attribute', 'date_from', 'date_to', 'id_shop')),
        'fields'    => array(
            'id_product'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
			'id_product_attribute'  =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'date_from'         =>  array('type' => 'datetime', 'required' => false, 'default_null' => true ),
            'date_to'           =>  array('type' => 'datetime', 'required' => false, 'default_null' => true ),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'reduction'         =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'reduction_type'    =>  array('type' => 'varchar', 'required' => false, 'size' => 16 ),
            'price'             =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
   
    
}