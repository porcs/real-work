<?php

class StockMovement extends ObjectModel
{
    public function __construct( $user, $id = null, $connect = true)
    {
        parent::__construct( $user, $id , $connect );  
        $this->user = $user;
    }
    
    public static $definition = array(
        'table'     => 'stock_mvt',
        'primary' => array('id_stock_mvt'),
        'fields'    => array(
            'id_stock_mvt'  => array('type' => 'INTEGER', 'required' => true, 'primary_key' => true),
            'id_stock'      => array('type' => 'INTEGER', 'required' => true),
            'id_shop'       => array('type' => 'INTEGER', 'required' => true),
            'action'        => array('type' => 'varchar', 'required' => false, 'size' => 64 ),
            'type'          => array('type' => 'varchar', 'required' => false, 'size' => 64 ),
            'source'        => array('type' => 'varchar', 'required' => false, 'size' => 64 ),
            'id_ref'        => array('type' => 'INTEGER', 'required' => false),
            'quantity'      => array('type' => 'int', 'required' => true, 'size' => 10 ),
            'movement_qty'     => array('type' => 'int', 'required' => true, 'size' => 10 ),
            'balance_qty'   => array('type' => 'int', 'required' => true, 'size' => 10 ),
            'date_add'      => array('type' => 'datetime', 'required' => true ),
        ),
       
    );  
}