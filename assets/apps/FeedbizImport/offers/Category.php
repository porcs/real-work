<?php

class Category extends ObjectModel
{
    public static $definition = array(
        'table'     => 'category',
        'unique'    => array('category' => array('id_category', 'id_shop')),
        'fields'    => array(
            'id_category'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_parent'         =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'is_root_category'  =>  array('type' => 'tinyint', 'required' => true, 'size' => 1),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ), 
        'lang' => array(
           'fields'    => array(
               'id_category'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
               'id_lang'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
               'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
               'name'          =>  array('type' => 'varchar', 'required' => false, 'size' => 128, 'default_null' => true ),
               'date_add'      =>  array('type' => 'text', 'required' => true ),
           ),
        ),
    );
    
    public function createCategorySelectedTable()
    {
        $query = 'CREATE TABLE '.$this->prefix_table.'category_selected (
                id_category_selected  INTEGER AUTO_INCREMENT,
                id_category  int(10) NOT NULL  ,
                id_profile  int(10) NOT NULL  ,
                id_shop  int(10) NOT NULL  ,
                id_mode  int(10) NOT NULL  ,
                date_add  datetime NOT NULL 
                ,PRIMARY KEY( id_category_selected,id_category,id_profile,id_shop,id_mode)
                ) ; ' ;

        if(!$this->db_exec($query,false))
            return false;
        
        return true;
    }
    
    /* $return Query (string) */
    public function importData($data, $time)
    {
        //Category
        foreach($data as $CategoriesXml)
        {
            $sql = '';
            $id_shop = $CategoriesXml->id_shop;
            $categorie = array();
            
            foreach ($CategoriesXml->Category as $Categories)
            {
                $categorie['id_category'] = (int)ObjectModel::escape_str($Categories['ID']);
                $categorie['id_parent'] = (int)ObjectModel::escape_str($Categories['parent']);
                $categorie['is_root_category'] = 0;
                $categorie['id_shop'] = (int)$id_shop;
                
                if($Categories['parent'] == "root")
                {
                    $categorie['id_parent'] = 0;
                    $categorie['is_root_category'] = 1;
                }
                
                if(empty($Categories['parent']) || $Categories['parent'] == '')
                    $categorie['id_parent'] = 1;
                
                $sql .= self::insert('category', $categorie, $time);
                
                $category_lang = array();
                foreach ($Categories->Name as $category_name_value)
                {
                    $category_lang['id_category'] = $categorie['id_category'];
                    $category_lang['id_lang'] = (int)$category_name_value['lang'];
                    $category_lang['id_shop'] = (int)$id_shop;
                    $category_lang['name'] = ObjectModel::escape_str($category_name_value);
                    
                    $sql .= self::insert('category_lang', $category_lang, $time);
                } 
            }
        }
        
       return $sql;
    }
    
     public function updateTable($id_shop)
    {  
        $del_query = '';
        if( !is_numeric($id_shop )) return true;
        
        $del_query .= "DELETE FROM {$this->prefix_table}category_selected "
        . "WHERE id_category NOT IN (SELECT id_category FROM {$this->prefix_table}category WHERE id_shop = '" . $id_shop . "') AND id_shop = '" . $id_shop . "';";
        
        if(!$this->db_exec($del_query))
            return FALSE;
        
        return TRUE; 
    }
}