<?php

class Stock extends ObjectModel
{
    public function __construct( $user, $id = null, $connect = true)
    {
        parent::__construct( $user, $id , $connect );  
        $this->user = $user;
    }

    public static $definition = array(
        'table'     => 'stock',
        'primary' => array('id_stock'),
        'fields'    => array(
            'id_stock'              =>  array('type' => 'INTEGER', 'required' => true, 'primary_key' => true),
            'id_shop'               =>  array('type' => 'INTEGER', 'required' => true),
            'id_product'            =>  array('type' => 'INTEGER', 'required' => false,),
            'id_product_attribute'  =>  array('type' => 'INTEGER', 'required' => false,),
            'name'                  =>  array('type' => 'varchar', 'required' => false, 'size' => 128),
            'reference'             =>  array('type' => 'varchar', 'required' => false, 'size' => 64 ),
            'sku'                   =>  array('type' => 'varchar', 'required' => false, 'size' => 64 ),
            'ean13'                 =>  array('type' => 'varchar', 'required' => false, 'size' => 13 ),
            'upc'                   =>  array('type' => 'varchar', 'required' => false, 'size' => 12 ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );  
}