<?php

class Rule extends ObjectModel
{
   
    public static $definition = array(
        'table'     => 'rule',
        'unique'    => array('rule' => array('id_rule', 'id_shop')),
        'fields'    => array(
            'id_rule'           =>  array('type' => 'INTEGER', 'required' => false, 'primary_key' => true),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_mode'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'              =>  array('type' => 'text', 'required' => true ),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
   
    public function createTableItems() 
    {
        $query = "CREATE TABLE {$this->prefix_table}rule_item (id_rule_item INTEGER PRIMARY KEY auto_increment, id_rule int(10), id_supplier int(10), id_manufacturer int(10), id_shop int(10), id_mode int(10), action int(10) ); " ;
        
        if(!$this->db_exec($query,false))
            return false;
        
        $query  = "CREATE TABLE {$this->prefix_table}rule_item_price_range (id_rule_item_price_range INTEGER PRIMARY KEY auto_increment , id_rule_item int(10), id_shop int(10), price_range_from decimal(20,6), price_range_to decimal(20,6), id_mode int(10) ); " ;
        
        if(!$this->db_exec($query,false))
            return false;
        
        return true;
    }
    
}