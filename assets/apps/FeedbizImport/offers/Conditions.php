<?php

class Conditions extends ObjectModel
{   
    public static $definition = array(
        'table'     => 'conditions',
        'unique'    => array('conditions' => array('id_condition', 'id_shop')),
        'fields'    => array(
            'id_condition'  =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    /*$return Query (string)*/
    public function importData($data, $time)
    {            
        //Conditions
        foreach($data as $ConditionsXml)
        {
            $sql = '';
            $id_shop = $ConditionsXml->id_shop;
            $conditions = array();
            
            foreach ($ConditionsXml->Condition as $Conditions)
            {
                $conditions['id_condition'] = (int)ObjectModel::escape_str($Conditions['ID']);
                $conditions['name'] = ObjectModel::escape_str($Conditions);
                $conditions['id_shop'] = (int)$id_shop;
                
                $sql .= self::insert('conditions', $conditions, $time);          
            }
        }
        
        return $sql;
    }
       
}