<?php

class MarketplaceOfferOption extends ObjectModel
{   
    private static $table_definition = '_product_option';    
    
    public $table;
    public $marketplace; 
    public $use_site = false;
    
    public static $tables = array(
	'Main'  => 'marketplace',
	'Amazon'  => 'amazon',
	'Ebay'  => 'ebay',
    );
    
    public static $fields = array(
	
	    'keys'  => array(
		'id_product'		    =>  array('type' => 'int', 'size' => 11, 'require' => 'NOT NULL' ),
                'id_product_attribute'	    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
                'sku'			    =>  array('type' => 'varchar', 'size' => 64, 'require' => '' ),
                'id_shop'		    =>  array('type' => 'int', 'size' => 11, 'require' => 'NOT NULL' ), 
		'id_country'		    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
		'id_lang'		    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
	    ),
	
	    'main'  => array(
                'c_force'		    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
                'no_price_export'	    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'no_quantity_export'	    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'latency'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'c_disable'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'price'			    =>  array('type' => 'decimal', 'size' => '20,6', 'require' => '' ), 
		'shipping'		    =>  array('type' => 'decimal', 'size' => '20,6', 'require' => '' ),
                'shipping_type'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
		'text'			    =>  array('type' => 'varchar', 'size' => 256, 'require' => '' ),
		'flag_update'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
	    ),	
	    
	    'amazon'  => array(
		'fba'                       =>  array('type' => 'tinyint', 'size' => 4 ),
		'fba_value'                 =>  array('type' => 'decimal', 'size' => '20,6' ),
		'asin1'                     =>  array('type' => 'varchar', 'size' => 16 ),
		'asin2'                     =>  array('type' => 'varchar', 'size' => 16 ),
		'asin3'                     =>  array('type' => 'varchar', 'size' => 16 ),
		'bullet_point1'             =>  array('type' => 'varchar', 'size' => 500 ),
		'bullet_point2'             =>  array('type' => 'varchar', 'size' => 500 ),
		'bullet_point3'             =>  array('type' => 'varchar', 'size' => 500 ),
		'bullet_point4'             =>  array('type' => 'varchar', 'size' => 500 ),
		'bullet_point5'             =>  array('type' => 'varchar', 'size' => 500 ),
		'browsenode'               =>  array('type' => 'varchar', 'size' => 256 ),
		'gift_wrap'                 =>  array('type' => 'tinyint', 'size' => 4 ),
		'gift_message'              =>  array('type' => 'tinyint', 'size' => 4 ),
                'shipping_group'            =>  array('type' => 'varchar', 'size' => 256 ),
	    ),
	
	    'amazon_repricing_product_strategies'  => array(
		'id_product'		    =>  '',
		'id_product_attribute'	    =>  '',
		'id_shop'		    =>  '', 
		'id_country'		    =>  '',
		'sku'			    =>  '',
		'minimum_price'		    =>  'Repricing_min',
		'target_price'		    =>  'Repricing_max',
	    )
    );    
    
    public static $repricing_tables = array(
	'amazon'  => 'amazon_repricing_product_strategies',
    );    
    
    public function __construct($user, $id = null, $connect = null) {
	
        parent::__construct($user, $id, $connect);
        $this->user = $user;
    }
    
    public function checke_table_exit($marketplace = null) {
        
        $_db = $this->conn_id;
	
	$marketplace = strtolower(isset(self::$tables[$marketplace]) ? self::$tables[$marketplace] : $marketplace);  
        $table = $marketplace . self::$table_definition;
	
	if(!$_db->db_table_exists($table, true)){
	    return $this->create_product_option($marketplace);  
	} else {
//	    $this->alter_table($marketplace);
	}
	
	return true;
    }        
      
    private function alter_table($marketplace){	
	
	$_db = $this->conn_id;	
	$table = $marketplace . self::$table_definition;
	$exclude_field = $_db->get_all_column_name($table, true, true);
	$marketplace_field = 0;
	
	if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
	    $marketplace_field = sizeof(self::$fields[$marketplace]);
	}
	//if(sizeof($exclude_field) == (sizeof(self::$fields['keys']) + sizeof(self::$fields['main'])+ $marketplace_field + 2)) {
	if(sizeof($exclude_field) == 17 || sizeof($exclude_field) == 30) {
	    return;
	}	
        if($_db->db_exec("DROP TABLE ".$table." ; ", false)){
            return $this->create_product_option($marketplace);
        }
        return false;
    }
     
    private function create_product_option($marketplace = null) {
	
        $site_primary_key = $fields = '';
        $_db = $this->conn_id;
	$table = $marketplace . self::$table_definition;
	
	if($marketplace != 'marketplace') {
	    $site_primary_key .= "id_country";
	} else {
	    $site_primary_key .= "id_lang";
	}
	 
	foreach (self::$fields['keys'] as $main_fields => $main_values){
	    $fields .= "$main_fields  ".$main_values['type']." (".$main_values['size'].") ".$main_values['require'].", ";
	}
	 
	foreach (self::$fields['main'] as $main_fields => $main_values){
	    $fields .= "$main_fields  ".$main_values['type']." (".$main_values['size'].") ".$main_values['require'].", ";
	}
	
	if($marketplace != 'main'){
	    if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
		foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
		    $fields .= "$marketplace_fields  ".$marketplace_values['type']." (".$marketplace_values['size']."), ";
		}
	    }
	}
        $query = "CREATE TABLE ".$table." ( ".
		$fields
		."date_add datetime,
                date_upd datetime,
                PRIMARY KEY (id_product, id_product_attribute, id_shop, $site_primary_key)
              ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;";
       
        if($_db->db_exec($query,false)){
            return true;
        }
        return false;
    } 
        
    public function get_product_option_by_ids($id_shop, $id_product_from, $id_product_to)
    {			
    	$return = array();
    	$_db = $this->conn_id;
    	
	foreach (self::$tables as $marketplace => $value)
	{
	    $table_name = $marketplace . self::$table_definition;
	    
	    if(!$_db->db_table_exists($table_name, true)) 
	    {
		    return;
	    }
	    
	    $sql = "SELECT * FROM $table_name "
	    . "WHERE id_product BETWEEN '" . $id_product_from . "' AND '" . $id_product_to . "' "
	    . "AND id_shop = ".(int)$id_shop." ";

	    $result = $_db->db_query_str($sql . "; ");

	    if(isset($result) && is_array($result)){
		foreach ($result as $row){

		    if(!isset($row['id_product_attribute']) || empty($row['id_product_attribute']) )
			$row['id_product_attribute'] = 0;
		    
		    $site = isset($row['id_country']) ? $row['id_country'] : ( isset($row['id_lang']) ? $row['id_lang'] : 0 ) ;
		    
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['id_country']	     = isset($row['id_country'])?$row['id_country']:null; 
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['id_product']	     = $row['id_product'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['id_product_attribute']   = $row['id_product_attribute'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['sku']                    = $row['sku'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['id_shop']                = $row['id_shop'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['id_lang']                = $row['id_lang'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['force']                  = $row['c_force'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['no_price_export']        = $row['no_price_export'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['no_quantity_export']     = $row['no_quantity_export'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['latency']                = $row['latency'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['disable']                = $row['c_disable'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['price']                  = $row['price'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['text']                   = $row['text'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['shipping']               = $row['shipping'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['shipping_type']          = $row['shipping_type'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['date_add']               = $row['date_add'];
		    $return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']]['date_upd']               = $row['date_upd'];
		    
		    
		    if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
			foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
			    if(isset($row[$marketplace_fields])) {
				$return[$marketplace][$site][$row['id_product']][$row['id_product_attribute']][$marketplace_fields] = $row[$marketplace_fields];
			    }
			}
		    }
		}
	    }
	}
	
    	return $return;
    }
    
    public function get_product_strategies($id_shop, $id_product_from, $id_product_to){   
	
        $product_list = array();  
	
	$_db = $this->conn_id;
    	
	foreach (self::$repricing_tables as $marketplace => $table_name)
	{
	    
	    if(!$_db->db_table_exists($table_name, true)) 
	    {
		    return;
	    }
	    
	    $sql = "SELECT * FROM $table_name WHERE id_product BETWEEN '" . $id_product_from . "' AND '" . $id_product_to . "' AND id_shop = ".(int)$id_shop."; ";
	    $result = $_db->db_query_str($sql);
	    
            foreach ($result as $key => $data){
		
		$site = $data['id_country'];
		$product_list[$marketplace][$site][$data['id_product']][$data['id_product_attribute']]['id_shop'] = $data['id_shop'];
		$product_list[$marketplace][$site][$data['id_product']][$data['id_product_attribute']]['id_country'] = $data['id_country'];
		$product_list[$marketplace][$site][$data['id_product']][$data['id_product_attribute']]['id_product'] = $data['id_product'];
		$product_list[$marketplace][$site][$data['id_product']][$data['id_product_attribute']]['id_product_attribute'] = $data['id_product_attribute'];
		$product_list[$marketplace][$site][$data['id_product']][$data['id_product_attribute']]['sku'] = $data['sku'];  
		$product_list[$marketplace][$site][$data['id_product']][$data['id_product_attribute']]['minimum_price'] = $data['minimum_price'];
		$product_list[$marketplace][$site][$data['id_product']][$data['id_product_attribute']]['target_price'] = $data['target_price'];
	    }
	}
	
	return $product_list;
       
    }
    
    public function set_offers_options($ProductOffersOptions, $marketplace, $CurrentOfferOptions = null){


	$sql = array();
	$date = date('Y-m-d H:i:s');
	$marketplace = strtolower($marketplace);
	$product_update_type = 'repricing';
	$product_update_status = 'reprice';
	$flag_update = false ;        
        
	if(isset($ProductOffersOptions->id_product) && isset($ProductOffersOptions->id_shop)) { 		
	    
	    $sql['options'] = '(';

	    foreach (self::$fields['keys'] as $main_fields => $main_values){		
		$sql['options'] .= isset($ProductOffersOptions->$main_fields) ? "'".strval($ProductOffersOptions->$main_fields)."'," : "null,";
	    }

	    foreach (self::$fields['main'] as $main_fields => $main_values){
		$main_fields = ucfirst(str_replace('c_', "", $main_fields));
		$sql['options'] .= isset($ProductOffersOptions->$main_fields) ? "'".strval($ProductOffersOptions->$main_fields)."'," : "null,";

                if(isset($ProductOffersOptions->$main_fields)){
                    if(isset($CurrentOfferOptions['options'][$main_fields]) && $ProductOffersOptions->$main_fields != $CurrentOfferOptions['options'][$main_fields])
                        $flag_update = true;
                    elseif(!isset($CurrentOfferOptions['options'][$main_fields]))
                        $flag_update = true;
                }
                
	    }
	    
	    if($marketplace != 'main'){
		
		if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
		    
		    foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
			
			$fields = $marketplace_fields;
			$marketplace_fields = ucfirst($marketplace_fields);			
			
			$sql['options'] .= isset($ProductOffersOptions->$marketplace_fields) ? "'".strval($ProductOffersOptions->$marketplace_fields)."'," : "null,";

                        if(isset($ProductOffersOptions->$marketplace_fields)){
                            if(isset($CurrentOfferOptions['options'][$marketplace_fields])
                                && $ProductOffersOptions->$marketplace_fields != $CurrentOfferOptions['options'][$marketplace_fields])
                                $flag_update = true;
                            elseif(!isset($CurrentOfferOptions['options'][$marketplace_fields]))
                                $flag_update = true;
                        }

		    }
		    
		    // Trigger repricing when Offer Option has Changed
		    // 
		    // if in allow marketplace e.g : amazon
		    if(array_key_exists(strtolower($marketplace), self::$repricing_tables)){
			
			// If (CURRENT) has Fba
			if(isset($CurrentOfferOptions['options']['fba']) && $CurrentOfferOptions['options']['fba'] == 1){
			
			    $trigger = false; 

			    // If (NEW) has Fba
			    if(isset($ProductOffersOptions->Fba) && strval($ProductOffersOptions->Fba) == 1 ) {

				// if (NEW) has Fba Value
				if(isset($ProductOffersOptions->Fba_value)) {
				    
				    $trigger = true; // because New Fba Value added
				    $type = 'New Fba Value added';
				    
				    // If value is match
				    if( isset($CurrentOfferOptions['options']['fba_value']) &&
					(float)$ProductOffersOptions->Fba_value  == (float)$CurrentOfferOptions['options']['fba_value'] ){
					
					$trigger = false; // because the value match
				    }	

				// if (NEW) don't has Fba Value
				} else {

				    // if (CURRENT) has Fba Value
				    if(isset($CurrentOfferOptions['options']['fba_value'])) {
					
					$trigger = true; // because New Fba Value removed
					$type = 'New Fba Value removed';
				    } 
				}				   

			    // If (NEW) don't has Fba    
			    } else {
				
				$trigger = true; // because FBA removed
				$type = 'NEW FBA removed';
			    }
			    
			    if($trigger){

				$sql['trigger_repricing']['product_update_log'] = "("
					. (int)$CurrentOfferOptions['options']['id_product']. ", "
					. (int)$CurrentOfferOptions['options']['id_product_attribute']. ", "
					. (int)$CurrentOfferOptions['options']['id_shop']. ", "
					. "'" .$product_update_type. "', "
					. "'" .$product_update_status. "', "
					. "'" .$date . "'" 
					. ")" ;

				$sql['trigger_repricing']['repricing_flag'] = "("
					. (int)$CurrentOfferOptions['options']['id_product']. ", "
					. (int)$CurrentOfferOptions['options']['id_product_attribute']. ", "
					. "'" .$CurrentOfferOptions['options']['sku']. "', "
					. (int)$CurrentOfferOptions['options']['id_shop']. ", "
					. (int)$CurrentOfferOptions['options']['id_country']. ", "
					. "1, "
					. "'" .$date . "'" 
					. ")" ;
			    }

			// If (CURRENT) don't has Fba
			} else {
			    
			     // If (NEW) has Fba
			    if(isset($ProductOffersOptions->Fba) && strval($ProductOffersOptions->Fba) == 1 ) {
				    
				$type = 'New Fba added';

				$sql['trigger_repricing']['product_update_log'] = "("
					. (int)$ProductOffersOptions->id_product. ", "
					. (int)$ProductOffersOptions->id_product_attribute. ", "
					. (int)$ProductOffersOptions->id_shop. ", "
					. "'" .$product_update_type. "', "
					. "'" .$product_update_status. "', "
					. "'" .$date . "'" 
					. ")" ;	

				$sql['trigger_repricing']['repricing_flag'] = "("
					. (int)$ProductOffersOptions->id_product. ", "
					. (int)$ProductOffersOptions->id_product_attribute. ", "
					. "'" .strval($ProductOffersOptions->sku). "', "
					. (int)$ProductOffersOptions->id_shop. ", "
					. (int)$ProductOffersOptions->id_country. ", "
					. "1, "
					. "'" .$date . "'" 
					. ")" ;
			    } 
			}
		    }
		}
	    }

	    $sql['options'] .= "'".$date."')";
	}
	
	# repricing
	if(isset(self::$repricing_tables[$marketplace])){
	   
	    $table_repricing = strtolower(self::$repricing_tables[$marketplace]); 
	    
	    if(isset(self::$fields[$table_repricing]) && !empty(self::$fields[$table_repricing])){
		
		$sql['repricing'] = '(';
		
		foreach (self::$fields[$table_repricing] as $repricing_fields => $repricing_values){		    
		    
		    $sql['repricing'] .= isset($ProductOffersOptions->$repricing_fields) ? "'".strval($ProductOffersOptions->$repricing_fields)."'," : 
			(isset($ProductOffersOptions->$repricing_values) ? "'".strval($ProductOffersOptions->$repricing_values)."'," : "null,");		    
		}
		
		$sql['repricing'] .= "'".$date."')";
		
		// Trigger repricing when Offer Option has Changed
		// 
		// if in allow marketplace e.g : amazon
		if(array_key_exists(strtolower($marketplace), self::$repricing_tables)){
			
		    // If (CURRENT) has repricing
		    if(isset($CurrentOfferOptions['repricing']['minimum_price']) && isset($CurrentOfferOptions['repricing']['target_price'])){

			$trigger = false;

			// If (NEW) has repricing
			if(isset($ProductOffersOptions->Repricing_min) || isset($ProductOffersOptions->Repricing_max)){

                            $trigger = true ;

                            if( (isset($CurrentOfferOptions['repricing']['minimum_price'])
                                && $CurrentOfferOptions['repricing']['minimum_price'] == (float)$ProductOffersOptions->Repricing_min)
                            ||  (isset($CurrentOfferOptions['repricing']['target_price'])
				&& $CurrentOfferOptions['repricing']['target_price'] == (float)$ProductOffersOptions->Repricing_max)    ) { // when not match
				$type = 'minimum price or target price not match';
                                $trigger = false ;
			    } 

			// If (NEW) don't has repricing
			} else {

			    $trigger = true ;
			    $type = 'minimum and target price has removed';
			}

			if($trigger){

			    $sql['trigger_repricing']['product_update_log'] = "("
				    . (int)$CurrentOfferOptions['repricing']['id_product']. ", "
				    . (int)$CurrentOfferOptions['repricing']['id_product_attribute']. ", "
				    . (int)$CurrentOfferOptions['repricing']['id_shop']. ", "
				    . "'" .$product_update_type. "', "
				    . "'" .$product_update_status. "', "
				    . "'" .$date . "'" 
				    . ")" ;

			    $sql['trigger_repricing']['repricing_flag'] = "("
				    . (int)$CurrentOfferOptions['repricing']['id_product']. ", "
				    . (int)$CurrentOfferOptions['repricing']['id_product_attribute']. ", "
				    . "'" .$CurrentOfferOptions['repricing']['sku']. "', "
				    . (int)$CurrentOfferOptions['repricing']['id_shop']. ", "
				    . (int)$CurrentOfferOptions['repricing']['id_country']. ", "
				    . "1, "
				    . "'" .$date . "'" 
				    . ")" ;
			}

		    // If (CURRENT) don't has repricing
		    } else {
			
			if(isset($ProductOffersOptions->Repricing_min) || isset($ProductOffersOptions->Repricing_max)){
			    
			    // If (NEW) has repricing
			    $type = 'minimum & target price has added';
			    $sql['trigger_repricing']['product_update_log'] = "("
				    . (int)$ProductOffersOptions->id_product. ", "
				    . (int)$ProductOffersOptions->id_product_attribute. ", "
				    . (int)$ProductOffersOptions->id_shop. ", "
				    . "'" .$product_update_type. "', "
				    . "'" .$product_update_status. "', "
				    . "'" .$date . "'" 
				    . ")" ;	

			    $sql['trigger_repricing']['repricing_flag'] = "("
				    . (int)$ProductOffersOptions->id_product. ", "
				    . (int)$ProductOffersOptions->id_product_attribute. ", "
				    . "'" .strval($ProductOffersOptions->sku). "', "
				    . (int)$ProductOffersOptions->id_shop. ", "
				    . (int)$ProductOffersOptions->id_country. ", "
				    . "1, "
				    . "'" .$date . "'" 
				. ")" ;
			}
		    }
		}
	    }
	}

        $sql['flag_update'] = $flag_update;
        
	return $sql;
    }
    
    public function save_repricing($ProductOptions){
	
	$sql = '';
	if(isset($ProductOptions) && !empty($ProductOptions)){
	    
	    foreach ($ProductOptions as $marketplace => $offers){
		
		$marketplace = strtolower($marketplace);
		$marketplace = isset(self::$repricing_tables[$marketplace]) ? self::$repricing_tables[$marketplace] : $marketplace;  
		    
		// check table exit
		if($this->conn_id->db_table_exists($marketplace, true)){

		    $table = $marketplace ;		    
		    $keys_stringified = array();
		    
		    if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
			foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
			    $keys_stringified[] = $marketplace_fields;
			}
		    }
		    
                    if(is_array($offers)){
                        $sep_no_row = 500;

                        if(sizeof($offers)>$sep_no_row){
                            $set_list = array();
                            foreach($offers as $i => $v){
                                $set_no = floor($i/$sep_no_row);
                                $set_list[$set_no][] = $v;
                                unset($offers[$i]);
                            }
                            foreach($set_list as $v){
                                $sql .= 'REPLACE INTO '. $table.' ('.implode(', ', $keys_stringified).', date_upd) VALUES '.implode(', ', $v) . "; \n";
                            }
                        }else{
                        $sql .= 'REPLACE INTO '. $table.' ('.implode(', ', $keys_stringified).', date_upd) VALUES '.implode(', ', $offers) . "; \n";
                        }
                    }else if(!empty($offers) && is_string ($offers)){
                        $sql .= 'REPLACE INTO '.$table.' ('.implode(', ', $keys_stringified).', date_upd) VALUES '. trim($offers, ", ") . "; ";
                    }
		 
		    unset($keys_stringified);
		}
	    }
	}
		
	return $sql;
    }  
    
    public function save_offers_options($ProductOptions){
	
	$sql = '';
	if(isset($ProductOptions) && !empty($ProductOptions)){
	    
	    foreach ($ProductOptions as $marketplace => $offers){
		    $marketplace = strtolower(isset(self::$tables[$marketplace]) ? self::$tables[$marketplace] : $marketplace);   		    
		    $table = $marketplace . self::$table_definition;		    
		    $keys_stringified = array();
		    foreach (self::$fields['keys'] as $main_fields => $main_values){
			$keys_stringified[] = $main_fields;
		    }
		    foreach (self::$fields['main'] as $main_fields => $main_values){
			$keys_stringified[] = $main_fields;
		    }
		    if($marketplace != 'main'){
			if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
			    foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
				$keys_stringified[] = $marketplace_fields;
			    }
			}
		    }

                    if(is_array($offers)){
                        $sep_no_row = 500;

                        if(sizeof($offers)>$sep_no_row){
                            $set_list = array();
                            foreach($offers as $i => $v){
                                $set_no = floor($i/$sep_no_row);
                                $set_list[$set_no][] = $v;
                                unset($offers[$i]);
                            }
                            foreach($set_list as $v){
                                $sql .= 'REPLACE INTO '. $table.' ('.implode(', ', $keys_stringified).', date_add) VALUES '.implode(', ', $v) . "; \n";
                            }
                        }else{
                        $sql .= 'REPLACE INTO '. $table.' ('.implode(', ', $keys_stringified).', date_add) VALUES '.implode(', ', $offers) . "; \n";
                        }
                    }else if(!empty($offers) && is_string($offers)){
                        $sql .= 'REPLACE INTO '.$table.' ('.implode(', ', $keys_stringified).', date_add) VALUES '. trim($offers, ", ") . "; ";
                    }
		    unset($keys_stringified);
	    }
	}
	
	return $sql;
    }  
    
    public function update_repricing_flag($ProductOptions){
        
        $sql = '';
	
	if($this->conn_id->db_table_exists('amazon_repricing_flag', true)){
	    
	    if(isset($ProductOptions) && !empty($ProductOptions)){

		foreach ($ProductOptions as $marketplace => $offers){

		    if(array_key_exists(strtolower($marketplace), self::$repricing_tables)) {

			$sql .= " REPLACE INTO log_product_update_log (id_product, id_product_attribute, id_shop, product_update_type, product_update_status, date_add)"
			      . " VALUES ". trim($offers['product_update_log'], ", ") ."; " ;

			$sql .= 'REPLACE INTO amazon_repricing_flag (id_product, id_product_attribute, sku, id_shop, id_country, flag, date_upd)'
			    . ' VALUES '. trim($offers['repricing_flag'], ", ") . "; ";
		    }
		}
	    }       
	}
           
	return $sql;
      
    }
    
}