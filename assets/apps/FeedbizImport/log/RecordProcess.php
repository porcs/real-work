<?php
if(!defined('BASE_DIRX'))
    define('BASE_DIRX',dirname(__FILE__).'/../..');
define('DEF_ASYN_DELAY',1);//2sec
define('DEF_ASYN_TIMEOUT',6);//6sec
define('DEF_STORE_FILE',BASE_DIRX.'/fifo_dir/');
define('DEF_FIFO_PATERN',"proc_{user}_{pid}{host}.ff");
define('DEF_FMSG_PATERN',"proc_{user}_{pid}{host}.txt");
//define('DEF_FIFO_PATERN',"proc_{user}.ff");
define('PROC_NSTR',0);
define('PROC_STR',1);
define('PROC_ERR',4);
define('PROC_FIN_FAIL',8);
define('PROC_FIN',9);
$_rptrack_glb=null;
$_rpt_glb_list=array();
$_rpt_glb_start=0;
declare(ticks = 1); 
require_once(dirname(__FILE__) . '/../../../../application/libraries/db.php');
require_once dirname(__FILE__) . '/../../../../libraries/ci_db_connect.php';
class user_inbox
{
    public $db;
    public $user;
    public $prefix_table = 'log_';
    public $allow_json_unescape = false;
    public function __construct($user)
    {
        $path   = BASE_DIRX;
        $this->user = $user; 
        $this->conn_id = new Db($user,'log',false); 
        $this->createProcessLogTable();
        if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
            $this->allow_json_unescape = true;
        }else{
            $this->allow_json_unescape = false;
        }
    }
    public function read_message($id,$status=1){
        $sql = "UPDATE {$this->conn_id->prefix_table}user_inbox SET  view_status  = '{$status}' ,view_time = datetime('now')  WHERE id = '".$id."' ;" ; 

        if(!$this->_exec($sql))
            return false; 
        return true;
    }
    public function record_message($title='',$contents='',$type='success',$expire_day = 30,$comments='' )
    {//$type = 'success' , 'error' , 'notify' ,'other'
            
        $sql = ' REPLACE INTO '.$this->prefix_table.'user_inbox (pid,title,contents,comments,type,expire_time, create_time  ) '
                . 'VALUES (' 
                . "'" . getmypid() . "', "
                . "'" . $this->prepare_msg($title) . "', "
                . "'" . $this->prepare_msg($contents) . "', "
                . "'" . $this->prepare_msg($comments) . "', "
                . "'" . $this->prepare_msg($type) . "', "
                . " datetime('now',' {$expire_day} days'),datetime('now'));" ; 
            
        if(!$this->_exec($sql))
            return false; 
        return true;
    }
    private function _exec($query) 
    { 
        $error = '';
        if(!$query || empty($query) || !isset($query) || $this->conn_id == null)
            return FALSE;

//        $exec = @sqlite_exec($this->conn_id, $query, $error); 
            
        $exec = $this->conn_id->db_exec($query,false,false);
        
        if(!$exec) return $exec;
//            return $error;
            
        return $exec;
    } 
    private function createProcessLogTable($new = false)
    { 
        if($this->conn_id == null) return false;
        if($new){
            $sql = "DROP TABLE IF EXISTS {$this->prefix_table}user_inbox";
            $this->_exec($sql);
        }
//        $sql ="SELECT name FROM sqlite_master WHERE type='table' AND name='user_inbox';";
////        $rs = sqlite_query($this->conn_id,$sql);
//            
//        $rs = $this->conn_id->db_query_string_fetch($sql);
            
        if($this->conn_id->db_table_exists('user_inbox')) return true;
        $sql = 'CREATE TABLE   '.$this->prefix_table.'user_inbox (  
                id INTEGER NOT NULL AUTO_INCREMENT, 
                pid INTEGER NOT NULL,  
                title TEXT,
                contents TEXT,
                comments TEXT,
                type TEXT,
                view_status INTEGER   DEFAULT 0,
                view_time  DATETIME ,  
                expire_time  DATETIME , 
                create_time  TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
                PRIMARY KEY (id)
                )  ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci 
                ;' ;
        
        if(!$this->_exec($sql,false))
            return false;
        
        return true;
    }
    
    private function prepare_msg($comments){
        if(is_array($comments) || is_object($comments)){
            if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')){
                $comments = json_encode($comments,JSON_UNESCAPED_UNICODE);
            }else{
                $comments = json_encode($comments);
            }
        }else if(is_null($comments)){
            $comments = 'NULL';
        }else{
            $comments = $this->conn_id->escape_str($comments);
        }
        return $comments;
    }
    
}
class dev_log
{
    public $db;
    public $user;
    public $prefix_table = 'log_';
    public $allow_json_unescape = false;
    public function __construct($user)
    {
        $path   = BASE_DIRX;
        $this->user = $user; 
        $this->conn_id = new Db($user,'log',false); 
        $this->createProcessLogTable();
        if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
            $this->allow_json_unescape = true;
        }else{
            $this->allow_json_unescape = false;
        }
    }
    private function _exec($query) 
    { 
        $error = '';
        if(!$query || empty($query) || !isset($query) || $this->conn_id == null)
            return FALSE;

//        $exec = @sqlite_exec($this->conn_id, $query, $error); 
            
        $exec = $this->conn_id->db_exec($query,false,false);
        
        if(!$exec) return $exec;
//            return $error;
            
        return $exec;
    } 
    public function createProcessLogTable($new = false)
    { 
        if($this->conn_id == null) return false;
        if($new){
            $sql = "DROP TABLE IF EXISTS {$this->prefix_table}dev_log";
            $this->_exec($sql);
        }
//        $sql ="SELECT name FROM sqlite_master WHERE type='table' AND name='dev_log';";
////        $rs = sqlite_query($this->conn_id,$sql);
//            
//        $rs = $this->conn_id->db_query_string_fetch($sql);
//            
//        if(sizeof($rs)>0) return true;
                if($this->conn_id->db_table_exists('dev_log')) return true;

        $sql = 'CREATE TABLE   '.$this->prefix_table.'dev_log (  
                log_id INTEGER NOT NULL AUTO_INCREMENT, 
                pid INTEGER NOT NULL,  
                comments TEXT,
                create_time  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,  
                PRIMARY KEY (log_id)
                )  ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci
                ;' ;
        
        if(!$this->_exec($sql,false))
            return false;
        
        return true;
    }
    
    public function record_dev_log($comments='' )
    {
        if(is_array($comments) || is_object($comments)){
            if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')){
                $comments = json_encode($comments,JSON_UNESCAPED_UNICODE);
            }else{
                $comments = json_encode($comments);
            }
        }else if(is_null($comments)){
            $comments = 'NULL';
        }else{
            $comments = str_replace("'","$",$comments);
        }
        $sql = ' REPLACE INTO '.$this->prefix_table.'dev_log (pid,comments, create_time  ) '
                . 'VALUES ('
                . '\'' . getmypid() . '\', ' 
                . '\'' . $comments . '\', ' 
                . 'now());' ;
            
        if(!$this->_exec($sql))
            return false; 
        return true;
    }
}
class import_process_log 
{
    public $db;
    public $user;
    public $prefix_table = 'log_';
    public $conn_id=null;
    /* Build object */
    public function __construct($user)
    {
        $path   = BASE_DIRX;
        $this->user = $user;
        $location = $path . '/users/' . $user . '/log.db';
        $this->conn_id = null;
//        if(file_exists($location)){
//            if ( is_writable( $path . '/users') ){
                 $this->conn_id = new Db($user,'log',true);
    //           $this->conn_id = sqlite_open($location);
//                $this->db = $this->conn_id;
//            }
//        }
    }
    
    public function _exec($query,$transaction=true) 
    { 
        $error = '';
        if(!$query || empty($query) || !isset($query) || $this->conn_id == null)
            return FALSE;

//        $exec = @sqlite_exec($this->conn_id, $query, $error);
        
        $exec = $this->conn_id->db_exec($query,$transaction,false);
        
        if(!$exec) return $exec;
//            return $error;
            
        return $exec;
    } 
    
    public function createProcessLogTable($new = false)
    { 
        if($this->conn_id == null) return false;
        if($new){
            $sql = "DROP TABLE IF EXISTS {$this->prefix_table}process_log";
            $this->_exec($sql);
             $sql = "DROP TABLE IF EXISTS {$this->prefix_table}process_fmsg";
            $this->_exec($sql);
            $sql = "DROP TABLE IF EXISTS log_issue_performance";
            $this->_exec($sql);
        }
        
//        $sql ="SELECT name FROM sqlite_master WHERE type='table' AND name='process_log';";
////        $rs = sqlite_query($this->conn_id,$sql);
//            
//        $rs = $this->conn_id->db_query_string_fetch($sql);
//            
//        if(sizeof($rs)>0) return true;
        if(!$this->conn_id->db_table_exists('log_issue_performance',true)){
        $sql = "CREATE TABLE log_issue_performance (
            id_key varchar(64) NOT NULL , 
            last_update datetime,
            cmd text,
            run_time text,
            mem_usage text,
            data text,
            user text NOT NULL, 
            PRIMARY KEY (id_key)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            $this->_exec($sql,false);
        }
        if(!$this->conn_id->db_table_exists('process_fmsg')){
            $sql = 'CREATE TABLE   '.$this->prefix_table.'process_fmsg (   
                fmsg_key VARCHAR(32) NOT NULL PRIMARY KEY   ,  
                fmsg_msg TEXT 
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci
                ;' ;
        
            $this->_exec($sql,false);
        }
        
        if($this->conn_id->db_table_exists('process_log')) return true;
        $sql = 'CREATE TABLE   '.$this->prefix_table.'process_log (   
                batch_id VARCHAR(32) NOT NULL PRIMARY KEY   ,  
                pid INTEGER NOT NULL,
                host TEXT,
                shop_id INTEGER NOT NULL,
                shop_name TEXT,
                type TEXT,
                task_status  tinyint(4),
                progress  tinyint(4),
                create_time  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                end_time  DATETIME ,
                last_time TIMESTAMP,
                display tinyint(1) DEFAULT 1 
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci
                ;' ;
        
        if(!$this->_exec($sql,false))
            return false;
        
        return true;
    }
    public function record_fmsg($data)
    {
        if($this->conn_id == null) return false;
        $data['msg'] = $this->conn_id->escape_str($data['msg']);
        $sql = " REPLACE INTO ".$this->prefix_table."process_fmsg (fmsg_key,fmsg_msg) values ('{$data['key']}','{$data['msg']}') ;\n";
            
        if(!$this->_exec($sql))
            return false;  
        return true;
    }
    public function record_process($data,$end=false)
    {
        $add = $add2 = '';
        if($end){
            $add = ', end_time';
            $add2 = '  now() , ';
            $sql = " update ".$this->prefix_table."process_log set  task_status = '{$data['task_status']}' ,progress = '{$data['progress']}',end_time = now() "
            . " where batch_id = '{$data['batch_id']}' and pid = '{$data['pid']}'";
        }else{
        $sql = ' REPLACE INTO '.$this->prefix_table.'process_log (batch_id,pid, shop_id,shop_name,type,task_status ,progress ,host  , last_time,display) '
                . 'VALUES (\'' . $data['batch_id'] . '\','
                . '\'' . $data['pid'] . '\', ' 
                . '\'' . $data['shop_id'] . '\', ' 
                . '\'' . $data['shop_name'] . '\', '     
                . '\'' . $data['type'] . '\', '     
                . '\'' . $data['task_status'] . '\', '
                . '\'' . $data['progress'] . '\', ' 
                . '\'' . $data['host'] . '\', '
                . '  now() , 1 );' ;
        }
        $e = new Exception;
        $trace = $e->getTraceAsString();
//        file_put_contents('/var/www/backend/script/log/rec.txt',print_r(array(date('c'),__LINE__,$trace,$sql),true),FILE_APPEND);
        if(!$this->_exec($sql))
            return false; 
        return true;
    }
    public function update_process_not_alive($pid ,$end=false){
        
        if($end){
            $running = 0;    
        }else{
            $running = 1; return;   
        }
        return;
        $sql = '';

        if(!empty($pid)&& is_array($pid)){
            $where = array();
            foreach($pid as $host=>$h){
                $where[] = "( host ='$host' and pid in ('".implode("','",$h)."') )";
            }
            $sql = "UPDATE {$this->prefix_table}process_log SET  display  = '".$running."'  WHERE create_time >= DATE_SUB(now(), INTERVAL 1 DAY) and  (".implode(" or ",$where).");" ;
        }elseif(!empty($pid) && is_numeric($sql)){
            $sql = "UPDATE {$this->prefix_table}process_log SET  display  = '".$running."'  WHERE create_time >= DATE_SUB(now(), INTERVAL 1 DAY) and pid = '".$pid."'   ;" ; 
        }
        

        if(!$this->_exec($sql,FALSE))
            return false; 
        return true;
    }
    public function remove_process_fmsg($key)
    {
        if(is_bool($key))return false;
        if(is_array($key)){
            $sql = "delete from {$this->prefix_table}process_fmsg where fmsg_key in ('".implode("','",$key)."') ";
        }else{
            $sql = "delete from {$this->prefix_table}process_fmsg where fmsg_key = '$key' ";
        }
        if(!$this->_exec($sql))
            return false; 
        return true;
    }
    
    public function remove_process($pid=0)
    {
        $sql = "delete from {$this->prefix_table}process_log where pid = '$pid' ";
        if(!$this->_exec($sql))
            return false; 
        return true;
    }
    
    public function get_processes()
    {
        if($this->conn_id == null) return false;
        
//        $sql ="SELECT name FROM sqlite_master WHERE type='table' AND name='process_log';";  
//        $rs = $this->conn_id->db_query_string_fetch($sql); 
//        if(sizeof($rs)==0) return array();
        if(!$this->conn_id->db_table_exists('process_log')) return array();
        $add = '';
        if(!empty($this->running_pid)){
            $add = " and pid in ('".implode("','",$this->running_pid)."') ";
        }
        $sql = "select pid,host from {$this->prefix_table}process_log where display = 1 $add and last_time >= DATE_SUB(now(), INTERVAL 1 DAY) order by last_time desc limit 20";
        //$sql = "SELECT * FROM main.sqlite_master WHERE type='table';";
         
//        $result =  @sqlite_query($this->conn_id,$sql ); 
        $result = $this->conn_id->db_query_string_fetch($sql);
        return ($result);
    }
    public function get_processes_fmsg($key)
    {
        if($this->conn_id == null) return false;
        
//        $sql ="SELECT name FROM sqlite_master WHERE type='table' AND name='process_log';";  
//        $rs = $this->conn_id->db_query_string_fetch($sql); 
//        if(sizeof($rs)==0) return array();
        if(!$this->conn_id->db_table_exists('process_fmsg')) return array();
        $result=array();
        if(!empty($key) && is_array($key)){
            $sql = "select fmsg_msg from {$this->prefix_table}process_fmsg where fmsg_key in ('".implode("','",$key)."')  ;\n";
//            if(defined('TEST_PROC_ONLY')) print_r($sql); 
            $result = $this->conn_id->db_query_string_fetch($sql);
            $out=array();
            foreach($result as $r){
                if(!empty($r['fmsg_msg']))
                $out[] = $r['fmsg_msg'];
            }
            return  $out;
        }else if(!empty($key) && is_string($key)){
            $sql = "select fmsg_msg from {$this->prefix_table}process_fmsg where fmsg_key = '{$key}'  ;\n";
            $result = $this->conn_id->db_query_string_fetch($sql);
            return isset($result[0]['fmsg_msg'])?$result[0]['fmsg_msg']:$result;
        }
        //$sql = "SELECT * FROM main.sqlite_master WHERE type='table';";
            
//        $result =  @sqlite_query($this->conn_id,$sql ); 
        
            
        
    }
    
    
    public function check_process_running($type='',$pid)
    {
        if($this->conn_id == null) return false;
//        $sql ="SELECT name FROM sqlite_master WHERE type='table' AND name='process_log';";  
//        $rs = $this->conn_id->db_query_string_fetch($sql); 
//        if(sizeof($rs)==0) return array();
        if(!$this->conn_id->db_table_exists('process_log')) return array();
        
        if($type==''){
         $sql = "select * from {$this->prefix_table}process_log where display = 1  and pid <> '$pid'  and  create_time >= DATE_ADD(NOW( ), INTERVAL -1 DAY ) order by last_time asc limit 2";   
        }else{
        $sql = "select * from {$this->prefix_table}process_log where display = 1 and type like '%$type%' and pid <> '$pid' and  create_time >= DATE_ADD(NOW( ), INTERVAL -1 DAY ) order by last_time asc limit 2";
        }
        //$sql = "SELECT * FROM main.sqlite_master WHERE type='table';";
            
//        $result =  @sqlite_query($this->conn_id,$sql ); 
        $result = $this->conn_id->db_query_string_fetch($sql);
        return ($result);
    }
   
   
    
}

class report_process
{
    //report parameter with public 
    public $at_time = 0;
    public $marketplace = '';
    public $msg_title = '';
    //public $msg_type = null;
    public $msg_percent = 0;
    public $batch_id = 0;
    public $pid = 0;
    public $shop_id = 0;
    public $shop_name = '';
    public $type = '';
    public $msg = '';
    //public $msg_status = null;
    //public $msg_url = '';
    public $delay = DEF_ASYN_DELAY; 
    public $timeout = DEF_ASYN_TIMEOUT; 
    //public $running_status = false;
    public $task_status = PROC_NSTR; // 0= not start , 1 = started , 9 = finished
    public $error_status = false;
    public $error_msg = '';
    public $popup_display = true;
    public $priority = 0;
    public $comments = '';
    public $host = '';
    
    //internal parameter with private
    private $user = null;
    private $cur_task = 0;
    private $min_task = 0;
    private $max_task = 100;
    private $file_fifo_name = '';
    private $file_msg_name = '';
    private $file_fmsg_name = '';
    private $path_fifo = DEF_STORE_FILE;
    
    private $file_patern = DEF_FIFO_PATERN;
    private $fifo_res = false;
    private $fmsg_res = false;
    private $fmsg_pointer = false;
    private $fifo_pointer = null;
    private $log = null;
    private $debug = false;
    //private   $member;
    private $avail_run = true; 
    private $fmsg_key = null;
    private $allow_json_unescape = false;
    private $track_record_flag = true;
    private $db_main = null;
    private function check_no_rec(){
        if(defined('_NO_REC_PROC_'))return true;
        return false;
    }
    public function __construct($user, $batch_id, $title='', $start=false, $debug=false, $popup=true, $type = '', $comments = ''){
        $this->db_main = new ci_db_connect();
        include(dirname(__FILE__) . '/../../../../application/config/config.php');
        $this->host = isset($config['host_code'])?$config['host_code']:'';

        //$this->member[] = $this;
        if($this->check_no_rec())return;
        global $_rptrack_glb,$_rpt_glb_start,$_rpt_glb_list;
        $_rptrack_glb = $this;
        if($_rpt_glb_start==0){
            $_rpt_glb_start = $this->microtime_float();
        }
        $this->set_user($user);
        $this->track_record();
        if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
            $this->allow_json_unescape = true;
        }else{
            $this->allow_json_unescape = false;
        }
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $this->avail_run = false;
        }
        if(!function_exists('pcntl_signal')){
            $this->avail_run = false; 
        }
        $this->avail_run = false;
         //echo $user.'xxx'.$batch_id;
        //$this->set_debug($debug);
        $this->setup_pid();  
        
        $this->set_batch_id($batch_id);
        if($title!='')$this->set_title($title);
            
        if($this->avail_run){ 
            $this->setup_collector_fifo();
            $this->setup_signal_handle();
        }else{
            $this->file_patern = DEF_FMSG_PATERN;
            $this->setup_collector_file();
        }
            
        $this->log = new import_process_log($user);    
        $this->log->createProcessLogTable(false); 
        $this->log->record_process($this->db_data());
        $this->popup_display = $popup;
//        $type = trim($type);
//        if($type==''&&$title!=''){
//            $type = strtolower($title);
//            $type = trim($type,'.');
//            $type = trim($type);
//            $type = str_replace(array(',','"',"'",'?','/','#','@','!','$','%','^','&',' '),'_',$type);
//        }elseif($type==''&&$title==''){
//            $type = md5(uniqid());
//        }
        $this->type = $type;
        $this->comments = $comments;
        
        if($start)$this->start_task(); 
        
    }
    private function microtime_float()
    {
        if($this->check_no_rec())return;
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
    public function trace_performance($additional=''){
        if($this->check_no_rec())return;
        $this->track_record($additional,true);
    }
    public function track_record( $additional='',$split_file=false){
        if($this->check_no_rec())return;
        if (php_sapi_name() != "cli") return;
        global $argv,$_rpt_glb_start,$_rpt_glb_list,$_rptrack_glb;
        if(function_exists('gc_enable'))gc_enable();
        
        if($_rptrack_glb!==$this){
            $_rptrack_glb=$this;
            register_shutdown_function(function(){
                global $_rptrack_glb;
                if(!empty($_rptrack_glb))$_rptrack_glb->track_record ('Shutdown');
            });
        }
        $call='';
        $file_name = '';
        if(empty($this->user))return;
        if(!empty($argv)){
            if(!empty($argv[0])){
                $file_name = str_replace('/var/www/backend/assets/apps/','',$argv[0]);
            }
            if(is_array($argv)){
                $call = implode(' ',$argv);
            }
        }
        if($this->track_record_flag==false){
            if(file_exists(BASE_DIRX.'/users/'.$this->user.'/xml_log/process_trace.txt')){
                unlink(BASE_DIRX.'/users/'.$this->user.'/xml_log/process_trace.txt');
            }
            if(file_exists(BASE_DIRX.'/users/'.$this->user.'/xml_log/process_trace_'.$file_name.'_.txt')){
                unlink(BASE_DIRX.'/users/'.$this->user.'/xml_log/process_trace_'.$file_name.'_.txt');
            }
            return;
        }
        $e = new Exception; 
        $trace = $e->getTraceAsString();
        if(!empty($additional)&&is_object($additional)){
            $additional = json_encode($additional);
        }
        $run_time = $this->microtime_float()-$_rpt_glb_start;
        $mem_usage_all = memory_get_usage(true);
        $mem_usage = memory_get_usage();
        $mem_peak = memory_get_peak_usage();
        $mem_peak_all = memory_get_peak_usage(true);
        $num = 0;
        if(function_exists('gc_collect_cycles')){$num = gc_collect_cycles();}
        $mem_all=array(
            'mem usage'=>($mem_usage/1024) .' k',
            'mem usage all'=>($mem_usage_all/1024) .' k',
            'mem peak'=>($mem_peak/1024) .' k',
            'mem peak all'=>($mem_peak_all/1024) .' k',
            'mem clear no'=> $num
        );
        if(isset($this->last_mem_use) && $this->last_mem_use == $mem_usage && sizeof($_rpt_glb_list)>100){
            return;
//            if(sizeof($_rpt_glb_list)-1<0){
//                $index=0;
//            }else{
//                $index = sizeof($_rpt_glb_list)-1;
//            }
//            $_rpt_glb_list[$index]=array('time'=>date('c'),'run(sec)'=>$run_time,'trace'=>"$trace",'data'=>$additional,'mem usage'=>($mem_usage/1024) .' k' );
        }else{
            $_rpt_glb_list[]=array('time'=>date('c'),'run(sec)'=>$run_time,'trace'=>"$trace",'data'=>$additional,$mem_all );
        }
        $this->last_mem_use =  ($mem_usage);
        
        
        
        $avail_list=array('amazon_update_offers_cron.php','insert_prestashop_offers.php','ebay_export_revise_product_cron.php','ebay_products_compress.php');
        $not_avail_list=array('import_offers_options.php','amazon_repricing_analysis_cron.php','amazon_repricing_export_cron.php');
        $add='';
        if (php_sapi_name() != "cli") {
             $add='front_';
        }
        
        $mem_limit_1 = 300;
        $mem_limit_2 = 600;
        if(strpos($file_name,'amazon')!==false){
            $mem_limit_1 = 200;
            $mem_limit_2 = 200;
        }
        if($run_time> 200 || $mem_usage/(1024*1024) > $mem_limit_1){
            if($mem_usage/(1024*1024)>$mem_limit_2 || $run_time> 3600){
                $id_key = md5($call.date('Y-m-d'));
            }else{
                $id_key = md5($call);
            }
            
            $dx =  print_r(array($call,date('c'),$_rpt_glb_list),true) ;
            
            $pp = BASE_DIRX.'/users/'.$this->user.'/xml_log/'.$add.'process_trace_'.$id_key. '_.txt';
            file_put_contents($pp, $dx); 
            $pp = '/assets/apps/users/'.$this->user.'/xml_log/'.$add.'process_trace_'.$id_key. '_.txt';
            $sql = "delete from log_issue_performance where id_key = '{$id_key}'";
            if((!empty($this->log) || !is_object($this->log) )&& !empty($this->user)){
                $this->log = new import_process_log($this->user);   
            }
            $this->log->_exec($sql,false);
            $sql = "replace into log_issue_performance (id_key,user,last_update,cmd,run_time,mem_usage,data) values ('{$id_key}','{$this->user}',now(),'{$call}','{$run_time}','{$mem_usage}' ,'".$pp."')";
            $this->log->_exec($sql,false);
            if(in_array($file_name,$avail_list)){
                file_put_contents(BASE_DIRX.'/users/'.$this->user.'/xml_log/'.$add.'process_trace_'.$file_name.'.txt', print_r(array($call,date('c'),$_rpt_glb_list),true));
            }
        }else{
            if(in_array($file_name,$not_avail_list) || !file_exists(BASE_DIRX.'/users/'.$this->user.'/xml_log/')){
                return;
            }elseif(in_array($file_name,$avail_list)){
                file_put_contents(BASE_DIRX.'/users/'.$this->user.'/xml_log/'.$add.'process_trace_'.$file_name.'.txt', print_r(array($call,date('c'),$_rpt_glb_list),true));

            }elseif( $split_file==false || empty($file_name)){ 
//                file_put_contents(BASE_DIRX.'/users/'.$this->user.'/xml_log/'.$add.'process_trace.txt', print_r(array($call,date('c'),$_rpt_glb_list),true));
            }else{
                file_put_contents(BASE_DIRX.'/users/'.$this->user.'/xml_log/'.$add.'process_trace_'.$file_name.'_s.txt', print_r(array($call,date('c'),$_rpt_glb_list),true));
            }
        }
        
    }
    public function has_other_running($type=''){
        if($this->check_no_rec())return;
        //f(!$this->avail_run)return;
//        if($this->type=='') return false;
        $this->track_record($type);
        $res = $this->log->check_process_running($type,$this->pid);
        $out =false;
        $process_list = $this->db_main->getBackendProcessID();
        $e = new Exception;
        $trace = $e->getTraceAsString();
        foreach($res as $p){
            if(!empty($p['host'])){
                if(isset($process_list[$p['pid']][$p['host']])){
                    file_put_content('/var/www/backend/script/log/dup_proc.txt',print_r(array(date('c'),$trace,$type,$res,$p['pid'],$p['host'],__LINE__),true),FILE_APPEND);
                    return true;
                }
            }else{
                if(isset($process_list[$p['pid']])){
                    file_put_content('/var/www/backend/script/log/dup_proc.txt',print_r(array(date('c'),$trace,$type,$res,$p['pid'],'no_host',__LINE__),true),FILE_APPEND);
                    return true;
                }
            }
//            $pid =$p['pid'];
//            if(!$this->processExists($pid)){
//                $this->log->update_process_not_alive($pid,true);
//                continue;
//            }
//
//            if(!empty($pid)) $out = true;
        }  
        //if($this->debug)print_r($this->pid_list);
        
        return $out;
    }
    public function end_current_process(){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->log->update_process_not_alive(array($this->host=>array($this->pid=>$this->pid)),true);
    }
    private function processExists($pid=null) {
        if($this->check_no_rec())return;
        $this->track_record();
        $unix = true;
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $unix = false;
        }
        if(!function_exists('pcntl_signal')){
            $unix = false;
        } 
        if($unix){
        if(is_null($pid)) $pid= $this->pid;
        if($this->debug)echo 'check process :'.$pid."\n";
        return file_exists("/proc/{$pid}");
        }else{  
            if(file_exists($this->path_fifo.$this->file_msg_name)){
            
                return true;
            }
        }
        return false;
    }
    public function set_marketplace($mkp){
        if($this->check_no_rec())return;
        $this->track_record($mkp);
        $this->marketplace = $mkp;
    }
    public function set_process_comments($comments){
        if($this->check_no_rec())return;
        $this->track_record($comments);
        $this->comments = $comments;
    }
    public function set_priority($prio){
        if($this->check_no_rec())return;
        $this->track_record($prio);
        $this->priority = $prio;
    }
    public function set_shop_id($id){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->shop_id = $id;
        if($this->debug)echo "Shop ID : ".$id."\n";
    }
    public function set_shop_name($name=''){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->shop_name = $name;
        if($this->debug)echo "Shop Name : ".$name."\n";
    }
    public function set_shop($id=0,$name=''){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->set_shop_id($id);
        $this->set_shop_name($name);
    }
    public function set_popup_display($popup=true){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->popup_display = $popup;
    }
    public function set_process_type($type=''){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->type=$type;
        if(!empty($type)){
            $this->log->record_process($this->db_data());
        }
    }
    public function set_debug($debug=true){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->debug = $debug;
    }
    public function start_task($record = true){
        if($this->check_no_rec())return;
        $this->task_status = PROC_STR;
        if($record)
        $this->log->record_process($this->db_data());
        $this->at_time = date('c');
        if(!empty($this)){
            if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')  ){
                $this->write_fmsg(json_encode($this,JSON_UNESCAPED_UNICODE)); 
            }else{
                $this->write_fmsg(json_encode($this)); 
            }
        }
        //$this->log->record_process($this->db_data());
    }
    public function finish_task($status = true){ 
        if($this->check_no_rec())return;
        //if(!$this->avail_run)return;
        if(!($this->task_status==PROC_FIN || $this->task_status==PROC_FIN_FAIL)){
            if($status){
            $this->task_status = PROC_FIN;
            }else{
                $this->task_status = PROC_FIN_FAIL;
            }
            $this->log->record_process($this->db_data(),true);
            $this->at_time = date('c');
            if(!empty($this)){
                if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')){
                    $this->write_fmsg(json_encode($this,JSON_UNESCAPED_UNICODE)); 
                }else{
                    $this->write_fmsg(json_encode($this)); 
                }
            
            }
            if($this->msg!='' || $this->error_msg !='' ){
//                sleep($this->delay*5);
//                sleep($this->delay*5);
            }else{
//                sleep($this->delay*5);
//                sleep($this->delay*5);
            } 
//            $this->remove_fmsg();
        }
    }
    public function set_delay($delay=DEF_ASYN_DELAY){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->delay =$delay;
    } 
    public function set_timeout($time=DEF_ASYN_TIMEOUT){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->delay =$time;
    }
    public function set_max_min_task($max=0 , $min=0){
        if($this->check_no_rec())return;
        $this->track_record();
        if($max<$min){
            $t = $max;
            $max = $min;
            $min = $t;
        }
        $this->min_task = $min;
        $this->max_task = $max;
    }
    public function set_error_msg($msg='',$finish = false){ 
        if($this->check_no_rec())return;
        $this->error_status=true;
        $this->error_msg=$msg;
        $this->task_status = PROC_ERR;
        $this->log->record_process($this->db_data(),true);
        $this->at_time = date('c');
        if(!empty($this)){
            if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')){
                $this->write_fmsg(json_encode($this,JSON_UNESCAPED_UNICODE)); 
            }else{
                $this->write_fmsg(json_encode($this)); 
            }
        
        }
        sleep($this->delay*5);
//        sleep($this->delay*5);
        if($finish) $this->finish_task();
    }
    public function clear_msg(){ 
        if($this->check_no_rec())return;
        $this->msg = '';
        if(!empty($this)){
            if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')){
                $this->write_fmsg(json_encode($this,JSON_UNESCAPED_UNICODE)); 
            }else{
                $this->write_fmsg(json_encode($this)); 
            }
        
        }
    }
    public function set_status_msg($msg=''){
        if($this->check_no_rec())return;
        $this->track_record($msg);
        $this->msg=$msg;
        //echo '<pre>' . print_r($this, true) . '</pre>'; 
        $this->log->record_process($this->db_data());
//        sleep($this->delay*5);
//        sleep($this->delay*5);
        $this->at_time = date('c');
        if(!empty($this)){
            if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')){
                $this->write_fmsg(json_encode($this,JSON_UNESCAPED_UNICODE)); 
            }else{
                $this->write_fmsg(json_encode($this)); 
            }
        
        }
    }
    public function set_running_task($run_id = 0){ 
        if($this->check_no_rec())return;
        if($run_id < $this->min_task){$run_id = $this->min_task;}
        if($run_id > $this->max_task){$run_id = $this->max_task;}
        $this->cur_task = $run_id;
        $this->set_cur_percent();
        //if(!$this->avail_run)return;
        $this->at_time = date('c');
        if($this->msg_percent==100){
            $this->finish_task();   
        }else if($this->msg_percent==0){
            $this->log->record_process($this->db_data());
            if(!empty($this)){
                if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')){
                    $this->write_fmsg(json_encode($this,JSON_UNESCAPED_UNICODE)); 
                }else{
                    $this->write_fmsg(json_encode($this)); 
                }

            }
        }else{
            if($this->task_status != PROC_STR)
            $this->start_task(); 
            if(!empty($this)){
                if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')){
                    $this->write_fmsg(json_encode($this,JSON_UNESCAPED_UNICODE)); 
                }else{
                    $this->write_fmsg(json_encode($this)); 
                }

            }
        }
        //$this->log->record_process($this->db_data());
    }
    
    private function get_pid(){
        if($this->check_no_rec())return;
        $this->track_record();
        return getmypid();
    }
    private function setup_pid(){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->pid = $this->get_pid();
        if($this->debug)echo "PID : ".$this->pid."\n";
    }
    public function set_title($title = ''){
        if($this->check_no_rec())return;
        $this->track_record();
        $this->msg_title=$title;
    }
    private function set_user($user=''){  
        if($this->check_no_rec())return;
        $this->user = $user;
        if($this->debug)echo "User : ".$user."\n";
    }
    public function set_batch_id($batch_id='0'){ 
        if($this->check_no_rec())return;
        $this->batch_id = $batch_id;
        if($this->debug)echo "Batch ID : ".$batch_id."\n";
    }
    
    private function set_cur_percent(){ 
        if($this->check_no_rec())return;
        if($this->max_task==0 || ($this->max_task - $this->min_task) <=0){
            $this->msg_percent = 0;
        }
        if($this->cur_task > $this->max_task){ 
            $this->set_running_task($this->max_task);
        }elseif($this->max_task - $this->min_task == 0){
            $this->msg_percent = 100;
        }else{
            $this->msg_percent = floor((($this->cur_task - $this->min_task)/($this->max_task - $this->min_task))*100);
        }
    }
    
    private function setup_collector_fifo(){
        if($this->check_no_rec())return;
        $this->track_record();
        $u_id = $this->user; 
        if(is_null($u_id)){
            return false;
        } 
        $this->file_fifo_name = str_replace('{user}',$u_id,$this->file_patern); 
        $this->file_fifo_name = str_replace('{pid}',$this->pid,$this->file_fifo_name);
        if(!empty($this->host)){
            $this->file_fifo_name = str_replace('{host}','_'.$this->host,$this->file_fifo_name);
        }else{
            $this->file_fifo_name = str_replace('{host}','',$this->file_fifo_name);
        }
        $this->fifo_res = $this->alloc_fifo();
        return $this->fifo_res;
    }
    private function setup_collector_file(){
        if($this->check_no_rec())return;
        $this->track_record();
        $u_id = $this->user; 
        if(is_null($u_id)){
            return false;
        } 
        $this->file_msg_name = str_replace('{user}',$u_id,$this->file_patern); 
        $this->file_msg_name = str_replace('{pid}',$this->pid,$this->file_msg_name);
        if(!empty($this->host)){
            $this->file_msg_name = str_replace('{host}','_'.$this->host,$this->file_msg_name);
        }else{
            $this->file_msg_name = str_replace('{host}','',$this->file_msg_name);
        }
//        file_put_contents('/var/www/backend/script/log/rec.txt',print_r(array(date('c'),__LINE__,$trace,$this->file_msg_name,md5($this->file_msg_name)),true),FILE_APPEND);
        $this->fmsg_key = md5($this->file_msg_name);
        $this->fmsg_res = $this->alloc_fmsg();
        
        return $this->fmsg_res;
    }
    private function alloc_fmsg(){ 
        if($this->check_no_rec())return;
        $this->track_record();
        if($this->avail_run)return false;
        if(!file_exists($this->path_fifo)){ 
            $oldmask = umask(0); 
            mkdir($this->path_fifo, 0777);umask($oldmask);
            
        }
//        $msg = $this->log->get_processes_fmsg($this->fmsg_key);
//        if(!empty($msg)){
//            $this->log->record_fmsg(array('key'=>$this->fmsg_key,'msg'=>''));
//        }
        return true;
//        if(file_exists($this->path_fifo.$this->file_msg_name)){
//            unlink($this->path_fifo.$this->file_msg_name);
//        }
//            $success = fopen($this->path_fifo.$this->file_msg_name, 'w+'); //or die("can't open file");//posix_mkfifo($this->path_fifo.$this->file_fifo_name, 0777);
//            if(!$success){
//                die('Error: Could not create a file msgs '. "\n");
//            }
//            return $success;
        
    }
    private function alloc_fifo(){
        if($this->check_no_rec())return;
        $this->track_record();
        if(!$this->avail_run)return false;
        if(!file_exists($this->path_fifo)){ 
            $oldmask = umask(0); 
            mkdir($this->path_fifo, 0777);umask($oldmask);
            
        }
        if(file_exists($this->path_fifo.$this->file_fifo_name)){
            unlink($this->path_fifo.$this->file_fifo_name);
        }
            $success = posix_mkfifo($this->path_fifo.$this->file_fifo_name, 0777);
            if(!$success){
                die('Error: Could not create a named pipe: '. posix_strerror(posix_errno()) . "\n");
            } 
            return $success;
        
    }
    
    private function write_fmsg($data = null){
        if($this->check_no_rec())return;
        $this->track_record($data);
        if($this->avail_run)return;
        if(empty($this->fmsg_res) || is_null($data)) return;
//        $this->fmsg_pointer = fopen($this->path_fifo.$this->file_msg_name, 'w');
//            if( !$this->fmsg_pointer){
//                die('Error: Could not open the file messages ' . "\n");
//                return false;
//            }
//        fwrite($this->fmsg_pointer,$data);
//        fclose($this->fmsg_pointer);
         $this->log->record_fmsg(array('key'=>$this->fmsg_key,'msg'=>$data));
    }
    private function remove_fmsg(){
        if($this->check_no_rec())return;
        $this->track_record();
        if($this->avail_run)return;
        unset($this->fmsg_res);
//        if(file_exists($this->path_fifo.$this->file_msg_name)){
//            unlink($this->path_fifo.$this->file_msg_name);
//        }
//        unset($this->fmsg_pointer);
        
        $this->log->remove_process_fmsg($this->fmsg_key);
    }
    
    private function write_fifo($data = null){
        if($this->check_no_rec())return;
        $this->track_record();
        if(!$this->avail_run)return;
        if(!$this->fifo_res || is_null($data)) return;
        $this->fifo_pointer = @fopen($this->path_fifo.$this->file_fifo_name, 'w');
            if( !$this->fifo_pointer){
                //die('Error: Could not open the named pipe: '. posix_strerror(posix_errno()) . "\n");
            
                return false;
            }
        fwrite($this->fifo_pointer,$data);
        fclose($this->fifo_pointer);
    }
    private function db_data(){ 
        if($this->check_no_rec())return;
        return array(
            'batch_id'=>$this->batch_id,
            'pid'=>$this->pid,
            'task_status'=>$this->task_status,
            'progress'=>$this->msg_percent, 
            'shop_id'=>$this->shop_id, 
            'shop_name'=>$this->shop_name, 
            'type'=>$this->type,
            'host'=>$this->host,
                );
    }
    public function signal_handle($sig){ 
        if($this->check_no_rec())return;
        //echo "sig ".$sig."\n";
        if(!$this->avail_run)return; 
            $pid = $this->get_pid();
        
        if(!$this->popup_display){
            $run_pid = pcntl_fork();
            if($run_pid == -1){ }
            elseif($run_pid){ return;}
        }
        
        switch($sig){
            case SIGUSR1: // signal check has start?
                if($this->debug)echo "($pid)Caught signal SIGINT \n";
                $this->write_fifo($this->task_status==PROC_STR?true:false);
                break;
            case SIGUSR2:// signal get info
                if($this->debug)echo "($pid)Caught signal SIGUSR1 \n";
                $this->at_time = date('c');
            
                    if($this->allow_json_unescape && defined('JSON_UNESCAPED_UNICODE')){
                        $this->write_fifo(json_encode($this,JSON_UNESCAPED_UNICODE));
                    }else{
                        $this->write_fifo(json_encode($this));
                    }
            
                
                break;
            case SIGBUS:// signal get progress
                if($this->debug)echo "($pid)Caught signal SIGBUS \n";
                $this->write_fifo($this->msg_percent);
                break;
            case SIGTERM:// signal check end task?
                if($this->debug)echo "($pid)Caught signal SIGTERM \n";
                $this->write_fifo($this->task_status==PROC_FIN?true:false);
                break;
            default: 
                if($this->debug)echo "($pid)Caught other signal \n";
                break;
        }
            
    }
    private function setup_signal_handle(){
        if($this->check_no_rec())return;
        $this->track_record();
        if(!$this->avail_run)return;
        $signals = array(SIGUSR1,SIGUSR2,SIGBUS,SIGTERM);
        foreach($signals as $sig){
            if($this->debug)echo "Setup signal $sig \n";
            pcntl_signal($sig,array(&$this,'signal_handle'));
        }
    }
    
    
}

class result_process{
    
    public $proc_info = array();
    
    private $log = null;
    private $user = '';
    private $tar_pid = 0;
    private $tar_host = '';
    private $file_fifo_name = '';
    private $file_fmsg_name = '';
    private $path_fifo = DEF_STORE_FILE;
    private $file_patern = DEF_FIFO_PATERN;
    private $fifo_res = false;
    private $fifo_pointer = null;
    private $fmsg_pointer = null;
    private $pid_list = array();
    private $process_list = array();
    private $debug = false;
    private $avail_run = true;
    private $fmsg_key = null;
    private $running_pid = array();
    private $host='';
    private $db_main=null;
    private $all_proccess=null;
    private $pid_list_info = array();
    public function __construct($user,$debug=false){
        $this->db_main = new ci_db_connect();
        $this->user = $user;
        include(dirname(__FILE__) . '/../../../../application/config/config.php');
        $this->host = isset($config['host_code'])?$config['host_code']:'';
        
//        $cmd = "ps -axo pid,cmd  | grep php | grep -v 'php-fpm'| grep -v 'php sig_update_status.php' | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' ";
//        ob_start();
//        passthru($cmd );
//        $outt = ob_get_contents();
//        ob_end_clean();
//        $list = explode("\n",$cmd);
//        $pid_list = array();
//        foreach($list as $l){
//           $out = explode(" ",$l);
//           $pid_list[$out[0]] = $out[0];
//        }
        $this->all_proccess = $this->db_main->getBackendProcessID(false);
        $this->running_pid = array_keys($this->all_proccess);
        
        $this->set_debug($debug);
        $this->set_user($user); 
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $this->avail_run = false;
        }
        
        if(!function_exists('pcntl_signal')){
            $this->avail_run = false;
        }
        $this->avail_run = false;
        if($this->avail_run){
            $this->log = new import_process_log($user);  
            $this->log->createProcessLogTable(false);
            $this->get_all_processes();
        }else{
            $this->file_patern = DEF_FMSG_PATERN;
            $this->log = new import_process_log($user);  
            $this->log->createProcessLogTable(false);
            $this->get_all_processes();
        }
        
    }
    public function get_running_process(){
        $pid_status = array('live'=>array(),'die'=>array());
//        if($this->avail_run){//not use
//            $out = array();
//            foreach($this->pid_list as $pid){
//                $this->set_target_pid($pid);
//                if($this->processExists($pid)){
//                    $pid_status['live'][$pid] = $pid;
////                    $this->log->update_process_not_alive($pid);
//                    $info = $this->sig_get_info();
//                    $out[] = $info;
//                }else{
//                    $pid_status['die'][$pid] = $pid;
////                    $this->log->update_process_not_alive($pid,true);
//                    ////$this->remove_history_pid($pid);
//                     if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//                         $this->remove_history_pid($pid);
//                     }
//                    $info = $this->sig_get_info();
//                    $out[] = $info;
//                }
//            }
//            $this->proc_info = $out;
//        }else{
            $out = array();
            $win=false;
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                $win=true;
            }
            foreach($this->pid_list_info as $host=>$h){
                foreach($h as $pid=>$v){
//                    $this->set_target_pid($pid);
//                    $this->set_target_host($host);
                    if(!empty($host)){
                        if(isset($this->all_proccess[$pid][$host])){
                            $pid_status['live'][$host][$pid] = $pid;
                        }else{
                            $pid_status['die'][$host][$pid] = $pid;
                            if ($win) {
                                $this->remove_history_pid($pid);
                            }
                        }
                    }else{
                        if(isset($this->all_proccess[$pid])){
                            $pid_status['live'][''][$pid] = $pid;
                        }else{
                            $pid_status['die'][''][$pid] = $pid;
                            if ($win) {
                                $this->remove_history_pid($pid);
                            }
                        }
                    }
            
                       
                }
            }
            $out = $this->get_fmsg_info($this->pid_list_info);
//             if(defined('TEST_PROC_ONLY')) print_r($out); 
            $this->proc_info = $out;
//        }
        $this->log->update_process_not_alive($pid_status['live']); 
        $this->log->update_process_not_alive($pid_status['die'],true);
        return $out;
    }
    public function set_debug($set = true){
        $this->debug=$set;
    }
    private function remove_history_pid($pid =0 ){
        return $this->log->remove_process($pid);
    }
    private function set_target_pid($pid){
        $this->tar_pid =$pid;
    }
    private function set_target_host($host){
        $this->tar_host =$host;
    }
    public function get_all_processes(){
        //f(!$this->avail_run)return;
        $res = $this->log->get_processes();
//        while($p = @sqlite_fetch_array($res)){  
        if(is_array($res))
        foreach($res as $p){
//            $pid =$p['pid'];
            if(empty($p['pid']) || empty($p['host'])){
                file_put_contents('/var/www/backend/script/log/rec.txt',print_r(array(date('c'),__LINE__,$p),true),FILE_APPEND);
                continue;
            }
                $this->pid_list[] = $p['pid'];
                $this->pid_list_info[$p['host']][$p['pid']] = true;
//                $this->process_list[] = $p; 
        } 
        //if($this->debug)print_r($this->pid_list);
        return $this->pid_list;
    }
//    private function processExists($pid=null) {
//        $unix = true;
//        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//            $unix = false;
//        }
//        if(!function_exists('pcntl_signal')){
//            $unix = false;
//        }
//
//
//        if($unix){
//        if(is_null($pid)) $pid= $this->tar_pid;
//        if($this->debug)echo 'check process :'.$pid."\n";
//        return file_exists("/proc/{$pid}");
//        }else{
//            $out = $this->setup_collector_fmsg($pid);
//            $msg = $this->log->get_processes_fmsg($out);
//            if(!empty($msg)){
//                return true;
//            }
////            if(file_exists($this->path_fifo.$this->file_fmsg_name)){
////
////                return true;
////            }
//        }
//        return false;
//    }
    private function read_fifo(){
        if(!$this->avail_run)return;
        $this->setup_collector_fifo();
        if(file_exists($this->path_fifo.$this->file_fifo_name)){
        $this->fifo_pointer = fopen($this->path_fifo.$this->file_fifo_name,'r');
        if( !$this->fifo_pointer){
                die('Error: Could not open the named pipe: '. posix_strerror(posix_errno()) . "\n");
                return false;
        }
        return fgets($this->fifo_pointer);
        }else{ return false;}
    }
    private function set_user($user=''){
        $this->user = $user;
    }
    private function sig_check_start(){
        if(!$this->avail_run)return;
        $pid = $this->tar_pid;
        posix_kill($pid,SIGUSR1);
        if($this->debug)echo "Send Signal Check start to $pid \n";
        $out = $this->read_fifo();
        return $out;
    }
    private function sig_check_finish(){
        if(!$this->avail_run)return;
        $pid = $this->tar_pid;
        posix_kill($pid,SIGTERM);
        if($this->debug)echo "Send Signal Check finished to $pid \n";
        $out = $this->read_fifo();
        return $out;
    }
    private function sig_get_info(){
        if(!$this->avail_run)return;
        $pid = $this->tar_pid;
        posix_kill($pid,SIGUSR2);
        if($this->debug)echo "Send Signal get info to $pid \n";
        $out = $this->read_fifo();
        return $out;
    }
    
    private function sig_get_prgress(){
        if(!$this->avail_run)return;
        $pid = $this->tar_pid;
        posix_kill($pid,SIGBUS);
        if($this->debug)echo "Send Signal get progress to $pid \n";
        $out = $this->read_fifo();
        return $out;
    }
    
    private function setup_collector_fifo(){
        if(!$this->avail_run)return;
        $u_id = $this->user; 
        if(is_null($u_id)){
            return false;
        } 
        $this->file_fifo_name = str_replace('{user}',$u_id,$this->file_patern); 
        $this->file_fifo_name = str_replace('{pid}',$this->tar_pid,$this->file_fifo_name);
        if(!empty($this->host)){
            $this->file_fifo_name = str_replace('{host}','_'.$this->host,$this->file_fifo_name);
        }else{
            $this->file_fifo_name = str_replace('{host}','',$this->file_fifo_name);
        } 
        //$this->fifo_res = $this->alloc_fifo();
        return true;
    }
    
    private function get_fmsg_info($pid='',$host=''){
        if($this->avail_run)return;
        if(empty($pid)){
            $pid = $this->tar_pid;
        }
//        posix_kill($pid,SIGUSR2);
//        if($this->debug)echo "Send Signal get info to $pid \n";
        $out = $this->read_fmsg($pid,$host);
        return $out;
    }
    private function setup_collector_fmsg($pid='',$host=''){
        
        if($this->avail_run)return;
        $u_id = $this->user;  
        if(is_null($u_id) || $pid==0 || empty($pid)){
            return false;
        } 
        
            
        $this->file_fmsg_user_patern = str_replace('{user}',$u_id,$this->file_patern);
        
        if(!empty($pid) && is_array($pid)){
            $out= array();
            foreach($pid as $host=>$h){
                foreach($h as $id=>$v){
                $this->file_fmsg_name = str_replace('{pid}',$id,$this->file_fmsg_user_patern);
                 $this->file_fmsg_name = str_replace('{host}','_'.$host,$this->file_fmsg_name);
                $fmsg_key = md5($this->file_fmsg_name);
                    $out[$id] = $fmsg_key;
                }
            }
//            if(defined('TEST_PROC_ONLY')) print_r($out);
            return $out;
            
        }else{
//            $this->file_fmsg_name = str_replace('{user}',$u_id,$this->file_patern); 
            $this->file_fmsg_name = str_replace('{pid}',$pid,$this->file_fmsg_user_patern);
            if(empty($host)){
                $this->file_fmsg_name = str_replace('{host}','',$this->file_fmsg_name);
            }else{
                $this->file_fmsg_name = str_replace('{host}','_'.$host,$this->file_fmsg_name);
            }
            $this->fmsg_key = md5($this->file_fmsg_name);
            return $this->fmsg_key;
        }
    }
    
    private function read_fmsg($pid='',$host=''){ 
        if($this->avail_run)return;
//        if(empty($pid))$pid=$this->tar_pid;
//        if(empty($host))$host=$this->tar_host; 
        $out = $this->setup_collector_fmsg($pid,$host);
        
            
            
        return $this->log->get_processes_fmsg($out);
//        if(file_exists($this->path_fifo.$this->file_fmsg_name)){
//        $this->fmsg_pointer = @fopen($this->path_fifo.$this->file_fmsg_name,'r');
//        if( !$this->fmsg_pointer){
//                die('Error: Could not open the file messages.  ' . "\n");
//                return false;
//        }
//        return fgets($this->fmsg_pointer);
//        }else{
//            return false;
//        }
    }
    public function clear_process(){
        if($this->avail_run)return;
        $pid_status=array();
        $win=false;
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $win=true;
        }
        foreach($this->pid_list_info as $host =>$h){
            foreach($h as $pid=>$v){
//            $this->set_target_pid($pid);
                if(!empty($host)){
                    if(isset($this->all_proccess[$pid][$host])){
                        $pid_status['live'][$host][$pid] = $pid;
                    }else{
                        $pid_status['die'][$host][$pid] = $pid;
                        if ($win) {
                            $this->remove_history_pid($pid);
                        }
                    }
                }else{
                    if(isset($this->all_proccess[$pid])){
                        $pid_status['live'][''][$pid] = $pid;
                    }else{
                        $pid_status['die'][''][$pid] = $pid;
                        if ($win) {
                            $this->remove_history_pid($pid);
                        }
                    }
                } 
            }
        }
        if(!empty($pid_status['die'])){
            $this->log->update_process_not_alive($pid_status['die'],true); 
        }
        if(!empty($pid_status['live'])){
            $this->log->update_process_not_alive($pid_status['live']);
        }
        $out = $this->setup_collector_fmsg($this->pid_list_info);
        $this->log->remove_process_fmsg($out);
    }
    
}
