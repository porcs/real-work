<?php

class ImportLog {
    
    public $database = 'log';
    public $prefix_table = 'log_';
    public $engine = " ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ";
    
    public $list_import_log = array();
    public $list_del_log = array();
    public $list_del_atr_log = array();
    
    
    public $list_prod_log = array();
    public $list_prod_atr_log = array();
    
    
    public static $out_of_stock = 7;
    public static $inactive = 8;

    public static $ecode = array(
        1 => 'Missing Product ID',
        2 => 'Missing Reference', 
        3 => 'Wrong SKU',
        4 => 'Missing EAN/UPC',
        5 => 'Wrong UPC',
        6 => 'Missing Name - id_lang ',
        7 => 'Out of stock',
        8 => 'Inactive Product',
        9 => 'Missing Product Reference in Product table',
        10 => 'Missing Product Price',
        11 => 'Missing SKU',
        12 => 'Missing Reference/SKU',
        13 => 'Product Attribute Missing Reference/SKU',
        14 => 'Wrong EAN',
    );
    
    public function __construct($user) {
        
        $this->conn_id = new Db($user, $this->database, false);
        $this->prefix_table=$this->conn_id->prefix_table;
        
        if(!$this->check_table_exists('notifications')){
           $this->_exec($this->create_table_notifications(),false,false);
        }

        if(!$this->check_table_exists('message_log')){
           $this->_exec($this->create_table_message_log(),false,false);
        }
        
        if(!$this->check_table_exists('product_update_log')){
           $this->_exec($this->create_table_product_update_log(),false,false);
        }
    }
    
    //check log
    private function check_table_exists($table_name) 
    {
        return $this->conn_id->db_table_exists($table_name);
    }
    
    public function createLogTable() {
        
        $sql = 'CREATE TABLE '.$this->prefix_table.'log ( 
                    id  int   AUTO_INCREMENT,
                    batch_id  varchar(32) ,
                    shop  varchar(128) ,
                    source  varchar(128) ,
                    type   varchar(32) ,
                    no_total  int(11),
                    no_success  int(11),
                    no_warning  int(11),
                    no_error  int(11),
                    datetime  datetime NOT NULL  ,PRIMARY KEY (id,batch_id,shop,source,type ) 
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;';
        
        if (!$this->_exec($sql,false))
            return false;
        
        $sql  = 'CREATE TABLE '.$this->prefix_table.'server_log (
                    pid int NOT NULL PRIMARY KEY AUTO_INCREMENT,
                    file_target  varchar(32),
                    status  tinyint(1),
                    datetime  datetime 
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;';
        
        if (!$this->_exec($sql,false))
            return false;
        
        $sql = $this->create_table_message_log();
        
        if (!$this->_exec($sql,false)){ 
            return false;
        }
        
        $sql = $this->create_table_product_update_log();
        if (!$this->_exec($sql,false)){ 
            return false;
        }

        return true;
    }

    private function _exec($query,$transaction=false,$multi_query=true) {
        
        if (!$query || empty($query) || !isset($query))
            return FALSE;

        $exec = $this->conn_id->db_exec($query,$transaction,$multi_query);

        return $exec;
    }

    public function insert_log($data) {
        
        $sql = 'REPLACE INTO '.$this->prefix_table.'log (batch_id, shop , source, type, no_total, no_success, no_warning, no_error, datetime) '
                . 'VALUES (\'' . $data['batch_id'] . '\','
                . '\'' . $data['shop'] . '\', '
                . '\'' . $data['source'] . '\', '
                . '\'' . $data['type'] . '\', '
                . '' . $data['no_total'] . ', '
                . '' . $data['no_success'] . ', '
                . '' . $data['no_warning'] . ', '
                . '' . $data['no_error'] . ', '
                . '\'' . $data['datetime'] . '\')  ;';

        if (!$this->_exec($sql,true,true))
            return false;

        return true;
    }
    
    private function create_table_product_update_log($check=true){
        // instock //active 
        if($check){
        if($this->check_table_exists('product_update_log')) 
            return 'select 1 from '.$this->prefix_table.'product_update_log;';
        }
        
        return 'CREATE TABLE '.$this->prefix_table.'product_update_log (
                    id_product INTEGER NOT NULL,
                    id_product_attribute INTEGER,
                    id_shop INTEGER NOT NULL,
                    product_update_type VARCHAR(64),
                    product_update_status text,
                    date_add  datetime NOT NULL,
                    PRIMARY KEY (id_product, id_product_attribute, id_shop, product_update_type) 
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;';
    }
    
    private function create_table_message_log($check=true){
        if($check){
        if($this->check_table_exists('message_log')) 
            return 'select 1 from '.$this->prefix_table.'message_log;';
        }
        
        return 'CREATE TABLE '.$this->prefix_table.'message_log ( 
                    id_log int NOT NULL  AUTO_INCREMENT,
                    batch_id  varchar(32) NOT NULL ,
                    id_shop  int NOT NULL ,
                    error_code  tinyint(1) ,
                    severity  tinyint(1) ,
                    product_type  varchar(16) ,
                    id_product  int(11) NOT NULL ,
                    id_product_attribute  int(11) ,
                    message  text,
                    date_add  datetime NOT NULL  ,
                   PRIMARY KEY (id_log,batch_id,id_shop,error_code,severity ,product_type,id_product,id_product_attribute) 
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;';
    }

    private function create_table_notifications($check=true){
        if($check){
            if($this->check_table_exists('notifications')){
                return 'select 1 from '.$this->prefix_table.'message_log;';
            }
        }
        return 'CREATE TABLE '.$this->prefix_table.'notifications (                
                `marketplace`  varchar(32) NOT NULL ,
                `id_shop`  int(11) NOT NULL ,
                `id_country`  int(11) NOT NULL ,
                `notification_identify`  varchar(16) NOT NULL ,
                `notification_date`  datetime NULL ,
                `notification_link`  varchar(255) NULL ,
                `flag_unread`  tinyint(1) NULL ,
                `notification_message` text NULL ,
                `notification_key` text NULL ,
                `date_upd`  datetime NULL ,
                PRIMARY KEY (`marketplace`, `id_shop`, `id_country`, `notification_identify`)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ' ;
    }
    
    public function empty_log($id_shop){
        
        $sql = $this->create_table_message_log();
        $this->_exec($sql,false,false);
        $sql = "DELETE FROM  {$this->prefix_table}message_log WHERE id_shop = '".$id_shop."' ; ";
 
        $out=$this->_exec($sql,false,false);
 
        if (!$out)
            return false;

        return true;
        
    }
    
    public function deltete_log($datas, $id_shop){
     
        $sql = '';
        
        if(isset($datas['id_product']) && !empty($datas['id_product'])){
            
            $error_code = isset($datas['error_code']) ? $datas['error_code'] : null;
            $severity = isset($datas['severity']) ? $datas['severity'] : null;
            $type = isset($datas['type']) ? $datas['type'] : null;
            $id_product = $datas['id_product'];
                        
            $sql .= "DELETE FROM {$this->prefix_table}message_log WHERE id_shop = ".$id_shop." "
                . "AND error_code = ".$error_code." "
                . "AND severity = ".$severity." "
                . "AND product_type = '".$type."' "
                . "AND id_product = ".$id_product." ";
            
            if(isset($datas['id_product_attribute'])){
                $sql .= "AND id_product_attribute = ".$datas['id_product_attribute']." ";
                $this->list_del_atr_log[]="('{$error_code}','{$severity}','{$type}','{$id_product}','{$datas['id_product_attribute']}')";
            }else{
                $this->list_del_log[]="('{$error_code}','{$severity}','{$type}','{$id_product}')"; 
            }
            
            $sql .= "; " ;
        }
 
         // Add product updated from inactive to active, out of stock to instock, to use with amazon repricing when product has change.
        if($error_code == ImportLog::$inactive || $error_code == ImportLog::$out_of_stock){
            
            $product_update_type = $product_update_status = null;
            $sql_id_product_attribute = $value_id_product_attribute = '';
            
            if(isset($datas['id_product_attribute'])){
                $sql_id_product_attribute = ", id_product_attribute";
                $value_id_product_attribute = ", " . $datas['id_product_attribute'] . " ";
            }
            
            if($error_code == ImportLog::$inactive){
                $product_update_type = '"active"';
                $product_update_status = '"active"';
            }
            else if($error_code == ImportLog::$out_of_stock) {
                $product_update_type = '"stock"';
                $product_update_status = '"instock"';
            } 
            
            $sql .= "REPLACE INTO {$this->prefix_table}product_update_log (id_product $sql_id_product_attribute, id_shop, product_update_type, product_update_status, date_add) ";
            $sql .= "VALUES ($id_product $value_id_product_attribute, $id_shop, $product_update_type, $product_update_status, '" . date('Y-m-d H:i:s') . "') ; ";
            if(!isset($datas['id_product_attribute'])){
                $this->list_prod_log[] = "($id_product $value_id_product_attribute, $id_shop, $product_update_type, $product_update_status, '" . date('Y-m-d H:i:s') . "')";
            }else{
                $this->list_prod_atr_log[] = "($id_product $value_id_product_attribute, $id_shop, $product_update_type, $product_update_status, '" . date('Y-m-d H:i:s') . "')";
            }
        }       

        return $sql;        
    }
          
    
    public function get_all_sql_delete_log($id_shop){
        $sql =''; 
        if(!empty($this->list_del_log)){
        $sql .= " DELETE FROM {$this->prefix_table}message_log WHERE id_shop = '".$id_shop."'  "
                . " and (error_code,severity,product_type,id_product) in (".implode(' , ', $this->list_del_log).") ; \n";
        }
        if(!empty($this->list_del_atr_log)){
        $sql .= " DELETE FROM {$this->prefix_table}message_log WHERE id_shop = '".$id_shop."'  "
                . " and (error_code,severity,product_type,id_product,id_product_attribute) in (".implode(' , ', $this->list_del_atr_log).") ; \n";
        }
        return $sql;
    }
    
    public function get_all_product_update_log(){
        $sql =''; 
        if(!empty($this->list_prod_log)){
        $sql .= " REPLACE INTO {$this->prefix_table}product_update_log (id_product , id_shop, product_update_type, product_update_status, date_add)  values "
                . " ".implode(' , ', $this->list_prod_log)." ; \n";
        }
        if(!empty($this->list_prod_atr_log)){
        $sql .= " REPLACE INTO {$this->prefix_table}product_update_log (id_product , id_product_attribute , id_shop, product_update_type, product_update_status, date_add)  values "
                . " ".implode(' , ', $this->list_prod_atr_log)." ; \n";
        }
        return $sql;
    }
    
    public function add($sql){
        if (!$this->_exec($sql,true,true))
            return false;

        return true;
    }

    public function message_log($datas, $batch_id, $id_shop, $time){
           
        $sql = '';
        
        if(isset($datas['id_product']) && !empty($datas['id_product']) && !empty($batch_id) && !empty($id_shop) && !empty($time)){
            
            $error_code = isset($datas['error_code']) ? $datas['error_code'] : null;
            $severity = isset($datas['severity']) ? $datas['severity'] : null;
            $type = isset($datas['product_type']) ? $datas['product_type'] : null;
            $id_product = $datas['id_product'];
            $id_product_attribute = isset($datas['id_product_attribute']) ? $datas['id_product_attribute'] : null;

            if(isset($datas['message']) && !empty($datas['message'])){
                $message = isset(self::$ecode[$datas['error_code']]) ? self::$ecode[$datas['error_code']] . ' : ' . $datas['message'] : null;
            } else {
                $message = isset(self::$ecode[$datas['error_code']]) ? self::$ecode[$datas['error_code']] : null;
            }
            
            $sql .= "REPLACE INTO {$this->prefix_table}message_log (batch_id, id_shop, error_code, severity, product_type, id_product, id_product_attribute, message, date_add) 
                    VALUES ('".$batch_id."', ".$id_shop.", ".$error_code.", ".$severity.", '".$type."', ".$id_product.", ".$id_product_attribute.", '".$message."', '". $time."'); ";
            $this->list_import_log[] = "('".$batch_id."', ".$id_shop.", ".$error_code.", ".$severity.", '".$type."', ".$id_product.", ".$id_product_attribute.", '".$message."', '". $time."')";
        }
        
        // remove product from product_update_log // to avoid to send inactive & out of stock product when send repricing to Amazon
        if($datas['error_code'] == ImportLog::$inactive || $datas['error_code'] == ImportLog::$out_of_stock){
            
            $product_update_type = $product_update_status = null;
            $sql_id_product_attribute = $value_id_product_attribute = '';
            
            if(isset($datas['id_product_attribute'])){
                $sql_id_product_attribute = ", id_product_attribute";
                $value_id_product_attribute = ", " . $datas['id_product_attribute'] . " ";
            }
            
            if($datas['error_code'] == ImportLog::$inactive){
                $product_update_type = '"active"';
                $product_update_status = '"inactive"';
            }
            else if($datas['error_code'] == ImportLog::$out_of_stock) {
                $product_update_type = '"stock"';
                $product_update_status = '"out_of_stock"';
            } 
            
            $sql .= "REPLACE INTO {$this->prefix_table}product_update_log (id_product $sql_id_product_attribute, id_shop, product_update_type, product_update_status, date_add) ";
            $sql .= "VALUES ($id_product $value_id_product_attribute, $id_shop, $product_update_type, $product_update_status, '" . date('Y-m-d H:i:s') . "') ; ";
            
            if(!isset($datas['id_product_attribute'])){
                $this->list_prod_log[] = "($id_product $value_id_product_attribute, $id_shop, $product_update_type, $product_update_status, '" . date('Y-m-d H:i:s') . "')";
            }else{
                $this->list_prod_atr_log[] = "($id_product $value_id_product_attribute, $id_shop, $product_update_type, $product_update_status, '" . date('Y-m-d H:i:s') . "')";
            }
            
        }    
        
        return $sql;
        /*if (!$this->_exec($sql))
            return false;

        return true;*/
        
    }
    public function get_all_message_log(){
        $sql ='';
        if(!empty($this->list_import_log)){
        $sql = "REPLACE INTO {$this->prefix_table}message_log (batch_id, id_shop, error_code, severity, product_type, id_product, id_product_attribute, message, date_add) 
                    VALUES ".implode(' , ', $this->list_import_log)." ; \n";
        }
        return $sql;
    }
    
    public function EAN_UPC_Check($code)
    {
        //first change digits to a string so that we can access individual numbers
        $digits = sprintf('%012s', substr(sprintf('%013s', $code), 0, 12));
        // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;
        // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;
        // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = $next_ten - $total_sum;
        
        return ((int) $code == (int) ($digits . $check_digit));
    }

    /*public function ValidateSKU($SKU)
    {
    	return( $SKU != null && strlen($SKU) && preg_match('/[\x00-\xFF]{1,40}/', $SKU) && preg_match('/[^ ]$/', $SKU) ) ;
    }*/
    
    public function ValidateSKU($SKU)
    {
        return ($SKU != null && strlen($SKU) && preg_match('/[\x00-\xFF]{1,40}/', $SKU) && preg_match('/[^ ]$/', $SKU) && preg_match('/^[^ ]/', $SKU));
    }
}
