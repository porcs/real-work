<?php

class FeatureValue extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'feature_value',
        'primary'   => array('id_feature_value', 'id_shop', 'id_lang'),
        'unique'    => array('feature_value' => array('id_feature_value', 'id_shop', 'id_lang')),
        'fields'    => array(
            'id_feature'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_feature_value'  =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_lang'           =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'value'             =>  array('type' => 'varchar', 'required' => true, 'size' => 255),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    
}