<?php

class ProductFeature extends ObjectModel
{
   
    public static $definition = array(
        'table'     => 'product_feature',
        'unique'    => array('product_feature' => array('id_product', 'id_feature', 'id_shop')),
        'fields'    => array(
            'id_product'        =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_feature'        =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_feature_value'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
}