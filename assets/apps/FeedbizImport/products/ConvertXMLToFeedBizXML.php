<?php

class ConvertXMLToFeedBizXML extends ObjectModel
{
     
    public function gmerchant($data, $time)
    {   
        $Document = new DOMDocument();
        $Document->preserveWhiteSpace = true;
        $Document->formatOutput = true;
        $Document->encoding = 'utf-8';
        $Document->version = '1.0';
        $ExportData     = $Document->appendChild($exportData = $Document->createElement('ExportData'));
        $Products       = $ExportData->appendChild($Document->createElement('Products'));
        $Attributes     = $ExportData->appendChild($Document->createElement('Attributes'));
        $Categories     = $ExportData->appendChild($Document->createElement('Categories'));
        $Conditions     = $ExportData->appendChild($Document->createElement('Conditions'));
        $Currencies     = $ExportData->appendChild($Document->createElement('Currencies'));
        $Languages      = $ExportData->appendChild($Document->createElement('Language'));
        $Manufacturers  = $ExportData->appendChild($Document->createElement('Manufacturers'));
        $Units          = $ExportData->appendChild($Document->createElement('Units'));
        
        $mno            = 0;
        $cno            = 1;
        $conno          = 0;
        $lno            = 0;
        $currno         = 0;
        $attr           = 0;
        $colorno        = 0;
        $sizeno         = 0;
        
        $ProductDetails = array();
        $history = array();
        
        $time = strtotime($time);
        //Attributes
        //color
        $ProductDetails['Attribute']['color']['ID'] = $Attributes->appendChild($color_type = $Document->createElement('Attribute')); 
        $color_type->setAttribute('ID', 1);
        $color_type->setAttribute('type', 'color');
        
        //select
        $ProductDetails['Attribute']['size']['ID'] = $Attributes->appendChild($color_type = $Document->createElement('Attribute')); 
        $color_type->setAttribute('ID', 2);
        $color_type->setAttribute('type', 'size');
        
        $ProductDetails['Attribute']['color']['Values'] = $ProductDetails['Attribute']['color']['ID']->appendChild($Document->createElement('Values'));
        $ProductDetails['Attribute']['size']['Values'] = $ProductDetails['Attribute']['size']['ID']->appendChild($Document->createElement('Values'));
        $gkey = 1;
        
        if( !empty($data->gdata->fileurl))
        {
            $get_xml = @file_get_contents($data->gdata->fileurl);
            $xml = str_replace('&acute;', '', $get_xml);
//            var_dump($xml); exit;
            
            if( isset($xml) && !empty($xml) && $xml != null )
            { 
                $oXml = new SimpleXmlElement($xml);
                
                $exportData->setAttribute('ShopName', $oXml->channel->title);

                $no = 0;

                //Language $Languages
                $lno++;
                //$ProductDetails['Language'][$lno] = $Languages->appendChild($Document->createElement('Language'));
                $ProductDetails['Language']['Name'][$lno] = $Languages->appendChild($language_id = $Document->createElement('Name'));
                $ProductDetails['Language']['Name'][$lno]->appendChild($Document->createCDATASection($data->gdata->language)); 

                $language_id->setAttribute('ID', $lno); 
                $language_id->setAttribute('iso_code', $data->gdata->iso_code);
                $language_id->setAttribute('is_default', $data->gdata->is_default);
                
                $currno++;
                $Currencies->appendChild($currency_id = $Document->createElement('Currency', $data->gdata->curr_name));
                $currency_id->setAttribute('ID', $currno); 
                $currency_id->setAttribute('iso_code', $data->gdata->curr_iso);
                            
                //root category
                if(!isset($ProductDetails['Category']) || empty($ProductDetails['Category']))
                {
                    $ProductDetails['Category'] = $Categories->appendChild($category_id = $Document->createElement('Category'));
                    $category_id->setAttribute('ID', 1);
                    $category_id->setAttribute('parent', 'root');
                    $category_id->setAttribute('code', 1);
                }

                $ProductDetails['CategoryName'] = $ProductDetails['Category']->appendChild($cat_name = $Document->createElement('Name'));
                $cat_name->setAttribute('lang', $gkey);
                $ProductDetails['CategoryName']->appendChild($Document->createCDATASection('Home')); 

                foreach ($oXml->channel->item as $item)
                {                                           
                    $namespaces = $item->getNameSpaces(true);
                    $g = $item->children($namespaces["g"]);

                    if(isset($g->item_group_id) && !empty($g->item_group_id))
                        $id_product = str_replace(array("-","+"), "", filter_var($g->item_group_id, FILTER_SANITIZE_NUMBER_INT));
                    else
                        $id_product = str_replace(array("-","+"," ","_"), "", filter_var($g->id, FILTER_SANITIZE_NUMBER_INT));

                    if(!isset($ProductDetails[$id_product]['ID']) || empty($ProductDetails[$id_product]['ID']))
                    {   
                        $no++;
                        $ProductDetails[$id_product]['ID'] = $Products->appendChild($product_no = $Document->createElement('Product'));
                        $product_no->setAttribute('ID', $no);
                        $ProductDetails[$id_product]['ProductData'] = $ProductDetails[$id_product]['ID']->appendChild($Document->createElement('ProductData'));
                    }

                    //Units
                    if(!isset($ProductDetails['Unit']) || empty($ProductDetails['Unit']))
                    {
                        if(isset($g->shipping_weight) && !empty($g->shipping_weight))
                        {
                            $shipping_weight = filter_var(preg_replace("/[^A-Z][^a-z]+/", '', $g->shipping_weight), FILTER_SANITIZE_STRING);
                            $ProductDetails['Unit'] = $Units->appendChild($unit = $Document->createElement('Unit', $shipping_weight));
                            $unit->setAttribute('ID', 1);
                            $unit->setAttribute('Type', 'Weight');
                        }
                    }

                    //Product Name
                    if ( isset($item->title) && !empty($item->title) )
                    {
                        if(!isset($ProductDetails[$id_product]['Names']) || empty($ProductDetails[$id_product]['Names']))
                             $ProductDetails[$id_product]['Names'] =  $ProductDetails[$id_product]['ProductData']->appendChild($Document->createElement('Names'));

                        if(!isset($ProductDetails[$id_product]['Name'][$gkey]) || empty($ProductDetails[$id_product]['Name'][$gkey]))
                        {    
                            $title = $item->title;

                            if(isset($item->link) || !empty($item->link))
                            {
                                $data = parse_url($item->link);

                                if(isset($data['fragment']) || !empty($data['fragment']))
                                {
                                    $fragment = explode("/", $data['fragment']);
                                    foreach ($fragment as $flag)
                                        if(isset($flag) && !empty($flag))
                                        {
                                            $flag = explode("-", str_replace(array("_"), " ", $flag)) ;
                                            $title_link = str_ireplace($flag[1], "/", str_ireplace('-', ' ', str_replace(array(',', '.', '@'), '-', $item->title)));
                                            break;
                                        }
                                    $title_link = explode("/", $title_link);
                                    $title = trim($title_link[0]);
                                }
                            }

                            $ProductDetails[$id_product]['Name'][$gkey] = $ProductDetails[$id_product]['Names']->appendChild($p_name = $Document->createElement('Name')); 
                            $name = rtrim(trim($title), ' - ');
                            $ProductDetails[$id_product]['Name'][$gkey]->appendChild($Document->createCDATASection($name)); 
                            $p_name->setAttribute('lang', $gkey);
                        }   
                    }

                    //Product Descriptions
                    if ( isset($item->description) && !empty($item->description) )
                    {
                        if(!isset($ProductDetails[$id_product]['Descriptions']) || empty($ProductDetails[$id_product]['Descriptions']))
                            $ProductDetails[$id_product]['Descriptions'] = $ProductDetails[$id_product]['ProductData']->appendChild($Document->createElement('Descriptions'));

                        if(!isset($ProductDetails[$id_product]['Description'][$gkey]) || empty($ProductDetails[$id_product]['Description'][$gkey]))
                        {    
                            $description =  strip_tags($item->description);
                            $ProductDetails[$id_product]['Description'][$gkey] = $ProductDetails[$id_product]['Descriptions']->appendChild($p_desc = $Document->createElement('Description'));                                     
                            $ProductDetails[$id_product]['Description'][$gkey]->appendChild($Document->createCDATASection($description)); 
                            $p_desc->setAttribute('lang', $gkey);
                        }   
                    }

                    //Identifier
                    if(!isset($ProductDetails[$id_product]['Identifier']) || empty($ProductDetails[$id_product]['Identifier']))
                    {
                        $ProductDetails[$id_product]['Identifier'] = $ProductDetails[$id_product]['ID']->appendChild($Document->createElement('Identifier'));
                        $ProductDetails[$id_product]['Identifier']->appendChild($i_id =$Document->createElement('Reference', (int)$id_product) );
                        $i_id->setAttribute('type', 'ID');
                        
                        //Reference
                        if(isset($g->mpn) && !empty($g->mpn))
                        {
                            $ProductDetails[$id_product]['Reference'] = $ProductDetails[$id_product]['Identifier']->appendChild($ref = $Document->createElement('Reference'));
                            $ProductDetails[$id_product]['Reference']->appendChild($Document->createCDATASection(trim($g->mpn))); 
                            $ref->setAttribute('type', 'Reference');
                        }

                        if(isset($g->gtin) && !empty($g->gtin))
                        {
                            $ProductDetails[$id_product]['Code'] = $ProductDetails[$id_product]['Identifier']->appendChild($ref = $Document->createElement('Code', $g->gtin));  
                            $ref->setAttribute('type', 'EAN'); 
                        }

                    }

                    if(!isset($history[$id_product]['Images']) || !$history[$id_product]['Images'])
                        $image_product_no = 1;

                    // Images Product
                    if(!isset($ProductDetails[$id_product]['Images']) || empty($ProductDetails[$id_product]['Images']))
                        $ProductDetails[$id_product]['Images'] = $ProductDetails[$id_product]['ID']->appendChild($Document->createElement('Images'));

                    if(isset($ProductDetails[$id_product]['Images']) && !empty($ProductDetails[$id_product]['Images']))
                    {
                        //default image
                        if (isset($g->image_link) && !empty($g->image_link))
                        {
                            $image_link = trim($g->image_link);
                    
                            if(!isset($history['image'][$image_link]) || !$history['image'][$image_link])
                            {
                                if(!isset($ProductDetails[$id_product]['Image'][1]) || empty($ProductDetails[$id_product]['Image'][1]))
                                {
                                    $ProductDetails[$id_product]['Image'][1] = $ProductDetails[$id_product]['Images']->appendChild($img_product = $Document->createElement('Image')); 
                                    $ProductDetails[$id_product]['Image'][1]->appendChild($Document->createCDATASection($g->image_link)); 
                                    $img_product->setAttribute('id', $image_product_no);
                                    $img_product->setAttribute('type', 'default');
                                }
                                else
                                {
                                    $image_product_no++;
                                    $ProductDetails[$id_product]['Image'][$image_product_no] = $ProductDetails[$id_product]['Images']->appendChild($img_product = $Document->createElement('Image')); 
                                    $ProductDetails[$id_product]['Image'][$image_product_no]->appendChild($Document->createCDATASection($g->image_link)); 
                                    $img_product->setAttribute('id', $image_product_no);
                                    $img_product->setAttribute('type', 'normal');
                                }
                                
                                $history['images'][$id_product][$image_product_no] = $image_link;
                            }
                            
                            $history['image'][$image_link] = true;
                        }

                    }
                    
                    //additional image
                    if (isset($g->additional_image_link) && !empty($g->additional_image_link))
                    {
                        foreach ($g->additional_image_link as $image_product)                            
                        {    
                            $image_product_no++;
                            $image_product = trim($image_product);
                            if(!isset($history['image'][$image_product]) || !$history['image'][$image_product])
                            {
                                $ProductDetails[$id_product]['Image']= $ProductDetails[$id_product]['Images']->appendChild($img_product = $Document->createElement('Image'));
                                $ProductDetails[$id_product]['Image']->appendChild($Document->createCDATASection($image_product)); 
                                $img_product->setAttribute('id', $image_product_no);
                                $img_product->setAttribute('type', 'normal');

                                $history['images'][$id_product][$image_product_no] = $image_product;
                            }
                            $history['image'][$image_product] = true;
                        }

                    }
                    $history[$id_product]['Images'] = true;

                    //Manufacturer
                    if(!isset($ProductDetails[$id_product]['Manufacturer']) || empty($ProductDetails[$id_product]['Manufacturer']))
                    {
                        if(isset($g->brand) && !empty($g->brand) && $g->brand != "" )
                        {
                            $manufacturer = (string)$g->brand;
                            if(!isset($history['manufactories'][$manufacturer]) 
                                    || !$history['manufactories'][$manufacturer] 
                                    || empty($history['manufactories'][$manufacturer]))
                            {
                                $mno++;
                                $ProductDetails[$id_product]['Manufacturer'] = $Manufacturers->appendChild($product_no = $Document->createElement('Manufacturer'));
                                $ProductDetails[$id_product]['Manufacturer']->appendChild($Document->createCDATASection($g->brand)); 
                                $product_no->setAttribute('ID', $mno); 
                                $product_no->setAttribute('code', $time); 

                                $history['manufactory'][$mno] = $manufacturer;
                            }

                            //Product Manufacturer
                            $ProductDetails[$id_product]['Manufacturer'] = $ProductDetails[$id_product]['ID']->appendChild($product_no = $Document->createElement('Manufacturer'));
                            $product_no->setAttribute('ID', array_search($manufacturer, $history['manufactory'])); 

                            $history['manufactories'][$manufacturer] = TRUE;
                        }
                    }

                    

                    //Category
                   // $num = 0;
                    if(isset($g->product_type) && !empty($g->product_type))
                    {
                        foreach ($g->product_type as $cate)
                        {

                            if(!isset($ProductDetails[$id_product]['Categories']) || empty($ProductDetails[$id_product]['Categories']))
                            {
                                //$num++;
                                $ProductDetails[$id_product]['Categories']=$ProductDetails[$id_product]['ID']->appendChild($category_att=$Document->createElement('Categories'));
                            }

                            $cate = ObjectModel::escape_str($cate);

                            if(strlen(strchr($cate,"&gt;")))
                                $categories = explode(" &gt; ", $cate);
                            else
                                $categories = explode(" > ", $cate);

                            foreach ($categories as $ckey => $category)
                            {
                                $category = trim($category);

                                if(!isset($history['Category'][$category][$gkey]) || !$history['Category'][$category][$gkey])
                                {
                                    $cno++;

                                    if((int)$ckey > 0)
                                    {
                                        $ckey_no = intval($ckey)-1;
                                        $parent  = array_search($categories[$ckey_no], $history['Category']['name']);
                                    }
                                    else 
                                        $parent = 1;

                                    if(!isset($history['Category'][$category][$gkey]) || empty($history['Category'][$category][$gkey]))
                                    {
                                        $ProductDetails[$id_product][$ckey]['Category'] = $Categories->appendChild($category_id = $Document->createElement('Category'));
                                        $category_id->setAttribute('ID', $cno);
                                        $category_id->setAttribute('parent', $parent);
                                        $category_id->setAttribute('code', $time);
                                        $history['Category']['name'][$cno] = $category;
                                    }

                                    $ProductDetails['CategoryName'] = $ProductDetails[$id_product][$ckey]['Category']->appendChild($cat_name = $Document->createElement('Name'));
                                    $cat_name->setAttribute('lang', $gkey);
                                    $ProductDetails['CategoryName']->appendChild($Document->createCDATASection($category)); 

                                    $history['Category']['number'][$ckey] = $cno;
                                }
                                $history['Category'][$category][$gkey] = true;
                            }
                        }

                        //product
                        if(!isset($ProductDetails[$id_product]['Category']) || empty($ProductDetails[$id_product]['Category']))
                        {
                            $category = array_search(trim($categories[(sizeof($categories)-1)]), $history['Category']['name']);
                            $ProductDetails[$id_product]['Category'] = $ProductDetails[$id_product]['Categories']->appendChild($category_att = $Document->createElement('Category')); 
                            $category_att->setAttribute('ID', $category);
                            $category_att->setAttribute('type', 'Default');
                        }
                    }

                    //Condition
                    if(!isset($ProductDetails[$id_product]['Condition']) || empty($ProductDetails[$id_product]['Condition']))
                    {
                        if(isset($g->condition) && !empty($g->condition))
                        {
                            $condition = str_replace("'", "", $g->condition);   
                            if(!isset($history['conditions'][$condition]) || empty($history['conditions'][$condition]))
                            {
                                $conno++;
                                $ProductDetails[$id_product]['Condition'] = $Conditions->appendChild($cond_new_id = $Document->createElement('Condition'));
                                $ProductDetails[$id_product]['Condition']->appendChild($Document->createCDATASection($condition )); 
                                $cond_new_id->setAttribute('ID', $conno);

                                $history['condition'][$conno] = $condition;   
                            }

                            $ProductDetails[$id_product]['Condition'] = $ProductDetails[$id_product]['ID']->appendChild($cond_new_id = $Document->createElement('Condition'));
                            $cond_new_id->setAttribute('ID', array_search($condition, $history['condition']));

                            $history['conditions'][$condition] = true;                               
                        }
                    }

                    //Combinations
                    $gid = array();
//                        echo '<pre>' . print_r ($id_product, true) . '</pre>';
//                        echo '<pre>' . print_r ($gid, true) . '</pre>';
                    if(isset($g->item_group_id) && !empty($g->item_group_id))
                    {
                        $gid[$id_product] = array(0 => str_replace(array("-","+"," ","_"), "", filter_var($g->id, FILTER_SANITIZE_NUMBER_INT)));
                        
                        if(isset($gid[$id_product]) && !empty($gid[$id_product]))
                        {
                            foreach ($gid[$id_product] as $id)
                            {
                                if((isset($g->color) && !empty($g->color)) || (isset($g->size) && !empty($g->size)))
                                {
                                    $id_item = str_replace(array("-","+"," ","_"), "", filter_var($g->id, FILTER_SANITIZE_NUMBER_INT));
                                    
                                    if( !isset($history['Combination'][$id_product]) || !$history['Combination'][$id_product]  )
                                        $attr = 0;

                                    //Combination
                                    if(!isset($ProductDetails[$id_product]['Combinations']) || empty($ProductDetails[$id_product]['Combinations']))
                                        $ProductDetails[$id_product]['Combinations'] = $ProductDetails[$id_product]['ID']->appendChild($Document->createElement('Combinations')); 

                                    if(!isset($ProductDetails[$id_product]['Combination'][$id]) || empty($ProductDetails[$id_product]['Combination'][$id]))
                                    { 
                                        $attr++;
                                        $ProductDetails[$id_product]['Combination'][$id] = $ProductDetails[$id_product]['Combinations']->appendChild($aid = $Document->createElement('Combination'));
                                        $aid->setAttribute('id', $attr);

                                        //Identifier
                                        $ProductDetails[$id_product]['Combination']['Identifier'] = $ProductDetails[$id_product]['Combination'][$id]->appendChild($id_com = $Document->createElement('Identifier'));
                                        $ProductDetails[$id_product]['Combination']['Identifier']->appendChild($id_com_Ref = $Document->createElement('Reference', $id_item));
                                        $id_com_Ref->setAttribute('type', 'ID');

                                        //Reference
                                        if(isset($g->mpn) && !empty($g->mpn))
                                        {
                                            $ProductDetails[$id_product]['Combination']['Reference'] = $ProductDetails[$id_product]['Combination']['Identifier']->appendChild($ref = $Document->createElement('Reference'));
                                            $ProductDetails[$id_product]['Combination']['Reference']->appendChild($Document->createCDATASection(trim($g->mpn))); 
                                            $ref->setAttribute('type', 'Reference');
                                        }

                                        if(isset($g->gtin) && !empty($g->gtin))
                                        {
                                            $ProductDetails[$id_product]['Combination']['Identifier']->appendChild($ref = $Document->createElement('Code', $g->gtin));   
                                            $ref->setAttribute('type', 'EAN'); 
                                        }
                        
                                        //Attribute
                                        $ProductDetails[$id_product]['Combination']['Attributes'][$id] = $ProductDetails[$id_product]['Combination'][$id]->appendChild($Document->createElement('Attributes'));     

                                        //Images 
                                        $ProductDetails[$id_product]['Combination']['Images'][$id] = $ProductDetails[$id_product]['Combination'][$id]->appendChild($Document->createElement('Images'));     

//                                        $images_link = array();
//
//                                        if(isset($g->image_link) && !empty($g->image_link))
//                                            $images_link = $g->image_link;
//                                        
//                                        else if(isset($g->additional_image_link) && !empty($g->additional_image_link))
//                                            $images_link = $g->additional_image_link;
//
//                                        foreach ($images_link as $gimage)
//                                        { 
                                            $g_image = array_search($g->image_link, $history['images'][$id_product]);
                                            if(isset($g_image) && !empty($g_image) && $g_image != "")
                                            {
                                                $ProductDetails[$id_product]['Combination']['Images'][$id]->appendChild($id_image = $Document->createElement('Image'));     
                                                $id_image->setAttribute("id", $g_image);
                                            }
//                                        }

                                        //PriceOveride
                                        if((isset($g->price) || !empty($g->price)) && (isset($g->item_group_id) && !empty($g->item_group_id)))
                                        {
                                            if(!isset($ProductDetails[$id_product]['Combination']['PriceOverride'][$id]) || empty($ProductDetails[$id_product]['Combination']['PriceOverride'][$id]))
                                            {
                                                $price_overide = filter_var($g->price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                                                $ProductDetails[$id_product]['Combination']['PriceOverride'][$id] = $ProductDetails[$id_product]['Combination'][$id]->appendChild($Document->createElement('PriceOverride'));
                                                $ProductDetails[$id_product]['Combination']['PriceOverride'][$id]->appendChild($price_comb = $Document->createElement('Operation', $price_overide));
                                                $price_comb->setAttribute('type', 'Increase');
                                                $price_comb->setAttribute('value', 1);
                                            }
                                        }

                                        //SaleOveride
                                        if(isset($g->sale_price) && !empty($g->sale_price))
                                        {
                                            $sale_overide = filter_var($g->sale_price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION); 
                                            $ProductDetails[$id_product]['Combination']['PriceOverride'][$id]->appendChild($Document->createElement('Striked', $sale_overide ));

                                        }

                                        //Topology 
                                        if(isset($g->shipping_weight) && !empty($g->shipping_weight))
                                        {
                                            if(!isset($ProductDetails[$id_product]['Combination']['Topology'][$id]) || empty($ProductDetails[$id_product]['Combination']['Topology'][$id]))
                                            {
                                                $ProductDetails[$id_product]['Combination']['Topology'][$id] = $ProductDetails[$id_product]['Combination'][$id]->appendChild($Document->createElement('Topology'));
                                                $ProductDetails[$id_product]['Combination']['Topology'][$id]->appendChild($weight = $Document->createElement('Unit', filter_var($g->shipping_weight, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION)));
                                                $weight->setAttribute('ID', 1);
                                                $weight->setAttribute('Name', "Weight");
                                            }
                                        }

                                        //Availabilities
                                        if(isset($g->item_group_id) && !empty($g->item_group_id))
                                        {
                                            $ProductDetails[$id_product]['Combination']['Availabilities'][$id] = $ProductDetails[$id_product]['Combination'][$id]->appendChild($Document->createElement('Availabilities'));
                                            $ProductDetails[$id_product]['Combination']['Availabilities'][$id]->appendChild($Document->createElement('Stock' , $g->quantity));
                                            $ProductDetails[$id_product]['Combination']['Availabilities'][$id]->appendChild($Document->createElement('Active' , 1));
                                        }
                                    }

                                    if(isset($g->item_group_id) && !empty($g->item_group_id))
                                    {
                                        //Attribute size
                                        
                                        if(isset($g->size) && !empty($g->size))
                                            foreach ($g->size as $gsize) 
                                            {
//                                                echo $gsize . '<br/>';
                                                $gsize =trim($gsize);

                                                if(!isset($history['Combination'][$gsize]) || !$history['Combination'][$gsize])
                                                {
                                                    if(!isset($ProductDetails[$id_product]['Attribute']['size'][$id]) || empty($ProductDetails[$id_product]['Attribute']['size'][$id]))
                                                    {
                                                        $sizeno++;
                                                        $ProductDetails[$id_product]['Attribute']['size'][$id] = $ProductDetails['Attribute']['size']['Values']->appendChild($Value_lang = $Document->createElement('Value'));
                                                        $Value_lang->setAttribute('Option', $sizeno);

                                                        $history['Combination']['size'][$sizeno] = $gsize;
                                                    }
                                                }

                                                if(isset($ProductDetails[$id_product]['Attribute']['size'][$id]) || !empty($ProductDetails[$id_product]['Attribute']['size'][$id]))
                                                {
                                                    if(!isset($ProductDetails[$id_product]['Attribute']['Name'][$gsize][$lno]) || empty($ProductDetails[$id_product]['Attribute']['Name'][$gsize][$lno]))
                                                    {
                                                        $ProductDetails[$id_product]['Attribute']['Name'][$gsize][$lno] = $ProductDetails[$id_product]['Attribute']['size'][$id]->appendChild($vname_lang = $Document->createElement('Name'));
                                                        $vname_lang->setAttribute('lang', $lno);
                                                        $ProductDetails[$id_product]['Attribute']['Name'][$gsize][$lno]->appendChild($Document->createCDATASection($gsize));
                                                    }
                                                }

                                                $history['Combination'][$gsize] = true;

                                                //Product Attribute
                                                if(isset($ProductDetails[$id_product]['Combination']['Attributes'][$id]) || !empty($ProductDetails[$id_product]['Combination']['Attributes'][$id]))
                                                {
                                                    $option2 = array_search($gsize, $history['Combination']['size']);
                                                    if(!isset($history['Combination']['Attributes'][$id_product][$id][2][$option2]) 
                                                            || !$history['Combination']['Attributes'][$id_product][$id][2][$option2] 
                                                            && !empty($option2))
                                                    {
                                                        $ProductDetails[$id_product]['Combination']['Attributes'][$id]->appendChild($attr_id = $Document->createElement('Attribute'));
                                                        $attr_id->setAttribute('ID', 2);
                                                        $attr_id->setAttribute('Option', $option2 );
                                                    }

                                                    $history['Combination']['Attributes'][$id_product][$id][2][$option2] = true;
                                                }  
                                            }

                                        //Attribute Color                
                                        if(isset($g->color) && !empty($g->color))
                                            foreach ($g->color as $gcolor) 
                                            {  
                                                $gcolor =trim($gcolor);

                                                if(!isset($history['Combination'][$gcolor]) || !$history['Combination'][$gcolor])
                                                {
                                                    $colorno++;
                                                    if(!isset($ProductDetails[$id_product]['Attribute']['color'][$id]) || empty($ProductDetails[$id_product]['Attribute']['color'][$id]))
                                                    {
                                                        $ProductDetails[$id_product]['Attribute']['color'][$id] = $ProductDetails['Attribute']['color']['Values']->appendChild($Value_lang = $Document->createElement('Value'));
                                                        $Value_lang->setAttribute('Option', $colorno);

                                                        $history['Combination']['color'][$colorno] = $gcolor;
                                                    }
                                                }

                                                if(isset($ProductDetails[$id_product]['Attribute']['color'][$id]) || !empty($ProductDetails[$id_product]['Attribute']['color'][$id]))
                                                {
                                                    if(!isset($ProductDetails[$id_product]['Attribute']['Name'][$gcolor][$lno]) || empty($ProductDetails[$id_product]['Attribute']['Name'][$gcolor][$lno]))
                                                    {
                                                        $ProductDetails[$id_product]['Attribute']['Name'][$gcolor][$lno] = $ProductDetails[$id_product]['Attribute']['color'][$id]->appendChild($vname_lang = $Document->createElement('Name'));
                                                        $vname_lang->setAttribute('lang', $lno);
                                                        $ProductDetails[$id_product]['Attribute']['Name'][$gcolor][$lno]->appendChild($Document->createCDATASection($gcolor));
                                                    }
                                                } 

                                                $history['Combination'][$gcolor] = true;

                                                //Product Attribute
                                                if(isset($ProductDetails[$id_product]['Combination']['Attributes'][$id]) || !empty($ProductDetails[$id_product]['Combination']['Attributes'][$id]))
                                                {
                                                    $option1 = array_search($gcolor, $history['Combination']['color']);

                                                    if(!isset($history['Combination']['Attributes'][$id_product][$id][1][$option1])
                                                            || !$history['Combination']['Attributes'][$id_product][$id][1][$option1] 
                                                            && !empty($option1))
                                                    {
                                                        $ProductDetails[$id_product]['Combination']['Attributes'][$id]->appendChild($attr_id = $Document->createElement('Attribute'));
                                                        $attr_id->setAttribute('ID', 1);
                                                        $attr_id->setAttribute('Option', $option1);
                                                    }
                                                    $history['Combination']['Attributes'][$id_product][$id][1][$option1] = true; 
                                                }
                                            } 
                                    }

                                   $history['Combination'][$id_product] = true;
                                }
                            }
                        }
                        
                    }
                    
                    $price = $sale_price = 0;
                    $Discount = array();
                    
                    if(!isset($g->item_group_id) || empty($g->item_group_id))
                    {
                        //Topology 
                        if(isset($g->shipping_weight) && !empty($g->shipping_weight))
                        {
                            if(!isset($ProductDetails[$id_product]['Combination']['Topology']) || empty($ProductDetails[$id_product]['Combination']['Topology']))
                            {
                                $ProductDetails[$id_product]['Combination']['Topology'] = $ProductDetails[$id_product]['ID']->appendChild($Document->createElement('Topology'));
                                $ProductDetails[$id_product]['Combination']['Topology']->appendChild($weight = $Document->createElement('Unit', filter_var($g->shipping_weight, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION)));
                                $weight->setAttribute('ID', 1);
                                $weight->setAttribute('Name', "Weight");
                            }
                        }

                        //Availabilities
                        if(isset($g->quantity) && !empty($g->quantity))
                        {
                            if(!isset($ProductDetails[$id_product]['Combination']['Availabilities']) 
                                    || empty($ProductDetails[$id_product]['Combination']['Availabilities']))
                            {
                                $ProductDetails[$id_product]['Combination']['Availabilities'] = $ProductDetails[$id_product]['ID']->appendChild($Document->createElement('Availabilities'));
                                $ProductDetails[$id_product]['Combination']['Availabilities']->appendChild($Document->createElement('Stock' , $g->quantity));
                                $ProductDetails[$id_product]['Combination']['Availabilities']->appendChild($Document->createElement('Active' , 1));
                            }
                        }

                        //Product Price
                        if(!isset($ProductDetails[$id_product]['Price']) || empty($ProductDetails[$id_product]['Price']))
                        {
                            //Price, Currency
                            if(isset($g->price) && !empty($g->price))
                            {
                                $price = filter_var($g->price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION); 

                                $ProductDetails[$id_product]['Price'] = $ProductDetails[$id_product]['ID']->appendChild($Document->createElement('Price'));
                                $ProductDetails[$id_product]['Price']->appendChild( $Document->createElement( 'Standard', $price ) ); 
                                $ProductDetails[$id_product]['Price']->appendChild($p_currency = $Document->createElement('Currency'));  
                                $p_currency->setAttribute('ID', $currno);
                            }

                        }
                    } 
                    else
                    {
                        //Product Price
                        if(!isset($ProductDetails[$id_product]['Price']) || empty($ProductDetails[$id_product]['Price']))
                        {
                            $ProductDetails[$id_product]['Price'] = $ProductDetails[$id_product]['ID']->appendChild($Document->createElement('Price'));
                            $ProductDetails[$id_product]['Price']->appendChild( $Document->createElement( 'Standard', 0 ) ); 
                            $ProductDetails[$id_product]['Price']->appendChild($p_currency = $Document->createElement('Currency'));  
                            $p_currency->setAttribute('ID', $currno);
                        }
                    }
                  
                    //Sale
                    if(isset($g->sale_price) && !empty($g->sale_price))
                    {
                        $sale_price = filter_var($g->sale_price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION); 
                        $base_price = filter_var($g->price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION); 

                        if($sale_price < $base_price || $base_price != 0)
                            $Discount = round( ($base_price -$sale_price) , 2);
                        
                    
                        if($sale_price && $Discount)
                        {
                            $percent = round(($Discount/$base_price)*100);

                            if(!isset($p_sale) || empty($p_sale))
                            {
                                $p_sale = $ProductDetails[$id_product]['Price']->appendChild($Document->createElement('Sale'));
                                $p_sale->setAttribute('value', $percent);
                                $p_sale->setAttribute('type', 'percentage');
                            
                                if(isset($g->sale_price_effective_date) && !empty($g->sale_price_effective_date))
                                {
                                    $sale_date_range = explode("/", $g->sale_price_effective_date);
                                    $startDate = date('c', strtotime($sale_date_range[0])) ;  
                                    $toDate = date('c', strtotime($sale_date_range[1])) ;

                                    $p_sale->setAttribute('startDate', $startDate);
                                    $p_sale->setAttribute('endDate', $toDate);
                                }
                            }
                        }
                        
                    }
                            
                } //end foreach ($oXml->channel->item as $item
                
                //Attributes
                //color
                $ProductDetails['Attribute']['Name'] = $ProductDetails['Attribute']['color']['ID']->appendChild($name_lang = $Document->createElement('Name'));
                $ProductDetails['Attribute']['Name']->appendChild($Document->createCDATASection('color')); 
                $name_lang->setAttribute('lang', $lno);

                //size
                $ProductDetails['Attribute']['Name'] = $ProductDetails['Attribute']['size']['ID']->appendChild($name_lang = $Document->createElement('Name'));
                $ProductDetails['Attribute']['Name']->appendChild($Document->createCDATASection('size')); 
                $name_lang->setAttribute('lang', $lno);

                
            }
            else
                return sprintf('can_not_load_data') ;
        }
        else 
            return sprintf('incorrect_urls') ;
        
        if (isset($ProductDetails) && count($ProductDetails) > 0)
            return $Document->saveXML();
       
        return FALSE;
    }
    
    
    public function opencart($url,$setting_url, $token)
    {
        $return = array();
//        $count = 0;
        
        //Products
        //$xmlstr = @file_get_contents($url . '?route=feedbiz/products_create/ProductUpdateXML&metoken=' . $token);
         //$setting_url = str_replace('/products&','/settings&',$url);
         
        $xmlstr = @file_get_contents($url );
        
        if( !isset($xmlstr) || empty($xmlstr) )
            return array('message' => sprintf('can_not_load_data'), 'error' => true);
        
        $oxml = simplexml_load_string($xmlstr);
        $status = (int)$oxml->Status->Code;
        
        if( !isset($status) || $status == 0 )
            return array('message' => $oxml->Status->Message, 'error' => true);  
        
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('empty_data'), 'error' => true);       
                
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('dom_error'), 'error' => true);
        
        if(isset($oxml->Products))
            $odxmlPRD = dom_import_simplexml($oxml->Products);
        
        if(isset($oxml->Attributes))
            $odxmlATTR = dom_import_simplexml($oxml->Attributes);
        
        if(isset($oxml->Features))
            $odxmlFEAT = dom_import_simplexml($oxml->Features);

        while($status != 1)
        {
            //$_xmlstr = @file_get_contents($url . '?route=feedbiz/products_create/ProductUpdateXML&metoken=' . $token);
            $_xmlstr = @file_get_contents($url);
            if( !isset($_xmlstr) || empty($_xmlstr) ){break;}
            $_oxml = simplexml_load_string($_xmlstr);
            $_xml = simplexml_load_string($_xmlstr);
            if( !isset($_oxml) || empty($_oxml) ){break;}
            $status = (int)$_oxml->Status->Code;
            
            if(isset($_xml->Products))
                foreach($_xml->Products->Product as $product)
                {
//                    $count = $count+1;
                    $odxmlChildPRD = dom_import_simplexml($product);
                    $odxmlChildPRD = $odxmlPRD->ownerDocument->importNode($odxmlChildPRD, true);
                    $odxmlPRD->appendChild($odxmlChildPRD);
                }
            
            if(isset($_xml->Attributes) && isset($_xml->Attributes->Attribute) && isset($oxml->Attributes))
                foreach($_xml->Attributes->Attribute  as $attribute)
                {
                    $odxmlChildATTR = dom_import_simplexml($attribute);
                    $odxmlChildATTR = $odxmlATTR->ownerDocument->importNode($odxmlChildATTR, true);
                    $odxmlATTR->appendChild($odxmlChildATTR);
                }
            
            if(isset($_xml->Features) && isset($_xml->Features->Feature) && isset($oxml->Features))
                foreach($_xml->Features->Feature  as $feature)
                {
                    $odxmlChildFEAT = dom_import_simplexml($feature);
                    $odxmlChildFEAT = $odxmlFEAT->ownerDocument->importNode($odxmlChildFEAT, true);
                    $odxmlFEAT->appendChild($odxmlChildFEAT);
                }
        }
        
        //Setting
        //$xmlsetting = @file_get_contents($url . '?route=feedbiz/products_setting&metoken=' . $token);
        $xmlsetting = @file_get_contents($setting_url );
        $setting = simplexml_load_string($xmlsetting);
        if( !isset($xmlsetting) || empty($xmlsetting) ){return array('message' => sprintf('can_not_load_setting_data'), 'error' => true);}
        if( !isset($setting) || empty($setting) ){return array('message' => sprintf('setting_dom_error'), 'error' => true);}
            
            
        if(isset($setting))
        {
            $odxml = dom_import_simplexml($oxml);
            foreach($setting  as $set_key => $set)
            {
                $odxmlSetting = dom_import_simplexml($set);
                $odxmlSetting = $odxml->ownerDocument->importNode($odxmlSetting, true);
                $odxml->appendChild($odxmlSetting);
            }
        }
        
        unset($oxml->Status);
        
        $return['xml'] = $oxml->saveXML();
//        $return['count'] = $count;
        
         //echo $return['xml'];
//         echo '<pre>' . print_r($return['xml'], true) . '</pre>';
        // exit;
        
        return $return;
    }
    
    
    public function prestashop($url,$setting_url, $token)
    {
        $return = array();

        $xmlstr = @file_get_contents($url);
        

        if( !isset($xmlstr) || empty($xmlstr) )
            return array('message' => sprintf('can_not_load_data'), 'error' => true);
        
        $oxml = simplexml_load_string($xmlstr);
        $status = (int)$oxml->Status->Code;
        
        if( !isset($status) || $status == 0 )
            return array('message' => $oxml->Status->Message, 'error' => true);  
        
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('empty_data'), 'error' => true);       
                
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('dom_error'), 'error' => true);
        
        if(isset($oxml->Products))
            $odxmlPRD = dom_import_simplexml($oxml->Products);
        
        if(isset($oxml->Attributes))
            $odxmlATTR = dom_import_simplexml($oxml->Attributes);
        
        if(isset($oxml->Features))
            $odxmlFEAT = dom_import_simplexml($oxml->Features);

        while($status != 1)
        {
            $_xmlstr = @file_get_contents($url);
            if( !isset($_xmlstr) || empty($_xmlstr) ){break;}
            $_oxml = simplexml_load_string($_xmlstr);
            $_xml = simplexml_load_string($_xmlstr);
            if( !isset($_oxml) || empty($_oxml) ){break;}
            $status = (int)$_oxml->Status->Code;
            
            if(isset($_xml->Products))
                foreach($_xml->Products->Product as $product)
                {
                    $odxmlChildPRD = dom_import_simplexml($product);
                    $odxmlChildPRD = $odxmlPRD->ownerDocument->importNode($odxmlChildPRD, true);
                    $odxmlPRD->appendChild($odxmlChildPRD);
                }
            
            if(isset($_xml->Attributes) && isset($_xml->Attributes->Attribute) && isset($oxml->Attributes))
                foreach($_xml->Attributes->Attribute  as $attribute)
                {
                    $odxmlChildATTR = dom_import_simplexml($attribute);
                    $odxmlChildATTR = $odxmlATTR->ownerDocument->importNode($odxmlChildATTR, true);
                    $odxmlATTR->appendChild($odxmlChildATTR);
                }
            
            if(isset($_xml->Features) && isset($_xml->Features->Feature) && isset($oxml->Features))
                foreach($_xml->Features->Feature  as $feature)
                {
                    $odxmlChildFEAT = dom_import_simplexml($feature);
                    $odxmlChildFEAT = $odxmlFEAT->ownerDocument->importNode($odxmlChildFEAT, true);
                    $odxmlFEAT->appendChild($odxmlChildFEAT);
                }
        }
        
        //Setting
        $xmlsetting = @file_get_contents($setting_url );
        $setting = simplexml_load_string($xmlsetting);
        if( !isset($xmlsetting) || empty($xmlsetting) ){return array('message' => sprintf('can_not_load_setting_data'), 'error' => true);}
        if( !isset($setting) || empty($setting) ){return array('message' => sprintf('setting_dom_error'), 'error' => true);}
            
        if(isset($setting))
        {
            $odxml = dom_import_simplexml($oxml);
            foreach($setting  as $set_key => $set)
            {
                $odxmlSetting = dom_import_simplexml($set);
                $odxmlSetting = $odxml->ownerDocument->importNode($odxmlSetting, true);
                $odxml->appendChild($odxmlSetting);
            }
        }
        
        unset($oxml->Status);
        
        $return['xml'] = $oxml->saveXML();
        return $return;
    }
    
    public function getSingleContent($url ,$user){
        
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'timeout' => 600,  
            )
        ));
        
        $start_time = date('c');
        if(function_exists('fbiz_get_contents')){
            $xmlstr = @fbiz_get_contents($url,600); 
        }else{
            $xmlstr = @file_get_contents($url,false,$context); 
        } 
         
        //echo rawurlencode(print_r($xmlstr,true));exit;
        if( !isset($xmlstr) || empty($xmlstr) )
            return array('message' => sprintf(l('can_not_download_data')), 'error' => true);
        try{
        $oxml = new SimpleXMLElement($xmlstr);
        
        
        if(!isset($oxml->Status->Code)){
            $return['status'] = isset($oxml->Status->Code)?(int)$oxml->Status->Code:'';
            $return['total'] = isset($oxml->Status->ExportTotal)?(int)$oxml->Status->ExportTotal:'';
            $return['current'] = isset($oxml->Status->CurrentProductID)?(int)$oxml->Status->CurrentProductID:'';
            $return['min'] = isset($oxml->Status->MinProductID)?(int)$oxml->Status->MinProductID:'';
            $return['max'] = isset($oxml->Status->MaxProductID)?(int)$oxml->Status->MaxProductID:'';
            if(isset($oxml->Status->CurrentPage)){
               $return['cur_page'] = (int)$oxml->Status->CurrentPage;
            }
            $return['start_load'] = $start_time;
            $return['end_load'] = date('c');
            $return['xml'] = $xmlstr; 
            //unset($oxml->Status);

           $save_path = './users/'.$user.'/xml_log/';
           if(!file_exists($save_path)){
               mkdir($save_path);
           }
           $fp = fopen($save_path.'data_prod.xml', 'a');
           if($fp){
               fwrite($fp, print_r($return,true));
               fclose($fp); 
           }
            
            if(function_exists('fbiz_get_contents')){
                $xmlstr = @fbiz_get_contents($url,600); 
            }else{
                $xmlstr = @file_get_contents($url,false,$context); 
            } 
            $oxml = new SimpleXMLElement($xmlstr);
        }
        }catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n"; 
            $return['status'] = isset($oxml->Status->Code)?(int)$oxml->Status->Code:'';
            $return['total'] = isset($oxml->Status->ExportTotal)?(int)$oxml->Status->ExportTotal:'';
            $return['current'] = isset($oxml->Status->CurrentProductID)?(int)$oxml->Status->CurrentProductID:'';
            $return['min'] = isset($oxml->Status->MinProductID)?(int)$oxml->Status->MinProductID:'';
            $return['max'] = isset($oxml->Status->MaxProductID)?(int)$oxml->Status->MaxProductID:'';
            if(isset($oxml->Status->CurrentPage)){
               $return['cur_page'] = (int)$oxml->Status->CurrentPage;
            }
            $return['start_load'] = $start_time;
            $return['end_load'] = date('c');
            $return['xml'] = $xmlstr; 
            //unset($oxml->Status);

           $save_path = './users/'.$user.'/xml_log/';
           if(!file_exists($save_path)){
               mkdir($save_path);
           }
           $fp = fopen($save_path.'data_prod.xml', 'a');
           if($fp){
               fwrite($fp, print_r($return,true));
               fclose($fp); 
           }
        }
        $status = (int)$oxml->Status->Code;
        
        $return['status'] = isset($oxml->Status->Code)?(int)$oxml->Status->Code:'';
         $return['total'] = isset($oxml->Status->ExportTotal)?(int)$oxml->Status->ExportTotal:'';
         $return['current'] = isset($oxml->Status->CurrentProductID)?(int)$oxml->Status->CurrentProductID:'';
         $return['min'] = isset($oxml->Status->MinProductID)?(int)$oxml->Status->MinProductID:'';
         $return['max'] = isset($oxml->Status->MaxProductID)?(int)$oxml->Status->MaxProductID:'';
         if(isset($oxml->Status->CurrentPage)){
            $return['cur_page'] = (int)$oxml->Status->CurrentPage;
         }
         $return['start_load'] = $start_time;
         $return['end_load'] = date('c');
         $return['xml'] = $xmlstr; 
         //unset($oxml->Status);
          
        $save_path = './users/'.$user.'/xml_log/';
        if(!file_exists($save_path)){
            mkdir($save_path);
        }
        $fp = fopen($save_path.'data_prod.xml', 'a');
        if($fp){
            fwrite($fp, print_r($return,true));
            fclose($fp); 
        }
        
        if(  !($oxml) )
            return array('message' => sprintf('empty_data'), 'error' => true);       
                
        if( !isset($status) || $status == '03' || $status == 0 )
            return array('message' => $oxml->Status->Message, 'error' => true);  
        
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('empty_data'), 'error' => true);     
        if( !isset($oxml) || empty($oxml) )
            return array('message' => sprintf('dom_error'), 'error' => true);
        
//        if(isset($oxml->Products))
//            $odxmlPRD = dom_import_simplexml($oxml->Products);
//        
//        if(isset($oxml->Attributes))
//            $odxmlATTR = dom_import_simplexml($oxml->Attributes);
//        
//        if(isset($oxml->Features))
//            $odxmlFEAT = dom_import_simplexml($oxml->Features);
          
         
         return $return;
    }
    
    public function getSetting($url ,$user){
         
        $return = array();
        if(function_exists('fbiz_get_contents')){
            $xmlsetting = @fbiz_get_contents($url,600); 
        }else{
            $xmlsetting = @file_get_contents($url); 
        }
	if(!empty($xmlsetting)){
            $xmlsetting=trim(str_replace('CURL error:(3)<url> malformed','', $xmlsetting));
        }
        $setting = simplexml_load_string($xmlsetting);
        if( !isset($xmlsetting) || empty($xmlsetting) ){return array('message' => sprintf('can_not_load_setting_data'), 'error' => true);}
        if( !isset($setting) || empty($setting) ){return array('message' => sprintf('setting_dom_error'), 'error' => true);}
        
        
        $return['xml'] = $setting->saveXML(); 
        $save_path = './users/'.$user.'/xml_log/'; 
        if(!file_exists($save_path)){
            mkdir($save_path);
        }
        if(file_exists($save_path.'data_setting.xml'))
        unlink($save_path.'data_setting.xml');
        if(file_exists($save_path.'data_prod.xml'))
        unlink($save_path.'data_prod.xml'); 
        $fp = fopen($save_path.'data_setting.xml', 'a');
        fwrite($fp, print_r($return,true));
        fclose($fp); 
        return $return;
    }
    
    public function saveLanguage($language, $time)
    {
        if(!isset($language) || empty($language))
            return FALSE;
    
        $sql = self::insert('language', $language, $time);
        
        if(!self::add($sql))
            return FALSE;
        
        return TRUE;
        
    }
}
