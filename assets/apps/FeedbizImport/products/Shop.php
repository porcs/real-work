<?php

class Shop extends ObjectModel
{
    public function __construct( $user, $id = null, $connect = true)
    {
        
        parent::__construct( $user, $id  , $connect  );  
        $this->user = $user;
    }
    
    public static $definition = array(
        'table'     => 'shop',
        'primary' => array('id_shop'),
        'fields'    => array(
            'id_shop'       =>  array('type' => 'INTEGER', 'required' => true,  'primary_key' => true),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 64 ),
            'is_default'    =>  array('type' => 'int', 'required' => false, 'size' => 1),
            'active'        =>  array('type' => 'int', 'required' => false, 'size' => 1),
            'description'   =>  array('type' => 'text', 'required' => false),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
    public function createShopTable($user) 
    {
        $tables =  array(
            'Attribute'         => 'attribute',
            'AttributeGroup'    => 'attribute_group',
            'Carrier'           => 'carrier',
            'Currency'          => 'currency',
            'Category'          => 'category',
            'Feature'           => 'feature',
            'Language'          => 'language',
            'Manufacturer'      => 'manufacturer',
            'Tax'               => 'tax',
            'Supplier'          => 'supplier',
        );
        
        foreach ($tables as $key => $table)
        {
            $class = new $key($user);
            $query = "CREATE TABLE {$this->prefix_table}". $table ."_shop (" . $class::$definition['primary'][0] . " UNIQUE," . self::$definition['primary'][0] . " UNIQUE ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; " ;
            
            if(!$this->db_exec($query,false))
                return false;
        }
        return true;
    }

    public function importData($shopname, $time, $details = NULL)
    {        
        $id_shop = null; 
        
        if(isset($shopname) && !empty($shopname))
        {
            $shopname = self::escape_str($shopname);
            if($this->checkShopName($shopname) == 0)
            {
                $shop['name']        = $shopname;
                $shop['active']      = 1;
                $shop['is_default']  = 1;
               
                if(isset($details) && !empty($details))
                    $shop['description']    = $details;
               
                $sql = self::insert(self::$definition['table'], $shop, $time);
                 
                if(self::add($sql,false,false)){
                    $id_shop = self::db_last_insert_rowid() ; 
                }
                
                if(!$id_shop){
                    $result = self::db_query_str("SELECT id_shop FROM {$this->prefix_table}shop WHERE name = '" . $shopname . "' ;"); 
                    if($result)
                    {
                        foreach ($result as $shop)
                            $id_shop = $shop['id_shop'];
                    }
                }
            }
            else 
            {
                $result = self::db_query_str("SELECT id_shop FROM {$this->prefix_table}shop WHERE name = '" . $shopname . "' ;");
            
                if($result)
                {
                    foreach ($result as $shop)
                        $id_shop = $shop['id_shop'];
                }
            }
            
             
            if(isset($id_shop) && !empty($id_shop))
                if($this->updateShopDefault($id_shop, 'products', $time, $details))
                    if($this->updateShopDefault($id_shop, 'offers', $time, $details))
                        return $id_shop;
        }
        
        return false;
        
    }
    
    public function checkShopName($shopname)
    { 
        if(isset($shopname) && !empty($shopname))
        {
            $sql = self::db_query("SELECT id_shop FROM {$this->prefix_table}shop WHERE name = '" . $shopname . "' ;");
            
            if($sql)
                return self::db_num_rows($sql);
            else
                return false;
        }
        
        return false;
    }
    
    public function updateShopDefault($id_shop, $database, $time, $details = null)
    { 
        
        if(isset($id_shop) && !empty($id_shop))
        {
        	$db = new Db($this->user, $database);
            
            if(isset($details) && !empty($details))
                $shop_data['description']    = $details;
            
            $shop_data['is_default']  = 1;
            $shop_data['date_add']  = $time;
            $shop_where['id_shop'] = array('operation' => '=', 'value' => $id_shop);

            $shop_sql = $this->update(self::$definition['table'], $shop_data, $shop_where);
            
            if($db->db_exec($shop_sql,false,false))
            {
                $updade_data['is_default']  = 0;
                $updade_where['id_shop'] = array('operation' => '!=', 'value' => $id_shop);
                $updade_sql = $this->update(self::$definition['table'], $updade_data, $updade_where);

                if($db->db_exec($updade_sql,false,false))
                    return true;
            }
                
            $db->db_close();
        }
        
        return false;
    }
    
    public function updateShopDetail($user, $shop_name, $shop_info, $database = 'products')
    { 
        $return = true;
        if(isset($shop_name) && !empty($shop_name) && isset($user) && !empty($user))
        {
        	$db = new Db($user, $database);
            
            $shop_data['description'] = $shop_info;
            $shop_where['name'] = array('operation' => '=', 'value' => (string)$shop_name);
            $shop_sql = $this->update(self::$definition['table'], $shop_data, $shop_where);
                        
            if($db->db_exec($shop_sql,false,false)){
                 $return = false;
            }
            $db->db_close();
        }
        return $return;
    }
}