<?php

class MarketplaceProductOption extends ObjectModel
{   
    private static $table_definition = '_product_option';    
    
    public $table;
    public $marketplace; 
    public $use_site = false;
    
    public static $tables = array(
	'Main'  => 'marketplace',
    );
    
    public static $fields = array(
	
	    'keys'  => array(
		'id_product'		    =>  array('type' => 'int', 'size' => 11, 'require' => 'NOT NULL' ),
                'id_product_attribute'	    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
                'sku'			    =>  array('type' => 'varchar', 'size' => 64, 'require' => '' ),
                'id_shop'		    =>  array('type' => 'int', 'size' => 11, 'require' => 'NOT NULL' ), 
		'id_country'		    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
		'id_lang'		    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
	    ),
	
	    'main'  => array(
                'c_force'		    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
                'no_price_export'	    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'no_quantity_export'	    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'latency'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'c_disable'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'price'			    =>  array('type' => 'decimal', 'size' => '20,6', 'require' => '' ), 
		'shipping'		    =>  array('type' => 'decimal', 'size' => '20,6', 'require' => '' ),
                'shipping_type'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
		'text'			    =>  array('type' => 'varchar', 'size' => 256, 'require' => '' ),	
		'flag_update'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
	    ),	
	    
	    'amazon'  => array(
		'fba'                       =>  array('type' => 'tinyint', 'size' => 4 ),
		'fba_value'                 =>  array('type' => 'decimal', 'size' => '20,6' ),
		'asin1'                     =>  array('type' => 'varchar', 'size' => 16 ),
		'asin2'                     =>  array('type' => 'varchar', 'size' => 16 ),
		'asin3'                     =>  array('type' => 'varchar', 'size' => 16 ),
		'bullet_point1'             =>  array('type' => 'varchar', 'size' => 500 ),
		'bullet_point2'             =>  array('type' => 'varchar', 'size' => 500 ),
		'bullet_point3'             =>  array('type' => 'varchar', 'size' => 500 ),
		'bullet_point4'             =>  array('type' => 'varchar', 'size' => 500 ),
		'bullet_point5'             =>  array('type' => 'varchar', 'size' => 500 ),
		'browsenode'		    =>  array('type' => 'varchar', 'size' => 256 ),
		'gift_wrap'                 =>  array('type' => 'tinyint', 'size' => 4 ),
		'gift_message'              =>  array('type' => 'tinyint', 'size' => 4 ),
		'shipping_group'            =>  array('type' => 'varchar', 'size' => 256 ),
		'advertising'               =>  array('type' => 'tinyint', 'size' => 2 ),
		'ad_groups'                 =>  array('type' => 'text', 'input_type' => 'text' ),
	    ),
	
	    'amazon_repricing_product_strategies'  => array(
		'id_product'		    =>  '',
		'id_product_attribute'	    =>  '',
		'id_shop'		    =>  '', 
		'id_country'		    =>  '',
		'sku'			    =>  '',
		'minimum_price'		    =>  'Repricing_min',
		'target_price'		    =>  'Repricing_max',
	    )
    );    
    
    public static $repricing_tables = array(
	'amazon'  => 'amazon_repricing_product_strategies',
    );
    
    public function __construct($user, $id = null, $connect = null) {
	
        parent::__construct($user, $id, $connect);
    }
    
    public function checke_table_exit($marketplace = null) {
        
        $_db = $this->conn_id;
	
	$marketplace = strtolower(isset(self::$tables[$marketplace]) ? self::$tables[$marketplace] : $marketplace);  
        $table = $marketplace . self::$table_definition;
	
	if(!$_db->db_table_exists($table, true)){
	    return $this->create_product_option($marketplace);  
	} else {
//	    $this->alter_table($marketplace);
	}
	
	return true;
    }        
      
    private function alter_table($marketplace){	
	
	$_db = $this->conn_id;	
	$table = $marketplace . self::$table_definition;
	$exclude_field = $_db->get_all_column_name($table, true, true);
	$marketplace_field = 0;
	
	if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
	    $marketplace_field = sizeof(self::$fields[$marketplace]);
	}
	//if(sizeof($exclude_field) == (sizeof(self::$fields['keys']) + sizeof(self::$fields['main'])+ $marketplace_field + 2)) {
	if(sizeof($exclude_field) == 17 || sizeof($exclude_field) == 30) {
	    return;
	}	
        if($_db->db_exec("DROP TABLE ".$table." ; ", false)){
            return $this->create_product_option($marketplace);
        }
        return false;
    }
     
    private function create_product_option($marketplace = null) {
	
        $site_primary_key = $fields = '';
        $_db = $this->conn_id;
	$table = $marketplace . self::$table_definition;
	
	if($marketplace != 'marketplace') {
	    $site_primary_key .= "id_country";
	} else {
	    $site_primary_key .= "id_lang";
	}
	 
	foreach (self::$fields['keys'] as $main_fields => $main_values){
	    $fields .= "$main_fields  ".$main_values['type']." (".$main_values['size'].") ".$main_values['require'].", ";
	}
	 
	foreach (self::$fields['main'] as $main_fields => $main_values){
	    $fields .= "$main_fields  ".$main_values['type']." (".$main_values['size'].") ".$main_values['require'].", ";
	}
	
	if($marketplace != 'main'){
	    if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
		foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
		    $fields .= "$marketplace_fields  ".$marketplace_values['type']." (".$marketplace_values['size']."), ";
		}
	    }
	}
        $query = "CREATE TABLE ".$table." ( ".
		$fields
		."date_add datetime,
                date_upd datetime,
                PRIMARY KEY (id_product, id_product_attribute, id_shop, $site_primary_key)
              ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;";
       
        if($_db->db_exec($query,false)){
            return true;
        }
        return false;
    } 
        
    public function set_offers_options($ProductOffersOptions, $marketplace){
	
	$sql = array();
	$marketplace = strtolower($marketplace);
	
	if(isset($ProductOffersOptions->id_product) && isset($ProductOffersOptions->id_shop)) { 		
	    
	    $sql['options'] = '(';

	    foreach (self::$fields['keys'] as $main_fields => $main_values){
		$sql['options'] .= isset($ProductOffersOptions->$main_fields) ? "'".$ProductOffersOptions->$main_fields."'," : "null,";
	    }

	    foreach (self::$fields['main'] as $main_fields => $main_values){
		$main_fields = ucfirst(str_replace('c_', "", $main_fields));
		$sql['options'] .= isset($ProductOffersOptions->$main_fields) ? "'".$ProductOffersOptions->$main_fields."'," : "null,";
	    }
	    
	    if($marketplace != 'main'){
		if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
		    foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
			$marketplace_fields = ucfirst($marketplace_fields);
			$sql['options'] .= isset($ProductOffersOptions->$marketplace_fields) ? "'".$ProductOffersOptions->$marketplace_fields."'," : "null,";
		    }
		}
	    }

	    $sql['options'] .= "'".date('Y-m-d H:i:s')."')";
	}
	
	# repricing
	if(isset($ProductOffersOptions->Repricing_min) || isset($ProductOffersOptions->Repricing_max)){
	    
	    $table_repricing = strtolower(isset(self::$repricing_tables[$marketplace]) ? self::$repricing_tables[$marketplace] : $marketplace); 
	    
	    if(isset(self::$fields[$table_repricing]) && !empty(self::$fields[$table_repricing])){
		
		$sql['repricing'] = '(';
		
		foreach (self::$fields[$table_repricing] as $repricing_fields => $repricing_values){
		    $sql['repricing'] .= isset($ProductOffersOptions->$repricing_fields) ? "'".$ProductOffersOptions->$repricing_fields."'," : 
			(isset($ProductOffersOptions->$repricing_values) ? "'".$ProductOffersOptions->$repricing_values."'," : "null,");		    
		}
		
		$sql['repricing'] .= "'".date('Y-m-d H:i:s')."')";
	    }
	}
	
	return $sql;
    }
    
    public function save_repricing($ProductOptions){
	
	$sql = '';
	if(isset($ProductOptions) && !empty($ProductOptions)){
	    
	    foreach ($ProductOptions as $marketplace => $offers){
		
		$marketplace = strtolower($marketplace);
		$marketplace = isset(self::$repricing_tables[$marketplace]) ? self::$repricing_tables[$marketplace] : $marketplace;  
		    
		// check table exit
		if($this->conn_id->db_table_exists($marketplace, true)){

		    $table = $marketplace ;		    
		    $keys_stringified = array();
		    
		    if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
			foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
			    $keys_stringified[] = $marketplace_fields;
			}
		    }
		    
		    //$sql .= "DELETE FROM " . $table . "; ";
//		    $sql .= 'REPLACE INTO '.$table.' ('.implode(', ', $keys_stringified).', date_upd) VALUES '. trim($offers, ", ") . "; ";
                    if(is_array($offers)){
                        $sep_no_row = 500;

                        if(sizeof($offers)>$sep_no_row){
                            $set_list = array();
                            foreach($offers as $i => $v){
                                $set_no = floor($i/$sep_no_row);
                                $set_list[$set_no][] = $v;
                                unset($offers[$i]);
                            }
                            foreach($set_list as $v){
                                $sql .= 'REPLACE INTO '. $table.' ('.implode(', ', $keys_stringified).', date_upd) VALUES '.implode(', ', $v) . "; \n";
                            }
                        }else{
                        $sql .= 'REPLACE INTO '. $table.' ('.implode(', ', $keys_stringified).', date_upd) VALUES '.implode(', ', $offers) . "; \n";
                        }
                    }else if(!empty($offers) && is_string($offers)){
                        $sql .= 'REPLACE INTO '.$table.' ('.implode(', ', $keys_stringified).', date_upd) VALUES '. trim($offers, ", ") . "; ";
                    }
		    unset($keys_stringified);
		}
	    }
	}
		
	return $sql;
    }  
    
    public function save_offers_options($ProductOptions){
	
	$sql = '';
	if(isset($ProductOptions) && !empty($ProductOptions)){
	    
	    foreach ($ProductOptions as $marketplace => $offers){
		    
		    $marketplace = strtolower(isset(self::$tables[$marketplace]) ? self::$tables[$marketplace] : $marketplace);   		    
		    $table = $marketplace . self::$table_definition;		    
		    $keys_stringified = array();
		    
		    foreach (self::$fields['keys'] as $main_fields => $main_values){
			$keys_stringified[] = $main_fields;
		    }

		    foreach (self::$fields['main'] as $main_fields => $main_values){
			$keys_stringified[] = $main_fields;
		    }
		    
		    if($marketplace != 'main'){
			if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
			    foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
				$keys_stringified[] = $marketplace_fields;
			    }
			}
		    }
		    
		    //$sql .= "DELETE FROM " . $table . "; ";
//		    $sql .= 'REPLACE INTO '.$table.' ('.implode(', ', $keys_stringified).', date_add) VALUES '. trim($offers, ", ") . "; ";

                    if(is_array($offers)){
                        $sep_no_row = 500;

                        if(sizeof($offers)>$sep_no_row){
                            $set_list = array();
                            foreach($offers as $i => $v){
                                $set_no = floor($i/$sep_no_row);
                                $set_list[$set_no][] = $v;
                                unset($offers[$i]);
                            }
                            foreach($set_list as $v){
                                $sql .= 'REPLACE INTO '. $table.' ('.implode(', ', $keys_stringified).', date_add) VALUES '.implode(', ', $v) . "; \n";
                            }
                        }else{
                        $sql .= 'REPLACE INTO '. $table.' ('.implode(', ', $keys_stringified).', date_add) VALUES '.implode(', ', $offers) . "; \n";
                        }
                    }else if(!empty($offers) && is_string($offers)){
                        $sql .= 'REPLACE INTO '.$table.' ('.implode(', ', $keys_stringified).', date_add) VALUES '. trim($offers, ", ") . "; ";
                    }
                    
		    unset($keys_stringified);
	    }
	}
		
	return $sql;
    }  
    
}