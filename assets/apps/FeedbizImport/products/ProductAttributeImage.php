<?php

class ProductAttributeImage extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_attribute_image',
        'unique'        => array('product_attribute_image' => array('id_product_attribute', 'id_image', 'id_shop')),
        'fields'    => array(
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_image'              =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
}