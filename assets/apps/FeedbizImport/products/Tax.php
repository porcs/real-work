<?php

class Tax extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'tax',
        'primary'   => array('id_tax', 'id_shop'),
        'unique'    => array('tax' => array('id_tax', 'id_shop')),
        'fields'    => array(
            'id_tax'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'rate'          =>  array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000' ),
            'type'          =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default_null' => true ),
            'id_currency'   =>  array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true ),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
        'lang' => array(
            'fields'    => array(
                'id_tax'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_shop'   =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
                'id_lang'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'      =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
                'date_add'  =>  array('type' => 'datetime', 'required' => true ),
            ),
        ),
        
    );
    
    /**
    * $return Query (string)
    */
    public function importData($data, $time)
    {   
        //Tax
        if(isset($data) && !empty($data))
        {
            foreach($data as $TaxsXml)
            {
                $sql = '';
                $tax = array();
                $id_shop = $TaxsXml->id_shop;

                foreach ($TaxsXml->Tax as $Taxs)
                {
                    $tax['id_tax'] = (int)ObjectModel::escape_str($Taxs['id']);
                    $tax['id_shop'] = (int)$id_shop;

                    //Tax Rate
                    foreach($Taxs->Rate as $taxRate)
                    {
                        $tax['type'] = ObjectModel::escape_str($taxRate['type']);
                        $tax['rate'] = (float)ObjectModel::escape_str($taxRate);

                        $sql .= self::insert('tax', $tax, $time);
                    }

                    //Language
                    $tax_lang = array();
                    foreach ($Taxs->Name as $taxName)
                    {
                        $tax_lang['id_tax'] = $tax['id_tax'];
                        $tax_lang['id_lang'] =(int) ObjectModel::escape_str($taxName['lang']);
                        $tax_lang['name'] = ObjectModel::escape_str($taxName);
                        $tax_lang['id_shop'] = (int)$id_shop;

                        $sql .= self::insert('tax_lang', $tax_lang, $time);
                    }

                }
            }
         
            return $sql;
        }
        
        return '';
    }

    
    
}