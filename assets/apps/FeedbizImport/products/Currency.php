<?php

class Currency extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'currency',
        'primary'   => array('id_currency', 'id_shop'),
        'unique'    => array('currency' => array('id_currency', 'id_shop')),
        'fields'    => array(
            'id_currency'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'iso_code'      =>  array('type' => 'varchar', 'required' => true, 'size' => 3 ),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 16 ),
            'is_default'    =>  array('type' => 'tinyint', 'required' => true, 'size' => 1 ),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
     
    /*
    * $return Query (string)
    */
    public function importData($data, $time)
    {
        //Currency
        foreach($data as $CurrenciesXml)
        {
            $sql = $id_currency_default = '';
            $currency = array();
            $id_shop = $CurrenciesXml->id_shop;

            foreach ($CurrenciesXml->Currency as $Currencies)
            {                
                $currency['id_currency'] = (int)ObjectModel::escape_str($Currencies['ID']);
                $currency['id_shop'] = (int)$id_shop;
                $currency['iso_code'] = ObjectModel::escape_str($Currencies['iso_code']);
                $currency['name'] = ObjectModel::escape_str($Currencies);
                $currency['is_default'] = ((int)ObjectModel::escape_str($Currencies['is_default']))*1;
                
                $sql .= self::insert('currency', $currency, $time);                
               
            }
        }           
        
        return $sql;
    }
    
    public function setDefaultCurrency($id_currency){
        require_once  (dirname(__FILE__).'/../../../../application/libraries/UserInfo/configuration.php');
        $user_info = new UserConfiguration();
        $user_info->setUserDefaultCurrency(ObjectModel::$user, $id_currency);
    }
    
}