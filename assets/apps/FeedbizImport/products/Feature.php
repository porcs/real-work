<?php

class Feature extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'feature',
        'primary'   => array('id_feature', 'id_shop', 'id_lang'),
        'unique'    => array('feature' => array('id_feature', 'id_shop', 'id_lang')), 
        'fields'    => array(
            'id_feature'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_lang'       =>  array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 128),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
        'table_link' => array( 
            '0' => array(
                'name' => 'feature_value', 
                'lang' => false 
            ),
        ),
    );

    /**
    * $return Query (string)
    */
    public function importData($data, $time)
    {
        $sql_out=array();
        //Feature
        if(isset($data) && !empty($data)) 
        {
//             define('KEEP_SQL_DIRECT',true);
        	$sql = '';
                $count = 0;
                $feature_sql_set=array();
                $feature_value_set = array();
            foreach($data as $FeaturesXml)
            {
                $feature = array();
                $id_shop = $FeaturesXml->id_shop;
                
                foreach ($FeaturesXml->Feature as $Features)
                {
                    
                    //feature name
                    foreach ($Features->Name as $FeatureName)
                    {
	                    $feature['id_feature'] = (int)ObjectModel::escape_str($Features['ID']);
	                    $feature['id_lang'] = (int)ObjectModel::escape_str($FeatureName['lang']);
	                    $feature['id_shop'] = (int)$id_shop;
                    	$feature ['name'] = ObjectModel::escape_str($FeatureName);
//                    	$sql .= self::insert('feature', $feature, $time);
                        $feature_sql_set[] = $feature;
                        
                    }
                    //find default value
//                    $sql_out[]=$sql;
                    $sql = '';
                    $count = 0;
                    
                    //
                    //feature values
                    
                    $feature_value = array();
                    foreach ($Features->Values->Value as $FeaturesValues)
                    {
                        $feature_value['id_feature'] = $feature['id_feature'];
                        $feature_value['id_feature_value'] = (int)ObjectModel::escape_str($FeaturesValues['Option']);
                        $feature_value['id_shop'] = (int)$id_shop;
                        
                        $default_value = '';
                        foreach ($FeaturesValues->Name as $FeaturesValuesName)
                        {
                            $feature_value['id_lang'] = (int)$FeaturesValuesName['lang'];
                            $feature_value['value'] = ObjectModel::escape_str($FeaturesValuesName);
                            if($feature_value['value'] == NULL || empty($feature_value['value'])|| ($feature_value['value'])=='' ){ 
                                if($default_value==''){
                                    foreach ($FeaturesValues->Name as $FeaturesValuesNameDEF)
                                    { 
                                        $tmp = ObjectModel::escape_str($FeaturesValuesNameDEF);
                                        if($default_value=='' && !($tmp == NULL || empty($tmp) || trim($tmp) =='' )){
                                                $default_value = $tmp;
                                        }
                                    } 
                                }
                                if($default_value=='')continue;
                                $feature_value['value'] = $default_value;
                            }
                            $feature_value_set[]=$feature_value;
//                            $sql .= self::insert('feature_value', $feature_value, $time);
//                            $count++;
//                            if($count==200){
//                                $sql_out[]=$sql;
//                                $sql = '';
//                                $count = 0;
//                            }
                        }
                    }
                    
                    $sql_out[]=$sql;
                }
            }
           
            $sql_out[] = self::insert('feature', $feature_sql_set, $time);
            $sql_out[] = self::insert('feature_value', $feature_value_set, $time);
            return $sql_out;
        }
        
        return '';
    }
    
}