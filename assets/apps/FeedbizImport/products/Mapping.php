<?php
class Mapping extends ObjectModel {
	public $attributes = array (
			'table' => 'mapping_attribute',
			// 'unique' => array('mapping_attribute' => array('attribute', 'id_shop')),
			'fields' => array (
					'attribute' => array (
							'type' => 'text',
							'required' => true 
					),
					'mapping' => array (
							'type' => 'text',
							'required' => false,
							'default_null' => true 
					),
					'id_shop' => array (
							'type' => 'int',
							'required' => true,
							'size' => 10 
					),
					'id_mode' => array (
							'type' => 'int',
							'required' => true,
							'size' => 10 
					),
					'session_key' => array (
							'type' => 'datetime',
							'required' => false 
					) 
			) 
	);
	public $characteristics = array (
			'table' => 'mapping_characteristic',
			// 'unique' => array('mapping_characteristic' => array('character', 'id_shop')),
			'fields' => array (
					'c_character' => array (
							'type' => 'int',
							'required' => true,
							'size' => 10 
					),
					'mapping' => array (
							'type' => 'text',
							'required' => false,
							'default_null' => true 
					),
					'id_shop' => array (
							'type' => 'int',
							'required' => true,
							'size' => 10 
					),
					'id_mode' => array (
							'type' => 'int',
							'required' => true,
							'size' => 10 
					),
					'session_key' => array (
							'type' => 'datetime',
							'required' => false 
					) 
			) 
	);
	public $manufacturers = array (
			'table' => 'mapping_manufacturer',
			// 'unique' => array('mapping_manufacturer' => array('manufacturer', 'id_shop')),
			'fields' => array (
					'manufacturer' => array (
							'type' => 'varchar',
							'required' => true,
							'size' => 255 
					),
					'mapping' => array (
							'type' => 'text',
							'required' => false,
							'default_null' => true 
					),
					'id_shop' => array (
							'type' => 'int',
							'required' => true,
							'size' => 10 
					),
					'id_mode' => array (
							'type' => 'int',
							'required' => true,
							'size' => 10 
					),
					'session_key' => array (
							'type' => 'datetime' 
					) 
			) 
	);
	public $conditions = array (
			'table' => 'mapping_condition',
			'unique' => array (
					'id_shop',
					'id_marketplace',
					'name' 
			),
			'fields' => array (
					'id_shop' => array (
							'type' => 'int',
							'required' => true,
							'size' => 10 
					),
					'id_marketplace' => array (
							'type' => 'int',
							'required' => true,
							'size' => 10 
					),
					'name' => array (
							'type' => 'varchar',
                                                        'size' => 255,
							'required' => false,
							'default_null' => true 
					),
					'condition_value' => array (
							'type' => 'text',
							'required' => false,
							'default_null' => true 
					) 
			) 
	);
	
	// CREATE TABLE mapping_condition(id_shop int(11),name text, id_marketplace int(11), value text, PRIMARY KEY (name,id_shop, id_marketplace) )
	public function createMappingTable() {
		$arr_data = array (
				$this->attributes,
				$this->characteristics,
				$this->manufacturers,
				$this->conditions 
		);
		
		
		foreach ( $arr_data as $data ) {
			$sql = array ();
			$tablename = $data ['table'];
			
			$data ['fields'] = $data ['fields'];
			$query = '';
		
			foreach ( $data ['fields'] as $key_field => $field ) {
				$default = '';
				$size = '';
				
				if (! isset ( $field ['required'] ))
					$field ['required'] = false;
				
				$required = ($field ['required']) ? ' NOT NULL ' : '';
				$default_null = (isset ( $field ['default_null'] ) && ! $field ['required']) ? ' DEFAULT NULL' : '';
				
				if (! isset ( $field ['default_null'] ))
					$default = isset ( $field ['default'] ) ? " DEFAULT '" . $field ['default'] . "'" : '';
				
				if (isset ( $field ['size'] ))
					$size = '(' . $field ['size'] . ') ';
				$unique = ' ';
//                                if(!empty($data['unique']) && in_array($key_field,$data['unique'])){
//                                    $unique = ' UNIQUE ';
//                                }
				$sql [] = $key_field . ' ' . $field ['type'] . $size . $required . $default_null .$unique. $default;
			}
			
			$sqls = implode ( ', ', $sql );
			$query .= "CREATE TABLE " . $this->prefix_table.$tablename . "(" . $sqls;
//			
			// Create Unique
			if (isset ( $data ['unique'] ) && ! empty ( $data ['unique'] )) {
//				foreach ( $data ['unique'] as $unique_key => $unique_value ) {
//					if (is_array ( $unique_value )) {
						$query .= ', PRIMARY KEY(' . implode ( ', ', $data ['unique'] ) . ')';
//					} else if (is_string ( $unique_value )) {
//						$query .= ', CONSTRAINT "' . $unique_key . '" UNIQUE (' . $unique_value . ')';
//					}
//				}
			}
			
			$query .= ") ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
                        if (! $this->db_exec ( $query,false ))
			return false;
		}
		
		
		
		return true;
	}
	public function updateTable($id_shop) {
		$query = '';
		$query .= "DELETE FROM {$this->prefix_table}mapping_attribute WHERE attribute NOT IN (SELECT DISTINCT name FROM {$this->prefix_table}attribute_lang WHERE id_shop = '" . $id_shop . "') AND id_shop = " . $id_shop . "  ; ";
		$query .= "DELETE FROM {$this->prefix_table}mapping_manufacturer WHERE manufacturer NOT IN (SELECT DISTINCT name FROM {$this->prefix_table}manufacturer WHERE id_shop = '" . $id_shop . "') AND id_shop = " . $id_shop . "  ; ";
		$query .= "DELETE FROM {$this->prefix_table}mapping_condition WHERE name NOT IN (SELECT DISTINCT name FROM {$this->prefix_table}conditions WHERE id_shop = '" . $id_shop . "') AND id_shop = " . $id_shop . "  ; ";
		
		$result = $this->db_exec ( $query,true,true );
		
		if (isset ( $result ) && ! empty ( $result ))
			return TRUE;
		
		return FALSE;
	}
}