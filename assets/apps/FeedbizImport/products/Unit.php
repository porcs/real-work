<?php

class Unit extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'unit',
        'primary'   => array('id_unit', 'id_shop'),
        'unique'    => array('unit' => array('id_unit', 'id_shop')),
        'fields'    => array(
            'id_unit'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'   =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'type'      =>  array('type' => 'varchar', 'required' => true, 'size' => 32 ),
            'name'      =>  array('type' => 'varchar', 'required' => true, 'size' => 16 ),
            'date_add'  =>  array('type' => 'datetime', 'required' => true ),
        ),
    );

    /**
    * $return Query (string)
    */
    public function importData($data, $time)
    {
        //Unit
        foreach($data as $UnitsXml)
        {
            $sql = '';
            $unit = array();
            $id_shop = $UnitsXml->id_shop;
            
            foreach ($UnitsXml->Unit as $Units)
            {
                $unit['id_unit'] = (int)ObjectModel::escape_str($Units['ID']);
                $unit['type'] = ObjectModel::escape_str($Units['Type']);
                $unit['name'] = ObjectModel::escape_str($Units);
                $unit['id_shop'] = (int)$id_shop;
                
                $sql .= self::insert('unit', $unit, $time);
            }
        }

        return $sql;
    }

    
    
}