<?php
// 2015-12-03
class ProductAttributeUrl extends ObjectModel
{  
    public static $definition = array(
        'table'     => 'product_attribute_url',
	'primary'       => array('id_product','id_product_attribute','id_shop'),
        'fields'    => array(
            'id_product'            =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
	    'id_lang'               =>  array('type' => 'int', 'required' => false, 'size' => 10),
	    'link'                  =>  array('type' => 'text', 'required' => true),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ), 
    );  
    
}