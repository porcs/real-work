<?php

class Tag extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'tag',
        'primary'   => array('id_tag', 'id_shop'),
        'unique'    => array('tag' => array('id_tag', 'id_shop')),
        'fields'    => array(
            'id_tag'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'   =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_lang'   =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'name'      =>  array('type' => 'varchar', 'required' => true, 'size' => 32),
            'date_add'  =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
}