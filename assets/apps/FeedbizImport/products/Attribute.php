<?php

class Attribute extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'attribute',
        'primary'   => array('id_attribute_group', 'id_attribute', 'id_shop'),
        'unique'    => array('attribute' => array('id_attribute_group', 'id_attribute', 'id_shop')),
        'fields'    => array(
            'id_attribute_group'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_attribute'          =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'color'                 =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default_null' => true ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
        'lang' => array(
            'fields'    => array(
                'id_attribute_group'=>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_attribute'      =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
                'id_lang'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 128),
                'date_add'          =>  array('type' => 'datetime', 'required' => true ),
            ),
        ),
        'table_link' => array( 
            '0' => array(
                'name' => 'attribute_group', 
                'lang' => true 
            ),
            
        ),
    );

    /**
     * $return Query (string)
     */
    public function importData($data, $time)
    {
        //Attributes
        foreach($data as $AttributesXml)
        {
            $id_shop = $AttributesXml->id_shop;
           
            $sql = '';
            $attribute_group = array();
            foreach ($AttributesXml->Attribute as $Attributes)
            {
                //
                $attribute_group['id_attribute_group'] = (int)ObjectModel::escape_str($Attributes['ID']);
                $attribute_group['id_shop'] = $id_shop;
                $attribute_group['is_color_group'] = ($Attributes['type'] == 'color') ? 1 : 0;
                $attribute_group['group_type'] = ObjectModel::escape_str($Attributes['type']);
                
                $sql .= self::insert('attribute_group', $attribute_group, $time);
                
                //attribute_group_lang
                $attribute_group_lang = array();
                foreach ($Attributes->Name as $AttributesName)
                {
                    $attribute_group_lang['id_attribute_group'] = $attribute_group['id_attribute_group'];
                    $attribute_group_lang['id_shop'] = $id_shop;
                    $attribute_group_lang['id_lang'] = (int)$AttributesName['lang'];
                    $attribute_group_lang['name']  = ObjectModel::escape_str($AttributesName);
                    
                    $sql .= self::insert('attribute_group_lang', $attribute_group_lang, $time);
                }

                //attribute
                $attribute = array();
                foreach ($Attributes->Values as $AttributesValues)
                {
                    foreach ($AttributesValues as $AttributesValue)
                    {
                        
                        $attribute['id_attribute_group'] = $attribute_group['id_attribute_group'];
                        $attribute['id_attribute'] = (int)ObjectModel::escape_str($AttributesValue['Option']) ;
                        $attribute['id_shop'] = (int)$id_shop ;
                        
                        if(isset($AttributesValue['code']))
                            $attribute['color'] = ObjectModel::escape_str($AttributesValue['code']);
                        
                        $sql .= self::insert('attribute', $attribute, $time);
                        
                        //attribute_lang
                        $attribute_lang = array();
                        $last_name = '';
                        foreach ($AttributesValue->Name as $AttributesValueName)
                        {
                            $attribute_lang['id_attribute_group'] = $attribute_group['id_attribute_group'];
                            $attribute_lang['id_attribute'] = $attribute['id_attribute'];
                            $attribute_lang['id_shop'] = $id_shop;
                            $attribute_lang['id_lang'] = (int)$AttributesValueName['lang'];
                            $name = ObjectModel::escape_str($AttributesValueName);
                            if(empty($name)){
                                $name = $last_name;
                            }
                            $attribute_lang['name'] = $name;
                            if(!empty($attribute_lang['name'])){$last_name=$attribute_lang['name'];}
                            $sql .= self::insert('attribute_lang', $attribute_lang, $time);
                        }
                    }
                }
               
            }//AttributesXml
            
        }//Attributes
//        echo '<pre>' . print_r($sql, true) . '</pre>';
//          exit;
        return $sql;
    }
}