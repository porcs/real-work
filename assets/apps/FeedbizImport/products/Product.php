<?php

class Product extends ObjectModel {

    public function __construct($user, $id = null, $connect = true) {
        parent::__construct($user, $id, $connect);
        $this->user = $user;
        $this->id = $id;
        $this->connect = $connect;
    }

    public static $definition = array(
        'table' => 'product',
        'primary' => array('id_product', 'id_shop'),
        'unique' => array('product' => array('id_product', 'id_shop')),
        'fields' => array(
            'id_product' => array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop' => array('type' => 'int', 'required' => true, 'size' => 10),
            'id_supplier' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'id_manufacturer' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'id_category_default' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'on_sale' => array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0'),
            'reference' => array('type' => 'varchar', 'required' => false, 'size' => 64),
            'old_reference' => array('type' => 'varchar', 'required' => false, 'size' => 64),
            'flag_update_ref' => array('type' => 'datetime', 'required' => false),
            'sku' => array('type' => 'varchar', 'required' => false, 'size' => 64),
            'ean13' => array('type' => 'varchar', 'required' => false, 'size' => 13),
            'upc' => array('type' => 'varchar', 'required' => false, 'size' => 12),
            'ecotax' => array('type' => 'decimal', 'required' => false, 'size' => '17,6', 'default' => '0.000000'),
            'quantity' => array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0'),
            'price' => array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000'),
            'wholesale_price' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'id_currency' => array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0'),
            'width' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'height' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'depth' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'weight' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'active' => array('type' => 'tinyint', 'required' => true, 'size' => 1, 'default' => '0'),
            'available_date' => array('type' => 'datetime', 'required' => false),
            'id_condition' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'id_tax' => array('type' => 'int', 'required' => false, 'size' => 10),
            'has_attribute' => array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0'),
            'date_add' => array('type' => 'datetime', 'required' => true),
            'date_upd' => array('type' => 'datetime', 'required' => false),
            'date_created' => array('type' => 'datetime', 'required' => false),
        ),
        'lang' => array(
            'fields' => array(
                'id_product' => array('type' => 'int', 'required' => true, 'size' => 10),
                'id_shop' => array('type' => 'int', 'required' => true, 'size' => 10),
                'id_lang' => array('type' => 'int', 'required' => true, 'size' => 10),
                'name' => array('type' => 'varchar', 'required' => true, 'size' => 128),
                'description' => array('type' => 'text', 'required' => false),
                'description_short' => array('type' => 'text', 'required' => false),
                'date_add' => array('type' => 'datetime', 'required' => true),
            ),
        ),
        'table_link' => array(
            '0' => array('name' => 'product_attribute', 'lang' => false),
            '1' => array('name' => 'product_attribute_combination', 'lang' => false),
            '2' => array('name' => 'product_attribute_image', 'lang' => false),
            '3' => array('name' => 'product_attribute_unit', 'lang' => false),
            '4' => array('name' => 'product_carrier', 'lang' => false),
            '5' => array('name' => 'product_category', 'lang' => false),
            '6' => array('name' => 'product_feature', 'lang' => false),
            '7' => array('name' => 'product_image', 'lang' => false),
            '8' => array('name' => 'product_sale', 'lang' => false),
            '9' => array('name' => 'product_supplier', 'lang' => false),
            '10' => array('name' => 'product_tag', 'lang' => true),
        ),
    );
    
    public function importData($data, $time) {
        
        $return = '';
       
        $log = new ImportLog($this->user);
	$ProductOption = new MarketplaceProductOption($this->user, $this->id, $this->connect);
        
        global $no_total, $no_success, $no_error, $no_warning, $batchID, $source;

        $shopname = $sql_log = '';
        $warning = $id_tag = 1;
        $error = 2;
        $sql_log_error = $sql_log_warning = $error_counter = $history = $ProductOptions = array();
        	
	// id_country
	$countries = ObjectModel::get_countries();
	
        // Products
        foreach ($data as $ProductsXml) {
            
            $id_shop = $ProductsXml->id_shop;
            $shopname = $ProductsXml->shopname;
            $source = $ProductsXml->source;

            foreach ($ProductsXml->Product as $Products) {
                $sql = '';
                $product = array();
                $product['on_sale'] = $product['active'] = $id_product = 0;
                $no_total++;

                //Product Name
                foreach ($Products->ProductData as $productsData) {
                    foreach ($productsData->Names as $productsDataName) {
                        foreach ($productsDataName as $productName) {
                            $product_name = ObjectModel::escape_str($productName);
                        }
                    }
                }

                //Product Identifier
                foreach ($Products->Identifier as $productsIdentifier) {
                    foreach ($productsIdentifier->Reference as $productsIdentifierReference) {
                        $product['id_shop'] = (int) $id_shop;

                        if ($productsIdentifierReference['type'] == "ID") {
                            $product['id_product'] = (int) ObjectModel::escape_str($productsIdentifierReference);
                            $id_product = $product['id_product'];
                            
                            if (!isset($id_product) || empty($id_product)) { 
                                
                                if(!isset($sql_log_error['product'][$error][1][$id_product][0])){
                                	$error_counter[$id_product] = 'error';
                                    $err = array(
                                        'product_type' => 'product',
                                        'severity' => $error,
                                        'error_code' => 1,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => 0,
                                        'message' => $product_name,
                                    );
                                    
                                    $sql_log .= $log->message_log($err, $batchID, $id_shop, $time);
                                    
                                    $sql_log_error['product'][$error][1][$id_product][0] = true ;
				    $error_counter[$id_product] = 'error';
                                }                                
                            }
                        }

                        if ($productsIdentifierReference['type'] == "SKU") {
                            
                            $product['sku'] = ObjectModel::escape_str($productsIdentifierReference);

                            if (!isset($product['sku']) || empty($product['sku'])){
                               
                                if(!isset($sql_log_error['offer'][$error][2][$id_product][0])){
				    $error_counter[$id_product] = 'error';
                                    
                                    $err = array(
                                        'product_type' => 'offer',
                                        'severity' => $error,
                                        'error_code' => 11,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => 0,
                                    );
                                    
                                    $sql_log .= $log->message_log($err, $batchID, $id_shop, $time);
                                    
                                    $sql_log_error['offer'][$error][2][$id_product][0] = true;
				    $error_counter[$id_product] = 'error';
                                }     
                            } else if (! $log->ValidateSKU($product['sku'])) {
                                if(!isset($sql_log_error['offer'][$error][3][$id_product][0])){
                                    
                                    $err = array(
                                        'product_type' => 'offer',
                                        'severity' => $error,
                                        'error_code' => 3,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => 0,
                                        'message'=> $product['sku'],
                                    );
                                    
                                    $sql_log .=  $log->message_log($err, $batchID, $id_shop, $time);
                                    
                                    $sql_log_error['offer'][$error][3][$id_product][0] = true;
				    $error_counter[$id_product] = 'error';
                                }     
                            }
                        }

                        if ($productsIdentifierReference['type'] == "Reference") {
                            $product['reference'] = ObjectModel::escape_str($productsIdentifierReference);
                        }
                    }

                    foreach ($productsIdentifier->Code as $productsIdentifierCode) {
                        if ($productsIdentifierCode['type'] == "EAN") {
                            $product['ean13'] = ObjectModel::escape_str($productsIdentifierCode);
                            if (!$log->EAN_UPC_Check($product['ean13'])){
                                if(!isset($sql_log_error['offer'][$warning][5][$id_product][0])){

                                    $err = array(
                                        'product_type' => 'offer',
                                        'severity' => $warning,
                                        'error_code' => 14,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => 0,
                                        'message'=> ObjectModel::escape_str($productsIdentifierCode)
                                    );

                                    $sql_log .=  $log->message_log($err,  $batchID, $id_shop, $time);

                                    $sql_log_error['offer'][$warning][5][$id_product][0] = true;
                                    $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                                } 
                            } 
                        }

                        if ($productsIdentifierCode['type'] == "UPC") {
                            $product['upc'] = ObjectModel::escape_str($productsIdentifierCode);
                            if (!$log->EAN_UPC_Check($product['upc'])){
                                if(!isset($sql_log_error['offer'][$warning][5][$id_product][0])){

                                    $err = array(
                                        'product_type' => 'offer',
                                        'severity' => $warning,
                                        'error_code' => 5,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => 0,
                                        'message'=> ObjectModel::escape_str($productsIdentifierCode)
                                    );

                                    $sql_log .=  $log->message_log($err,  $batchID, $id_shop, $time);

                                    $sql_log_error['offer'][$warning][5][$id_product][0] = true;
                                    $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                                } 
                            } 
                        }             
                    } 
		    
		    // CreateDate //date_created
		    if(isset($productsIdentifier->CreateDate)) {
			foreach ($productsIdentifier->CreateDate as $CreateDate) {            

			    $product['date_created'] = date('Y-m-d H:i:s', strtotime((string)$CreateDate));   

			}
		    }
                }

                if (!isset($product['reference']) || empty($product['reference'])) {             
                    if(!isset($sql_log_error['product'][$warning][2][$id_product][0])){                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $warning,
                            'error_code' => 2,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );
                        
                        $sql_log .=  $log->message_log($err,  $batchID, $id_shop, $time);                        
                        $sql_log_error['offer'][$warning][2][$id_product][0] = true;                        
                    }  
                }              
		
		#OffersOptions 
		if(isset($Products->OffersOptions) && (!isset($Products->Combinations) || ($Products->Combinations->count() == 0))){
		    foreach ($Products->OffersOptions as $OffersOptions){
			foreach($OffersOptions as $OptionKeys => $OffersOption){
			    
			    if(!empty($OptionKeys)){

				// check table exits
				if(!isset($history[$OptionKeys])){
				    $history[$OptionKeys] = $ProductOption->checke_table_exit($OptionKeys);
				}

				if(isset($history[$OptionKeys]) && $history[$OptionKeys]){
				    foreach ($OffersOption as $OfferOption){

					if($OfferOption->count() > 0) {
					    if(isset($OfferOption['region'])){
						$Offer_key = ObjectModel::escape_str($OfferOption['region']);
						$id_country= (int) $countries[ObjectModel::escape_str($OfferOption['region'])]['id'];
					    }

					    if(isset($OfferOption['lang'])){
						$Offer_key = ObjectModel::escape_str($OfferOption['lang']);
						$id_lang= (int) ObjectModel::escape_str($OfferOption['lang']);
					    }

					    $OfferOption->id_product = $product['id_product'];
					    $OfferOption->id_product_attribute = 0;
					    $OfferOption->sku = isset($product['reference']) ? $product['reference'] : null;
					    $OfferOption->id_shop = (int) $id_shop;
					    $OfferOption->id_country = isset($id_country) ? $id_country : null;
					    $OfferOption->id_lang =  isset($id_lang) ? $id_lang : null;
					    
					    $offers = $ProductOption->set_offers_options($OfferOption, $OptionKeys);
					    
					    if(isset($offers['options']) && !empty($offers['options'])){
						if(!isset($ProductOptions['options'][$OptionKeys])){
						     $ProductOptions['options'][$OptionKeys] = array();
						}
						$ProductOptions['options'][$OptionKeys][]=   $offers['options'];
					    }
					    
					    if(isset($offers['repricing']) && !empty($offers['repricing'])){
						if(!isset($ProductOptions['repricing'][$OptionKeys])){
						    $ProductOptions['repricing'][$OptionKeys] = array();
						}
						$ProductOptions['repricing'][$OptionKeys][] =  $offers['repricing'];
					    }
					    
					    unset($offers);
					    unset($OfferOption);
					}
				    }
				} 
			    }					
			}
		    }
		}		
		#OffersOptions 
		
                //Product Price
                foreach ($Products->Price as $productsPrices) {
                    
                    $product['price'] = (float) ObjectModel::escape_str($productsPrices->Standard);
                    $product['id_tax'] = (int) ObjectModel::escape_str($productsPrices->Tax['ID']);
                    $product['id_currency'] = (int) ObjectModel::escape_str($productsPrices->Currency['ID']);
                    
                    /* wholesale_price ADD : 16/07/2015 */
                    if(isset($productsPrices->WholeSale)){
                        $product['wholesale_price'] = (float) ObjectModel::escape_str($productsPrices->WholeSale);
                    }
                    if(!isset($product_sale)){
                        $product_sale = array();$k=0;
                    }else{
                        $k = sizeof($product_sale);
                    }
                    //Sale 
                    if (isset($productsPrices->Sales)) {
                        foreach ($productsPrices->Sales  as $productsPricesSales) {
                            if (isset($productsPricesSales->Sale)) {
                                foreach ($productsPricesSales->Sale as  $productsPricesSale) {
                                    $from = '0000-00-00 00:00:00';
                                    if(strtotime(ObjectModel::escape_str($productsPricesSale['startDate'])) !== false){
                                        $from = ObjectModel::escape_str($productsPricesSale['startDate']);
                                    }
                                    $to = '0000-00-00 00:00:00';
                                    if(strtotime(ObjectModel::escape_str($productsPricesSale['endDate'])) !== false){
                                        $to = ObjectModel::escape_str($productsPricesSale['endDate']);
                                    }
                                    $product_sale[$k]['id_product'] = $product['id_product'];
                                    $product_sale[$k]['date_from'] = $from;
                                    $product_sale[$k]['date_to'] = $to;
                                    $product_sale[$k]['id_shop'] = (int)$id_shop;
                                    $product_sale[$k]['reduction'] = (float)ObjectModel::escape_str($productsPricesSale['value']);
                                    $product_sale[$k]['reduction_type'] = ObjectModel::escape_str($productsPricesSale['type']);

                                    if(isset($productsPricesSale['combination'])){
                                        $product_sale[$k]['id_product_attribute'] = (int)ObjectModel::escape_str($productsPricesSale['combination']);
                                    }
                                    $k++;
                                }
                            }
                           
                        }                     
                        $product['on_sale'] = 1;
                        
                    } else if (isset($productsPrices->Sale)) {
                        
                        $product_sale[$k]['id_product'] = $product['id_product'];
                        $product_sale[$k]['date_from'] = ObjectModel::escape_str($productsPrices->Sale['startDate']);
                        $product_sale[$k]['date_to'] = ObjectModel::escape_str($productsPrices->Sale['endDate']);
                        $product_sale[$k]['id_shop'] = (int)$id_shop;
                        $product_sale[$k]['reduction'] = (float)ObjectModel::escape_str($productsPrices->Sale['value']);
                        $product_sale[$k]['reduction_type'] = ObjectModel::escape_str($productsPrices->Sale['type']);
                        $product_sale[$k]['price'] = (float)ObjectModel::escape_str($productsPrices->Sale);
                        
                        $product['on_sale'] = 1;
                    } else {
                        if(!isset($delete_product_sale)){$delete_product_sale=array();}
                        $delete_product_sale[]=$product['id_product'];
                    }
                }
               
                //Suppliers - one or many ???
                if(!isset($supplier)){
                    $supplier = array();$k=0;
                }else{
                    $k=sizeof($supplier);
                }
                foreach ($Products->Suppliers as $productSuppliers) {
                    
                    foreach ($productSuppliers as $productSupplier) {
                        
                        $supplier[$k]['id_product'] = $product['id_product'];
                        $supplier[$k]['supplier_reference'] = ObjectModel::escape_str($productSupplier->Reference);
                        $supplier[$k]['id_supplier'] = (int) ObjectModel::escape_str($productSupplier['ID']);
                        $supplier[$k]['id_product_supplier'] = (int) ObjectModel::escape_str($productSupplier['ID']);
                        $supplier[$k]['id_shop'] = (int) $id_shop;

                        $product['id_supplier'] = $supplier[$k]['id_supplier'];
                        $k++;
                    } 
                }

                //Product Manufacturer
                if (isset($Products->Manufacturer['ID'])){
                    $product['id_manufacturer'] = (int) ObjectModel::escape_str($Products->Manufacturer['ID']);
                }

                //Product Category 
                if(!isset($category)){
                    $category = array();$k=0;
                }else{
                    $k=sizeof($category);
                }  
                foreach ($Products->Categories as $productsCategories) {
                    
                    foreach ($productsCategories as  $productsCategory) {
                        
                        $category[$k]['id_product'] = $product['id_product'];
                        $category[$k]['id_category'] = (int) ObjectModel::escape_str($productsCategory['ID']);
                        $category[$k]['id_shop'] = (int) $id_shop;

                        //Table product_category
                        if (isset($productsCategory['type']) && ($productsCategory['type'] == "Default")){
                            $product['id_category_default'] = (int) ObjectModel::escape_str($productsCategory['ID']);
                        }
                        $k++;
                    }
                    
                }

                //Product Data
                if(!isset($name)){
                        $name = array();$k=0;
                }else{
                        $k=sizeof($name);
                } 
                foreach ($Products->ProductData as $productsData) {
                    //Product Data Names 
                    foreach ($productsData->Names as $productsDataName) {
                        
                        foreach ($productsDataName as  $productName) {
                            $k = $product['id_product'].'_'.((int) ObjectModel::escape_str($productName['lang']));
                            $name[$k]['id_product'] = $product['id_product'];
                            $name[$k]['id_shop'] = (int) $id_shop;
                            $name[$k]['name'] = ObjectModel::escape_str($productName);
                            $name[$k]['id_lang'] = (int) ObjectModel::escape_str($productName['lang']);

                            if (!isset($name[$k]['name']) || empty($name[$k]['name'])) {
                                if(!isset($sql_log_error['product'][$error][6][$id_product][0])){
                                    
                                    $err = array(
                                        'product_type' => 'product',
                                        'severity' => $error,
                                        'error_code' => 6,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => 0,
                                        'message' =>  $name[$k]['id_lang'],
                                    );

                                    $sql_log .=  $log->message_log($err,  $batchID, $id_shop, $time);

                                    $sql_log_error['product'][$error][6][$id_product][0] = true;
                                	$error_counter[$id_product] = 'error';
                                        unset($name[$k]);
                                } 
                            } 
                        }                        
                    }

                    //Product Data Descriptions
                    foreach ($productsData->Descriptions as $productsDataDescription) {
                        foreach ($productsDataDescription as $productDescription) {
			    
                            $k = $product['id_product'].'_'.((int) ObjectModel::escape_str($productDescription['lang']));
                            if(!isset($name[$k])) continue;
                            $name[$k]['description'] = ObjectModel::escape_str($productDescription);
                        }
                    }

                    //Product Data Short Descriptions
                    if (isset($productsData->ShortDescriptions) && !empty($productsData->ShortDescriptions)) {
                        foreach ($productsData->ShortDescriptions as $productsDataShortDescription) {
                            foreach ($productsDataShortDescription as $productShortDescription) {
                                $k = $product['id_product'].'_'.((int) ObjectModel::escape_str($productShortDescription['lang']));
                                if(!isset($name[$k])) continue;
                                $name[$k]['description_short'] = ObjectModel::escape_str($productShortDescription);
                            }
                        }
                    }
		    
		    // Product url 2015-12-03		    
		    if (isset($productsData->ProductLink) && !empty($productsData->ProductLink)) {
                        //foreach ($productsData->ProductLink as $ProductLink) {
                                $k = $product['id_product'];
				$link[$k]['id_product'] = (int) $product['id_product'];
				$link[$k]['id_shop'] = (int) $id_shop;
                                $link[$k]['link'] = ObjectModel::escape_str($productsData->ProductLink);
                        //}
                    }
		    
                    if(!isset($tag)){
                        $tag = array();
                    }
                    if(!isset($tag_lang)){
                        $tag_lang = array(); $k=0;
                    }else{
                        $k=sizeof($tag_lang);
                    }
		    
                    //Product Data Tags
                    if (isset($productsData->Tags) && !empty($productsData->Tags)) {
                        //Table product_tag
                        
                        $tag[$product['id_product']]['id_product'] = $product['id_product'];
                        $tag[$product['id_product']]['id_product_tag'] = $product['id_product'];
                        $tag[$product['id_product']]['id_shop'] = (int) $id_shop;

                        foreach ($productsData->Tags as $productsDataTag) {
                            
                            foreach ($productsDataTag as  $productTag) {
                                
                                $tag_lang[$k]['id_product_tag'] = $tag[$product['id_product']]['id_product_tag'];
                                $tag_lang[$k]['id_shop'] = (int) $id_shop;
                                $tag_lang[$k]['name'] = ObjectModel::escape_str($productTag);
                                $tag_lang[$k]['id_lang'] = (int) ObjectModel::escape_str($productTag['lang']);

                                $k++;
                            }
                        }
                    }
                }

                //Product Images
                if(!isset($image)){
                    $image = array(); $k=0;
                }else{
                    $k=sizeof($image);
                }
		
                foreach ($Products->Images as $productsImages) {
                    
                    foreach ($productsImages->Image as  $productsImage) { 
                        $image[$k]['id_product'] = $product['id_product'];
                        $image[$k]['id_image'] = (int) ObjectModel::escape_str($productsImage['id']);
                        $image[$k]['id_shop'] = (int) $id_shop;
                        $image[$k]['image_type'] = ObjectModel::escape_str($productsImage['type']);
                        $image[$k]['image_url'] = ObjectModel::escape_str($productsImage);

                        $k++;
                    }
                }

                //Product Carriers
                if(!isset($carrier)){
                    $carrier = array(); $k=0;
                }else{
                    $k=sizeof($carrier);
                }
		
                foreach ($Products->Carriers as $productsCarriers) {
                    
                    foreach ($productsCarriers as  $productsCarrier) {
                        if (isset($productsCarrier['ID']) && !empty($productsCarrier['ID']) && $productsCarrier['ID'] != null) {
                            
                            $carrier[$k]['id_product'] = $product['id_product'];
                            $carrier[$k]['id_carrier'] = (int) ObjectModel::escape_str($productsCarrier['ID']);
                            $carrier[$k]['id_shop'] = (int) $id_shop;
                            $carrier[$k]['price'] = (float) ObjectModel::escape_str($productsCarrier['price']); 
                          
                            $k++;
                        }
                    }
                }

                //Product Features 
                if (!isset($feature)) {
                    $feature = array();
                    $k = 0;
                } else {
                    $k = sizeof($feature);
                }
		
                foreach ($Products->Features as $productsFeatures) {
                    
                    foreach ($productsFeatures as $productsFeature) {
                        
                        $feature[$k]['id_product'] = $product['id_product'];
                        $feature[$k]['id_feature'] = (int) ObjectModel::escape_str($productsFeature['ID']);
                        $feature[$k]['id_feature_value'] = (int) ObjectModel::escape_str($productsFeature['Option']);
                        $feature[$k]['id_shop'] = (int) $id_shop;
                        $k++;
                    }                     
                }

                //Product Condition  
                $product['id_condition'] = (int) ObjectModel::escape_str($Products->Condition['ID']);

                //Topology
                if (isset($Products->Topology)) {
                    foreach ($Products->Topology as $productTopology) {
                        foreach ($productTopology->Unit as $units) {
                            if ($units['Name'] == "Width")
                                $product['width'] = ObjectModel::escape_str($units);
                            if ($units['Name'] == "Height")
                                $product['height'] = ObjectModel::escape_str($units);
                            if ($units['Name'] == "Depth")
                                $product['depth'] = ObjectModel::escape_str($units);
                            if ($units['Name'] == "Weight")
                                $product['weight'] = ObjectModel::escape_str($units);
                        }
                    }
                }
               
                if (!isset($combination)) {
                    $combination = array(); $k = 0;
                } else {
                    $k = sizeof($combination);
                }  
                if (!isset($combination_images)) {
                    $combination_images = array(); $k1 = 0;
                } else {
                    $k1 = sizeof($combination_images);
                }
                if(!isset($arr_unit)){
                    $arr_unit=array();
                }
                
                if(!isset($arr_atr)){
                    $arr_atr=array();
                } 		
		
                if (isset($Products->Combinations)) {
                    //Product Combinations 
                    $product['has_attribute'] = $product['quantity'] = $id_product_attribute = 0;
                    
                    foreach ($Products->Combinations as $productsCombinations) {
                        foreach ($productsCombinations as $productsCombination) {
                            $product['has_attribute'] ++;

                            $attribute = array();
                            $attribute['id_product'] = $product['id_product'];
                            $attribute['id_shop'] = (int) $id_shop;

                            //Identifier
                            foreach ($productsCombination->Identifier as $combinationIdentifiers) {
                                foreach ($combinationIdentifiers as $combinationIdentifier) {
                                    if ($combinationIdentifier['type'] == "ID") {
                                        $attribute['id_product_attribute'] = (int) ObjectModel::escape_str($combinationIdentifier);
                                        $id_product_attribute = $attribute['id_product_attribute'];
                                    }
                                    if ($combinationIdentifier['type'] == "SKU") {
                                        $attribute['sku'] = ObjectModel::escape_str($combinationIdentifier);
                                    }
                                    if ($combinationIdentifier['type'] == "Reference") {
                                        $attribute['reference'] = ObjectModel::escape_str($combinationIdentifier);
                                    }
                                }

                                if ($combinationIdentifiers->Code['type'] == "EAN") {
                                    $attribute['ean13'] = ObjectModel::escape_str($combinationIdentifiers->Code);
                                }
                                if ($combinationIdentifiers->Code['type'] == "UPC") {
                                    $attribute['upc'] = ObjectModel::escape_str($combinationIdentifiers->Code);
                                }
                            }

                            if ((!isset($attribute['reference']) || empty($attribute['reference'])) && (!isset($attribute['sku']) || empty($attribute['sku']))) {
                                if(!isset($sql_log_error['offer'][$warning][2][$id_product][$id_product_attribute])){
                                    
                                    $err = array(
                                        'product_type' => 'offer',
                                        'severity' => $warning,
                                        'error_code' => 13,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => $id_product_attribute,
                                    );
                                    
                                    $sql_log .=  $log->message_log($err,  $batchID, $id_shop, $time);
                                    
                                    $sql_log_error['offer'][$warning][2][$id_product][$id_product_attribute] = true;
                                	$error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';                                    
                                } 
                            } else {
                                if ((!isset($attribute['ean13']) || empty($attribute['ean13'])) && (!isset($attribute['upc']) || empty($attribute['upc']))) {
                                    if(!isset($sql_log_error['offer'][$warning][4][$id_product][$id_product_attribute])){
                                    
                                        $err = array(
                                            'product_type' => 'offer',
                                            'severity' => $warning,
                                            'error_code' => 4,
                                            'id_product' => $id_product,
                                            'id_product_attribute' => $id_product_attribute,
                                        );
                                        
                                        $sql_log .=  $log->message_log($err,  $batchID, $id_shop, $time);
                                        
                                        $sql_log_error['offer'][$warning][4][$id_product][$id_product_attribute] = true;
                                		$error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                                    } 
                                }
                            }

                            //Product Attribute URL
                            if(isset($productsCombination->ProductCombinationLink) && !empty($productsCombination->ProductCombinationLink)){
                                $attribute_link[$id_product.'_'.$id_product_attribute]['id_product']=(int)$id_product;
                                $attribute_link[$id_product.'_'.$id_product_attribute]['id_product_attribute']=(int)$id_product_attribute;
                                $attribute_link[$id_product.'_'.$id_product_attribute]['id_shop']=(int)$id_shop;
                                $attribute_link[$id_product.'_'.$id_product_attribute]['link'] = ObjectModel::escape_str($productsCombination->ProductCombinationLink);
                            }
			    
			    #OffersOptions 
			    // When the parent product has Offers Options, set the options to all id_product_tribute
			    if(isset($Products->OffersOptions) && isset($id_product_attribute)){
				foreach ($Products->OffersOptions as $OffersOptions){
				    foreach($OffersOptions as $OptionKeys => $OffersOption){

					if(!empty($OptionKeys)){

					    // check table exits
					    if(!isset($history[$OptionKeys])){
						$history[$OptionKeys] = $ProductOption->checke_table_exit($OptionKeys);
					    }

					    if(isset($history[$OptionKeys]) && $history[$OptionKeys]){
						foreach ($OffersOption as $OfferOption){

						    if($OfferOption->count() > 0) {
							if(isset($OfferOption['region'])){
							    $Offer_key = ObjectModel::escape_str($OfferOption['region']);
							    $id_country= (int) $countries[ObjectModel::escape_str($OfferOption['region'])]['id'];
							}

							if(isset($OfferOption['lang'])){
							    $Offer_key = ObjectModel::escape_str($OfferOption['lang']);
							    $id_lang= (int) ObjectModel::escape_str($OfferOption['lang']);
							}

							$OfferOption->id_product = $product['id_product'];
							$OfferOption->id_product_attribute = $id_product_attribute;
							$OfferOption->sku = isset($attribute['reference']) ? $attribute['reference'] : null;
							$OfferOption->id_shop = (int) $id_shop;
							$OfferOption->id_country = isset($id_country) ? $id_country : null;
							$OfferOption->id_lang =  isset($id_lang) ? $id_lang : null;

							$offers = $ProductOption->set_offers_options($OfferOption, $OptionKeys);

							if(isset($offers['options']) && !empty($offers['options'])){
							    if(!isset($ProductOptions['options'][$OptionKeys])){
								$ProductOptions['options'][$OptionKeys] = array();
                                                            }
                                                            $ProductOptions['options'][$OptionKeys][]=   $offers['options'];
							}

							if(isset($offers['repricing']) && !empty($offers['repricing'])){
							    if(!isset($ProductOptions['repricing'][$OptionKeys])){
								$ProductOptions['repricing'][$OptionKeys] = '';
							    }
							    $ProductOptions['repricing'][$OptionKeys] .= ", ". $offers['repricing'];
							}

							unset($offers);
							unset($OfferOption);
						    }
						}
					    } 
					}					
				    }
				}
			    }	
			    // When childred has Offers Options, replace offer option in this attribute
			    if(isset($productsCombination->OffersOptions) && isset($id_product_attribute)){
				foreach ($productsCombination->OffersOptions as $OffersOptions){
				    foreach($OffersOptions as $OptionKeys => $OffersOption){

					if(!empty($OptionKeys)){

					    // check table exits
					    if(!isset($history[$OptionKeys])){
						$history[$OptionKeys] = $ProductOption->checke_table_exit($OptionKeys);
					    }

					    if(isset($history[$OptionKeys]) && $history[$OptionKeys]){
						
						foreach ($OffersOption as $OfferOption){

						    if($OfferOption->count() > 0) {
							if(isset($OfferOption['region'])){
							    $Offer_key = ObjectModel::escape_str($OfferOption['region']);
							    $id_country= (int) $countries[ObjectModel::escape_str($OfferOption['region'])]['id'];
							}

							if(isset($OfferOption['lang'])){
							    $Offer_key = ObjectModel::escape_str($OfferOption['lang']);
							    $id_lang= (int) ObjectModel::escape_str($OfferOption['lang']);
							}

							$OfferOption->id_product = $product['id_product'];
							$OfferOption->id_product_attribute = $id_product_attribute;
							$OfferOption->sku = isset($attribute['reference']) ? $attribute['reference'] : null;
							$OfferOption->id_shop = (int) $id_shop;
							$OfferOption->id_country = isset($id_country) ? $id_country : null;
							$OfferOption->id_lang =  isset($id_lang) ? $id_lang : null;

							$offers = $ProductOption->set_offers_options($OfferOption, $OptionKeys);

							if(isset($offers['options']) && !empty($offers['options'])){
							    if(!isset($ProductOptions['options'][$OptionKeys])){
								$ProductOptions['options'][$OptionKeys] = array();
                                                            }
                                                            $ProductOptions['options'][$OptionKeys][]=   $offers['options'];
							}

							if(isset($offers['repricing']) && !empty($offers['repricing'])){
							    if(!isset($ProductOptions['repricing'][$OptionKeys])){
								$ProductOptions['repricing'][$OptionKeys] = '';
							    }
							    $ProductOptions['repricing'][$OptionKeys] .= ", ". $offers['repricing'];
							}

							unset($offers);
							unset($OfferOption);
						    }
						}
					    } 
					}					
				    }
				}
			    } 			    
			    #OffersOptions 
			    	
                            //Attributes
                            foreach ($productsCombination->Attributes as $combinationAttributes) {
                                
                                foreach ($combinationAttributes->Attribute as  $combinationAttribute) {
                                    $combination[$k]['id_product_attribute'] = $attribute['id_product_attribute'];
                                    $combination[$k]['id_attribute_group'] = (int) ObjectModel::escape_str($combinationAttribute['ID']);
                                    $combination[$k]['id_attribute'] = (int) ObjectModel::escape_str($combinationAttribute['Option']);
                                    $combination[$k]['id_shop'] = (int) $id_shop;

                                    $k++;
                                }
                            }

                            //Images
                            foreach ($productsCombination->Images as $combinationImages) {
                                
                                foreach ($combinationImages as  $combinationImage) { 
                                    $combination_images[$k1]['id_product_attribute'] = $attribute['id_product_attribute'];
                                    $combination_images[$k1]['id_image'] = (int) ObjectModel::escape_str($combinationImage['id']);
                                    $combination_images[$k1]['id_shop'] = (int) $id_shop; 
                                 
                                    $k1++;
                                }
                            }

                            //PriceOverride
                            foreach ($productsCombination->PriceOverride as $combinationPriceOverride) {
                                $attribute['price_type'] = $combinationPriceOverride->Operation['type'];
                                $attribute['price'] = (float) $combinationPriceOverride->Operation;
                                
                                /* wholesale_price ADD : 16/07/2015 */
                                if(isset($combinationPriceOverride->WholeSale)){
                                    $attribute['wholesale_price'] = (float)ObjectModel::escape_str($combinationPriceOverride->WholeSale);
                                }
                            }

                            //Topology                            
                            foreach ($productsCombination->Topology as $combinationTopology) {
                                $unit = array();
                                if(empty($combinationTopology->Unit)){
                                    continue;
                                }
                                $unit['id_product_attribute'] = $attribute['id_product_attribute'];
                                $unit['id_shop'] = (int) $id_shop;
                                foreach ($combinationTopology->Unit as $units) {
                                    $unit['id_unit'] = (int) ObjectModel::escape_str($units['ID']);

                                    if ($units['Name'] == "Width")
                                        $product['width'] = (float) ObjectModel::escape_str($units);
                                    if ($units['Name'] == "Height")
                                        $product['height'] = (float) ObjectModel::escape_str($units);
                                    if ($units['Name'] == "Depth")
                                        $product['depth'] = (float) ObjectModel::escape_str($units);
                                    if ($units['Name'] == "Weight")
                                        $attribute['weight'] = (float) ObjectModel::escape_str($units);

                                    $unit['name'] = $units['Name'];
                                    $unit['value'] = (float) ObjectModel::escape_str($units);
                                }
                                $arr_unit[]=$unit;
                            }

                            //Availabilities
                            $attribute['quantity'] = 0;

                            if (isset($productsCombination->Availabilities) && !empty($productsCombination->Availabilities))
                                foreach ($productsCombination->Availabilities as $combinationAvailabilities) {
                                    $attribute['quantity'] = (int) ObjectModel::escape_str($combinationAvailabilities->Stock);
                                    $product['quantity'] = $product['quantity'] + $attribute['quantity'];

                                    $availability_active = (int) ObjectModel::escape_str($combinationAvailabilities->Active);
                                    if (($availability_active > 0) && ($availability_active != 0))
                                        $product['active'] = 1;

                                    if (isset($combinationAvailabilities->Date))
                                        $attribute['available_date'] = date('Y-m-d H:i:s', strtotime((string)$combinationAvailabilities->Date));
                                }

			    $arr_atr[] = $attribute;
                        }
                    }
                }		
		
                //Availabilities
                if (isset($Products->Availabilities)) {
                    foreach ($Products->Availabilities as $productAvailabilities) {
                        if (isset($productAvailabilities->Stock))
                            $product['quantity'] = (int) ObjectModel::escape_str($productAvailabilities->Stock);

                        if (isset($productAvailabilities->Active))
                            $product['active'] = (int) ObjectModel::escape_str($productAvailabilities->Active);

                        if (isset($Products->Date))
                            $product['available_date'] = date('Y-m-d H:i:s', strtotime((string)$Products->Date));
                    }
                }

                if (!isset($product['quantity']) || empty($product['quantity']) || $product['quantity'] == 0){
                    if(!isset($sql_log_error['offer'][$warning][7][$id_product][0])){
                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $warning,
                            'error_code' => 7,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );
                        
                        $sql_log .=  $log->message_log($err,  $batchID, $id_shop, $time);
                        
                        $sql_log_error['offer'][$warning][7][$id_product][0] = true;
                        $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                    }   
                }

                if (!isset($product['active']) || empty($product['active']) || $product['active'] == 0){
                    if(!isset($sql_log_error['offer'][$warning][8][$id_product][0])){
                                    
                        $err = array(
                            'product_type' => 'offer',
                            'severity' => $warning,
                            'error_code' => 8,
                            'id_product' => $id_product,
                            'id_product_attribute' => 0,
                        );
                        
                        $sql_log .=  $log->message_log($err,  $batchID, $id_shop, $time);
                        
                        $sql_log_error['offer'][$warning][8][$id_product][0] = true;
                        $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'warning';
                    }  
                }

                //Table product
                if(!isset($arr_prod))
		    $arr_prod=array();
		
                $arr_prod[]=$product;
                
                $p_err = isset($sql_log_error['product'][$error])?sizeof($sql_log_error['product'][$error]):0;
                $o_err = isset($sql_log_error['offer'][$error])?sizeof($sql_log_error['offer'][$error]):0;                
                $p_warn = isset($sql_log_error['product'][$warning])?sizeof($sql_log_error['product'][$warning]):0;
                $o_warn  = isset($sql_log_error['offer'][$warning])?sizeof($sql_log_error['offer'][$warning]):0;
                
                $error_counter[$id_product] = isset($error_counter[$id_product]) ? $error_counter[$id_product] : 'success';               
                $return .= $sql;                
            } 

            $error_counter_sumary = array_count_values($error_counter);
            $no_success = isset($error_counter_sumary['success']) ? $error_counter_sumary['success'] : 0;
            $no_error = isset($error_counter_sumary['error']) ? $error_counter_sumary['error'] : 0;
            $no_warning = isset($error_counter_sumary['warning']) ? $error_counter_sumary['warning'] : 0;            
        }
		
        //group query by table
        $sql='';
	
        if(!empty($delete_product_sale))
	    $sql .= "DELETE FROM {$this->prefix_table}product_sale WHERE id_product in ( " . implode(',', $delete_product_sale) . " ) ; \n";
        if(isset($product_sale))
	    $sql .= self::insert('product_sale', $product_sale, $time);
	if(isset($supplier))
	    $sql .= self::insert('product_supplier', $supplier, $time);
	if(isset($category))
	    $sql .= self::insert('product_category', $category, $time);
	if(isset($name))
	    $sql .= self::insert('product_lang', $name, $time);
	if(isset($link))
	    $sql .= self::insert('product_url', $link, $time);
	if(isset($tag))
	    $sql .= self::insert('product_tag', $tag, $time);
	if(isset($tag_lang))
	    $sql .= self::insert('product_tag_lang', $tag_lang, $time);
	if(isset($image))
	    $sql .= self::insert('product_image', $image, $time);
	if(isset($carrier))
	    $sql .= self::insert('product_carrier', $carrier, $time);
	if(isset($feature))
	    $sql .= self::insert('product_feature', $feature, $time);
	if(isset($combination))
	    $sql .= self::insert('product_attribute_combination', $combination, $time);
	if(isset($combination_images))
	    $sql .= self::insert('product_attribute_image', $combination_images, $time);
	if(isset($arr_unit))
	    $sql .= self::insert('product_attribute_unit', $arr_unit, $time);
        if(isset($attribute_link))
            $sql .= self::insert('product_attribute_url', $attribute_link, $time);
	if(isset($arr_atr)){
            if(!empty($arr_atr)&&!empty($id_shop)){
                global $p_atr_ref_info;
                if(empty($p_atr_ref_info)){
                    $p_atr_ref_info=array();
                    $sqlb = "SELECT p.id_product,p.id_product_attribute, p.reference,p.old_reference,p.flag_update_ref FROM products_product_attribute p  where p.id_shop = '$id_shop' ";
                    $list = $this->conn_id->db_query_str($sqlb);
                    foreach($list as $l){
                        $p_atr_ref_info[$l['id_product'].'_'.$l['id_product_attribute']] = $l;
                    }
                }
            
                foreach($arr_atr as $k=>$atr){
                    $id_product = $atr['id_product'];
                    $id_product_atr = $atr['id_product_attribute'];
                    $arr_atr[$k]['old_reference'] = isset($p_atr_ref_info[$id_product.'_'.$id_product_atr]['old_reference'])?
                        $p_atr_ref_info[$id_product.'_'.$id_product_atr]['old_reference']:'';
                    $arr_atr[$k]['flag_update_ref'] = isset($p_atr_ref_info[$id_product.'_'.$id_product_atr]['old_reference'])?
                        $p_atr_ref_info[$id_product.'_'.$id_product_atr]['flag_update_ref']:'';
                    if(!empty($atr['reference']) && isset($p_atr_ref_info[$id_product.'_'.$id_product_atr])){
                        if($atr['reference'] != $p_atr_ref_info[$id_product.'_'.$id_product_atr]['reference']){
                            $arr_atr[$k]['old_reference'] = $p_atr_ref_info[$id_product.'_'.$id_product_atr]['reference'];
                            $arr_atr[$k]['flag_update_ref'] = date('Y-m-d H:i:s');
                        }
                    }
                }
            }

	    $sql .= self::insert('product_attribute', $arr_atr, $time);
            $sql .= "update products_product_attribute set flag_update_ref=null where old_reference is null;";
        }
	if(isset($arr_prod)){
//            if(!defined('KEEP_SQL_DIRECT')){define('KEEP_SQL_DIRECT',true);}
            if(!empty($arr_prod)&&!empty($id_shop)){
                global $p_ref_info;
                if(empty($p_ref_info)){
                    $p_ref_info=array();
                    $sqlb = "SELECT p.id_product, p.reference,p.old_reference,p.flag_update_ref FROM products_product p  where p.id_shop = '$id_shop' ";
                    $list = $this->conn_id->db_query_str($sqlb);
                    foreach($list as $l){
                        $p_ref_info[$l['id_product']] = $l;
                    }
                }

                foreach($arr_prod as $k=>$pro){
                    $id_product = $pro['id_product'];
                    $arr_prod[$k]['old_reference'] = isset($p_ref_info[$id_product]['old_reference'])?
                        $p_ref_info[$id_product]['old_reference']:'';
                    $arr_prod[$k]['flag_update_ref'] = isset($p_ref_info[$id_product]['flag_update_ref'])?
                        $p_ref_info[$id_product]['flag_update_ref']:date('Y-m-d H:i:s');
                    if(!empty($pro['reference']) && isset($p_ref_info[$id_product])){
                        if($pro['reference'] != $p_ref_info[$id_product]['reference']){
                            $arr_prod[$k]['old_reference'] = $p_ref_info[$id_product]['reference'];
                            $arr_prod[$k]['flag_update_ref'] = date('Y-m-d H:i:s');
                        }
                    }
                }
            }

	    $sql .= self::insert(self::$definition['table'], $arr_prod, $time);
            $sql .= "update products_product set flag_update_ref=null where old_reference is null;";
        }
	
        $return .= $sql;
        
        // Log
        $sql_log = $log->get_all_message_log();
        $return .= $sql_log;	
	
	#offersoptions
	if(isset($ProductOptions['options']) && !empty($ProductOptions['options'])){
	    $return .= $ProductOption->save_offers_options($ProductOptions['options']);
	}
	if(isset($ProductOptions['repricing']) && !empty($ProductOptions['repricing'])){
	    $return .= $ProductOption->save_repricing($ProductOptions['repricing']);
	}
	unset($ProductOptions);
	
        return $return ;
    }

    /*private function offersOptions($xmlOffersOptions, $countries, $id_shop, $id_product, $id_product_attribute = 0, $reference = null){
	
	foreach ($xmlOffersOptions->OffersOptions as $OffersOptions){
	    
	    foreach($OffersOptions as $OptionKeys => $OffersOption){

		if(!empty($OptionKeys)){

		    // check table exits
		    if(!isset($history[$OptionKeys])){
			$history[$OptionKeys] = $ProductOption->checke_table_exit($OptionKeys);
		    }

		    if(isset($history[$OptionKeys]) && $history[$OptionKeys]){

			foreach ($OffersOption as $OfferOption){

			    if($OfferOption->count() > 0) {
				if(isset($OfferOption['region'])){
				    $Offer_key = ObjectModel::escape_str($OfferOption['region']);
				    $id_country= (int) $countries[ObjectModel::escape_str($OfferOption['region'])]['id'];
				}

				if(isset($OfferOption['lang'])){
				    $Offer_key = ObjectModel::escape_str($OfferOption['lang']);
				    $id_lang= (int) ObjectModel::escape_str($OfferOption['lang']);
				}

				$OfferOption->id_product = $id_product;
				$OfferOption->id_product_attribute = $id_product_attribute;
				$OfferOption->sku = $reference;
				$OfferOption->id_shop = (int) $id_shop;
				$OfferOption->id_country = isset($id_country) ? $id_country : null;
				$OfferOption->id_lang =  isset($id_lang) ? $id_lang : null;

				$offers = $ProductOption->set_offers_options($OfferOption, $OptionKeys);

				if(isset($offers['options']) && !empty($offers['options'])){
				    if(!isset($ProductOptions['options'][$OptionKeys])){
					$ProductOptions['options'][$OptionKeys] = '';
				    }
				    $ProductOptions['options'][$OptionKeys] .= ", ". $offers['options'];
				}

				if(isset($offers['repricing']) && !empty($offers['repricing'])){
				    if(!isset($ProductOptions['repricing'][$OptionKeys])){
					$ProductOptions['repricing'][$OptionKeys] = '';
				    }
				    $ProductOptions['repricing'][$OptionKeys] .= ", ". $offers['repricing'];
				}

				unset($offers);
				//unset($OfferOption);
			    }
			}
		    } 
		}					
	    }
	}	
    }*/
}