<?php

class ProductTag extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_tag',
        //'unique'    => array('product_tag' => array( 'id_product_tag', 'id_shop')),
        'fields'    => array(
            'id_product'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_product_tag'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
        'lang' => array(
            'fields'    => array(
                'id_product_tag'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_lang'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
                'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 128),
                'date_add'          =>  array('type' => 'datetime', 'required' => true ),
            ),
        ),
       
    );
  
    
}