<?php

class OTP extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'token',
        'fields'    => array(
            'token'         =>  array('type' => 'varchar', 'required' => true, 'size' => 255),
            'feed_source'   =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'status'        =>  array('type' => 'int', 'required' => true, 'size' => 1 ),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    public function generate_token()
    {
        $token = md5(uniqid(mt_rand(), true));
        $time = date("Y-m-d H:i:s");
        
        $data = array(
            'token' => $token,
            'status' => '1',
        );
        
        $sql = self::insert('token', $data, $time);
        
        if($sql)
            if(self::add($sql))
                return TRUE;
        
        return FALSE;
    }
    
    public function check_token($token)
    {
        if(!isset($token) || empty($token))
            return FALSE;
        
        $row = 0;
        
        $sql = self::db_query("SELECT * FROM {$this->prefix_table}token WHERE token = '" . $token . "' AND status = 1");

        if($sql)
             $row = self::db_num_rows($sql);
         
        if($row == 1)
            return TRUE;
        
        return FALSE;
    }
    
}

