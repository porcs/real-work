<?php

class ProductAttributeCombination extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_attribute_combination',
        'unique'        => array('product_attribute_combination' => array('id_product_attribute',  'id_attribute_group', 'id_attribute', 'id_shop')),
        'fields'    => array(
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_attribute_group'    =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_attribute'          =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
    
}