<?php

class ProductImage extends ObjectModel
{
   
    public function __construct( $user, $id = null, $connect = true)
    {
        parent::__construct( $user, $id  , $connect );  
        $this->user = $user;
    }
    
    public static $definition = array(
        'table'     => 'product_image',
        'unique'    => array('product_image' => array('id_product', 'id_image', 'id_shop')),
        'fields'    => array(
            'id_product'   =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_image'     =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_shop'      =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'image_url'    =>  array('type' => 'text', 'required' => false, ),
            'image_type'   =>  array('type' => 'varchar', 'required' => true, 'size' => 20 ),
            'date_add'     =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
    public function getImages($id_shop)
    {
        $productImages = array();
                
        $result = self::db_query_str("SELECT * FROM {$this->prefix_table}product_image WHERE id_shop = " . $id_shop);
        
        foreach ($result as $keys =>$image)
        {
            foreach ($image as $key => $img)
            {
                if(!is_int($key))
                    $productImages[$keys][$key] = $img;
            }
            
        }
        
        return $productImages;
    }
    
    public function setImages($sql)
    {
        $return = true;
        
        self::db_trans_begin();
        if(!self::add($sql))
            $return = false;
        self::db_trans_commit();
        
        return $return;
    }
    
}