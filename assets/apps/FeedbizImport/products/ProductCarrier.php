<?php

class ProductCarrier extends ObjectModel
{
    
    public static $definition = array(
        'table'     => 'product_carrier',
        'unique'        => array('product_attribute_unit' => array('id_product', 'id_carrier', 'id_shop')),
        'fields'    => array(
            'id_product'    =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_carrier'    =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'price'         =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
}