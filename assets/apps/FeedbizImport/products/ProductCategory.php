<?php

class ProductCategory extends ObjectModel
{
   
    public static $definition = array(
        'table'     => 'product_category',
        'unique'    => array('product_category' => array('id_product', 'id_category', 'id_shop')),
        'fields'    => array(
            'id_product'    =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_category'   =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
}