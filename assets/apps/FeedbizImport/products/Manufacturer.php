<?php

class Manufacturer extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'         => 'manufacturer',
        'primary'       => array('id_manufacturer', 'id_shop'),
        'unique'        => array('manufacturer' => array('id_manufacturer', 'id_shop')),
        'fields'        => array(
            'id_manufacturer'   =>  array('type' => 'INTEGER', 'required' => true),
            'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    /**
    * $return Query (string)
    */
    public function importData($data, $time)
    {
        //Manufacturer
        foreach($data as $ManufacturersXml)
        {
            $sql = '';
            $manufacturer = array();
            $id_shop = $ManufacturersXml->id_shop;
            
            foreach ($ManufacturersXml->Manufacturer as $Manufacturers)
            {
                $manufacturer['id_manufacturer'] = (int)ObjectModel::escape_str($Manufacturers['ID']);
                $manufacturer['id_shop'] = (int)$id_shop;
                $manufacturer['name'] = ObjectModel::escape_str($Manufacturers);
                if(empty($manufacturer['name']))continue;
                $sql .= self::insert('manufacturer', $manufacturer, $time);
                
            }
        }
        
        return $sql;
    }
    
}