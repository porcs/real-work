<?php

class AttributeGroup extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'attribute_group',
        'primary'   => array('id_attribute_group', 'id_shop'),
        'unique'    => array('attribute_group' => array('id_attribute_group', 'id_shop')),
        'fields'    => array(
            'id_attribute_group'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'is_color_group'        =>  array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0' ),
            'group_type'            =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default' => 'select' ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
        'lang' => array(
            'fields'    => array(
                'id_attribute_group'=>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
                'id_lang'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 128),
                'date_add'          =>  array('type' => 'datetime', 'required' => true ),
            ),
        ),
    );
    
}