<?php

class ProductAttributeUnit extends ObjectModel
{

    public static $definition = array(
        'table'     => 'product_attribute_unit',
        'unique'        => array('product_attribute_unit' => array('id_product_attribute', 'id_shop')),
        'fields'    => array(
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_unit'               =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'                  =>  array('type' => 'varchar', 'required' => true, 'size' => 16 ),
            'value'                 =>  array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000' ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
}