<?php

class Language extends ObjectModel
{
    public $id;

    /** @var string Name */
    public $name;

    /** @var string 2-letter iso code */
    public $iso_code;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'language',
        'primary'   => array('id_lang', 'id_shop'),
        'unique'    => array('language' => array('id_lang', 'id_shop')),
        'fields'    => array(
            'id_lang'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 32),
            'iso_code'      =>  array('type' => 'varchar', 'required' => true, 'size' => 2),
            'is_default'    =>  array('type' => 'tinyint', 'required' => true, 'size' => 1 ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
    );

    /**
    * $return Query (string)
    */
    public function importData($data, $time)
    {        
        //Language
        foreach($data as $LanguagesXml)
        {
            $sql = '';
            $language = array();
            $id_shop = $LanguagesXml->id_shop;
            
            foreach ($LanguagesXml->Name as $Languages)
            {
                $language['id_lang'] = (int)ObjectModel::escape_str($Languages['ID']);
                $language['id_shop'] = $id_shop;
                $language['iso_code'] = ObjectModel::escape_str($Languages['iso_code']);
                $language['name'] = ObjectModel::escape_str($Languages);
                $language['is_default'] = ObjectModel::escape_str($Languages['is_default']);
                
                $sql .= self::insert('language', $language, $time);
            }
        }
                
        return $sql;
    }
    
    public static function getLanguages($id_shop)
    {
        $languages = array();
        
        $_db = $this->conn_id;
        $_db->from('language');
        $_db->where(array('id_shop' => (int)$id_shop));
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $language)
        {
            $languages[$language['id_lang']]['id_lang'] = $language['id_lang'];
            $languages[$language['id_lang']]['name'] = $language['name'];
            $languages[$language['id_lang']]['iso_code'] = $language['iso_code'];
        }
        return $languages;
    }
}