<?php

require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');

abstract class ObjectModel
{
    public $user;
    public static $database = 'products';
    public $prefix_table = 'products_';
    public static $prefix_db_table = 'products_';
    public $engine = " ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ";
    
    /* Build object */
    public function __construct( $user, $id = null, $connect = true)
    {
        $this->user = $user;
        $this->path = USERDATA_PATH . $user . '/' . ObjectModel::$database . '.db';
        
        if ($id)
            $this->id = (int)$id;
         
        /* Open connection SQLite user db */
         if($connect)
             $this->db_connect($this->path);
    }
    
    public static function createDatabase($user) 
    {
        $path   = DIR_SERVER . '/assets/apps/';
        $result = array();
        $oldmask = umask(0); 
        if ( !is_dir('users/' . $user) )
        {
            if ( ! mkdir('users/' . $user , 0777) )
            {
                $result['error'] = sprintf('Directory %s - Failed to create user directory', $user) ; 
            }else if ( !is_writable( $path . '/users') )
            {
                $result['error'] = sprintf('Directory %s is not writeable', $path) ; 
            }
        }
       
        if(!isset($result['error']))
        {
            $error = '';
            $conn_id = new Db($user,ObjectModel::$database,false  );

            if(!file_exists('users/' . $user . '/process')){
            if ( !mkdir('users/' . $user . '/process', 0777) )
                $result['error'] = sprintf('Failed to create process directory') ; 
            }
            
        }
        
        umask($oldmask);
        return $result;
    }
    
    public function createTable()
    {   
        $result = '';
        $class = get_called_class();
        $data =  $class::$definition;
        
        $lang = false;
        $loop = 1 ;
        
        if (isset($data['lang']) || !empty($data['lang']))
            $lang = true;
        
        if ($lang)
            $loop = 2;

        for( $l=1; $l <= $loop; $l++ )
        {
            $query = '';
            $sql = array();
            $tablename = $data['table'];
            
            if($l == 2)
            {
               $tablename = $data['table'] . '_lang';
               $data['fields'] = $data['lang']['fields'];
            }
            else 
                $data['fields'] = $data['fields'];
            
            foreach ($data['fields'] as $key_field => $field)
            {
                $default = '';
                $size = '';
                
                if(!isset($field['required']))
                    $field['required'] = false;
                
                $required = ($field['required']) ? ' NOT NULL ' : '' ;
                $default_null =  (isset($field['default_null']) && !$field['required']) ? ' DEFAULT NULL' : '' ;
                $others =  ( isset($field['other']) && !empty($field['other']) ) ? ' ' . $field['other'] : '' ;
		$primary_key = (isset($field['primary_key'])) ? '  AUTO_INCREMENT ' : '' ;
                
                if(!isset($field['default_null']))
                    $default =  isset($field['default']) ? " DEFAULT '" . $field['default'] . "'" : '' ;
                
                if(isset($field['size']))
                    $size = '(' . $field['size'] . ') ';
                $unique = ' ';

                $sql[] = $key_field . ' ' . $field['type'] .  $size  . $required . $default_null . $default . $primary_key . $others .$unique;
            }
            
            $arr_key=array();
            if(isset($data['unique']))
            foreach($data['unique'] as $v){
                if(is_array($v)){
                    foreach($v as $datab){
                        $arr_key[$datab]=$datab;
                    }
                }else{
                    $arr_key[$v]=$v;
                }
            }
            if(isset($data['primary']))
            foreach($data['primary'] as $v){ 
                    $arr_key[$v]=$v; 
            } 
            if($l == 2){
                $arr_key['id_lang']='id_lang';
            }
            if(!empty($arr_key)){
                $sql[] = " PRIMARY KEY (".implode(',',$arr_key).")";
            } 
            
            $sqls = implode(', ', $sql);
            $query .= "CREATE TABLE " . $this->prefix_table . $tablename . "(" . $sqls ;
            
            $query .= ") {$this->engine};" ;
            $out = $this->db_exec($query,false);
        }
        
        if($out!== true)
            $result .= sprintf('Failed to create table %s  '."<br>$query<br>$out" , $class  ) ; 
        
        //Create Index
        if (isset($data['primary']) && !empty($data['primary']))
        {
            $out = $this->createIndex( $data['table'] . " Index", $data['table'], true, $data['primary']);
            if($out !== true)
                $result .= sprintf('Failed to create Index %s '."<br>$out<br>", $data['table'] . " Index" ) ;
        }
        
        return $result;
    }
   
    function createIndex($index_name, $table_name, $unique = false, $columns = array()) 
    {
        $query = '';
        $column_name = array();
        $type = ($unique) ? 'UNIQUE' : '' ; 
        foreach ($columns as $column)
            $column_name[] = "" . $column . "" ;
        
        $column_names = implode(', ', $column_name);
        $query = "CREATE " . $type . " INDEX " . $index_name . " ON " .$this->prefix_table . $table_name . " (" . $column_names . ')' ;
        
         if(!$this->db_exec($query)){
            return $query; 
         }
         
        return true;
    }
    
    public function import($data, $time,$status=1,$from_task=true)
    {   
        global $table_list;
        $id_shop = $data->id_shop;
                
//        if(!isset($this->table_list))$this->table_list = array();
        $class = get_called_class();
        
        //definition
        $definition = $class::$definition;
        if(!empty($definition['table']) && !in_array($definition['table'],array('carrier'))){
            if(isset($definition['table']))
                $table_list[$definition['table']] = $definition['table'];

            if(isset($definition['lang']))
                $table_list[$definition['table'] . '_lang'] = $definition['table'] . '_lang';

            //other table
            if(isset($definition['table_link']))
            {
                foreach ($definition['table_link'] as $table_link)
                {
                    $table_list[$table_link['name']] =  $table_link['name'];

                    if($table_link['lang'])
                       $table_list[$table_link['name'] . '_lang'] =  $table_link['name'] . '_lang';
                }
            }
        }

        //Import data
        if(!empty($data) || isset($data))
        {
            $result = array();
            $theClass = new $class($this->user);
            $results = $theClass->importData($data, $time);
              
            if(is_array($results) && !empty($results)){
                foreach($results as $r){
                    if($class=="Product"){
                        $add = self::add($r,false,true,true); 
                    }else{
                    $add = self::add($r,false,true,true,false);
                    } 
                }
            }elseif(!is_array($results) ){
                if($class=="Product"){
                    if($status==1){
                        $truncate_sql = $theClass->get_truncate_products_sql($table_list, $time, $id_shop);
                        $results.=$truncate_sql;
                        $add = self::add($results,false,true,true,true,!$from_task);  
                    }else{
                        $add = self::add($results,false,true,true,false); 
                    }
                    
                }else{
                    
                    $add = self::add($results,false,true,true,true);
                }
            }

            if(isset($add) && !$add && !empty($results)){ 
                $result['error'] = sprintf('Import class %s error. ', $class) ;
            } 
            $result['table_list'] = $table_list;
            
            if($class == 'Attribute' || $class == 'Manufacturer')
            {
                $mapping = new Mapping($this->user);
                if(!$mapping->updateTable($id_shop))
                    $result['error'] = sprintf('Mapping updateTable error. ') ; 
            }
            
            if($class == 'Supplier' || $class == 'Manufacturer')
            {
                $filter = new Filter($this->user);
                    if(!$filter->updateTable($id_shop))
                        $result['error'] = sprintf('Filter updateTable error. ') ; 
            }   
            
            if($class == 'Carrier')
            {
                $Carrier = new Carrier($this->user);
                    if(!$Carrier->updateTable($id_shop))
                        $result['error'] = sprintf('Carrier updateTable error. ') ; 
            }   
            
            if($class == 'Category')
            {
                $Category = new Category($this->user);
                    if(!$Category->updateTable($id_shop))
                        $result['error'] = sprintf('Category updateTable error. ') ; 
            }
            if($status==1&&$class=="Product"){
                $this->forceUpdateOffersByProductsImport();
            }
            return $result;
        }
        
        return $result;
    }
    
    public function truncate_products($table_list, $time, $id_shop )
    {
        $result = array();
        foreach ($table_list as $table) {
            foreach ($table as $t) {
                if(!$this->delete($t, $time, $id_shop)) {
                    $result['error'] = sprintf('Delete table %s error. ', $t) ; 
                    return $result;
                }
            }
        }
        
        return true;
    }
    
    public function get_truncate_products_sql($table_list, $time, $id_shop )
    {
        $result = array();
        $sql = '';
        foreach ($table_list as $t) { 
            $sql.=$this->delete($t, $time, $id_shop,true); 
        }
        
        return $sql;
    }
    
    public function insert($table, $data, $time = NULL)
    {
        if (!$data || empty($data))
            return false;
        
        // Check if $data is a list of row
        $current = current($data);
        if (!is_array($current) || isset($current['type']))
            $data = array($data);

        $keys = array();
        $values_stringified = array();
        foreach ($data as $row_data)
        {
            $values = array();
            foreach ($row_data as $key => $value)
            {
                if (isset($keys_stringified))
                {
                    // Check if row array mapping are the same
                    if (!in_array("$key", $keys))
                         return false;
                }
                else
                    $keys[$key] = "$key";
                        
                if($value === '' || is_null($value))
                    $value = 'NULL';
                else if(is_int($value))
                    $value = (int)$value;
                else if(is_float($value))
                    $value = (float)$value;
                else
                    $value = "'{$value}'";
                 
                $values[$key] = $value;
            }  
            if(isset($time) && !empty($time) && $time != NULL)
            {
                $values['date_add'] = "'" . $time . "'";
            }
            
            $values_stringified[] = $values;
        }
        if(isset($time) && !empty($time) && $time != NULL)
        {
            $keys[] = "date_add"; 
        }

        $keys_stringified = implode(', ', $keys);
        
        foreach($values_stringified as $j => $values){
            $out=array();
            foreach($keys as $k){
                if(isset($values[$k])){
                $out[$k] = $values[$k];
                }else{
                    $out[$k] = 'NULL';
                }
            }
            $values_stringified[$j]= ' ('.implode(',',$out).') ';
        }
        $sep_no_row = 500;
        $sql='';
        if(sizeof($values_stringified)>$sep_no_row){
            $set_list = array();
            foreach($values_stringified as $i => $v){
                $set_no = floor($i/$sep_no_row);
                $set_list[$set_no][] = $v;
                unset($values_stringified[$i]);
            }
            foreach($set_list as $v){
                $sql .= 'REPLACE INTO '.$this->prefix_table .$table.' ('.$keys_stringified.') VALUES '.implode(', ', $v) . "; \n";
            }
        }else{
        $sql = 'REPLACE INTO '.$this->prefix_table .$table.' ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified) . "; \n";
        }
         
        return (string)$sql;
    }
    
    public function delete($table, $time, $id_shop,$return_sql=false)
    {
        $data = true;
        
        if (!$table || !$time)
            return false;
        
        if($table == "product"){
            $query = "UPDATE {$this->prefix_table}product SET active = 0, date_upd = '" . $time . "' WHERE date_add < '" . $time . "' AND id_shop = '" . $id_shop . "'; ";
        }else{
            if(is_array($table)){
                return '';
            }else{
                    $query =  "DELETE FROM " . $this->prefix_table .$table . " WHERE date_add < '" . $time . "' AND id_shop = '" . $id_shop . "' ; ";
            }
            
        }
        if($return_sql)return $query;
        if(!$this->db_exec($query))
            return FALSE;
        
        return $data;
    }
    
    public function update($table, $data, $where = array(), $limit = 0)
    {
        if (!$data)
            return false;

        $sql = 'UPDATE '. ObjectModel::$prefix_db_table .$table.' SET ';
        
        foreach ($data as $key => $value)
             $sql .= ($value === '' || is_null($value)) ? "$key = NULL," : "$key = '{$value}',";

        $sql = rtrim($sql, ',');
        
        if (!empty($where))
        {
            $sql .= ' WHERE ';
            
            $sql_where = array();
            foreach ($where as $key => $value)
            {
                if($value['value'] === '' || is_null($value['value']))
                    $sql_where[] = $key . ' ' . $value['operation'] . ' NULL';
                else if(is_int($value['value']))
                    $sql_where[] = $key . ' ' . $value['operation'] . ' ' . (int)$value['value'];
                else if(is_float($value['value']))
                    $sql_where[] = $key . ' ' . $value['operation'] . ' ' . (float)$value['value'];
                else
                    $sql_where[] = $key . ' ' . $value['operation'] . ' ' .  "'{$value['value']}'";
            }
            
            $sql .= implode(' AND ', $sql_where);
        }
        
        if ($limit)
                $sql .= ' LIMIT '.(int)$limit;

        return (string)$sql . '; ';
    }
        
    public function add($query_string,$transaction=true,$multi_query=true,$direct=false,$commit=true,$asyn=true)
    { 
        
        if(isset($query_string) && !empty($query_string))
            if(!$this->db_exec($query_string,$transaction,$multi_query,$direct,$commit,$asyn))
                return false;
        
        return true;
    }
    
    public static function truncate_database($user)
    {
    	$path = USERDATA_PATH . $user . '/' . ObjectModel::$database . '.db';
        $oldmask = umask(0); 
        $conn_id = $this->conn_id;
        $sql = '';
        
        if(!$conn_id)
            return false;
        
        $sql = "show tables";
        $exec = $this->conn_id->db_query_str($sql);
        
        foreach ($exec as $table)
           $sql .= "DELETE FROM " . $this->prefix_table .reset($table) . "; ";
        
        if(!$this->conn_id->db_exec($sql));
            return false;
        
        $token = new OTP($user);
        if(!$token->generate_token())
            return false;
        
        return true;
    }
    
    public function truncate_table($table, $where = null)
    {
        if(!$this->db_exec("DELETE FROM " . $this->prefix_table .$table . " " . isset($where)? $where : '' . "; "))
            return false;
        
        return true;
    }
    
    public function db_connect($location) 
    { 
        $error = '';
        $oldmask = umask(0); 
        $this->conn_id = new Db($this->user,ObjectModel::$database,false); 
        umask($oldmask);
             
    } 

    public function db_exec($query,$transaction=false,$multi_query=true,$direct=false,$commit=true,$asyn=true) 
    { 
        $error = '';
        if(!$query || empty($query) || !isset($query)||empty($this->conn_id))
            return FALSE;

        $exec = $this->conn_id->db_exec($query,$transaction,$multi_query,$direct,$commit,$asyn); 
        if(!$exec)
            return $this->conn_id->_error();
            
        return $exec;
    } 
    
    public function db_query($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
        $exec = $this->conn_id->db_sqlit_query($query);
        return $exec;
    }
    
    public function db_query_str($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
        $exec = $this->conn_id->db_query_str($query);
        return $exec;
    }
    
    public function db_num_rows($result) 
    {
        if(!isset($result) || empty($result) || !$result)
            return FALSE;
        
        $exec = $this->conn_id->db_num_rows($result);
        return $exec;
    }
    
    public function db_last_insert_rowid() 
    {
        return $this->conn_id->db_last_insert_rowid();
    }
    
    public function db_close() 
    { 
        return $this->conn_id->db_close();
    }
    
    public function db_trans_begin()
    {
        $this->conn_id->db_exec('BEGIN');
        return TRUE;
    }
    
    public function db_trans_commit()
    {
        $this->conn_id->db_exec('COMMIT');
        return TRUE;
    }
    
    public function db_table_exists($table, $no_prefix){
        return $this->conn_id->db_table_exists($table, $no_prefix);
    }
    
    public static function escape_str($str)
    {
        if(isset($str) && !empty($str))
        {
            if (is_array($str))
            {
                foreach ($str as $key => $val)
                    $str[$key] = self::escape_str($val);
                return $str;
            }
             
            $str = Db::escape_string($str);
            return $str;
        }
        
        return '';
    }
    
    public static function append_simplexml($simplexml_to, $simplexml_from)
    {
        foreach ($simplexml_from->children() as $simplexml_child)
        {
            $simplexml_temp = $simplexml_to->addChild($simplexml_child->getName(), (string) $simplexml_child);
            foreach ($simplexml_child->attributes() as $attr_key => $attr_value)
            {
                $simplexml_temp->addAttribute($attr_key, $attr_value);
            }

            ObjectModel::append_simplexml($simplexml_temp, $simplexml_child);
        }
    }
    
    public static function get_marketplace_by_id($id) {
	
	$name = '';
	$mysql_db = new ci_db_connect();  
        $query = $mysql_db->select_query("SELECT name_offer_pkg FROM offer_packages WHERE id_offer_pkg = ".(int)$id.";" );
	
        while($row = $mysql_db->fetch($query)){ 
	    $name = $row['name_offer_pkg'];
	}
	
        return $name;
    }
    
    public static function get_countries() {
	
	$countries = array();
	$mysql_db = new ci_db_connect();  
        $query = $mysql_db->select_query("SELECT * FROM offer_sub_packages WHERE iso_code IS NOT NULL;" );
	
        while($row = $mysql_db->fetch($query)){ 
	    $countries[$row['iso_code']]['id'] = $row['id_offer_sub_pkg'];
	    $countries[$row['iso_code']]['name'] = $row['title_offer_sub_pkg'];
	    $countries[$row['iso_code']]['iso_code'] = $row['iso_code'];
	}
	
        return $countries;
    }
    
    public function forceUpdateOffersByProductsImport(){
        $sql = "SELECT
	p.id_product,p.quantity,of.quantity,p.price,of.price,p.date_add,of.date_add
        FROM
                offers_product of,
                products_product p
        WHERE
                p.id_product = of.id_product
        AND p.id_shop = of.id_shop
        AND of.date_add < DATE_ADD(p.date_add, INTERVAL - 1 HOUR)
        AND (
                p.quantity <> of.quantity
                OR p.price <> of.price
                OR p.id_currency <> of.id_currency
                OR p.id_condition <> of.id_condition
                OR p.id_tax <> of.id_tax
        )";
        $list = $this->conn_id->db_query_str($sql);
        $product_id_list_all = array();
        $product_id_list = array();
        foreach($list as $l){
            $product_id_list[$l['id_product']] = $l['id_product'];
            $product_id_list_all[$l['id_product']] = $l;
        }
        unset($list);
        $sql = "update offers_product of, products_product p set 
             of.quantity = p.quantity , of.price = p.price , of.id_currency = p.id_currency ,of.id_condition = p.id_condition , of.id_tax = p.id_tax  
             where  p.id_product = of.id_product
                AND p.id_shop = of.id_shop
                AND of.date_add < DATE_ADD(p.date_add, INTERVAL - 1 HOUR)
                AND (
                        p.quantity <> of.quantity
                        OR p.price <> of.price
                        OR p.id_currency <> of.id_currency
                        OR p.id_condition <> of.id_condition
                        OR p.id_tax <> of.id_tax
                )" ;
        $this->db_exec($sql,false);
        
        $sql = "update offers_product_attribute of, products_product_attribute p set 
             of.quantity = p.quantity , of.price = p.price   
             where  p.id_product = of.id_product
                AND p.id_shop = of.id_shop and p.id_product_attribute = of.id_product_attribute
                AND of.date_add < DATE_ADD(p.date_add, INTERVAL - 1 HOUR)
                AND (
                        p.quantity <> of.quantity
                        OR p.price <> of.price 
                )" ;
        $this->db_exec($sql,false);
        if(!empty($product_id_list)){
            $sql = "update offers_flag_update set flag = 1 where id_product in ('".implode("','",$product_id_list)."')";
            $this->db_exec($sql,false);
        }

        $sql = "delete offers_product_sale
FROM
offers_product_sale
left JOIN  products_product_sale ON products_product_sale.id_product = offers_product_sale.id_product AND products_product_sale.id_product_attribute = offers_product_sale.id_product_attribute
and products_product_sale.id_shop = offers_product_sale.id_shop
and products_product_sale.reduction_type = offers_product_sale.reduction_type
WHERE
(products_product_sale.id_product is null)
";
        $this->db_exec($sql,false);

        $sql = "update offers_flag_update ofp,
(SELECT  ps.id_product ,ps.id_shop FROM products_product_sale ps
LEFT JOIN offers_product_sale fs ON fs.id_product = ps.id_product AND fs.id_product_attribute = ps.id_product_attribute AND fs.id_shop = ps.id_shop
WHERE ps.date_add > fs.date_add or fs.date_add is null GROUP BY ps.id_product,ps.id_shop) t
set ofp.flag = 1 where ofp.id_product = t.id_product and ofp.id_shop = t.id_shop";
        $this->db_exec($sql,false);

        $sql = "update offers_product_sale ofp,
(SELECT  ps.* FROM products_product_sale ps
LEFT JOIN offers_product_sale fs ON fs.id_product = ps.id_product AND fs.id_product_attribute = ps.id_product_attribute AND fs.id_shop = ps.id_shop
WHERE ps.date_add > fs.date_add or fs.date_add is null GROUP BY ps.id_product,ps.id_shop) t
set ofp.date_add = now() , ofp.reduction = t.reduction , ofp.reduction_type = t.reduction_type , ofp.date_from = t.date_from , ofp.date_to = t.date_to
where ofp.id_product = t.id_product and t.id_product_attribute = ofp.id_product_attribute and ofp.id_shop = t.id_shop";
        $this->db_exec($sql,false);



        
//        file_put_contents('/var/www/backend/assets/apps/fifo_dir/test_call.txt',date('c')."\n",FILE_APPEND);
    }
    
}
