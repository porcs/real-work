<?php

class Log extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'log',
        'primary'   => array('id_log', 'id_shop'),
        'fields'    => array(
            'id_log'            =>  array('type' => 'INTEGER', 'required' => false, 'primary_key' => true),
            'batch_id'          =>  array('type' => 'varchar', 'required' => false, 'size' => 32),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'name'              =>  array('type' => 'varchar', 'required' => false, 'size' => 128 ),
            'id_product'        =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'severity'          =>  array('type' => 'tinyint', 'required' => false, 'size' => 1),
            'error_code'        =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'message'           =>  array('type' => 'text'),
            'object_id'         =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'date_add'          =>  array('type' => 'date', 'required' => true ),
        ),
    );

    /**
     * $return Query (string)
     */
    public function insert_string($batch_id, $id_shop, $id_product, $severity, $name, $message, $time)
    {
        return "INSERT INTO " . $this->prefix_table .self::$definition['table'] . " (batch_id, id_shop, id_product, severity, name, message, date_add) "
                . "VALUES ( '" . $batch_id . "', " . $id_shop . ", " . $id_product . ", " . $severity . ", '" . $name . "', '" . $message . "', '" . $time . "'); ";
    }
    
    public function EAN_UPC_Check($code)
    {
        //first change digits to a string so that we can access individual numbers
        $digits = sprintf('%012s', substr(sprintf('%013s', $code), 0, 12));
        // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;
        // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;
        // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = $next_ten - $total_sum;
        
        return ((int) $code == (int) ($digits . $check_digit));
    }
    
    public static function ValidateSKU($SKU)
    {
        return( $SKU != null && strlen($SKU) && preg_match('/[\x00-\xFF]{1,40}/', $SKU) && preg_match('/[^ ]$/', $SKU) ) ;
    }
    
    public function updateTable($id_shop, $time)
    {   
        $query = "DELETE FROM " . $this->prefix_table .self::$definition['table'] . "  WHERE date_add < '" . $time . "' AND id_shop = '" . $id_shop . "'; ";
        //$query = "UPDATE product SET active = 0 WHERE date_add < '" . $time . "' AND id_shop = '" . $id_shop . "'; ";
        $result = $this->db_exec($query);
        
        if(isset($result) && !empty($result))
            return TRUE;
        
        return FALSE;
    }
}