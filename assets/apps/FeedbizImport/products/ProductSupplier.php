<?php

class ProductSupplier extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_supplier',
        'unique'    => array('product_sale' => array('id_product', 'id_product_attribute', 'id_supplier', 'id_shop')),
        'fields'    => array(
            'id_product'            =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_product_supplier'   =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'id_product_attribute'  =>  array('type' => 'int', 'required' => false, 'size' => 10, 'default' => 0 ),
            'id_supplier'           =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'supplier_reference'    =>  array('type' => 'varchar', 'required' => false, 'size' => 10, 'default_null' => true ),
            'id_currency'           =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
    
}