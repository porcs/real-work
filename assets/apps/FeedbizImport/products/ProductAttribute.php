<?php

class ProductAttribute extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_attribute',
        'primary'   => array('id_product','id_product_attribute', 'id_shop'),
        'unique'        => array('product_attribute' => array('id_product','id_product_attribute', 'id_shop')),
        'fields'    => array(
            'id_product'            =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'reference'             =>  array('type' => 'varchar', 'required' => false, 'size' => 64, 'default_null' => true ),
            'old_reference'         =>  array('type' => 'varchar', 'required' => false, 'size' => 64, 'default_null' => true ),
            'flag_update_ref'       => array('type' =>  'datetime', 'required' => false),
            'sku'                   =>  array('type' => 'varchar', 'required' => false, 'size' => 64, 'default_null' => true ),
            'ean13'                 =>  array('type' => 'varchar', 'required' => false, 'size' => 13, 'default_null' => true ),
            'upc'                   =>  array('type' => 'varchar', 'required' => false, 'size' => 12, 'default_null' => true ),
            'wholesale_price'       =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'price'                 =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'price_type'            =>  array('type' => 'tinyint', 'required' => false, 'default_null' => true ),
            'quantity'              =>  array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0' ),
            'weight'                =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'available_date'        =>  array('type' => 'datetime', 'required' => false, 'default_null' => true ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
    
}