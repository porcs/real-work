<?php

class Filter extends ObjectModel
{
    
    public $manufacturer = array(
        'table'     => 'exclude_manufacturer',
        'unique'    => array('exclude_manufacturer' => array('id_manufacturer', 'id_shop')),
        'fields'    => array(
            'id_manufacturer'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_mode'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'session_key'       =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default_null' => true ),
        ),
    );
    
    public $supplier = array(
        'table'     => 'exclude_supplier',
        'unique'    => array('exclude_supplier' => array('id_supplier', 'id_shop')),
        'fields'    => array(
            'id_supplier'       =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_mode'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'session_key'       =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default_null' => true ),
        ),
    );
    
    public function createFilterTable()
    {   
        $arr_data =  array($this->manufacturer, $this->supplier );
        
        

        foreach( $arr_data as $data)
        {
            $sql = array();
            $tablename = $data['table'];
            
            $data['fields'] = $data['fields'];
            $query = '';
            foreach ($data['fields'] as $key_field => $field)
            {
                $default = '';
                $size = '';
                
                if(!isset($field['required']))
                    $field['required'] = false;
                
                $required = ($field['required']) ? ' NOT NULL ' : '' ;
                $default_null =  (isset($field['default_null']) && !$field['required']) ? ' DEFAULT NULL' : '' ;

                if(!isset($field['default_null']))
                    $default =  isset($field['default']) ? " DEFAULT '" . $field['default'] . "'" : '' ;
                
                if(isset($field['size']))
                    $size = '(' . $field['size'] . ') ';
                $unique = ' ';
                if(!empty($data['unique']) && in_array($key_field,$data['unique'])){
                    $unique = ' UNIQUE';
                }
                $sql[] = $key_field . ' ' . $field['type'] .  $size  . $required . $default_null .$unique. $default;
            }
            
            $sqls = implode(', ', $sql);
            $query .= "CREATE TABLE " . $this->prefix_table .$tablename . "(" . $sqls ;
            
            //Create Unique
//            if(isset($data['unique']) && !empty($data['unique']))
//            {
//                foreach ($data['unique'] as $unique_key => $unique_value)
//                {
//                    $query .= ', CONSTRAINT "' . $unique_key . '" UNIQUE (' .implode(', ', $unique_value) .')';
//                }
//            }
            
            $query .= ")  ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci  ; " ;
            if(!$this->db_exec($query,false))
            return false;
        }
        
        
        
        return true;
    }
    
    public function updateTable($id_shop)
    {  
        $del_query = '';
        
        $del_query .= "DELETE FROM {$this->prefix_table}exclude_supplier WHERE id_supplier NOT IN (SELECT id_supplier FROM {$this->prefix_table}supplier WHERE id_shop = '" . $id_shop . "') AND id_shop = " . $id_shop . " ; ";
        $del_query .= "DELETE FROM {$this->prefix_table}exclude_manufacturer WHERE id_manufacturer NOT IN (SELECT id_manufacturer FROM {$this->prefix_table}manufacturer WHERE id_shop = '" . $id_shop . "') AND id_shop = " . $id_shop . "  ; ";
        if(!$this->db_exec($del_query))
            return FALSE;
        
        return TRUE; 
    }
    
}