<?php
    
    chdir(dirname(__FILE__)); 
    include_once (dirname(__FILE__) . '/../../libraries/newrelic_report.php');
    require_once  (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])){
        die( 'No input data!');
    }
    $user_id = $argv[1];
    $shop_id = isset($argv[2])?$argv[2]:0;
    $skip=false;
    if(isset($argv[3]) && $argv[3]=='skip'){
        $skip=true;
    }
    $uinfo = new UserConfiguration();
    
     
    $input = $uinfo->check_site_verify($user_id); 
    if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){
        die('This site was not verified.');
    }
    $shop = $input['feed_shop_info'];
    if(isset($shop['software'])){
        $shop_type = strtolower($shop['software']);
    }else{
        exit;
    }
    
//    $user_id = $input['feed_biz']['user_id'];
    $user_name = $input['feed_biz']['username'];
    $type = 'import_feed'; 
    $next_time = 1440;//mins 
    $sleep_time = 3600;//sec
    $shop_name = $shop['name'];
    require_once(dirname(__FILE__) . '/../../application/config/config.php');
    require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');
//    $batch_id = 'check exist other process running';
//    $rep = new report_process($user_name,$batch_id);
//    $rep->end_current_process();
//    if($rep->has_other_running()){
//        die();
//    }
    $uinfo->record_history($user_id,$shop_name,$type,$next_time); 
    
        
        
    $out=array();
    switch($shop_type){
        case 'prestashop': 
            $continue = false;
            do{
            $res = exec('php insert_prestashop_products.php '.rawurlencode($uinfo->feed_prepare_argv($input)));
            $out[] = json_decode($res,true);  
                if($out[0]['pass']==true){
                    $continue = false;
                }elseif(isset($out[0]['connect_error']) &&  $out[0]['connect_error']=='data_skip_page'){
                    $continue=true;
                    sleep($sleep_time);
                }else{
                    $continue = false;
                }
            }while($continue);
        
            if($out[0]['pass']==true){ 
                $res = exec('php insert_prestashop_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);  
            } 
//            echo json_encode($out); 
        break;
        case 'opencart': 
            $res = exec('php insert_opencart_products.php '.rawurlencode($uinfo->feed_prepare_argv($input)));
            $out[] = json_decode($res,true);  
            if($out[0]['pass']==true){  
                $res = exec('php insert_opencart_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);  
            }
//            echo json_encode($out);
        break;
        case 'gmerchant':
            $res = exec('php insert_gmerchant_products.php '.rawurlencode($uinfo->feed_prepare_argv($input)));
            $out = json_decode($res);
            if($out->pass){
                $res = exec('php insert_gmerchant_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
            }
        break;
        case 'magento':
            $res = exec('php insert_magento_products.php '.rawurlencode($uinfo->feed_prepare_argv($input,'products',$shop_type))); 
//            print_r($input);
//            print_r($uinfo->feed_prepare_argv($input,'products',$shop_type));
//            echo $shop_type;
            $out[] = json_decode($res,true);  
            if($out[0]['pass']==true){  
                $res = exec('php insert_magento_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers',$shop_type)));
                $out[] = json_decode($res,true);  
            }
//            echo json_encode($out);
        break;
        case 'woocommerce': 
            $res = exec('php insert_woocommerce_products.php '.rawurlencode($uinfo->feed_prepare_argv($input)));
            $out[] = json_decode($res,true);  
            if($out[0]['pass']==true){  
                $res = exec('php insert_woocommerce_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);  
            }
//            echo json_encode($out);
        break;
        case 'shopify': 
            $res = exec('php insert_shopify_products.php '.rawurlencode($uinfo->feed_prepare_argv($input)));
            $out[] = json_decode($res,true);  
            if($out[0]['pass']==true){  
                $res = exec('php insert_shopify_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);  
            }
//            echo json_encode($out);
        break;
        default://other cms
            
        break;
    }
    
    
    foreach ($out as $o) {
        if ($o['error'] == "Access Denied.") {
            $uinfo->unverify_site($user_id,$input);
        }
    }
    if($skip){exit;}
    if($shop_id==0){ echo 'no shop';exit;}
    $amazon_list = $uinfo->get_amazon_update_site($user_id,array($shop_id));
    $market_id = 2; 
    echo  "\n"; 
    foreach($amazon_list as $item){
       $ext = $item['ext'];  
//       if((int)$item['cron_delete_products']==1){
       if(!isset($item['deny']['delete'])){    
            $url = "php amazon_delete_products_cron.php {$user_id} {$user_name} {$ext} {$shop_id} {$shop_name} {$market_id}"; 
             echo $url."\n"; 
            exec($url);
       } 
//       echo $url."\n";//exec($url);
//       if((int)$item['cron_create_products']==1){
       if(!isset($item['deny']['create'])){    
            $url = "php amazon_create_cron.php {$user_id} {$user_name} {$ext} {$shop_id} {$shop_name} {$market_id}";
            echo $url."\n"; 
            exec($url);
       }
//       echo $url."\n"; //exec($url);
    }

    require_once(dirname(__FILE__) .'/../../libraries/Hooks.php');
    $hook = new Hooks();
    $hook->_call_hook('import_products', array(
            'user_name' =>$user_name,
            'id_shop' => $shop_id,
    ));
        