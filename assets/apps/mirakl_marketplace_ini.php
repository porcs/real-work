<?php

/* Update every day */
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Mirakl/classes/mirakl.config.class.php';

$update = new MiraklConfig();
$update->marketplaceInit();