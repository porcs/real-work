<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.feeds.php';

$data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
$datas = json_decode($data);

if(!isset($datas->data) || empty($datas->data))
{
    echo json_encode(array('status' => 'error', 'message' => 'Incorrect User.'));
    exit;
}
$users = json_decode($datas->data);
$debug = false;
$amazon_feeds = new AmazonFeeds($users, AmazonDatabase::CREATE, $debug);
$amazon_feeds->FeedsData();

//    $data = '{"id_customer":"1","user_name":"u00000000000001","id_country":"2","ext":".fr","countries":"France","currency":"EUR","merchant_id":"A1MS0E6JGREIS3","aws_id":"AKIAIWU4UT2E5EB2GFXA","secret_key":"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","id_mode":"1","id_shop":"1","id_lang":"2","iso_code":"fr","id_region":"eu","allow_automatic_offer_creation":false,"synchronize_quantity":true,"synchronize_price":true,"synchronization_field":"ean13","creation":true,"mode":"%7B%22exclude_manufacturer%22%3Afalse%2C%22exclude_supplier%22%3Afalse%2C%22only_selected_carrier%22%3Afalse%2C%22send_image%22%3Atrue%2C%22limiter%22%3A%2220%22%7D"}';