<?php

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.feeds.php';

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;

//https://qa.feed.biz/assets/apps/amazon_synchronize.php
//amazon_synchronize.php 26 u00000000000026 .co.uk 1 FOXCHIP 2
// debug
 if($debug) {

    $info = array(
          'user_id' => $_GET['user_id'],
          'user_name' => $_GET['user_name'],
          'ext' => $_GET['ext'],
          'id_shop' => $_GET['id_shop'],
          'shop_name' => $_GET['shop_name'],
          'id_marketplace' => $_GET['id_marketplace'],
          'next_time' => 20,
          'owner' => 'system',
          'action' => 'synchronize_offer',
      );

      // Get userdata fromdatabase
      $data = AmazonUserData::get($info);
      $users = json_decode(json_encode($data), FALSE);
      $cron = true;
      
} else {
    // ajax
    if(!isset($argv[2]) || $argv[2]==''
        || !isset($argv[3]) || $argv[3]==''
        || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
        || !isset($argv[5]) || $argv[5]==''
        || !isset($argv[6]) || $argv[6]=='')
    {
        $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
        $datas = json_decode($data);
        $users = json_decode($datas->data);
        $cron = false;
    // cron
    } else {

        // Get argv from url
        $info = array(
            'user_id' => $argv[1],
            'user_name' => $argv[2],
            'ext' => $argv[3],
            'id_shop' => $argv[4],
            'shop_name' => str_replace('_',' ',$argv[5]),
            'id_marketplace' => $argv[6],
            'next_time' => 20,
            'owner' => 'system',
            'action' => 'synchronize_offer',
        );

        // Get userdata fromdatabase
        $data = AmazonUserData::get($info);

        $data['mode'] = AmazonDatabase::SYNC;
        $object = json_decode(json_encode($data), FALSE);

        // Synchronize Amazon
        $object->creation = false;

        $users = json_decode(json_encode($data), FALSE);
        $cron = true;
    }
}

$users->creation = false;
$amazon_feeds = new AmazonFeeds($users, AmazonDatabase::SYNC, $debug, $cron);
$amazon_feeds->FeedsData();