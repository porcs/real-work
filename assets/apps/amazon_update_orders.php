<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.status.orders.php';

$debug = false;

if(!$debug)
{
    $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $datas = json_decode($data);
    $users = json_decode($datas->data);
}
else
{
    $data = '{"id_customer":"352","user_name":"u00000000000352","id_marketplace":"2","id_country":"2","ext":".fr","countries":"France","currency":"EUR","merchant_id":"A1MS0E6JGREIS3","aws_id":"AKIAIWU4UT2E5EB2GFXA","secret_key":"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","id_mode":"1","id_shop":"2","id_lang":"2","iso_code":"fr","id_region":"eu","allow_automatic_offer_creation":false,"synchronize_quantity":false,"synchronize_price":false,"synchronization_field":"ean13","creation":false,"language":"english"}';
    $users = json_decode($data);
}

$amazonOrders = new AmazonStatusOrders($users, $debug, AmazonStatusOrders::AmazonOrderFulfillment);
$amazonOrders->confirmOrder();  