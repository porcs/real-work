var restify = require('restify'), 
    userSave = require('save')('user'),
    url = require('url'),
    exec = require('child_process').exec,
    formidable = require('formidable'),
    http = require('http'),
    util = require('util'),
    jade = require('jade'),
    fs   = require('fs-extra'),
    path = require('path'),
    mysql= require('mysql');
var https_options = {
    name: 'https_feed-biz-api',
//    key: fs.readFileSync('/etc/httpd/ssl/myserver.key'),
//    certificate: fs.readFileSync('/etc/httpd/ssl/dev_feed_biz.crt')
};

var server = restify.createServer(https_options);
    
//var server = restify.createServer({ name: 'feed-biz-api' });
var currentDir = process.cwd();
var parentDir = path.resolve(process.cwd(), '../..');
var object = {};

server
  .use(restify.fullResponse())
  .use(restify.bodyParser());

server.get('/createuser/product/:data', function (req, res, next) {
    var stdout_products;
    exec('php create_user_products.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
        exec('chown -R ec2-user:www users/');
        exec('chmod -R 777 users/');
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr);
        res.send(stdout_products);
    }); 

});

server.get('/createuser/offer/:data', function (req, res, next) {
    var stdout_offers;
    exec('php create_user_offers.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        exec('chown -R ec2-user:www users/');
        exec('chmod -R 777 users/');
        stdout_offers = JSON.parse(stdout);
        console.log('Return: ', stdout_offers , ', Error: ', stderr);
        res.send(stdout_offers);
    }); 
    
});

server.get('/createuser/order/:data', function (req, res, next) {
    var stdout_offers;
    exec('php create_user_orders.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
	exec('chown -R ec2-user:www users/');
        exec('chmod -R 777 users/');
    stdout_offers = JSON.parse(stdout);
        console.log('Return: ', stdout_offers , ', Error: ', stderr);
        res.send(stdout_offers);
    }); 
    
});
server.get('/import_shop_order/:data', function (req, res, next) {
    //console.log(req.params);
    console.log('Start import shop Order');
    exec('php import_shop_order_process.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        console.log(stdout); 
    console.log('End import shop Order');
        res.send(stdout); 
    }); 
});

server.get('/import_feed/:data', function (req, res, next) {
    //console.log(req.params);
    console.log('Start import Feed');
    exec('php import_feed_process.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        console.log(stdout);
//        var product_stdout = JSON.parse(stdout);
//        console.log('Product: ', product_stdout , ', Error: ', error + stderr);
//        res.send(product_stdout); 
    console.log('End import Feed');
        res.send(stdout); 
    }); 
});
server.get('/import_product/:data', function (req, res, next) {
    //console.log(req.params);
    console.log('Start import Product');
    exec('php import_product_process.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        console.log(stdout);
//        var product_stdout = JSON.parse(stdout);
//        console.log('Product: ', product_stdout , ', Error: ', error + stderr);
//        res.send(product_stdout); 
    console.log('End import Product');
        res.send(stdout); 
    }); 
});
server.get('/import_offer/:data', function (req, res, next) {
    //console.log(req.params);
    console.log('Start import Offer');
    exec('php import_offer_process.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        console.log(stdout);
//        var product_stdout = JSON.parse(stdout);
//        console.log('Product: ', product_stdout , ', Error: ', error + stderr);
//        res.send(product_stdout); 
    console.log('End import Offer');
        res.send(stdout); 
    }); 
});
server.get('/import/Prestashop/Product/:data', function (req, res, next) {	
    exec('php insert_prestashop_products.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        var product_stdout = JSON.parse(stdout);
        console.log('Product: ', product_stdout , ', Error: ', error + stderr);
        res.send(product_stdout);
    }); 
});

server.get('/import/Prestashop/Offer/:data', function (req, res, next) {	
    exec('php insert_prestashop_offers.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        var offer_stdout = JSON.parse(stdout);
        console.log('Offer: ', offer_stdout, ', Error: ', error + stderr);
        res.send(offer_stdout);
    }); 
});

server.get('/import/GMerchant/Product/:data', function (req, res, next) {	
    exec('php insert_gmerchant_products.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        //var product_stdout = JSON.parse(stdout);
        console.log('Product: ',stdout, ', Error: ', stderr + error );
        res.send(stdout);
    }); 
});

server.get('/import/GMerchant/Offer/:data', function (req, res, next) {	
    exec('php insert_gmerchant_offers.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        //var product_stdout = JSON.parse(stdout);
        console.log('Offer: ',stdout, ', Error: ', stderr + error );
        res.send(stdout);
    }); 
});

server.get('/import/OpenCart/Product/:data', function (req, res, next) {	
    exec('php insert_opencart_products.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        //var product_stdout = JSON.parse(stdout);
        
        console.log('Product: ',stdout, ', Error: ', stderr + error );
        res.send(stdout);
    }); 
});

server.get('/import/OpenCart/Offer/:data', function (req, res, next) {	
    //console.log('- Start : ' + username.username + ', '+  new Date());
    exec('php insert_opencart_offers.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        var offer_stdout = JSON.parse(stdout);
        console.log('Offer : ' +  stdout , ', Error: ', error + stderr);
        res.send(offer_stdout);
    }); 
});

server.post('/truncate', function (req, res, next) {	
    
    var  stdout_offers;

    exec('php delete_user_offer.php ' + encodeURIComponent(JSON.stringify(req.params)) , function (error, stdout, stderr) {
        stdout_offers = JSON.parse(stdout);
        console.log('Return: ', stdout_offers);
        res.send(stdout_offers);
    }); 
});

function templateEngine(res, dir, obj) {
        jade.renderFile('tpl/' + dir, { pretty: true, globals: obj}, function (err, html) {
                if (err) throw err;
                else {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(html);
                    res.end();
                }
        });
}

function execProcess(res, tem, next) {
//        process.chdir(parentDir);
        console.log('Run....');
//        object.method = 'download';
//        var child = exec('php ebay_products_compress.php ' + encodeURIComponent(JSON.stringify(object)) , function (error, stdout, stderr) {
        exec('php ebay_products_compress.php ' + encodeURIComponent(JSON.stringify(object)) , function (error, stdout, stderr) {
//                process.chdir(currentDir);
                console.log(stdout, error, stderr);
                 
                var stdout = JSON.parse(stdout);
                if ( stdout.pass == true ) {
                        if ( stdout.output.method == 'compress' ) {
                                var output = {
                                    username    : object.username,
                                    userid      : object.userid,
                                    newzip      : stdout.output.AddFixedPriceItem,
                                    listzip     : stdout.output.RelistFixedPriceItem,
                                    editzip     : stdout.output.ReviseFixedPriceItem,
                                    endzip      : stdout.output.EndFixedPriceItem,
                                    method      : 'upload',
                                }

                                object.method   = 'upload';
                                object.batch_id = stdout.output.batch_id;
                                execProcess(res);
                        }
                        else if ( stdout.output.method == 'upload' ) {
                                var output = {
                                    username    : object.username,
                                    userid      : object.userid,
                                    AjobID      : stdout.output.AddFixedPriceItem.jobID,
                                    Aupload     : stdout.output.AddFixedPriceItem.upload,
                                    AstartUpload: stdout.output.AddFixedPriceItem.startUpload,
                                    LjobID      : stdout.output.RelistFixedPriceItem.jobID,
                                    Lupload     : stdout.output.RelistFixedPriceItem.upload,
                                    LstartUpload: stdout.output.RelistFixedPriceItem.startUpload,
                                    RjobID      : stdout.output.ReviseFixedPriceItem.jobID,
                                    Rupload     : stdout.output.ReviseFixedPriceItem.upload,
                                    RstartUpload: stdout.output.ReviseFixedPriceItem.startUpload,
                                    EjobID      : stdout.output.EndFixedPriceItem.jobID,
                                    Eupload     : stdout.output.EndFixedPriceItem.upload,
                                    EstartUpload: stdout.output.EndFixedPriceItem.startUpload,
                                    method      : 'download',
                                }
                                object.method = 'download';
                                object.batch_id = stdout.output.batch_id;
                                execProcess(res);
                        }
                        else if ( stdout.output.method == 'download' ) {
                                console.log("method download");
                                console.log(stdout);
                                var output = {
                                    username    : object.username,
                                    userid      : object.userid,
                                    newdownload : stdout.output.AddFixedPriceItem,
                                    listdownload: stdout.output.RelistFixedPriceItem,
                                    editdownload: stdout.output.ReviseFixedPriceItem,
                                    enddownload : stdout.output.EndFixedPriceItem,
                                    method      : 'download',
                                }
                                if ( object.link === 'products' || object.link === 'offers' || object.link === 'ends' || object.link === 'wizard' || object.link === 'shipment' ) {
                                        res.write('Success');
                                        res.end();
                                }
                                else {
                                        templateEngine(res, tem, output);
                                }
                                console.log(output);
                        }
                        else if ( stdout.output.method == 'compress_file' ) {
                                var output = {
                                    username    : object.username,
                                    userid      : object.userid,
                                    newzip      : stdout.output.AddFixedPriceItem,
                                    listzip     : stdout.output.RelistFixedPriceItem,
                                    editzip     : stdout.output.ReviseFixedPriceItem,
                                    endzip      : stdout.output.EndFixedPriceItem,
                                    method      : 'compress_file',
                                }

                                object.method   = 'compress_file';
                                object.batch_id = stdout.output.batch_id;
                                res.write('Success');
                                res.end();
                        }
                }
                else {
                        console.log(stdout, error, stderr, output);
                        res.write("Fail");
                        res.end();
                }
        });
}

server.get('/ebay/export/:data', function (req, res, next) {	
	req_data = JSON.parse(decodeURI(JSON.stringify(req.params)));
	req_udata = JSON.parse(req_data.data);
        object = {
            username    : req_udata.username,
            userid      : req_udata.userid,
            siteid      : req_udata.siteid,
            method      : req_udata.method,
            link        : req_udata.link,
        };
        next(req_udata.method);
});

server.get({name: 'compress_file', path: '/ebay/products/compress_file'}, function (req, res, next) {
//		console.log('start compress file');
//        object = {
//            username    : 'u00000000000035',
//            userid      : 35,
//            siteid      : 1,
//            method      : 'compress_file',
//            link        : 'products',
//        };
		console.log('call compress file');
        execProcess(res);
});

server.get({name: 'compress', path: '/ebay/products/compress'}, function (req, res, next) {
		console.log('call compress');
        execProcess(res, 'compress.jade');
});

server.get({name: 'upload', path: '/ebay/products/upload'}, function (req, res, next) {
        execProcess(res, 'upload.jade');
});

server.get({name: 'download', path: '/ebay/products/download'}, function (req, res, next) {
        execProcess(res, 'download.jade');
});

server.get({name: 'specifics', path: '/ebay/specifics'}, function (req, res, next) {
        execProcess(res, 'specifics.jade');
});

server.get({name: 'features', path: '/ebay/features'}, function (req, res, next) {
        execProcess(res, 'specifics.jade');
});

server.get({name: 'categories', path: '/ebay/categories'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});

server.get({name: 'shippings', path: '/ebay/categories'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});

server.get({name: 'shippings', path: '/ebay/categories'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});

server.get({name: 'synchronization', path: '/ebay/synchronization'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});

server.get({name: 'synchronization_matching', path: '/ebay/synchronization_matching'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});

server.get({name: 'revise_product', path: '/ebay/synchronization_matching'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});

server.get({name: 'verify_product', path: '/ebay/synchronization_matching'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});

server.get({name: 'stock', path: '/ebay/delete/stock'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});

server.get({name: 'deactivate', path: '/ebay/delete/deactivate'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});

server.get({name: 'full', path: '/ebay/delete/full'}, function (req, res, next) {
        execProcess(res, 'categories.jade');
});


function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

server.get('/run_cli/:data', function (req, res) {	  
    exec('php run_cmd.php ' + encodeURIComponent(JSON.stringify(req.params)), function(error, stdout, stderr) {
        console.log('admin run : '+decodeURI(JSON.stringify(req.params)),stdout,error,stderr);
        res.end(stdout);
    }); 
});

server.get('/get_process', function (req, res) {	 
//    var  stdout_offers; 
    var user = req.url.replace('/get_process?','');
    //console.log('REQ update process : ',user  );
    exec('php sig_update_status.php ' + user, function(error, stdout, stderr) {
        //console.log('get call '+user,stdout,error,stderr);

        if(stdout != '[]' && user!='view_system_process'&& user!='register_backend_server'&& user!='backend_update_status'){
            console.log('get call '+user,stdout,error,stderr);
        }

        res.end(stdout);
    }); 
});

server.get('/clear_process', function (req, res) {	 
//    var  stdout_offers; 
    var user = req.url.replace('/clear_process?','');
    //console.log('REQ clear process : ',user  );
    exec('php sig_update_status.php ' + user+ ' remove', function(error, stdout, stderr) {
        console.log('get remove proc '+user,stdout);
        res.end(stdout);
    }); 
});


server.post('/ebay/send/orders', function (req, res) {
    
    console.log('Start send orders.');
    var stdout_products;
    exec('php ebay_send_orders.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
//        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});


//// Start Amazon/////

server.get('/amazon/create/:data', function (req, res, next) {
    exec('chown -R ec2-user:www users/');
    exec('chmod -R 777 users/');
    console.log('Start create product on Amazon.');
    var stdout_products;
    exec('php amazon_create.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 

});

server.get('/amazon/synchronize/:data', function (req, res, next) {
    exec('chown -R ec2-user:www users/');
    exec('chmod -R 777 users/');
    console.log('Start synchronize Amazon.');
    var stdout_products;
    exec('php amazon_synchronize.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    }); 

});

server.get('/amazon/get/report/:data', function (req, res, next) {
    exec('chown -R ec2-user:www users/');
    exec('chmod -R 777 users/');
    console.log('Start get report from amazon.');
    var stdout_products;
    exec('php amazon_report_process.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    }); 

});

server.get('/amazon/send/offers/:data', function (req, res, next) {
    exec('chown -R ec2-user:www users/');
    exec('chmod -R 777 users/');
    console.log('Start synchronize amazon. ');
    var stdout_products;
    exec('php amazon_synchronize.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    }); 

});

server.get('/amazon/get/orders/:data', function (req, res, next) {
    console.log('Start get orders from amazon.');
    var stdout_products;
    exec('php amazon_get_orders.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    }); 

});

server.get('/amazon/update/orders/:data', function (req, res, next) {
    console.log('Start update orders to amazon.');
    var stdout_products;
    exec('php amazon_update_orders.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    }); 

});

server.get('/amazon/delete/products/:data', function (req, res, next) {
    console.log('Start delete products from amazon.');
    var stdout_products;
    exec('php amazon_delete_products.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    }); 

});

//// End Amazon/////


//// Start Mirakl/////

server.get('/mirakl/send/offers/:data', function (req, res, next) {
    exec('chown -R ec2-user:www users/');
    exec('chmod -R 777 users/');
    console.log('Start offers mirakl.');
    var stdout_products;
    exec('php mirakl_offers.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {

        console.log('Return: ', stdout);
        console.log('Error: ', stderr);
        console.log('Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    });
});

server.get('/mirakl/send/products/:data', function (req, res, next) {
    exec('chown -R ec2-user:www users/');
    exec('chmod -R 777 users/');
    console.log('Start products mirakl.');
    var stdout_products;
    exec('php mirakl_products.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {

        console.log('Return: ', stdout);
        console.log('Error: ', stderr);
        console.log('Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    });
});

server.get('/mirakl/delete/products/:data', function (req, res, next) {
    exec('chown -R ec2-user:www users/');
    exec('chmod -R 777 users/');
    console.log('Start delete products mirakl.');
    var stdout_products;
    exec('php mirakl_delete_products.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {

        console.log('Return: ', stdout);
        console.log('Error: ', stderr);
        console.log('Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    });
});

//server.get('/mirakl/get/orders/:data', function (req, res, next) {
//    console.log('Start get orders from mirakl.');
//    var stdout_products;
//    exec('php mirakl_get_orders.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
//        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
//	stdout_products = JSON.parse(stdout);
//        res.send(stdout_products);
//    }); 
//});

server.get('/mirakl/import/order/:data', function (req, res, next) {
    console.log('Start import orders from mirakl.');
    var stdout_products;
    exec('php mirakl_import_order.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    }); 
});

server.get('/mirakl/accept/order/:data', function (req, res, next) {
    console.log('Start accept orders from mirakl.');
    var stdout_products;
    exec('php mirakl_accept_orders.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
	stdout_products = JSON.parse(stdout);
        res.send(stdout_products);
    }); 
});

//// End Mirakl/////

//// send orders ////
server.get('/send/orders/:data', function (req, res) {
    
    console.log('Start send orders.');
    var stdout_products;
    exec('php send_orders_cron.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});

server.get('/amazon/update/validvalue', function (req, res) {
    
    console.log('Start update valid value.');
    var stdout_products;
    exec('php amazon_update_validvalue_cron.php ', function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});

server.get('/amazon/excutefile/validvalue', function (req, res) {
    
    console.log('Start excute file valid value.');
    var stdout_products;
    exec('php amazon_executefile_validvalue.php ', function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});

/*download flat file*/
server.get('/amazon/download/flatfile', function (req, res) {
    
    console.log('Start download flat file.');
    var stdout_products;
    exec('php amazon_download_flatfile.php ', function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});


/*Repricing*/
server.get('/amazon/repricing/:data', function (req, res) {    
    console.log('Start amazon repricing.');   
    var stdout_products;
    exec('php amazon_repricing.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});

/*Import Offers Options*/
server.get('/import/offers_options/:data', function (req, res) {    
    console.log('Start import offers options.');   
    var stdout_products;
    exec('php import_offers_options.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});

/*Import Product Strategies*/
server.get('/import/product_strategies/:data', function (req, res) {    
    console.log('Start import product strategies.');   
    var stdout_products;
    exec('php amazon_product_strategies_upload.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});

/*FBA ESTIMATED */
server.get('/amazon/fba/estimated/:data', function (req, res) {    
    console.log('Start fba estimated.');   
    var stdout_products;
    exec('php amazon_report_fba_fees.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout);
    }); 
});

/*FBA MANAGER*/
server.get('/amazon/fba/manager/:data', function (req, res) {    
    console.log('Start fba manager.');   
    var stdout_products;
    exec('php amazon_fba_manager.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout , ', Error: ', stderr,', Error: ', error);
        res.send(stdout);
    }); 
});

/*FBA ORDERS STATUS*/
server.get('/amazon/fba/order_status/:data', function (req, res) {    
    console.log('Start fba order status.');   
    var stdout_products;
    exec('php amazon_fba_order_status.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});

/*SHIPPING GROUP NAME*/
server.get('/amazon/shipping/group_names/:data', function (req, res) {    
    console.log('Start Update Groups Names from Amazon.');   
    var stdout_products;
    exec('php amazon_get_shipping_group.php ' + encodeURIComponent(JSON.stringify(req.params)), function (error, stdout, stderr) {
	stdout_products = JSON.parse(stdout);
        console.log('Return: ', stdout_products , ', Error: ', stderr,', Error: ', error);
        res.send(stdout_products);
    }); 
});

server.listen(3001, function () {
  console.log('%s listening at %s', server.name, server.url)
});
