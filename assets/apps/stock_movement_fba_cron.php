<?php
define('BASEPATH', '');
chdir(dirname(__FILE__)); 

require_once dirname(__FILE__) . '/../../application/libraries/UserInfo/configuration.php';
require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.order.php';
require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');
require_once(dirname(__FILE__) . '/../../application/libraries/tools.php');
require_once dirname(__FILE__) . '/../../application/libraries/FeedBiz/config/orders.php';
require_once(dirname(__FILE__) . '/../../application/libraries/FeedBiz/config/stock.php');

$debug = isset($_GET['debug']) ? (bool) $_GET['debug'] : false ;

// php stock_movement_fba_cron.php 9 1 2

// Debug by user id $ id shop
if($debug) {

    $user_id = $_GET['user_id'];
    $id_shop = $_GET['id_shop'];
    $site = $_GET['site']; //2
    
} else {
    
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1]) 
        || !isset($argv[2]) || $argv[2]=='' || !is_numeric($argv[2])
        || !isset($argv[3]) || $argv[3]=='' || !is_numeric($argv[3])){
	die( 'No input data!');
    }

    $user_id  = $argv[1];
    $id_shop  = $argv[2];
    $site     = $argv[3]; //2
    
}

$config = new UserConfiguration(); 
$input = $config->check_site_verify($user_id); 
$shop = $input['feed_shop_info']; 

if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){ 
    die();
}

$user_name = $input['user_name'];
$id_user = $input['user_id'];
$type = 'import_stock_movement_fba';
$next_time = 20; //mins
$shop['name'] = empty($shop['name'])?'Shop name':$shop['name'];
$shop_name = str_replace('_',' ', $shop['name']); 
    
$config->record_history($user_id,$shop_name,$type,$next_time);

$data['id_user'] = $id_user;
$data['user_name'] = $user_name;
$data['id_shop'] = $id_shop;
$data['shop_name']= $shop_name;

if(!isset($data) || empty($data) ){
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {

    $id_user = $data['id_user'];
    $id_shop = $data['id_shop'];    
    $user_name = $data['user_name'];
    $shop = $data['shop_name'];
    
    $id_marketplace = Amazon_Order::$MarketplaceID;
    $marketplace = Amazon_Order::Amazon;
    
    $history = array();
    $batch_id = uniqid();
    $date = date('Y-m-d H:i:s');
    $loop = 1; 
    
    $userdata = $config->getShopInfo($id_user);
    $Language = $config->getUserDefaultLaguage($id_user);    

    // Load lange file
    load('orders', $Language);
    
    if (isset($userdata) && !empty($userdata)) {         
                        
        $process_title = l('Update stock movement fba to ') . ' ' . $shop . '...';
        $process_type =  'Stock movement FBA';
	
	if(!$debug) {
	    $proc_rep = new report_process($user_name, $batch_id, $process_title, false, true);
	    $proc_rep->set_process_type($process_type); 
	}

        $Orders = new Orders($user_name, null, $debug);
        $OrderItems = new OrderItems($user_name);
        $token = $config->getUserCode($id_user);


        require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.database.php');
        require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.fba.stock.php');

        $amazonDb = new AmazonDatabase($user_name, $debug);
        $stock_movement = new StockMovement($user_name);

        $limit = null;
        if($debug){
            $limit = 2 ;
        }
        
        $list_sku = $amazonDb->get_fba_flag($id_shop, $site, AmazonFBAStock::PROCESS_KEY, AmazonFBAStock::FBA_STOCK_SYNCH, null, 1, $limit);

        if($loop == 1 && !$debug) {
            $proc_rep->set_max_min_task(sizeof($list_sku), 1);
        }
        
        $list = array();
        $offers_stock_mvt_logs = $updatedStock = '';

        foreach ($list_sku as $k => $sku) {

            if(!$debug)
                $proc_rep->set_running_task($loop);

            $stockmovement_url = $userdata['url_stockmovementfba'];

            // Determine Software type and prepare URL
            $conj = strpos($stockmovement_url, '?') !== FALSE ? '&' : '?';
            $preparedURL = $stockmovement_url . $conj . sprintf('fbtoken=%s&fbproduct=%s&fbcombination=%s&fbstock_fba=%s&fbstock=%s&fbsku=%s',
                            $token, (int) $sku['id_product'], (int) $sku['id_product_attribute'], (int) $sku['quantity_fba'], (int) $sku['quantity'], $sku['sku']);
            
            if(function_exists('fbiz_get_contents')){
                $return = fbiz_get_contents($preparedURL, array('curl_method'=>1));
            }else{
                $return = @file_get_contents($preparedURL);
            }

            if(!$return || empty($return)) {

                $message = 'Update stock movement fba fail, can not connect to Shop.';

            } else {

                if(isset($return) && $return) {

                    $return_dom = new SimpleXMLElement($return);

                    $error_code = isset($return_dom->Status->Code) ? (int)$return_dom->Status->Code : '-1' ;

                    if(isset($return_dom->Status->Code) && $return_dom->Status->Code == 1)
                    {
                        if(isset($return_dom->StockFlag->Item))
                        {
                            foreach ($return_dom->StockFlag->Item as $items)
                            {
                                //foreach ($stock_items as $items)
                                //{
                                    $flag = isset($items['Flagged']) && $items['Flagged'] ? 0 : 1 ;
                                    $id_product = (int)$items['ProductId'];
                                    $id_product_attribute = (int)$items['ProductAttributeId'];
                                    $sku = strval($items['Reference']);
                                    $stock_fba = strval($items['StockFBA']);
                                    $stock = strval($items['Stock']);

                                    // set flagged
                                    $update_flag = array(
                                        'flag_stock'        => $flag,
                                        'date_upd'		=> $date,
                                    );

                                    $where_flag = array(
                                        'id_product'		=> array('value' => $id_product, 'operation' => '='),
                                        'id_product_attribute'	=> array('value' => $id_product_attribute, 'operation' => '='),
                                        'sku'			=> array('value' => $sku, 'operation' => '='),
                                        'id_shop'                   => array('value' => $id_shop, 'operation' => '='),
                                        'process'                   => array('value' => AmazonFBAStock::PROCESS_KEY, 'operation' => '='),
                                        'type'			=> array('value' => AmazonFBAStock::FBA_STOCK_SYNCH, 'operation' => '='),
                                    );

                                    $updatedStock = $amazonDb->update_string('amazon_fba_flag', $update_flag, $where_flag);
                                    $amazonDb->exec_query($updatedStock);

                                    if (isset($stock_fba) && $stock_fba == 0) {
                                        $message = Amazon_Tools::l('Product became out of stock');

                                    } else if(isset($stock_fba) && $stock_fba <> 0) {
                                        $message = Amazon_Tools::l('Product in stock (FBA)');
                                    }

                                    if($flag) {
                                        //AmazonFBAStock::$log[$sku][$site]['message'] = sprintf('%s - %s (%d)', $message, Amazon_Tools::l('Stock Updated'), $stock_fba);
                                        // add log stock movement
                                        $data_offer = $sku;
                                        $data_offer['quantity'] = $stock_fba;
                                        $marketplace = strtolower(AmazonFBAStock::$tag);
                                        $stock_movement->stockMovementProcess($data_offer, $id_shop, $date, 'fba_manager', $marketplace);
                                    } else {
                                        //AmazonFBAStock::$log[$sku][$site]['message'] = sprintf('%s(%s) - error code : %s', Amazon_Tools::l('Stock Update FAILED.'), $return_dom->Status->Code, $sku);
                                    }

                                    // keep log
                                    $data_insert = array(
                                        'id_shop' => $id_shop,
                                        'id_marketplace' => $id_marketplace,
                                        'site' => $site,
                                        'error_code' => $error_code,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => $id_product_attribute,
                                        'message' => $message,
                                        'quantity' => (($stock - $stock_fba) * -1),
                                        'action_type' => 'fba_manager - synchronize' ,
                                        'date_add' => date('Y-m-d H:i:s')
                                    );

                                    $offers_stock_mvt_logs = $amazonDb->insert_string('offers_stock_mvt_logs', $data_insert, true);
                                    $amazonDb->exec_query($offers_stock_mvt_logs);

                                    if($debug){
                                        var_dump($updatedStock);
                                        var_dump($offers_stock_mvt_logs);
                                    }
                                }
                            //}
                        }
                    }
                }
            }

        }

        if(!$debug) {

            /*if(strlen($updatedStock)){
                $amazonDb->exec_query($updatedStock);
            }
            if(strlen($offers_stock_mvt_logs)){
                $amazonDb->exec_query($offers_stock_mvt_logs);
            }*/
            $proc_rep->set_status_msg(l('COMPLETED'));
        } else {
            var_dump($updatedStock);
            var_dump($offers_stock_mvt_logs);
            echo '<pre>COMPLETED</pre>' ;
        }

	if(!$debug) 
	    $proc_rep->finish_task();
    
    } else {
        
        echo json_encode(array('status' => 'error', 'process' => 'Export orders', 'message' => 'Wrong user.'));
        die();
        
    }
    
}