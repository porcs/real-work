<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

class FbaOrderTasks {
    
    const CREATE = 1;

    public function __construct( $user ) {
	$this->user = $user ; 	
    }
    
    public function Dispatch($action, $id_shop, $site, $id_order, $force_send=false){
		
	switch ($action) {
	    case FbaOrderTasks::CREATE :	
		$file = dirname(__FILE__) . "/amazon_fba_order_create.php";		
		$url = "php {$file} {$this->user} {$id_shop} {$site} {$id_order} {$force_send}"; 		
		exec($url);		
		break;	    
	}
		
    }
    
}