<?php 
    define('BASEPATH', '');
    chdir(dirname(__FILE__)); 
    require_once  (dirname(__FILE__).'/../../libraries/ci_db_connect.php');
    require_once  (dirname(__FILE__). "/../../application/libraries/Ebay/ebayexport.php"); 
    require_once  (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');


    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1]) 
            || !isset($argv[2]) || $argv[2]=='' 
            || !isset($argv[3]) || $argv[3]=='' || !is_numeric($argv[3] )
            || !isset($argv[4]) || $argv[4]==''
            || !isset($argv[5]) || $argv[5]==''
            || !isset($argv[6]) || $argv[6]==''){
 
        die( 'No input data!');
    }
    
    $user_id = $argv[1];
    $user_name =  $argv[2];
    $site_id = $argv[3];
    $site_domain= $argv[4];
    $shop_name = str_replace('_',' ',$argv[5]);
    $id_marketplace = $argv[6];
    $next_time = 3600 * 24;
    $ext = str_replace('ebay','', $site_domain);
    
        
    $uinfo = new UserConfiguration(); 
    $input = $uinfo->check_site_verify($user_id); 

    $shop = $input['feed_shop_info']; 

    if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){
        die();
    }
    
    require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');
    
     
 
    $type = "export_{$site_domain}_products";
    $uinfo->record_history($user_id,$shop_name,$type,$next_time); 
    
    
    $cron = true;
    $fail               = 1;
    $product            = new ebayexport(array($user_name, $user_id, $site_id, $cron));
    $batch_id           = !empty($product->batch_id) ? $product->batch_id : 'check exist ebay running';
    $rep = new report_process($user_name, $batch_id, "Export eBay ".$site_domain." product", true, true, true, 'export_products_site_'.$site_id);
    if($rep->has_other_running('export_products_site_'.$site_id)){
        die();
    }
    
    $rep->set_process_type($type); 
    $rep->set_priority(1); 
    $rep->set_shop_name($shop_name); 
    $rep->set_max_min_task(0, 100); 
    $result             = $product->export_compress();
    $result             = $product->export_bulk();
    $result             = $product->export_download($rep);
    
    ( !empty($rep) ) ? $rep->finish_task($fail) : $rep->finish_task(0);
