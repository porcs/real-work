<?php if( !defined('BASEPATH') ) define('BASEPATH', '');

chdir(dirname(__FILE__));
require_once(dirname(__FILE__) . '/../../application/libraries/Marketplaces/classes/marketplaces.configuration.php');
require_once(dirname(__FILE__) . '/../../application/libraries/GoogleShopping/classes/google.config.php');
require_once(dirname(__FILE__) . '/../../application/libraries/GoogleShopping/classes/google.install.php');
require_once(dirname(__FILE__) . '/../../libraries/ci_db_connect.php');
require_once(dirname(__FILE__) . '/../../application/libraries/UserInfo/configuration.php');
require_once(dirname(__FILE__) . '/../../application/libraries/tools.php');//for lang
require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');

##http://ndb.feed.dev/assets/apps/google_shopping_get_categories_cron.php
//
//$debug = isset($_GET['debug']) ? (bool) $_GET['debug'] : false ;
//if($debug){
//    $user_id = $_GET['user_id'];
//    $user_name = $_GET['user_name'];
//} else {
//    if ( !isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1]) ) {
//        die('No input data!');
//    }
//    $user_id = $argv[1];
//    $user_name =  $argv[2];
//}
//
//$uinfo = new UserConfiguration();
//$Language = $uinfo->getUserDefaultLaguage($user_id);//lang
//load(strtolower(_MARKETPLACE_NAME_), $Language);//lang
//$input = $uinfo->check_site_verify($user_id);
//
//if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){
//    die('Shop not verified!');
//}
//
//$shop = $input['feed_shop_info'];
//$next_time = 1440;
//$type = "get_all_categories";
//
//$feedbiz = new FeedBiz(array($user_name));
//$DefaultShop = $feedbiz->getDefaultShop($user_name);
//if ( !empty($DefaultShop) ) {
//    $id_shop = $DefaultShop['id_shop'];
//    $shop_name = $DefaultShop['name'];
//}
//
//if(!$debug) {
//    $uinfo->record_history($user_id, $shop_name, $type, $next_time);
//    $batch_id = uniqid();
//    $rep = new report_process($user_name, $batch_id, l("Get categories"), true, true, true, $type);
//    if($rep->has_other_running($type)){
//        die('Has other running!');
//    }
//}

$google_feed = new GoogleInstall();
$google_feed->getCategoryLanguages();
//
//if(isset($rep)) {
//    $rep->set_process_type($type);
//    $rep->set_priority(1);
//    $rep->set_shop_name($shop_name);
//    $fail = 1;
//    ( !empty($rep) ) ? $rep->finish_task($fail) : $rep->finish_task(0);
//}