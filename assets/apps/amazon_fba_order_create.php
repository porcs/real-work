<?php

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.fba.orders.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;
   
if($debug) {
    
    // Get argv from url
    $info = array(
	'user_name' => $_GET['user_name'],//'u00000000000085',
	'id_shop' => $_GET['id_shop'],
	'site' => $_GET['site'], 
	'next_time' => 1440,
	'owner' => 'system',
	'action' => 'fba_order',
    );

    $id_order = $_GET['id_order'];
    $force_send = (isset($_GET['force_send'])) ? (bool) $_GET['force_send'] : false;
    
} else {
    
    if(!isset($argv[1]) || $argv[1]=='' || !isset($argv[2]) || $argv[2]=='' || !isset($argv[3]) || $argv[3]=='' ){
	die( 'No input data!');
    }

    // Get argv from url
    $info = array(
	'user_name' => $argv[1],
	'id_shop' => $argv[2],
	'site' => $argv[3], 
	'next_time' => 1440,
	'owner' => 'system',
	'action' => 'fba_order',
    );
    
    $id_order = (isset($argv[4]) && !$argv[4]=='') ? $argv[4] : null;
    $force_send = (isset($argv[5]) && !$argv[5]=='') ? (bool) $argv[5] : false;
}

if( (!isset($info['user_name']) || empty($info['user_name'])) || (!isset($info['site']) || empty($info['site'])) || (!isset($info['id_shop']) || empty($info['id_shop'])) )
{
    $info['ext'] =  $info['site'];
    $info['countries'] = $info['site'];
    $info['shop_name'] = $info['user_name'];
    
    // Set history log
    AmazonHistoryLog::set($info);
     
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided, Missing user_name, site, id_shop.') ) ;
    echo $json ; exit;
} 

// 1. find Master Platform of region by  site
$master_platform = AmazonUserData::getMasterFBA($info['user_name'], $info['site'], $info['id_shop']);

if((empty($master_platform)) || !$master_platform)
{
    $info['ext'] =  $info['site'];
    $info['countries'] = $info['site'];
    $info['shop_name'] = $info['user_name'];
    
    // Set history log
    AmazonHistoryLog::set($info);
     
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Missing Amazon Master Platform.') ) ;
    echo $json ; exit;
} 

$info['site'] = $master_platform['id_country'];
$info['ext'] = $master_platform['ext'];
$info['region'] = $master_platform['region'];

// Get userdata fromdatabase
$data = AmazonUserData::get($info, false, true);

if( (!isset($data) || empty($data)) ){
    
    $info['ext'] =  $info['site'];
    $info['countries'] = $info['site'];
    $info['shop_name'] = $info['user_name'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    /*// Only if FBA MultiChannel is active
    if (!isset($data['fba_multichannel']) || !(bool)$data['fba_multichannel'])
    {
	$json = json_encode( array('error' => false, 'pass' => true, 'output' => 'fba multichannel is not allow.') ) ;
	echo $json ; exit;
    }
    // If not force send and Only if FBA MultiChannel auto is active
    if (!$force_send && !isset($data['fba_multichannel_auto']) || !(bool)$data['fba_multichannel_auto'])
    {
	$json = json_encode( array('error' => false, 'pass' => true, 'output' => 'fba multichannel auto is not allow.') ) ;
	echo $json ; exit;
    }*/  
        
    $options = array();
    
    if($force_send)
	$options['force_send'] = true;
    
    $info['shop_name'] = $data['shop_name'];
    $info['countries'] = $data['countries'];
    $info['ext'] =  $data['ext'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $users = json_decode(json_encode($data), false);
    $cron = false; 
    
    $amazon_stocks = new AmazonFBAOrder($users, $cron, $debug);
    $amazon_stocks->Dispatch(AmazonFBAOrder::fba_create, $id_order, $options);
    
}