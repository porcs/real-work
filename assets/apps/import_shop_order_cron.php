<?php

define('BASEPATH', '');
chdir(dirname(__FILE__)); 
require_once  (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
require_once  (dirname(__FILE__).'/../../libraries/newrelic_report.php');

if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])){
    die( 'No input data!');
}

$user_id = $argv[1];
$uinfo = new UserConfiguration(); 
$input = $uinfo->check_site_verify($user_id); 
  
$shop = $input['feed_shop_info']; 

if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){
 
    die();
}else if( !isset($shop['url']['shippedorders'])){
    die();
}

$shippedorder_url = $shop['url']['shippedorders'];
$conj = strpos($shippedorder_url, '?') !== FALSE ? '&' : '?'; 
$input['xml_url'] =$shippedorder_url . $conj . 'fbtoken=' . $input['feed_token'];

if(!isset($input['xml_url']) || empty($input['xml_url']) || !isset($input['user_name']) || empty($input['user_name'])){
    die('Data input is broken');
}

$user_name = $input['user_name'];
$id_user = $input['user_id'];
$type = 'import_order'; 
$next_time = 60; //mins
$shop_name = $shop['name']; 
    
$uinfo->record_history($user_id,$shop_name,$type,$next_time);

include (dirname(__FILE__). "/../../application/config/config.php");
require_once DIR_SERVER . '/application/libraries/FeedBiz_Orders.php';
require_once DIR_SERVER . '/application/libraries/Ebay/ObjectBiz.php';
require_once DIR_SERVER . '/application/libraries/Ebay/ebayshipment.php';        
require_once DIR_SERVER . '/application/libraries/Amazon/functions/amazon.status.orders.php';
require_once (dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php'); 
$ebayObjectBiz = new ObjectBiz(array($user_name));
$feedBizOrder = new FeedBiz_Orders(array($user_name));
$ebayCarriers = $ebayObjectBiz->getAllCarriers();
//INITIAL END

//retrieve setting + connector
$orders = array();
$order_ids = array();
//echo "url ".$input['xml_url']."\n";
if(function_exists('fbiz_get_contents')){
    $xml_string = fbiz_get_contents($input['xml_url']); 
}else{
    $xml_string = file_get_contents($input['xml_url']); 
}


// validate xml
libxml_use_internal_errors(true);
$xml_doc = simplexml_load_string($xml_string);
$shop_id_list = array();

if(empty($xml_doc)){
    sleep(5);
//    echo "\n ---Reload XML--- \n";
    if(function_exists('fbiz_get_contents')){
        $xml_string = fbiz_get_contents($input['xml_url']);  
    }else{
        $xml_string = file_get_contents($input['xml_url']); 
    }
    $xml_doc = simplexml_load_string($xml_string);
}


// 1. LOAD DOM AND UPDATE ORDER TRACKING
if ($xml_doc) {
    
    $xml_dom = new SimpleXMLElement($xml_string);
    
    foreach ($xml_dom->Orders->Order as $orderDOM) {
        $seller_id = $orderDOM->attributes()->{'ID'};
        if ( empty($orderDOM->MPOrderID) && !empty($seller_id)) {
            $seller_ids[] = strval($seller_id);
            $seller_datas[strval($seller_id)] = array(
                'CarrierID' => strval($orderDOM->CarrierID),
                'CarrierName' => strval($orderDOM->CarrierName),
                'ShippingNumber' => strval($orderDOM->ShippingNumber),
                'ShippingDate' => strval($orderDOM->ShippingDate)
            );
        }
        $orders [strval($orderDOM->MPOrderID)] = array(
            'MPOrderID' => strval($orderDOM->MPOrderID),
            'CarrierID' => strval($orderDOM->CarrierID),
            'CarrierName' => strval($orderDOM->CarrierName),
            'ShippingNumber' => strval($orderDOM->ShippingNumber),
            'ShippingDate' => strval($orderDOM->ShippingDate)
        );
        $order_ids [] = strval($orderDOM->MPOrderID);
    }

    if ( !empty($seller_ids)) {
        $resultSellerIDs = $feedBizOrder->findOrdersFromSellerOrderId(
            $user_name, $seller_ids
        );

        foreach ( $resultSellerIDs as $sellerID => $order ) {
            $seller_datas[$sellerID]['MPOrderID'] = $order;

            $orders[$order] = $seller_datas[$sellerID];
        }
        unset($seller_datas);
    }
//echo "order list : ".sizeof($orders)."\n";

    if (!empty($orders)) {    
        
        $ordersNoTracking = $feedBizOrder->getOrdersNoTracking($user_name, $order_ids);
        $feedBizOrder->updateOrderTracking($user_name, $orders);
        $current = new DateTime();
        $logs = array();
        foreach($ordersNoTracking as $oid=>$orderNoTracking){
            
            $sale_channel = strtolower($orderNoTracking['sales_channel']);
            $id_marketplace = 0;
            
            if(strpos($sale_channel, 'ebay') !== FALSE)
                    $id_marketplace = 3;
            
            if(strpos($sale_channel, 'amazon') !== FALSE)
                    $id_marketplace = 2;
            if(isset($orders[$oid]))
            $logs[] = array('batch_id'=>$orderNoTracking['id_marketplace_order_ref'],
                            'request_id'=>$orderNoTracking['id_orders'],
                            'error_code'=>0,
                            'message'=>'Update tracking number: '.$orders[$oid]['ShippingNumber'].' Shipped Date: '.$orders[$oid]['ShippingDate'],
                            'date_add'=>$current->format('Y-m-d H:i:s'),
                            'id_shop'=>$orderNoTracking['id_shop'],
                            'id_marketplace'=>$id_marketplace,
                            'site'=>$orderNoTracking['site']
            );
        }
        $feedBizOrder->insertLog($user_name, $logs);
    }
}else{
    echo "Error xml :\n";
    print_r($xml_string);
}

$row = $uinfo->check_ebay_config($id_user);
//$sql = "select id_customer from configuration where id_customer ='$id_user' and name in ('EBAY_TOKEN' ,'EBAY_USER') and value <> '' ";
//$row = $mysql_con->count_rows($mysql_con->select_query($sql));

// 2. LIST ORDER id_shop TO UPDATE 
$raworders = $feedBizOrder->getOrdersForShip($user_name);
foreach ($raworders as $channel=>$sites) { 
    foreach($sites as $site_id => $site_orders){
        foreach($site_orders as $order){ 
            $shop_id_list[] = $order['id_shop'] ;
        }
    }
}   

// 3. CHECK FOR EBAY UPDATE
if($row == 2){
   //MP
    $current = new DateTime();
    $logs = array();
    $done = array();
    $orderMPMapping = array();
    $shipments = array();
    
    foreach ($raworders as $channel => $sites) {
        //EBAY
        if (strpos(strtolower($channel), 'ebay') !== FALSE) {
            foreach ($sites as $site_id => $site_orders) {
                foreach ($site_orders as $order) {
                    $id_shop = $order['id_shop'];
                    $id_carrier = $order['id_carrier'];
                    $shipments[] = array('OrderID' => $order['id_marketplace_order_ref'],
                        //'OrderLineItemID' => $order['id_marketplace_order_ref'],
                        'ShipmentTrackingNumber' => $order['tracking_number'],
                        'ShippedTime' => $order['shipping_date'],
                        'ShippingCarrierUsed' => isset($ebayCarriers[$id_shop]) && isset($ebayCarriers[$id_shop][$id_carrier]) ? 
                                                $ebayCarriers[$id_shop][$id_carrier]['name'] : 'Feedbiz'
                    );
                    $orderMPMapping[$order['id_marketplace_order_ref']] = $order;
                }

                //API
                if ($shipments) {
                    $fail               = 1;
                    $ebayshipment = new ebayshipment(array($user_name, $id_user, $site_id));
                    $batch_id = !empty($ebayshipment->batch_id) ? $ebayshipment->batch_id : uniqid();
                    $rep = new report_process($user_name, $batch_id, "Import orders eBay", true, true, true, 'import_orders_eBay_'.$site_id);
                    if($rep->has_other_running('import_orders_eBay_'.$site_id)){
                         die();
                    }
                    $rep->set_process_type('import_orders_eBay_'.$site_id); 
                    $rep->set_priority(1); 
                    $rep->set_shop_name($shop_name); 
                    $ebayshipment->export_shipment($shipments);
                    $ebayshipment->export_bulk();
                    $rep->set_max_min_task(0, 100); 
                    $ebayshipment->export_download($rep);
                    $rs = !empty($ebayshipment->responsing) ? $ebayshipment->responsing : array(); 
                    
                    ( !empty($rep) ) ? $rep->finish_task($fail) : $rep->finish_task(0);

                    //Response
                    if ($rs) {
                        foreach ($rs as $orderResponse) {
                            $Ack = strval($orderResponse->Ack);
                            $OrderID = strval($orderResponse->OrderID);
                            $raworder = isset($orderMPMapping[$OrderID]) ? $orderMPMapping[$OrderID] : null;
                            $id_shop = $raworder['id_shop'];
                            $id_carrier = $raworder['id_carrier'];
                            $carrier_name = isset($ebayCarriers[$id_shop]) && isset($ebayCarriers[$id_shop][$id_carrier]) ? $ebayCarriers[$id_shop][$id_carrier]['name'] : 'Feedbiz';

                            $sale_channel = strtolower($raworder['sales_channel']);
                            $id_marketplace = 0;
                            if (strpos($sale_channel, 'ebay') !== FALSE)
                                $id_marketplace = 3;
                            if (strpos($sale_channel, 'amazon') !== FALSE)
                                $id_marketplace = 2;

                            if ($raworder) {
                                if (strtolower($Ack) == 'success' || strtolower($Ack) == 'warning') {
                                    $logs[] = array('batch_id' => $OrderID,
                                        'request_id' => intval($orderMPMapping[$OrderID]['id_orders']),
                                        'error_code' => 0,
                                        'message' => 'Ebay SetShipmentTrackingInfo: ' . $raworder['tracking_number'] . ' Shipped Date: ' . $raworder['shipping_date'],
                                        'date_add' => $current->format('Y-m-d H:i:s'),
                                        'id_shop' => $raworder['id_shop'],
                                        'id_marketplace' => $id_marketplace,
                                        'site' => $raworder['site']
                                    );
                                    $done[] = intval($orderMPMapping[$OrderID]['id_orders']);
                                } else {
                                    $error_messages = array();
                                    foreach ($orderResponse->Errors as $errDOM) {
                                        $error_messages[] = strval($errDOM->ErrorCode) . ': ' . strval($errDOM->LongMessage);
                                    }
                                    $logs[] = array('batch_id' => $OrderID,
                                        'request_id' => intval($orderMPMapping[$OrderID]['id_orders']),
                                        'error_code' => 1,
                                        'message' => 'Ebay SetShipmentTrackingInfo: ' . implode('<br/>', $error_messages),
                                        'date_add' => $current->format('Y-m-d H:i:s'),
                                        'id_shop' => $raworder['id_shop'],
                                        'id_marketplace' => $id_marketplace,
                                        'site' => $raworder['site']
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //SQLITE
    $feedBizOrder->insertLog($user_name, $logs);
    $feedBizOrder->updateShippedStatus($user_name, $done);
    //SQLITE END
}

// 4. CHECK FOR AMAZON UPDATE
//$shop_list_txt = implode("','",$shop_id_list) ;
//$sql = "select ac.* ,u.* from amazon_configuration ac   ,users u ,offer_price_packages op ,user_package_details up where "
//        . "    ac.active = '1'    and  u.id = ac.id_customer and u.id = '$id_user' and ac.id_shop in ('".$shop_list_txt."') "
//        . " and op.ext_offer_sub_pkg = ac.ext and up.id_users = u.id and up.id_package = op.id_offer_price_pkg "
//        . " group by ac.id_customer,id_country,id_shop ";

require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');
$amazon_site = $uinfo->get_amazon_update_site($id_user, $shop_id_list);
//$q = $mysql_con->select_query($sql);
$debug = false; //true;
//echo "shop list\n";
//print_r($shop_id_list);
//echo "amazon site\n";
//print_r($amazon_site);
//while($row = $mysql_con->fetch($q)){ //loop for each amazon site
foreach($amazon_site as $row){
    if(isset($item['deny']['update shop'])){    
        continue;
    }
        $info = array(
        'user_id' => $id_user,
        'user_name' => $row['user_name'],
        'ext' => $row['ext'],
        'id_shop' => $row['id_shop'],
        'shop_name' => str_replace('_',' ',$shop_name),
        'id_marketplace' => 2,
        'next_time' => 60,
        'countries' => $row['countries'],
        'owner' => 'system',
        'action' => 'update_order',
        );

        $data = AmazonUserData::get($info); 
        AmazonHistoryLog::set($info); 
        if(!isset($data) || empty($data) ){
            continue;
        }else{
            $object = json_decode(json_encode($data), FALSE);

            $amazonOrders = new AmazonStatusOrders($object, $debug, AmazonStatusOrders::AmazonOrderFulfillment);
            $amazonOrders->confirmOrder();  
        }

}
//echo "\n-----------end shop order cron-------------\n";
