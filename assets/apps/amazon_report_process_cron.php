<?php

require_once dirname(__FILE__) . '/../../libraries/newrelic_report.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');
require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.reports.php';

$debug = false;
$cron = true; 

if(!$debug) {
    
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]=='' 
	|| !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
	|| !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]=='' ){
	die( 'No input data!');
    }

    // Get argv from url
    $info = array(
	'user_id' => $argv[1],
	'user_name' => $argv[2],
	'ext' => $argv[3],
	'id_shop' => $argv[4],
	'shop_name' => str_replace('_',' ',$argv[5]),
	'id_marketplace' => $argv[6],
	'next_time' => 60*24,
	'owner' => 'system',
	'action' => 'get_inventory_report'
    );
    
} else {
    
    // debug 
    $info = array(
        'user_id' => 328,
        'user_name' => 'Praew',
        'ext' => '.fr',
        'id_shop' => 1,
        'shop_name' => str_replace('_',' ','Chaussmoi'),
        'id_marketplace' => 2,
        'next_time' => 60,
        'owner' => 'system',
        'action' => 'get_inventory_report'
    );
}

// Get userdata fromdatabase
$data = AmazonUserData::get($info);

// Set history log
$info['countries'] = $data['countries'];
AmazonHistoryLog::set($info);

if(!isset($data) || empty($data) ){
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {

    $object = json_decode(json_encode($data), FALSE);
    
    $amazon_reports = new AmazonReports($object, $debug, $cron);
    $amazon_reports->getReport();
    
}