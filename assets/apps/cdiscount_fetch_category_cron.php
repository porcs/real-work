<?php

/* per customer */
/* once a week */

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);

require_once dirname(__FILE__) . '/../../application/libraries/CDiscount/functions/categories_load.php';

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;

if($debug) {

    $info = array(
	'user_id' => $_GET['id_customer'],
	'user_name' => $_GET['user_name'],	
	'next_time' => 10080, // 1 week
	'owner' => 'system',
	'action' => 'cdiscount_fetch_category',
    );

} else {

    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]==''){
	die( 'No input data!');
    }
    
    $info = array(
	'user_id' => $argv[1],
	'user_name' => $argv[2],
	'next_time' => 10080,  // 1 week
	'owner' => 'system',
	'action' => 'cdiscount_fetch_category',
    );
}


$cdiscountCategoriesLoad = new CDiscountCategoriesLoad($info['user_name'], $debug);
$cdiscountCategoriesLoad->dispatch('universes');
