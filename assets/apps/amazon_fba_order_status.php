<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.fba.orders.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;
   
if($debug) {
    
    // Get argv from url 
    $info = array(
	'user_id' => $_GET['user_id'],
	'user_name' => $_GET['user_name'],
	'ext' => $_GET['ext'],
	'id_shop' => $_GET['id_shop'],
	'shop_name' => $_GET['shop_name'],
	'id_marketplace' => 2,
	'next_time' => 60,
	'owner' => 'system',
	'action' => 'fba_order_status',
    );
    
} else {
    
    $argv_1 = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $infos = json_decode($argv_1);
    $user_info = json_decode($infos->data, true);

    // Get userdata fromdatabase
    $info = array(
	'user_id' => $user_info['id_customer'],
	'user_name' => $user_info['user_name'],
	'ext' => $user_info['ext'],
	'id_shop' => $user_info['id_shop'],
	'shop_name' => $user_info['shop_name'],
	'id_marketplace' => 2,
	'next_time' => 60,
	'owner' => 'system',
	'action' => 'fba_order_status',
    );
}

// Get userdata fromdatabase
$data = AmazonUserData::get($info, false, true);

if( (!isset($data) || empty($data)) ){
    
    $info['countries'] = $info['ext'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {

    // skip un - master platform 
    if(!isset($data['fba_master_platform']) || empty($data['fba_master_platform']) || !$data['fba_master_platform']){

	$json = json_encode( array('error' => false, 'pass' => true, 'output' => 'we will run fba manager for master plateform only.') ) ;
	echo $json ; exit;

    }

    $data['shop_name'] = $info['shop_name'];
    $info['countries'] = $data['countries'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $users = json_decode(json_encode($data), false);
    $cron = false;
    //$debug = false;
    
    $amazon_stocks = new AmazonFBAOrder($users, $cron, $debug);
    $amazon_stocks->Dispatch(AmazonFBAOrder::fba_status);
    
}