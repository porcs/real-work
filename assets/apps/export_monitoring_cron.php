<?php
if(!defined('BASEPATH')){
    define('BASEPATH', '');
}
chdir(dirname(__FILE__));
require_once  (dirname(__FILE__).'/../../assets/apps/export_monitoring.php');
require_once  (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
require_once  (dirname(__FILE__).'/../../application/libraries/tools.php');

class export_monitoring_cron extends export_monitoring {
    
        protected $uinfo=null;
        private $user_lang = null;
	function __construct(){
		parent::__construct();

                $this->user_lang = $this->uinfo->getAllUserDefaultLaguage();//lang 
	}
        
	public function usersReports(){
	    
	    if (!empty($this->base_url))		
		$base_url = isset($this->base_url) && !empty($this->base_url) ? $this->base_url : $this->dir_server;
	    
	    elseif (!empty($this->dir_server))		
		$base_url = $this->dir_server;
	    
	    $style_img = 'border: 0 !important; display: block !important; outline: none !important; padding: 5px 5px;';
	    
	    $users = $this->getUserReports();
	    
	    $total_user = count($users['users']);
	    $active_user = $users['active'];
	    $inactive_user = $users['inactive'];
	    
	    $this->_smarty->assign('users', $users['users']);
	    $this->_smarty->assign('total_user', $total_user);
	    $this->_smarty->assign('active_user', $active_user);
	    $this->_smarty->assign('inactive_user', $inactive_user);
            $this->_smarty->assign('site_name', $this->site_name);
	    $this->_smarty->assign('logo', '<img src="'.$base_url.'/nblk/images/feedbiz_logob.png" alt="feed.biz" style="'.$style_img.'" />');
	    
	    $html  = $this->_smarty->fetch(dirname(__FILE__) . '/../../application/views/templates/email/english/export_user_report.tpl');
	    
	    $this->sendAdminMessage($html);    
	    
	}
	
        function sendMorningMessage(){
	    
                $this->site = $this->uinfo->getSiteByIdMargetplace();
		
                // each user
		foreach($this->users as $user){
               
                        $html   = '';
			$this->user_name = $user['user_name'];
			$this->id_customer = $user['id_customer'];			
			$this->user_email = $user['user_email'];
			$this->first_name = $user['user_first_name'];
			$this->last_name = $user['user_las_name'];
                        $Language='english';
                        if(isset($this->user_lang[$this->id_customer])){
                            $Language=$this->user_lang[$this->id_customer];
                        }
                        load('email', $Language);//lang
                        
			$FEED_BIZ = $user['shop_info'];
			$this->shop_name = isset($FEED_BIZ['name']) ? $FEED_BIZ['name'] : '';                        
                        if(empty($FEED_BIZ['url']))continue;
                        // each marketplace
                        foreach($user['marketplace'] as $marketplace){
                            
                            $this->id_marketplace = $marketplace['id_marketplace'];
                            $this->marketplace = $marketplace['name'];                            
                            
                            //verify read customer & marketplace
                            if(isset($this->read_user[$this->user_name][$this->marketplace])){
                                    continue;
                            }

                            if( strpos($FEED_BIZ['url']['products'], '54.200.229.35') > -1 || 
				strpos($FEED_BIZ['url']['products'], 'http://demo.common-services.com') > -1 || 
				strpos($FEED_BIZ['url']['products'], 'http://amazon.demo.common-services.com') > -1 ){
                                    continue;
                            }

                            //eBay Data
                            if(isset($marketplace['name']) && $marketplace['name'] == "eBay") {
                                $results = array();
                                $message = array();
                                $sum                        = array(
                                        'order_total'      => 0,
                                        'create_total'      => 0,
                                        'create_success'    => 0,
                                        'create_error'      => 0,
                                        'offer_total'       => 0,
                                        'offer_success'     => 0,
                                        'offer_error'       => 0,
                                        'total_error'       => 0,
                                );
                                foreach ( $this->site[$this->id_marketplace] as $site ) :
                                        if ( !empty($this->customer_site[$this->id_customer]) && is_numeric($site['id_site_ebay']) && in_array($site['id_site_ebay'], $this->customer_site[$this->id_customer]) ) :
                                                $this->id_site = $site['id_site_ebay'];
                                                $this->uinfo->record_history('0', 'Feedbiz', 'ebay_products_monitoring', 0);
                                                $result  = $this->geteBayMessage();
                                                if ( !empty($result) ) :
                                                        $results[$site['id_site_ebay']] = $result;
                                                        $results[$site['id_site_ebay']]['domain_detail'] = $site;
                                                        $sum['order_total']         += !empty($result['Order']) ? $result['Order'] : 0;
                                                        $sum['create_total']        += !empty($result['Product']['Total']) ? $result['Product']['Total'] : 0;
                                                        $sum['create_success']      += !empty($result['Product']['Success']) ? $result['Product']['Success'] : 0;
                                                        $sum['create_error']        += !empty($result['Product']['Error']) ? $result['Product']['Error'] : 0;
                                                        $sum['offer_total']         += !empty($result['Offer']['Total']) ? $result['Offer']['Total'] : 0;
                                                        $sum['offer_success']       += !empty($result['Offer']['Success']) ? $result['Offer']['Success'] : 0;
                                                        $sum['offer_error']         += !empty($result['Offer']['Error']) ? $result['Offer']['Error'] : 0;

                                                        foreach ( $result as $key => $res ) :
                                                                if ( $key == 'Product' || $key == 'Offer' ) :
                                                                        foreach ( $res['messages'] as $mes ) :
                                                                                if ( strtolower($mes['Level']) == 'error' ) :
                                                                                        $message[$mes['Message']] = !empty($message[$mes['Message']]) ? $message[$mes['Message']] + $mes['Count'] : $mes['Count'];
                                                                                endif;
                                                                        endforeach;
                                                                endif;
                                                        endforeach;
                                                endif;
                                        endif;
                                endforeach;
                                $sum['total_error']     = (!empty($sum['create_error']) ? $sum['create_error'] : 0) + (!empty($sum['offer_error']) ? $sum['offer_error'] : 0);
                                $html   .= $this->generateEbayTemplates($results, $sum, $message,$Language);
                            }
			    
                            //Amazon Data
                            if(isset($marketplace['name']) && $marketplace['name'] == "Amazon")
			    {
                                $yesturday = date('Y-m-d',strtotime("-1 days"));
                                $this->uinfo->record_history('0', 'Feedbiz', 'amazon_products_monitoring', 0);
                                $amazonResult = $this->getAmazonMessage($yesturday);
                                $html .= $this->generateAmazonTemplates($marketplace['name'], $amazonResult, $yesturday,$Language);
                            }                             
                        }
                        $this->sendCustomerMessage($html);
		}
	}
        
        function sendCustomerMessage($html = null) {
	    
                if (strlen($html)<= 100) {
                        return null;
                }
                
                $header  = $this->_smarty->fetch(dirname(__FILE__) . '/../../application/views/templates/email/english/export_monitor_header.tpl');
                $footer  = $this->_smarty->fetch(dirname(__FILE__) . '/../../application/views/templates/email/english/export_monitor_footer.tpl');
           
                if($this->debug){
			echo $header.$html.$footer;
		}else{
			$transport = Swift_SmtpTransport::newInstance();
			$mailer = Swift_Mailer::newInstance($transport);
			$swift_message = Swift_Message::newInstance();
			$swift_message->setSubject($this->site_name.' : '.l('Daily Report'));
			$swift_message->setFrom(array('support@feed.biz' => $this->site_name));
			$swift_message->setBody($header.$html.$footer, 'text/html');
			$swift_message->setTo(array($this->user_email));
			$swift_message->setBcc(
				array('praew@common-services.com', 
				    'por@common-services.com', 
				    'olivier@common-services.com', 
                                    'palm@common-services.com',
                                'alexandre@common-services.com')
				); 
			$mailer->send($swift_message);
		}
        }
	
	function sendAdminMessage($html = null) {
	    
                if (strlen($html)<= 100) {
                        return null;
                }
           
                if($this->debug){
			echo $html;
		}else{
			$transport = Swift_SmtpTransport::newInstance();
			$mailer = Swift_Mailer::newInstance($transport);
			$swift_message = Swift_Message::newInstance();
			$swift_message->setSubject($this->site_name.' : '.l('User Report'));
			$swift_message->setFrom(array('support@feed.biz' => $this->site_name));
			$swift_message->setBody($html, 'text/html');
			$swift_message->setTo(
			    array(
				'palm@common-services.com',
				'por@common-services.com', 
				'praew@common-services.com', 
				'olivier@common-services.com',
                                'alexandre@common-services.com'
				)
			    ); 
			$mailer->send($swift_message);
		}
        }
}

$products_monitoring = new export_monitoring_cron();
$products_monitoring->usersReports();
$products_monitoring->sendMorningMessage();