<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.import.orders.php';

$debug = false;

if(!$debug)
{
    $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $datas = json_decode($data);
    $users = json_decode($datas->data);
}
else
{
    $data = '{"id_customer":"328","user_name":"Praew","id_marketplace":"2","id_country":"4","ext":".de","countries":"German","currency":"EUR","merchant_id":"A2J21X9NOHTQ9T","aws_id":"AKIAJKBG23TRU7HNXBOQ","secret_key":"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","id_mode":"1","id_shop":"1","id_lang":"4","iso_code":"de","id_region":"eu","allow_automatic_offer_creation":false,"synchronize_quantity":false,"synchronize_price":false,"synchronization_field":"ean13","creation":true,"mode":"%7B%22datefrom%22%3A%2218-02-2015%22%2C%22dateto%22%3A%2218-02-2015%22%2C%22status%22%3A%22All%22%7D"}';
    $users = json_decode($data);
}

$users->option = $users->mode;
unset($users->mode);
$amazon_get_order = new AmazonImportOrders($users, $debug);
$amazon_get_order->Dispatch();