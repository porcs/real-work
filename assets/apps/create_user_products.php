<?php
    require_once(dirname(__FILE__) . '/FeedbizImport/config/products.php');
    
    $error  = null ;
    $output = null;
    
//    $time = time();
//    $argv[1] = json_encode(array("username" => "Test_" . $time));

    $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $userdata = json_decode($data);
    $userdata = json_decode($userdata->data);
    
    if(!isset($userdata->username) || empty($userdata->username))
    {
        echo json_encode( array('error' => 'Username wrong') ) ; 
        exit;
    } 
    
    $user = str_replace(" ", "_", $userdata->username);
    
    //////Products//////
    $database = ObjectModel::createDatabase($user);

    if(isset($database['error']))
    {
       $error = $database['error'];
    }   
    else 
    {
        $arr_table_name = array(
            'Attribute',
            'AttributeGroup',
            'Carrier',
            'Category',
            'Conditions',
            'Currency',
            'Feature',
            'FeatureValue',
            'Manufacturer',
            'Language',
            'Product',
            'ProductAttribute',
            'ProductAttributeCombination',
            'ProductAttributeImage',
            'ProductAttributeUnit',
            'ProductAttributeUrl',//2016-10-11
            'ProductCarrier',
            'ProductCategory',
            'ProductFeature',
            'ProductImage',
            'ProductSupplier',
            'ProductSale',        
            'ProductTag',
            'Supplier',
            'Tag',
            'Tax',
            'Unit',
            'OTP',
            'Shop',
            'Log',
            'ProductUrl', //2015-12-03
            //'MarketplaceProductOption',
        );

        foreach ($arr_table_name as $table_name)
            if(method_exists($table_name, 'createTable'))
            {
                $class = new $table_name($user);
                $result = $class->createTable();

                if(strlen($result))
                   $error .= $result;
            }
            else
                $error .= sprintf('Table %s - method doesn\'t exists. ', $table_name) ;
        
        //create mapping table
        $mapping = new Mapping($user);
        if(!$mapping->createMappingTable())
            $error .= sprintf('Table mapping - can not create') ;

        //create filter table
        $filter = new Filter($user);
        if(!$filter->createFilterTable())
            $error .= sprintf('Table filter - can not create') ;

        //create category selected table
        $category = new Category($user);
        if(!$category->createCategorySelectedTable())
            $error .= sprintf('Table category_selected - can not create') ;

        //create carrier selected table
        $carrier = new Carrier($user);
        if(!$carrier->createCarrierSelectedTable())
            $error .= sprintf('Table carrier_selected - can not create') ;
        
        //create token
        $token = new OTP($user);
        if(!$token->generate_token())
            $error .= sprintf('Token - can not generate') ;
        
    }
   
    if(!isset($error) || empty($error) || $error == null)
    {
        $log = new ImportLog($user);
        
        if($log)
        {
            if($log->createLogTable())
                $output = sprintf('User %s successfully created', $user) ;
            else
              $error .= sprintf('Log table - can not generate') ;  
        }
        else
            $error .= sprintf('Log import - can not generate') ;
    }
    
    
    $json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $user) ) ;

    echo $json ;