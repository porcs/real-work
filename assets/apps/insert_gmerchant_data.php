<?php

require_once(dirname(__FILE__) . '/FeedbizImport/config/products.php');
 
$error  = null ;
$output = null ;

$data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($argv[1])),null,'UTF-8');
$datas = json_decode($data);
$gdata = json_decode($datas->data);
$user = str_replace(" ", "_", $gdata->username);

if(isset($user) && !empty($user))
{
    $otp = new OTP($user);
    $otp_result = $otp->check_token($gdata->token);
    
    if( !$otp_result )
    {
        $error = sprintf('token_wrong') ;
        $output = $gdata;
    }
    else
    {
        $time = date("Y-m-d H:i:s"); 
        $Convert = new ConvertXMLToFeedBizXML($user);
        $convert_data = $Convert->gmerchant($gdata, $time);

        $oXml =  simplexml_load_string($convert_data);
        
        $arr_class_name = array(
            'Attributes'      => 'Attribute',
            'Carriers'        => 'Carrier',
            'Categories'      => 'Category',
            'Conditions'      => 'Conditions',
            'Currencies'      => 'Currency',
            'Features'        => 'Feature',
            'Language'        => 'Language',
            'Manufacturers'   => 'Manufacturer',
            'Suppliers'       => 'Supplier',
            'Taxes'           => 'Tax',
            'Units'           => 'Unit',
            'Products'        => 'Product',
        );
        
        if(isset($oXml) && !empty($oXml))
        {
            //shop name
            $shopname = $oXml['ShopName'];
            
            if(isset($shopname) && !empty($shopname))
            {
                $time = date("Y-m-d H:i:s");  
                $shop = new Shop($user);
                $id_shop = $shop->importData($shopname, $time, $gdata->gdata->fileurl);
                
                foreach ($arr_class_name as $xml_key => $class_name)
                {
                    if(method_exists($class_name, 'importData'))
                    {
                        $oXml->$xml_key->id_shop = $id_shop;
                        $oXml->$xml_key->shopname = $shopname;
                        $oXml->$xml_key->source = 'GMerchant';

                        $class = new $class_name($user);
                        $result = $class->import($oXml->$xml_key, $time);

                        if(isset($result['error']))
                            $error .= $result['error'] ;  
                    }
                    else
                    {
                        $error .= sprintf('Data table %s - Cannot be imported. ', $class_name) ;
                    }   
                }
            }
        }
        else
        {
            $error .=  sprintf('Data feed wrong.') ;
        }

        if(!strlen($error))
            $output = sprintf('Import successful. Time : ' . $time) ;

    }          
}
else
{
    $error = 'Invalid user.';  
}

$json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $output, 'shopname' => (string)$shopname, 'id_shop' => (int)$id_shop) ) ;
echo $json ;
