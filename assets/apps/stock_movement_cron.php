<?php
define('BASEPATH', '');
chdir(dirname(__FILE__)); 
require_once(dirname(__FILE__) . '/../../libraries/newrelic_report.php');
require_once dirname(__FILE__) . '/../../application/libraries/UserInfo/configuration.php';
require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.order.php';
require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');
require_once(dirname(__FILE__) . '/../../application/libraries/tools.php');
require_once dirname(__FILE__) . '/../../application/libraries/FeedBiz/config/orders.php';
require_once(dirname(__FILE__) . '/../../application/libraries/FeedBiz/config/stock.php');

$debug = isset($_GET['debug']) ? (bool) $_GET['debug'] : false ;

// Debug by user id $ id shop
if($debug) {

    $user_id = $_GET['user_id'];
    $id_shop = $_GET['id_shop'];
    
} else {
    
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1]) || !isset($argv[2]) || $argv[2]=='' || !is_numeric($argv[2])){
	die( 'No input data!');
    }

    $user_id = $argv[1];
    $id_shop = $argv[2];
    
}

$config = new UserConfiguration(); 
$input = $config->check_site_verify($user_id); 
$shop = $input['feed_shop_info']; 

if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){ 
    die();
}

$user_name = $input['user_name'];
$id_user = $input['user_id'];
$type = 'import_stock_movement'; 
$next_time = 20; //mins
$shop['name'] = empty($shop['name'])?'Shop name':$shop['name'];
$shop_name = str_replace('_',' ', $shop['name']); 
    
$config->record_history($user_id,$shop_name,$type,$next_time);

$data['id_user'] = $id_user;
$data['user_name'] = $user_name;
$data['id_shop'] = $id_shop;
$data['shop_name']= $shop_name;

if(!isset($data) || empty($data) ){
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {

    $id_user = $data['id_user'];
    $id_shop = $data['id_shop'];    
    $user_name = $data['user_name'];
    $shop = $data['shop_name'];
    
    $amazon_id_marketplace = Amazon_Order::$MarketplaceID;
    $eBay_id_marketplace = 3;
    
    $history = array();
    $batch_id = uniqid();
    $date = date('Y-m-d H:i:s');
    $loop = 1; 
    
    $userdata = $config->getShopInfo($id_user);
    $Language = $config->getUserDefaultLaguage($id_user);    

    // Load lange file
    load('orders', $Language);
    
    if (isset($userdata) && !empty($userdata)) {         
                        
        $process_title = l('Update stock movement to ') . ' ' . $shop . '...';
        $process_type =  'Stock movement';
	
	if(!$debug) {
	    $proc_rep = new report_process($user_name, $batch_id, $process_title, false, true);
	    $proc_rep->set_process_type($process_type); 
	}
	
        $Stock = new StockMovement($user_name);
	
        $Orders = new Orders($user_name, null, $debug);
        $OrderItems = new OrderItems($user_name);       
        $token = $config->getUserCode($id_user);
        
        // 1. Get list of sent stock update
        $ref_id_list = $Stock->getOrderIdStock($id_shop);
	
	if($debug) {
	    //var_dump(array('list of sent stock update' => $ref_id_list));
	}
	
        if(isset($ref_id_list) && !empty($ref_id_list)) {
            
            $list = implode(', ', $ref_id_list);
        } else {
            $list = '';
        }
        
        // 2. Select order where order id not in list of stock update
        $send_order_list = $Orders->getOrdersNotInList($id_shop, $list);
	
	if($debug) {
	    var_dump(array('list of stock update to update' => $send_order_list));
	}
	
        if($loop == 1 && !$debug) {
            $proc_rep->set_max_min_task(sizeof($send_order_list), 1);
        }
        
        if(isset($send_order_list) && !empty($send_order_list)) {
            
            foreach ($send_order_list as $order) {
                
                if($order['id_marketplace'] == $eBay_id_marketplace && ($order['order_status'] == "Active" || $order['order_status'] == "Canceled"))
                    continue;
                
		if(!$debug) 
		    $proc_rep->set_running_task($loop);
                
                if(!isset($order['id_orders']) || empty($order['id_orders'])) {
		    
		    if($debug) {
			echo 'Missing id_orders : <pre>' . nl2br(print_r($order, true)) . '</pre>' ;
		    }
		    
                    continue;
		}
		
                $source = '';
		
		$id_marketplace = isset($order['id_marketplace']) ? $order['id_marketplace'] : null ;
		$site = isset($order['site']) ? $order['site'] : null ;
		 
                if( isset($site) && isset($id_marketplace) ) {
                    
                    $Marketplace = $config->getMargetplaceById($id_marketplace, $site);

                    //source
                    if(isset($Marketplace['domain']) && !empty($Marketplace['domain'])){
                        $source = $Marketplace['domain'];
                    } else if (isset($Marketplace['name']) && isset($Marketplace['ext']) && !empty($Marketplace['ext']) ) {
                        $source = $Marketplace['name'] . $Marketplace['ext'];
                    } else {
                        if($id_marketplace == $amazon_id_marketplace) {
                            $source = 'Amazon' ;
                        } elseif($id_marketplace == $eBay_id_marketplace) {
                            $source = 'eBay' ;
                        } else {
                            $source = 'Others' ;
                        }
                    }   
                    
                }
                
                $id_orders = $order['id_orders'];

                // 3. Get order item to update stock movement
                $items = $OrderItems->getOrderItem($id_orders);
		
		if(!empty($items))
		{
		    foreach($items as $item) {
			if(isset($item) && !empty($item)) {
			    $order_list[$id_orders]['source'] = $source;
			    $order_list[$id_orders]['site'] = $site;
			    $order_list[$id_orders]['items'][$item['id_order_items']] = array(
				'id_product' => $item['id_product'],
				'id_product_attribute' => $item['id_product_attribute'],
				'quantity' => $item['quantity'],
				'quantity_in_stock' => $item['quantity_in_stock'],
				'reference' => $item['reference'],
				'product_name' => $item['product_name'],
				'time' => $date
			    );                       
			}
		    }
		} else {
		    
		    // Log 
		    $Orders->order_log($id_shop, $id_marketplace, $site, $batch_id, $id_orders, '0', 'StockMovement : Missing order items');
		    
		}

                $loop ++;
            }

            // Stock Movement
            if(isset($order_list) && !empty($order_list)) {
		
		if($debug) {
		    echo 'Order list : <pre>' . (print_r($order_list, true)) . '</pre>' ;
		}
		
                $Orders->stockMovement($userdata['url_stockmovement'], $token, $id_shop, $batch_id, $id_marketplace, $site, $order_list);                
            }
	    
	    if(!$debug) {
		$proc_rep->set_status_msg(l('COMPLETED'));
	    } else {
		echo '<pre>COMPLETED</pre>' ; 
	    }
	    
        } else {
	    
	    if(!$debug) {
		$proc_rep->set_error_msg(l('NO STOCK TO UPDATE'));
	    } else {
		echo '<pre>NO STOCK TO UPDATE</pre>' ; 
	    }
	    
        }
	
	if(!$debug) 
	    $proc_rep->finish_task();
    
    } else {
        
        echo json_encode(array('status' => 'error', 'process' => 'Export orders', 'message' => 'Wrong user.'));
        die();
        
    }
    
}