<?php
define ( 'BASEPATH', '' );
chdir ( dirname ( __FILE__ ) );
require_once (dirname ( __FILE__ ) . '/../../application/config/config.php');
require_once (dirname ( __FILE__ ) . '/../../libraries/ci_db_connect.php');
require_once (dirname ( __FILE__ ) . '/../../application/libraries/FeedBiz.php');
require_once (dirname ( __FILE__ ) . '/../../application/libraries/FeedBiz_Orders.php');
require_once (dirname ( __FILE__ ) . '/../../application/libraries/Ebay/ebayorder.php');
require_once (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
require_once (dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');

if (! isset ( $argv [1] ) || $argv [1] == '' || ! is_numeric ( $argv [1] ) || ! isset ( $argv [2] ) || $argv [2] == '' || ! isset ( $argv [3] ) || $argv [3] == '' || ! is_numeric ( $argv [3] ) || ! isset ( $argv [4] ) || $argv [4] == '' || ! isset ( $argv [5] ) || $argv [5] == '' || ! isset ( $argv [6] ) || $argv [6] == '') {
die ( 'No input data!' );
}

$user_id = $argv [1];
$user_name = $argv [2];
$site_id = $argv [3];
$site_domain = $argv [4];
$shop_name = str_replace ( '_', ' ', $argv [5] );
$id_marketplace = $argv [6];
$id_shop = $argv [8];
$ext = str_replace('ebay','', $site_domain);

class LoadMPOrderCron {
	var $mysqlPortal = null;
	var $user_id = 0;
	var $user_name = '';
	var $user_code = '';
	var $shop_id = 0;
	var $id_marketplace = 0;
	var $site_id = 0;
	var $site_domain = '';
	var $shop_name = '';
        var $next_time = 20;
        
	public function __construct($user_id, $id_marketplace, $site_id, $site_domain, $shop_name, $id_shop) {
		$this->user_id = $user_id;
		$this->mysqlPortal = new MysqlPortal ();
		$user = $this->mysqlPortal->getUsers ( $this->user_id );
		$this->user_name = $user ['user_name'];
		$this->user_code = $user ['user_code'];
		$FeedBiz = new FeedBiz ( array (
				$this->user_name 
		) );
		if ( empty($id_shop) ) {
                        $shops = $FeedBiz->getDefaultShop ( $this->user_name );
                        $this->shop_id = $shops ['id_shop'];
                }
                else {
                        $this->shop_id = $id_shop;
                }
		$this->site_id = $site_id;
		$this->site_domain = $site_domain;
		$this->shop_name = $shop_name;
		$this->id_marketplace = $id_marketplace;
	}
	public function dispatch() {
		if ($this->id_marketplace == 3) {
			$param = array (
					$this->user_name,
					$this->user_id,
					$this->site_id 
			);
			$ebayorder = new ebayorder ( $param );
			$parser = $ebayorder->get_modifier_orders();

			$this->mysqlPortal->logCron ( array (
					'user_id' => $this->user_id,
					'user_name' => $this->user_name,
					'history_action' => "mod_{$this->site_domain}_orders",
					'history_date_time' => date ( 'Y-m-d H:i:s' ),
					'history_table_name' => str_replace('_',' ',$this->shop_name),
					'history_data' => json_encode ( array(
                                            'next_time' => strtotime("+{$this->next_time} minutes")
                                        ) ) 
			) );
		}
	}
}
class MysqlPortal {
	var $ci_db_connect = null;
	public function __construct() {
		$this->ci_db_connect = new ci_db_connect ();
	}
	public function getUsers($user_id) {
		$user_sql = "select id as user_id, user_name, user_code from users where id = '$user_id'";
		$user_query = $this->ci_db_connect->select_query ( $user_sql );
		$user_row = $this->ci_db_connect->fetch ( $user_query );
		return $user_row;
	}
	function getConfiguration($user_id) {
		$mainconfiguration = array ();
		$configuration_sql = "select * from configuration where id_customer = '" . intval ( $user_id ) . "' and name IN ('FEED_BIZ', 'FEED_MODE','FEED_SHOP_INFO','FIRST_CONF', 'EBAY_USER')";
		$configuration_query = $this->ci_db_connect->select_query ( $configuration_sql );
		while ( $row = $this->ci_db_connect->fetch ( $configuration_query ) ) {
			$mainconfiguration [$row ['name']] = $row ['value'];
		}
		return $mainconfiguration;
	}
	function logCron($data) {
		$this->ci_db_connect->add_db ( 'histories', $data );
	}
}

$batch_id = !empty($product->batch_id) ? $product->batch_id : 'check exist ebay running';
$rep = new report_process($user_name, $batch_id, "Update {$site_domain} orders", true, true, true, "mod_{$site_domain}_orders");
if($rep->has_other_running("mod_{$site_domain}_orders")){
     die();
}
$object = new LoadMPOrderCron ( $user_id, $id_marketplace, $site_id, $site_domain, $shop_name, $id_shop );
$object->dispatch ();