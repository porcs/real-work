<?php 
    define('BASEPATH', '');
    chdir(dirname(__FILE__)); 
    require_once  (dirname(__FILE__).'/../../libraries/ci_db_connect.php');
    require_once dirname(__FILE__). "/../../application/libraries/Ebay/ebayofferexport.php"; 
    require_once  (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
    require_once(dirname(__FILE__) . '/../../application/libraries/tools.php');//for lang
    
    
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1]) || !isset($argv[2]) || $argv[2]=='' || !isset($argv[3]) || $argv[3]=='' || !is_numeric($argv[3] )|| !isset($argv[4]) || $argv[4]==''|| !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]==''){
 
        die( 'No input data!');
    }
    
    $user_id = $argv[1];
    $user_name =  $argv[2];
    $site_id = $argv[3];
    $site_domain= $argv[4];
    $shop_name = str_replace('_',' ',$argv[5]);
    $id_marketplace = $argv[6];
    $next_time = 20;
    $ext = str_replace('ebay','', $site_domain);
    $owner = 'system'; 
    $other = array(
        'next_time' => strtotime("+{$next_time} minutes")
    );
        
    $uinfo = new UserConfiguration(); 
    $Language = $uinfo->getUserDefaultLaguage($user_id);//lang
        load('ebay', $Language);//lang
    $input = $uinfo->check_site_verify($user_id); 

    $shop = $input['feed_shop_info']; 

    if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){
        die();
    }
        
    require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');

    
     
    $data=array(
            'user_id'=>$user_id,
            'user_name'=>$owner,
            'history_action'=>"export_{$site_domain}_offers",
            'history_date_time'=>date('Y-m-d H:i:s'),
            'history_table_name'=>str_replace('_',' ',$shop_name) ,
            'history_data'=>  json_encode($other),
    );
    $mysql_con = new ci_db_connect();     
    $mysql_con->add_db('histories',$data);
    
    
     $type = "export_{$site_domain}_offers";
     $product            = new ebayofferexport(array($user_name, $user_id, $site_id));
     $batch_id = !empty($product->batch_id) ? $product->batch_id : 'check exist ebay running';
     $rep = new report_process($user_name, $batch_id, l("Export eBay")." ".$site_domain." ".l("offer"), true, true, true, 'export_offers_site_'.$site_id);
     if($rep->has_other_running('export_offers_site_'.$site_id)){
         die();
     }
     $rep->set_process_type($type); 
     $rep->set_priority(1); 
     $rep->set_shop_name($shop_name); 
     $result             = $product->export_compress();
     $fail               = 1;
     if ( !empty($result['msg']['empty_products']) ) :
                $msg_error      = l('Empty products.');
                $rep->set_error_msg($msg_error); 
                $fail = 0;
     elseif ( !empty($result['msg']['no_export']) ) :
            $msg_error      = l('No export.');
            $rep->set_error_msg($msg_error); 
            $fail = 0;
     else : $rep->set_max_min_task(0, 100); 
            $result             = $product->export_bulk();
            $result             = $product->export_download($rep);
     endif;

    ( !empty($rep) ) ? $rep->finish_task($fail) : $rep->finish_task(0);
