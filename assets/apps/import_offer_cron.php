<?php
    chdir(dirname(__FILE__)); 
    include_once (dirname(__FILE__) . '/../../libraries/newrelic_report.php');
    require_once  (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])){
        die( 'No input data!');
    }

    if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
        $str = array(); 
        $str = implode(' ', $argv);
        ob_start();
        $cmd = "ps aux | grep php | grep '{$str}'  | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' ";
        passthru($cmd);
        if (sizeof(explode("\n", trim(ob_get_clean()))) > 1) {
            exit("dup");
        }
    }

    $user_id = $argv[1];
    $shop_id = isset($argv[2])?$argv[2]:0;
    $skip = isset($argv[3])?$argv[3]:0;
    $uinfo = new UserConfiguration();
    
    $input = $uinfo->check_site_verify($user_id); 
    
    if(!isset($input['feed_biz']['verified']) || $input['feed_biz']['verified']!='verified'){
        die('This site was not verified.');
    }
    $shop = $input['feed_shop_info'];
    if(empty($shop['software']))exit;
    $shop_type = strtolower($shop['software']); 
    
//    $user_id = $input['feed_biz']['user_id'];
    $user_name = $input['feed_biz']['username'];
    $type = 'import_offer'; 
    $next_time = 20;//mins
        
    $shop_name = $shop['name'];
    require_once(dirname(__FILE__) . '/../../application/config/config.php');
    require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');
//    $batch_id = 'check exist amazon running';
//    $rep = new report_process($user_name,$batch_id);
//    $rep->end_current_process();
//    if($rep->has_other_running()){
//        die();
//    }
        
    $uinfo->record_history($user_id,$shop_name,$type,$next_time); 
    
    
    
    $batch_id = 'check exist import offer running';
     $rep = new report_process($user_name,$batch_id);
     $rep->end_current_process();
     if($rep->has_other_running('import offer')){
         die();
     }
        
    $out=array();
    switch($shop_type){
        case 'prestashop': 
                $res = exec('php insert_prestashop_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);   
                break;
        case 'opencart': 
                $res = exec('php insert_opencart_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
                $out[] = json_decode($res,true);   
        break;
        case 'gmerchant':
            $res = exec('php insert_gmerchant_products.php '.rawurlencode($uinfo->feed_prepare_argv($input)));
            $out = json_decode($res);
            if($out->pass){
                $res = exec('php insert_gmerchant_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
            }
        break;
        case 'magento':
            $res = exec('php insert_magento_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers',$shop_type)));
            $out[] = json_decode($res,true);   
        break;
        case 'woocommerce': 
            $res = exec('php insert_woocommerce_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
            $out[] = json_decode($res,true);   
            echo json_encode($out);
        break;
        case 'shopify': 
            $res = exec('php insert_shopify_offers.php '.rawurlencode($uinfo->feed_prepare_argv($input,'offers')));
            $out[] = json_decode($res,true);   
        break;
        default://other cms
            
        break;
    }
    if($skip){exit;}
    
    //amazon delete product
//    $sql = "select ac.*,h.history_date_time,u.user_created_on from amazon_configuration ac , histories h ,users u where  
//            CONCAT('export_amazon',ac.ext,'_synchronize') = h.history_action 
//            and  ac.active = '1' /*and ac.allow_automatic_offer_creation = '1'*/ and ac.id_customer = h.user_id and u.id = ac.id_customer  and u.id = '$user_id'
//             group by ac.id_customer,id_country,id_shop ";
////    echo $sql;
//    $q = $mysql_con->select_query($sql);
//        
//    if($mysql_con->count_rows($q)>0){
//        while($row = $mysql_con->fetch($q)){
//            $ext =  $row['ext'];
//            $id_shop = $row['id_shop']; 
////            print_r($row);
//        }
//    }
    
//     foreach ($out as $o) {
//        if ($o ['error'] == "Access Denied.") {
//           $uinfo->unverify_site($user_id,$input);
//        }
//    }
    
    if($shop_id==0){ echo 'no shop';exit;}
    $amazon_list = $uinfo->get_amazon_fba_update_site($user_id,array($shop_id));
    $market_id = 2; 
    echo  "\n"; 
    foreach($amazon_list as $item){
       $ext = $item['ext'];  
//       if((int)$item['cron_delete_products']==1){
       if(!isset($item['deny']['fba_manager'])){    
            $url = "php amazon_fba_manager_cron.php {$user_id} {$user_name} {$ext} {$shop_id} {$shop_name} {$market_id}"; 
             echo $url."\n"; 
            echo exec($url);
       }   
    }
    