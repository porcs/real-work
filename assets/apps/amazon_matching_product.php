<?php
require_once getcwd() . '/application/libraries/Amazon/classes/amazon.database.php';
require_once getcwd() . '/application/libraries/Amazon/functions/automaton.php';

class Matching 
{
    public function __construct($userdata, $debug = false)
    {
        $this->debug = $debug;
        $this->userdata =  array(
                'user_name' => trim($userdata['user_name']),
                'id_shop' => trim($userdata['id_shop']),
                'id_mode' => trim($userdata['id_mode']),
                'id_country' => trim($userdata['id_country']),
                'MerchantID' => trim($userdata['merchant_id']),
                'AWSAccessKeyID' => trim($userdata['aws_id']),
                'SecretKey' => trim($userdata['secret_key']),
                'ext' => ltrim($userdata['ext'], "."), 
                'lang' => ltrim($userdata['ext'], "."), 
                'id_lang' => trim($userdata['id_lang']),
                'iso_code' => trim($userdata['iso_code']),
                'language' => trim($userdata['iso_code']),
                'synchronization_field' => ltrim($userdata['synchronization_field']),     
                'currency' => ltrim($userdata['currency']),     
                'ext' => ltrim($userdata['ext']),     
            );
    }
    
    function products($mode = null, $page = 1)
    {        
        $users = $this->userdata;
        
        if(isset($users) && !empty($users))
        {
            $auth = $this->userdata;

            $marketPlace = array(
                'Currency' => $users['currency'],
                'Country' => ltrim($users['ext'], ".")
            );
                       
            if(!$amazonAutomaton = new AmazonAutomaton($auth, $marketPlace, null, $this->debug))
            {
                if($this->debug)
                    echo 'Unable to login';
                
                return(false); 
            }
            else
            {            
                $Automaton = $amazonAutomaton->Dispatch('match-products', null, null, $mode, $page);

                if(!isset($Automaton) || empty($Automaton))
                {
                    if($this->debug)
                        echo 'Unable to connect Automaton';
                    
                    return(false); 
                }
                else
                {
                    if($this->debug)
                        echo '<pre>' . print_r($Automaton, true) .'</pre>';
                    
                    return($Automaton); 
                }
            }
        }
        else 
        {
            if($this->debug)
                echo  'Invalid user data';
            
            return(false);   
        }
    }
    
    function confirm_products($products)
    {      
        $users = $this->userdata;
        if(isset($users) && !empty($users))
        {            
            $auth = $this->userdata;
            $marketPlace = array(
                'Currency' => $users['currency'],
                'Country' => ltrim($users['ext'], ".")
            );

            if(!$amazonAutomaton = new AmazonAutomaton($auth, $marketPlace, null, $this->debug))
            {
               
                if($this->debug)
                    echo 'Unable to login';
                
                return(false); 
            }
            else
            {            
                $Automaton = $amazonAutomaton->Dispatch('confirm-products', null, $products);
                
                if(!isset($Automaton) || empty($Automaton) || !$Automaton)
                {
                    if($this->debug)
                        echo 'Unable to connect Automaton';
                    
                    return(false); 
                }
                else
                {
                    if($this->debug)
                        echo '<pre>' . print_r($Automaton, true) .'</pre>';
                    
                    return($Automaton); 
                }
            }
        }
        else 
        {
            if($this->debug)
                echo  'Invalid user data';
            
            return(false);   
        }
    }
}