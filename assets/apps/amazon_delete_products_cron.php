<?php

define('BASEPATH', '');
chdir(dirname(__FILE__)); 

require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.feeds.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

#http://ndb.feed.dev/assets/apps/amazon_delete_products_cron.php?debug=1&user_id=26&user_name=u00000000000026&ext=.fr&id_shop=1&shop_name=FOXCHIP&id_marketplace=2

$debug = isset($_GET['debug']) ? (bool) $_GET['debug'] : false ;

if($debug) {
    $argv = array();
    $argv[1] = $_GET['user_id'];
    $argv[2] = $_GET['user_name'];
    $argv[3] = $_GET['ext'];
    $argv[4] = $_GET['id_shop'];
    $argv[5] = $_GET['shop_name'];
    $argv[6] = $_GET['id_marketplace'];
} else {
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]==''
        || !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
        || !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]==''){
        die( 'No input data!');
    }
}

// Get argv from url
$info = array(
    'user_id' => $argv[1],
    'user_name' => $argv[2],
    'ext' => $argv[3],
    'id_shop' => $argv[4],
    'shop_name' => str_replace('_',' ',$argv[5]),
    'id_marketplace' => $argv[6],
    'next_time' => 1440,
    'owner' => 'system',
    'action' => 'delete',
);

// Get userdata fromdatabase
$data = AmazonUserData::get($info);

if(!isset($data) || empty($data) ){
    
    $info['countries'] = $info['ext'];

    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    $info['countries'] = $data['countries'];

    // Set history log
    AmazonHistoryLog::set($info);
    
    $data['mode'] = rawurlencode((json_encode(array('delete' => AmazonDatabase::DELETE_INACTIVE))));
    $object = json_decode(json_encode($data), FALSE);
    $object->creation = false;
    
    $amazon_feeds = new AmazonFeeds($object, AmazonDatabase::DELETE, false, true);
    $amazon_feeds->FeedsData();
    
}