<?php

function simpleCallWS($params, $API, $method, $returnXML = true) {

    $uri = '/' . trim($API);
               
    ksort($params);
    $host = $_SERVER['SERVER_NAME'];

    switch ($API) {
        case 'getOrders' :
            $uri = '/webservice/WsFeedBiz/api/Order/getOrders';
            break;
    }

    foreach ($params as $param => $value) {
        $param = str_replace("%7E", "~", rawurlencode($param));
        $value = str_replace("%7E", "~", rawurlencode($value));
        $canonicalized_query[] = $param . "=" . $value;
    }

    $canonicalized_query = implode("&", $canonicalized_query);
    
    $curlOptions = array(
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_RETURNTRANSFER => true,
    );

    $curlOptions[CURLOPT_URL] = "https://" . $host . $uri . "?" . $canonicalized_query;
    $curlOptions[CURLOPT_SSL_VERIFYPEER] = false;
    $curlOptions[CURLOPT_VERBOSE] = false;

    $curlHandle = curl_init();
    curl_setopt_array($curlHandle, $curlOptions);
//    
//    $json = json_encode( array('$curlOptions' => $curlOptions) ) ;
//    echo $json ; die();
                
    if (($result = curl_exec($curlHandle)) == false)
        $pass = false;
    else
        $pass = true;

    if (!$pass)
        return(false);
    
    curl_close($curlHandle);

    if ($returnXML) {
        try {
            $xml = new SimpleXMLElement($result);
            return($xml);
        } catch (Exception $e) {
            echo("Exception Caught: " . $e->getMessage() . "\n");
        }
    } else {    
        return($result);
    }
}

function post_orders_ws($params, $url) {
    if (!$url)
        return(false);
    
    $params = array('xml' => $params);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_URL, $url);

    $result = curl_exec($ch);

    if ($ch)
        curl_close($ch);

    if (!$result)
        return(false);

    $string = htmlspecialchars_decode($result);

    $xml = simplexml_load_string($string);
    
    if ($xml instanceOf SimpleXMLElement) {
        return($xml);
    } else {
        return(false);
    }
}
