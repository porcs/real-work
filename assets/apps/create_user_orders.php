<?php
    require_once(dirname(__FILE__) . '/FeedbizImport/config/orders.php');
    
    $error  = null ;
    $output = null;

    $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $userdata = json_decode($data);
    $userdata = json_decode($userdata->data);
    
    if(!isset($userdata->username) || empty($userdata->username))
    {
        echo json_encode( array('error' => 'Username wrong') ) ; 
        exit;
    } 
    
    $user = str_replace(" ", "_", $userdata->username);
    $database = ObjectModel::createDatabase($user);
    if(!isset($user) || empty($user))
    {
       $error = sprintf('User Error, %s. ', $user) ;
    }   
    else 
    {
        $arr_table_name = array(
            'OrderLog',
            'OrderBuyers',
            'OrderInvoices',
            'OrderItems',
            'OrderItemsAttribute',
            'OrderPayments',
            'OrderSeller',
            'OrderShippings',
            'OrderStatus',
            'OrderTaxes',
            'Orders'
        );

        foreach ($arr_table_name as $table_name)
            if(method_exists($table_name, 'createTable'))
            {
                $class = new $table_name($user);
                $result = $class->createTable();

                if(strlen($result))
                   $error .= $result;
            }
            else
                $error .= sprintf('Table %s - method doesn\'t exists. ', $table_name) ;
	    
	    $Orders = new Orders($user);
	    if(!$Orders->create_mp_multichannel()){
		$error .= sprintf('Table mp_multichannel - create fail. ') ;
	    }
        
    }
   
    $json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $user) ) ;

    echo $json ;