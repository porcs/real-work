<?php

require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.scheme.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.history.log.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/functions/mirakl.products.delete.php';

$debug = false;

if (!$debug) {
    $data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($argv[1])), null, 'UTF-8');
    $datas = json_decode($data);
    $users = json_decode($datas->data);
} else {
    $users = new stdClass;
    $users->id_user = '56';
    $users->id_shop = '1';
    $users->sub_marketplace = '13';
    $users->id_country = '10';
}

if (!isset($users) || empty($users)) {
    echo json_encode(array('status' => 1, 'message' => 'Access Denided'));
    return;
}

// init var
$id_user = $users->id_user;
$id_shop = $users->id_shop;
$sub_marketplace = $users->sub_marketplace;
$id_country = $users->id_country;

// get data
$mirakl_scheme = new MiraklScheme();
$shop_info = $mirakl_scheme->getShopInfo($id_user);
$get_config = $mirakl_scheme->getMiraklConfiguration($id_user, $sub_marketplace, $id_country, $id_shop);

$params = array();

$mirakl_function = new MiraklDeleteProducts($get_config, false);
//$results = $mirakl_function->productDelete($params);
$results = $mirakl_function->processProduct($params);

$log_history = array(
    'user_id' => $id_user,
    'ext' => $get_config['ext'],
    'shop_name' => isset($shop_info['name']) ? $shop_info['name'] : '',
    'countries' => $get_config['countries'],
    'sub_marketplace' => $sub_marketplace,
    'next_time' => 1440,
    'owner' => 'user',
    'action' => MiraklHistoryLog::ACTION_TYPE_DELETE_PRODUCT,
    'transaction' => $results['status'] // check status in woking for check next time.
);

// Set history log
MiraklHistoryLog::set($log_history);

// response to node
echo json_encode($results);
