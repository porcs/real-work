<?php
    header('Content-Type: text/html; charset=utf-8');
//    error_reporting(E_ALL);
    error_reporting(E_ERROR | E_PARSE);
    ini_set('Display_Errors', 1);
    chdir(dirname(__FILE__));
    require_once(dirname(__FILE__) . '/FeedbizImport/log/RecordProcess.php');
    
    require_once(dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
    require_once(dirname(__FILE__) . '/../../application/libraries/tools.php');//for lang
    
    chdir('../..');
    
    if( ! ini_get('date.timezone') ){ date_default_timezone_set('GMT');}
    $error              = null ;
    $output             = null;

//    $argv[1] = json_encode(
//    array(
//        'username'  => "u00000000000342",
//        'userid'    => '342',
//        'siteid'    => '71',
//        'method'    => 'compress',
//        'link'      => 'shipment',
//    ));

    $data               = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
    $userdata           = json_decode($data);
//    $userdata->batch_id = '557119b34bf8e';
    $user               = str_replace(" ", "_", $userdata->username);
    $user_id            = $userdata->userid;
    $site_id            = $userdata->siteid;
    $method             = $userdata->method;
    $link               = $userdata->link;
    $batch_id           = isset($userdata->batch_id) ? $userdata->batch_id : '';
    $synchronize        = false;
    $type_use           = 'add';
    $next               = $method;
    $comm_config        = '';
    
    $uinfo = new UserConfiguration(); 
    $Language = $uinfo->getUserDefaultLaguage($user_id);//lang
    load('ebay', $Language);//lang

    if(isset($error) || !empty($error) || $error == null)
            $output = sprintf('User %s successfully created', $user) ;
    
    if (    $method == 'verify_product' ) :
            $method         = 'compress';
            $type_use       = 'verify';
            $next           = $method;
    elseif ($method == 'revise_product' ) :
            $method         = 'compress';
            $type_use       = 'revise';
            $next           = $method;
            $synchronize    = true;
    elseif ($method == 'compress_file' ) :
            $method         = 'compress';
//            $type_use       = 'revise';
//            $synchronize    = true;
    elseif ($method == 'compress_db' ) :
            $method         = 'compress';
            $comm_config    = 'export_product_db';
            $next           = $method;
    elseif ($method == 'compress_query' ) :
            $method         = 'compress';
            $comm_config    = 'export_product_query';
            $next           = $method;
    endif;

    switch ( $link ) :
            case "products" :
                    require dirname(__FILE__). "/../../application/libraries/Ebay/ebayexport.php";
                    switch ( $method ) :
                            case "compress" :   $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Process Export : eBay compress files XML'), true, true, true, 'export_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_products_site_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $proc_rep->set_max_min_task(0, 100); 
                                                $product->compress_config($comm_config);
                                                $result             = $product->export_compress($synchronize, $type_use, $proc_rep);
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['empty_products']) ) :
                                                                $msg_error      .= l(' Empty products. ');
                                                        endif;
                                                        if ( !empty($result['msg']['validation_error']) ) :
                                                                $msg_error      .= l(' XML Validation Error. ');
                                                        endif;
                                                        if ( !empty($result['msg']['mapping_category']) ) :
                                                                $msg_error      .= l(" Mapping category. ");
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = $next;
                                                $result['batch_id'] = $product->batch_id; 
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                
                                                $json               = @json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "upload"   :   $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $product->batch_id  = $batch_id;
                                                $proc_rep           = new report_process($userdata->username, $batch_id, l('Process Export : Upload file to eBay site'), true, true, true, 'export_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_products_site_'.$site_id); 
                                                $proc_rep->set_priority(2); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->export_bulk();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createjob']) ) :
                                                                $msg_error      .= l(' Cannot create job. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createfile']) ) :
                                                                $msg_error      .= l(' Ebay feed transfer failed. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_startupload']) ) :
                                                                $msg_error      .= l(" Ebay feed process failed. ");
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = $next;
                                                $result['batch_id'] = $batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "download" :   $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $product->batch_id  = $batch_id;
                                                $proc_rep           = new report_process($userdata->username, $batch_id, l('Process Export : Download file response from eBay'), true, true, true, 'export_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_products_site_'.$site_id); 
                                                $proc_rep->set_priority(3); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $proc_rep->set_max_min_task(0, 100); 
                                                $result             = $product->export_download($proc_rep);
                                                $result['method']   = $next;
                                                $result['batch_id'] = $batch_id;

                                                if ( !empty($result['msg']) && count($result['msg']) ) :
	                                                $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['job_failure']) ) :
                                                                $msg_error      .= l(" Upload Job Failure. ");
	                                                endif;
	                                                if ( !empty($result['msg']['bulk_download']) ) :
                                                                $msg_error      .= l(" Download Response Failed. ");
	                                                endif;
                                                        
	                                                $proc_rep->set_error_msg($msg_error);
	                                                $fail = 0;
	                                                $error = true;
                                                endif;
                                                
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "specifics" :  require dirname(__FILE__). "/../../application/libraries/Ebay/ebayspecifics.php";
                                                $product            = new ebayspecifics(array($user, $user_id, $site_id));
                                                $result             = $product->get_specific();
                                                $result['method']   = 'specific';
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "features" :   require dirname(__FILE__). "/../../application/libraries/Ebay/ebayfeatures.php";
                                                $product            = new ebayfeatures(array($user, $user_id, $site_id));
                                                $result             = $product->get_features();
                                                $result['method']   = 'features';
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "categories" : require dirname(__FILE__). "/../../application/libraries/Ebay/ebaycategories.php";
                                                $product            = new ebaycategories(array($user, $user_id, $site_id));
                                                $result             = $product->get_categories();
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "shippings" :  require dirname(__FILE__). "/../../application/libraries/Ebay/ebayshippings.php";
                                                $product            = new ebayshippings(array($user, $user_id, $site_id));
                                                $result             = $product->get_shippings();
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "synchronization" :  if ( class_exists('ebaysynchronization') == FALSE ) require dirname(__FILE__). "/../../application/libraries/Ebay/ebaysynchronization.php";
                                                $product            = new ebaysynchronization(array($user, $user_id, $site_id));
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Get Inventory : eBay Synchronization get XML'), true, true, true, 'synchronization_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('synchronization'); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->get_inventory();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $result['method']   = 'download';
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "synchronization_matching" :  if ( class_exists('ebaysynchronization') == FALSE ) require dirname(__FILE__). "/../../application/libraries/Ebay/ebaysynchronization.php";
                                                $product            = new ebaysynchronization(array($user, $user_id, $site_id));
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Get Inventory : eBay Synchronization get XML'), true, true, true, 'synchronization_site_'.$site_id, 'synchronization' );
                                                $proc_rep->set_process_type('synchronization_site_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : synchronization')); 
                                                $proc_rep->set_process_comments('synchronization');
                                                $proc_rep->set_max_min_task(0, 100); 
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result             = $product->get_percent_inventory($proc_rep);
                                                $result             = $product->generate_ebay_product_item_id();
                                                $result['method']   = 'download';
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                    endswitch;
            break;
            case "wizard" :
                    require dirname(__FILE__). "/../../application/libraries/Ebay/ebayexport.php";
                    switch ( $method ) :
                            case "compress" :   $product            = new ebayexport(array($user, $user_id, $site_id, null, true));
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Process Export : eBay compress files XML'), true, true, true, 'export_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_products_site_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $proc_rep->set_max_min_task(0, 100); 
                                                $result             = $product->export_compress(false, $type_use, $proc_rep);
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['empty_products']) ) :
                                                                $msg_error      .= l(' Empty products. ');
                                                        endif;
                                                        if ( !empty($result['msg']['validation_error']) ) :
                                                                $msg_error      .= l(' XML Validation Error. ');
                                                        endif;
                                                        if ( !empty($result['msg']['mapping_category']) ) :
                                                                $msg_error      .= l(" Mapping category. ");
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = 'compress';
                                                $result['batch_id'] = $product->batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $json               = @json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "upload"   :   $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $product->batch_id  = $batch_id;
                                                $proc_rep           = new report_process($userdata->username, $batch_id, l('Process Export : Upload file to eBay site'), true, true, true, 'export_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_products_site_'.$site_id); 
                                                $proc_rep->set_priority(2); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->export_bulk();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createjob']) ) :
                                                                $msg_error      .= l(' Cannot create job. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createfile']) ) :
                                                                $msg_error      .= l(' Ebay feed transfer failed. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_startupload']) ) :
                                                                $msg_error      .= l(" Ebay feed process failed. ");
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = 'upload';
                                                $result['batch_id'] = $batch_id;
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "download" :   $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $product->batch_id  = $batch_id;
                                                $proc_rep           = new report_process($userdata->username, $batch_id, l('Process Export : Download file response from eBay'), true, true, true, 'export_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_products_site_'.$site_id); 
                                                $proc_rep->set_priority(3); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $proc_rep->set_max_min_task(0, 100); 
                                                $result             = $product->export_download($proc_rep);
                                                $result['method']   = 'download';
                                                $result['batch_id'] = $batch_id;
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
	                                                $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['job_failure']) ) :
                                                                $msg_error      .= l(" Upload Job Failure. ");
	                                                endif;
	                                                if ( !empty($result['msg']['bulk_download']) ) :
                                                                $msg_error      .= l(" Download Response Failed. ");
	                                                endif; 
                                                        
                                                        $proc_rep->set_error_msg($msg_error);
	                                                $fail = 0;
	                                                $error = true;
                                                endif;
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                    endswitch;
            break;
            case "ends" :
                    require dirname(__FILE__). "/../../application/libraries/Ebay/ebayexport.php";
                    switch ( $method ) :
                            case "compress" :   $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Process Delete : eBay compress files XML'), true, true, true, 'delete_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('delete_products_site_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->export_end_compress();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['empty_products']) ) :
                                                                $msg_error      .= l('Empty Product. ');
                                                        endif;
                                                        if ( !empty($result['msg']['validation_error']) ) :
                                                                $msg_error      .= l('XML Validation Error. ');
                                                        endif;
                                                        if ( !empty($result['msg']['mapping_category']) ) :
                                                                $msg_error      .= l('Mapping Category. ');
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['batch_id'] = $product->batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $result['method']   = 'compress';
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "deactivate" : $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Process Delete : eBay compress files XML'), true, true, true, 'delete_deactivate_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('delete_deactivate_products_site_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->export_zero_active_end_compress();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['empty_products']) ) :
                                                                $msg_error      .= l('Empty Product. ');
                                                        endif;
                                                        if ( !empty($result['msg']['validation_error']) ) :
                                                                $msg_error      .= l('XML Validation Error. ');
                                                        endif;
                                                        if ( !empty($result['msg']['mapping_category']) ) :
                                                                $msg_error      .= l('Mapping Category. ');
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['batch_id'] = $product->batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $result['method']   = 'compress';
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "stock" :      $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Process Delete : eBay compress files XML'), true, true, true, 'delete_stock_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('delete_stock_products_site_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->export_out_of_stock_end_compress();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['empty_products']) ) :
                                                                $msg_error      .= l('Empty Product. ');
                                                        endif;
                                                        if ( !empty($result['msg']['validation_error']) ) :
                                                                $msg_error      .= l('XML Validation Error. ');
                                                        endif;
                                                        if ( !empty($result['msg']['mapping_category']) ) :
                                                                $msg_error      .= l('Mapping Category. ');
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['batch_id'] = $product->batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $result['method']   = 'compress';
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "full" :       $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Process Delete : eBay compress files XML'), true, true, true, 'delete_full__products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('delete_full_products_site_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $proc_rep->set_max_min_task(0, 100); 
                                                $result             = $product->export_product_active_end_compress($proc_rep);
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['empty_products']) ) :
                                                                $msg_error      .= l('Empty Product. ');
                                                        endif;
                                                        if ( !empty($result['msg']['validation_error']) ) :
                                                                $msg_error      .= l('XML Validation Error. ');
                                                        endif;
                                                        if ( !empty($result['msg']['mapping_category']) ) :
                                                                $msg_error      .= l('Mapping Category. ');
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['batch_id'] = $product->batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $result['method']   = 'compress';
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "upload"   :   $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $product->batch_id  = $batch_id;
                                                $proc_rep           = new report_process($userdata->username, $batch_id, l('Process Delete : Upload file to eBay site'), true, true, true, 'delete_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('delete_products_site_'.$site_id); 
                                                $proc_rep->set_priority(2); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->export_bulk();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createjob']) ) :
                                                                $msg_error      .= l(' Cannot create job. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createfile']) ) :
                                                                $msg_error      .= l(' Ebay feed transfer failed. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_startupload']) ) :
                                                                $msg_error      .= l(" Ebay feed process failed. ");
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = 'upload';
                                                $result['batch_id'] = $batch_id;
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "download" :   $product            = new ebayexport(array($user, $user_id, $site_id));
                                                $product->batch_id  = $batch_id;
                                                $proc_rep           = new report_process($userdata->username, $batch_id, l('Process Delete : Download file response from eBay'), true, true, true, 'delete_products_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('delete_products_site_'.$site_id); 
                                                $proc_rep->set_priority(3); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $proc_rep->set_max_min_task(0, 100); 
                                                $result             = $product->export_download($proc_rep);
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
	                                                $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['job_failure']) ) :
                                                                $msg_error      .= l(" Upload Job Failure. ");
	                                                endif;
	                                                if ( !empty($result['msg']['bulk_download']) ) :
                                                                $msg_error      .= l(" Download Response Failed. ");
	                                                endif;
	                                                
                                                        $proc_rep->set_error_msg($msg_error);
	                                                $fail = 0;
	                                                $error = true;
                                                endif;
                                                $result['method']   = 'download';
                                                $result['batch_id'] = $batch_id;
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => !empty($result) ? $result : ''));
                            break;
                    endswitch;
            break;
            case "offers" :
                    require dirname(__FILE__). "/../../application/libraries/Ebay/ebayofferexport.php";
                    switch ( $method ) :
                            case "compress_file" :   
                                                $product            = new ebayofferexport(array($user, $user_id, $site_id));
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Offer Export : eBay compress files XML'), true, true, true, 'export_offers_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_offers_site_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->export_compress(false, $type_use, $proc_rep);
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['empty_products']) ) :
                                                                $msg_error      .= l(' Empty products. ');
                                                        endif;
                                                        if ( !empty($result['msg']['validation_error']) ) :
                                                                $msg_error      .= l(' XML Validation Error. ');
                                                        endif;
                                                        if ( !empty($result['msg']['mapping_category']) ) :
                                                                $msg_error      .= l(" Mapping category. ");
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = 'compress_file';
                                                $result['batch_id'] = $product->batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $json               = @json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "compress" :   $product            = new ebayofferexport(array($user, $user_id, $site_id));                                              
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Offer Export : eBay compress files XML'), true, true, true, 'export_offers_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_offers_site_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);

                                                $result             = $product->export_compress();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['empty_products']) ) :
                                                                $msg_error      .= l(' Empty products. ');
                                                        endif;
                                                        if ( !empty($result['msg']['validation_error']) ) :
                                                                $msg_error      .= l(' XML Validation Error. ');
                                                        endif;
                                                        if ( !empty($result['msg']['no_export']) ) :
                                                                $msg_error      .= l(" No online product. ");
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = 'compress';
                                                $result['batch_id'] = $product->batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $json               = @json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "upload"   :   $product            = new ebayofferexport(array($user, $user_id, $site_id));
                                                $product->batch_id  = $batch_id;
                                                $proc_rep           = new report_process($userdata->username, $batch_id, l('Offer Export : Upload file to eBay site'), true, true, true, 'export_offers_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_offers_site_'.$site_id); 
                                                $proc_rep->set_priority(2); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->export_bulk();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createjob']) ) :
                                                                $msg_error      .= l(' Cannot create job. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createfile']) ) :
                                                                $msg_error      .= l(' Ebay feed transfer failed. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_startupload']) ) :
                                                                $msg_error      .= l(" Ebay feed process failed. ");
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = 'upload';
                                                $result['batch_id'] = $batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "download" :   $product            = new ebayofferexport(array($user, $user_id, $site_id));
                                                $product->batch_id  = $batch_id;
                                                $proc_rep           = new report_process($userdata->username, $batch_id, l('Offer Export : Download file response from eBay'), true, true, true, 'export_offers_site_'.$site_id, $method );
                                                $proc_rep->set_process_type('export_offers_site_'.$site_id); 
                                                $proc_rep->set_priority(3); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $proc_rep->set_max_min_task(0, 100); 
                                                $result             = $product->export_download($proc_rep);
                                                $result['method']   = 'download';
                                                $result['batch_id'] = $batch_id;

                                                if ( !empty($result['msg']) && count($result['msg']) ) :
	                                                $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['job_failure']) ) :
                                                                $msg_error      .= l(" Upload Job Failure. ");
	                                                endif;
	                                                if ( !empty($result['msg']['bulk_download']) ) :
                                                                $msg_error      .= l(" Download Response Failed. ");
	                                                endif;
	                                                
                                                        $proc_rep->set_error_msg($msg_error);
	                                                $fail = 0;
	                                                $error = true;
                                                endif;
                                                
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                    endswitch;
            break;
            case "shipment" :
                    require dirname(__FILE__). "/../../application/libraries/Ebay/ebayshipment.php";
                    switch ( $method ) :
                            case "compress" :   $product            = new ebayshipment(array($user, $user_id, $site_id));                                              
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Export tracking number : eBay compress files XML'), true, true, true, 'import_orders_eBay_'.$site_id, $method );
                                                $proc_rep->set_process_type('import_orders_eBay_'.$site_id); 
                                                $proc_rep->set_priority(1); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);

                                                $result             = $product->getTrackingFromStore();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['no_verified']) ) :
                                                                $msg_error      .= l(' Verified not found. ');
                                                        endif;
                                                        if ( !empty($result['msg']['no_shippedorders_link']) ) :
                                                                $msg_error      .= l(' go to verified. ');
                                                        endif;
                                                        if ( !empty($result['msg']['no_data']) ) :
                                                                $msg_error      .= l(' Link Error. ');
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = 'compress';
                                                $result['batch_id'] = $product->batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $json               = @json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "upload"   :   $product            = new ebayshipment(array($user, $user_id, $site_id));                                              
                                                $proc_rep           = new report_process($userdata->username, $product->batch_id, l('Export tracking number : Upload file to eBay site'), true, true, true, 'import_orders_eBay_'.$site_id, $method );
                                                $product->batch_id  = $batch_id;
                                                $proc_rep->set_process_type('import_orders_eBay_'.$site_id); 
                                                $proc_rep->set_priority(2); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $result             = $product->export_bulk();
                                                if ( !empty($result['msg']) && count($result['msg']) ) :
                                                        $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createjob']) ) :
                                                                $msg_error      .= l(' Cannot create job. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_createfile']) ) :
                                                                $msg_error      .= l(' Ebay feed transfer failed. ');
                                                        endif;
                                                        if ( !empty($result['msg']['bulk_startupload']) ) :
                                                                $msg_error      .= l(" Ebay feed process failed. ");
                                                        endif;
                                                        
                                                        $proc_rep->set_error_msg($msg_error); 
                                                        $fail = 0;
                                                        $error = true;
                                                endif;
                                                $result['method']   = 'upload';
                                                $result['batch_id'] = $batch_id;
                                                $result['error_msg'] = isset($result['msg']) ? $result['msg'] : '';
                                                if(!empty($result['error_msg'])) $result['error_msg'] = l($result['error_msg']);
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                            case "download" :   $product            = new ebayshipment(array($user, $user_id, $site_id));
                                                $product->batch_id  = $batch_id;
                                                $proc_rep           = new report_process($userdata->username, $batch_id, l('Export tracking number : Download file response from eBay'), true, true, true, 'import_orders_eBay_'.$site_id, $method );
                                                $proc_rep->set_process_type('import_orders_eBay_'.$site_id); 
                                                $proc_rep->set_priority(3); 
                                                $proc_rep->set_status_msg(l('Start : ').l($method)); 
                                                $proc_rep->set_process_comments($method);
                                                $proc_rep->set_max_min_task(0, 100); 
                                                $result             = $product->export_download($proc_rep);
                                                $result['method']   = 'download';
                                                $result['batch_id'] = $batch_id;

                                                if ( !empty($result['msg']) && count($result['msg']) ) :
	                                                $msg_error = '';
                                                        if ( !empty($result['msg']['empty_token']) ) :
                                                                $msg_error      .= l(' Go to authentication. ');
                                                        endif;
                                                        if ( !empty($result['msg']['job_failure']) ) :
                                                                $msg_error      .= l(" Upload Job Failure. ");
	                                                endif;
	                                                if ( !empty($result['msg']['bulk_download']) ) :
                                                                $msg_error      .= l(" Download Response Failed. ");
	                                                endif;
	                                                
                                                        $proc_rep->set_error_msg($msg_error);
	                                                $fail = 0;
	                                                $error = true;
                                                endif;
                                                $json               = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $result));
                            break;
                    endswitch;
            break;
    endswitch;
   
    echo $json;
    if ( !empty($proc_rep) ) :
            $proc_rep->finish_task(isset($fail) ? $fail : 1);
    endif;