<?php

/* Update every day */

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.validValues.update.php';

class AmazonExcuteFileValidValues extends UpdateValidValues
{
    public function __construct() {
        parent::__construct();
    }
    
    public function Dispatch(){
        $this->ExecuteFileValidValues();
    }
            
}

$update_validvalue = new AmazonExcuteFileValidValues();
$update_validvalue->Dispatch();