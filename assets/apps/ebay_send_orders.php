<?php

require_once(dirname(__FILE__) . '/FeedbizImport/config/orders.php');
require_once(dirname(__FILE__) . '/simple_call_ws.php');

$error  = null ;
$output = null;

$data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');

$data = json_encode(array('id_order' => "18", 'username' => "Test1", 'token' => "805070727", 'base_url' => "http://192.168.1.10/opencart1564/index.php?route=feedbiz/orders"));

$userdata = json_decode($data);

if(!isset($userdata->username) || empty($userdata->username))
{
    echo json_encode( array('error' => 'Username wrong') ) ; 
    exit;
} 

$user = str_replace(" ", "_", $userdata->username);
$token = str_replace(" ", "_", $userdata->token);
//
if(!isset($user) || empty($user))
{
   $error = sprintf('User Error, %s. ', $user) ;
}   
else 
{

    $params = array(
        'login'  => $user,
        'token' => $token,
        'id_order' => $userdata->id_order,
    );

    $order = simpleCallWS($params, 'getOrders', 'GET', false);
    
    //var_dump($order); die;
//    
////    $json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $order) ) ;
////
////echo $json ;
////    
//    if($order )
//    {
        $url = $userdata->base_url. '&fbuser=' . $user . '&fbtoken=' . $token;
        $page = post_orders_ws($order, $url);
         
        if(isset($page->Status->Code))
        {
            if(strval($page->Status->Code) == 1)
            {
                $orders = new Orders($user);
                $result = $orders->updateOrderStatus($userdata->id_order, '1');

                if($result)
                {
                    $output = strval($page->Status->Message);;
                }
                else 
                {
                    $error = sprintf('Update Status Error') ;
                }
            }
            else 
            {
                $error = strval($page->Status->Message);
            }
        }
        else 
        {

            $error = sprintf('Send Error') ;
        }
//    }
//    else
//    {
//        $error = sprintf('XML Error') ;
//    }

}



$json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $output) ) ;

echo $json ;