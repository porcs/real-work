<?php

//https://client.feed.biz/assets/apps/amazon_get_orders_cron.php?debug=1&user_id=9&user_name=u00000000000009&ext=.fr&id_shop=1&shop_name=Chaussmoi

$debug = isset($_GET['debug']) ? (bool) $_GET['debug'] : false;

chdir(dirname(__FILE__)); 
require_once dirname(__FILE__) . '/../../libraries/newrelic_report.php';
require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.import.orders.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

if(!$debug) {
    
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]=='' 
	|| !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4])
	|| !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]=='' || !isset($argv[7]) || $argv[7]=='' || !is_numeric($argv[7])
        ){
	die( 'No input data!');
    }

    // Test 
    $cron_send_orders = $argv[7];

    // Get argv from url
    $info = array(
	'user_id' => $argv[1],
	'user_name' => $argv[2],
	'ext' => $argv[3],
	'id_shop' => $argv[4],
	'shop_name' => str_replace('_',' ',$argv[5]),
	'id_marketplace' => $argv[6],
	'recheck' => isset($argv[8]) ? $argv[8] : null,
	'next_time' => 20,
	'owner' => 'system',
	'action' => 'get_order'
    );
    
} else {
    
    // debug
    $cron_send_orders = 1;

    $info = array(
        'user_id' => $_GET['user_id'],
        'user_name' => $_GET['user_name'],
        'ext' => $_GET['ext'],
        'id_shop' => $_GET['id_shop'],
        'shop_name' => $_GET['shop_name'], 
        'id_marketplace' => 2,
        'recheck' => isset($_GET['recheck']) ? $_GET['recheck'] : null,
        'next_time' => 60,
        'owner' => 'system',
        'action' => 'get_order'
    );
}

// Get userdata fromdatabase
$data = AmazonUserData::get($info);

if($debug){
    echo 'User Data : <pre>' . print_r($data, true) . '</pre>';
}

if(!isset($data) || empty($data) ){
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {

    // Set history log
    $info['countries'] = $data['countries'];
    AmazonHistoryLog::set($info);

    $object = json_decode(json_encode($data), FALSE);

    $mode = array();
    $mode['datefrom'] = date('d-m-Y',strtotime('-1 day'));
    $mode['dateto'] = date('d-m-Y');
    $mode['status'] = 'All';    
    $mode['recheck'] = isset($info['recheck']) ? $info['recheck'] : null;
    
    if($debug){
	echo 'Options #1: <pre>' . print_r($mode, true) . '</pre>';
    }

    $object->option = rawurlencode((json_encode($mode)));  
    $object->cron_send_orders = $cron_send_orders;  
    
    $amazon_get_order = new AmazonImportOrders($object, $debug, true);
    $amazon_get_order->Dispatch(); 
    
}

