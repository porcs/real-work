<?php

require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.scheme.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/classes/mirakl.history.log.php';

require_once dirname(__FILE__).'/../../application/libraries/Mirakl/hook/mirakl.additional_field.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/hook/mirakl.attribute.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/hook/mirakl.condition.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/hook/mirakl.hierarchy.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/hook/mirakl.value.php';
require_once dirname(__FILE__).'/../../application/libraries/Mirakl/hook/mirakl.carrier.php';


$debug = false;

if (!$debug) {

    if (
            !isset($argv[1]) || $argv[1] == '' || !is_numeric($argv[1]) ||
            !isset($argv[2]) || $argv[2] == '' || !is_numeric($argv[2]) ||
            !isset($argv[3]) || $argv[3] == '' || !is_numeric($argv[3]) ||
            !isset($argv[4]) || $argv[4] == '' || !is_numeric($argv[4]) ||
            !isset($argv[5]) || $argv[5] == '') {
        die('No input data!');
    }
} else {

    $argv[1] = 53;
    $argv[2] = 1;
    $argv[3] = 10;
    $argv[4] = 2;
    $argv[5] = 'value'; 
}

// init var
$id_user = $argv[1];
$id_shop = $argv[2];
$sub_marketplace = $argv[3];
$id_country = $argv[4];
$type = $argv[5]; // additional | attribute | condition | hierarchy | value | carrier

// get data
$mirakl_scheme = new MiraklScheme();
$shop_info = $mirakl_scheme->getShopInfo($id_user);
$get_config = $mirakl_scheme->getMiraklConfiguration($id_user, $sub_marketplace, $id_country, $id_shop);

if (empty($get_config)) {
    die('No config data!');
}

if ($type == 'additional') {

    $class = new MiraklAdditional($get_config);
    $class->getAdditional();

    $next_time = 1440; // Recommended: 1/day
    $action = MiraklHistoryLog::getConfig($type);
    //
} else if ($type == 'attribute') {

    $class = new MiraklAttribute($get_config);
    $class->getAttribute();

    $next_time = 720; // Recommended: 12/hour
    $action = MiraklHistoryLog::getConfig($type);
    //
} else if ($type == 'condition') {

    $class = new MiraklCondition();
    $class->getCondition($get_config);

    $next_time = 1440;
    $action = MiraklHistoryLog::getConfig($type);

    // condition not have api. Read data in the config.ini
} else if ($type == 'hierarchy') {

    $class = new MiraklHierarchy($get_config);
    $class->getHierarchy();

    $next_time = 720; // Recommended: 12/hour
    $action = MiraklHistoryLog::getConfig($type);
    //
} else if ($type == 'value') {

    $class = new MiraklValue($get_config);
    $class->getValue();

    $next_time = 1440 * 16; // Recommended: 16/day
    $action = MiraklHistoryLog::getConfig($type);
    //
} else if ($type == 'carrier') {

    $class = new MiraklCarrier($get_config);
    $class->getCarrier();

    $next_time = 1440; // Recommended: 1/day
    $action = MiraklHistoryLog::getConfig($type);
    //
} else {
    print_r('invalid type');
    exit;
    // invalid type
}


$log_history = array(
    'user_id' => $id_user,
    'ext' => $get_config['ext'],
    'shop_name' => isset($shop_info['name']) ? $shop_info['name'] : '',
    'countries' => $get_config['countries'],
    'sub_marketplace' => $sub_marketplace,
    'next_time' => $next_time,
    'owner' => 'system',
    'action' => $action,
    'transaction' => 1 // check status in woking for check next time.
);

// Set history log
MiraklHistoryLog::set($log_history);

// response to node
//echo json_encode($results);



/* 
 * doc next tine 
 *  1440 = 24h = 1day
 *  720 = 12h
 *  
 *  
 * 
*/