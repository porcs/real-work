<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.advertising.product.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;
if($debug) {
    #http://ndb.feed.dev/assets/apps/amazon_advertising_cron.php?debug=true&id_customer=26&user_name=u00000000000026&ext=.com&id_shop=1&shop_name=FOXCHIP
    $info = array(
	'user_id' => $_GET['id_customer'],
	'user_name' => $_GET['user_name'],
	'ext' => $_GET['ext'],
	'id_shop' => $_GET['id_shop'],
	'shop_name' => $_GET['shop_name'],
	'id_marketplace' => 2,
	'next_time' => 20,
	'owner' => 'system',
	'action' => 'advertising_product',
    );  
} else {
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]=='' 
	|| !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
	|| !isset($argv[5]) || $argv[5]==''){
	die( 'No input data!');
    }
    // Get argv from url
    $info = array(
	'user_id' => $argv[1],
	'user_name' => $argv[2],	
	'ext' => $argv[3],
	'id_shop' => $argv[4],
	'shop_name' => str_replace('_',' ',$argv[5]),
	'id_marketplace' => $argv[6],
	'next_time' => 20,
	'owner' => 'system',
	'action' => 'advertising_product',
    );
}

// Get userdata fromdatabase
$data = AmazonUserData::get($info, false, true);

if(!isset($data) || empty($data) ){
    $info['countries'] = $info['ext'];

    // Set history log
    AmazonHistoryLog::set($info);

    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;

} else {
    $data['shop_name'] = $info['shop_name'];
    $info['countries'] = $data['countries'];

    // Set history log
    AmazonHistoryLog::set($info);

    $users = json_decode(json_encode($data), false);
    $cron = true;
    $amazon_advertising = new AmazonAdvertisingProduct($users, $cron, $debug);
    $amazon_advertising->Dispatch();
}