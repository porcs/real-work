<?php
header('Content-Type: text/html; charset=utf-8');
require_once(dirname(__FILE__) . '/FeedbizImport/config/products.php');
require_once  (dirname(__FILE__).'/../../application/libraries/UserInfo/configuration.php');
require_once(dirname(__FILE__) . '/../../application/libraries/tools.php');
$error  = null ;
$output = null ;
$shopname = null;
$id_shop = null;
$shop_type = 'Woocommerce';         
$feed_type = 'Product';
$process_title =  'Importing your products';
$process_type =  'import product';
    $arr_setting = array(
        'Attributes'      => 'Attribute',
        'Carriers'        => 'Carrier',
        'Categories'      => 'Category',
        'Conditions'      => 'Conditions',
        'Currencies'      => 'Currency',
        //'Features'        => 'Feature',
        'Language'        => 'Language',
        'Manufacturers'   => 'Manufacturer',
        'Suppliers'       => 'Supplier',
        'Taxes'           => 'Tax',
        'Units'           => 'Unit',
//        'Products'        => 'Product',
    );
    $arr_content = array(
        'Products'        => 'Product',
//        'Features'        => 'Feature',
//        'Attributes'      => 'Attribute',
    );
    
//$argv[1] = json_encode(
//    array(
//        'username' => "Praew",
//        //'token' => '5768f01802597c541df2be0a9659286d',
//        'gdata' => array(
////            'fileurl' => "http://dev.prestashop-less.biz/modules/feedbiz/functions/products.php?fbtoken=581209142",
////            'setting_fileurl' => "http://dev.prestashop-less.biz/modules/feedbiz/functions/settings.php?fbtoken=581209142",
//            'fileurl' => dirname(__FILE__) . '/users/Praew/products.xml',
//            'setting_fileurl' => dirname(__FILE__) . '/users/Praew/settings.xml',
//            //'ws_token'   => "5768f01802597c541df2be0a9659286d",
//        )
//));

$data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($argv[1])), null, 'UTF-8');
$datas = json_decode($data);
$userdata = $datas;
$user = str_replace(" ", "_", $userdata->username);
$dev_log = new dev_log($user);
set_error_handler('var_dump', 0);
$from_task = true;
restore_error_handler();
$time = date("Y-m-d H:i:s");

$uinfo = new UserConfiguration(); 
$out = $uinfo->getUserByUsername($user);
$user_id = isset($out['id_customer'])?$out['id_customer']:0;
$Language = $uinfo->getUserDefaultLaguage($user_id);//lang
load('my_feeds', $Language);//lang
$process_title = l($process_title);

if (isset($user) && !empty($user)) {

    if (!empty($userdata->gdata->fileurl)) {        
	
        $Convert = new ConvertXMLToFeedBizXML($user);

        //get setting xml
        $xml = $Convert->getSetting($userdata->gdata->setting_fileurl, $user);		    

        if (!isset($xml) || empty($xml) || (isset($xml['error']) && $xml['error'] == true)) {
            if ($xml['error']) {
                $error = $xml['message'];
            } else {
                $error = sprintf('can_not_load_setting_data', $user);
            }
        } else {

            $arr_class_name = $arr_setting;
            $oXml = simplexml_load_string($xml['xml']);

            if (!isset($oXml) || empty($oXml) || !$oXml){
                $error .= sprintf('Data feed wrong.');
            }

            if (isset($oXml->Status->Code) && ($oXml->Status->Code == 0|| $oXml->Status->Code == '03')) {
                $error .= sprintf('Access Denied.');
            }
            
            if (isset($oXml->Status->Code) && $oXml->Status->Code == 01 && !empty($oXml->Status->Message)) {
                $error = sprintf('11-00003');
            }

            //shop name
            $shopname = $oXml['ShopName'];
            
            if ($error === null) {
                
                if (isset($shopname) && !empty($shopname)) {
                    
                    $shop = new Shop($user);
                    $id_shop = $shop->importData($shopname, $time, $userdata->gdata->fileurl);
                    
                    $table_list = array();

                    foreach ($arr_class_name as $xml_key => $class_name) {
                        
                        if (method_exists($class_name, 'importData')) {
                            $class = new $class_name($user);

                            if (isset($oXml->$xml_key)) {
                                
                                $oXml->$xml_key->id_shop = $id_shop;
                                $oXml->$xml_key->shopname = $shopname;
                                $oXml->$xml_key->source = $shop_type;

                                $result = $class->import($oXml->$xml_key, $time);

                                if (isset($result['error'])){
                                    $error .= $result['error'];
                                }

                                //List table for delete product not use
                                if (isset($result['table_list']))
                                    $table_list[] = $result['table_list'];
                            } else
                                $error .= sprintf('No data export %s. ', $class_name);
                        }
                        else {
                            $error .= sprintf('Data table %s - Cannot be imported. ', $class_name);
                        }
                    }

                    if (!strlen($error))
                        $output = sprintf('Import successful. Time : ' . $time);
                }
                else {
                    $error = 'Incorrect Shop Name.';
                }
            }
        }
        
        if ($error !== null || $shopname === null || $id_shop === null || (isset($argv[2]) && $argv[2] == 'skip')) {
            $json = json_encode(array('error' => $error, 'pass' => $error ? false : true, 'output' => $output, 'shopname' => (string) $shopname, 'id_shop' => (int) $id_shop));
            $dev_log->record_dev_log($json);
            echo $json;
            exit;
        }  
        $error = error_get_last();
        if (!empty($error)) {
            $dev_log->record_dev_log(json_encode($error));
        }
        
        $error = '';
        //import log for global var
        $import_log = new ImportLog($user);
        $no_total = 0;
        $no_success = 0;
        $no_error = 0;
        $no_warning = 0;
        $batchID = uniqid();
        $source = null;
        $cur_page = 0;
        //get product xml
        $status = null;
        $loop_at = 0;
        
        /* process report conf */
        $proc_rep = new report_process($user, $batchID, $process_title, false, true);
        $proc_rep->set_process_type($process_type);
        $proc_rep->set_shop($id_shop, $shopname);
        $connect_error = '';
        /* process report conf end */
        
        $no_current_id = 0;
        
        // Clear Logs
        $log = new ImportLog($user);

        if(!$log->empty_log($id_shop)){
            $error = 'Delete log error.';
        }
        
        do {

            $xml = $Convert->getSingleContent($userdata->gdata->fileurl, $user);
            if (!isset($xml) || empty($xml) || (isset($xml['error']) && $xml['error'] == true)) {
                if ($xml['error']) {
                    $error = $xml['message'];
                } else {
                    $error = sprintf('can_not_load_data', $user);
                }
                $status = 0;
            } else {
                //check page continuous
                if (isset($xml['cur_page'])) {
                    if ($cur_page + 1 != (int) $xml['cur_page']) {
                        $connect_error = sprintf('data_skip_page', $user);
                        break;
                    } else {
                        $cur_page = (int) $xml['cur_page'];
                    }
                }

                if ($no_current_id == $xml['current'] && $xml['total'] != 0) {
                    $error = sprintf('duplicate_load_data', $user);
                    break;
                } else {
                    $no_current_id = $xml['current'];
                }
                $status = $xml['status'];
                $arr_class_name = $arr_content;

                $oXml = simplexml_load_string($xml['xml']);
                
                if (!isset($oXml) || empty($oXml) || !$oXml)
                    $error .= sprintf('Data feed wrong.');

                if (isset($shopname) && !empty($shopname)) {
                    foreach ($arr_class_name as $xml_key => $class_name) {
                        if (method_exists($class_name, 'importData')) {
                            $class = new $class_name($user);

                            if (isset($oXml->$xml_key)) {
                                $oXml->$xml_key->id_shop = $id_shop;
                                $oXml->$xml_key->shopname = $shopname;
                                $oXml->$xml_key->source = $shop_type;

                                $result = $class->import($oXml->$xml_key, $time,$status,$from_task);

                                if (isset($result['error']))
                                    $error .= $result['error'];

                                //List table for delete product not use
                                if (isset($result['table_list']))
                                    $table_list[] = $result['table_list'];
                            } else
                                $error .= sprintf('No data export %s. ', $class_name);
                        }
                        else {
                            $error .= sprintf('Data table %s - Cannot be imported. ', $class_name);
                        }
                    }

                    if (!strlen($error))
                        $output = sprintf('Import successful. Time : ' . $time);
                }
                else {
                    $error = 'Incorrect Shop Name.';
                }

                /* process report conf */
                if ($loop_at == 0) {
                    $proc_rep->set_max_min_task($xml['max'], $xml['min']);
                }
                $proc_rep->set_running_task($xml['current']);
                /* process report conf */
            }

            $loop_at++;
        } while ($status == -1);

        $data = array(
            'batch_id' => $batchID,
            'shop' => ObjectModel::escape_str($shopname),
            'source' => ObjectModel::escape_str($source),
            'type' => $feed_type,
            'no_total' => (int) $no_total,
            'no_success' => (int) $no_success,
            'no_warning' => (int) $no_warning,
            'no_error' => (int) $no_error,
            'datetime' => $time,
        );

        $import_log->insert_log($data);
        
        /* process report conf */
        if ($error != '') {
            $proc_rep->set_error_msg($error);
        } else {
            $proc_rep->finish_task();
        }
        /* process report conf */
    
    } else {
        $error = 'Incorrect Urls.';
    }

} else {
    $error = 'username_wrong';
}

$json = json_encode(array(
            'error' => $error, 
            'pass' => $error ? false : true, 
            'output' => $output, 
            'shopname' => (string) $shopname, 
            'id_shop' => (int) $id_shop, 
            'connect_error' => $connect_error)
        );

$error = error_get_last();

if (!empty($error)) {
    $dev_log->record_dev_log(json_encode($error));
}

echo $json;