<?php 

define('BASEPATH', '');
chdir(dirname(__FILE__)); 

require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.feeds.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

//https://qa.feed.biz/assets/apps/amazon_create_cron.php?debug=1&user_id=26&user_name=u00000000000026&ext=.fr&id_shop=1&shop_name=FOXCHIP&id_marketplace=2

 $debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;

if($debug){
    $info = array(
        'user_id' =>$_GET['user_id'],
        'user_name' => $_GET['user_name'],
        'ext' => $_GET['ext'],
        'id_shop' => $_GET['id_shop'],
        'shop_name' => $_GET['shop_name'],
        'id_marketplace' => $_GET['id_marketplace'],
        'next_time' => 1440,
        'owner' => 'system',
        'action' => 'create',
    );
} else {
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]==''
        || !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
        || !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]==''){
        die( 'No input data!');
    }

    // Get argv from url
    $info = array(
        'user_id' => $argv[1],
        'user_name' => $argv[2],
        'ext' => $argv[3],
        'id_shop' => $argv[4],
        'shop_name' => str_replace('_',' ',$argv[5]),
        'id_marketplace' => $argv[6],
        'next_time' => 1440,
        'owner' => 'system',
        'action' => 'create',
    );
}

// Get userdata fromdatabase
$data = AmazonUserData::get($info);
$data['shop_name'] = $info['shop_name'];

if(!isset($data) || empty($data) ){
    
    $info['countries'] = $info['ext'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    $info['countries'] = $data['countries'];
    
    // Set history log
    AmazonHistoryLog::set($info);

    $data['mode'] = rawurlencode((json_encode(array('send_image' => true))));
    $object = json_decode(json_encode($data), FALSE);   
    $cron = true;
    
    // Creation
    $amazon_feeds = new AmazonFeeds($object, AmazonDatabase::CREATE, $debug, $cron);
    $amazon_feeds->FeedsData();
    
}