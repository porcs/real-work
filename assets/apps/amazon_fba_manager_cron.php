<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.fba.stock.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

$debug = isset($_GET['debug']) ? (bool)$_GET['debug'] : false;
   
if($debug) {
   
    #https://qa.feed.biz/assets\apps\amazon_fba_manager_cron.php?debug=true&id_customer=26&user_name=u00000000000026&ext=.es&id_shop=1&shop_name=FOXCHIP
    #https://client.feed.biz/assets\apps\amazon_fba_manager_cron.php?debug=true&id_customer=9&user_name=u00000000000009&ext=.fr&id_shop=1&shop_name=Chaussmoi
    //php amazon_fba_manager_cron.php 9 u00000000000009 .fr 1 Chaussmoi 2 25
    
    $info = array(
	'user_id' => $_GET['id_customer'],
	'user_name' => $_GET['user_name'],
	'ext' => $_GET['ext'],
	'id_shop' => $_GET['id_shop'],
	'shop_name' => $_GET['shop_name'],
	'id_marketplace' => 2,
	'next_time' => 20,
	'owner' => 'system',
	'action' => 'fba_manager',
	'days' => isset($_GET['days']) && !empty($_GET['days']) ? $_GET['days'] : null,
    );  

    /*if($_GET['id_customer'] == 9){
        $debug = false;
    }*/
} else {
    
    if(!isset($argv[1]) || $argv[1]=='' || !is_numeric($argv[1])|| !isset($argv[2]) || $argv[2]=='' 
	|| !isset($argv[3]) || $argv[3]=='' || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4] )
	|| !isset($argv[5]) || $argv[5]==''|| !isset($argv[6]) || $argv[6]==''){
	die( 'No input data!');
    }
    // Get argv from url
    $info = array(
	'user_id' => $argv[1],
	'user_name' => $argv[2],	
	'ext' => $argv[3],
	'id_shop' => $argv[4],
	'shop_name' => str_replace('_',' ',$argv[5]),
	'id_marketplace' => $argv[6],
	'next_time' => 20,
	'owner' => 'system',
	'action' => 'fba_manager',
	'days' => isset($argv[7]) && !empty($argv[7]) ? $argv[7] : null,
    );
}

if(isset($info['days']) && $info['days']){
    $info['action'] = 'fba_manager_double';
}

// Get userdata fromdatabase
$data = AmazonUserData::get($info, false, true);

if(!isset($data) || empty($data) ){
    
    $info['countries'] = $info['ext'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $json = json_encode( array('error' => true, 'pass' => false, 'output' => 'Access Denided.') ) ;
    echo $json ; exit;
    
} else {
    
    // we will run fba_manager for master plateform only, when user choose Synchronize Behavior.
    if(isset($data['fba_stock_behaviour']) && in_array($data['fba_stock_behaviour'], array(AmazonFBAStock::FBA_STOCK_SWITCH,AmazonFBAStock::FBA_STOCK_SYNCH))) {

	// skip un - master platform 
	if(!isset($data['fba_master_platform']) || empty($data['fba_master_platform']) || !$data['fba_master_platform']){

	    $json = json_encode( array('error' => false, 'pass' => true, 'output' => 'we will run fba manager for master plateform only.') ) ;
	    echo $json ; exit;

	}
    }

    if($debug &&  isset($_GET['fba_stock_behaviour'])){
        $data['fba_stock_behaviour'] = $_GET['fba_stock_behaviour'];
    }

    $data['shop_name'] = $info['shop_name'];
    $info['countries'] = $data['countries'];
    
    // Set history log
    AmazonHistoryLog::set($info);
    
    $users = json_decode(json_encode($data), false);
    $cron = true;
    $anticipate = true;
    
    $amazon_stocks = new AmazonFBAStock($users, $cron, $debug);
    $amazon_stocks->Dispatch(false, $anticipate, isset($info['days']) ? (int)$info['days'] : 3);
    
}