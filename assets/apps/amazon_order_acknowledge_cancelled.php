<?php

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);  

chdir(dirname(__FILE__));
require_once dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.status.orders.php';
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.userdata.php');
require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.history.log.php');

$debug = isset($_GET['debug']) ? (bool) $_GET['debug'] : false;

if(!$debug) {

    if(!isset($argv[1]) || $argv[1]==''
        || !isset($argv[2]) || $argv[2]=='' || !is_numeric($argv[2])
	|| !isset($argv[3]) || $argv[3]=='' || !is_numeric($argv[3])
        || !isset($argv[4]) || $argv[4]=='' || !is_numeric($argv[4])
        || !isset($argv[5]) || $argv[5]=='' || !is_numeric($argv[5])
        ){
	die('No input data!');
    }

    $info = array(
        'user_name' => $argv[1],
	'id_order' => $argv[2],
	'id_marketplace' => $argv[3],
	'id_shop' => $argv[4],
        'site' => $argv[5],
	'next_time' => 1440,
	'owner' => 'shop',
	'action' => 'update_order_acknowledge'
    );

} else {

    // debug
    $cron_send_orders = 1;

    $info = array(
        'user_name' => $_GET['user_name'],
        'id_order' => $_GET['id_order'],
        'id_marketplace' => 2,
        'id_shop' => $_GET['id_shop'],
        'site' => $_GET['site'],
        'next_time' => 1440,
        'owner' => 'shop',
        'action' => 'update_order_acknowledge'
    );
}
       
// check is Amazon Order
if(!isset($info['id_order']) || empty($info['id_order'])) {
    die('Missing id order');
}

// check is Amazon Order
if(!isset($info['id_marketplace']) || $info['id_marketplace'] <> 2) {
    die('Not Amazon Order');
}

// 1. Get userdata from database
$data = AmazonUserData::get($info, false, true);

if( (!isset($data) || empty($data)) ){

    $info['ext'] =  $info['site'];
    $info['countries'] = $info['site'];
    $info['shop_name'] = $info['user_name'];

    // Set history log
    AmazonHistoryLog::set($info, 'Access Denided.');
    return ;

} else {

    $info['shop_name'] = $data['shop_name'];
    $info['countries'] = $data['countries'];
    $info['ext'] =  $data['ext'];

    // Set history log
    AmazonHistoryLog::set($info);

    // 2. update order acknowledge
    $params = json_decode(json_encode($data), false);
    $amazonOrders = new AmazonStatusOrders($params, $debug, AmazonStatusOrders::AmazonOrderAcknowledgement);
    $amazonOrders->cancelOrder(array('0'=>$info['id_order']));

}