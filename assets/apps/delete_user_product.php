<?php

require_once(dirname(__FILE__) . '/FeedbizImport/config/products.php');

$error  = null ;
$output = null;

//$argv[1] = json_encode(array("username" => "100007406051767"));

$data = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($argv[1])),null,'UTF-8');
$userdata = json_decode($data);
$user = str_replace(" ", "_", $userdata->username);

//////Products//////
$products_database = ObjectModel::truncate_database($user);

if(!$products_database)
   $error = sprintf('Delete database error, %s', $user) ;

if(isset($error) || !empty($error) || $error == null)
    $output = sprintf('Delete database success, %s', $user) ;

$json = json_encode( array('error' => $error, 'pass' => $error ? false : true, 'output' => $output) ) ;

echo $json ;