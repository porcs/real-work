$(function(){
    $('#frmConfig').validationEngine();
    
    $("#frmConfig").bind("jqv.form.result", function(event , errorFound){
        if(!errorFound)
            showWaiting(true);
    });
    
    $('.btn-savechanges').click(function(e){
        $('#frmConfig').submit();
    });
    
    $('.btn-savechanges-pre').click(function(e){
        showWaiting(true);
        $('#frmPreapproval').submit();
    });
});

$(window).load(function(){
    if(preapprovalClick == 'true')
    {
        $('a').each(function(){
            if($(this).attr('data-toggle') == 'tab' && $(this).attr('href') == '#preapproval'){
                $(this).click();
            }
        });
    }
});