$(document).ready(function () {

    $('#stock_report').DataTable({
        "columnDefs": [
            { "visible": false,  "targets": 0 },
            { "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},
        ],
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "asStripeClasses": [ '' ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "stock/carts_page",
            "type": "POST"
        },
        "columns": [   

            { "data": "reference", "bSortable": false, },
            /*{ "data": "product_name", "bSortable": true, "width": "20%", "className": 'text-left'},*/
            { "data": "order_date", "bSortable": false, "width": "20%", "className": 'text-center'},
            { "data": "marketplace", "bSortable": false, "width": "15%", "className": 'text-left'},
            { "data": "detail", "bSortable": false, "width": "30%", "className": 'text-left'},
            { "data": "quantity", "bSortable": false, "width": "10%", "className": 'text-right p-r10'},
            /*{ "data": null, "bSortable": false, "width": "10%", "className": 'text-right',
                 render: function ( data) {
                    if(data.type === "outside_source") {
                        return '<span style="color:#ccc">'+data.movement_quantity+'</span>';
                    } else {
                        return data.movement_quantity;
                    }
                }
            },
            { "data": "balance_quantity", "bSortable": false, "width": "10%", "className": 'text-right' }*/
        ],       
      
        "pagingType": "full_numbers" ,  
        
        "drawCallback": function ( ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5" class="p-l10"><b>'+group+'</b></td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } ); 
    
});