$(document).ready(function (){

	$('head').append('<style>#search-menu ul>li>a{text-transform: capitalize;text-decoration: none;}</style>');
	if($('#search-string').val().length > 0){		
	    $.each($('#menu ul').find('a'), function(i,j){
	    	search_menu($(this), j);
	    });
	}

});

var icon_right = ' <i class="fa fa-angle-right"></i> ';
var search_string = $('#search-string').val();
var menu_string;

if($('#menu-string').val().length > 0){
    menu_string = $.parseJSON(decodeURIComponent($('#menu-string').val()));
}

function search_menu(element, link, child){

	var link_value = $.trim($(link).text());
	link_value = link_value.toLowerCase();

	//search-string
	if( link_value.indexOf(search_string.toLowerCase()) > -1 || child) { 

            if(element.attr('href')){

                    if(element.attr('href') == "" || element.attr('href') == "#"){
                            if(element.parent().find('ul').length > 0){

                                    $.each(element.parent().find('ul').find('a'), function(i,j){

                                    search_menu($(this), j, true);

                                });
                            }

                    } else {

                        var href = element.attr('href');

                        var res = href.replace(base_url, "");
                        var split = res.split("/");
                        var text_show = country = first_menu = '';
                        $last_menu = '';

                        $.each(split, function(s,p){

                                // first
                                if(s == 0){
                                        first_menu = p;
                                }

                                //last
                                if((s+1) == split.length){

                                        $last_menu = p;

                                        if(menu_string){

                                                $.each(menu_string, function(menu,menu_value){	

                                                        if(menu.toLowerCase() === first_menu.toLowerCase()){

                                                                if(jQuery.inArray($last_menu, menu_value.submenu)){

                                                                        $.each(menu_value.submenu, function(key,val){
                                                                                if($last_menu === val.id) {
                                                                                        country = val.country;
                                                                                }
                                                                        });
                                                                }
                                                        }
                                                });
                                        }
                                } 

                                p = p.replace("_", " ");
                                
                                if(link_value.indexOf(p) > -1){
                                    console.log(link_value.indexOf(p));
                                    p = '<span class="text-pink">' + p + '</span>';
                                }
                                
                                text_show = text_show  + ( (country != '') ? ' (' : ( (text_show != '') ? icon_right : '' ) ) + ( ((s+1) != split.length || country == '') ? p : '' ) + country + ( (country != '') ? ')' : '' );

                        });

                        var a = $(link).clone().removeClass('sidebar-item_Fouth-down-link').addClass('more');
                        a.find('i').remove();
                        a.find('span').removeClass('more');
                        a.html(text_show);
                        var main_li = $('#search-menu-main').clone().show().append(a);
                        $('#search-menu ul').append(main_li.removeAttr('id'));
                        $('#search-menu').parent().show();
                    }

                }
                    
	}
}