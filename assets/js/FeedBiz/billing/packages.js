//$(function(){
//    
//        /* Select2 - Advanced Select Controls */
//	if (typeof $.fn.select2 != 'undefined')
//	{
//        var thisurl = $('#thisurl').val();
//            $("#currency").select2({ width: 'resolve' })
//             .on('change',function(e) {
//                    showWaiting();
//                    $.ajax({
//                        url: thisurl,
//                        type: 'POST',
//                        data: { currency: e.val },
//                        error: function(){ return; },
//                        success: function(){
//                              location.reload();
//                        },
//                        results: function (data) { // parse the results into the format expected by Select2.
//                            var objects = [];
//                            $.each(data,function(i,obj){
//                                map[obj.translations__name] = obj;
//                                objects.push({
//                                    "text": obj.translations__name,
//                                    'id': map[obj.translations__name].id
//
//                                });
//                            });
//                            return {results: objects};
//                          }
//                    });
//                });
//        }
//    });
    var thisurl = $('#thisurl').val();
    $("#currency").on('change',function(e){
        showWaiting(true);
        //console.log(this.value);
        $.ajax({
            url: thisurl,
            type: 'POST',
            data: { currency: this.value },
            error: function(){ return; },
            success: function(){
                  location.reload();
            },
            results: function (data) { // parse the results into the format expected by Select2.
                var objects = [];
                $.each(data,function(i,obj){
                    map[obj.translations__name] = obj;
                    objects.push({
                        "text": obj.translations__name,
                        'id': map[obj.translations__name].id

                    });
                });
                return {results: objects};
              }
        });
    });