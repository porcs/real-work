
$('.wizard_btn').on('click', function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    var obj = $(this);
    console.log(url);
    $.colorbox({
        iframe:true , 
        href:url,
        innerWidth:'70%', 
        innerHeight:'80%',
        width:'90%', 
        height:'80%',
        escKey: true, //true
        overlayClose: false, //true
        onLoad: function() {
            //$('#cboxClose').remove(); //hidden
            $.ajax({url:"/users/ajax_switch_popup/0"});
            $('body').css('overflow-y','hidden');
        },
        onClosed:function(){
            //setTimeout(function(){window.location.reload();},1000);
            $('body').css('overflow-y','auto');
            $.ajax({url:"/users/ajax_switch_popup"});
        }
    });
    $(window).resize(function(){
        var WIDTH = $(this).width(); 
        var HEIGHT = $(this).height(); 
        if(WIDTH >= 1280 && HEIGHT >= 768){
            obj.colorbox.resize({innerWidth:'70%', innerHeight:'80%',width:'90%', height:'80%'});
        }else if(WIDTH >= 800 && HEIGHT >= 600){
            obj.colorbox.resize({innerWidth:'95%', innerHeight:'95%',width:'95%', height:'95%'});
        }else{
            obj.colorbox.resize({innerWidth:'100%', innerHeight:'100%',width:'100%', height:'100%'});
        }
    });
});
            