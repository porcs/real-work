$(document).ready(function () {
    var call_countinue = false;
    $('button#save_continue_data').click(function () {
        call_countinue = true;
    });

    $('form').submit(function (e) {
        e.preventDefault();

        $('select[required="required"]').each(function (i, selector) {
            var _parent_selector = $(this).parents('.form-group');
            if ($(selector).val()) {
                _parent_selector.removeClass('has-error');
            } else {
                _parent_selector.addClass('has-error');
            }
        });

        var va = $.map($('form .form-group.has-error:not(:first)'), function (e, i) {
            return $(e).is(':visible');
        });

        if ($.inArray(true, va) !== -1) {
            //$('#tasks .form-group.has-error:first').focus();
            return false;
        }

        $.ajax({
            type: "POST",
            url: base_url + $('#class_call').val() + "/" + $('#function_call').val() + '/' + $('#id_country').val(),
            data: {method: $('#method_value').val()},
            error: function (jqXHR, textStatus, errorThrown) {
                notification_alert(errorThrown, 'bad');
            },
            beforeSend: function (jqXHR, settings) {
                try {
                    var serialize = ajaxBeforeSend();
                } catch (err) {
                }

                if (typeof serialize === 'undefined') {
                    serialize = encodeURIComponent($('form#form-api-setting').serialize());
                }
                settings.data = settings.data ? (settings.data + '&formdata=' + serialize) : settings.data;
            },
            success: function (results) {
                $('#loading-result').hide();
                if ($.trim(results) === 'Success') {
                    if (typeof (module) !== 'undefined' && module === 1) {
                        wizard_start();
                    } else {
                        notification_alert($(".update-" + $('#function_call').val() + "-success").val());
                        setTimeout(function () {
                            if (call_countinue) {
                                location.href = base_url + $('#class_call').val() + "/" + $('#next_page').val();
                            } else {
                                location.href = base_url + $('#class_call').val() + "/" + $('#function_call').val() + '/' + $('#id_country').val();
                            }
                        }, 1200);
                    }
                } else {
                    try {
                        new ajaxSuccessError();
                    } catch (err) {
                    }
                    notification_alert($(".update-" + $('#function_call').val() + "-fail").val(), 'bad');
                }
            }
        });
    });
});