var validation = false;
$(document).ready(function () {

    //Add
    $('#add').click(function () {
        if (!$('.market-profile .form-group.has-error').is(':visible'))
            add_profile();
    });

    if ($('input#popup_status_more').length > 0) {
        console.log($('#popup_more_no_model'));
        if ($('#popup_more_no_model').length > 0) {
        } else {
            add_profile();
        }
    }

    $(document).on('click', '.removeProfile', function (e) {
        edit($(this));
    });

    applyRules();
});

function add_profile()
{
    var cloned = $('#main').clone().show();
    $('#tasks').prepend(cloned);

    cloned.attr('id', '');
    countBlock($('.countProfile'), $('.market-profile'));

    var mm = $.map($('.market-profile'), function (el, index) {
        return $(el).attr('rel');
    });

    var maxnum = Math.max.apply(null, mm);
    var nextnum = maxnum + 1;
    cloned.find('.market-profile input[rel="id_profile"]').val(nextnum);
    cloned.find('.market-profile').attr('rel', nextnum);

    cloned.find('.remove').on('click', function () {
        $('#count').html(parseInt($('#count').text()) - 1);
        $(this).closest('div.market-profiles').remove();
    });

    cloned.find('input[rel=name]').focus();

    //Price Rule
    cloned.find('.addPriceRule').click(function () {
        addPriceRule($(this));
    });

    cloned.find('input.price_rule').on('keydown', function () {
        clearPriceError($(this));
    });

    cloned.find('select[rel="type"]').on('change', function () {
        changePriceRuleType($(this));
    });

    cloned.find('input[rel="recommended_browse_node"]').on('blur', function ()
    {
        recommended_browse_node($(this));
    });

    cloned.find('input[rel="out_of_stock"]').on('blur', function ()
    {
        checkOutOfStock($(this));
    });

    //set select chosen
    cloned.find('select').addClass('search-select').chosen();

    cloned.find('.removeProfile').on('click', function (e) {
        e.preventDefault();
        var $ruleCheckBo = $('.ruleSettings').find('.checkBo').length;

        $(this).closest('.showSelectBlock').remove();

        countBlock($('.countProfile'), $('.showSelectBlock'));

        $('.setBlockMain').addClass('hidden');

        $('.setBlock').remove();
    });

    // Run CheckBo
    cloned.find('input[type="radio"]').each(function () {
        $(this).parents('label').addClass('cb-radio');
    });
    cloned.find('input[type="checkbox"]').each(function () {
        $(this).parents('label').addClass('cb-checkbox');
    });

    cloned.find('.profileBlock').checkBo();
}

function applyRules() {

    changeName();

    $('#tasks').find('.market-profile').each(function (i, _modelBlock) {
        $(_modelBlock).find('input[rel="name"]').each(function () {
            initChangeName($(this), false);
        });
        $(_modelBlock).find('.price_rule_li').each(function () {
            changeNamePriceRules($(this), false);
        });
    });

    //Price Rule
    $('.addPriceRule').unbind('click').click(function () {
        addPriceRule($(this));
    });

    $('.removePriceRule').unbind('click').click(function () {
        changeNamePriceRules($(this).parents('.price_rule_li'), true);
        //$(this).closest('.price_rule_li').remove();
    });

    $('input.price_rule').unbind('keydown').on('keydown', function () {
        clearPriceError($(this));
    });

    $('select[rel="type"]').unbind('change').on('change', function () {
        changePriceRuleType($(this));
    });

    $('.remove_profile').unbind('click').on("click", function () {
        remove($(this));
    });

    $('input[rel="recommended_browse_node"]').unbind('blur').on('blur', function ()
    {
        recommended_browse_node($(this));
    });

    $('input[rel="out_of_stock"]').unbind('blur').on('blur', function ()
    {
        checkOutOfStock($(this));
    });

    $('.edit_profile').unbind('click').on("click", function () {
        edit($(this));
    });

}

//Change name
function changeName() {

    $('#tasks').on('blur', 'input[rel="name"]', function ()
    {
        initChangeName($(this), true);
    });
}

function initChangeName(_content, _recheckBo) {
    var content = _content.parents('div.market-profile');
    var content_group = _content.parents('div.form-group');
    var duplicate = 0;
    var name = $.trim(_content.val());
    var rel = _content.parents('.profileBlock').attr('rel');

    if (name)
    {
        if (content_group.hasClass('has-error'))
        {
            content_group.removeClass('has-error');
            content_group.find('.error').hide();
        }

        name = name.replace(/[^a-zA-Z0-9\s_]/g, '').replace(/\s/g, '_');

        if ($('.profileBlock:not([rel="' + rel + '"]):not(:first)').find('input[name="profile[' + name + '][name]"]').length !== 0)
        {
            name = name + '_' + duplicate;
            duplicate++;
        }

        content.find('label').removeClass('disabled');
        content.find('select, input, div, span').each(function ()
        {
            if ($(this).hasClass('disabled-none')) {
                $(this).removeClass('disabled');
            }

            if (!$(this).attr('rel'))
                return;

            if ($(this).is('select, input')) {
                $(this).removeAttr('disabled');
                $(this).closest('.form-group').removeClass('disabled');
            }

            if ($(this).is('select')) {
                $(this).parent().find('.chosen-single div').remove();
                $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                $(this).chosen().trigger("chosen:updated");
            }

            var field_name = $(this).attr('rel');

            if ($(this).attr('rel') != "type"
                    && $(this).attr('rel') != "from"
                    && $(this).attr('rel') != "to"
                    && $(this).attr('rel') != "value")
            {
                var variable_prefix = 'profile[' + name + '][' + field_name + ']';
                var id_prefix = 'profile_' + name + '_' + field_name;

                $(this).attr('name', variable_prefix);
                $(this).attr('id', id_prefix);
                $(this).attr('data-content', name);
            }
            changeNamePriceRules($(this), false);
        });


    } else
    {
        if (!content_group.hasClass('has-error'))
        {
            content_group.addClass('has-error');
            content_group.find('.error').show();
        }

        content.find('select, input, div, span').each(function ()
        {
            if ($(this).is('select, input') && !$(this).is('input[rel="name"]')) {
                $(this).attr('disabled', true);

                $(this).closest('.form-group').addClass('disabled');

                if ($(this).is('select')) {
                    $(this).parent().find('.chosen-single div').remove();
                    $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                    $(this).chosen().trigger("chosen:updated");
                }
            }
        });

    }

    /* Destroy and Call CheckBo agian */
    recheckBo(content, _recheckBo);

}

//CheckBo
function recheckBo(_profile, _recheckBo) {

    _profile.find('.cb-radio').each(function () {
        $(this).find('input').appendTo($(this));
        $(this).find('.cb-inner').remove();
    });

    _profile.find('.cb-checkbox').each(function () {
        $(this).find('input').appendTo($(this));
        $(this).find('.cb-inner').remove();
    });

    if (_recheckBo)
        _profile.checkBo();
}

function changeNamePriceRules(content, remove)
{
    var id = 0;
    var _parent = content.parents('.price_rules');
    var _number_remove = content.attr('rel') - 1;

    if (remove) {
        _parent.find('.price_rule_li').each(function (i, selectors) {
            if (i == _number_remove)
                $(this).remove();
        });
    }

    _parent.find('.price_rule_li').each(function (i, selectors) {
        $(selectors).find('input.price_rule, select').each(function ()
        {
            var name = $(selectors).attr('data-content');
            if (!$(this).attr('rel'))
                return;

            var field_name = $(this).attr('rel');

            var variable_prefix = 'profile[' + name + '][price_rules][value][' + (i + 1) + '][' + field_name + ']';
            var id_prefix = 'profile_' + name + '_price_rules_value_' + (i + 1) + '_' + field_name;

            $(this).attr('name', variable_prefix);
            $(this).attr('id', id_prefix);
            $(this).attr('data-content', name);
            $(this).attr('data-row', (i + 1));

            $(this).parents('.price_rule_li').attr('name', variable_prefix);
            $(this).parents('.price_rule_li').attr('rel', (i + 1));

        });
    });
}
//Remove
function remove(obj) {

    var name = $(obj).attr('rel');

    bootbox.confirm($('#confirm-delete-profile-message').val() + ", " + name + "?", function (result) {

        if (result) {
            var id = $(obj).val();
            var country = $('#id_country').val();

            $.ajax({
                type: "POST",
                url: base_url + $('#class_call').val() + "/profiles/" + $("#id_country").val(),
                data: 'id_profile=' + id + '&id_country=' + country + '&method=remove_profile',
                success: function (rdata)
                {
                    if (rdata === "Success")
                    {
                        notification_alert($('#success-message').val() + ' ' + $('#delete-success-message').val() + ', "' + name + '".', 'good');

                        //var id = parseInt($('#count').text()) - 1;
                        //$('#count').html(id);
                        $(obj).closest('div.market-profiles').remove();

                        $('#main').find('select[id="duplicate-profile"] option').each(function () {
                            if ($(this).attr('value') !== "")
                            {
                                if (name === $(this).text())
                                    $(this).remove();
                            }
                        });
                        countBlock($('.countProfile'), $('.showSelectBlock'));
                    } else
                    {
                        notification_alert($('#error-message').val() + ' ' + $('#delete-unsuccess-message').val() + ', "' + name + '".', 'bad');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    notification_alert($('#error-message').val() + ' ' + textStatus + ' ' + errorThrown + '.', 'bad');
                }
            });
        }
    });
}

//ADD Recommended Browse Node
function addPriceRule(obj) {

    var ul = obj.closest('div.price_rule_main');
    var li = obj.closest('div.price_rule_li');

    //Check value
    if (checkPriceRange(li))
    {
        var count = ul.find('input[rel="price_rule_count"]');
        var cloned = li.clone().appendTo(ul);

        li.find('input[rel="to"]').on('blur',
                function () {
                    checkPriceRange(li);
                });
        li.find('input.price_rule').on('keydown', function () {
            clearPriceError($(this));
        });
        li.find('.addPriceRule').removeClass('cb-plus').removeClass('good').removeClass('addPriceRule')
                .addClass('cb-minus').addClass('bad').addClass('removePriceRule').unbind('click').on("click", function () {
            changeNamePriceRules($(this).parents('.price_rule_li'), true);
        });
        cloned.find('select[rel="type"]').on('change', function () {
            changePriceRuleType($(this));
        });
        cloned.find('select[rel="type"]').val(li.find('select[rel="type"]').val());
        cloned.find('.addPriceRule').click(
                function () {
                    addPriceRule($(this));
                });
        cloned.find('input[rel="from"]').val(parseFloat(li.find('input[rel="to"]').val()) + 1);
        cloned.find('input[rel="to"]').val('').focus();
        cloned.find('input[rel="value"]').on('blur',
                function () {
                    checkPriceRange(cloned);
                }).val('');
        cloned.find('input.price_rule').on('keydown', function () {
            clearPriceError($(this));
        });

        count.val(parseInt(count.val()) + 1);
        cloned.attr('rel', parseInt(count.val()));

        changeNamePriceRules(cloned);

        updateChosen(cloned);
    }
}

function recommended_browse_node(obj)
{
    if (!myFunc($(obj))) {
        $(obj).val("");
        $(obj).closest('div.form-group').addClass('has-error');
        $(obj).closest('div.form-group').find('.error').show();

    } else {
        $(obj).closest('div.form-group').removeClass('has-error');
        $(obj).closest('div.form-group').find('.error').hide();
    }
}

//Allow only digit
function myFunc(txt) {
    var value = txt.val();
    var re = /^([0-9+-]+[\.]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?|[0-9]+)$/g;

    if (!re.test(value))
        return false;
    else
        return true;
}

//Check Price From To
function checkPriceRange(obj)
{
    var price_from = $(obj).find('input[rel="from"]');
    var price_to = $(obj).find('input[rel="to"]');
    var value = $(obj).find('input[rel="value"]');

    if (!value.val().length || !price_from.val().length || !price_to.val().length) {
        if (!value.val().length) {
            value.closest('div.input-group').css('border', '1px solid red');
        }
        if (!price_from.val().length) {
            price_from.closest('div.input-group').css('border', '1px solid red');
        }
        if (!price_to.val().length) {
            price_to.closest('div.input-group').css('border', '1px solid red');
        }
        $(obj).find('.has-error').show().find('span').html($('#none-value').val());
        validation = true;
        return(false);

    } else if (parseFloat(price_from.val()) >= parseFloat(price_to.val())) {

        price_to.parent().css('border', '1px solid red');
        $(obj).find('.has-error').show().find('span').html($('#invalid-price-range').val());
        validation = true;
        return(false);

    } else {
        validation = false;
        return(true);
    }

}

function clearPriceError(obj)
{
    obj.closest('.input-group').removeAttr('style');
    obj.closest('div.price_rule_li').find('.has-error').hide();
}

function changePriceRuleType(obj)
{
    var type = $(obj).val();

    if (type == "percent") {
        $(obj).closest('div.price_rule_li').find('input[rel="value"]').closest('.input-group').find('.input-group-addon strong').html('%');
    } else if (type == "value") {
        $(obj).closest('div.price_rule_li').find('input[rel="value"]').closest('.input-group').find('.input-group-addon strong').html($('#currency_sign').val());
    }
}

function updateChosen(obj) {
    obj.find('.chosen-container').remove();
    obj.find('.chosen-single div').remove();
    obj.find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
    choSelect();
}

function edit(obj) {

    if ($(obj).closest('div.market-profiles').find('div.market-profile').hasClass('showSelectBlock')) {
        $(obj).closest('div.market-profiles').find('div.market-profile').removeClass('showSelectBlock');
    } else {
        $(obj).closest('div.market-profiles').find('div.market-profile').addClass('showSelectBlock');
    }

    countBlock($('.countProfile'), $('.market-profile'));

}

function checkOutOfStock(obj) {
    if (!myFunc(obj)) {
        obj.val("");
        obj.parent().addClass('has-error').find('.error').show();
    } else {
        obj.parent().removeClass('has-error').find('.error').hide();
    }
}
