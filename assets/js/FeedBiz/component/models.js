var validation = false;
$(document).ready(function () {
    /* == Animation Loading == */
    $('.feedbizloader').find('.feedbizcolor').sprite({fps: 10, no_of_frames: 30});
    $('form').attr('novalidate', 'novalidate');
    //Add
    $('#add').click(function () {
        if (!$('.market-model .form-group.has-error').is(':visible'))
            add_model();
    });

    if ($('input#popup_status_more').length > 0) {
        console.log($('#popup_more_no_model'));
        if ($('#popup_more_no_model').length > 0) {
        } else {
            add_model();
        }
    }

    $(document).on('click', '.removeProfile', function (e) {
            edit($(this));
    });

    //Replace underscore all the first label
    $('.form-group > .col-md-12 > label').each(function (i, selector) {
        $(selector).text($(selector).text().replace(/_/g, " "));
    });

    applyRules();
});

function add_model()
{
    var cloned = $('#main').clone().show();
    $('#tasks').prepend(cloned);

    cloned.attr('id', '');
    countBlock($('.countModel'), $('.market-model'));

    var mm = $.map($('.market-model'), function (el, index) {
        return $(el).attr('rel');
    });

    var maxnum = Math.max.apply(null, mm);
    var nextnum = maxnum + 1;
    cloned.find('.market-model input[rel="id_model"]').val(nextnum);
    cloned.find('.market-model').attr('rel', nextnum);

    cloned.find('.remove').on('click', function () {
        $('#count').html(parseInt($('#count').text()) - 1);
        $(this).closest('div.market-models').remove();
    });

    cloned.find('input[rel=name]').focus();

    init(cloned, true);
}

function clearSelectError(_selector) {
    if ($(_selector).val()) {
        $(_selector).parents('.form-group').removeClass('has-error');
    } else if ($(_selector).prop('required')) {
        $(_selector).parents('.form-group').addClass('has-error');
    }
}

function init(_content, _runcheckBo) {
    _content.find('.addProducttype').on('click', addProducttype);
    _content.find('.removeProducttype').on('click', removeProducttype);
    //_content.find('select[rel="product_type"]').on('change', selectOnceOnly);

    _content.find('.variation_fields_options select, .specific_fields_options select, .custom_label_options select').each(function () {
        $(this).parents('.form-group').find('select:first').each(function (i, e) {
            $(e).on('change', function () {
                if ($(this).val() == "default_value") {
                    $(this).parents('.form-group').find('input[type="text"]:not(:first), select:not(:first), .chosen-container:not(:first), label:not(:first,:last), input[type="checkbox"]:not(:first,:last), input[type="radio"]').each(function () {
                        $(this).removeClass('hidden').prop("disabled", false);
                        resetDataNameOBJ($(this));
                    });
                    $(this).attr('name', '');
                } else {
                    $(this).parents('.form-group').find('input[type="text"]:not(:first), select:not(:first), .chosen-container:not(:first), label:not(:first,:last), input[type="checkbox"]:not(:first,:last), input[type="radio"]').addClass('hidden').prop("disabled", true);
                    $(this).attr('name', $(this).attr('data-name'));
                    setDataName(_content);
                    removeDataName(_content);
                }
                checkSelectInput($(this));
            });
            checkSelectInput($(this));
        });
    });

    _content.find('.specific_fields_options input').each(function () {
        checkSelectInput($(this));
    });

    _content.find('select').on('change', function () {
        clearSelectError($(this));
    });

    _content.find('input[rel="recommended_browse_node"]').on('blur', function ()
    {
        recommended_browse_node($(this));
    });

    //set select chosen
    _content.find('select').addClass('search-select').chosen({search_contains: true});

    _content.find('.removeProfile').on('click', function (e) {
        e.preventDefault();
        var $ruleCheckBo = $('.ruleSettings').find('.checkBo').length;

        $(this).closest('.showSelectBlock').remove();

        countBlock($('.countModel'), $('.showSelectBlock'));

        $('.setBlockMain').addClass('hidden');

        $('.setBlock').remove();
    });

    // get Product type
    _content.find('select[rel="product_universe"]').unbind('change').on("change", function () {
        getProducttype($(this));
    });

    // Run CheckBo
    if (_runcheckBo) {
        initcheckBo(_content, true, true);
    }

    checkHiddenDisableOBJ(_content);
}

function initcheckBo(_content, _runcheckBo, _offSwitch) {
    _content.find('input[type="radio"]').each(function () {
        $(this).parents('label').addClass('cb-radio');
    });
    _content.find('input[type="checkbox"]').each(function () {
        if ($(this).parent('label.inner-switcher').length > 0) {
            $(this).parent().parent('label').addClass('cb-switcher');
        } else {
            $(this).parents('label').addClass('cb-checkbox');
        }
    });

    if (_runcheckBo)
        _content.find('.modelBlock').checkBo();

    if (_offSwitch)
        _content.find('label.cb-switcher input').off();
}

function setupSelect($obj) {
    $($obj).find('input[name*="[product_universe]"]').each(function (index, selector) {
        var _this_parent = $(selector).parent();
        var clone = $('#main.market-models select[rel="product_universe"]').parent().clone().show();
        var parent_select = clone.insertAfter(_this_parent);
        var select = $(parent_select).find('select');
        // Assign All Elements
        select.val($(selector).val());
        select.attr('rel', $(selector).attr('rel'));
        select.attr('name', $(selector).attr('name'));
        select.attr('id', $(selector).attr('id'));
        select.attr('data-name', $(selector).attr('data-name'));
        select.attr('data-name', $(selector).attr('data-name'));
        select.attr('data-content', $(selector).attr('data-content'));
        select.attr('requied', $(selector).attr('requied'));
        _this_parent.remove();

    });

    $($obj).each(function (i, form) {
        var form_model = $(form).find('input[name*="[product_type]"]');
        var form_model_last_index = $(form).find('input[name*="[product_type]"]').length - 1;
        form_model.each(function (index, selector) {
            var _this_parent = $(selector).parent();
            var clone = $('#main.market-models select[rel="product_type"]').parent().clone().show();
            var parent_select = clone.insertAfter(_this_parent);
            var select = $(parent_select).find('select');
            var button = $(parent_select).find('i');
            // Assign All Elements
            select.val($(selector).val());
            select.attr('rel', $(selector).attr('rel'));
            select.attr('name', $(selector).attr('name'));
            select.attr('id', $(selector).attr('id'));
            select.attr('data-name', $(selector).attr('data-name'));
            select.attr('data-name', $(selector).attr('data-name'));
            select.attr('data-content', $(selector).attr('data-content'));
            select.attr('requied', $(selector).attr('requied'));
            if (index !== 0) {
                button.attr('class', 'cb-minus bad removeProducttype');
            }
            _this_parent.remove();
        });
    });
}

function applyRules() {
    //setupSelect();
    //remove_error_label();
    changeName();

    $('.remove_model').unbind('click').on("click", function () {
        remove($(this));
    });

    $('.edit_model').unbind('click').on("click", function () {
            var _this = $(this);
            if(!$(_this).parents('.market-models').find('.profileBlock').attr('data-load')){
                $('.feedbizloader').removeClass('hidden');
                $.get( base_url + $('#class_call').val() + '/models_selected/' + $('#id_country').val() + '?id=' + $(_this).parents('.market-models').find('.profileBlock').attr('rel'), function( data ) {
                    $(_this).parents('.market-models').find('.profileBlock').append(data);
                    setupSelect($(_this).parents('.market-models').find('.profileBlock'));
                    //initChangeName($(_this).parents('.market-models').find('.profileBlock'), true);
                    initChangeName($(_this).parents('.market-models').find('.profileBlock').find('input[rel="name"]'), true);
                    /*$(obj).parents('.market-models').find('.profileBlock').checkBo();
                    $(obj).parents('.market-models').find('.profileBlock').find('select').chosen({
                            disable_search_threshold: 1,
                            no_results_text: "Nothing found!"
                    }).trigger("chosen:updated");	

                    $('.chosen-single').find('div').addClass('button-select');
                    $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');*/
                    edit($(_this));
                    init($(_this).parents('.market-models').find('.profileBlock'), true);
                    $(_this).parents('.market-models').find('.profileBlock').attr('data-load', true);
                    $('.feedbizloader').addClass('hidden');
                });
            }else{
                edit($(_this));
            }
        });

    setName('MfrPartNumber', 'mfr_part_number');
    setName('ProductSubtype', 'sub_product_type');
    setName('VariationTheme', 'variation_theme');

    //RUN CheckBo
    $('#tasks').find('.market-models').each(function (i, _modelBlock) {
        init($(_modelBlock), false);
        initcheckBo($(_modelBlock), false, false);
        initChangeName($(_modelBlock).find('input[rel="name"]'), false);
    });
}

//Change name
function changeName() {

    $('#tasks').on('blur', 'input[rel="name"]', function ()
    {
        initChangeName($(this), true);
    });
}

function initChangeName(_content, _recheckBo) {
    var content = _content.parents('div.market-model');
    var content_group = _content.parents('div.form-group');
    var duplicate = 0;
    var name = $.trim(_content.val());
    var rel = _content.parents('.profileBlock').attr('rel');

    if (name)
    {
        if (content_group.hasClass('has-error'))
        {
            content_group.removeClass('has-error');
            content_group.find('.error').hide();
        }

        name = name.replace(/[^a-zA-Z0-9\s_]/g, '').replace(/\s/g, '_');

        if ($('.profileBlock:not([rel="' + rel + '"]):not(:first)').find('input[name="model[' + name + '][name]"]').length !== 0)
        {
            name = name + '_' + duplicate;
            duplicate++;
        }

        content.find('label').removeClass('disabled');
        content.find('select, input, div, span').each(function (i, selector)
        {
            if ($(this).hasClass('disabled-none')) {
                $(this).removeClass('disabled');
            }

            if (!$(this).attr('rel'))
                return;

            if ($(this).is('select, input')) {
                $(this).removeAttr('disabled');
                $(this).closest('.form-group').removeClass('disabled');
            }

            if ($(this).is('select')) {
                $(this).parent().find('.chosen-single div').remove();
                $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                $(this).chosen({search_contains: true}).trigger("chosen:updated");
                $('.chosen-single').find('div').addClass('button-select');
                $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
                $('.chosen-single').find('div').addClass('button-select');
                $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
            }

            var field_name = $(this).attr('rel');

            if ($(this).attr('rel') != "type"
                    && $(this).attr('rel') != "from"
                    && $(this).attr('rel') != "to"
                    && $(this).attr('rel') != "value")
            {

                var type_prefix = $(this).parents('.variation_fields_options, .specific_fields_options, .custom_label_options').attr('rel');
                if (type_prefix) {
                    field_name = field_name.replace(/ /g, "_");
                    if ($(this).attr('type') == 'hidden') {
                        var variable_prefix = 'model[' + name + '][' + type_prefix + '][' + field_name + '][hidden]';
                    } else {
                        if (type_prefix == 'variation') {
                            //var tagselection = $(selector).get(0).tagName=="INPUT"? $(selector).get(0).type.toLowerCase() : $(selector).get(0).tagName.toLowerCase();
                            var tagselection = $(selector).get(0).type == "checkbox" ? 'tag' : 'value';
                            var variable_prefix = 'model[' + name + '][' + type_prefix + '][' + field_name + '][' + tagselection + ']';
                        } else {
                            var variable_prefix = 'model[' + name + '][' + type_prefix + '][' + field_name + '][value]';
                        }
                    }
                    var id_prefix = 'model_' + name + '_' + type_prefix + '_' + field_name;
                } else {
                    var variable_prefix = 'model[' + name + '][' + field_name + ']';
                    var id_prefix = 'model_' + name + '_' + field_name;
                }
                
                $(this).attr('name', variable_prefix);
                $(this).attr('id', id_prefix);
                $(this).attr('data-content', name);
            }
        });

        productTypesetOrder(content);
        changeNamePriceRules(content, name);

    } else
    {
        if (!content_group.hasClass('has-error'))
        {
            content_group.addClass('has-error');
            content_group.find('.error').show();
        }

        content.find('select, input, div, span').each(function ()
        {
            if ($(this).is('select, input') && !$(this).is('input[rel="name"]')) {
                $(this).attr('disabled', true);

                $(this).closest('.form-group').addClass('disabled');

                if ($(this).is('select')) {
                    $(this).parent().find('.chosen-single div').remove();
                    $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                    $(this).chosen({search_contains: true}).trigger("chosen:updated");
                    $('.chosen-single').find('div').addClass('button-select');
                    $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
                    $('.chosen-single').find('div').addClass('button-select');
                    $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
                }
            }
        });

    }

    $('#tasks').find('div[rel="variation_data"], div[rel="recommended_data"], div[rel="specific_fields"]').each(function ()
    {
        var name = $(this).attr('data-content');
        var validation_rel = $(this).attr('rel');

        setValuseNames(validation_rel, name);
    });

    content.find('.variation > .form-group > label.cb-switcher').unbind().on('click', function () {
        checkgroup($(this));
    });

    setDataName(content);
    removeDataName(content);

    /* Destroy and Call CheckBo agian */
    recheckBo(content, _recheckBo);
}

function checkSelectInput(_this) {
    var findSelectInput,
            findSelectInputArray = [];
    _this.parents('.form-group').find('input[type="text"], select, input[type="radio"]').each(function (i, selector) {
        if ($(selector).attr('type') !== 'hidden' && !$(selector).hasClass('hidden') && !$(selector).attr('autocomplete')) {
            if ($.inArray("radio", findSelectInputArray) == -1)
                findSelectInputArray.push($(selector).get(0).tagName.toLowerCase() == 'input' ? $(selector).get(0).type.toLowerCase() : $(selector).get(0).tagName.toLowerCase());
        }
    });
    findSelectInput = findSelectInputArray.toString();
    findSelectInput = findSelectInput.replace(',', '|');
    _this.parents('.form-group').find('input[type="hidden"]').val(findSelectInput);
}


function checkHiddenDisableOBJ(_content) {
    _content.find('select, input').each(function (i, select) {
        if ($(select).hasClass('hidden')) {
            $(select).parent().find('.chosen-container').addClass('hidden');
            $(select).prop("disabled", true);
            $(select).unbind();
        }
    });
}

function setDataName(_content) {
    _content.find('select, input').each(function (i, select) {
        if ($(select).attr('name'))
            $(select).attr('data-name', $(select).attr('name'));
    });
}

function removeDataName(_content) {
    _content.find('select, input').each(function (i, select) {
        if ($(select).hasClass('hidden')) {
            $(select).attr('name', '');
        }
    });
}

function resetDataName(_content) {
    _content.find('select, input').each(function (i, select) {
        resetDataNameOBJ(select);
    });
}

function resetDataNameOBJ(_obj) {
    $(_obj).attr('name', $(_obj).attr('data-name'));
}

function productTypesetOrder(_profileBlock) {
    _profileBlock.find('select[rel="product_type"]').each(function (i)
    {
        var field_name = $(this).attr('rel');
        var duplicate = 0;
        var name = $.trim(_profileBlock.find('input[rel="name"]').attr('data-content'));
        if (name) {
            var type_prefix = $(this).parents('div[rel="product_type_container"]').attr('rel');
            if (type_prefix) {
                field_name = field_name.replace(/ /g, "_");
                var variable_prefix = 'model[' + name + '][' + field_name + '][' + (i + 1) + ']';
                var id_prefix = 'model_' + name + '_' + field_name + '_' + (i + 1);
            } else {
                var variable_prefix = 'model[' + name + '][' + field_name + ']';
                var id_prefix = 'model_' + name + '_' + field_name;
            }

            $(this).attr('name', variable_prefix);
            $(this).attr('id', id_prefix);
            $(this).attr('data-content', name);
        }
    });
}

//CheckBo
function recheckBo(_model, _recheckBo) {

    _model.find('.cb-radio').each(function () {
        $(this).find('input').appendTo($(this));
        $(this).find('.cb-inner').remove();
    });

    _model.find('.cb-checkbox').each(function () {
        $(this).find('input').appendTo($(this));
        $(this).find('.cb-inner').remove();
    });

    _model.find('.cb-switcher').each(function () {
        $(this).find('input').appendTo($(this));
        $(this).find('.inner-switcher').remove();
    });

    if (_recheckBo)
        _model.checkBo();
}

function changeNamePriceRules(content, name)
{
    var id = 0;
    var name = name;
    content.find('.price_rule').each(function ()
    {
        if (!$(this).attr('rel'))
            return;

        var field_name = $(this).attr('rel');

        if (field_name === "type")
            id = $(this).parent().parent().parent().attr('rel');

        var variable_prefix = 'model[' + name + '][price_rules][value][' + id + '][' + field_name + ']';
        var id_prefix = 'model_' + name + '_price_rules_value_' + id + '_' + field_name;

        $(this).attr('name', variable_prefix);
        $(this).attr('id', id_prefix);
        $(this).attr('data-content', name);
        $(this).attr('data-row', id);

    });
}
//Remove
function remove(obj) {

    var name = $(obj).attr('rel');
    var market_models = $(obj).parents('.market-models');

    bootbox.confirm($('#confirm-delete-model-message').val() + ", " + name + "?", function (result) {

        if (result) {
            var id = $(obj).val();
            var country = $('#id_country').val();

            $.ajax({
                type: "POST",
                url: base_url + $('#class_call').val() + "/models/" + $("#id_country").val(),
                data: 'id_model=' + id + '&id_country=' + country + '&method=remove_model',
                success: function (rdata)
                {
                    if (rdata === "Success")
                    {
                        notification_alert($('#success-message').val() + ' ' + $('#delete-success-message').val() + ', "' + name + '".', 'good');

                        market_models.remove();
                        countBlock($('.countModel'), $('.market-model'));
                    } else
                    {
                        notification_alert($('#error-message').val() + ' ' + $('#delete-unsuccess-message').val() + ', "' + name + '".', 'bad');
                    }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    notification_alert($('#error-message').val() + ' ' + textStatus + ' ' + errorThrown + '.', 'bad');
                }
            });
        }
    });
}


function recommended_browse_node(obj)
{
    if (!myFunc($(obj))) {
        $(obj).val("");
        $(obj).closest('div.form-group').addClass('has-error');
        $(obj).closest('div.form-group').find('.error').show();

    } else {
        $(obj).closest('div.form-group').removeClass('has-error');
        $(obj).closest('div.form-group').find('.error').hide();
    }
}

//Allow only digit
function myFunc(txt) {
    var value = txt.val();
    var re = /^([0-9+-]+[\.]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?|[0-9]+)$/g;

    if (!re.test(value))
        return false;
    else
        return true;
}

function edit(obj) {
    
    if ($(obj).closest('div.market-models').find('div.market-model').is(':visible')) {
        $(obj).closest('div.market-models').find('div.market-model').hide();
    } else {
        $(obj).closest('div.market-models').find('div.market-model').show();
    }

    checkHiddenDisableOBJ($(obj).parents('.market-models'));
    countBlock($('.countModel'), $('.market-model'));
}

function setValuseNames(element, name) {
    var e = element;
    var n = name;
    var SpecificFieldsSelect = $('#model_' + n + '_' + e).find('select.search-select');

    if (SpecificFieldsSelect.length > 0)
    {
        SpecificFieldsSelect.each(function (i, j)
        {
            var SpecificRel = $(this).attr('rel');

            if (!$(this).hasClass('nmp_option_name') && !$(this).hasClass('attribute') && !$(this).hasClass('custom_value'))
            {

                $(this).attr('name', 'model[' + n + '][' + e + '][' + SpecificRel + '][value]');
                $(this).attr('id', 'model_' + n + '_' + e + '_' + SpecificRel + '_value');
                $(this).attr('data-content', n);

                $(this).change(function () {
                    custom_value($(this), e, n, SpecificRel);
                });

                var SpecificDataAttr = $(this).closest('div.form-group').find('.attribute');

                SpecificDataAttr.each(function () {

                    var SpecificAttrRel = $(this).attr('rel');

                    $(this).attr('name', 'model[' + n + '][' + e + '][' + SpecificRel + '][attr][' + SpecificAttrRel + ']');
                    $(this).attr('id', 'model_' + n + '_' + e + '_' + SpecificRel + '_attr_' + SpecificAttrRel);
                    $(this).attr('data-content', n);

                });
            } else {
                //Custom Value
                if ($(this).hasClass('custom_value')) {
                    $(this).attr('name', 'model[' + n + '][' + e + '][' + SpecificRel + '][CustomValue]');
                }
            }

        });
    }

    var SpecificFieldsInput = $('#model_' + n + '_' + e).find('input');

    SpecificFieldsInput.each(function ()
    {
        if (!$(this).hasClass('nmp_option_name') /*&& !$(this).hasClass('custom_value')*/)
        {
            var SpecificRel = $(this).attr('rel');

            $(this).attr('name', 'model[' + n + '][' + e + '][' + SpecificRel + '][value]');
            $(this).attr('id', 'model_' + n + '_' + e + '_' + SpecificRel + '_value');
            $(this).attr('data-content', n);

            var SpecificDataAttr = $(this).closest('div.form-group').find('.attribute');

            SpecificDataAttr.each(function () {

                var SpecificAttrRel = $(this).attr('rel');

                $(this).attr('name', 'model[' + n + '][' + e + '][' + SpecificRel + '][attr][' + SpecificAttrRel + ']');
                $(this).attr('id', 'model_' + n + '_' + e + '_' + SpecificRel + '_attr_' + SpecificAttrRel);
                $(this).attr('data-content', n);

            });
        }
    });

}

function setName(rel, element) {
    var e = element;

    $('#tasks').find('select[rel="' + rel + '"], input[rel="' + rel + '"]').each(function ()
    {
        if (!$(this).attr('rel'))
            return;
        //console.log(rel,element);        
        var n = $(this).closest('.market-models').find('input[rel="name"]').attr('data-content');
        $(this).attr('name', 'model[' + n + '][' + e + ']');
        $(this).attr('id', 'model_' + n + '_' + e);
        $(this).attr('rel', e);
        $(this).attr('data-content', n);

    });
}

function checkgroup(_this) {
    if (!_this.find('input').prop('checked')) {
        _this.parents('.variation').find('label.cb-checkbox').removeClass('hidden');
    } else {
        _this.parents('.variation').find('label.cb-checkbox').addClass('hidden');
    }
}

function getProducttype(e) {
    
    var val = $(e).find('option:selected').val();
    var url = base_url + $('#class_call').val() + "/productType/" + val;
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function (r) {
           
            if(r.productType){
                var cloned = $(e).closest('.market-models').find('div[rel="product_type_container"]').find('.form-group').first().clone();
                var product_type = cloned.find('select');
                var parent = $(e).closest('.market-models').find('div[rel="product_type_container"]');
                
                var model_name = product_type.attr('data-content');
                var product_type_name = 'model['+model_name+'][product_type][]';
                product_type.attr('name', product_type_name);
                console.log(product_type);
                if(parent.html('')){
                    if(product_type.find('option').remove()){
                        product_type.html(r.productType);
                        parent.html(cloned);
                    }
                }

                parent.find('.addProducttype').on('click', addProducttype);
                /*parent.find('.addProducttype').on('click', function(i,j){
                    var form_group = $(this).closest('.form-group');
                    var form_group_cloned = form_group.clone();
                    form_group_cloned.find('i').removeClass()
                                    .unbind()
                                    .attr('class', 'cb-minus bad removeProducttype')
                                    .on('click', function(i,j){
                                        $(this).parents('.form-group.withIcon').remove();                                        
                                    });
                    parent.append(form_group_cloned);
                    $(form_group_cloned).find('select[rel="product_type"]').chosen('destroy');
                    $(form_group_cloned).find('select[rel="product_type"]').trigger('change');
                    $(form_group_cloned).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                    //setChosen(form_group);
                });*/

                $(product_type).chosen('destroy');
                $(product_type).trigger('change');
                $(product_type).trigger('chosen:updated');
                $(product_type).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                //setChosen($(e).closest('.market-models'));
                $('.chosen-single').find('div').addClass('button-select');
                $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
            }
        }
    });

}

function addProducttype() {   
    
    // Check Disable
    if($(this).closest('.form-group').is('.disabled')){
        return false;
    }
    
    var parent = $(this).closest('.market-models').find('div[rel="product_type_container"]');
    var form_group = $(this).closest('.form-group');

    var model_name = $(form_group).find('select').attr('data-content');
    var product_type_name = 'model['+model_name+'][product_type][]';
    $(form_group).find('select').attr('name', product_type_name);

    // Remove used options
    var selected_select = $(form_group).find('select');
    $(parent).find('select[rel="product_type"] option:selected').each(function (index, option) {
        selected_select.find('option[value="' + option.value + '"]').remove();
    });

    var form_group_cloned = form_group.clone();
    form_group_cloned.find('i').removeClass()
                    .unbind()
                    .attr('class', 'cb-minus bad removeProducttype')
                    .on('click', removeProducttype);
    parent.append(form_group_cloned);
    $(form_group_cloned).find('select[rel="product_type"]').chosen('destroy');
    $(form_group_cloned).find('select[rel="product_type"]').trigger('change');
    $(form_group_cloned).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
    /*var _this = $(this).parents('.form-group.withIcon');
    var _profileBlock = $(this).parents('.profileBlock.market-model');
    if (_this.find('select[rel="product_type"] option').length <= 2)
        return false;
    _this.find('select[rel="product_type"]').chosen('destroy');
    var cloned = $(this).parents('.form-group.withIcon').clone().show();   
    $(this).parents('div[rel="product_type_container"] > .form-group.withIcon').before(cloned);
    cloned.find('i').removeClass()
            .unbind()
            .attr('class', 'cb-minus bad removeProducttype')
            .on('click', removeProducttype);
    cloned.find('select[rel="product_type"]').val(_this.find('select[rel="product_type"]').val());
    cloned.find('select[rel="product_type"]').on('change', selectOnceOnly);
    if (_this.find('select[rel="product_type"]').val()) {
        cloned.find('select[rel="product_type"]').trigger('change');
    } else {
        setChosen(_this);
        setChosen(cloned);
    }
    productTypesetOrder(_profileBlock);*/
}

function removeProducttype() {
    $(this).parents('.form-group.withIcon').remove();    
    /*var _this = $(this).parents('.form-group.withIcon');
    var _profileBlock = $(this).parents('.profileBlock.market-model');
    _this.find('select[rel="product_type"]').chosen('destroy');
    _this.find('select[rel="product_type"]').html('');
    _this.find('select[rel="product_type"]').trigger('change');
    _this.remove();*/
    //productTypesetOrder(_profileBlock);
}

function selectOnceOnly() {
    var selectedPrefs = [];
    var parentModel = $(this).parents('.profileBlock');

    //What items are selected
    parentModel.find('select[rel="product_type"]').each(function (i, select) {
        if ($(select).parent().find('.chosen-container').length > 0) {
            $(select).chosen('destroy');
        }
        if ($(this).val()) {
            if ($.inArray($(this).val(), selectedPrefs) !== -1 && selectedPrefs.length > 0) {
                $(this).find('option:selected').remove();
            }
            selectedPrefs.push($(this).val());
        } else {
            //$(this).remove();
        }
    });
    //make sure to add selection back to other selects
    parentModel.find('select[rel="product_type"]').each(function (i, select) {
        $(select).empty();
        $(select).append($('select[rel="product_type"]:first').html());
        var elZero = selectedPrefs[i];
        $(select).val(elZero);
    });
    //remove already selected options
    parentModel.find('select[rel="product_type"]').each(function (i, select) {
        parentModel.find('select[rel="product_type"] option').each(function (ii, option) {
            if ($(option).val() != "" && selectedPrefs[i] == $(option).val() && selectedPrefs[i] != $(option).parent().val()) {
                $(option).remove();
            }
        });
    });

    setChosen(parentModel);
}

function setChosen(_content) {
    _content.find('select[rel="product_type"]').each(function () {
        $(this).addClass('search-select').chosen({search_contains: true});

        if ($(this).is('select')) {
            $(this).parent().find('.chosen-single div').remove();
            $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
            $(this).chosen({search_contains: true}).trigger("chosen:updated");
            $('.chosen-single').find('div').addClass('button-select');
            $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
            $('.chosen-single').find('div').addClass('button-select');
            $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
        }
    });
}
