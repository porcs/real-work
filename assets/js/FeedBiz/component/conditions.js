$(document).ready(function () {
    $('.search-select').on('change', function () {
        var val = $(this).val();
        var obj = $(this);
        var name = obj.attr('name');

        $('.search-select').each(function () {
            var cpm = $(this).val();
            var cpm_name = $(this).attr('name');
            if (val == cpm && name != cpm_name) {
                $(this).val('');
            }
        }).trigger("chosen:updated");
    });
});       