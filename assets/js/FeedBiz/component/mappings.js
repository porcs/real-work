jQuery(function (a, b) {
		a.fn.feed_colorpicker = function (c) {        
			var d = a.extend({
				pull_right: false,
				caret: true
			}, c);
			this.each(function () {
				var g = a(this);
				var e = "";
				var f = "";
				a(this).hide().find("option").each(function () {
					var h = "colorpick-btn";
					if (this.selected) {
						h += " selected";
						f = this.text;
					}
					// rel="text"
					if (this.title && this.title === "text")
					{
						e += '<li>';
						e += '<a class="' + h + '" href="#" data-color="' + this.value + '" title="' + this.value + '" style="width: 100%; text-align: center; background-color:' + this.text + '; font-size: 12px;">' + this.value + '</a>';
						e += '</li>';
					}
					else
					{
						e += '<li>';
						e += '<a class="' + h + '" href="#" style="background-color:' + this.value + ';" data-color="' + this.value + '" title="' + this.value + '"></a>';
						e += '</li>';
					}
				})
				.end()
				.on("change", function () {
					if (this.options[this.selectedIndex].title && this.options[this.selectedIndex].title === "text")
					{
						console.log(this.text);
						a(this).next().find(".btn-colorpicker").removeAttr('style');
						a(this).next().find(".btn-colorpicker").css('border', '1px solid #ccc').css("background-color", this.options[this.selectedIndex].text).css("height", '22px').css("width", '22px').html('').parent().find('.value-colorpicker').html(this.options[this.selectedIndex].value);
					}
					else
					{
						a(this).next().find(".btn-colorpicker").css("background-color", this.options[this.selectedIndex].text).css("width", '22px').css("height", '22px').html('');
					}
				})
				/*feed_colorpicker*/
				.after('<div class="dropdown dropdown-colorpicker m-t6 clearfix"><a data-toggle="dropdown" class="dropdown-toggle a-value-colorpicker"><div class="btn-colorpicker pull-left" style="' + (this.options[this.selectedIndex].title && this.options[this.selectedIndex].title === "text" ? "background:" + this.options[this.selectedIndex].text + ";width:22px;height:22px;border: 1px solid #e2e2e2;" : 'background-color:' + f + ';') + '">&nbsp;</div> <div class="value-colorpicker poor-gray pull-left small m-t5 m-l10">' + this.options[this.selectedIndex].value + '</div></a><ul class="dropdown-menu' + (d.caret ? " dropdown-caret" : "") + (d.pull_right ? " pull-right" : "") + '" style="max-width: 130px; min-width: 130px">' + e + '</ul></div>')
				.next()
				.find(".dropdown-menu")
				.on("click", function (j) {
					var h = a(j.target);
					if (!h.is(".colorpick-btn")) {
						return false;
					}
					h.closest("ul").find(".selected").removeClass("selected");
					h.addClass("selected");
					var i = h.data("color");
					g.val(i).change();
					j.preventDefault();
					return true;
				});
			});
			return this;
		};
	});
	
$(document).ready(function () {

    $('head').append('<link rel="stylesheet" href="'+base_url+'assets/css/bootstrap/bootstrap-colorpicker.css">');
	
    $('#back').on('click', function () {
        window.location.replace(base_url + "amazon/category/" + $('#id_country').val() + '/' + 3 + '/' + $('#mode').val() );
    });

    /*$('#form-mapping').validate({
        submitHandler: function (form)
        {
            var flag = true;
//            $('select[rel="attribute-color"]').each(function (i, j) {
//
//                if (!$(this).val() || $(this).val() == "None")
//                {
//                    flag = false;
//                    $('#error-color').show();
//                    $(this).closest('div.attribute-color').find('.value-colorpicker').html($('#Please-select-color').val()).css('color', 'red').removeClass('poor-gray');
//                }
//            });

            if (flag === true)
            {
                if($('#popup_status_more').length > 0) {
                    form.submit();
                } else {
                    bootbox.confirm($('#submit-confirm').val(), function(result) {
                        if(result) {
                            form.submit();
                        }                    
                    });
                }
              
            } else {
                $("html, body").animate({
                    scrollTop: 0
                }, 1000); 
            }
        }
    });*/

    $('#attribute-mapping').find('select.colorpicker').each(function ()
    {
        $(this).feed_colorpicker();
    });

    $('.attr-link').click(function () {
        var rel = $(this).attr('rel');
        console.log($(this).closest('div.none_valid_value').find('.mapping-valid-value'));
        $(this).closest('div.none_valid_value').find('.mapping-valid-value').toggle();
        if(rel == 1){
            $(this).attr('rel', 2);
            $(this).html($('#attr-link-2').val());
        } else {
            $(this).attr('rel', 1);
            $(this).html($('#attr-link-1').val());
        }
    });
    
    $('.addnewmappingcarrier').click(function ()
    {
        var cloned = $('#main-carrier').clone().appendTo('#new-mapping-carrier');

        cloned.removeClass('bg-color');
        if (cloned.prev().length > 0)
        {
            if (cloned.prev().hasClass('bg-color'))
            {
                cloned.removeClass('bg-color');
            } else {
                cloned.addClass('bg-color');
            }
        } else {
            cloned.addClass('bg-color');
        }

        cloned.css('padding', '5px 0');
        cloned.removeAttr('id');
        cloned.find('input').val('').attr('disabled', false);
        cloned.find('select').val('').attr('disabled', false).css('background', '#fff');

        var selected_select = cloned.find('select[rel="carrier-select"]');

        // Remove used options
        $('#new-mapping-carrier').find('select[rel="carrier-select"] option:selected').each(function (index, option)
        {
            selected_select.find('option[value="' + option.value + '"]').remove();
        });

        if (!selected_select.find('option').length)
        {
            cloned.remove();
            return(false);
        }

        var selected_value = selected_select.find('option:first').val();

        selected_select.attr('id', 'carrier-select-' + selected_value);
        selected_select.attr('data-content', selected_value);

        cloned.find('select[rel="carrier-value"]').attr('id', 'carrier-value-' + selected_value);
        cloned.find('select[rel="carrier-value"]').attr('name', 'carrier[' + selected_value + '][id_carrier]');

        selected_select.on('change', function () {

            var selected_option = $(this).find('option:selected').val();
            $(this).attr('id', 'carrier-select-' + selected_option);

            //change attribute value id
            $('#carrier-value-' + selected_value).attr('name', 'carrier[' + selected_option + '][id_carrier]');
            $('#carrier-value-' + selected_value).attr('id', 'carrier-value-' + selected_option);

            //change attribute id
            selected_select.attr('id', 'carrier-select-' + selected_option);
            selected_select.attr('data-content', selected_option);
        });

        cloned.find('.addnewmappingcarrier').remove();
        cloned.find('.removemappingcarrier').show().
                click(function ()
                {
                    removemappingcarrier($(this));
                });
    });

    //carrier-select-value
    $('#carrier-mapping').find('.carrier-select-value').on('change', function () {

        var data_contdent = $(this).attr('data-content');
        var selected_value = data_contdent;

        var selected_option = $(this).find('option:selected').val();
        console.log(selected_value);
        //change attribute value id
        $('#carrier-value-' + selected_value).attr('name', 'carrier[' + selected_option + '][id_carrier]');
        $('#carrier-value-' + selected_value).attr('id', 'carrier-value-' + selected_option);

        //change attribute id
        $(this).attr('id', 'carrier-select-' + selected_option);
        $(this).attr('data-content', selected_option);
    });

    function removemappingcarrier(obj)
    {
        var rel = $(obj).attr('rel');
        $(obj).parent().remove();

        if ($('#new-mapping-feature-' + rel).children().length === 0)
        {
            $('#feature-group-' + rel).find('input[rel="is_color"]').attr('disabled', false);
            $('#feature-group-' + rel).find('input[rel="is_color"]').parent().find('span.lbl').css('color', '#32a3ce');
        }
    }

});