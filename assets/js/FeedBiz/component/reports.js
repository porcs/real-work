var table_error_logs ;

$(document).ready(function(){

    // error log
    $('#product_reports').dataTable( {
         "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         "",
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + $('#class_call').val() + "/reports/" + $('#id_country').val(),
            "data": {method: "get_reports"},
            "type": "POST"
        },
        "columns": [           
            { "data": "batch_id", "bSortable": true, "width": "20%" },
            { "data": "sku", "bSortable": false, "width": "20%"  },
            { "data": "result_code", "bSortable": false, "width": "10%" },
            { "data": "result_message", "bSortable": false, "width": "30%" },
            { "data": "result_description", "bSortable": false, "width": "30%" },
            { "data": "date_add", "bSortable": true, "width": "20%"  }
        ],  
        fnDrawCallback: function( oSettings ) {
            console.log(oSettings);
        },     
        "pagingType": "full_numbers"
    });
    
    
    $('#product_reports tbody').on( 'click', 'tr td', function () {
        console.log($(this));

        if ( $(this).closest('tr').hasClass('selected') ) {
            $(this).closest('tr').removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).closest('tr').addClass('selected');
        }

        var tr = $(this).closest('tr');
        var row = table.row( tr );
        var data = row.data();

        $('html, body').animate({
            scrollTop: $(".error_logs").offset().top
        }, 2000);

       
        $('#error_logs').closest('.error_logs').removeAttr('style');
        table_error_logs.search( data.batch_id ).draw();
    } );


    var table = $('#product_reports').DataTable();
    
});
