var table_offer, table_product, option ;
var selected = [];
var detailsTableHtml;
var offer_process = false;

$(document).ready(function() {
    detailsTableHtml = $("#error_details").html();

    /* Offers */
    $('#error_offers').dataTable({       
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "asStripeClasses": [ 'b-Top' ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + /*$('#marketplace_name').val() +*/ "new_marketplace/error_resolutions/" + $('#id_country').val(),
            "type": "POST",
            "data": {
                'method': 'test',
                'action_type': 'Create',
            }
        },
        "columns": [         	     
           	{ "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                render: function () {
                    return '<a class="data_control plus">+</a>';
                }
            },
           	{ "data": "code", "bSortable": true, "width": "10%", "className": 'p-l10'  },
            { "data": "description", "bSortable": true, "width": "70%", "className": 'p-l10' },
            { "data": "rows", "bSortable": true, "width": "15%", "className": 'text-right p-r10' },           
        ],       
        "bFilter" : true,
        "paging": true,
        "pagingType": "full_numbers" ,  
        "order": [[1, 'asc']],   
        "drawCallback": function() {
            console.log("AAA");
//            error_products();
            offer_process = true;
            var api = this.api();
            var search = api.search();

            $.each( api.rows().data(), function ( i,j ) {
                // If Search By SKU
                if(j.sku){
                    var tr = api.row(i).node();
                    var row = api.table().row( tr );

                    //console.log(row.child.isShown());
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        $(tr).removeClass('shown');                        
                        $(tr).find('.data_control').removeClass('minus').addClass('plus').html('+');

                    } else {
                        var d = row.data();
                        var iTableCounter = 'amazon_error_' + $('#offers').val() + '_' + d.code;
                        var table_clone = fnFormatDetails(iTableCounter, detailsTableHtml);            
                        
                        // Open this row
                        row.child( table_clone ).show();

                        //console.log(tr);
                        if($('#'+iTableCounter).length > 0)
                        {
                            row_details(d, $('#offers').val(), search);
                        }

                        row.child().find('select').addClass('search-select');

                        //console.log(row.child().find('select'));
                        row.child().addClass('background-gray');    

                        $(".search-select").chosen({
                            disable_search_threshold: 1,
                            no_results_text: $('#l_Nothing_found').val()
                        }).trigger("chosen:updated");   

                        $('.chosen-single').find('div').addClass('button-select');
                        $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');                        
                        $(".dataTables_wrapper .chosen-search").remove();

                        $(tr).addClass('shown');            
                        $(tr).find('.data_control').removeClass('plus').addClass('minus').html('-');
                    }
                }
            });    
        }  	
    }); 
    
    $('#error_offers_filter').hide();
    table_offer = $('#error_offers').DataTable();
    
    $('#search_sku_offers').on('change', function () {       
        table_offer.search( 'sku=' + this.value ).draw();

    });

    $('#error_offers tbody').on('click', '.data_control', function () {		
        var tr = $(this).closest('tr');
        var row = table_offer.row( tr );
        //console.log(row);
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');            
            $(this).removeClass('minus').addClass('plus').html('+');

        } else {

            var d = row.data();

            console.log(d);
            //row_details(row.data(), $('#offers').val(), tr, row, $(this));
            var iTableCounter = 'amazon_error_' + $('#offers').val() + '_' + d.code;
            var table_clone = fnFormatDetails(iTableCounter, detailsTableHtml);            
            
            // Open this row
            row.child( table_clone ).show();

            console.log(d);
            //td.css('background-color','white');
            if($('#'+iTableCounter).length > 0)
            {
                row_details(d, $('#offers').val());
            }

            row.child().find('select').addClass('search-select');
            row.child().addClass('background-gray');	

            $(".search-select").chosen({
                    disable_search_threshold: 1,
                    no_results_text: $('#l_Nothing_found').val()
            }).trigger("chosen:updated");	

            $('.chosen-single').find('div').addClass('button-select');
            $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');

            $(".dataTables_wrapper .chosen-search").remove();

            tr.addClass('shown');            
            $(this).removeClass('plus').addClass('minus').html('-');
            row.child().find('.p-l10 .text-center').addClass('checkboSettings');	
        }
    });
}); 