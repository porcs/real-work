$(document).ready(function () {
    $('#my_task_list').DataTable({ searching:false, paging: false});
    var ext                 = $('#my_task_list').attr('ext');
    var market              = $('#my_task_list').attr('market');
    var id_country          = $('#id_country').val();
    var marketplace_name    = $('#marketplace_name').val();
    var class_call          = $('#class_call').val();
    
    $('.countdown_next').each(function(){      
        $(this).countdown({
            timestamp	: (new Date()).getTime() + $(this).attr('rel') * 1000,
            callback	: function(days, hours, minutes, seconds){},
            break_sign  : ':'
        });
    });
    
    $('table thead tr').addClass("b-Bottom");
    $('#my_task_list input').checkBo();
    setTimeout(function(){
    $('#my_task_list input[type=checkbox]').trigger('change'); 
        $('#my_task_list input[type=checkbox]').change(function() {
            if($(this).hasClass('sel_all')) {
                $('#my_task_list input[type=checkbox].sel_row').prop('checked', $(this).is(':checked')).trigger('change');
            } 
            else {
                var cid     = $(this).attr('crel');
                var status  = $(this).is(':checked');
                var method  = 'ajax_update_cron_status';
                $.ajax({ method: 'post', url: base_url + class_call + '/scheduled_tasks/' + id_country, data:{ method: method, ext:ext, cid:cid, status:status },
                    success:function(data){
                        console.log(data);
                    }
                });
            }
            
        });
        $('table th:eq(2)').prepend('<div id="category_options_processing" class="dataTables_processing"></div>');

        if($('.dataTables_processing').length > 0) {
            $('.dataTables_processing').html('');
            $('.dataTables_processing').append('<div class="feedbizloader"><div class="feedbizcolor"></div><p class="m-t10 text-info">'+$('#processing').val()+'</p></div>');
            if($('.dataTables_processing').find('.feedbizloader')){
                $('.dataTables_processing').find('.feedbizloader').find('.feedbizcolor').sprite({fps: 10, no_of_frames: 30});
            }
        }
         if($('.dataTables_processing').length > 0){
            $('.dataTables_processing').show();
        }  
        var run = [];
        $(".task_row").each(function(){
            var rel = $(this).attr('rel');
            if(rel!='order'){
                rel = 'product';
                $(this).attr('rel',rel);
            }
            
            if( !run[rel] ) {
                run[rel] = true;
                var method  = 'ajax_get_serv_status';
                $.ajax({ method: 'post', url: base_url + class_call + '/scheduled_tasks/' + id_country, data:{ method: method, task: rel, id_country: id_country }, dataType:'json',
                    success:function(data){
 
                        var status = data.status;
                        var timestamp = data.timestamp;
                        $('#my_task_list').find('tr.task_row[rel='+rel+']').each(function(){
                           $(this).find('.t_time').html(timestamp);
                           $(this).find('.t_status').addClass(status);
                        });
                        
                        if($('.dataTables_processing').length > 0){
                            $('.dataTables_processing').hide();
                        }
                    }
                });
            }
        });
    },100);
    
    
});