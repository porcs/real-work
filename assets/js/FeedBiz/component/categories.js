
var just_cal = false;
var just_check_all = false;
$(document).ready(function(){
    
    getSelectedAllCategory();
    apply();     
    
});

function init_rel_all_row(){
    var i=1;
    if(just_cal) return;
    just_cal = true;
    $('.cp_down_btn').each(function(){
       if($(this).parents('#main').length>0)return;
       var row = $(this).closest('li');
       if(row.length == 0)return;
       row.attr('rel',i); 
       i++;
    });
}

function click_copy_val_sel(obj){

    showWaiting(true);
    var time =0;
    var num = $('body').find('select[name*="category"]').length;
    if(num > 200){
        time =200;
    } 

     setTimeout(function(){
        init_rel_all_row();
        var p_row =  obj.closest('li');
        var cur_pos = p_row.attr('rel');
        var currentLeafOffset = $(p_row).offset();
        var cur_sel_val = obj.closest('.treeRight').find('select').val(); 
        var break_f = false;
        var break_l = false; 

        $('#tree1').find('li').each(function(eleIndex, eleObject){
            if(typeof $(this).attr('rel') == 'undefined')return;
            var rel = $(this).attr('rel'); 
            if(break_f)return; 
            if(currentLeafOffset.top < $(eleObject).offset().top){
                /*var test_val;
                var l =  $(this).find('select').length ;
                $(this).find('select').each(function(eleIndex){ 
                    if(eleIndex*1==(l*1)-1){ 
                         test_val = $(this).val(); 
                    }
                });
                 
                if(test_val!=''){  
                    break_f=true;
                    return;
                }   */
                $(this).find('select').each(function(eleIndex){ 
                    //if(eleIndex*1==(l*1)-1){ 
                            $(this).val(cur_sel_val).chosen({
                                disable_search_threshold: 1,
                                no_results_text: "Nothing found!"
                            }).trigger("chosen:updated");; 
                    //}
                });                        
                       
            }else{ return;}

        });

        showWaiting(false);

     },time);
   
}

function apply(){

    setTimeout(function(){$('li.expandAll').trigger('click');  },100);
    
    $('.add_category').unbind('click').on('click', function(){        
        expandCollapse($(this)); 
    });
    
    $('input[rel="id_category"]').unbind('click').change(function(){//.unbind('click')
        getProfile($(this));
        recParCheck($(this));
        if(!just_check_all){
            just_cal=false; 
        }      
    
    });

    function recParCheck(cur){
        
        var par = cur.parents('ul').prev();
        if(!par.hasClass('tree_item')) return;
        par = par.find('input[rel="id_category"]');//.first();
        if(!par.is(':checked') && cur.is(':checked')){
           par.prop('checked', true).change();
           recParCheck(par);
        }             
    }    
    
   $.fn.enableCheckboxRangeSelection = function() {
        var lastCheckbox = null;
        var $spec = this;
        var lastType = null;
        $spec.unbind("click");
        $spec.bind("click", function(e) {
            showWaiting(true);
            lastType = $(lastCheckbox).find('input[type=checkbox]').is(':checked');// e.currentTarget.checked;
            var $target = this;//e.target
            if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {

                var $checkbox = $spec.slice(
                    Math.min($spec.index(lastCheckbox), $spec.index(this)),
                    Math.max($spec.index(lastCheckbox), $spec.index(this)) + 1
                );
                
                $checkbox.find('input[type=checkbox]').each(function () {
                       
                    $(this).prop({ 'checked' : lastType}).change();
                    var chk = Boolean($(this).find('input').attr('checked'));
                    
                    if($(this)[0] !== lastCheckbox){
                        switch(true){
                            case chk :  $(this).parent().parent().find('.treeRight').remove(); break;
                            case !chk : getProfile($(this).parent().find($("input[type=checkbox]"))); break;
                            default: getProfile($(this).parent().find($("input[type=checkbox]"))); break;
                        }
                    }
                });
            }
            lastCheckbox = this;
            showWaiting(false);
        });
    };

    $('label.cb-checkbox').enableCheckboxRangeSelection();

}

//Get Profile
function getProfile(e) {

    var select;
    var ele = $(e).parents('.tree_item').parent();

    if($(e).is(':checked'))
    {
        if(ele.find('select.profile').length === 0)
        {
            var cloned = $('#main').find('.treeRight').clone();
            cloned.appendTo(ele);

            cloned.find('select.profile').attr('name', 'category[' + $(e).val() + '][id_profile]');
            cloned.find('.cp_down_btn').click(function(){
                click_copy_val_sel($(this));
            });

			ele.find('select').each(function(i,selector){
				if(!$(selector).attr('name')){
					$(selector).attr('name', 'category[' + $(e).val() + '][id_profile]');
					$(selector).attr('id', 'category' + $(e).val() + '_id_profile');
				}
			});

            select = cloned.find('select.profile');                 
        }  else {
            select = ele.find('select.profile');
        }
    } else {
        ele.find('.treeRight').remove();
    }

    // Remove old chosen
    if(ele.find('div.chosen-container').length > 0){
        ele.find('div.chosen-container:last').remove();
    }

    choSelect();   
        
    return select;
}

function getSelectedAllCategory() {
    showWaiting(true);
    var url = base_url + $('#class_call').val() + "/categories/" + $('#id_country').val();

    if ($('#popup_status').length != 0)
    {
        url = url + '/' + $('#mode').val();
    }

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: { method: 'getSelectedAllCategory' },
        success: function(data) {

            if(data && data != "")
            {
                $('#has_category').show();
                $('#tree1').html(data);
   
                tree($('.tree'));

                apply();
                setTimeout(function(){

                    $(".search-select").chosen();
                    $('.chosen-single').find('div').addClass('button-select');
                    $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
                    showWaiting(false);
                    $('#page-loaded').show();
                    $('#page-load').hide(); 
                    $('.custom-form').checkBo();
                },500);

                $('.chosen-single').find('div').addClass('button-select');
                $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');    
                
            }
            else
            {
                showWaiting(false);
                $('#has_category').hide();
                $('#none_category').show();
                $('#page-loaded').show();
                $('#page-load').hide();      
                $('.tree').hide();      
                apply();
            }

        },
        error: function(data, error) {
            console.log(data);
            console.log(error);
        }
    });
}

function expandCollapse(e){
    
    if(!e.hasClass('active')){
        e.parent().parent().parent().find('ul.tree_child').slideDown();
        e.addClass('active');
    }else{
        e.parent().parent().parent().find('ul.tree_child').slideUp();
        e.removeClass('active');
    }
}

function expandCollapseAll(e){
    
    var content = e.parent().parent().find('.tree-folder-content');
        
    if(content.children().length > 1){
        content.show();                
    }else{
        getSelectedAllCategory(e);
    }
        
}