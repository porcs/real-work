var table_error_logs ;

$(document).ready(function(){

    // error log
    $('#error_logs').dataTable( {
         "language": {
            "emptyTable":     $('#emptyError').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "searchPlaceholder":         $('input#search').val(),
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "processing": true,
        "serverSide": true,
        "order": [[0, 'asc']],
        "ajax": {
            "url": base_url + $('#class_call').val() + "/logs/" + $('#id_country').val(),
            "data": {method: "get_logs_details"},
            "type": "POST"
        },
        "columns": [           
            { "data": "reference", "bSortable": true, "width": "20%" },
            { "data": "message", "bSortable": false, "width": "80%"  }
        ],       
        "pagingType": "full_numbers"
    } );
    
    table_error_logs = $('#error_logs').DataTable();

    $('#product_logs').dataTable( {
         "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "searchPlaceholder":         $('input#search').val(),
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + $('#class_call').val() + "/logs/" + $('#id_country').val(),
            "data": {method: "get_logs"},
            "type": "POST"
        },
        "columns": [           
            { "data": "date_upd", "bSortable": true, "width": "20%"  },
            { "data": "batch_id", "bSortable": true, "width": "20%" },
            { "data": "action_type", "bSortable": true, "width": "40%" },
            { "data": "no_send", "bSortable": true, "width": "10%", "class":"text-right m-r5" },
            { "data": "no_skipped", "bSortable": false, "width": "10%","class":"text-right m-r5"  },
        ],       
        "pagingType": "full_numbers"
    });
    
    
    $('#product_logs tbody').on( 'click', 'tr td', function () {
        console.log($(this));

        if ( $(this).closest('tr').hasClass('selected') ) {
            $(this).closest('tr').removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).closest('tr').addClass('selected');
        }

        var tr = $(this).closest('tr');
        var row = table.row( tr );
        var data = row.data();

        $('html, body').animate({
            scrollTop: $(".error_logs").offset().top
        }, 2000);

       
        $('#error_logs').closest('.error_logs').removeAttr('style');
        table_error_logs.search( data.batch_id ).draw();
    } );


    var table = $('#product_logs').DataTable();
    
});
