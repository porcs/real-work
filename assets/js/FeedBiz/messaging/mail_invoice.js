$(document).ready(function () {
	$('.cb-switcher input').on('change', function(e){
		//if($(e.currentTarget).find('input:checkbox').prop('checked')){
		if($(this).prop('checked')){
			$(this).parents('.form-group').parent().find('.configure-content').animate( { height: "show" }, 500, "easeInExpo", function(){ 
			$(this).removeClass('hidden'); } );
		}else{
			$(this).parents('.form-group').parent().find('.configure-content').animate( { height: "hide" }, 1500, "easeOutExpo", function(){ 
			$(this).addClass('hidden'); } );
		}
	});
	
	$('#uploadFile').on('shown.bs.modal', function (e) {
		var _this = $(e.currentTarget);
		_this.find('#fileToUpload').val('');
		_this.find('input[type="text"]').attr('value','');
		angular.element('#fileuploadctrl').scope().files = [];
		angular.element('#fileuploadctrl').scope().setTitleModel(e);
		angular.element('#fileuploadctrl').scope().fileOnServer();
		if(angular.element('#fileuploadctrl').scope().currentUploadFile == "additionnal-file"){
			$(e.currentTarget).find('#fileToUpload').attr('accept','.pdf');
		}else if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
			$(e.currentTarget).find('#fileToUpload').attr('accept','.html');
		}
	});
		
	$('a#preview_template').colorbox({
        iframe:true , 
        href: function(){
			var url = base_url + "messaging/preview_file/" + $('input[name="marketplace_name"]').val() + "/" + $('input[name="id_country"]').val() + "/" + $('select[name="mail_invoice_template"] option:selected').val();
			if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
				url += '/html';
			}
			$.ajax({url: url, success: function(result){
				$('#cboxLoadedContent').html(result.output);
				$('iframe html').html('');
			}});
			/*$.getJSON(url, function(data) {
				$('#cboxLoadedContent').html(data.output);
				$('iframe html').html('');
			});*/
			
		},
		innerWidth:'70%',
        innerHeight:'80%',
        width:'90%',
        height:'80%',
        escKey: true, //true
        overlayClose: true, //true
        onLoad: function() {
        },
        onClosed:function(){
            //setTimeout(function(){window.location.reload();},1000);
            //$('body').css('overflow-y','auto');
            //$.ajax({url:"/users/ajax_switch_popup"});
        }
    });
});

angular.module('app', []).controller('FileUploadCtrl', function($scope,$http){
	//============== DRAG & DROP =============
    // source for drag&drop: http://www.webappers.com/2011/09/28/drag-drop-file-upload-with-html5-javascript/
    /*var dropbox = document.getElementById("dropbox");
    $scope.dropText = 'Drop files here...';

    // init event handlers
    function dragEnterLeave(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        $scope.$apply(function(){
            $scope.dropText = 'Drop files here...';
            $scope.dropClass = '';
        });
    }
    dropbox.addEventListener("dragenter", dragEnterLeave, false);
    dropbox.addEventListener("dragleave", dragEnterLeave, false);
    dropbox.addEventListener("dragover", function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        var clazz = 'not-available';
        var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') >= 0
        $scope.$apply(function(){
            $scope.dropText = ok ? 'Drop files here...' : 'Only files are allowed!';
            $scope.dropClass = ok ? 'over' : 'not-available';
        })
    }, false)
    dropbox.addEventListener("drop", function(evt) {
        console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)));
        evt.stopPropagation();
        evt.preventDefault();
        $scope.$apply(function(){
            $scope.dropText = 'Drop files here...';
            $scope.dropClass = '';
        })
        var files = evt.dataTransfer.files;
        if (files.length > 0) {
            $scope.$apply(function(){
                $scope.files = [];
                for (var i = 0; i < files.length; i++) {
                    $scope.files.push(files[i]);
                }
            })
        }
    }, false)*/
    //============== END DRAG & DROP =============
	$scope.setTitleModel = function(element){
		$scope.$apply(function($scope){
			$scope.name_title = element.relatedTarget.innerText;
			$scope.currentUploadFile = element.relatedTarget.className.split(" ")[1];
			//console.log($scope.currentUploadFile);
		});
	}

    $scope.setFiles = function(element){
		$scope.$apply(function($scope){
			//console.log('files:', element.files);
			 // Turn the FileList object into an Array
			$scope.files = [];
			$scope.namefile_all = '';
				for (var i = 0; i < element.files.length; i++){
				  $scope.files.push(element.files[i]);
				  $scope.namefile_all += element.files[i].name;
				  if(element.files.length > 1 && i < element.files.length-1)
					  $scope.namefile_all += ', ';
				}
			//$scope.namefileoneline();
			$scope.progressVisible = false;
		});
    };
	
	$scope.removeFileItem = function(file, index){
		$scope.files.splice($scope.files.indexOf(file),1);
		//files.splice($index, 1);
		//$scope.files.splice(index, 1);
		
		$scope.namefile_all = '';
		for (var i = 0; i < $scope.files.length; i++){
		  $scope.namefile_all += $scope.files[i].name;
		  if($scope.files.length > 1 && i < $scope.files.length-1)
			  $scope.namefile_all += ', ';
		}
	};

    $scope.uploadFile = function(){
		
        var formdata = new FormData();
        for (var i in $scope.files){
            formdata.append("file-upload["+i+"]", $scope.files[i]);
        }
        /*var xhr = new XMLHttpRequest();
        xhr.upload.addEventListener("progress", uploadProgress, false);
        xhr.addEventListener("load", uploadComplete, false);
        xhr.addEventListener("error", uploadFailed, false);
        xhr.addEventListener("abort", uploadCanceled, false);
        xhr.open("POST", base_url+"messaging/upload_file/" + $('input[name="action"]').val() + "/" + $('input[name="marketplace_name"]').val() + "/" + $('input[name="id_country"]').val());
		xhr.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
		$scope.progressVisible = true;
        xhr.send(formdata);*/
		
		if (formdata) {
			$scope.progressVisible = true;
			var url = base_url + "messaging/upload_file/" + $('input[name="marketplace_name"]').val() + "/" + $('input[name="id_country"]').val();
			if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
				url += '/html';
			}
			$.ajax({
				url: url,
				type: "POST",
				data: formdata,
				xhr: function() {
					var xhr = $.ajaxSettings.xhr();
					if(xhr.upload){
						xhr.upload.addEventListener("progress", uploadProgress, false);
						xhr.addEventListener("load", uploadComplete, false);
						xhr.addEventListener("error", uploadFailed, false);
						xhr.addEventListener("abort", uploadCanceled, false);
					}
					return xhr;
				},
				processData: false,
				contentType: false,
				success: function(res) {
					$scope.progressVisible = false;
					$scope.fileOnServer();
				},
				error: function(res) {
					$scope.progressVisible = false;
				}
			});
        }
    };

    function uploadProgress(evt) {
        $scope.$apply(function(){
            if (evt.lengthComputable){
                $scope.progress = Math.round(evt.loaded * 100 / evt.total);
            } else {
                $scope.progress = 'unable to compute';
            }
        });
    }

    function uploadComplete(evt){
        /* This event is raised when the server send back a response */
        //alert(evt.target.responseText)
		$scope.progressVisible = false;
		$scope.files = [];
    }

    function uploadFailed(evt){
        alert("There was an error attempting to upload the file.");
    }

    function uploadCanceled(evt){
        $scope.$apply(function(){
            $scope.progressVisible = false;
        });
        alert("The upload has been canceled by the user or the browser dropped the connection.");
    }
	
	$scope.fileOnServer = function(){
		$scope.progressVisible = true;
		var url = base_url + "messaging/get_file_name/" + $('input[name="marketplace_name"]').val() + "/" + $('input[name="id_country"]').val()+"/1";
		if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
			url += '/html';
		}
		$http({
			method: 'GET',
			url: url,
			headers: {'Content-Type': 'application/json'},
		}).success(function(data, status, headers, config){
			//console.log(data);
			if(data.pass || data.length > 0){
				$scope.files_server = data;
			}else{
				$scope.files_server = "";
			}
				$scope.updateAdditionSelectFile();
		}).error(function(data, status, headers, config){
			
		});
	};
	
	$scope.updateAdditionSelectFile = function(){		
		if(angular.element('#fileuploadctrl').scope().currentUploadFile == "additionnal-file"){
			$('.search-select.additionnal').chosen('destroy');
			$('.search-select.additionnal option[value!=""]').remove();
			angular.forEach($scope.files_server, function(value, key) {
				$('.search-select.additionnal').append('<option value="' + value + '">' + value + '</option>');
			});
			$('.search-select.additionnal').chosen();
		}else if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
			$('select[name="mail_invoice_template"].search-select').chosen('destroy');
			$('select[name="mail_invoice_template"].search-select option[value!=""].customize_template').remove();
			angular.forEach($scope.files_server, function(value, key) {
				$('select[name="mail_invoice_template"].search-select').append('<option class="customize_template" value="' + value.replace(/\.[^/.]+$/, "") + '">' + value.replace(/\.[^/.]+$/, "") + '</option>');
			});
			$('select[name="mail_invoice_template"].search-select').chosen();
		}
		
		$('.chosen-single').find('div').addClass('button-select');
		$('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
	};
	
	$scope.deleteFileOnServer = function(file_s){
		var url = base_url + "messaging/delete_file/" + $('input[name="marketplace_name"]').val() + "/" + $('input[name="id_country"]').val();
		if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
			url += '/html';
		}
		$.ajax({
				url: url,
				type: "POST",
				data: { filename: file_s },
				dataType: 'json',
				success: function(res) {
					if(res.pass){
						$scope.fileOnServer();
					}else{
						alert("Can not Delete File!");
					}
				},
				error: function(res) {
					$scope.progressVisible = false;
				}
			});
	};
	
});