$(document).ready(function () {

	$('#duplicate_smtp').on('click', function(e){	
		$('#smtp_select').parent().toggle();
	});	

	$('#smtp_select').on('change', function(e){	

		if($(this).val() == ''){
			$('#smtp_parameters').find('select, input').each(function(i,j) {
				if($(j).is('input')){
        			$(j).val('');
        		} else if($(j).is('select')) {
					$(j).find('option').removeAttr('selected');
			        $(j).trigger("chosen:updated");
        		}
			});
			return;
		} 

		$.ajax({
	        url: base_url + "/messaging/get_duplicate_messaging/" + $(this).val(),
	        type: 'POST',
	        dataType: 'json',
	        success: function(data){
	            if(data){
	            	$.each(data, function(k,d){
	            		if($('#'+k).is('input')){
	            			$('#'+k).val(d);
	            		} else if($('#'+k).is('select')) {
							$('#'+k).find('option').each(function(index, option) {
					        	if($(option).val() == d){
					        		$(this).attr('selected','selected');
					        		$('#'+k).trigger("chosen:updated");
					        	}
					        });
	            		}
	            	});	   
	            }	            
	        },
	        error: function(error, message){
	            notification_alert($('#error-trigger-reprice').val(), 'bad'); 
	            console.log(error, message);
	        }   
	    }); 
	});	

	$('.cb-switcher input').on('change', function(e){		
		if($(this).prop('checked')){
			$(this).parents('.form-group').parent().find('.configure-content').animate( { height: "show" }, 500, "easeInExpo", function(){ 
			$(this).removeClass('hidden'); } );
		}else{
			$(this).parents('.form-group').parent().find('.configure-content').animate( { height: "hide" }, 1500, "easeOutExpo", function(){ 
			$(this).addClass('hidden'); } );
		}
	});
	
	$('#uploadFile').on('shown.bs.modal', function (e) {
		var _this = $(e.currentTarget);
		_this.find('#fileToUpload').val('');
		_this.find('input[type="text"]').attr('value','');
		angular.element('#fileuploadctrl').scope().files = [];
		angular.element('#fileuploadctrl').scope().setTitleModel(e);
		angular.element('#fileuploadctrl').scope().fileOnServer();

		if(angular.element('#fileuploadctrl').scope().currentUploadFile == "additionnal-file"){
			$(e.currentTarget).find('#fileToUpload').attr('accept','.pdf');
		}else if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
			$(e.currentTarget).find('#fileToUpload').attr('accept','.html');
		}
	});
		
	$('a#preview_template').colorbox({
        iframe:true , 
        href: function(){
			var url = base_url + "messaging/preview_file/" + $('input[name="marketplace_name"]').val() + "/" + $('input[name="id_country"]').val() + "/" + $('select[name="'+$('#action').val()+'_template"] option:selected').val();
			if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
				url += '/html';
			}
			$.ajax({url: url, success: function(result){
				$('iframe html').html('');
				$("#cboxLoadedContent>iframe").contents().find("body").html(result.output);
			}});
		},
		innerWidth:'70%',
        innerHeight:'80%',
        width:'90%',
        height:'80%',
        escKey: true, //true
        overlayClose: true, //true
        onLoad: function() {
        },
        onClosed:function(){
        }
    });

    $('#popUp').on('shown.bs.modal', function (e) {
        var _this = $(e.currentTarget);
        _this.find('.modal-body').html($('input#'+e.relatedTarget.dataset.text).val());
     });
	
	$('#uploadFile').on('shown.bs.modal', function (e) {
		var _this = $(e.currentTarget);
		_this.find('#fileToUpload').val('');
		_this.find('input[type="text"]').attr('value','');
		angular.element('#fileuploadctrl').scope().files = [];
		angular.element('#fileuploadctrl').scope().fileOnServer();
	});
});


angular.module('app', []).controller('FileUploadCtrl', function($scope,$http){
	
    //============== END DRAG & DROP =============
	$scope.setTitleModel = function(element){
		$scope.$apply(function($scope){
			$scope.name_title = element.relatedTarget.innerText;
			$scope.currentUploadFile = $(element.relatedTarget).attr('data-name');
		});
	}

    $scope.setFiles = function(element){
		$scope.$apply(function($scope){
			// Turn the FileList object into an Array
			$scope.files = [];
			$scope.namefile_all = '';
				for (var i = 0; i < element.files.length; i++){
				  $scope.files.push(element.files[i]);
				  $scope.namefile_all += element.files[i].name;
				  if(element.files.length > 1 && i < element.files.length-1)
					  $scope.namefile_all += ', ';
				}
			$scope.progressVisible = false;
		});
    };
	
	$scope.removeFileItem = function(file, index){

		$scope.files.splice($scope.files.indexOf(file),1);	
		$scope.namefile_all = '';
		for (var i = 0; i < $scope.files.length; i++){
		  $scope.namefile_all += $scope.files[i].name;
		  if($scope.files.length > 1 && i < $scope.files.length-1)
			  $scope.namefile_all += ', ';
		}
	};

    $scope.uploadFile = function(){
		
        var formdata = new FormData();
        for (var i in $scope.files){
            formdata.append("file-upload["+i+"]", $scope.files[i]);
        }
		
		if (formdata) {
			$scope.progressVisible = true;
			var url = base_url + "messaging/upload_file/" + $('input[name="marketplace_name"]').val() + "/" + $('input[name="id_country"]').val();
			if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
				url += '/html';
			}
			$.ajax({
				url: url,
				type: "POST",
				data: formdata,
				xhr: function() {
					var xhr = $.ajaxSettings.xhr();
					if(xhr.upload){
						xhr.upload.addEventListener("progress", uploadProgress, false);
						xhr.addEventListener("load", uploadComplete, false);
						xhr.addEventListener("error", uploadFailed, false);
						xhr.addEventListener("abort", uploadCanceled, false);
					}
					return xhr;
				},
				processData: false,
				contentType: false,
				success: function(res) {
					$scope.progressVisible = false;
					$scope.fileOnServer();
				},
				error: function(res) {
					$scope.progressVisible = false;
				}
			});
        }
    };

    function uploadProgress(evt) {
        $scope.$apply(function(){
            if (evt.lengthComputable){
                $scope.progress = Math.round(evt.loaded * 100 / evt.total);
            } else {
                $scope.progress = 'unable to compute';
            }
        });
    }

    function uploadComplete(evt){
        /* This event is raised when the server send back a response */
		$scope.progressVisible = false;
		$scope.files = [];
    }

    function uploadFailed(evt){
        alert("There was an error attempting to upload the file.");
    }

    function uploadCanceled(evt){
        $scope.$apply(function(){
            $scope.progressVisible = false;
        });
        alert("The upload has been canceled by the user or the browser dropped the connection.");
    }
	
	$scope.fileOnServer = function(){
		$scope.progressVisible = true;
		var url = base_url + "messaging/get_file_name/" + $('input[name="marketplace_name"]').val() + "/" + $('input[name="id_country"]').val()+"/1";
		if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
			url += '/html';
		}
		$http({
			method: 'GET',
			url: url,
			headers: {'Content-Type': 'application/json'},
		}).success(function(data, status, headers, config){
			//console.log(data);
			if(data.pass || data.length > 0){
				$scope.files_server = data;
			}else{
				$scope.files_server = "";
			}
			$scope.updateAdditionSelectFile();
		}).error(function(data, status, headers, config){
			
		});
	};
	
	$scope.updateAdditionSelectFile = function(){		
		if(angular.element('#fileuploadctrl').scope().currentUploadFile == "additionnal-file"){
			$('.search-select.additionnal').chosen('destroy');
			$('.search-select.additionnal option[value!=""]').remove();
			angular.forEach($scope.files_server, function(value, key) {
				$('.search-select.additionnal').append('<option value="' + value + '">' + value + '</option>');
			});
			$('.search-select.additionnal').chosen();
		}else if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
			console.log($('#action').val());
			$('select[name="'+$('#action').val()+'_template"].search-select').chosen('destroy');
			$('select[name="'+$('#action').val()+'_template"].search-select option[value!=""].customize_template').remove();
			angular.forEach($scope.files_server, function(value, key) {
				$('select[name="'+$('#action').val()+'_template"].search-select').append('<option class="customize_template" value="' + value.replace(/\.[^/.]+$/, "") + '">' + value.replace(/\.[^/.]+$/, "") + '</option>');
			});
			$('select[name="'+$('#action').val()+'_template"].search-select').chosen();
		}
		
		$('.chosen-single').find('div').addClass('button-select');
		$('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
	};
	
	$scope.deleteFileOnServer = function(file_s){
		var url = base_url + "messaging/delete_file/" + $('input[name="marketplace_name"]').val() + "/" + $('input[name="id_country"]').val();
		if(angular.element('#fileuploadctrl').scope().currentUploadFile == "mail-template"){
			url += '/html';
		}
		$.ajax({
				url: url,
				type: "POST",
				data: { filename: file_s },
				dataType: 'json',
				success: function(res) {
					if(res.pass){
						$scope.fileOnServer();
					}else{
						alert("Can not Delete File!");
					}
				},
				error: function(res) {
					$scope.progressVisible = false;
				}
			});
	};
	
});