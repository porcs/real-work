 
    $.xhrPool = [];
    $.xhrPool.abortAll = function() {
        $(this).each(function(i, jqXHR) {   //  cycle through list of recorded connection
            jqXHR.abort();  //  aborts connection
            $.xhrPool.splice(i, 1); //  removes from list by index
        });
    }
    $.ajaxSetup({
        beforeSend: function(jqXHR) { $.xhrPool.push(jqXHR); }, //  annd connection to list
        complete: function(jqXHR) {
            var i = $.xhrPool.indexOf(jqXHR);   //  get index for current connection completed
            if (i > -1) $.xhrPool.splice(i, 1); //  removes from list by index
        }
    });
    function uniqId() {
        return Math.round(new Date().getTime() + (Math.random() * 100));
    }   
    if($('input#upgrade_mode_txt').val()==''){
        var restrict_notify_txt = '<div id="notification_mode" class="notification_mode"><div class="container"><p><i class="fa fa-lock"></i> '+$('input#restrict_txt').val()+'</p></div></div>';
    }else{
        var restrict_notify_txt = '<div id="notification_mode" class="notification_mode"><div class="container"><p><i class="fa fa-lock"></i> '+$('input#restrict_txt').val()+'<a class="link" data-toggle="modal" data-target="#upgrade_mode"><i class="fa fa-star"></i> '+$('input#upgrade_mode_txt').val()+'</a></p></div></div>';
    }
    var dp_name = 'restrict_panel';
    var dp_name2 = 'restrict_panel';
     function check_protection(){     
         if($(".notification_mode").length==0){ 
             $('header').after($(restrict_notify_txt));
         }else if($(".notification_mode a.link").length==0){
             $('body .notification_mode').remove();
             $('header').after($(restrict_notify_txt));
         }
         $('body').find('.notification_mode').css('z-index','1001');
        var $main = $('body div.main'); 
        if($main.find('.'+dp_name).length!=0 && !$main.find('.'+dp_name).is(':visible')){
                $main.find('.'+dp_name).remove();
        }
        if($main.find('.'+dp_name).length==0){ 
            
            dp_name = 'restrict_panel_'+uniqId();
            dp_name2 = 'restrict_panel_'+uniqId();
            var $restrict_panel = $('<div class="'+dp_name+'"><div class="'+dp_name2+'"></div></div>');
            $main.append($restrict_panel);
        }
        $main.css('opacity',1);
        $main.find('.'+dp_name).show().css({'display':'block ', 'position':'absolute','top':'0','left':0,'z-index':'999','width':'100%','height':'100%','opacity':'0.6','background':'white'});
        if($main.find('.validateRow').length>0){
            $main.find('.validateRow').parent().parent().parent().css({'position':'relative','z-index':'1000'});
        }
        
        if($main.find('form').length>0){
            $main.find('form').each(function(){
                $(this).unbind('submit').submit(function(e){
                   e.preventDefault();
                   return false;
                });
            })
        }
        if($main.find('.cb-checkbox').length>0){
            $main.find('.cb-checkbox').each(function(){
                $(this).unbind();
            })
        }
        
        $main.find('select, input,textarea').removeAttr('name').prop('disabled', true);
        $main.find('button').each(function(){
                    $(this).removeAttr('id').removeAttr('type').prop('disabled', true);
                    $(this).unbind().on('click',function(e){
                        e.preventDefault();
                        return false;
                    })
        });
        
     }
     setInterval(check_protection,3000);
     check_protection();
 