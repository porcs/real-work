
$(document).ready(function() { 
    
    $('#token').on('change',function(){
        var old = $(this).attr('source');
        var token = $(this).val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        var obj = $(this);
        
        if($.trim(token)){
            $.ajax({ 
                    type: "post",
                    url: base_url + "/manager/ajax_update_token",
                    dataType:'json',
                    data:{token:token},
                    success:function(res){
                       if(res.result!='pass'){
                           obj.val(old);
                       }else{
                           obj.attr('source',token);
                       }
                    }
            });
        }
    });
});