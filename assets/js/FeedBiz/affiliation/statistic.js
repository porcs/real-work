$(function(){
    var data = [ 
        $('#graph').val()['',0]
    ];
    
    $.plot("#graph-container",[data],{
        series: {
            bars: {
                show: true,
                barWidth: 0.4,
                align: "center"
            }
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        }
    });
    
    $('#graph-container>canvas').width('100%');
});