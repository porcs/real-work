$(function () {
    var tmp = $.fn.popover.Constructor.prototype.show;
    $.fn.popover.Constructor.prototype.show = function() {
        tmp.call(this); if (this.options.callback) {
            this.options.callback(this);
        }
    }
    $url='google.com';

    var $count_to = $('input[name=count_to]').val();
    var $degree = $('input[name=degree]').val();
    var $degree_all = $('input[name=degree_all]').val();
    var $f_step = $('input[name=f_step]').val();
    var $process_running = $('input[name=process_running]').val();
    var $process_not_found = $('input[name=process_not_found]').val();
    var $service_comming_soon = $('input[name=service_comming_soon]').val();
    var $fb_wizard_title = $('input[name=fb_wizard_title]').val();

    var $spacefree = $('input[name=spacefree]').val();
    var $active = $('input[name=active]').val();
    var $inactive = $('input[name=inactive]').val();

    $use_percent = $('input[name=use_percent]').val();
    $inuse_precent = $('input[name=inuse_precent]').val();
    $freeuse_precent = $('input[name=freeuse_precent]').val();

    if ($f_step) {

        var cmd_obj = $('#command').val();
        if ($('#' + cmd_obj).length > 0) {
            var obj = $('#' + cmd_obj);
            obj.click();
        }

        var step = $("#f_step").val();
        if (step != 'fin') {
            setTimeout(function () {
                /* == Colorbox Resize == */
                jQuery.colorbox.settings.innerWidth = '80%';
                jQuery.colorbox.settings.innerHeight = '90%';

                jQuery(window).resize(function () {
                    // Resize Colorbox when resizing window or changing mobile device orientation
                    resizeColorBox();
                    window.addEventListener("orientationchange", resizeColorBox, false);
                    console.log(cmd_obj, obj, step);
                });

                var resizeTimer;
                var WIDTH,HEIGHT,innerWidth,innerHeight;
                var checksize = function(){
                    WIDTH = $(window).width(); 
                    HEIGHT = $(window).height();
                    innerWidth = '70%';
                    innerHeight = '80%';

                    if(WIDTH >= 1280 && HEIGHT >= 768){
                        innerWidth = '70%';
                        innerHeight = '80%';
                    }else if(WIDTH >= 800 && HEIGHT >= 600){
                        innerWidth = '95%';
                        innerHeight = '95%';
                    }else{
                        innerWidth = '100%';
                        innerHeight = '100%';
                    }
                };
                    
                checksize();
                    
                function resizeColorBox() {
                    if (resizeTimer) {
                        clearTimeout(resizeTimer);
                    }
                    resizeTimer = setTimeout(function () {
                        if (jQuery('#cboxOverlay').is(':visible')) {
                            //jQuery.colorbox.resize({innerWidth: '80%', innerHeight: '90%'});
                            checksize();
                            jQuery.colorbox.resize({innerWidth: innerWidth, innerHeight: innerHeight});
                        }
                    }, 300);
                }
                
                
                /* == Colorbox End Resize == */
                $.colorbox({
                    iframe: true,
                    href: base_url + 'dashboard/iframe_popup_conf/' + step,
                    innerWidth: innerWidth,
                    innerHeight: innerHeight,
                    onComplete: function () {
                        $('#cboxTitle').text($fb_wizard_title);
                    },
                    onLoad: function () {
                        $('body').css('overflow-y', 'hidden');
                    },
                    onClosed: function () {
                        $('body').css('overflow-y', 'auto');
                    }
                });
            }, 500);
        }
    }

    $('.minichart').each(function () {
        $(this).highcharts({
            title: {
                text: '',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            tooltip: {
                enabled: false
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0,
                enabled: false
            },
            credits: {
                enabled: false
            },
            exporting: {
                buttons: {
                    contextButton: {
                        enabled: false
                    }
                }
            },
            xAxis: {
                lineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                labels: {
                    enabled: false
                },
                minorTickLength: 0,
                tickLength: 0
            },
            yAxis: {
                lineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                labels: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                minorTickLength: 0,
                tickLength: 0
            },
            series: [{
                    name: 'Goodies (FOXCHIP)',
                    data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                }],
        });
    });

    /* Ini Donut 2 Chart */
    if($inuse_precent==0){
        var datax = [
            {"Free space": 0.5000, "Inactive": 0.000001, "Active": 0.5000},
            {"Free space": $freeuse_precent, "Inactive": 0.000001, "Active": $use_percent}
        ];
    }else{
        var datax = [
            {"Free space": 0.5000, "Inactive": $inuse_precent, "Active": 0.5000},
            {"Free space": $freeuse_precent, "Inactive": $inuse_precent, "Active": $use_percent}
        ];
    }
    var order_list = $('input[name=orders_items]').val();
    var data_order = jQuery.parseJSON(order_list);
  
    var svg = d3.select("#chartdonut")
            .append("svg")
            .append("g");
    var svg_order = d3.select("#chartdonut_order")
            .append("svg")
            .append("g");

    var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight || e.clientHeight || g.clientHeight;

    var width = $('#chartdonut').width(),
            width_order = $('#chartdonut_order').width(),
            height = 200,
            radius = Math.min(width, height) / 2;

    svg.append("g")
            .attr("class", "slices");
    svg.append("g")
            .attr("class", "labels");
    svg.append("g")
            .attr("class", "percent");
    svg.append("g")
            .attr("class", "lines");
    svg_order.append("g")
            .attr("class", "slices");
    svg_order.append("g")
            .attr("class", "labels");
    svg_order.append("g")
            .attr("class", "percent");
    svg_order.append("g")
            .attr("class", "lines");

    var pie = d3.layout.pie()
            .sort(null)
            .value(function (d) {
                return d.value;
            });

    var arc = d3.svg.arc()
            .outerRadius(radius * 0.7)
            .innerRadius(radius * 0.4);

    var outerArc = d3.svg.arc()
            .innerRadius(radius * 0.7)
            .outerRadius(radius * 0.9);

    function updateWindow() {
        x = w.innerWidth || e.clientWidth || g.clientWidth;
        y = w.innerHeight || e.clientHeight || g.clientHeight;

        svg.attr("transform", "translate(" + $('#chartdonut').width() / 2 + "," + height / 2 + ")");
        svg_order.attr("transform", "translate(" + $('#chartdonut_order').width() / 2 + "," + height / 2 + ")");
    }
    window.onresize = updateWindow;

    svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
    svg_order.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var key = function (d) {
        return d.data.label;
    };

    var color = d3.scale.category10()
            .domain(["Inactive", "Free space", "Active"])
            .range(["#ff4646", "#398bff", "#f99b01"]);


    function randomData(q, c) {
        var labels = c.domain();
        return labels.map(function (label) {
            /*console.log(datax);*/
            return {label: label, value: q[label]/*Math.random()*/}
        }).filter(function () {
            //return Math.random() > .5;
            return 1;
        }).sort(function (a, b) {
            return d3.ascending(a.label, b.label);
        });
    }

    function prepare_order_data(q, c) {
        var labels = c.domain();
        var sum = 0;

        $.each(q, function (index, value) {
            sum = sum * 1 + value * 1;
        });
        if (sum == 0)
            sum = 1;

        return labels.map(function (label) {
            /*console.log(datax);*/
            /*console.log(q[label]/sum,q[label],sum);*/
            return {label: label + ' (' + q[label] + ')', value: (q[label] * 1) / (1 * sum) /*Math.random()*/}
        }).filter(function () {
            //return Math.random() > .5;
            return 1;
        }).sort(function (a, b) {
            return d3.ascending(a.label, b.label);
        });
    }
    
    if(datax[1]['Inactive']!=0 && datax[1]['Active']!=0){
        change(svg, randomData(datax[1], color));
    }
    if(data_order[0]!=null){
        window.setTimeout(function(){change(svg_order, randomData(data_order[0], color_order));}, 3000);
    }
    //change(svg_order,randomData(data_order[0],color_order));
    var sum = 0;
    var range = "day";
    if (data_order[range] != null && data_order[range]['data'])
        $.each(data_order[range]['data'], function (index, value) {
            sum = sum * 1 + value * 1;
        });
    if (sum == 0) {
        $('.day_order').removeClass('active').remove();
        var range = "week";
        if (data_order[range] != null && data_order[range]['data'])
            $.each(data_order[range]['data'], function (index, value) {
                sum = sum * 1 + value * 1;
            });
        if (sum == 0) {
            $('.day7_order').removeClass('active').remove();
            var range = "month";
            if (data_order[range] != null && data_order[range]['data'])
                $.each(data_order[range]['data'], function (index, value) {
                    sum = sum * 1 + value * 1;
                });
            $('.day30_order').addClass('active');
        } else {
            $('.day7_order').addClass('active');
        }
    } else {
        $('.day_order').addClass('active');
    }


    if (sum != 0) {

        var color_order = d3.scale.category10()
                .domain($.map(data_order['label'], function (el) {
                    return el;
                }));

        change(svg_order, prepare_order_data(data_order[range]['data'], color_order));
        d3.select(".day30_order")
                .on("click", function () {
                    var range = "month";

                    change(svg_order, prepare_order_data(data_order[range]['data'], color_order));
                });

        d3.select(".day7_order")
                .on("click", function () {
                    var range = "week";
                    sum = 0;
                    $.each(data_order[range]['data'], function (index, value) {
                        sum = sum * 1 + value * 1;
                    });
                    if (sum == 0) {
                        $(this).unbind().hide('slow');
                        return;
                    }
                    change(svg_order, prepare_order_data(data_order[range]['data'], color_order));
                });

        d3.select(".day_order")
                .on("click", function () {
                    var range = "day";
                    sum = 0;
                    $.each(data_order[range]['data'], function (index, value) {
                        sum = sum * 1 + value * 1;
                    });
                    if (sum == 0) {
                        $(this).unbind().hide('slow');
                        return;
                    }
                    change(svg_order, prepare_order_data(data_order[range]['data'], color_order));
                });
    } else {
        $('#chartdonut_order').closest('.chart').hide('fast');
    }

    function mergeWithFirstEqualZero(first, second) {
        var secondSet = d3.set();
        second.forEach(function (d) {
            secondSet.add(d.label);
        });

        var onlyFirst = first
                .filter(function (d) {
                    return !secondSet.has(d.label)
                })
                .map(function (d) {
                    return {label: d.label, value: 0};
                });
        return d3.merge([second, onlyFirst])
                .sort(function (a, b) {
                    return d3.ascending(a.label, b.label);
                });
    }

    function change(svgobj, data) {
        var duration = 2500;
        var data0 = svgobj.select(".slices").selectAll("path.slice")
                .data().map(function (d) { /*console.log(d.data);*/
            return d.data
        });
        if (data0.length == 0)
            data0 = data;
        var was = mergeWithFirstEqualZero(data, data0);
        var is = mergeWithFirstEqualZero(data0, data);

        /* ------- SLICE ARCS -------*/

        var slice = svgobj.select(".slices").selectAll("path.slice")
                .data(pie(was), key);

        slice.enter()
                .insert("path")
                .attr("class", "slice")
                .style("fill", function (d) {
                    /*console.log(color(d.data.label));*/return color(d.data.label);
                })
                .each(function (d) {
                    this._current = d;
                });

        slice = svgobj.select(".slices").selectAll("path.slice")
                .data(pie(is), key);

        slice
                .transition().duration(duration)
                .attrTween("d", function (d) {
                    var interpolate = d3.interpolate(this._current, d);
                    var _this = this;
                    return function (t) {
                        _this._current = interpolate(t);
                        return arc(_this._current);
                    };
                });

        slice = svgobj.select(".slices").selectAll("path.slice")
                .data(pie(data), key);

        slice
                .exit().transition().delay(duration).duration(0)
                .remove();

        /* ------- TEXT LABELS -------*/
        var text = svgobj.select(".labels").selectAll("text")
                .data(pie(was), key);

        text.enter()
                .append("text")
                .attr("dy", "1em")
                .style("opacity", 0)
                .style("font-size", "12px")
                .text(function (d) {
                    var out = d.data.label;
                    switch (d.data.label) {
                        case 'Active':
                            out = $active;
                            break;
                        case 'Inactive':
                            out = $inactive;
                            break;
                        case 'Space free':
                            out = $spacefree;
                            break;
                    }
                    return out;
                })
                .each(function (d) {
                    this._current = d;
                });

        function midAngle(d) {
            return d.startAngle + (d.endAngle - d.startAngle) / 2;
        }

        text = svgobj.select(".labels").selectAll("text")
                .data(pie(is), key);

        text.transition().duration(duration)
                .style("opacity", function (d) {
                    return d.data.value == 0 ? 0 : 1;
                })
                .attrTween("transform", function (d) {
                    var interpolate = d3.interpolate(this._current, d);
                    var _this = this;
                    return function (t) {
                        var d2 = interpolate(t);
                        _this._current = d2;
                        var pos = outerArc.centroid(d2);
                        pos[0] = radius * (midAngle(d2) < Math.PI ? 1.2 : -1.2);
                        return "translate(" + pos + ")";
                    };
                })
                .styleTween("text-anchor", function (d) {
                    var interpolate = d3.interpolate(this._current, d);
                    return function (t) {
                        var d2 = interpolate(t);
                        return midAngle(d2) < Math.PI ? "start" : "end";
                    };
                });

        text = svgobj.select(".labels").selectAll("text")
                .data(pie(data), key);

        text
                .exit().transition().delay(duration)
                .remove();

        /* ------- TEXT LABELS PERCENT % -------*/
        var textpercent = svgobj.select(".percent").selectAll("text")
                .data(pie(was), key);

        textpercent.enter()
                .append("text")
                .attr("dy", "-0.2em")
                .style("opacity", 0)
                .style("font-size", "18px")
                /*.text(function(d) {
                 console.log(d.data);
                 return (d.data.value*100).toFixed(2)+'%';
                 })*/
                .text(0)
                .each(function (d) {
                    this._current = d;
                });

        /*function midAngle(d){
         return d.startAngle + (d.endAngle - d.startAngle)/2;
         }*/

        textpercent = svgobj.select(".percent").selectAll("text")
                .data(pie(is), key);


        textpercent.transition().duration(duration)
                .style("opacity", function (d) {
                    return d.data.value == 0 ? 0 : 1;
                })
                .attrTween("transform", function (d) {
                    var interpolate = d3.interpolate(this._current, d);
                    var _this = this;
                    return function (t) {
                        var d2 = interpolate(t);
                        _this._current = d2;
                        var pos = outerArc.centroid(d2);
                        pos[0] = radius * (midAngle(d2) < Math.PI ? 1.2 : -1.2);

                        return "translate(" + pos + ")";
                    };
                })
                .styleTween("text-anchor", function (d) {
                    var interpolate = d3.interpolate(this._current, d);
                    return function (t) {
                        var d2 = interpolate(t);
                        return midAngle(d2) < Math.PI ? "start" : "end";
                    };
                })
                /*.text(function(d) {
                 //console.log(d.data);
                 return (d.data.value*100).toFixed(2)+'%';
                 })*/
                .tween("text", function (d) {
                    var a = parseFloat(d.data.value) * 100;
                    var b = parseFloat(this.textContent);
                    var i = d3.interpolate(b, a),
                            prec = (a + "").split("."),
                            round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
                    //console.log(b);
                    return function (t) {
                        this.textContent = (Math.round(i(t) * round) / round).toFixed(2) + "%";
                    };
                })
                //.tween("text", function(d) { tweenText( d.data.value );})
                /*.text(function(d) {
                 console.log(d.data);
                 return (d.data.value*100).toFixed(2)+'%';
                 })*/;

        textpercent = svgobj.select(".percent").selectAll("text")
                .data(pie(data), key);

        textpercent
                .exit().transition().delay(duration)
                .remove();

        /* ------- SLICE TO TEXT POLYLINES -------*/

        var polyline = svgobj.select(".lines").selectAll("polyline")
                .data(pie(was), key);

        polyline.enter()
                .append("polyline")
                .style("opacity", 0)
                .each(function (d) {
                    this._current = d;
                });

        polyline = svgobj.select(".lines").selectAll("polyline")
                .data(pie(is), key);

        polyline.transition().duration(duration)
                .style("opacity", function (d) {
                    return d.data.value == 0 ? 0 : 0.7;
                })
                .attrTween("points", function (d) {
                    this._current = this._current;
                    var interpolate = d3.interpolate(this._current, d);
                    var _this = this;
                    return function (t) {
                        var d2 = interpolate(t);
                        _this._current = d2;
                        var pos = outerArc.centroid(d2);
                        pos[0] = radius * 1.5 * (midAngle(d2) < Math.PI ? 1 : -1);
                        return [arc.centroid(d2), outerArc.centroid(d2), pos];
                    };
                });

        polyline = svgobj.select(".lines").selectAll("polyline")
                .data(pie(data), key);

        polyline
                .exit().transition().delay(duration)
                .remove();
    }
    ;


    /* Init long Chart*/
    var randomScalingFactor = function () {
        return Math.round(Math.random() * 1500)
    };
    var line_data = $('input[name=orders_graph]').val();
    line_data = jQuery.parseJSON(line_data);

    if (line_data[range] != null && line_data[range]['data']) {
        var range = 'day';
        var $data = [];
        
        if(typeof line_data[range]['data'] != 'undefined'){
            var $mk = 'Ebay';
            if (typeof line_data[range]['data'][$mk] != 'undefined') {
                $data.push({
                    label: "Ebay",
                    fillColor: "rgba(84,155,255,0.2)",
                    strokeColor: "rgba(84,155,255,0.7)",
                    pointColor: "rgba(84,155,255,0.7)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(84,155,255,0.7)",
                    data: line_data[range]['data'][$mk]//[randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                });
            }
            var $mk = 'Amazon';
            if (typeof line_data[range]['data'][$mk] != 'undefined') {
                $data.push({
                    label: "Amazon",
                    fillColor: "rgba(255,70,70,0.2)",
                    strokeColor: "rgba(255,70,70,0.7)",
                    pointColor: "rgba(255,70,70,0.7)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(255,70,70,0.7)",
                    data: line_data[range]['data'][$mk]//[randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                });
            }
            var lineChartData = {
                labels: line_data[range]['label'], //["1 Jan","2 Jan","3 Jan","4 Jan","5 Jan","6 Jan","7 Jan"],
                datasets: $data

            };
        }
        var range = 'week';
        var $data = [];
        
        if(typeof line_data[range]['data'] != 'undefined'){
            var $mk = 'Ebay';
            if (typeof line_data[range]['data'][$mk] != 'undefined') {
                $data.push({
                    label: "Ebay",
                    fillColor: "rgba(84,155,255,0.2)",
                    strokeColor: "rgba(84,155,255,0.7)",
                    pointColor: "rgba(84,155,255,0.7)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(84,155,255,0.7)",
                    data: line_data[range]['data'][$mk]//[randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                });
            }
            var $mk = 'Amazon';
            if (typeof line_data[range]['data'][$mk] != 'undefined') {
                $data.push({
                    label: "Amazon",
                    fillColor: "rgba(255,70,70,0.2)",
                    strokeColor: "rgba(255,70,70,0.7)",
                    pointColor: "rgba(255,70,70,0.7)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(255,70,70,0.7)",
                    data: line_data[range]['data'][$mk]//[randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                });
            }
            var lineChartData7 = {
                labels: line_data[range]['label'], //["1 Jan","2 Jan","3 Jan","4 Jan","5 Jan","6 Jan","7 Jan"],
                datasets: $data
            };
        }
        var range = 'month';
        var $data = [];
        
        if(typeof line_data[range]['data'] != 'undefined'){
            var $mk = 'Ebay';
            if (typeof line_data[range]['data'][$mk] != 'undefined') {
                $data.push({
                    label: "Ebay",
                    fillColor: "rgba(84,155,255,0.2)",
                    strokeColor: "rgba(84,155,255,0.7)",
                    pointColor: "rgba(84,155,255,0.7)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(84,155,255,0.7)",
                    data: line_data[range]['data'][$mk]//[randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                });
            }
            var $mk = 'Amazon';
            if (typeof line_data[range]['data'][$mk] != 'undefined') {
                $data.push({
                    label: "Amazon",
                    fillColor: "rgba(255,70,70,0.2)",
                    strokeColor: "rgba(255,70,70,0.7)",
                    pointColor: "rgba(255,70,70,0.7)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(255,70,70,0.7)",
                    data: line_data[range]['data'][$mk]//[randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                });
            }
            var lineChartData30 = {
                labels: line_data[range]['label'], //["1 Jan","2 Jan","3 Jan","4 Jan","5 Jan","6 Jan","7 Jan"],
                datasets: $data

            };
        }
    }
    var ctx = document.getElementById("chart_sale").getContext("2d");

    if (line_data[range] != null &&line_data[range]['label']) {
        window.onload = function () {
            window.myLine = new Chart(ctx).Line(lineChartData, {
                responsive: true
            });
        };
    } else {
        $('.chart.lineChart').closest('.row').slideUp('fast');
    }

    function loaddatasale(a) {
        if (window.myLine)
            window.myLine.destroy();
        window.myLine = new Chart(ctx).Line(a, {
            responsive: true
        });
    }
    ;

    $('#sale_day1').click(function () {
        loaddatasale(lineChartData);
    });
    $('#sale_day7').click(function () {
        loaddatasale(lineChartData7);
    });
    $('#sale_day30').click(function () {
        loaddatasale(lineChartData30);
    });

    $('.tabs').on('click', function () {
        $(this).closest('.tabs').find('li').removeClass('active');
        $(this).addClass('active');
    });

//        $('.btn-group button').on('click', function(){
//                $(this).parent().find('button').removeClass('active').addClass('btn-default').removeClass('btn-success');
//                $(this).addClass('active').removeClass('btn-default').addClass('btn-success');
//                //console.log($(this));
//        });

});