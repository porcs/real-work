 $('.marketplace').click(function () {
            
            if($(this).find(".fa-square-o").length > 0)
            {
                $(this).find(".fa-square-o").removeClass("fa-square-o").addClass("fa-check-square-o");
                $(this).parent().parent().parent().removeClass("collapsed");
                $(this).parent().parent().parent().find("input[rel=1]").prop('checked', true);
            }
            else
            {
                $(this).parent().parent().parent().find("input[type=checkbox]").prop('checked', false);
                $(this).find(".fa-check-square-o").removeClass("fa-check-square-o").addClass("fa-square-o");
                $(this).parent().parent().parent().addClass("collapsed");
            }
        });
        
        $('form').on('reset', function(e) {
            setTimeout(function() {
                $('.checkbox-custom > input[type=checkbox]').each(function() {
                    //var $this = $(this);
                    //console.log($this.is(:checked));
                    if ($(this).is(':checked') == false) {
                        //$(this).checkbox('check');
                        //}else{
                        $(this).prop('checked', false);
                        $(this).parent().parent().find('.pull-right').remove();
                        $(this).checkbox('uncheck');
                    }
                });

            });
        });