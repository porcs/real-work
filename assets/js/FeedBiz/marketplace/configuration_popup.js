$(document).ready(function () {
    
    $('a#check_all').click(function(){
        //console.log($(this).parents('.row').find('input'));
        var cl = $(this).parents('.widget-body').find('input[type=checkbox]');
        cl.prop('checked', true);
        cl.parent().parent().addClass('active');
        cl.checkbox('check');
    });
    
    $('a#uncheck_all').click(function(){
        //console.log($(this).parents('.row').find('input'));
        var cl = $(this).parents('.widget-body').find('input[type=checkbox]');
        cl.prop('checked', false);
        cl.parent().parent().removeClass('active');
        cl.checkbox('uncheck');
    });
    
    $('a#check_list').click(function(){
        //console.log($(this).parents('.row').find('input'));
        var cl = $(this).parents('.row').find('input[type=checkbox]');
        cl.prop('checked', true);
        cl.parent().parent().addClass('active');
        cl.checkbox('check');
    });
    
    $('a#uncheck_list').click(function(){
        //console.log($(this).parents('.row').find('input'));
        var cl = $(this).parents('.row').find('input[type=checkbox]');
        cl.prop('checked', false);
        cl.parent().parent().removeClass('active');
        cl.checkbox('uncheck');
    });
    
    $('.marketplace').click(function () {

        if ($(this).find(".icon-check-empty").length > 0)
        {
            $(this).find(".icon-check-empty").removeClass("icon-check-empty").addClass("icon-check");
            $(this).parent().parent().parent().removeClass("collapsed");
            $(this).parent().parent().parent().find("input[rel=1]").prop('checked', true);
        }
        else
        {
            $(this).parent().parent().parent().find("input[type=checkbox]").prop('checked', false);
            $(this).find(".icon-check").removeClass("icon-check").addClass("icon-check-empty");
            $(this).parent().parent().parent().addClass("collapsed");
        }
    });

    var act_time = '500';
    var current_step = 0;
    var sub_step_def = 0;
    var cur_sub_step = 0;
    $('.market_place_sel_btn').click(function () {
        var obj = $(this);
        if (obj.hasClass('selected')) {
            obj.removeClass('selected');
            obj.removeClass('btn-success');
            obj.addClass('btn-light');
        } else {
            obj.addClass('selected');
            obj.addClass('btn-success');
            obj.removeClass('btn-light');
        }
        console.log($('.market_place_sel_btn.selected').length);
        if ($('.market_place_sel_btn.selected').length > 0) {
            $("#step1 .form-actions").fadeIn(act_time);
        } else {
            $("#step1 .form-actions").fadeOut(act_time);
        }
        current_step = 1;
        sub_step_def = $('.market_place_sel_btn.selected').length;
    })

    $("#step1 .form-actions .btn").click(function () {
        if (!sub_step_def)
            return;
        $("#step1").slideUp(act_time);
        $("#step2").slideDown(act_time);
        current_step = 2;
        cur_sub_step = 1;
        get_next_market_list();
    })

    $("form .btn_back").click(function () {
        if (cur_sub_step == 1) {
            $("#step1").slideDown(act_time);
            $("#step2").slideUp(act_time);
        } else {
            cur_sub_step--;
            get_next_market_list();
        }
    });
    $("#step2 .btn_next").click(function () {
        //next market place
        cur_sub_step++;
        get_next_market_list();
    });
    $("form  .btn_submit").click(function () {
        if ($("form input:checked").length == 0) {
            alert($('#select_atl_once').val());
            return;
        } else {
            $('#frmPackages').submit();
        }
    });

    function get_next_market_list() {
        var active_i = 0;
        $('.market_conf_list').each(function (i, v) {
            var rel = $(this).attr('rel');

            if ($('.market_place_sel_btn#' + rel).hasClass('selected')) {
                active_i++;
                if (active_i == cur_sub_step) {
                    $(this).slideDown(act_time);
                } else {
                    $(this).slideUp(act_time);
                }
            } else {
                $(this).slideUp(act_time);
            }
        })
        if (cur_sub_step == sub_step_def) {
            $('.btn_next').hide();
            $('.btn_submit').show();
        } else {
            $('.btn_next').show();
            $('.btn_submit').hide();
        }
    }
});

$('select').chosen();
$("#sub_marketplace").change(function () {
    var sub_marketplace = $('#sub_marketplace').val();
    $("#tabGeneral > [id^='sub_marketplace_']").hide();
    $("#sub_marketplace_"+sub_marketplace).show();
    
    $("#tabGeneral > [id^='sub_marketplace_']").find("[disabled]").removeAttr("disabled");
    $("#tabGeneral > [id^='sub_marketplace_']").find(".disabled").removeClass("disabled");
    
    $("#tab_selected").remove();
});

//
//$("#sub_marketplace").change(function () {
//   var id_marketplace = $('#id_marketplace').val();
//   var sub_marketplace = $('#sub_marketplace').val();
//
//    if(sub_marketplace == ""){
//        $('#tabGeneral > div').remove();
//        return;
//    }
//
//   $.ajax({
//       type: 'POST',
//       url: base_url + '/marketplace/get_general_country/'+id_marketplace+'/'+sub_marketplace,
//       /*data: ,*/
//       /*dataType: 'json',*/
//       success: function (data) {
//          // console.log(data); return;
//        if(data){
//            $('#tabGeneral > div').remove(); 
//            $('#tabGeneral').html(data);
//            $('#tabGeneral').find('.allCheckBo').checkBo({
//                checkAllButton : '.checkAll',
//                checkAllTarget : '.checkboSettings'
//           });
//        }
//       }
//   });
//});
//
//$(".tab_general").click(function(){
//
//   var marketplace = $('#chkBoxMarketplace').val();
//   var donmain = $(this).html();
//   if(donmain == ""){
//        $('#tabGeneral > div').remove();  
//        return;
//    }
//    
//    $.ajax({
//       type: 'POST',
//       url: base_url + '/marketplace/get_general_country/'+marketplace+'/'+donmain,
//       /*data: ,*/
//       /*dataType: 'json',*/
//       success: function (data) {
//           
//        if(data){
//            $('#tabGeneral > div').remove(); 
//            $('#tabGeneral').html(data);
//            $('#tabGeneral').find('.allCheckBo').checkBo({
//                checkAllButton : '.checkAll', 
//                checkAllTarget : '.checkboSettings'
//           });
//        }                                           
//       }
//   });
//   
//});