$(function() {
    
    var base_url = location.origin;
    
    if( $('#feed_product').length === 1)
    {
        $.ajax({
            url: base_url + '/validation/log_last_import/Product/' + encodeURIComponent($('#shopname').val()),
            type: 'GET',
            contentType: "application/json",
            cache: false,
            success: function(datas) 
            {
                var placeholder = $('#product').css('width','90%').css('min-height','150px');
                var product = jQuery.parseJSON(datas);

                var total = product.no_total;
                var error = (parseInt(product.no_error)/parseInt(total))*100;
                var warning = (parseInt(product.no_warning)/parseInt(total))*100;
                var success = (parseInt(product.no_success)/parseInt(total))*100;
                $('#product_no_error').html(product.no_error);
                $('#product_no_success').html(product.no_success);
                $('#product_no_warning').html(product.no_warning);
                $('#product_no_total').html(product.no_total);


                var data = [
                        { label: "Success",  data: success.toFixed(2), color: "#68BC31"},
                        { label: "Warning",  data: warning.toFixed(2), color: "#FEE074"},
                        { label: "Error",  data: error.toFixed(2), color: "#DA5430"}
                  ];

                drawPieChart(placeholder, data);
                placeholder.data('chart', data);
                placeholder.data('draw', drawPieChart);
                var $tooltip = $("<div class='tooltip top in hide'><div class='tooltip-inner'></div></div>").appendTo('body');
                var previousPoint = null;

                placeholder.on('plothover', function (event, pos, item) {
                    if(item) {
                        if (previousPoint !== item.seriesIndex) {
                            previousPoint = item.seriesIndex;
                            var tip = item.series['label'] + " : " + item.series['percent']+'%';
                            $tooltip.show().children(0).text(tip);
                        }
                        $tooltip.css('top',pos.pageY + 10).css('left', pos.pageX + 10);
                    } else {
                        $tooltip.hide();
                        previousPoint = null;
                    }
               });

            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Return Error : ' + errorThrown);
            }

        });
    }
    
    if( $('#feed_offer').length === 1)
    {
        $.ajax({
            url: base_url + '/validation/log_last_import/Offer/' + encodeURIComponent($('#shopname').val()),
            type: 'GET',
            contentType: "application/json",
            cache: false,
            success: function(datas) 
            {
                var placeholder = $('#offer').css('width','90%').css('min-height','150px');
                var offer = jQuery.parseJSON(datas);

                var total = offer.no_total;
                var error = (parseInt(offer.no_error)/parseInt(total))*100;
                var warning = (parseInt(offer.no_warning)/parseInt(total))*100;
                var success = (parseInt(offer.no_success)/parseInt(total))*100;
                $('#offer_no_error').html(offer.no_error);
                $('#offer_no_success').html(offer.no_success);
                $('#offer_no_warning').html(offer.no_warning);
                $('#offer_no_total').html(offer.no_total);

                var data = [
                        { label: "Success",  data: success.toFixed(2), color: "#68BC31"},
                        { label: "Warning",  data: warning.toFixed(2), color: "#FEE074"},
                        { label: "Error",  data: error.toFixed(2), color: "#DA5430"}
                  ];

                drawPieChart(placeholder, data);
                placeholder.data('chart', data);
                placeholder.data('draw', drawPieChart);
                var $tooltip = $("<div class='tooltip top in hide'><div class='tooltip-inner'></div></div>").appendTo('body');
                var previousPoint = null;

                placeholder.on('plothover', function (event, pos, item) {
                    if(item) {
                        if (previousPoint !== item.seriesIndex) {
                            previousPoint = item.seriesIndex;
                            var tip = item.series['label'] + " : " + item.series['percent']+'%';
                            $tooltip.show().children(0).text(tip);
                        }
                        $tooltip.css('top',pos.pageY + 10).css('left', pos.pageX + 10);
                    } else {
                        $tooltip.hide();
                        previousPoint = null;
                    }
               });

            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Return Error : ' + errorThrown);
            }

        });
    }
    
    function drawPieChart(placeholder, data, position) {
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    tilt:0.8,
                    highlight: {
                        opacity: 0.25
                    },
                    stroke: {
                        color: '#fff',
                        width: 2
                    },
                    startAngle: 2
                }
            },
            legend: {
                show: true,
                position: position || "ne", 
                labelBoxBorderColor: null,
                margin:[-30,15]
            }
            ,
            grid: {
                hoverable: true,
                clickable: true
            }
       })
   }
   
});