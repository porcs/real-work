$(document).ready(function() {
    var filter_ignore_product = true;
    var filter_ignore_offer = true;
    var tb_prod = null;
    var tb_offer = null;
    $('#chk_ignore_product').change(function(){
       filter_ignore_product = $(this).is(':checked'); 
       tb_prod.api().ajax.reload();
    });
    $('#chk_ignore_offer').change(function(){
       filter_ignore_offer = $(this).is(':checked'); 
       tb_offer.api().ajax.reload();
    });
    tb_prod = $('#message-products').dataTable({  
       
        "oSearch": {"sSearch": $('#default_search').val()},
   
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "asStripeClasses": [''],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "my_feeds/messages_page/" + $('#type_products').val(),
            "type": "POST",
            "data": {ignore_case:function(){
                    return filter_ignore_product;
                }
            }
        },
        "columns": [  
            //{ "data": "batch_id", "bSortable": true, "width": "10%", "className": 'm-l10'  },
            { "data": "id_product", "bSortable": true, "width": "5%", "className": 'p-l10'  },
            { "data": null, "bSortable": false, "width": "30%", "className": 'p-l10'  ,
                render: function (data) {
                    if(data.name) {
                        return data.name;
                    } else {
                        return 'Product ID ' + data.id_product;
                    }
                }  
            },  
            { "data": "shop", "bSortable": false, "width": "15%", "className": 'p-l10'  },
            { "data": null, "bSortable": false, "width": "10%", "className": 'p-l10' ,

                render: function (data) {
                    if(data.severity == 1 ) {
                        return '<span class="label label-warning">'+$('#message_Warning').val()+'</span>';
                    } else {
                        return '<span class="label label-danger">'+$('#message_Error').val()+'</span>';
                    }
                }  
            },           
            { "data": "message", "bSortable": true, "width": "25%", "className": 'p-l10' },
            { "data": "date_add", "bSortable": false, "width": "15%", "className": 'text-left m-r10' },           
        ],       
        "bFilter" : true,
        "paging": true,
        "pagingType": "full_numbers" ,  
        "order": [[0, 'asc']],           
    } ); 

    tb_offer = $('#message-offers').dataTable({       
        "oSearch": {"sSearch": $('#default_search').val()},
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "asStripeClasses": [''],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "my_feeds/messages_page/" + $('#type_offers').val(),
            "type": "POST",
            "data": {ignore_case:function(){
                    console.log(filter_ignore_offer);
                    return filter_ignore_offer;
                }}
        },
        "columns": [  
            //{ "data": "batch_id", "bSortable": true, "width": "10%", "className": 'p-l10'  },
            { "data": "id_product", "bSortable": true, "width": "5%", "className": 'p-l10'  },
            { "data": null, "bSortable": false, "width": "30%", "className": 'p-l10'  ,
                render: function (data) {
                    if(data.name) {
                        return data.name;
                    } else {
                        return 'Product ID ' + data.id_product;
                    }
                }  
            },   
            { "data": "shop", "bSortable": false, "width": "15%", "className": 'p-l10'  },
            { "data": null, "bSortable": false, "width": "10%", "className": 'p-l10' ,

                render: function (data) {
                    if(data.severity == 1 ) {
                        return '<span class="label label-warning">'+$('#message_Warning').val()+'</span>';
                    } else {
                        return '<span class="label label-danger">'+$('#message_Error').val()+'</span>';
                    }
                }  
            },           
            { "data": "message", "bSortable": true, "width": "25%", "className": 'p-l10' },
            { "data": "date_add", "bSortable": false, "width": "10%", "className": 'text-left m-r10' },           
        ],       
        "bFilter" : true,
        "paging": true,
        "pagingType": "full_numbers" ,  
        "order": [[0, 'asc']],
        "fnDrawCallback": function(settings, data){
        	  $('.dataTables_filter').show();
                  $('.dataTables_paginate').show();
                  $('.dataTables_info').show();
          }          
    } ); 
});