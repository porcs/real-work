$( document ).ready(function() {
    $('.responsive-table').DataTable({
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "asStripeClasses": [''],"processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "my_feeds/batches_page/",
                "type": "POST"
            },
            "columns": [  
                { "data": "batch_id", "bSortable": true,   },
                { "data": "source", "bSortable": true,   },
                { "data": "shop", "bSortable": true,   },
                { "data": "type", "bSortable": true,   },
                { "data": "no_total", "bSortable": true,   },
                { "data": "no_success", "bSortable": true,   },
                { "data": "no_warning", "bSortable": true,   },
                { "data": "no_error", "bSortable": true,   },
                { "data": "datetime", "bSortable": true,   },
                { "data": "download", "bSortable": true,   "defaultContent": '' ,"data": "dwn" },
              
            ],       
            "bFilter" : true,
            "paging": true,
            "pagingType": "full_numbers" , 
            "order": [[ 8, "desc" ]]
        
            ,"fnRowCallback": function( nRow, aData, iDisplayIndex ) { 
                $('td', nRow).each(function(index){
                    if(index==0){
                       $(this).css('cursor','pointer');
                       $(this).on( "mouseenter", function() {
                            $(this).css('font-weight','bold');
                          })
                          .on( "mouseleave", function() {
                            $(this).css('font-weight','400');
                          });
                           
                       $(this).unbind('click').on('click',function(){ 
                           window.location = "/my_feeds/messages?batch_id="+ aData.batch_id;;
                       });
                    }
                    $(this).attr('rel',$('.responsive-table .table-head th:eq('+index+')') .attr('rel'));
                });
                
                
                if(aData.download && aData.download==1){
                    $('td:eq(9)', nRow).html('<a class="btn btn-primary" href="/my_feeds/log_download/products/'+aData.batch_id+'"><i class="fa fa-download"></i></a>');
                }else if(aData.download && aData.download==2){
                    $('td:eq(9)', nRow).html('<a class="btn btn-primary" href="/my_feeds/log_download/offers/'+aData.batch_id+'"><i class="fa fa-download"></i></a>');
                }else{
                    $('td:eq(9)', nRow).html('');
                }
            }
        }    
            );

        $('.dataTables_wrapper').find('select').addClass('search-select');

        $(".search-select").chosen({
                disable_search_threshold: 1,
                no_results_text: "Nothing found!"
        }).trigger("chosen:updated");	

        $('.chosen-single').find('div').addClass('button-select');
        $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');

        $(".dataTables_wrapper .chosen-search").remove();


var checkboTableX = (function () {
    $('.dropCheckbo input[type="checkbox"]').unbind('change'); 
    $('.dropCheckbo .cb-checkbox').click(function(){
        var $_this = $(this); 
        
        if ($_this.hasClass('checked')) {
             $_this.find('input[type="checkbox"]').prop('checked', false).change();
             $_this.removeClass('checked');
        }else{
            $_this.find('input[type="checkbox"]').prop('checked', true).change();
            $_this.addClass('checked');
        }
    })
    $('.dropCheckbo input[type="checkbox"]').change(function () {
        var $this = $(this),
                $_tableCheckbo = $this.closest('.dropCheckbo'),
                $_tableRow = $this.closest('.row').find('.row');

        $this.each(function () {
            var $_this = $(this),
                    $tableRow = $_this.closest('.row'),
                    $t_Head = $tableRow.find('thead'),
                    $t_Foot = $tableRow.find('tfoot');
                    var rel = $this.attr('name');
                    
            if ($_this.is(':checked')) {
                 $tableRow.find('td[rel="'+rel+'"],th[rel="'+rel+'"]').show();
                 $t_Head.find('td[rel="'+rel+'"],th[rel="'+rel+'"]').show();
                 $t_Foot.find('td[rel="'+rel+'"],th[rel="'+rel+'"]').show(); 
            } else {
                $tableRow.find('td[rel="'+rel+'"],th[rel="'+rel+'"]').hide();
                 $t_Head.find('td[rel="'+rel+'"],th[rel="'+rel+'"]').hide();
                 $t_Foot.find('td[rel="'+rel+'"],th[rel="'+rel+'"]').hide(); 
            }
            if ($this.closest('.dropCheckbo').find('.cb-checkbox.checked').length == 0) {
                $_tableRow.find('.tableEmpty').show();
                $_tableRow.find('.dataTables_filter').hide();
                $_tableRow.find('.dataTables_info').hide();
                $_tableRow.find('.dataTables_paginate').hide();
            } else {
                $_tableRow.find('.tableEmpty').hide();
                $_tableRow.find('.dataTables_filter').show();
                $_tableRow.find('.dataTables_info').show();
                $_tableRow.find('.dataTables_paginate').show();
            }
        });
    });
});
checkboTableX();
});