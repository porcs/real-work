$(document).ready(function() {
        function removeMapping(obj) {
            var id = parseInt($('#count').val()) - 1;
            $('#count').val(id);

            var select_value = $(obj).parent().find('select').val();
            $('#attribute_1').append($('<option>', {
                value: select_value,
                text: select_value
            }));

            $(obj).parents('.row').remove();
        }

        $('.removemapping').click(function() {
            removeMapping(this);
        });
        var id_group;
        $('.addnewmapping').click(function() {
            id_group = $(this).attr('rel');
            //console.log(id_group);
            var root = $(this).parent().parent().parent().find('select.search-select').chosen('destroy');
            //console.log(root);

            var cloned = $('div[id^="manufacturer-1"]').filter(':last').clone().appendTo('#new-mapping');
            var indexX = id_group;
            var newIndex = parseInt(indexX) + 1;

            $(this).attr('rel', newIndex);

            var class_name;

            root.chosen({
                disable_search_threshold: 1,
		no_results_text: "Nothing found!"
            });            
            cloned.find('select option').filter(function() {
                return !this.value || $.trim(this.value).length == 0 || $('select[data-placeholder="'+this.value+'"]').length > 0;
            }).remove();
            var def_txt = cloned.find('option').length > 0 ? $(cloned.find('option').get(0)).html() : '';
            cloned.find('select').attr('data-placeholder',def_txt);
            cloned.find('select, input').val('').attr('disabled', false).attr('readonly', false);
            cloned.find('select.search-select').chosen({
                disable_search_threshold: 1,
		no_results_text: "Nothing found!"
            }); 

            /* if( parseInt(newIndex%2) === 0)
                 class_name = 'row-even';
             else
                 class_name = 'row-odd';*/

            cloned.attr('id', 'attribute-' + newIndex);
            cloned.attr('rel', newIndex);
            cloned.attr('class', class_name);
            
            //cloned.find('select:first').val($('#manufacturer-1').find('select:first').val());
            //cloned.find('select.select').select2('val',$('#manufacturer-1').find('select option:eq('+(newIndex-1)+')').val());
            //cloned.find('select.select').select2('val',cloned.find('select option:eq(1)').val());
            //console.log(cloned.find('select option:eq(1)').val());
            //console.log($('#manufacturer-1').find('select:first').val());
            //console.log($('#manufacturer-1').find('select option:eq(1)').val());
            
            cloned.find('input:first').val($('#manufacturer-1').find('input:first').val());
             
            cloned.find('select').attr('name', 'manufacturer[]');
            cloned.find('input[rel=mapping]').attr('name', 'mapping[]');

            var sname = cloned.find('select').attr('id');
            var selected_select = cloned.find('select');

            // Remove used options
            $('select[id^="' + sname + '"] option:selected').each(function(index, option) {
                selected_select.find('option[value="' + option.value + '"]').remove();
            });
            cloned.find('select.search-select').chosen('val', cloned.find('select.search-select option:eq(1)').val());
            if (!cloned.find('option').length) {
                cloned.remove();
                return (false);
            } else {
                var id_count = parseInt($('#count').val()) + 1;
                $('#count').val(id_count);
            }

            cloned.find('.addnewmapping').remove();
            cloned.find('.removemapping').show().
            click(function() {
                removeMapping(this);
            });
            
            $('.chosen-single').find('div').addClass('button-select');
            $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
            cloned.find('select').val(def_txt);
            cloned.find('select').trigger('chosen:updated');

        });

    });