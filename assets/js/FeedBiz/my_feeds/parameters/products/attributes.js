$(document).ready(function() 
{ 
    function removeMapping(obj)
    {
        var id = parseInt($('#count').val()) - 1 ;
        $('#count').val(id);

        var select_value =$(obj).parent().find('select').val();
        $('#attribute_1').append($('<option>', { 
            value: select_value,
            text : select_value 
        }));
        $(obj).parent().remove();
    }

    $('.removemapping').
        click(function()
        {
            removeMapping($(this));
        });

    $('.addnewmapping').click(function()
    {
        var id_group = $(this).attr('rel') ;

        var cloned = $('div[id^="attribute-1"]').filter(':last').clone().appendTo('#new-mapping');

        var indexX = id_group;
        var newIndex = parseInt(indexX) + 1 ;

        $(this).attr('rel', newIndex) ;
        
        var class_name ;
        
        if( parseInt(newIndex%2) === 0)
            class_name = 'row-even';
        else
            class_name = 'row-odd';

        cloned.attr('id', 'attribute-' + newIndex) ;
        cloned.attr('rel', newIndex) ;
        cloned.attr('class', class_name) ;
        cloned.find('select, input').val('').attr('disabled', false) ;   
        cloned.find('select:first').val($('#attribute-1').find('select:first').val());
        cloned.find('input:first').val($('#attribute-1').find('input:first').val());

        cloned.find('select').attr('name', 'attribute[]') ; 
        cloned.find('input').attr('name', 'mapping[]') ; 

        var sname = cloned.find('select').attr('id');
        var selected_select = cloned.find('select') ;

        // Remove used options
        $('select[id^="' + sname  + '"] option:selected').each(function( index, option ) 
        {
            selected_select.find('option[value="' + option.value + '"]').remove() ;
        });

        if ( !cloned.find('option').length )
        {
            cloned.remove();
            return(false) ;
        }
        else
        {
            var id_count = parseInt($('#count').val()) + 1 ;
            $('#count').val(id_count);
        }

        cloned.find('.addnewmapping').remove() ;
        cloned.find('.removemapping').show().
            click(function()
            {
                removeMapping($(this));
            });
    });

});