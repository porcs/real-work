$(document).ready(function() 
{  
    
    $("#checkAll").click(function () 
    {
        $("input[type=checkbox]").prop('checked', true);
    });
    $("#unCheckAll").click(function () 
    {
        $("input[type=checkbox]").prop('checked', false);
    });

    var category;

    var DataSourceTree = function(options) {
        this._data 	= options.data;
        this._delay     = options.delay;
    };

    DataSourceTree.prototype.data = function(options, callback) {
        
        var $data = null;

        if(!("name" in options) && !("type" in options)){
            $data = this._data;//the root tree
            callback({ data: $data });
            return;
        }
        else if("type" in options && options.type === "folder") {

            if("additionalParameters" in options && "children" in options.additionalParameters)
                $data = options.additionalParameters.children;
            else $data = {}
        };

        if($data !== null)
            setTimeout(function(){callback({ data: $data });} , 0);
    };

    $.ajax({
        type:"get",
        url: base_url + "/my_feeds/parameters/products/getSelectedCategories/",
        contentType: "application/json",
        success: function(data) 
        {
            $('#tree1_loading').hide();
            var obj = JSON.parse(data);

            if(obj.category === "")
                $('.alert-warning').show();

            category = obj.category;
            var treeDataSource = new DataSourceTree({data: category});

            $(function(){
                $('#tree1').ace_tree({
                    dataSource: treeDataSource ,
                    multiSelect: true,
                    loadingHTML:'<div class="tree-loading"><i class="icon-refresh icon-spin blue"></i></div>',
                    'open-icon' : 'icon-minus',
                    'close-icon' : 'icon-plus',
                    'selectable' : true,
                    'selected-icon' : null,
                    'unselected-icon' : null

                });

                var selectTreeFolder = function($treeEl, folder, $parentEl) {
                    var $parentEl = $parentEl || $treeEl;
                    if (folder.type === "folder") {
                        var $folderEl = $parentEl.find("div.tree-folder-name").filter(function (_, treeFolder) {
                            return $(treeFolder).text() === folder.name;
                        }).parent();

                        $treeEl.one("loaded", function () {
                            if(folder.children)
                                $.each(folder.children, function (i, item) {
                                    selectTreeFolder($treeEl, item, $folderEl.parent());
                                });
                        });
                        $treeEl.tree("selectFolder", $folderEl);
                    }
                    else {
                        selectTreeItem($treeEl, folder, $parentEl);
                    }
                };
                var selectTreeItem = function ($treeEl, item, $parentEl) {

                    var $parentEl = $parentEl || $treeEl;

                    var $itemEl ;
                    if (item.type === "item") {

                        $itemEl = $parentEl.find("div.tree-item-name").filter(function (_, treeItem) {
                            return $(treeItem).text() === item.name && !$(treeItem).parent().is(".tree-selected");
                        }).parent();

                        $treeEl.one("loaded", function () {
                            selectTreeItem($treeEl, item, $parentEl);
                        });

                        $treeEl.tree("selectItem", $itemEl);
                    }
                    else {
                        selectTreeFolder($treeEl, item, $parentEl);
                    }
                };

                var expandTreeFolder = function ($treeEl, folder, $parentEl) {

                    var $parentEl = $parentEl || $treeEl;

                    if (folder.type === "folder") {
                        var $folderEl = $parentEl.find("div.tree-folder-name").filter(function (_, treeFolder) {
                            return $(treeFolder).text() === folder.name;
                        }).parent();

                        $parentEl.one("loaded", function () {
                            if(folder.additionalParameters.children)
                            {   
                                $.each(folder.additionalParameters.children, function (i, item) {
                                    expandTreeFolder($parentEl, item, $folderEl.parent());
                                });
                            }
                        });

                        $treeEl.tree("expandAll", $folderEl);

                    } else if (folder.type === "item"){
                        expandTreeItem($treeEl, folder, $parentEl);
                    }
                };

                var expandTreeItem = function ($treeEl, item, $parentEl) {

                   //console.log(folder);
                    var $parentEl = $parentEl || $treeEl;

                    if (item.type === "item")  {
                        var $itemEl = $parentEl.find("div.tree-item-name").filter(function (_, treeItem) {
                            return $(treeItem).text() === item.name && !$(treeItem).parent().is(".tree-selected");
                        }).parent();

                        $treeEl.one("loaded", function () {
                            expandTreeItem($treeEl, item, $parentEl);
                        });

                        $treeEl.tree("expandAll", $itemEl);

                    } else if (item.type === "folder"){
                        expandTreeFolder($treeEl, item, $parentEl);
                    }
                };


                if(obj.selected !== null)
                {
                    $.each(obj.selected, function (_, val) {
                        selectTreeItem($("#tree1"), val );
                    });
                }

                $('#expandAll').on('click', function () {
                    $.each(category, function (_, val) {
                        expandTreeItem($("#tree1"), val);
                    });
                });

                $('#collapseAll').on('click', function () {
                    $('#tree1').tree('collapse');
                });
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
        }
    });

    $("#checkAll").click(function () {
        $(".checbox-tree-folder").prop('checked', $(this).prop('checked'));
    });

});