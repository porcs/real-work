$(function() {
    $('input[type=checkbox]').click(function() {
        if($(this).is(':checked')) {
            $(this).parent().parent().addClass('active');
        } else {
            $(this).parent().parent().removeClass('active');
        }
    });

    $("#checkAll").click(function () {
        $("input[type=checkbox]").prop('checked', true).change();
        $("input[type=checkbox]").parent().parent().addClass('active');
        //$("input[type=checkbox]").checkbox('check');
    });
    $("#unCheckAll").click(function () {
        $("input[type=checkbox]").prop('checked', false).change();
        $("input[type=checkbox]").parent().parent().removeClass('active');
        //$("input[type=checkbox]").checkbox('uncheck');
    });
});