$(document).ready(function() 
{ 
    $('.removemapping').
        click(function()
        {
            var id = parseInt($('#count').val()) - 1 ;
            $('#count').val(id);
            $(this).parent().remove() ;
        });

    $('.addnewmapping').click(function()
    {
        var id_group = $(this).attr('rel') ;

        var cloned = $('div[id^="character-1"]').filter(':last').clone().appendTo('#new-mapping');

        var indexX = id_group;
        var newIndex = parseInt(indexX) + 1 ;

        $(this).attr('rel', newIndex) ;
        
        var class_name;
        
        if( parseInt(newIndex%2) === 0)
            class_name = 'row-even';
        else
            class_name = 'row-odd';

        cloned.attr('id', 'characteristics-' + newIndex) ;
        cloned.attr('rel', newIndex) ;
        cloned.attr('class', class_name) ;
        cloned.find('input').val('').attr('disabled', false) ;   

        cloned.find('.input-text').attr('name', 'character[' + newIndex + ']') ; 
        cloned.find('.input-value').attr('name', 'mapping[' + newIndex + ']') ; 

        var id_count = parseInt($('#count').val()) + 1 ;
        $('#count').val(id_count);

        cloned.find('.addnewmapping').remove() ;
        cloned.find('.removemapping').show().
            click(function()
            {
                var id = parseInt($('#count').val()) - 1 ;
                $('#count').val(id);
                $(this).parent().remove() ;
            });
    });

});