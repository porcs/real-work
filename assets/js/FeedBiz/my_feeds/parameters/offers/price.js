$(document).ready(function() 
{ 
    $('head').append('<link rel="stylesheet" href="'+base_url+'assets/css2/admin/module.admin.page.modals.min.css" />');
    
    
    if ($('#no_shop_def').length > 0) {
        var action = 'import_product';
        var error_count = 0;
        var running = true;

        function get_report(act) {
            $.ajax({
                url: "/users/ajax_get_sig_process/" + act,
                dataType: "json",
                success: function(r) {

                    //console.log(r); 

                    var timeout = r.timeout;
                    var delay = r.delay;
                    if (r.data) {
                        var status = r.data.status;
                        var err_msg = r.data.err_msg;
                        var msg = r.data.msg;
                        //var status_text = r.data.status_txt;
                        if (status == 4) {
                            window.history.back();
                        }
                        if (status == 9 && act == 'import_offer') {
                            running = false;
                            location.reload();
                        }
                        if (status == 9 && act == 'import_product') {
                            act = 'import_offer';
                        }


                    } else {
                        error_count++;
                        //console.log(r.error_msg);
                        delay = timeout;
                    }
                    if (error_count >= 5 && act == 'import_product') {
                        act = 'import_offer';
                    }
                    if (error_count >= 10) {
                        window.history.back();
                    }
                    if (running) {
                        if ($('#no_shop_def'))
                            setTimeout(function() {
                                get_report(act);
                            }, delay);
                    }
                },
                error: function(xhr, status, error) {

                    if (xhr.responseText) {
                        console.log('Progress status error :', xhr.responseText);
                        if (xhr.responseText === 'terminate') {
                            return;
                        }
                    }

                    //setTimeout(call_update, delay);   
                }
            });
        }
        get_report(action);
    }
    
    var duplicate= $('#count').val();
    var i=0;
    var colors = ["#eccb71","#e05656","#abbac3", "#569add", "#999", "#9ecf6a", "#dd56c0", "#f89406", "#3a87ad"];

    //colors
    $('#tasks li').each(function()
    {
        if(i === colors.length)i=0;
        $(this).css( "border-left-color", colors[i]);
        i++;
    });

    //numberic function
    $('#tasks').on('blur', 'input[rel="price_percentage"], input[rel="price_value"]', function()
    {
        myFunc($(this));
    });

    function myFunc(txt) {
        var value = txt.val();
        var re = /^([0-9+-]+[\.]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?|[0-9]+)$/g;

        if (!re.test(value)) 
        {
            txt.val("");
            txt.parent().parent().addClass('error');
            txt.parent().find('.help-inline').show();
        }
        else
        {
            txt.parent().parent().removeClass('error');
            txt.parent().find('.help-inline').hide();
        }

    }

    $('#tasks').on('change', 'input[rel="name"]', function()
    {
        var profile_content = $(this).parent().parent().parent() ;
        var profile_name = $(this).val() ;

        profile_name = profile_name.replace(/[^a-zA-Z0-9\s_]/g,'').replace(/\s/g,'_');

        if($('input[name="price[' + profile_name + '][name]"]').length !== 0)
        {
            profile_name = profile_name + '_' + duplicate;
            duplicate++;                    
        } 

        profile_content.find('select, input, textarea').each(function() 
        {
            if ( ! $(this).attr('rel') )
                return ;

            var field_name = $(this).attr('rel') ;
            var variable_prefix = 'price[' + profile_name + '][' + field_name + ']' ;

            $(this).attr('name', variable_prefix) ;
        });           

    });

    //Add
    $('#add').click(function(){

        if(i === colors.length)i=0;
        cloned = $('#main').clone().appendTo('#tasks').show();
        $( "li" ).last().css( "border-left-color", colors[i]);
        i++;

        var id_count = parseInt($('#count').val());
        var id_count_new = id_count+1;
        $('#count').val(id_count_new);

        //Rounding
        cloned.find('input[rel="rounding"]').attr('name', 'price[' + id_count_new + '][rounding]');

        var removeClass = cloned.find('.remove');
        removeClass.removeClass('btn-grey');
        removeClass.addClass('btn-danger');
        removeClass.find('.icon-only').addClass('icon-minus bigger-100');
        removeClass.find('.icon-only').removeClass('icon-trash bigger-110');

        removeClass.show().click(function(){
            
            $(this).parents('li').remove();
            var id = parseInt($('#count').val()) - 1 ;
            $('#count').val(id);
        });

    });

    //Remove
    function remove(obj)
    {
        var name = $(obj).attr('rel');

        bootbox.confirm( $('#confirm-message').val() + ' "' + name + '" ?', function(result) {

            if(result) 
            {
                var id = $(obj).val();

                $.ajax({
                    type: "get",
                    url: base_url + "/my_feeds/parameters/offers/price_delete/" +  id,
                    success: function(rdata) 
                    {
                        if(rdata === "true")
                        {
                            $.gritter.add({
                                title: $('#delete-success-message').val(),
                                text: name + ", " + $('#deleted-message').val(),
                                sticky: false,
                                class_name: 'gritter-light'
                            });

                            var id = parseInt($('#count').val()) - 1 ;
                            $('#count').val(id);
                            
                                $(obj).parent().parent().remove();
                        }
                        else
                        {
                            $.gritter.add({
                                title: $('#error-message').val(),
                                text: name + ", " + $('#unsuccess-message').val(),
                                sticky: false,
                                class_name: 'gritter-error'
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) 
                    {
                        $.gritter.add({
                            title: $('#error-message').val(),
                            text: $('#error-message').val() + errorThrown + ', ' + textStatus,
                            sticky: false,
                            class_name: 'gritter-error'
                        });
                    }
                });
            }
        });
    }

    $('.remove').click(function(){
        remove($(this));
    });

});