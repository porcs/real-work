 $(document).ready(function() 
 {
     $('form').on('change','.item input[type="checkbox"]' ,function(){
         var rel = $(this).attr('rel');
         var sb_rel = $(this).parents('.setBlock').attr('rel');
         var rule_cv  =$(this).parents('.ruleSet');
         var summary = rule_cv.find('.main-summary .sum_item[rel="'+sb_rel+'"]');
//         console.log(summary.find('div[rel='+rel+']'));
         if($(this).is(":checked")){
             summary.find('div[rel='+rel+']').removeClass('display-none');
         }else{
             summary.find('div[rel='+rel+']').addClass('display-none');
         } 
     });
     $('form').on('change','.item select.search-select' ,function(){
         var rel = $(this).attr('sum-rel');
         var sb_rel = $(this).parents('.setBlock').attr('rel');
         var rule_cv  =$(this).parents('.ruleSet');
         var summary = rule_cv.find('.main-summary .sum_item[rel="'+sb_rel+'"]');
//         console.log(summary.find('div[rel='+rel+'] span'));
         summary.find('div[rel='+rel+'] span').html($("option:selected",this).text());
         $(this).parents('.setBlock').find('input[rel="'+rel+'"]').attr('checked',true).change();
       
     });
     
     $('form').on('change','.item input.form-control' ,function(){
         var val = jQuery.trim($(this).val()).replace(",", ""); ;
         
         if(val!='0'&& !$.isNumeric(val)){val='';}  
         $(this).val(val);
         
         var rel = $(this).attr('sum-rel');
         var srel = $(this).attr('sum-srel');
         var sb_rel = $(this).parents('.setBlock').attr('rel');
         var rule_cv  =$(this).parents('.ruleSet');
         var summary = rule_cv.find('.main-summary .sum_item[rel="'+sb_rel+'"]');
         
         var pr_rel = $(this).parents('.price_range').attr('rel'); 	  
         summary.find('div[rel='+rel+'] .sum_price_range_item p[rel="'+pr_rel+'"] span[rel='+srel+']').html($(this).val());
//         console.log(val,'div[rel='+rel+'] .sum_price_range_item[rel="'+pr_rel+'"] span[rel='+srel+']', summary.find('div[rel="'+rel+'"] .sum_price_range_item[rel="'+pr_rel+'"] span[rel="'+srel+'"]'));
         if(srel == 'from'){
             if(val!='0'&&val*1==0){
                 $(this).parents('.price_range').find('input[sum-srel="to"]').attr("readonly", true);
             }else{
                 $(this).parents('.price_range').find('input[sum-srel="to"]').attr("readonly", false);
             }
             
         }
         $(this).parents('.setBlock').find('input[rel="'+rel+'"]').attr('checked',true).change();
         
     });
     $('.item select.search-select,.item input.form-control').each(function(){
         if($(this).val()!='')
         $(this).change();
     })
//    $(' .item input[type="checkbox"]:checked','form').each(function(){ 
//        if($(this).attr('checked')){
//            $(this).change();
//        } 
//    });

     
    $('#form').on('click', '.price-range-add', function()
    {
        var dest_i = $(this).parent().parent();//.parent().find('.col-md-10:last');//.css('margin-top', '5px');
        var rel = dest_i.attr('rel');
        var new_rel =  parseInt(rel)+1;
        
        var from_val = Number(dest_i.find('input[rel=price_range_from]').val()) ;
        var to_val = Number(dest_i.find('input[rel=price_range_to]').val()) ;

        if ( ! dest_i.find('input[rel=price_range_from]').val().length  )
            from_val  = null ;

        if ( ! dest_i.find('input[rel=price_range_to]').val().length )
            to_val  = null ;

        if ( to_val === null  || from_val=== null ||  from_val >= to_val )
        {
            if ( from_val === null )
            {
                dest_i.find('input[rel=price_range_from]').css("border-color","red").css("box-shadow","1px 1px 2px 0px") ;
            }

            if ( to_val === null )
            {
                dest_i.find('input[rel=price_range_to]').css("border-color","red").css("box-shadow","1px 1px 2px 0px") ;
            }

            if ( from_val >= to_val)
            {
                dest_i.find('input.price').css("border-color","red").css("box-shadow","1px 1px 2px 0px") ;
            }

            if(!dest_i.find('.help-block').is(":visible"))
                dest_i.append('<div class="help-block red">' + $('#price-range-error-message').val() + '</div>');

            return(false) ;
            
        } else {

            var data_content_from = dest_i.find('input[rel=price_range_from]').attr('data-content');
            var data_content_to = dest_i.find('input[rel=price_range_to]').attr('data-content');

            //cloned = dest_i.clone();
            cloned = dest_i.clone();
            cloned.appendTo(dest_i.parent());
            cloned.insertBefore(dest_i);
            
            dest_i.find('input[rel=price_range_from]').attr('name', data_content_from + '[' + new_rel + ']' + '[price_range_from]') ;
            dest_i.find('input[rel=price_range_to]').attr('name', data_content_to + '[' + new_rel + ']' + '[price_range_to]') ;

            dest_i.find('input').val('');
            dest_i.attr('rel',new_rel);
            dest_i.find('input[rel=price_range_from]').val( parseInt(to_val + 1) ) ;
            //cloned.find('input[rel=price_range_from]').val( parseInt(to_val + 1) ) ;
            //cloned.find('.price-range-add').hide();
            cloned.find('input.price').change(function()
            {
                changePriceRange($(this));
            });
            cloned.find('.price-range-remove').show().
              click(function()
              {
                $(this).parent().parent().remove() ;
              });

              cloned.find('label:first').text('');
              cloned.find('.price-range-add').parent().addClass('hidden');

              //Clear
              dest_i.parent().find('.help-block').html('');
              cloned.parent().find('.help-block').html('');

            //Summary 
            var item_no = cloned.find('input[rel=price_range_to]').parent().parent().parent().parent().parent().parent().parent().parent().find('input[rel="item_count"]').val();
            var summary = cloned.find('input[rel=price_range_to]').parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find('.summary');

            summary.find('li[rel="'+item_no+'"]').find('span[rel="price_range"]').show().append(parseInt(to_val + 1));
            summary.find('li[rel="'+item_no+'"]').find('span[rel="price_range"]').show().append(' - ');

            //Member Parent
            //var member_parent = cloned;
            //Move Out Class
            //cloned.appendTo(cloned.parent());
            //cloned.insertBefore(member_parent);
        }
    });
    
        $('form').on('click', '.choZn .cb-plus.good', function(){
		var sb_rel = $(this).parents('.setBlock').attr('rel');
                var rule_cv  =$(this).parents('.ruleSet');
                var summary = rule_cv.find('.main-summary .sum_item[rel="'+sb_rel+'"]');
                var input_to =  $(this).parents('.price_range').find('input[sum-srel="to"]');
                var val = input_to.val();
                
                if(val==''){
                    $(this).parents('.price_range').addClass('has-error');
                    return;
                }else{
                    $(this).parents('.price_range').removeClass('has-error');
                }
                var new_price = $(this).parents('.price_range').clone(true).insertAfter($(this).closest('.row')); 
                var rel = new_price.attr('rel');
                new_price.attr('rel',rel*1+1);
                $(this).removeClass('good').addClass('bad');
//		new_price.find('.cb-plus.good').removeClass('good').addClass('bad');
                new_price.find('input[sum-srel="from"]').val(val*1+1); 
                new_price.find('input[sum-srel="to"]').val(''); 
                var new_sum_price = summary.find('.sum_price_range_item p[rel="'+rel+'"]').clone(true).insertAfter(summary.find('.sum_price_range_item p[rel="'+rel+'"]'));
                new_sum_price.find('span[rel="from"]').html(val*1+1);
                new_sum_price.attr('rel',rel*1+1);
//                console.log(val*1+1,new_price.find('input[sum-srel="from"]'),new_price);
                new_sum_price.find('span[rel="to"]').html('');
                $(this).closest('.showSelectBlock').find('input[rel="name"]').trigger('change');
	});

	$('form').on('click', '.choZn .cb-plus.bad', function(){
                var sb_rel = $(this).parents('.setBlock').attr('rel');
                var rule_cv  =$(this).parents('.ruleSet');
                var summary = rule_cv.find('.main-summary .sum_item[rel="'+sb_rel+'"]');
                var rel = $(this).parents('.price_range').attr('rel');
		$(this).closest('.row').remove();
                summary.find('.sum_price_range_item p[rel="'+rel+'"]').remove();
	});
	
     
     
    
    $('form').on('change', 'input[rel="name"]', function()
    {
        var profile_content =$(this).parents('.ruleSet');//= $(this).parent().parent().parent() ;
        var profile_name = $(this).val() ; 
         var summary = profile_content.find('.main-summary');

        profile_name = profile_name.replace(/[^a-zA-Z0-9\s_]/g,'').replace(/\s/g,'_');
        summary.find('.rule-name').html(profile_name); 
        if(!validateRuleName(this)){
        	return;
        }
        
        
        
        var duplicate= 1;
        if(!$('input[name="rule[' + profile_name + '][name]"]').is($(this)) && $('input[name="rule[' + profile_name + '][name]"]').length !== 0)
        {
//            console.log($('input[name="rule[' + profile_name + '][name]"]'));
            profile_name = profile_name + '_' + duplicate;
            duplicate++;                    
        } 

        profile_content.find('input, textarea').each(function() 
        {
            if ( ! $(this).attr('rel') )
                return ;
            if($(this).parents('.setBlock_content').parent().hasClass('hidden'))return;

            var field_name = $(this).attr('rel') ;
            var variable_prefix = 'rule[' + profile_name + '][' + field_name + ']' ;

            $(this).attr('name', variable_prefix) ;
            $(this).attr('data-content', profile_name) ;
        });  

        profile_content.find('.items input[rel="id_rule_item"], .items select').each(function() 
        {
            var rel = $(this).closest("div.setBlock").attr('no-rel');

            if ( ! $(this).attr('rel') )
                return ;
            if($(this).parents('.setBlock_content').parent().hasClass('hidden'))return;

            var field_name = $(this).attr('rel') ;
            var variable_prefix = 'rule[' + profile_name + '][rule][' + rel + '][' + field_name + ']' ;
            $(this).attr('name', variable_prefix) ;
        }); 

        //Price Range
        profile_content.find('.items input[rel="price_range_from"], .items input[rel="price_range_to"], .items input[rel="id_rule_item_price_range"]').each(function() 
        {
            var rel = $(this).closest("div.setBlock").attr('no-rel');

            if ( ! $(this).attr('rel') )
                return ;
            if($(this).parents('.setBlock_content').parent().hasClass('hidden'))return;

            var item_rel = $(this).closest("div.price_range").attr('rel');
            var variable_prefix = 'rule[' + profile_name + '][rule][' + rel + '][price_range][' + item_rel + '][' + $(this).attr('rel') + ']' ;
            
            if($(this).val().length > 0) {
//                console.log(variable_prefix,$(this).val().length );
                $(this).attr('name', variable_prefix) ;
                $(this).attr('data-content', 'rule[' + profile_name + '][rule][' + rel + '][price_range]') ;
            } else {
//                console.log(variable_prefix,$(this).val().length );
                $(this).attr('name', variable_prefix) ;
            }
        }); 

    });
     var add_at = 1;
     $(document).on('click', '.showProfile', function(e){
		e.preventDefault();

		var new_block = $(this).closest('.row').next().clone(true).insertAfter($(this).closest('.row').next()).show();
                new_block.find('.setSettings').show();
                new_block.removeAttr('id');
		countBlock($('.countProfile'), $('.showSelectBlock'));	
		choSelect();		
	});

	rulesAddSettings();

	$(document).on('click', '.showSet', function(e){
		e.preventDefault();
		var $this 				= $(this),
			$showSelectBlock 	= $this.closest('.showSelectBlock'),
			$ruleCheckBo 		= $('#main .ruleSettings').find('.checkBo'),
			
			$rowCount 			= $ruleCheckBo.length,
			$choZN 				= $('#main .ruleSettings').find('.choZn'),
			
                        
			$templateBlock 	= '<div class="setBlock clearfix">'+
							'<div class="m-b10 clearfix">'+
								'<a href="" class="pull-right removeSet">Remove set <i class="cb-remove"></i></a>'+
							'</div>'+
							'<div class="setBlock_content">'+
								'<div class="setBlock_text">'+									
									
								'</div>'+
							'</div>'+
						'</div>',
			$templateText 	= '<div class="row addSettings item">'+
								'<div class="col-sm-6 addCheckBo">'+

								'</div>'+
								'<div class="col-sm-6 addSelect">'+

								'</div>'+
							'</div>';
                                                
                        var rule_name_obj = $showSelectBlock.find('input[rel="name"]');
                        if(jQuery.trim(rule_name_obj.val())== ''){
                            rule_name_obj.focus();
                            rule_name_obj.parent().addClass('has-error').find('div.error').slideDown('fast');
                            return;
                        }else{ 
                            rule_name_obj.parent().removeClass('has-error').find('div.error').slideUp('fast');
                        }
                        var rule_cv  =$(this).parents('.ruleSet');
                        var summary = rule_cv.find('.main-summary');
			choSelect();

			$showSelectBlock.find('.setBlockMain') ;

			$($templateBlock).appendTo($showSelectBlock.find('.items'));//.insertBefore($this.parent());

			$($templateText).appendTo($showSelectBlock.find('.setBlock').last().find('.setBlock_text'));
			
			$showSelectBlock.find('.setBlock').removeClass('hidden');
                        
//                        console.log($showSelectBlock,$ruleCheckBo,$rowCount)
                        
			for (var i=0; i < $rowCount; i++){
				$($templateText).appendTo($showSelectBlock.find('.setBlock').last().find('.setBlock_text'));
                                var $addSelect 		= $showSelectBlock.find('.setBlock').last().find('.addSelect'),
                                $addCheckBo 		= $showSelectBlock.find('.setBlock').last().find('.addCheckBo');
 				$ruleCheckBo.eq(i).clone(true).appendTo($addCheckBo.eq(i));
				$choZN.eq(i).clone().appendTo($addSelect.eq(i));
                                
				choSelect();
			}

			$showSelectBlock.find('.setSettings').show();

			var item_added = $showSelectBlock.find('.items .setBlock').last();

			//countRule();
			if($this.closest('.profileBlock').find('.setBlockMain')){
				$this.closest('.profileBlock').find('.countRule').text($this.closest('.profileBlock').find('.setBlock').length  );
			}
			else{
				$this.closest('.profileBlock').find('.countRule').text($this.closest('.profileBlock').find('.setBlock').length);
			}
                        var main_sitem =summary.find('.sum_item.display-none');
                        var new_sitem = main_sitem.clone(true).insertAfter(summary.find('.sum_item').last()).slideDown('fast');
                        item_added.attr('rel','s'+add_at).attr('no-rel',add_at);
                        new_sitem.attr('rel','s'+add_at).removeClass('display-none')
                        $showSelectBlock.find('input[rel="name"]').trigger('change');
                        add_at++;
	});

	$(document).on('click', '.removeSet', function(e){
		e.preventDefault();
		var $this = $(this);
                
                var rule_cv  =$(this).parents('.ruleSet');
                var sb_rel = $(this).parents('.setBlock').attr('rel');
                var summary = rule_cv.find('.main-summary .sum_item[rel="'+sb_rel+'"]');
		 
                $this.closest('.profileBlock').find('.countRule').text($('.setBlock').length   - 1);
		 
		summary.remove();
		$this.closest('.setBlock').remove(); 
                var id = $(this).attr('data-id');
                if(id==undefined)return;
                $.ajax({
                    type: "get",
                    url:  base_url + "/my_feeds/parameters/offers/rules_item_delete/" +  id,});
	});

	$(document).on('click', '.removeProfile', function(e){
		e.preventDefault();	
		var removeProfile = this;
                var name = $(this).parent().find('input[rel="name"]').val();
		bootbox.confirm( $('#confirm-message').val() + ', "' + name + '" ?', function(result) {
                    if(result){
			var $ruleCheckBo = 	$('.ruleSettings').find('.checkBo').length;
	
			$(removeProfile).closest('.showSelectBlock').remove();
	
			countBlock($('.countProfile'), $('.showSelectBlock'));
			 
	                var id = $(removeProfile).attr('data-id');
	                if(id==undefined)return;
	                $.ajax({
	                type: "get",
	                url:  base_url + "/my_feeds/parameters/offers/rules_delete/" +  id,
	                });
                    }
		});
		//rulesRemoveSettings();

	});

	countBlock($('.countProfile'), $('.showSelectBlock'));

	$(document).on('click', '.withIcon .cb-plus.bad', function(){
		$(this).closest('.row').remove();
	});

	$(document).on('click', '.withIcon .cb-plus', function(){
		countBlock($('.count'), $('.showSelectBlock'));
	});

	$('form').on('submit', function(){
		return $('div.error:visible').length === 0;
	});
    function validateRuleName(element){		
        var errorFound = false;
        if(jQuery.trim($(element).val()) == ''){
        	errorFound = true;
        	jQuery('[rel="err_rule_name_blank"]', $(element).parent()).removeClass('hide');
        	jQuery('[rel="err_rule_name_duplicate"]', $(element).parent()).addClass('hide');
        }
        else if(jQuery('input[rel="name"]').filter(function(){ return jQuery.trim($(element).val()) == jQuery.trim($(this).val()); }).length > 1){
        	errorFound = true;
        	jQuery('[rel="err_rule_name_blank"]', $(element).parent()).addClass('hide');
        	jQuery('[rel="err_rule_name_duplicate"]', $(element).parent()).removeClass('hide');
        }
        
        if(errorFound){
            $(element).focus();
            $(element).parent().addClass('has-error').find('div.error').show();
            return false;
        }else{ 
            $(element).parent().removeClass('has-error').find('div.error').hide();
            return true;
        }
    }
});