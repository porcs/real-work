$(document).ready(function () {
    $('form').on('reset', function (e) {
        setTimeout(function () {
            $('.tree-folder-name > input[type=checkbox]').each(function () {
                //var $this = $(this);
                //console.log($this.is(:checked));
                if ($(this).is(':checked') == false) {
                    //$(this).checkbox('check');
                    //}else{
                    $(this).prop('checked', false).change;
                    $(this).parent().parent().find('.pull-right').remove();
                    $(this).checkbox('uncheck');
                }
            });
        });
    });
});

//var base_url = location.origin;
var just_cal = false;
var just_check_all = false;
$(document).ready(function () {

    //$('head').append('<style>.profile{ width: 90%; height: 25px; font-size: 12px; border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; }div.tree-folder i.cp_down_btn.icon-arrow-down { transform: rotate(0deg); }  <style>');

    /*$("#checkAll").click(function () {
     $("input[type=checkbox]").each(function(){
     if(!$(this).prop( "checked" )){
     $(this).prop('checked', true);
     $(this).checkbox('check');
     
     getProfile($(this));
     if(!just_check_all){
     just_cal=false;
     just_check_all = true;
     }
     }
     });
     });
     
     $("#unCheckAll").click(function () {
     $("input[type=checkbox]").each(function(){
     $(this).prop('checked', false);
     $(this).parent().parent().find('.pull-right').remove();
     $(this).checkbox('uncheck');
     });
     });*/

    /*$('.add_category').each(function(){
     $(this).parent().parent().find('.tree-folder-content').append('<i class="icon-spinner icon-spin orange"></i>');
     expandCollapseAll($(this));
     });*/
    getSelectedAllCategory();
    apply();
    //init_rel_all_row();        

});
function init_rel_all_row() {
    var i = 1;
    if (just_cal)
        return;

    just_cal = true;
    $('.icon-more').each(function () {
        if ($(this).parents('#main').length > 0)
            return;
        var row = $(this).parents('li');
        if (row.length == 0)
            return;
        row.attr('rel', i);
        i++;
    });

}

function click_copy_val_sel(obj) {
    showWaiting(true);
    var time = 0;
    var num = $('body').find('select').length;
    if (num > 200) {
        time = 200;
    }

    setTimeout(function () {
        var currentTree = $(obj).closest('.tree_point');
        var currentLeaf = $(obj).closest('.treeRight');
        var currentLeafOffset = $(currentLeaf).offset();
        var currentValue = $('select', currentLeaf).val();
        $('.treeRight', currentTree).each(function (eleIndex, eleObject) {
            if ($(eleObject).parent().find('input[type="checkbox"]').prop('checked'))
                if (currentLeafOffset.top < $(eleObject).offset().top) {
                    //case blank
                    var test_val = $('select', eleObject).val();
//                        console.log($('select', eleObject),test_val);
                    if (test_val == '')
                        if (currentValue == '' || currentValue == undefined) {
                            $('select', eleObject).val('');//.trigger("chosen:updated");
                        }//case value
                        else if ($('select', eleObject).val() == '' || $('select', eleObject).val() == undefined) {
                            $('select', eleObject).val(currentValue);//.trigger("chosen:updated");
                        } else if ($('select', eleObject).val() > 0) {
                            $('select', eleObject).val(currentValue);//.trigger("chosen:updated");
                        }
                }
        });
        showWaiting(false);

    }, time);
}

function apply() {
    setTimeout(function () {
        $('li.expandAll').trigger('click');
    }, 100);
    /*$('#collapseAll').unbind('click').on('click', function(){
     $('input[rel="id_category"]').each(function(){
     if($(this).parent().parent().parent().find('.icon-minus').length > 0){
     $(this).parent().parent().parent().parent().find('.tree-folder-content').hide();
     $(this).parent().parent().parent().find('.icon-minus').removeClass('icon-minus').addClass('icon-plus');
     }
     }); 
     });
     
     $('#expandAll').unbind('click').on('click', function(){
     $('.add_category').each(function(){
     $(this).parent().parent().parent().parent().find('.tree-folder-content').show();
     $(this).parent().parent().parent().find('.icon-plus').removeClass('icon-plus').addClass('icon-minus');
     //$(this).parent().parent().find('.tree-folder-content').append('<i class="icon-spinner icon-spin orange"></i>');
     //expandCollapseAll($(this));
     }); 
     });*/

    $('.add_category').unbind('click').on('click', function () {
        //$(this).parent().parent().find('.tree-folder-content').append('<i class="icon-spinner icon-spin orange"></i>');
        expandCollapse($(this));
    });

    $('input[rel="id_category"]').each(function (i, elm) {
        $(elm).unbind('click').change(function () {//.unbind('click')
            getProfile($(this));
            if (!$('.checkBaAll').hasClass('active'))
                recParCheck($(this));
            if (!just_check_all) {
                just_cal = false;
            }
        });
    });

    function recParCheck(cur) {

        //var par = cur.parent().parent().parent().parent().parent().parent();
        var par = cur.parents('ul').prev().prev();

        $(par).each(function (i, elm) {
            //if(!par.hasClass('tree-folder')) return;
            if ($(elm).hasClass('tree_item')) {
                //par = par.find('.tree-folder-header');
                par = $(elm).find('input[rel="id_category"]');//.first();
                if (!par.is(':checked') && cur.is(':checked')) {
                    //par.trigger('click');
                    par.prop('checked', true).change();
                    //recParCheck(par);
                }
            }
        });
    }

    $.fn.enableCheckboxRangeSelection = function () {
        var lastCheckbox = null;
        var $spec = this;
        //var chk_fristh = $(this).parent().find('input').attr('checked');

        var lastType = null;
        $spec.unbind("click");
        $spec.bind("click", function (e) {
            
            lastType = $(lastCheckbox).find('input[type=checkbox]').is(':checked');// e.currentTarget.checked;
            var $target = this;//e.target
            if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {

                var $checkbox = $spec.slice(
                        Math.min($spec.index(lastCheckbox), $spec.index(this)),
                        Math.max($spec.index(lastCheckbox), $spec.index(this)) + 1
                        );

                $checkbox.find('input[type=checkbox]').each(function () {
//                                  if($(this).is($($target).find('input[type=checkbox]')))return;

                    $(this).prop({'checked': lastType}).change();
                    var chk = Boolean($(this).find('input').attr('checked'));

                    if ($(this)[0] !== lastCheckbox) {
                        switch (true) {
                            case chk :
                                $(this).parent().parent().find('.treeRight').remove();
                                break;
                            case !chk :
                                getProfile($(this).parent().find($("input[type=checkbox]")));
                                break;
                            default:
                                getProfile($(this).parent().find($("input[type=checkbox]")));
                                break;
                        }
                    }
                });
            }
            lastCheckbox = this;
        });
    };

    $('label.checkBa').enableCheckboxRangeSelection();

    //$('input[rel="id_category"]').shiftClick();

    /*$('input[rel="id_category"]').shiftcheckbox({
     checkboxSelector : ':checkbox',
     selectAll        : '#demo3 .all'
     });*/
}

/*$.fn.shiftClick = function () {
 
 var lastSelected; // Firefox error: LastSelected is undefined
 var selected = { };
 var checkBoxes = $(this);
 
 this.each(function () {
 $(this).click(function (ev) {
 console.log('clic');
 var selectedValue = this.value ;
 
 if (ev.shiftKey) {
 var last = checkBoxes.index(lastSelected);
 var first = checkBoxes.index(this);
 var start = Math.min(first, last);
 var end = Math.max(first, last);
 var chk = lastSelected.checked;
 
 console.log(last, first, start, end, chk);
 
 for (var i = start; i <= end; i++) 
 {
 checkBoxes[i].checked = chk;
 console.log(chk);
 checkBoxes[i].closest('.checkbox-custom').find('i');
 checkBoxes[i].checkbox('check');
 selected[i] = getProfile(checkBoxes[i]);
 
 if(selected[i])
 {
 selected[i].each(function () {
 if(i === start)
 selectedValue = this.value;
 
 $(this).on('change', function(){
 selectedValue = $(this).val();
 });
 
 $(this).val(selectedValue);
 });
 }
 
 }
 
 if(!just_check_all){
 just_cal=false; 
 }
 } else {
 lastSelected = this;
 }
 });
 });
 };*/
// $('input[rel="id_category"]').shiftClick();


//Get Profile
function getProfile(e) {
    var select;
    var ele = $(e).parents('.tree_item').parent();
    //$('.tree_show').show();
    var select_ele = ele.children('.treeRight').find('select');
    if ($(e).is(':checked'))
    {
        if (ele.children('.treeRight:visible').length === 0) {
            if (ele.children('.treeRight').length === 1) {
                ele.children('.treeRight').removeClass('hide');
            }
            if (select_ele.length === 0)
            {
                var cloned = $('#main').find('.treeRight').clone();
                cloned.appendTo(ele);
                cloned.find('select.search-select').attr('name', 'category[' + $(e).val() + '][id_profile]');
                select_ele.attr('name', 'category[' + $(e).val() + '][id_profile]');
                select_ele.attr('id', 'category' + $(e).val() + '_id_profile');

                select = cloned.find('select.search-select');
                if (ele.children('.treeRight').children('.chosen-container').length > 0) {
                    ele.children('.treeRight').children('.chosen-container').remove();
                }
                //console.log(cloned,ele,select);
                //select.chosen();
//                    choSelect();
            } else {
                //select = ele.find('select.search-select');
                select_ele.attr('name', select_ele.attr('data-name'));
            }
        }
    } else {
        select_ele.attr('data-name', select_ele.attr('name'));
        select_ele.removeAttr('name');
        ele.children('.treeRight').addClass('hide');
    }

//        if(ele.find('div.chosen-container').length > 0){
//            ele.find('div.chosen-container').remove();
//        }

//        choSelect();
    return select;

}

//Get All Selected Category
function getSelectedAllCategory(ele) {
    showWaiting(true);
    var url = base_url + "/profiles/getSelectedAllCategory/";
    //console.log(url);

    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {

            if (data && data != "" && data.length > 2)
            {
                $('#has_category').show();
                $('#tree1').html(data);

                tree($('.tree'));

                apply();
                $('.chosen-single').find('div').addClass('button-select');
                $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');

                setTimeout(function () {
                    $("#tree1 .search-select").each(function () {
                        var size = $('option', this).size();

                        if (size <= 1) {
                            $(this).parent().parent().remove();
//                                $(this).hide();
//                                $('i',$(this).parent()).hide();
                        } else {
                            $(this).chosen({
                                disable_search_threshold: 1,
                                no_results_text: "Nothing found!"
                            }).trigger("chosen:updated");
                        }
                    });
                    showWaiting(false);
                    $('#page-loaded').show();
                    $('#page-load').hide();
                    $('.custom-form').checkBo();
                }, 1000);
                //init_rel_all_row();
            } else
            {
                $('#none_category').show();
                $('#page-loaded').show();
                $('#page-load').hide();
                $('.tree').hide();
                $('.btnShow').hide();
                $('.btnPrevious').show();

                apply();
                showWaiting(false);

            }
        },
        error: function (data, error) {
            console.log(data);
            console.log(error);
            showWaiting(false);
        }
    });
    /*
     $.ajax({
     type: 'GET',
     url: base_url + "/profiles/getSelectedCategory/" + ele.parent().find('input[rel="id_category"]').val(),
     dataType:'json',
     
     success: function(data) {
     ele.parent().parent().find('.tree-folder-content').html(data);
     var add = ele.parent().parent().find('.tree-folder-content').find('.add_category');
     if(add.length > 0)
     {
     add.each(function(){
     expandCollapseAll($(this));
     });
     }
     ele.removeClass('icon-plus').addClass('icon-minus');
     apply();
     
     console.log(data);
     
     },
     error: function(data, error){
     var add = ele.parent().parent().find('.tree-folder-content').find('.add_category');
     if(add.length > 0)
     {
     add.each(function(){
     expandCollapseAll($(this));
     });
     }
     console.log(error);
     }
     });*/
}

function expandCollapse(e) {
    if (!e.hasClass('active')) {
        e.parent().parent().parent().find('ul.tree_child').slideDown();
        e.addClass('active');
    } else {
        e.parent().parent().parent().find('ul.tree_child').slideUp();
        e.removeClass('active');
    }
//        var content = e.parent().parent().find('.tree-folder-content').first();
//        
//        if(e.hasClass('icon-plus'))
//        {
//            if(content.children().length > 1){
//                content.show();
//                content.find('.icon-spinner').remove();
//            }//else{
//                //getSelectedCategory(e);
//            //}
//            //getSelectedCategory(e);
//            e.removeClass('icon-plus').addClass('icon-minus');
//        }
//        else
//        {
//            content.hide();
//            e.removeClass('icon-minus').addClass('icon-plus');
//        }
}

function expandCollapseAll(e) {

    var content = e.parent().parent().find('.tree-folder-content');

    /*if(e.hasClass('icon-plus'))
     {*/
    if (content.children().length > 1) {
        content.show();
    } else {
        getSelectedAllCategory(e);
    }
    //content.show();
    //getSelectedAllCategory(e);

    /*}
     else
     {
     //content.hide();
     //e.removeClass('icon-minus').addClass('icon-plus');
     }*/
}