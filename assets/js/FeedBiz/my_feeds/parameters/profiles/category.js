$(document).ready(function () {
    $('form').on('reset', function (e) {
        setTimeout(function () {
            $('.tree-folder-name > input[type=checkbox]').each(function () {
                //var $this = $(this);
                //console.log($this.is(:checked));
                if ($(this).is(':checked') == false) {
                    //$(this).checkbox('check');
                    //}else{
                    $(this).prop('checked', false);
                    $(this).parent().parent().find('.pull-right').remove();
                    $(this).checkbox('uncheck');
                }
            });

        });
    });
});

//var base_url = location.origin;
var just_cal = false;
var just_check_all = false;
$(document).ready(function () {

    //$('head').append('<style>.profile{ width: 90%; height: 25px; font-size: 12px; border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; }div.tree-folder i.cp_down_btn.icon-arrow-down { transform: rotate(0deg); }  <style>');

    /*$("#checkAll").click(function () {
     $("input[type=checkbox]").each(function () {
     if (!$(this).prop("checked")) {
     $(this).prop('checked', true);
     $(this).checkbox('check');
     
     getProfile($(this));
     if (!just_check_all) {
     just_cal = false;
     just_check_all = true;
     }
     }
     });
     });
     
     $("#unCheckAll").click(function () {
     $("input[type=checkbox]").each(function () {
     $(this).prop('checked', false);
     $(this).parent().parent().find('.pull-right').remove();
     $(this).checkbox('uncheck');
     });
     });*/

    /*$('.add_category').each(function(){
     $(this).parent().parent().find('.tree-folder-content').append('<i class="icon-spinner icon-spin orange"></i>');
     expandCollapseAll($(this));
     });*/
    getSelectedAllCategory();
    apply();
    //init_rel_all_row();        

});
function init_rel_all_row() {
    var i = 1;
    if (just_cal)
        return;

    just_cal = true;
    $('.cp_down_btn').each(function () {
        if ($(this).parents('#main').length > 0)
            return;
        var row = $(this).parents('.tree-folder-header');
        if (row.length == 0)
            return;
        row.attr('rel', i);
        i++;
    });


}

function click_copy_val_sel(obj) {
    showWaiting(true);
    var time = 0;
    var num = $('body').find('select').length;
    if (num > 200) {/*!just_cal &&*/
        time = 200;
    }

    setTimeout(function () {
        init_rel_all_row();
        var p_row = obj.parents('.tree-folder-header');
        var cur_pos = p_row.attr('rel');
        var cur_sel_val = p_row.find('select').val();
        $('body').find('.tree-folder-header').each(function () {
            var rel = $(this).attr('rel');

            if (rel * 1 > cur_pos * 1) {
                $(this).find('select').val(cur_sel_val).trigger("change");
            }
        });
        showWaiting(false);
    }, time);
}

function apply() {
    setTimeout(function () {
        $('li.expandAll').trigger('click');
    }, 100)
    $('#collapseAll').unbind('click').on('click', function () {
        $('input[rel="id_category"]').each(function () {
            if ($(this).parent().parent().parent().find('.icon-minus').length > 0) {
                $(this).parent().parent().parent().parent().find('.tree-folder-content').hide();
                $(this).parent().parent().parent().find('.icon-minus').removeClass('icon-minus').addClass('icon-plus');
            }
        });
    });

    $('#expandAll').unbind('click').on('click', function () {
        $('.add_category').each(function () {
            $(this).parent().parent().parent().parent().find('.tree-folder-content').show();
            $(this).parent().parent().parent().find('.icon-plus').removeClass('icon-plus').addClass('icon-minus');
            //$(this).parent().parent().find('.tree-folder-content').append('<i class="icon-spinner icon-spin orange"></i>');
            //expandCollapseAll($(this));
        });
    });

    $('.add_category').unbind('click').on('click', function () {
        //$(this).parent().parent().find('.tree-folder-content').append('<i class="icon-spinner icon-spin orange"></i>');
        expandCollapse($(this));
    });

    $('input[rel="id_category"]').unbind('click').on('change', function () {
        console.log('xx');
        getProfile($(this));
        //recParCheck($(this));
        if (!just_check_all) {
            just_cal = false;
        }

        /*$(this).parent().parent().parent().find('select').select2({
         placeholder: "Select a Option",
         allowClear: true
         });*/

    });
    function recParCheck(cur) {

        //var par = cur.parent().parent().parent().parent().parent().parent();
        var par = cur.parents('ul').prev().prev();

        $(par).each(function (i, elm) {
            if ($(elm).hasClass('tree-folder')) {
                //par = par.find('.tree-folder-header');
                par = $(elm).find('input[rel="id_category"]');//.first();
                if (!par.is(':checked') && cur.is(':checked')) {
                    par.trigger('click');
                    recParCheck(par);
                }
            }
        });
    }


    $.fn.enableCheckboxRangeSelection = function () {
        var lastCheckbox = null;
        var $spec = this;
        //var chk_fristh = $(this).parent().find('input').attr('checked');

        $spec.unbind("click.checkboxrange");
        $spec.bind("click.checkboxrange", function (e) {
            showWaiting(true);
            if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {
                /*$spec.slice(
                 Math.min($spec.index(lastCheckbox)+1, $spec.index(e.target)),
                 Math.max($spec.index(lastCheckbox), $spec.index(e.target)) + 1
                 ).toggleClass('checked');*/

                $spec.slice(
                        Math.min($spec.index(lastCheckbox), $spec.index(e.target)),
                        Math.max($spec.index(lastCheckbox), $spec.index(e.target)) + 1
                        ).each(function () {
                    var chk = Boolean($(this).parent().find('input').attr('checked'));
                    //var chk = Boolean(lastCheckbox.parent().find('input').attr('checked'));
                    //$(this).parent().find('input').attr('checked', !chk);
                    //console.log(chk);
                    if ($(this)[0] !== lastCheckbox) {
                        switch (true) {
                            case chk :
                                $(this).parent().find('input').checkbox('uncheck');
                                $(this).parent().parent().find('.pull-right').remove();
                                break;
                            case !chk :
                                $(this).parent().find('input').checkbox('check');
                                getProfile($(this).parent().find($("input[type=checkbox]")));
                                break;
                            default:
                                $(this).parent().find('input').checkbox('check');
                                getProfile($(this).parent().find($("input[type=checkbox]")));
                                break;
                        }
                    }
                });
            }
            lastCheckbox = e.target;
            showWaiting(false);
        });
    };


    $('.lbl').enableCheckboxRangeSelection();

    //$('input[rel="id_category"]').shiftClick();

    /*$('input[rel="id_category"]').shiftcheckbox({
     checkboxSelector : ':checkbox',
     selectAll        : '#demo3 .all'
     });*/
}

/*$.fn.shiftClick = function () {
 
 var lastSelected; // Firefox error: LastSelected is undefined
 var selected = { };
 var checkBoxes = $(this);
 
 this.each(function () {
 $(this).click(function (ev) {
 console.log('clic');
 var selectedValue = this.value ;
 
 if (ev.shiftKey) {
 var last = checkBoxes.index(lastSelected);
 var first = checkBoxes.index(this);
 var start = Math.min(first, last);
 var end = Math.max(first, last);
 var chk = lastSelected.checked;
 
 console.log(last, first, start, end, chk);
 
 for (var i = start; i <= end; i++) 
 {
 checkBoxes[i].checked = chk;
 console.log(chk);
 checkBoxes[i].closest('.checkbox-custom').find('i');
 checkBoxes[i].checkbox('check');
 selected[i] = getProfile(checkBoxes[i]);
 
 if(selected[i])
 {
 selected[i].each(function () {
 if(i === start)
 selectedValue = this.value;
 
 $(this).on('change', function(){
 selectedValue = $(this).val();
 });
 
 $(this).val(selectedValue);
 });
 }
 
 }
 
 if(!just_check_all){
 just_cal=false; 
 }
 } else {
 lastSelected = this;
 }
 });
 });
 };*/
// $('input[rel="id_category"]').shiftClick();


//Get Profile
function getProfile(e) {

    var select;
    var ele = $(e).parent().parent();

    if ($(e).is(':checked'))
    {
        if (ele.find('select').length === 0)
        {
            var cloned = $('#main').find('.pull-right').clone();
            cloned.appendTo(ele);
            //console.log($(e).val());
            cloned.find('select').attr('name', 'category[' + $(e).val() + '][id_profile]');
            //cloned.find('select').attr('style', 'background : #DAFFF0 !important;');

            cloned.find('.cp_down_btn').click(function () {
                click_copy_val_sel($(this));
            });

            ele.find('select').attr('name', 'category[' + $(e).val() + '][id_profile]');
            ele.find('select').attr('id', 'category' + $(e).val() + '_id_profile');

            select = cloned.find('select');
        } else {
            ele.find('select').attr('name', ele.find('select').attr('data-name'));
            select = ele.find('select');
        }
    } else {
        ele.find('select').attr('data-name', ele.find('select').attr('name'));
        ele.find('select').removeAttr('name');
        ele.children('.treeRight').addClass('hide');
        //ele.find('.pull-right').remove();
    }

    /*cloned.find('select').select2({
     placeholder: "Select a Option",
     allowClear: true
     });
     
     ele.find('select').select2();*/

    return select;

}

//Get All Selected Category
function getSelectedAllCategory(ele) {
    showWaiting(true);
    var url = base_url + "/profiles/getSelectedAllCategory/";
    console.log(url);
    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {

            if (data && data != "")
            {
                $('#has_category').show();
                $('#tree1').html(data);
                //init_rel_all_row();
            } else
            {
                $('#none_category').show();
                $('.tree').hide();
            }

            $('#page-loaded').show();
            $('#page-load').hide();

            apply();
            showWaiting(false);
        },
        error: function (data, error) {
            console.log(data);
            console.log(error);
            showWaiting(false);
        }
    });
//        {*
//        $.ajax({
//            type: 'GET',
//            url: base_url + "/profiles/getSelectedCategory/" + ele.parent().find('input[rel="id_category"]').val(),
//            dataType:'json',
//
//            success: function(data) {
//                 ele.parent().parent().find('.tree-folder-content').html(data);
//                 var add = ele.parent().parent().find('.tree-folder-content').find('.add_category');
//                 if(add.length > 0)
//                 {
//                    add.each(function(){
//                        expandCollapseAll($(this));
//                    });
//                 }
//                 ele.removeClass('icon-plus').addClass('icon-minus');
//                 apply();
//                 
//                 console.log(data);
//                 
//            },
//            error: function(data, error){
//                var add = ele.parent().parent().find('.tree-folder-content').find('.add_category');
//                 if(add.length > 0)
//                 {
//                    add.each(function(){
//                        expandCollapseAll($(this));
//                    });
//                 }
//                console.log(error);
//            }
//        });*}
}

function expandCollapse(e) {
    if (!e.hasClass('active')) {
        e.parent().parent().parent().find('ul.tree_child').slideDown();
        e.addClass('active');
    } else {
        e.parent().parent().parent().find('ul.tree_child').slideUp();
        e.removeClass('active');
    }

//        var content = e.parent().parent().find('.tree-folder-content');
//        
//        if(e.hasClass('icon-plus'))
//        {
//            if(content.children().length > 1){
//                content.show();
//                content.find('.icon-spinner').remove();
//            }//else{
//                //getSelectedCategory(e);
//            //}
//            //getSelectedCategory(e);
//            e.removeClass('icon-plus').addClass('icon-minus');
//        }
//        else
//        {
//            content.hide();
//            e.removeClass('icon-minus').addClass('icon-plus');
//        }
}

function expandCollapseAll(e) {

    var content = e.parent().parent().find('.tree-folder-content');

//        {*if(e.hasClass('icon-plus'))
//        {*}
    if (content.children().length > 1) {
        content.show();
    } else {
        getSelectedAllCategory(e);
    }
    //content.show();
    //getSelectedAllCategory(e);

//        {*}
//        else
//        {
//            //content.hide();
//            //e.removeClass('icon-minus').addClass('icon-plus');
//        }*}
}