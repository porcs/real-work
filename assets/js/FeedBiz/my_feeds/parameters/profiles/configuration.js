$(document).ready(function() 
{     
    
    $(document).on('click', '.showProfile', function(e){
		e.preventDefault();

		var cloned = $(this).closest('.row').next().clone(true).insertAfter($(this).closest('.row').next()).show();
                        
                cloned.parent().find('.removeProfile').on('click',function(e){
                    e.preventDefault();
                    var $ruleCheckBo = 	$('.ruleSettings').find('.checkBo').length;

                    $(this).closest('.showSelectBlock').remove();

                    countBlock($('.countProfile'), $('.showSelectBlock'));

                    $('.setBlockMain').addClass('hidden');

                    $('.setBlock').remove('');
                    
                    rulesRemoveSettings();
                });
                
		countBlock($('.countProfile'), $('.showSelectBlock'));
                
                cloned.attr('rel', $('.countProfile').html());                
                
		choSelect();		
	});
    
    $('form').submit(function(){
    	return $('div.error:visible').length === 0;
    });

	rulesAddSettings();

	$(document).on('click', '.showSet', function(e){
		e.preventDefault();
		var $this 				= $(this),
			$showSelectBlock 	= $this.closest('.showSelectBlock'),
			$ruleCheckBo 		= $showSelectBlock.find('.ruleSettings').find('.checkBo'),
			$addCheckBo 		= $showSelectBlock.find('.setBlock').last().find('.addCheckBo'),
			$rowCount 			= $ruleCheckBo.length,
			$choZN 				= $showSelectBlock.find('.ruleSettings').find('.choZn'),
			$addSelect 			= $showSelectBlock.find('.setBlock').last().find('.addSelect'),

			$templateBlock 	= '<div class="setBlock clearfix">'+
							'<div class="m-b10 clearfix">'+
								'<a href="" class="pull-right removeSet">Remove set <i class="cb-remove"></i></a>'+
							'</div>'+
							'<div class="setBlock_content">'+
								'<div class="setBlock_text">'+									
									
								'</div>'+
							'</div>'+
						'</div>',
			$templateText 	= '<div class="row addSettings">'+
								'<div class="col-sm-6 addCheckBo">'+

								'</div>'+
								'<div class="col-sm-6 addSelect">'+

								'</div>'+
							'</div>';
			choSelect();

			$showSelectBlock.find('.setBlockMain').removeClass('hidden');

			$($templateBlock).insertBefore($this.parent());

			$($templateText).appendTo($showSelectBlock.find('.setBlock').last().find('.setBlock_text'));
			
			$showSelectBlock.find('.setBlock').removeClass('hidden');

			for (var i=0; i < $rowCount; i++){
				$($templateText).appendTo($showSelectBlock.find('.setBlock').last().find('.setBlock_text'));
				
				$ruleCheckBo.eq(i).clone(true).appendTo($addCheckBo.eq(i));
				$choZN.eq(i).clone().appendTo($addSelect.eq(i));

				choSelect();
			}

			$showSelectBlock.find('.setSettings').show();

			$showSelectBlock.find('.setBlock').last().addClass('hidden');

			//countRule();
			if($this.closest('.profileBlock').find('.setBlockMain')){
				$this.closest('.profileBlock').find('.countRule').text($('.setBlock').length + $('.setBlockMain').length - 2);
			}
			else{
				$this.closest('.profileBlock').find('.countRule').text($('.setBlock').length);
			}
			
	});

	$(document).on('click', '.removeSet', function(e){
		e.preventDefault();
		var $this = $(this);
		
		if($this.closest('.profileBlock').find('.setBlockMain')){
			$this.closest('.profileBlock').find('.countRule').text($('.setBlock').length + $('.setBlockMain').length - 1);
		}
		else{
			$this.closest('.profileBlock').find('.countRule').text($('.setBlock').length);
		}
		
		$this.closest('.setBlock').remove();

		$this.closest('.setBlockMain').remove();

	});

	$(document).on('click', '.removeProfile', function(e){
		e.preventDefault();	
        remove(this);
	});
        
         //Remove
//    function remove(obj)
//    {
//        var name = $(obj).attr('rel');
//        if(name){
//        bootbox.confirm( $('#confirm-message').val() + ', "' + name + '" ?', function(result) {
//
//            if(result) 
//            {
//                var id = $(obj).val();
//
//                $.ajax({
//                    type: "get",
//                    url: base_url + "/my_feeds/parameters/profiles/configuration_delete/" +  id,
//                    success: function(rdata) 
//                    {
//                        if(rdata === "true")
//                        {
//                            $.gritter.add({
//                                title: $('#delete-success-message').val(),
//                                text: name + ' ' + $('#deleted-message').val(),
//                                class_name: 'gritter-light'
//                            });
//
//                            var id = parseInt($('#count').val()) - 1 ;
//                            $('#count').val(id);
//                               // $(obj).parent().parent().remove();
//                            var $ruleCheckBo = 	$('.ruleSettings').find('.checkBo').length;
//
//                            $(this).closest('.showSelectBlock').remove();
//
//                            countBlock($('.countProfile'), $('.showSelectBlock'));
//
//                            $('.setBlockMain').addClass('hidden');
//
//                            $('.setBlock').remove('');
//
//                            rulesRemoveSettings();
//                               
//                        }
//                        else
//                        {
//                            $.gritter.add({
//                                title: $('#error-message').val(),
//                                text: name + ", " + $('#unsuccess-message').val(),
//                                sticky: false,
//                                class_name: 'gritter-error'
//                            });
//                        }
//                        
//                       
//                    },
//                    error: function(jqXHR, textStatus, errorThrown) 
//                    {
//                        $.gritter.add({
//                            title: $('#error-message').val(),
//                            text: textStatus + " " + errorThrown,
//                            sticky: false,
//                            class_name: 'gritter-error'
//                        });
//                    }
//                });
//            }
//        });
//        }
//    }
        

	countBlock($('.countProfile'), $('.showSelectBlock'));

	$(document).on('click', '.withIcon .cb-plus.bad', function(){
		$(this).closest('.row').remove();
	});

	$(document).on('click', '.withIcon .cb-plus', function(){
		countBlock($('.count'), $('.showSelectBlock'));
	});

	$(document).on('click', '.choZn .cb-plus.good', function(){
		$(this).closest('.row').clone(true).insertBefore($(this).closest('.row'));
		$(this).removeClass('good').addClass('bad');
	});

	$(document).on('click', '.choZn .cb-plus.bad', function(){
		$(this).closest('.row').remove();
	});
        
       /* == Old Code == */
       
       $('input[rel="is_default"]').on('change', function(){ 
        if($(this).is(':checked')){
           $('input[type="radio"]').not(this).each(function(){
              $(this).parent().bootstrapSwitch('setState', false);
           });
        }
    });
        
 //if (typeof $.fn.select2 != 'undefined'){
//    {*$("#id_profile_price").select2({
//        placeholder: "Select a Price",
//            allowClear: false
//    });
//                
//    {*$("#id_rule").select2({
//        placeholder: "Select a Rule",
//            allowClear: false
//    });*}
//    $("#id_profile_price2").select2({
//            placeholder: "Select a Price",
//                allowClear: false
//        });
//    }
    
    //default profile
    var $unique = $('input.default_profile');
    $unique.click(function() {
        $unique.removeAttr('checked');
        $(this).prop('checked', true);
    });

    //help-inline
    $('<style> @media only screen and (max-width: 767px){ .help-inline, .input-icon+.help-inline { display: none !important; } }</style>').appendTo('head');

    var duplicate= $('#count').val();
    var i=0;
    var colors = ["#999", "#9ecf6a", "#dd56c0", "#f89406", "#3a87ad", "#eccb71","#e05656","#abbac3", "#569add"];

    //colors
//    $('#tasks li').each(function()
//    {
//        if(i === colors.length)i=0;
//        $(this).css( "border-left-color", colors[i]);
//        i++;
//    });

//    $(document).on('blur', 'input[rel="name"]', function()
//    {
//        if($(this).val() === "")
//        {
//            var control_group = $(this).parent().parent().parent().find('.form-group');
//            control_group.css('opacity', '0.5');
//            control_group.find('select').prop('disabled', true).chosen().trigger("chosen:updated")//.select2("enable", false);
//            control_group.find('button').prop('disabled', true);
//
//            $(this).parent().find('span.help-inline').show().addClass('inline');
//            $(this).parent().parent().addClass('error').css('opacity', '1');;
//        }
//    });

    $(document).on('keyup', 'input[rel="name"]', function()
    {
        $(this).parent().find('span.help-inline').hide().removeClass('inline');
        $(this).parent().parent().removeClass('error');
        var control_group = $(this).parent().parent().parent().find('.form-group');
        control_group.css('opacity', '1');
        control_group.find('select').prop('disabled', false).chosen().trigger("chosen:updated");//.select2().select2("enable", true);
        control_group.find('button').prop('disabled', false);
    });

    $(document).on('change', 'input[rel="name"]', function()
    {
        var profile_content = $(this).parent().parent().parent() ;
        var profile_name = $(this).val() ;

        profile_name = profile_name.replace(/[^a-zA-Z0-9\s_]/g,'').replace(/\s/g,'_');

        if($('input[name="profile[' + profile_name + '][name]"]').length !== 0)
        {
            profile_name = profile_name + '_' + duplicate;
            duplicate++;                    
        } 

        profile_content.find('select, input, textarea').each(function() 
        {
            if ( ! $(this).attr('rel') )
                return ;

            var field_name = $(this).attr('rel') ;
            var variable_prefix = 'profile[' + profile_name + '][' + field_name + ']' ;

            $(this).attr('name', variable_prefix) ;
            $(this).attr('data-content', profile_name) ;
        });           

        validateProfileName(this);
    });

     //Change Rule name
    $(document).on('change', 'select[rel="id_rule"]', function()
    {
        ruleName($(this));
    });

    //Add Rule
    $(document).on('click', '.add-rule', function()
    {
        addRule($(this));
    });

    //Remove
    function remove(obj)
    {
        var name = $(obj).attr('rel');
        if(name){
        bootbox.confirm( $('#confirm-message').val() + ', "' + name + '" ?', function(result) {

            if(result) 
            {
//                var id = $(obj).val();
            	var id = $(obj).parent().find('input:hidden[rel="id_profile"]').val();

                $.ajax({
                    type: "get",
                    url: base_url + "/my_feeds/parameters/profiles/configuration_delete/" +  id,
                    success: function(rdata) 
                    {
                        if(rdata === "true")
                        {
                        	$(obj).parent().remove();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) 
                    {
                        $.gritter.add({
                            title: $('#error-message').val(),
                            text: textStatus + " " + errorThrown,
                            sticky: false,
                            class_name: 'gritter-error'
                        });
                    }
                });
            }
        });
        }
    }

    $('.remove').click(function(){
        remove($(this));
    });

    function ruleName(e)
    {
        e.attr('name', 'profile['+ e.attr('data-content') +'][rule]['+e.find('option:selected').val()+'][id_rule]');
        console.log(e.parent().find('input[rel="id_profile_item"]').attr('name', 'profile['+ e.attr('data-content') +'][rule]['+e.find('option:selected').val()+'][id_rule]'));
    }

    function addRule(obj)
    {
        //add-rule
        var li =  $(obj).parents('.showSelectBlock');
        var rel = li.attr('rel');
        var cloned = $(obj).parent().next().clone().appendTo($('div[rel="'+ rel +'"]').find('.rule-item')).removeClass('main-item').show();
        //cloned.css('margin-top', '10px');

        // Remove used options
        var selected_select = cloned.find('select');

        $(obj).parents('div[rel="'+ rel +'"]').find('select option:selected').each(function( index, option ) 
        {
            selected_select.find('option[value="' + option.value + '"]').remove() ;
            selected_select.find('option:first').attr('selected','selected');
        });
        if ( !cloned.find('option').length )
        {
            cloned.remove();
            var validate_yellow = $('.validate.yellow:first');
            var validate_yellow_clond = validate_yellow.parents('.row').clone();
            validate_yellow_clond.on('click','.fa-remove',function(){ $(this).parent().parent().parent().parent().parent().animate({ opacity: 0 }, { easing: 'swing',duration: 1000,complete: function(){ $(this).remove(); } }); });
            validate_yellow_clond.appendTo($('div[rel="'+ rel +'"]').find('.rule-item')).hide().show('slow');
            $('.validate.yellow').parent().parent().animate({ opacity: 100 }, { easing: 'swing',duration: 1000,complete: function(){ /*$(this).remove();*/ } });
            return(false) ;
        }else{
            console.log(selected_select.parent().find('.search-select').length);
            // Add Libraly Select2
            //selected_select.parent().remove();
            var countDiv = selected_select.parent().find('.search-select').length;
            if(countDiv > 0){
                for(var i=0;i<countDiv;i++){selected_select.parents('.rule-item').find('select:first').remove();}
            }
            
//            selected_select.select2({
//                    placeholder: "Select a Rule",
//                    allowClear: true
//            });

            //selected_select.chosen();
            selected_select.addClass('search-select');
            $(".search-select").chosen({
		disable_search_threshold: 1,
		no_results_text: "Nothing found!"
            }).trigger("chosen:updated");	

	$('.chosen-single').find('div').addClass('button-select');
	$('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
        }

        //Name
        var data_content ='';

        if(selected_select.attr('data-content'))
            data_content = selected_select.attr('data-content');
        else
        {
            data_content= $(obj).parents('div[rel="'+ rel +'"]').attr('rel');
            selected_select.attr('data-content',data_content);
        }

        selected_select.attr('name', 'profile['+ data_content +'][rule]['+selected_select.find('option:first').val()+'][id_rule]');

        //Change Name
        cloned.find('select[rel="id_rule"]').change(function(){
            ruleName($(this));
        });

        cloned.find('.remove-rule').show().click(function(){
            $(this).parent().parent().remove();                
        });
    }

    //Remove
    function removeRuleItem(obj)
    {
        var name = $(obj).attr('rel');
        
        bootbox.confirm( $('#confirm-message').val() + ', "' + name + '" ?', function(result) 
        {
            if(result) 
            {
                var id = $(obj).attr('value');//$(obj).val();
                $.ajax({
                    type: "get",
                    url: base_url + "/my_feeds/parameters/profiles/configuration_item_delete/" +  id,
                    success: function(rdata) 
                    {
                        console.log(rdata);
                        if(rdata === "true")
                        {
                            $.gritter.add({
                                title: $('#delete-success-message').val(),
                                text: '"' + name + '", ' + $('#deleted-message').val(),
                                class_name: 'gritter-light'
                            });
                            
                            $(obj).parent().parent().remove();
                        }
                        else
                        {
                            $.gritter.add({
                                title: $('#error-message').val(),
                                text: '"' + name + '", ' + $('#unsuccess-message').val(),
                                sticky: false,
                                class_name: 'gritter-error'
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) 
                    {
                        $.gritter.add({
                            title: $('#error-message').val(),
                            text: textStatus + " " + errorThrown,
                            sticky: false,
                            class_name: 'gritter-error'
                        });
                    }
                });
            }
        });
    }
    
    function validateProfileName(element){		
        var errorFound = false;
        if(jQuery.trim($(element).val()) === ''){
        	errorFound = true;
        	jQuery('[rel="err_rule_name_blank"]', $(element).parent()).removeClass('hide');
        	jQuery('[rel="err_rule_name_duplicate"]', $(element).parent()).addClass('hide');
        }
        else if(jQuery('input[rel="name"]').filter(function(){ return jQuery.trim($(element).val()) === jQuery.trim($(this).val()); }).length > 1){
        	errorFound = true;
        	jQuery('[rel="err_rule_name_blank"]', $(element).parent()).addClass('hide');
        	jQuery('[rel="err_rule_name_duplicate"]', $(element).parent()).removeClass('hide');
        }
        
        if(errorFound){
            $(element).focus();
            $(element).parent().addClass('has-error').find('div.error').removeClass('hide');
            return false;
        }else{ 
            $(element).parent().removeClass('has-error').find('div.error').addClass('hide');
            return true;
        }
	}

    $('.remove-rule').click(function(){
        removeRuleItem($(this));
    });
        
});
