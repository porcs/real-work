
$(document).ready(function() {
    
    var table = $('#my_prod_list').DataTable( {
        "ajax": base_url+'my_feeds/my_product_data', 
        "columns": [
            {
                "className":      'p_id',
                "data":           "cond",
                "defaultContent": '',"width": "5%","orderable": false
            },
            { "className":      'p_img',"data": "pid","width": "40px" ,"orderable": true},
            
            { "className":      'p_name',"data": "name","width": "50%" },
            { "className":      'p_variant',"data": "search","width": "20%" ,"orderable": false},
            { "className":      'p_price',"data": "price","width": "40px" ,"orderable": true},
            { "className":      'p_qty',"data": "qty","width": "40px" ,"orderable": true}
        ],
        "order": [[1, 'asc']],
        "fnRowCallback": function( nRow, aData, iDisplayIndex ) { 
             var row = table.row( nRow );
            function product_detail(data){
                    var out = '<table cellpadding="5" cellspacing="0" border="0" class="prod_sku">';  
                    var i=0;
                    $.each(data.items,function(k,d){  
                        out+=
                            '<tr>'+
                            '<td class="p_id" >&nbsp;</td>'+
                            '<td class="p_img">&nbsp;</td>'+
                            '<td class="p_name"><i class="fa fa-reply lb"></i> '+d.dref+'</td>'+
                            '<td class="p_variant">'+d.atr+'</td>'+
                            '<td class="p_price">'+d.price+'</td>'+
                            '<td class="p_qty">'+d.qty+'</td>' 
                            +'</tr>'
                            ; 
                        i++;
                    }); 
                    out+='</table>';
                    return out;
            }
            var image = aData.img
            if(jQuery.trim(image)!=''){
                $('td:eq(1)', nRow).html('<a href="'+image+'"><img src="'+image+'" style="height:30px"></a>');
                 $('td:eq(1)', nRow).find('a').colorbox();
            }else{
                $('td:eq(1)', nRow).html('');
            }   
            
            $('td:eq(0)', nRow).html('<input class="sel_row" type="checkbox" rel="'+aData.pid+'">');
            $('td:eq(3)', nRow).html('');
            $('td:eq(4)', nRow).html('');
            $('td:eq(5)', nRow).html('');
            var ch = row.child('', 'no-padding ch_row c_'+iDisplayIndex) ;
            var txt = product_detail(aData);
            ch.show();
            setTimeout(function(){ 
                $('#my_prod_list').find('td.c_'+iDisplayIndex).html(txt);
                $('td:eq(2),td:eq(3),td:eq(4),td:eq(5)', nRow).unbind('click').click(function(){
                    $(this).parent().find('.sel_row').click();
                 });
            },1);
            
          }
    });
    $.ajax({
    url:base_url+'my_feeds/getJsonCategory',dataType:'json'
    ,success:function(data){
        
        $.each(data,function(k,d){
            var id = d.id;
            var name ='';
            var tab = d.lv;
            for(var i=0; i<tab; i++){
                name += "&nbsp&nbsp&nbsp&nbsp";
            }
            if(d.name){
                var namex = '';
                $.each(d.name,function(l,n){
                    namex = n;
                });
                name+=namex;
            }else{
                return;
            }
            var opt = "<option value=\"'"+id+"'\">"+name+"</option>";
            $("#sel_category").append(opt);
            
        })
        $("#sel_category").select2({
                allowClear: true,width: 'resolve'
        }).on("change", function(e) {
          // mostly used event, fired to the original element when the value changes
          table.column(3).search( e.val , false, false ).draw();
        });
        $("#sel_condition").select2({
                allowClear: true,width: 'resolve'
        }).on("change", function(e) {
          // mostly used event, fired to the original element when the value changes
          table.column(0).search( e.val).draw();
        });
        
    }
    });
    $('#sel_all').click(function(){
            var status = $(this).attr('checked');
            $('#my_prod_list').find('.sel_row').each(function(){
                if(status){
                    $(this).attr('checked',status);
                }else{
                    $(this).attr('checked',false);
                }
            })
    });
    
});