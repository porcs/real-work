$(function() {        
        $('#shopform').validate({
            errorElement: 'span',
            errorClass: 'help-inline error',
            focusInvalid: false,
            rules: { 
                mode:{
                    required: true
                }
            },
            messages: {
                mode: {
                    required: $('#please_choose_feed_source')
                }
            },
            submitHandler: function(form) 
            {
                var checked = $('input[rel="checked"]');
                var check = $('input[name="id_shop"]');
                
                bootbox.confirm($('#q1').val(), function(result) 
                {
                    if(result) 
                    {
                        ////set_default_shop_product
                        $.ajax({
                            type: "post",
                            url: base_url+"feed_biz_shop/set_default_shop_product",
                            data: $('#shopform').serialize(),
                            success: function(data) 
                            {
                                if(data === "true")
                                {
                                    $.ajax({
                                        type: "post",
                                        url: base_url+"feed_biz_shop/set_default_shop_offer",
                                        data: $('#shopform').serialize(),
                                        success: function(rdata) 
                                        {
                                            if(rdata === "true")
                                               location.reload();
                                            else
                                            {
                                                $.gritter.add({
                                                    title: $('#Error').val(),
                                                    text: $('#Cannot_change_shop_offer').val(),
                                                    sticky: false,
                                                    time: '',
                                                    class_name: 'gritter-error'
                                                });

                                            }      
                                        },
                                        error: function(jqXHR, textStatus, errorThrown) 
                                        {
                                            console.log(jqXHR, textStatus, errorThrown);
                                        }
                                    });
                                }
                                else
                                {
                                    $.gritter.add({
                                        title: $('#Error').val(),
                                        text: $('#Cannot_change_shop_offer').val(),
                                        sticky: false,
                                        time: '',
                                        class_name: 'gritter-error'
                                    });

                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) 
                            {
                                $.gritter.add({
                                    title: $('#Error').val(),
                                    text: textStatus + ' ' + errorThrown,
                                    sticky: false,
                                    time: '',
                                    class_name: 'gritter-error'
                                });
                            }
                        });
                    }
                    else
                    { 
                        console.log(checked);
                        check.prop('checked', false);
                        checked.prop('checked', true);
                    }
                });
            }
        });
            
        //set shop default
        $('#shopform').on('click', '.shopbox', function(){
        
            var check = $(this).find('input[name="id_shop"]');
            if(!check.is(':checked'))
            {
                $('#shopform input[name="id_shop"]').prop('checked', false);
                check.prop('checked', true);
            }
        });
        
        //get offer
//        {*$.ajax({
//            url: '{$base_url}/feed_biz_shop/get_default_shop_offer',
//            type: 'get',
//            contentType: "application/json",
//            //timeout: 5000,
//            cache: false,
//            success: function(datas) {
//                //console.log(datas);
//                var data = $.parseJSON(datas);
//                
//                if(data.pass)
//                {
//                    $('.shop').each( function(){
//                        var shop = $(this);
//                        $.each(data.offer_shop, function(i,j) {
//                            
//                            if(shop.text() === j.name)
//                            {
//                                var li = shop.parent().parent().find(".offer").show();
//                                li.find('span[rel="description"]').append(j.description);
//                                li.find('span[rel="date"]').append(j.date_add);
//                            }
//                        });
//                      
//                    });
//                }
//                else
//                {
//                    $("#success").hide();
//                    $("#error").show();
//                    $("#error").html(data.error);
//                }
//            },
//            error: function(jqXHR, textStatus, errorThrown) 
//            {
//                $("#loading").hide();
//                $("#error").show();
//                $("#error").html(jqXHR, textStatus, errorThrown);
//                console.log('Return Error : ' + errorThrown);
//            }
//
//        });*}
        
    });