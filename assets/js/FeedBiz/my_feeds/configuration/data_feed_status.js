$(function() {
    
    $('#import-history').dataTable({       
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "asStripeClasses": [''],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "my_feeds/import_history_page/",
            "type": "POST"
        },
        "columns": [  
            { "data": "history_table_name", "bSortable": true, "width": "30%", "className": 'm-l10'  },
            { "data": "history_action", "bSortable": true, "width": "20%", "className": 'm-l10'  },
            { "data": "user_name", "bSortable": true, "width": "20%", "className": 'm-l10'  },
            { "data": "history_date_time", "bSortable": true, "width": "30%", "className": 'text-left m-r10' },           
        ],       
        "bFilter" : true,
        "paging": true,
        "pagingType": "full_numbers" ,  
        "order": [[3, 'desc']],       
    } ); 
    
    /*
     * ,
             "success" : function(json) {
               console.log(json);
            }
     */
    
    $('#mode').validate({
        errorElement: 'span',
        errorClass: 'help-inline error',
        focusInvalid: false,
        rules: { 
            mode:{
                required: true
            }
        },
        messages: {
            mode: {
                required: $('#status-message').val()
            }
        },
        submitHandler: function(form) 
        {
            var checked = $('input[rel="checked"]');
            var check = $('input[name="mode"]');

            if($('input[name="mode"]:checked').val() !== checked.val())
            {
                bootbox.confirm($('#status-confirm').val(), function(result) 
                {
                    if(result)
                    {
                        form.submit();
                    }
                    else
                    {
                        check.prop('checked', false);
                        checked.prop('checked', true);
                    }
                    
                });
            }
        }
    });

    //Mode
    $('#mode').on('click', '.modebox', function(){

        var check = $(this).find('input[name="mode"]');

        $('#mode input[name="mode"]').prop('checked', false);
        check.prop('checked', true);

    });

});