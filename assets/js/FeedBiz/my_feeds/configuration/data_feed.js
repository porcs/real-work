    var count=1;
    var action_time = '300';
    var finish_txt = '';
    var running_txt ='';
    function callback_after_done_process(){
        finish_txt = $('body').find('#all-process-complete').val();
        if($('body').find('button.btn_import.disabled').length>0){
            //$('body').find('#import_feed_msg .msg').html(finish_txt);
            //$('body').find('#import_feed_msg').slideDown(action_time);
            $('body').find('button.btn_import.disabled').each(function(){
               $(this).removeClass('disabled') ;
            });
        }
    }
    function callback_when_running_process(){
      //console.log('run call back');
//        var running_txt = $('body').find('#some-process-running').val();
//        if($('body').find('button.btn_import.disabled').length==0){
//            //$('body').find('#import_feed_msg .msg').html(finish_txt);
//            //$('body').find('#import_feed_msg').slideDown(action_time);
//            $('body').find('button.btn_import').each(function(){
//               $(this).addClass('disabled') ;
//               $('body').find('#import_feed_msg .msg').html(running_txt);
//                $('body').find('#import_feed_msg').slideDown(action_time);
//            });
//        }
        $('body').find('#import_feed_msg').slideUp(action_time);
    }

$(document).ready(function() { 
    
//    var base_url = location.origin;
    
//    function feed_data( feed_source, type, user_data )
//    {
//        $.ajax({
//            url: base_url + ':3001/import/' + feed_source + '/' +  type + '/' + encodeURIComponent(user_data),
//            type: 'GET',
//            contentType: "application/json",
//            cache: false,
//            async:false
//        });  
//
//    }
//    var running_feed_data = false;
//    function feed_data(user_data){
//        if(running_feed_data) return;
//        running_feed_data=true;
//        $.ajax({ 
//            url: base_url + ':3001/import_feed/' + encodeURIComponent(user_data),
//            type: 'GET',
//            //contentType: "application/json",
//            cache: false,
//            async:true,timeout:0
//            ,success:function(data){
////                running_feed_data = false;
////                console.log(data);
//                $.ajax({
//                    type: "get",
//                    url: base_url + "/users/get_default_shop", 
//
//                success: function(id_shop) 
//                {
//                    if(id_shop!=''){
//                        $('ul#parameter_sidebar').addClass('imported');
//                        var par = $('ul#parameter_sidebar').parent();
//                        $(par).find('a.dropdown-toggle').click();
//                    }
//
//                }});
//            }
//        });  
//    }
    
     
    $('button.btn_import').click(function(){
		var $obj = $(this);
		bootbox.confirm( $('#confirm-message').val(), function(result) {
		if(result){
			var finish_txt = $('#all-process-complete').val();
			var running_txt = $('#some-process-running').val();
			var type = $obj.attr('rel');
			var other = $obj.siblings('button');
			if($('#import_feed_msg').hasClass('alert-success')){
				$('#import_feed_msg').removeClass('alert-success').removeClass('alert-danger').addClass('alert-warning');
			}
			if($obj.hasClass('disabled')){
				$('#import_feed_msg .msg').html(running_txt);
				$('#import_feed_msg').slideDown(action_time);
				$('.notification').each(function(){ if($obj.css('display') != 'none'){ $obj.animate({ opacity: 0 }, 'slow'); } });
				$('.notification.good.ajax_notify').show().css({ opacity: 0 }).animate({ opacity: 100 }, 'slow').find('p').html(running_txt);
				return;
			}else{
				$('#import_feed_msg .msg').html(running_txt);
				$('#import_feed_msg').slideDown(action_time);
				$('.notification').each(function(){ if ($obj.css('display') != 'none'){ $obj.animate({ opacity: 0 }, 'slow'); } });
				$('.notification.good.ajax_notify').show().css({ opacity: 0 }).animate({ opacity: 100 }, 'slow').find('p').html(running_txt);
			}
			$('button.btn_import').each(function(){
				$obj.addClass('disabled');
			});        
	//        other.each(function(){
	//            $(this).hide(action_time);
	//        });
			
			$.ajax({
				type: "get",
				//url: base_url + "/my_feeds/data_feed", 
				url: base_url + "/tasks/ajax_run_data_feed/"+type,
				dataType: 'json',
				timeout:0,
				success:function(r){
		//                running_feed_data = false; 
				if(r.status){
					var fail = 0;
					var err_msg = '';
					var err_code='';
					if(r.result){
						var res = JSON.parse(r.result);
						res = JSON.parse(res);
	//                    console.log(res);
						$.each(res,function(k,v){
	//                        console.log(v);
							if(v == null || v == undefined || v.pass == false){
								fail++;
								err_msg = v.error;
							}
						})
					} 
					if(fail> 0){
						$.ajax({
							type: "get",
							url: base_url + "/users/clr_history"
						});
						$('body').find('button.btn_import.disabled').each(function(){
						   $obj.removeClass('disabled') ;
						}); 
						if(err_msg && err_msg!==''){
							var msg_lang = $("input.cus_msg[rel='"+ err_msg +"']");
							var err_msg_lang = $("input[err_code='"+ err_msg +"']");
							if(err_msg_lang.length==0){
								var err_msg_lang = $("input[id='"+ err_msg +"']");
							}
							
							if(err_msg_lang.length!=0){
								msg_lang = err_msg_lang;
								$('.btn_ver').removeClass('verified'); 
								$('input[name=base_url]').change();
							}
							var is_text = false;
							if(err_msg.length>20){
								var msg_lang2 =err_msg;
								is_text=true;
							}else{
								var msg_lang2 = $('body').find("#"+ err_msg);
							}
							if(msg_lang.length != 0){
								var msg = $('#import_feed_msg .msg').fbError() ; 
								err_msg = msg_lang.val();
								if(typeof msg_lang.attr('err_code')  !== typeof undefined){
									msg.append_icon_help(msg_lang.attr('err_code'));
									err_code=msg_lang.attr('err_code');
									console.log(msg_lang.attr('err_code'));
								}
								if(err_msg.indexOf('_') !== -1){
									if($('#'+err_msg).length == 1){
										err_msg = $('#'+err_msg).val();
									}
								}
							}
							else if(msg_lang2.length != 0 && !is_text){
								err_msg = msg_lang2.val();
							}else if(msg_lang2.length != 0 && is_text){
								err_msg = msg_lang2;
							}else{
								err_msg = err_msg;
							} 
							
							notification_alert(err_msg,'bad',err_code);
							 
	//                        $('#import_feed_msg .msg').html(err_msg);
	//                         
	//                        $('#import_feed_msg').removeClass('alert-warning').removeClass('alert-success').addClass('alert-danger').slideDown(action_time);
						}
					}else{
						 //$('#import_feed_msg .msg').html(finish_txt);
						 
						 notification_alert(finish_txt);
						$('#import_feed_msg').removeClass('alert-warning').removeClass('alert-danger').addClass('alert-success').slideDown(action_time);
						$.ajax({
							type: "get",
							url: base_url + "/users/get_default_shop", 

						success: function(id_shop) 
						{
							if(id_shop!=''){
								$('ul#parameter_sidebar').addClass('imported');
								var par = $('ul#parameter_sidebar').parent();
								$(par).find('a.dropdown-toggle').click();
							}

						}});
						if($('#popup_status').length>0){
							var step = $('#popup_status').val()*1;
							step++;
							setTimeout(function(){
								//window.location.href = base_url + "/my_feeds/parameters/offers/price/"+step;
								window.location.href = base_url + "/my_feeds/parameters/profiles/category/"+step;
								
							},3000);
						}
					}
				}else{
					if(r.txt){
						notification_alert(r.txt,'warning');
	//                    $('.notification').each(function(){ if($(this).css('display') != 'none'){ $(this).animate({ height: 0, opacity: 0 }, 'slow'); } });
	//                    $('.notification.good.ajax_notify').show().css({ height:0, opacity: 0 }).animate({ height:36,opacity: 100 }, 'slow').find('p').html(r.txt);
	//                    $('#import_feed_msg .msg').html(r.txt);
	//                    $('#import_feed_msg').slideDown(action_time);
						setTimeout(function(){
							window.location.reload();
						},60000)
					}
				}
				
				},
				error: function(xhr, ajaxOptions, thrownError){
					console.log(xhr.responseText);
					setTimeout(function(){
						window.location.reload();
					},60000);
				}
			});
        
			}
		});
    });
});


		
			
        