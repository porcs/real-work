$(document).ready(function() 
{ 
     
    /*$('#history_logs').dataTable({
        "aoColumns": [
            null, null, null, null],
        "aaSorting": [[3, "desc"]]
    });*/
    
    var date = new Date();
    var utcOffset = -date.getTimezoneOffset()/60;
    $('body').find('#cli_time_zone').val(utcOffset);
   

    if (typeof $.fn.select2 != 'undefined')
    {
        $("#language").select2({
            placeholder: "Select a State",
            allowClear: true
        });


        $("#language2").select2({
            placeholder: "Select a State",
            allowClear: true
        });
    }
    
    $(".chzn-select").chosen();
    var act_time = 500;
    /*$('#feed_type_product').on('click' , function(){
        if($("#feed_type_product").is(':checked'))
            $("#feed_product").slideDown(act_time);  // checked
        else
            $("#feed_product").slideUp(act_time);  // unchecked
    });

    $('#feed_type_offer').on('click' , function(){
        if($("#feed_type_offer").is(':checked'))
            $("#feed_offer").slideDown(act_time);  // checked
        else
            $("#feed_offer").slideUp(act_time);  // unchecked
    });*/

    $('input[name="feed_source"]:checked').each(function(){
        $('.content').slideUp(act_time);
        $(this).parent().parent().find('.content').slideDown(act_time);
    });

    $('input[name="feed_source"]').on('click', function(){
        $('.content').slideUp(act_time);
        $(this).parent().parent().find('.content').slideDown(act_time);
    });

    $('input[name="source"]').on('click' , function(){
        $('input[name="source"]').parent().parent().removeClass('header-color-blue');
        $('input[name="source"]').parent().parent().parent().addClass('collapsed');
        $(this).parent().parent().addClass('header-color-blue');
        $(this).parent().parent().parent().removeClass('collapsed');
    });

    $('input[name="feed_type"]').on('click', function(){
        $('.feed-type-content').slideUp(act_time);
        $(this).parent().parent().find('.feed-type-content').slideDown(act_time);
    }); 

    $('div#language_chzn').css('min-width', '65%');

    $("#language").on('change', function (e) {
        $("#iso").val($("#language option:selected").attr('data-iso'));
        $("#curr_name").val($("#language option:selected").attr('data-curr-name'));
        $("#curr_iso").val($("#language option:selected").attr('data-curr-iso'));
    });
    var just_update = false;
    $('input[name=base_url]').on('change',function(){
        //console.log(just_update,$(this).val());
        if(just_update!==false && just_update == $(this).val()){ just_update = false;return;}
        var obj = $('.btn_ver');
        if(obj.hasClass('verified')){
            $(".icon_ver",obj).addClass('fa-remove').removeClass('fa-check').show(act_time);
            $(".icon_chk",obj).hide(act_time);
            obj.removeClass('verified').removeClass('btn-success').addClass('btn-error');
             $(".txt_ver",obj).html($('#verify').val());
             $('input[name=verified]').val('');
            $('.btn_import').hide(act_time);
         }else{
            $(".icon_ver",obj).removeClass('fa-remove').removeClass('fa-check').show(act_time);//.removeClass('bigger-160').addClass('icon-warning-sign')
            $(".icon_chk",obj).hide(act_time); 
            obj.removeClass('verified').removeClass('btn-success').addClass('btn-error').addClass('btn-verify');
            $(".txt_ver",obj).html($('#error_connection').val());
            var btn = $( obj).fbError() ;  
            if($('#check').attr('err_code')){
                btn.append_icon_help($('#check').attr('err_code'));
            } 
            $('input[name=verified]').val('');
         }
        
    });
    $('input[name=base_url]').on('paste',function(){
       //console.log(just_update);
       setTimeout(function(){
           if($('input[name=base_url]').val()==''){return;}
           $('.btn_ver').click(); if(!$('.btn_ver').hasClass('verified'))just_update = $('input[name=base_url]').val();
       
       },500);
    });
    
    var txt = $('#empty_url').val();
    if($('input[name=base_url]').val()==''){
        $('input[name=base_url]').val(txt).css('color','#3a87ad');
    }
    $('input[name=base_url]').on('focus',function(){
         if($('input[name=base_url]').val()==txt){
             $('input[name=base_url]').val('').css('color','#393939');
         }
    })
    $('.btn_ver').on('click',function(){
        
        if($(this).hasClass('verified'))return;
        if($(this).hasClass('waiting'))return;
        var url_val = $('input[name=base_url]').val();
        if(jQuery.trim(url_val) == ''){
            $('input[name=base_url]').val(txt).css('color','#3a87ad');
            return;
        }
        
        var obj = $(this);
        $(this).addClass('waiting').removeClass('btn-error');
        $(".icon_ver",this).hide(act_time);
        $(".icon_chk",this).show(act_time);
        $(".txt_ver",this).html($('#verifying').val());
        var btn = $( obj).fbError() ;  
          $.ajax({
            type: "post",
            data: {url:url_val },
            url: base_url+ "my_feeds/verify_feed",
            success: function(data){
//                console.log(data);
                
                if(data === 'verified'){
                    btn.remove_icon_help();
                    $(".icon_ver",obj).removeClass('fa-remove').addClass('fa-check').show(act_time);//.removeClass('icon-warning-sign')
                    $(".icon_chk",obj).hide(act_time);
                    obj.addClass('verified').removeClass('btn-error').removeClass('btn-verify').addClass('btn-success');
                    $(".txt_ver",obj).html($('#verify').val());
                    $('input[name=verified]').val(data);
                    if($('#popup_status')){
                        $('#mainconfigform').submit();
                    }
                }else{
//                    data = data + ' ' + $('#check').val();
                    $(".icon_ver",obj).removeClass('fa-remove').removeClass('fa-check').show(act_time);//.removeClass('bigger-160').addClass('icon-warning-sign')
                    $(".icon_chk",obj).hide(act_time);
                    obj.removeClass('verified').removeClass('btn-success').addClass('btn-error').addClass('btn-verify');
                    $(".txt_ver",obj).html($('#error_connection').val());
                    var err_code = '';
                    if($('#check').attr('err_code')){
                        btn.append_icon_help($('#check').attr('err_code'));
                        err_code = $('#check').attr('err_code');
                        console.log('xxx',err_code);
                    } 
                    $('input[name=verified]').val('');
                    notification_alert($('#error_connection').val(),'bad',err_code);
                }
                obj.removeClass('waiting');
            },
            error:function(xhr, status, error){
                console.log(error);
                var data= $('#error_connection').val();
                $(".icon_ver",obj).removeClass('fa-remove').removeClass('fa-check').show(act_time);//.removeClass('bigger-160').addClass('icon-warning-sign')
                $(".icon_chk",obj).hide(act_time);
                obj.removeClass('verified').removeClass('btn-success').removeClass('btn-error').addClass('btn-verify');
                $(".txt_ver",obj).html($('#error_connection').val());
                $('input[name=verified]').val('');
                obj.removeClass('waiting');
                var err_code = '';
                if($('#error_connection').attr('err_code')){
                    btn.append_icon_help($('#error_connection').attr('err_code'));
                    err_code = $('#error_connection').attr('err_code');
                    console.log('xxx',err_code);
                } 
                notification_alert($('#error_connection').val(),'bad',err_code);
            }
        });
    });
    
    $('input#input_standard').click();
});    