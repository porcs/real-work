$(function() {
    $('#mode').validate({
        errorElement: 'span',
        errorClass: 'help-inline error',
        focusInvalid: false,
        rules: { 
            mode:{
                required: true
            },
        },
        messages: {
            mode: {
                required: $('#general-message').val()
            },
        },
        submitHandler: function(form) 
        {
//            var checked = $('input[rel="checked"]');
//            var check = $('input[name="mode"]');

//            if($('input[name="mode"]:checked').val() !== checked.val())
//            {
//                bootbox.confirm($('#general-confirm').val(), function(result) 
//                {
//                    if(result)
//                    {
                        form.submit();
//                    }
//                    else
//                    {
//                        check.prop('checked', false);
//                        checked.prop('checked', true);
//                    }
//                    
//                });
//            }
        }
    });

    //Mode
    $('#mode').on('click', '.modebox', function(){

        var check = $(this).find('input[name="mode"]');

        $('#mode input[name="mode"]').prop('checked', false);
        check.prop('checked', true);

    });
        
    $('input[name="token"]').focus(function(){
        $(this).get(0).type = 'text';
    });
    $('input[name="token"]').focusout(function(){
        $(this).get(0).type = 'password';
    });
    
    $('#save_data').click(function(){
        event.preventDefault();
        $('#mode').submit();
    });
});