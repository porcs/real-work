$(function() {
    
    //initial upgrade_mode
    $('#upgrade_mode').on('shown.bs.modal', function() {
        if($('#upgrade_mode iframe').attr('src').length == 0){
            $('#upgrade_mode iframe').attr('src',$('#upgrade_mode iframe').attr('data-url'));
        }
    });
    
    //set shop default
    $('input[name="id_shop"]').on('click', function(){
        
       var idShop = $(this).val();
       console.log(idShop);
        //$('#ace-settings-box').removeClass('open');
        $('#shop-default').html('<i class="icon-spinner icon-spin bigger-125"></i> '+$txt_loading+'..');
        
        //set_default_shop_product
        $.ajax({
            type: "post",
            url: base_url+"feed_biz_shop/set_default_shop_product/" + idShop,
            success: function(data) 
            {
                console.log(data);
                if(data === "true")
                {
                    $.ajax({
                        type: "post",
                        url: base_url+"feed_biz_shop/set_default_shop_offer/" + idShop,
                        success: function(rdata) 
                        {
                            console.log(rdata);
                            if(rdata === "true")
                               location.reload();
                            else
                            {
                                $.gritter.add({
                                    title: $txt_error+'!',
                                    text: $txt_cannot_change_shop_offer,
                                    sticky: false,
                                    time: '',
                                    class_name: 'gritter-error'
                                });

                            }      
                        },
                        error: function(jqXHR, textStatus, errorThrown) 
                        {
                            console.log(jqXHR, textStatus, errorThrown);
                        }
                    });
                }
                else
                {
                    $.gritter.add({
                        title: $txt_error+'!',
                        text: $txt_cannot_change_shop_offer,
                        sticky: false,
                        time: '',
                        class_name: 'gritter-error'
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                $.gritter.add({
                    title: $txt_error+'!',
                    text: textStatus + ' ' + errorThrown,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-error'
                });
            }
        });
        
        //$('form#shopform').submit();
    });
    
    $('body').find('form').each(function(){
        var $k = $("li.phide span").attr('data-k');
        var $v = $("li.phide span").html();
        var $txt = '<input type="hidden" name="'+$k+'" value="'+$v+'">'; 
        $(this).append($txt);
    })
	
	$(".notificationicon").click(function () {
		$(this).toggleClass("open");
		$("#notificationMenu").toggleClass("open").toggleClass('hidden');
	});
});