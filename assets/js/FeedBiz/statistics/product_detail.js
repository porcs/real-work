$(function () {
    $(".chzn-select").chosen();

    $('.datetime').daterangepicker({format: 'DD/MM/YYYY', }).prev().on(ace.click_event, function () {
        $(this).next().focus();

    });

    $('textarea[class*=autosize]').autosize({append: "\n"});
    $('#information input[type=text]').addClass("span8");

    //we could just set the data-provide="tag" of the element inside HTML, but IE8 fails!
    var tag_input = $('#product_tags');
    if (!(/msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase())))
        tag_input.tag({placeholder: tag_input.attr('placeholder')});
    else {
        //display a textarea for old IE, because it doesn't support this plugin or another one I tried!
        tag_input.after('<textarea id="' + tag_input.attr('id') + '" name="' + tag_input.attr('name') + '" rows="3">' + tag_input.val() + '</textarea>').remove();
        //$('#form-field-tags').autosize({append: "\n"});
    }
});

var DataSourceTree = function (options) {
    this._data = options.data;
    this._delay = options.delay;
};

DataSourceTree.prototype.data = function (options, callback) {
    var self = this;
    var $data = null;

    if (!("name" in options) && !("type" in options)) {
        $data = this._data;//the root tree
        callback({data: $data});
        return;
    }
    else if ("type" in options && options.type == "folder") {
        if ("additionalParameters" in options && "children" in options.additionalParameters)
            $data = options.additionalParameters.children;
        else
            $data = {}//no data
    }
    ;

    if ($data != null)//this setTimeout is only for mimicking some random delay
        setTimeout(function () {
            callback({data: $data});
        }, parseInt(Math.random() * 500) + 200);

    //we have used static data here
    //but you can retrieve your data dynamically from a server using ajax call
    //checkout examples/treeview.html and examples/treeview.js for more info
};

$.ajax({
    type: "get",
    url: "/statistics/getCategories/",
    contentType: "application/json",
    success: function (data) {

        $('#tree1_loading').hide();
        var obj = JSON.parse(data);
        treeDataSource = new DataSourceTree({data: obj.category});

        $(function () {
            $('#tree1').ace_tree({
                dataSource: treeDataSource,
                multiSelect: true,
                loadingHTML: '<div class="tree-loading"><i class="icon-refresh icon-spin blue"></i></div>',
                'open-icon': 'icon-minus',
                'close-icon': 'icon-plus',
                'selectable': true,
                'selected-icon': 'icon-ok',
                'unselected-icon': 'icon-remove',
            });

            var selectTreeFolder = function ($treeEl, folder, $parentEl) {
                var $parentEl = $parentEl || $treeEl;
                if (folder.type == "folder") {
                    var $folderEl = $parentEl.find("div.tree-folder-name").filter(function (_, treeFolder) {
                        return $(treeFolder).text() == folder.name;
                    }).parent();
                    $treeEl.one("loaded", function () {
                        $.each(folder.children, function (i, item) {
                            selectTreeFolder($treeEl, item, $folderEl.parent());
                        });
                    });
                    $treeEl.tree("selectFolder", $folderEl);
                }
                else {
                    selectTreeItem($treeEl, folder, $parentEl);
                }
            };

            var selectTreeItem = function ($treeEl, item, $parentEl) {
                var $parentEl = $parentEl || $treeEl;
                if (item.type == "item") {
                    var $itemEl = $parentEl.find("div.tree-item-name").filter(function (_, treeItem) {
                        return $(treeItem).text() == item.name && !$(treeItem).parent().is(".tree-selected");
                    }).parent();
                    $treeEl.tree("selectItem", $itemEl);
                }
                else if (item.type == "folder") {
                    selectTreeFolder($treeEl, item, $parentEl);
                }
            };

            var selected = {
                name: "other",
                type: "folder",
                'additionalParameters': {id: '2', },
                children: [
                    {
                        additionalParameters: {
                            id: '48',
                        },
                        name: "Electronique",
                        type: "folder",
                        children: [
                            {
                                name: "Matériel de Soudure",
                                type: "item",
                                'additionalParameters': {id: '98'},
                            }
                        ]
                    }
                ]
            };

            selectTreeItem($("#tree1"), selected);

        });
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
    }
});