var data;

function get_category_options_page(){
    return $.ajax({
        type: 'GET',
        url: base_url + 'my_shop/category_options_page/' + $('#id_country').val() + "/" + $('#id_marketplace').val(),
        dataType: 'json',
    });
}

function setChildren(data) {   

    var icon = '<i class="fa fa-check"></i>';

    $.each(data, function(i, d) {  
        //console.log(d);        
        d['disable_data'] = d.disable;

        if(d.disable && d.disable == 1){
            d['disable'] = icon;
        }

        d['no_price_export_data'] = d.no_price_export;
        if(d.no_price_export && d.no_price_export == 1){
            d['no_price_export'] = icon;
        } 

        d['no_quantity_export_data'] = d.no_quantity_export;
        if(d.no_quantity_export && d.no_quantity_export == 1){
            d['no_quantity_export'] = icon;
        } 

        d['gift_wrap_data'] = d.gift_wrap;
        if(d.gift_wrap && d.gift_wrap == 1){
            d['gift_wrap'] = icon;
        } 

        d['gift_message_data'] = d.gift_message;
        if(d.gift_message && d.gift_message == 1){
            d['gift_message'] = icon;
        } 

        d['shipping_type_data'] = d.shipping_type;
        if(d.shipping_type && d.shipping_type >= 0){
            d['shipping_type'] = $('#shipping_type_' + d.shipping_type).val();
        }

        if(d.children != undefined){
            setChildren(d.children);
            d['edit'] = '';
        } else {
            d['edit'] = '<button type="button" class="link" value="'+d.id_category+'" id="cate_'+d.id_category+'"><i class="fa fa-pencil"></i> <b>'+$('#l_Edit').val()+'</b></button> | ' + '<button type="button" class="link reset" style="color: red" value="" title="" onclick="reset_product(this,'+d.id_category+')"><i class="fa fa-refresh"></i> <b>'+$('#l_Reset').val()+'</b></button>';               
        }
    });
}

com_github_culmat_jsTreeTable.register(this)

var options = {
    idAttr : 'id_category',
    slider : false,
    renderedAttr : {
        category : {'width': '90%','name':'<i class="icon-folder"></i><label>Category</label>'},
        //price : {'width': '5%','name':'Price Override'},
        //disable : {'width': '5%','name':'Disable'},
        //force : {'width': '5%','name':'Force'},
        //no_price_export : {'width': '5%','name':'Do not export price'},
        //no_quantity_export : {'width': '5%','name':'Do not export quantity'},
        //latency : {'width': '5%','name':'Latency'},
        //gift_wrap : {'width': '5%','name':'Gift Wrap'},
        //gift_message : {'width': '5%','name':'Gift Message'},
        //shipping : {'width': '5%','name':'Shipping'},
        //shipping_type : {'width': '5%','name':'Shipping Type'},
        //fba_value : {'width': '5%','name':'FBA Value'},
        edit : {'width': '10%','name':''},
    },
    tableAttributes :  {
        'class' : 'responsive-table tree',  
        'id' : 'category_option',       
    },
    initialExpandLevel : 5
}

var get_category = get_category_options_page();
get_category.success(function(d){
    data = d;    
    setChildren(data.category);
    appendTreetable(data.category, options, $('#category_options'));
    set_edit_data(data.category);

    if($('.dataTables_processing').length > 0){
        $('.dataTables_processing').hide();
    }

});

function set_edit_data(category){
    $.each(category, function(i, cate) {  

        if(cate.disable_data){
            cate['disable'] = cate.disable_data;
        }

        if(cate.no_price_export_data){
            cate['no_price_export'] = cate.no_price_export_data;
        } 

        if(cate.no_quantity_export_data){
            cate['no_quantity_export'] = cate.no_quantity_export_data;
        } 

        if(cate.gift_wrap_data){
            cate['gift_wrap'] = cate.gift_wrap_data;
        } 

        if(cate.gift_message_data){
            cate['gift_message'] = cate.gift_message_data;
        } 

        if(cate.shipping_type_data){
            cate['shipping_type'] = cate.shipping_type_data;
        }

        if(cate.children != undefined){
            set_edit_data(cate.children)
        } 

        $('#cate_'+cate.id_category).on('click', function(){
            add_row_details($(this), cate);
        });
                    
    });
}

var table;
var main = '-';

$(document).ready(function () {
    
    $('head').append('<link rel="stylesheet" href="'+base_url+'assets/css/feedbiz/my_shop/offers_options.css" />');

    main = $('#model').find('.table_detail').html();

    var icon = '<i class="fa fa-check"></i>';

    // ON SUBMIT
    $('.btn-save').on('click', function(){
        submit_form($(this));
    });   

    $('#category_options').prepend('<div id="category_options_processing" class="dataTables_processing"></div>');

    if($('.dataTables_processing').length > 0){
        $('.dataTables_processing').html('');
        $('.dataTables_processing').append('<div class="feedbizloader"><div class="feedbizcolor"></div><p class="m-t10 text-info">'+$('#processing').val()+'</p></div>');
        if($('.dataTables_processing').find('.feedbizloader')){
            $('.dataTables_processing').find('.feedbizloader').find('.feedbizcolor').sprite({fps: 10, no_of_frames: 30});
        }
    } 
});

function add_row_details(e, d){
    
    //console.log(d);
    var category = data.category;
    var data_detail = d;  
    var tr = $(e).closest('tr');
    
    //set tab color margin left on left
    var $window = $(window).width();
    var tableft;
    if($window > 996){
        tableft = tr.find('p').offset().left-275;
    }else{
        tableft = tr.find('p').offset().left;
    }

    if ( tr.hasClass('shown') ) {

        tr.next().hide();
        tr.removeClass('shown'); 

    } else {  

        var iTableCounter = 'category_option_' + data_detail.id_category;
        var table_clone = format(iTableCounter, main); 

        tr.after('<tr class="background-gray"><td colspan="10">'+table_clone+'</td></tr>');
        tr.next().css('border','none');
        tr.next().find('div').first().css('margin-left',tableft).css('border-left','1px solid #4693FE').css('border-right','1px solid #4693FE');
        
        if($('#'+iTableCounter).length > 0) {                
            row_details(data_detail);                   
        }            
        tr.addClass('shown'); 

        tr.next().checkBo();              
    }
}

function row_edit_scale(){
    //set tab color margin left on left
    var $window = $(window).width();
    var tableft;
    if($window > 996){
        tableft = tr.find('p').offset().left-275;
    }else{
        tableft = tr.find('p').offset().left;
    }
}

function show_force(obj){
    var object = $(obj);
    if(object.next().length > 0){
        if(object.find('input[type=checkbox]').prop('checked') == false){
            object.next().show();
            object.next().find('input[rel="force"]').removeAttr('disabled');
        }else{
            object.next().hide();
            object.next().find('input[rel="force"]').attr('disabled', 'disabled');
        }      
    }
}

function row_details(data){

    //console.log(data);
    var table_cloned = $("#category_option_" + data.id_category);    
    table_cloned.find('.id_category').val(data.id_category);
    
    $.each( table_cloned.find('input'), function(i,j){

        var object = $(this);
        var rel = object.attr('rel');
        
        if(typeof rel != undefined && rel != null){
            object.attr('name', 'category_options['+data.id_category+']'+'[' +rel+']' ); 
        }

        if(typeof data[rel] != undefined){

            if(rel === "force" && data[rel]){
                
                object.removeAttr('disabled');
                object.closest('.row').show(); 
                object.closest('td').find('input[rel="force_chk"]').prop('checked', true).change().trigger('change');
                object.closest('td').find('input[rel="force_chk"]').attr('checked', true);                
                object.closest('td').find('input[rel="force"]').removeAttr('disabled');
            }

            if(object.is(':checkbox')){

                object.val(1);
                if(data[rel] == 1){
                    object.prop('checked', true).change().trigger('change');
                    
                    object.attr('checked', true);  
                } 

            } else if(object.is(':radio')){
                
                object.each(function(i,j){

                    object.val($(j).attr('data-content'));

                    if($(j).val() === data[rel]){
                        $(j).prop('checked', true).change().trigger('change');
                        $(j).attr('checked', true);  
                    }
                });

            } else {

                object.show().removeAttr('readonly');
                object.parent().find('input[type="'+object.attr('type')+'"].disabled').remove();
                object.val(data[rel]);

            }
            
        }

    });  
}

function format ( id, main, hidden ) {
    var html = '';
    if(hidden){
        var hidden = 'style="display:none"';
    }
    html = '<div class="row-edit p-10 p-l40" '+hidden+' >'+
               '<table cellpadding="5" cellspacing="5" border="0" class="table_detail" id="'+id+'" width="95%">' +
                    main +
               '</table>'+
           '</div>';
    return html;   
}

function submit_form(e){

    // show submitting
    addStatus(e);

    //call ajax 
    var formObj = $("#category_options_form");
    var formData = $("#category_options_form").serialize();
    $.ajax({
        url: base_url + 'my_shop/save_category_options/' + $('#id_country').val() + "/" + $('#id_marketplace').val(),
        type: 'POST',
        data:  formData,           
        success: function(data, textStatus, jqXHR)
        {
            successStatus(e);
            reload_ajax();
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            addError(e, jqXHR);
            console.log(jqXHR, textStatus, errorThrown);
        }          
    });
       
}

function reset_product(element, id_category){
    var parent_name = $(element).closest('tr').find('td:first').text();
    bootbox.confirm($('#reset-confirm').val()+'"'+parent_name+'"'+'?', function(result) {
        if(result) { 

            if($('.dataTables_processing').length > 0){
                $('.dataTables_processing').show();
            }     

            //call ajax 
            $.ajax({
                url: base_url + 'my_shop/reset_category_options/' + $('#id_country').val() + "/" + $('#id_marketplace').val(),
                type: 'POST',
                dataType: 'json',
                data: {'id_category': id_category},           
                success: function(data, textStatus, jqXHR)
                {
                    //console.log(data);
                    reload_ajax();
                },
                error: function(jqXHR, textStatus, errorThrown) 
                {
                    console.log(jqXHR, textStatus, errorThrown);
                }          
            });
               
        }                    
    });
}

function reload_ajax(){
    /*var table = $('#category_option');
    
    table.remove();
    var get_category = get_category_options_page();
    get_category.success(function(d){
        setChildren(d.category)
        appendTreetable(d.category, options, table)

        $('.dataTables_processing').hide();
    });*/
    location.reload();
}

function successStatus(e){
    e.attr('disabled', false);
    e.closest('div').find('.status').find('i').removeClass('fa-spinner fa-spin').addClass('fa-check');
    e.closest('div').find('.status').find('p').find('span').html($('#l_Success').val());
}

function addStatus(e){

    //   
    e.attr('disabled', true);
    e.addClass('update');
    e.closest('div').addClass('form-group has-update');
    e.closest('div').find('.status').addClass('update').show();
   
    if(e.closest('div').find('.status').find('i').hasClass('fa-check')){
        e.closest('div').find('.status').find('i').addClass('fa-spinner fa-spin').removeClass('fa-check');
        e.closest('div').find('.status').find('p').find('span').html($('#l_Submit').val());
    }
}

function addError(e, r){    
    e.attr('disabled', false);
    e.closest('div').addClass('form-group has-error');
    e.closest('div').find('.status').find('i').removeClass('fa-spinner fa-spin').addClass('fa-minus');
    e.closest('div').find('.status').find('p').find('span').html($('#l_Error').val() + ' ' + r); 
}