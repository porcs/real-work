$(document).ready(function () {
    $('form#form-api-setting').checkBo();
    $('.copy_to_clipboard').on('click', function () {
        $(this).parent().parent().find('input[name="url"]').select();
        try {
            var successful = document.execCommand('copy');
            if (successful) {
                var copyItem = $('span.copyAnswer').append('<span class="text-success p-r10"><small>Copied!</small></span>').fadeIn('slow', 'swing', function () {
                    var _this = $(this).find('span');
                    setTimeout(function () {
                        //$('span.copyAnswer').html('');
                        _this.fadeOut(3000, function () {
                            $(this).remove();
                        });
                    }, 2000);
                });
            } else {
                $('span.copyAnswer').html('Unable to copy!');
            }
        } catch (err) {
            $('span.copyAnswer').html('Unsupported Browser!');
        }
    });
});