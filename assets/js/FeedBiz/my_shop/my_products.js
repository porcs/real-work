

$(document).ready(function() {
    
    var table_language = {};
    table_language.emptyTable           = $('#emptyError').val();
    table_language.info                 = $('#info').val();
    table_language.infoEmpty            = $('#infoEmpty').val();
    table_language.infoFiltered         = $('#infoFiltered').val();
    table_language.infoPostFix          = $('#infoPostFix').val();
    table_language.thousands            = $('#thousands').val();
    table_language.lengthMenu           = $('#lengthMenu').val();
    table_language.loadingRecords       = $('#loadingRecords').val();
    table_language.processing           = $('#processing').val();
    table_language.search               = '';
    table_language.zeroRecords          = $('#zeroRecords').val();
    table_language.paginate = {};
    table_language.paginate.first       = $('#paginate-first').val();
    table_language.paginate.last        = $('#paginate-last').val();
    table_language.paginate.next        = $('#paginate-next').val();
    table_language.paginate.previous    = $('#paginate-previous').val();
    table_language.aria = {};
    table_language.aria.sortAscending   = $('#aria-sortAscending').val();
    table_language.aria.sortDescending  = $('#aria-sortDescending').val();
    
    var checkboMyProductTable = (function (run) {
    if(!run){    
    $('input[type="checkbox"]:not(input.amazon-input)').change(function () {
        var $this = $(this),
                $_tableCheckbo = $this.closest('.dropCheckbo'),
//                $_tableRow = $this.closest('.row').find('.row');
        $_tableRow= $('#my_prod_list');

        $this.each(function () {
            var $_this = $(this),
//                    $tableRow = $_this.closest('.row'),
                     $tableRow = $_tableRow,
                    $t_Head = $tableRow.find('thead'),
                    $t_Foot = $tableRow.find('tfoot');
            if ($_this.is(':checked')) {
                 $tableRow.find('.'+$_this.attr('name')).show();
            } else {
                $tableRow.find('.'+$_this.attr('name')).hide();
            }
            if ($this.closest('.dropCheckbo').find('.cb-checkbox.checked').length == 0) {
                $_tableRow.find('.tableEmpty').show();
                $_tableRow.find('.dataTables_filter').hide();
                $_tableRow.find('.dataTables_info').hide();
                $_tableRow.find('.dataTables_paginate').hide();
            } else {
                $_tableRow.find('.tableEmpty').hide();
                $_tableRow.find('.dataTables_filter').show();
                $_tableRow.find('.dataTables_info').show();
                $_tableRow.find('.dataTables_paginate').show();
            }
        });
        
        var list = [];
        $('input[type="checkbox"]:not(input.amazon-input)').each(function(){
            var $this = $(this);
            var obj = {};
            obj[$this.attr('name')] = $this.is(':checked');
            list.push(obj);
            
        })
        
        if(list){
            $.ajax({url:base_url+'my_shop/my_product_column_filter_save',
                data:{'col_filter':JSON.stringify(list)},method:'post', 
                success:function(d){
                    
                }
            });
        }
    });
    }else{
    $('input[type="checkbox"]:not(input.amazon-input)').each(function () {
        var $this = $(this),
                $_tableCheckbo = $this.closest('.dropCheckbo'),
//                $_tableRow = $this.closest('.row').find('.row');
            $_tableRow=$('#my_prod_list');
 
            var $_this = $(this),
                    $tableRow = $_tableRow;//$_this.closest('.row'),
                    $t_Head = $tableRow.find('thead'),
                    $t_Foot = $tableRow.find('tfoot');
            if ($_this.is(':checked')) {
                 $tableRow.find('.'+$_this.attr('name')).show();
            } else {
                $tableRow.find('.'+$_this.attr('name')).hide();
            }
         
    });
    }
});
     
    
    $('#sel_all').change(function(){
            showWaiting(true);
                if($(this).prop('checked')){
                $('#my_prod_list>tbody>tr>.p_id').find('input[type=checkbox]').each(function(){
                        $(this).prop('checked', true).change();
                });
                }else{
                    $('#my_prod_list>tbody>tr>.p_id').find('input[type=checkbox]').each(function(){
                        $(this).prop('checked', false).change();
                    });
                }
            showWaiting(false);
            });
    
    var table = $('#my_prod_list').DataTable( {
        "language": table_language,
        "ajax": base_url+'my_shop/my_product_data?condition='+$("select#my_prod_list_filter_condition").val()+'&category='+$("select#my_prod_list_filter_category").val(), 
        "fnPreDrawCallback" : function( oSettings ) {
        	oSettings.ajax              = base_url+'my_shop/my_product_data?condition='+$("select#my_prod_list_filter_condition").val()+'&category='+$("select#my_prod_list_filter_category").val();
//        	console.log(oSettings.ajax);
        },
        "processing": true,
        "serverSide": true,
        "order": [[1, 'asc']],
        "fnDrawCallback": function(oSettings) {
            showWaiting(true);
            //console.log(oSettings);
            $('.custom-form > tbody > tr').checkBo();
            
//            $('.allCheckBo').checkBo({
//		checkAllButton : '.checkAll', 
//                checkAllTarget : '.checkboSettings', 
//	});
        },
        "columns": [
            {
              "className":      'p_id', "data":"cond", "defaultContent": '',"width": "5%","orderable": false
            },
            { "className":      'p_img',"data": "pid","width": "5%" ,"orderable": false},
            
            { "className":      'p_name',"data": "name", "orderable": false },
            { "className":      'cat_name',"data": "category_default_name","width": "10%","orderable": false }, 
            
            { "className":      'p_all_ref' ,"data": "search","width": "300px" ,"orderable": false},
            { "className":      'p_status',"data": "status", "defaultContent": '',"width": "7%","orderable": false  },
            { "className":      'p_condition',"data": "condition_name", "defaultContent": '',"width": "7%" ,"orderable": false},
            { "className":      'p_price',"data": "price","width": "10%" ,"orderable": false},
            { "className":      'p_qty',"data": "qty","width": "6%" ,"orderable": false},
            { "className":      'p_sale_price',"data": "sale_price","defaultContent": '',"width": "3%" ,"orderable": false},
            { "className":      'p_sale_from',"data": "sale_from","defaultContent": '',"width": "4%" ,"orderable": false},
            { "className":      'p_sale_to',"data": "sale_to","defaultContent": '',"width": "4%" ,"orderable": false}
           
        ],
        "fnRowCallback": function( nRow, aData, iDisplayIndex ) { 
            $('select[name="my_prod_list_length"]').chosen({
                            search_contains:true,
                            disable_search_threshold: 1,
                            no_results_text: "Nothing found!"
                    });
            $('input[type="Search"]').addClass('form-control');
            $(this).removeClass().addClass('responsive-table').addClass('custom-form').attr('style', '');
            
            $('.chosen-single').find('div').addClass('button-select');
            $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
            
            $(".dataTables_wrapper .chosen-search").remove();
             
            var row = table.row( nRow );
            function product_main_detail(data){
                  var $out = '<table>'+
                            '<tr><td class="p_ref" data-th="REF"> '+data.ref.ref+'</td>'+
                            /*'<td class="p_sku" data-th="SKU"> '+data.ref.sku+'</td>'+*/
                            '<td class="p_ean" data-th="EAN"> '+data.ref.ean13+'</td>'+
                            '<td class="p_upc" data-th="UPC"> '+data.ref.upc+'</td>'+
                            '</tr></table>';
                        return $out;
            }
            var arr = [];
            var arr_dummy = [];
            function product_detail(data,iDisplayIndex,row){
//                    var out = '<table class="responsive-table table-white">';  //class="prod_sku"
                    if(aData.no_attr)return;
                    var i=0;
                      arr[iDisplayIndex] = [];
                      arr_dummy[iDisplayIndex] = [];
                    $.each(data.items,function(k,d){  
                        
                        
//                        console.log(d);
                        var sale = '';
                        if(d.sale_to!=''){
                            sale = (d.sale_price+'<br><span>'+d.sale_from+' - '+d.sale_to +'</span>');
                        }
                         arr[iDisplayIndex][i] =
//                            '<table class="responsive-table table-white"><tr>'+
                            '<td class="p_id p_atr_id tableCheckbo">'+k+'</td>'+
                            '<td class="p_img">&nbsp;</td>'+
                            '<td class="p_variant" data-th="Variants:"><i class="fa fa-reply lb"></i>'+d.atr+'</td>'+
                            '<td class="cat_name" data-th="Category">'+''+'</td>'+
                            '<td class="all_ref"><table><tr><td class="p_ref" data-th="REF"> '+d.ref.ref+'</td>'+
                            /*'<td class="p_sku" data-th="SKU"> '+d.ref.sku+'</td>'+*/
                            '<td class="p_ean" data-th="EAN"> '+d.ref.ean13+'</td>'+
                            '<td class="p_upc" data-th="UPC"> '+d.ref.upc+'</td>'+
                            '</tr></table></td>'+
                            '<td class="p_status" data-th="Status:">'+''+'</td>'+
                            '<td class="p_condition" data-th="Condition:">'+''+'</td>'+
                            '<td class="p_price" data-th="Price:">'+d.price+'</td>'+
                            '<td class="p_qty" data-th="Quantity:">'+d.qty+'</td>' +
                            '<td class="p_sale_price" data-th="Price:">'+d.sale_price+'</td>'+
                            '<td class="p_sale_from" data-th="From:">'+d.sale_from+'</td>'+
                            '<td class="p_sale_to" data-th="To:">'+d.sale_to+'</td>' 
                            
//                            +'</tr></table>'
                            ; 
                             
                            arr_dummy[iDisplayIndex][i] = '';    
                             
                        i++;
                    }); 
                    var ch = row.child(arr_dummy[iDisplayIndex], 'no-padding ch_row c_'+iDisplayIndex ) ;
                        ch.show();
                    
                    setTimeout(function(){  
                        $.each(arr[iDisplayIndex],function(k,d){  
//                        console.log(iDisplayIndex, k,d);
                            $('#my_prod_list').find('tr.c_'+iDisplayIndex).eq(k).html(d);
//                                        $('td:eq(2),td:eq(3),td:eq(4),td:eq(5)', nRow).unbind('click').click(function(){
//                                            $(this).parent().find('.sel_row').click();
//                                         });                 
                             showWaiting(false);
                         });

                    },1);
//                    out+='</table>';
//                    return out;
            }
            var image = aData.img
            if(jQuery.trim(image)!=''){
                $('td:eq(1)', nRow).html('<a href="'+image+'" class="gallery-image"><i class="fa fa-eye fa-2x link"></i></a>');//<img src="'+image+'" class="image-table" alt="" onError="this.onerror=null;this.src=\''+base_url+'assets/images/not_found_image.png\';">
                $('td:eq(1)', nRow).addClass('text-center');
                //$('td:eq(1)', nRow).find('a').colorbox();
            }else{
                $('td:eq(1)', nRow).html('');
            }   
            $('td:eq(0)', nRow).html(aData.pid);
            
            
//            $('td:eq(0)', nRow).html('<label class="cb-checkbox"><input class="sel_row" type="checkbox" rel="'+aData.pid+'" /></label> ');
            
            if(!aData.no_attr){
                $('td:eq(6)', nRow).html('');
                $('td:eq(4)', nRow).html('');
                $('td:eq(5)', nRow).html('');
            }
            if(aData.act && aData.act==1){
                $('td.p_status', nRow).html($('input#status_active').val());
            }else{
                $('td.p_status', nRow).html($('input#status_inactive').val());
            }
            if(aData.cond ){
                if($('#my_prod_list_filter_condition option[value="'+aData.cond+'"]')){
                    $('td.p_condition', nRow).html($('#my_prod_list_filter_condition option[value="'+aData.cond+'"]').html());
                } 
            }
              
            $('td.p_sale_price', nRow).html(aData.sale_price);
            $('td.p_sale_from', nRow).html(aData.sale_from);
            $('td.p_sale_to', nRow).html(aData.sale_to);
           
            var txt = product_detail(aData,iDisplayIndex,row);
            var mtxt = product_main_detail(aData);
            
            var cb_checkbox = $('td:eq(0) .cb-checkbox', nRow);  
            //cb_checkbox.checkBo();
            
//            cb_checkbox.on('click',function(){ 
//                if($(this).hasClass('just')){return;}
//                $(this).addClass('just'); 
//                    if ($(this).hasClass('checked')) {
////                         $(this).find('input.sel_row').prop('checked', false);
//                         $(this).removeClass('checked');
//                     } else { 
////                             $(this).find('input.sel_row').prop('checked', true);
//                             $(this).addClass('checked');
//                     } 
//                     var obj = $(this);
//                      setTimeout(function(){ 
//                          obj.removeClass('just');
//                      },100);
//                 });
            setTimeout(function(){  
                    $('td:eq(4)', nRow).html(mtxt);
                    showWaiting(false);
                 
            },1);
            
          },
          "fnInitComplete": function(oSettings, json) {
                showWaiting(false);
                //custom move datatable filter controls
                $('#my_prod_list_length select[name="my_prod_list_length"]').appendTo('#my_prod_list_length_container');
                $('#my_prod_list_length .chosen-container').appendTo('#my_prod_list_length_container');
                $('#my_prod_list_filter input[type="search"]').appendTo('#my_prod_list_filter_container');
                $('#my_prod_list_length').remove();
                $('#my_prod_list_filter').remove();
                checkboMyProductTable();
     
              },
          "fnDrawCallback": function(settings, data){
        	  checkboMyProductTable(true);
                  setTimeout(function(){ 
                    checkboMyProductTable(true);
                    setTimeout(function(){ 
                        showWaiting(false);
                        if(!$('input[type="search"]').is('form-control'))
                        $('input[type="search"]').addClass('form-control');
                    },500);
                  },1);
          }
    });
    
    $.ajax({
    url:base_url+'my_shop/getJsonCategory',dataType:'json'
    ,success:function(data){
        
        $.each(data,function(k,d){
            var id = d.id;
            var name ='';
            var tab = d.lv;
            for(var i=0; i<tab; i++){
                name += "&nbsp&nbsp";
            }
            if(d.name){
                var namex = '';
                $.each(d.name,function(l,n){
                    namex = n;
                });
                name+=namex;
            }else{
                return;
            }
            var opt = "<option value=\"'"+id+"'\">"+name+"</option>";
            $("#my_prod_list_filter_category").append(opt);
            
        })
        
        //custom add datatable filter controls
        $("#my_prod_list_filter_condition").chosen({search_contains: true, width: 'resolve'}).on("change", function(e) {
          // mostly used event, fired to the original element when the value changes
          table.column(0).search(  $("#my_prod_list_filter_condition").val() , false, false ).draw();
        });
        $("#my_prod_list_filter_category").chosen({search_contains: true, width: 'resolve'}).on("change", function(e) {
          // mostly used event, fired to the original element when the value changes
          table.column(3).search(  $("#my_prod_list_filter_category").val() , false, false ).draw();
        });
        $("#sel_condition").chosen({
                search_contains:true,
                allowClear: true,
                width: 'resolve'
        }).on("change", function(e) {
          // mostly used event, fired to the original element when the value changes
          table.column(0).search( e.val).draw();
        });
        
        showWaiting(false);
        
    }
    });
    $('#sel_all').click(function(){
            var status = $(this).attr('checked');
            $('#my_prod_list').find('.sel_row').each(function(){
                if(status){
                    $(this).attr('checked',status);
                }else{
                    $(this).attr('checked',false);
                }
            });
    });
    
    
});