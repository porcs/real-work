

$(document).ready(function() {
    
    $('#sel_all').change(function(){
            showWaiting(true);
                if($(this).prop('checked')){
                $('#my_prod_list>tbody>tr>.p_id').find('input[type=checkbox]').each(function(){
                        $(this).prop('checked', true).change();
                });
                }else{
                    $('#my_prod_list>tbody>tr>.p_id').find('input[type=checkbox]').each(function(){
                        $(this).prop('checked', false).change();
                    });
                }
            showWaiting(false);
            });
    
    var table = $('#my_prod_list').DataTable( {
        "ajax": base_url+'my_shop/my_product_data?condition='+$("select#my_prod_list_filter_condition").val()+'&category='+$("select#my_prod_list_filter_category").val(), 
        "fnPreDrawCallback" : function( oSettings ) {
        	oSettings.ajax              = base_url+'my_shop/my_product_data?condition='+$("select#my_prod_list_filter_condition").val()+'&category='+$("select#my_prod_list_filter_category").val();
//        	console.log(oSettings.ajax);
        },
        "processing": true,
        "serverSide": true,
        "order": [[1, 'asc']],
        "fnDrawCallback": function(oSettings) {
            showWaiting(true);
            //console.log(oSettings);
            $('.custom-form > tbody > tr').checkBo();
            
//            $('.allCheckBo').checkBo({
//		checkAllButton : '.checkAll', 
//                checkAllTarget : '.checkboSettings', 
//	});
        },
        "columns": [
            {
              "className":      'p_id', "data":"cond", "defaultContent": '',"width": "5%","orderable": false
            },
            { "className":      'p_img',"data": "pid","width": "5%" ,"orderable": true},
            
            { "className":      'p_name',"data": "name","width": "40%" },
            
            { "className":      'p_all_ref' ,"data": "search","width": "300px" ,"orderable": false},
            { "className":      'p_price',"data": "price","width": "10%" ,"orderable": true},
            { "className":      'p_qty',"data": "qty","width": "10%" ,"orderable": true}
        ],
        "fnRowCallback": function( nRow, aData, iDisplayIndex ) { 
            $('select[name="my_prod_list_length"]').chosen({
                            disable_search_threshold: 1,
                            no_results_text: "Nothing found!"
                    });
            $('input[type="Search"]').addClass('form-control');
            $(this).removeClass().addClass('responsive-table').addClass('custom-form').attr('style', '');
            
            $('.chosen-single').find('div').addClass('button-select');
            $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
            
            $(".dataTables_wrapper .chosen-search").remove();
             
            var row = table.row( nRow );
            function product_main_detail(data){
                  var $out = '<table><tr><td class="p_ref" data-th="REF"> '+data.ref.ref+'</td>'+
                            '<td class="p_sku" data-th="SKU"> '+data.ref.sku+'</td>'+
                            '<td class="p_ean" data-th="EAN"> '+data.ref.ean13+'</td>'+
                            '<td class="p_upc" data-th="UPC"> '+data.ref.upc+'</td>'+
                            '</tr></table>';
                        return $out;
            }
            var arr = [];
            var arr_dummy = [];
            function product_detail(data,iDisplayIndex,row){
//                    var out = '<table class="responsive-table table-white">';  //class="prod_sku"
                    if(aData.no_attr)return;
                    var i=0;
                      arr[iDisplayIndex] = [];
                      arr_dummy[iDisplayIndex] = [];
                    $.each(data.items,function(k,d){  
                        
                        
//                        console.log(d);

                         arr[iDisplayIndex][i] =
//                            '<table class="responsive-table table-white"><tr>'+
                            '<td class="p_id tableCheckbo">&nbsp;</td>'+
                            '<td class="p_img">&nbsp;</td>'+
                            '<td class="p_variant" data-th="Variants:"><i class="fa fa-reply lb"></i>'+d.atr+'</td>'+
                            
                            '<td class="all_ref"><table><tr><td class="p_ref" data-th="REF"> '+d.ref.ref+'</td>'+
                            '<td class="p_sku" data-th="SKU"> '+d.ref.sku+'</td>'+
                            '<td class="p_ean" data-th="EAN"> '+d.ref.ean13+'</td>'+
                            '<td class="p_upc" data-th="UPC"> '+d.ref.upc+'</td>'+
                            '</tr></table></td>'+
                            
                            '<td class="p_price" data-th="Price:">'+d.price+'</td>'+
                            '<td class="p_qty" data-th="Quantity:">'+d.qty+'</td>' 
//                            +'</tr></table>'
                            ; 
                             
                            arr_dummy[iDisplayIndex][i] = '';    
                             
                        i++;
                    }); 
                    var ch = row.child(arr_dummy[iDisplayIndex], 'no-padding ch_row c_'+iDisplayIndex ) ;
                        ch.show();
                    
                    setTimeout(function(){  
                        $.each(arr[iDisplayIndex],function(k,d){  
//                        console.log(iDisplayIndex, k,d);
                            $('#my_prod_list').find('tr.c_'+iDisplayIndex).eq(k).html(d);
//                                        $('td:eq(2),td:eq(3),td:eq(4),td:eq(5)', nRow).unbind('click').click(function(){
//                                            $(this).parent().find('.sel_row').click();
//                                         });                 
                             showWaiting(false);
                         });

                    },1);
//                    out+='</table>';
//                    return out;
            }
            var image = aData.img
            if(jQuery.trim(image)!=''){
                $('td:eq(1)', nRow).html('<a href="'+image+'" class="gallery-image"><img src="'+image+'" class="image-table" alt="" onError="this.onerror=null;this.src=\''+base_url+'assets/images/not_found_image.png\';"></a>');
                 //$('td:eq(1)', nRow).find('a').colorbox();
            }else{
                $('td:eq(1)', nRow).html('');
            }   
            $('td:eq(0)', nRow).html(aData.pid);
            
//            $('td:eq(0)', nRow).html('<label class="cb-checkbox"><input class="sel_row" type="checkbox" rel="'+aData.pid+'" /></label> ');
            
            if(!aData.no_attr){
                $('td:eq(3)', nRow).html('');
                $('td:eq(4)', nRow).html('');
                $('td:eq(5)', nRow).html('');
            }
             
           
            var txt = product_detail(aData,iDisplayIndex,row);
            var mtxt = product_main_detail(aData);
            
            var cb_checkbox = $('td:eq(0) .cb-checkbox', nRow);  
            //cb_checkbox.checkBo();
            
//            cb_checkbox.on('click',function(){ 
//                if($(this).hasClass('just')){return;}
//                $(this).addClass('just'); 
//                    if ($(this).hasClass('checked')) {
////                         $(this).find('input.sel_row').prop('checked', false);
//                         $(this).removeClass('checked');
//                     } else { 
////                             $(this).find('input.sel_row').prop('checked', true);
//                             $(this).addClass('checked');
//                     } 
//                     var obj = $(this);
//                      setTimeout(function(){ 
//                          obj.removeClass('just');
//                      },100);
//                 });
            setTimeout(function(){  
                    $('td:eq(3)', nRow).html(mtxt);
                    showWaiting(false);
                 
            },1);
            
          },
          "fnInitComplete": function(oSettings, json) {
                showWaiting(false);
                //custom move datatable filter controls
                $('#my_prod_list_length select[name="my_prod_list_length"]').appendTo('#my_prod_list_length_container');
                $('#my_prod_list_length .chosen-container').appendTo('#my_prod_list_length_container');
                $('#my_prod_list_filter input[type="search"]').appendTo('#my_prod_list_filter_container');
                $('#my_prod_list_length').remove();
                $('#my_prod_list_filter').remove();
              },
          "fnDrawCallback": function(settings, data){
        	  showWaiting(false);
          }
    });
    
    $.ajax({
    url:base_url+'my_shop/getJsonCategory',dataType:'json'
    ,success:function(data){
        
        $.each(data,function(k,d){
            var id = d.id;
            var name ='';
            var tab = d.lv;
            for(var i=0; i<tab; i++){
                name += "&nbsp&nbsp";
            }
            if(d.name){
                var namex = '';
                $.each(d.name,function(l,n){
                    namex = n;
                });
                name+=namex;
            }else{
                return;
            }
            var opt = "<option value=\"'"+id+"'\">"+name+"</option>";
            $("#my_prod_list_filter_category").append(opt);
            
        })
        
        //custom add datatable filter controls
        $("#my_prod_list_filter_condition").chosen({width: 'resolve'}).on("change", function(e) {
          // mostly used event, fired to the original element when the value changes
          table.column(0).search(  $("#my_prod_list_filter_condition").val() , false, false ).draw();
        });
        $("#my_prod_list_filter_category").chosen({width: 'resolve'}).on("change", function(e) {
          // mostly used event, fired to the original element when the value changes
          table.column(3).search(  $("#my_prod_list_filter_category").val() , false, false ).draw();
        });
        $("#sel_condition").chosen({
                allowClear: true,width: 'resolve'
        }).on("change", function(e) {
          // mostly used event, fired to the original element when the value changes
          table.column(0).search( e.val).draw();
        });
        
        showWaiting(false);
        
    }
    });
    $('#sel_all').click(function(){
            var status = $(this).attr('checked');
            $('#my_prod_list').find('.sel_row').each(function(){
                if(status){
                    $(this).attr('checked',status);
                }else{
                    $(this).attr('checked',false);
                }
            });
    });
});