var table;
var main = '';

$(document).ready(function () {
    $('#select_all_opt').change(function(){
       if($(this).is(':checked')){
           $('tr.group input[type="checkbox"],tr.row input[type="checkbox"]').prop('checked',true).change();
       } else{
           $('tr.group input[type="checkbox"],tr.row input[type="checkbox"]').prop('checked',false).change();
       }
    });
    
    
    $('head').append('<link rel="stylesheet" href="'+base_url+'assets/css/feedbiz/my_shop/offers_options.css" />');

    $('.order-date').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        //dateFormat: 'MM yy',
        dateFormat: 'yy-mm-dd'
    });

    main = $('#model').find('.table_detail').html();

    var icon = '<i class="fa fa-check"></i>';

    $('#product_options').DataTable({
        "columnDefs": [
            { "visible": false,  "targets": 0 },
            { "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},
            /*{ "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},
            { "visible": true},*/
        ],
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "asStripeClasses": [ '' ],
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "bAutoWidth": false,
        "ajax": {
            "url": base_url + "my_shop/product_options_page",
            "type": "POST"
        },
        "columns": [           
            { "data": "parent_sku", "bSortable": false  },  
            { "data": null, "bSortable": false,  "width": "5%", "className": 'text-center p-l15', 
                 render: function () {               
                    return '';  
                }
            }, 
            { "data": null, "bSortable": true, "width": "25%", "className": 'text-left p-l10',
                 render: function (data) {
                    var class_name = 'item-' + data.parent_sku_key;   
                  
                    return '<label class="cb-checkbox"><input type="checkbox" class="item '+class_name+'" name="product_options['+data.parent_sku_key+']'+'[child]'+'['+data.key+'][sku]" value="'+data.reference+'" />' + 
                        ( (data.name) ? data.name : data.reference) + '</label>';  
                               
                }
            }, 
            { "data": null, "bSortable": false, "width": "10%", "className": 'text-center',
                 render: function (data) {
                    var price =  (data.price) ? data.price : '';
                    return price;                   
                }
            },
            { "data": null, "bSortable": false, "width": "5%", "className": 'text-center', 
                 render: function (data) {
                    var disable =  (data.disable && data.disable == 1) ? icon : '';
                    return  disable;             
                }
            },           
            { "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                 render: function (data) {
                    var force =  (data.force) ? data.force : '';
                    return  force;             
                }
            },
            { "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                 render: function (data) {
                    var checked =  (data.no_price_export && data.no_price_export == 1) ? icon : '';
                    return  checked;                 
                }
            },
            { "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                 render: function (data) {  
                    var checked =  (data.no_quantity_export && data.no_quantity_export == 1) ? icon : '';
                    return  checked;               
                }
            },           
            { "data": null, "bSortable": false, "width": "10%", "className": 'text-center',
                 render: function (data) {
                    var latency =  (data.latency) ? data.latency : '';
                    return latency;   
                }
            },           
            /*{ "data": null, "bSortable": false, "width": "10%", "className": 'text-center',
                 render: function (data) {
                    var asin1 =  (data.asin1) ? data.asin1 : '';
                    return '<button type="button" class="link asin1">' + asin1 + '</button>';   
                }
            },
            { "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                 render: function (data) {
                    var gift_wrap =  (data.gift_wrap && data.gift_wrap == 1) ? icon : '';
                    return  gift_wrap;                      
                }
            },
            { "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                 render: function (data) {
                    var gift_message =  (data.gift_message && data.gift_message == 1) ? icon : '';
                    return  gift_message;                 
                }
            },*/
            { "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                 render: function (data) {
                    var shipping =  (data.shipping) ? data.shipping : '';
                    return shipping;   
                }
            },
            /*{ "data": null, "bSortable": false, "width": "10%", "className": 'text-center',
                 render: function (data) {
                    //console.log(data.shipping_type);
                    var shipping_type = '';
                    if(data.shipping_type){
                        shipping_type = $('#shipping_type_' + data.shipping_type).val();   
                    }
                    return shipping_type;   
                }
            },
            { "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                 render: function (data) {
                    var fba_value =  (data.fba_value) ? data.fba_value : '';
                    return  fba_value;                
                }
            },  */       
            { "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                 render: function (data) {
                    return '';                   
                }
            },
        ],             
        "bFilter" : true,
        "paging": true,
        "pagingType": "full_numbers" ,  
        "order": [[0, 'asc'],[1, 'asc']], 
        "drawCallback": function ( ) {
            $('#select_all_opt').prop('checked',false).change();
            $('#select_all_opt').parents('th').removeClass('sorting_asc').addClass('sorting_disabled');
            var api = this.api();
            var rows = api.rows().nodes();
            var last = null;

            api.column(0, {page:'current'} ).data().each( function ( group, i ) {

                var row_data = api.rows(i).data();

                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group">'+
                            '<td class="text-center p-l15">'+ 
                                '<input type="hidden" id="id_product_'+row_data[0].parent_sku_key+'" value="'+row_data[0].id_product+'" />'+
                                '<input type="hidden" id="sku_'+row_data[0].parent_sku_key+'" value="'+row_data[0].reference+'" />'+
                                '<input type="hidden" id="parent_sku_'+row_data[0].parent_sku_key+'" value="'+row_data[0].parent_sku+'" />'+
                                '<label class="cb-checkbox"><input type="checkbox" class="parent_check" value="'+row_data[0].parent_sku_key+'"  /></label>'+ 
                            '</td>'+
                            '<td colspan="7" class="p-l10 dark-gray montserrat">'+
                                ( (row_data[0].remark_missing_reference) ? '<b> <span class="text-uc" style="color:red">*</span> '+group+'</b>' : '<b class="text-uc">'+group+'</b>' )+
                                ( (row_data[0].p_name) ? ' - ' + (row_data[0].p_name) : '' )+
                            '</td>'+ 
                            
                            '<td colspan="2" class="p-r15 text-uc dark-gray montserrat text-right">'+
                                '<button type="button" class="link details-parent-control disabled" disabled><i class="fa fa-pencil"></i> <b>'+$('#l_Edit').val()+'</b></button>'+
                                ' | ' +
                                '<button type="button" class="link reset" style="color: red" ><i class="fa fa-refresh"></i> <b>'+$('#l_Reset').val()+'</b></button>'+
                            '</td>'+
                        '</tr>'
                    );
                    /*onclick="parent_checked(this)"*/
                    last = group;  
                }

            });              //l_missing_reference
            
            $(rows).checkBo();
            $('tr.group').checkBo();

            //parent_check
            $('input.parent_check').change(function(){
                var object = $(this);    
                $('.item-' + object.val()).prop('checked',  object.prop('checked')).change(); 
            }).trigger('change');
            
            $('tr').on('change', 'input[type="checkbox"]', function(){
                set_id($(this));
            }).trigger('change');

            /*$('td').find('input[type="checkbox"]').on('click', function () {
                set_id($(this));
            });*/
        },
        "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            //console.log(aData, iDisplayIndex);
            $('td',nRow).each(function(i,v, y){
                if($(this).html() != "") {
                    if(i == 0) $(v).attr('data-th', '');
                    if(i == 1) $(v).attr('data-th', '');
                    if(i == 2) $(v).attr('data-th', 'Price Override');
                    if(i == 3) $(v).attr('data-th', 'Disabled');
                    if(i == 4) $(v).attr('data-th', 'Force');
                    if(i == 5) $(v).attr('data-th', 'Do not export price');
                    if(i == 6) $(v).attr('data-th', 'Do not export quantity');
                    if(i == 7) $(v).attr('data-th', 'Latency');
                    //if(i == 8) $(v).attr('data-th', 'Gift Wrap');
                    //if(i == 9) $(v).attr('data-th', 'Gift Message');
                    if(i == 8) $(v).attr('data-th', 'Shipping');
                    //if(i == 11) $(v).attr('data-th', 'Shipping Type');
                    if(i == 9) $(v).attr('data-th', '');
                }
            });
        }
        
    } ); 
    
    $('#product_options').on('click', '.asin1', function(){
        $(this).parent().parent().find('input').prop('checked', true).change().trigger('change');
    });
    
    table = $('#product_options').DataTable();   

    $('#product_options').on('click', 'button.reset', function(){
        var th = $(this);
        var parent_key = th.closest('.group').find('.parent_check').val();
        var parent_name =  th.closest('.group').find('#parent_sku_'+parent_key).val();

        bootbox.confirm($('#reset-confirm').val()+'"'+parent_name+'"'+'?', function(result) {
            if(result) {
                var id_product = th.closest('.group').find('#id_product_'+parent_key).val();
                reset_product(id_product);
            }                    
        });
    });

    $('#product_options').on('click', 'button.details-parent-control', function () {

        var parent_name = $(this).closest('.group').find('.parent_check').val(); 
        var tr = $(this).closest('tr');
                    
        if ( tr.hasClass('shown') ) {
            var row = $("#product_option_" + parent_name).closest('.row-edit');

            row.closest('tr.added_row').remove();
            tr.removeClass('shown');
            
            $(tr).nextUntil('.group').each(function(){
                $(this).removeClass('editing');
            });

        } else {
            
            $(tr).nextUntil('.group').each(function(){
                $(this).addClass('editing');
            });

            var id_product_val = $(this).closest('.group').find('#id_product_'+parent_name).val(); 
            var sku_val = $(this).closest('.group').find('#parent_sku_'+parent_name).val(); 
            var parent_sku_val = $(this).closest('.group').find('#parent_sku_'+parent_name).val(); 

            // Open this row            
            var d = {};
            d['key'] = parent_name;
            d['id_product'] = id_product_val;
            d['reference'] = parent_sku_val;
            d['parent_sku'] = parent_sku_val;               

            var iTableCounter = 'product_option_' + parent_name;
            var table_clone = format(iTableCounter, main);  
            tr.after('<tr role="row" class="background-gray added_row"><td colspan="10">'+table_clone+'</td></tr>');
            
            if($('#'+iTableCounter).length > 0) {

                $('#'+iTableCounter).addClass('transparent');
                
                if($('input.item-' + parent_name + ':checked').length > 0) {
                    var list_sku = [];
                    $.each($('input.item-' + parent_name + ':checked'), function(i,j){
                        list_sku.push( $(j).val() );
                    });
                }

                var parent_value = get_parent_value(list_sku);
                parent_value.success(function(parent){
                    
                    d = parent;
                    d['key'] = parent_name;
                    d['id_product'] = id_product_val;
                    d['reference'] = parent_sku_val;
                    d['parent_sku'] = parent_sku_val; 
                    
                    row_details(d, true);
                    $('#'+iTableCounter).removeClass('transparent');     
                    
                    tr.next().checkBo();
                    tr.next().find('input[type=checkbox]').change().trigger('change');
                    tr.next().find('input[rel=force_chk]').change(function(){
                        show_force($(this).parents('.cb-checkbox'));
                    }).trigger('change');
                });
                parent_value.error( function(data, error) {
                    console.log(error, data);
                    $('#'+iTableCounter).removeClass('transparent');  
                });   
            }            
            tr.addClass('shown');          
        }
    });

    // ON SUBMIT
    $('#submit_product_options').on('click', function(){
        submit_form($(this));
    }); 

    // Filter by Category
    $('#category').on('change', function(){
        var cate = {};
        cate.id_category = this.value;

        $('#product_options').DataTable().column(0).search( cate.id_category ).draw();
    });

    /* upload template */
    $('#upload_product_options').on( 'click', function () {
        upload_file($(this));
    });

    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });

    // Filter by Date   
    $('#order-date-from').on('change', function(){
        filter_by_created_date();
    });

    $('#order-date-to').on('change', function(){
        filter_by_created_date();
    });

});

function filter_by_created_date(){

    if(($('#order-date-from').val() != '' && $('#order-date-to').val() != '') || ($('#order-date-from').val() == '' && $('#order-date-to').val() == '')) {

        $('#product_options').DataTable().column(1).search( $('#order-date-from').val() ).column(2).search( $('#order-date-to').val() ).draw();
        $('.notification').animate( { opacity: 0 }, { easing: 'swing', duration: 1000, complete: function(){} }); 
        $('#order-date-from').parent().removeClass('form-group has-error');
        $('#order-date-to').parent().removeClass('form-group has-error');

    } else {

        if($('#order-date-from').val() == '' && $('#order-date-to').val() != '') {

            notification_alert($('#l_missing_order-date-from').val(), 'bad'); 
            $('#order-date-from').parent().addClass('form-group has-error');

        }         
    }
}

function show_force(obj){
    var object = $(obj);
    if(object.next().length > 0){
        if(object.find('input[type=checkbox]').prop('checked') == true){
            object.next().show();
            object.next().find('input[rel="force"]').removeAttr('disabled');
        }else{
            object.next().hide();
            object.next().find('input[rel="force"]').attr('disabled', 'disabled');
        }      
    }
}

function parent_checked(obj){

    var object = $(obj);    
    $('.item-' + object.val()).prop('checked',  object.prop('checked')).change().trigger('change');    

    // Open this row  
    $( $('.item-' + object.val()) ).each(function(i,j){        
        set_id($(this));
    });
}

function set_id(obj){
    
    var object = $(obj);
    var tr = object.closest('tr');
    var row = table.row( tr );
    var data = row.data();

    if(data == null){ 
        return false; 
    }

    var iTableCounter = 'product_option_' + data.key;  

    if($('input.item-' + data.parent_sku_key + ':checked').length > 0) {
        var list_sku = [];
        $.each($('input.item-' + data.parent_sku_key + ':checked'), function(i,j){
            list_sku.push( $(j).val() );
        });
    }

    var parent_value = get_parent_value(list_sku);
    parent_value.success(function(parent){
        //console.log(parent);
        d = parent;
        d['key'] = data.parent_sku_key;
        d['id_product'] = data.id_product;
        d['reference'] = data.sku;
        d['parent_sku'] = data.parent_sku; 
        d['parent_sku_key'] = data.parent_sku_key;

        //console.log(d);
        row_details(d, true);
    });  

    if(object.is(':checked')){
       
        if(data){

            //set id product
            if(data.id_product){
                object.closest('td').
                append('<input type="hidden" name="product_options['+data.parent_sku_key+'][child]'+'[' +data.key+']'+'[id_product]" value="'+data.id_product+'" />');
            }

            if(data.id_product_attribute){
                object.closest('td').
                append('<input type="hidden" name="product_options['+data.parent_sku_key+'][child]'+'[' +data.key+']'+'[id_product_attribute]" value="'+data.id_product_attribute+'" />');
            }

            if(data.reference){
                object.closest('td').
                append('<input type="hidden" name="product_options['+data.parent_sku_key+'][child]'+'[' +data.key+']'+'[sku]" value="'+data.reference+'" />');
            }

            if(data.parent_sku){
                object.closest('td').
                append('<input type="hidden" name="product_options['+data.parent_sku_key+'][child]'+'[' +data.key+']'+'[parent_sku]" value="'+data.parent_sku+'" />'); 
            }
        }

        // set color
        object.closest('tr').find('td').addClass('active');

        // ASIN to input
        var asin1 = (data && data.asin1) ? data.asin1 : '';
        var asin = object.closest('tr').find('.asin1'); 
        asin.hide();
        if(asin.closest('td').find('input').length <= 0){
            asin.closest('td').append('<input type="text" name="product_options['+data.parent_sku_key+'][child]'+'[' +data.key+']'+'[asin1]" value="'+asin1+'" class="form-control" />');
        }

    } else {

        // remove color
        object.closest('tr').find('td').removeClass('active');

        // remove ASIN
        object.closest('tr').find('.asin1').show().closest('td').find('input').remove();

        var iTableCounter = 'product_option_' + data.key;   

        // remove data 
        object.closest('td').find('input[type="hidden"]').remove();

        if($('#'+iTableCounter).length > 0) {
            $('#'+iTableCounter).remove();
        }
    }

    //console.log($('input.item-' + data.parent_sku_key + ':checked'));
    if($('input.item-' + data.parent_sku_key + ':checked').length > 0) {
        $('#product_options').find('input[value="'+data.parent_sku+'"]').closest('tr').find('button.link.disabled').removeAttr('disabled').removeClass('disabled');
    } else {
        $('#product_options').find('input[value="'+data.parent_sku+'"]').closest('tr').find('button.link.details-parent-control').attr('disabled', true).addClass('disabled');
    }    
}

function row_details(data, is_parent){

    console.log(is_parent);
    var table_cloned = $("#product_option_" + data.key);    
    table_cloned.find('.id_product').val(data.id_product);
    table_cloned.find('.parent_sku').val(data.parent_sku);
    table_cloned.find('.sku').val(data.reference);
    table_cloned.find('.id_product_attribute').val(data.id_product_attribute);
    
    $.each( table_cloned.find('input'), function(i,j){

        var object = $(this);
        var rel = object.attr('rel');
        
        if(typeof rel != undefined && rel != null){

            // parent
            if(is_parent){  

                if(rel === "asin1"){
                    object.next().show();
                    object.closest('tr').remove();
                }                 
               
                object.attr('name', 'product_options['+data.key+']'+'[' +rel+']' );

             // child
            } else {

                if(rel === "force"){
                    object.removeAttr('disabled');
                }

                object.attr('name', 'product_options['+data.parent_sku_key+'][child]'+'[' +data.key+']'+'[' +rel+']' ); 
            }

        }

        if(typeof data[rel] != undefined){

            //console.log(rel,data[rel]);
            if(data[rel] == 'disable') {

                object.val('disabled');
                object.attr('readonly', true).attr('disabled', true);

                if(object.is(':checkbox') || object.is(':radio')){

                    object.closest('label').addClass('disabled').attr('disabled', true).css('cursor', 'not-allowed');
                    object.prop('checked', false);
                    object.attr('checked', false); 

                    if(object.is(':checkbox')) {
                        if(object.closest('label').find('p.error').length == 0) {
                            object.closest('label').append('<p class="error">'+$('#l_Mismatch').val()+'</p>');
                        }
                    } else {
                        if(object.closest('div').find('p.error').length == 0) {
                            object.closest('div').append('<p class="error">'+$('#l_Mismatch').val()+'</p>');
                        }
                    }

                } else {
                    object.hide();

                    if(object.parent().find('input[type="'+object.attr('type')+'"].disabled').length <= 0){
                        object.parent().append('<input type="'+object.attr('type')+'" readonly disabled class="disabled form-control"/>');
                    }

                    if(object.closest('td').find('p.error').length == 0) {
                        object.closest('td').append('<p class="error">'+$('#l_Mismatch').val()+'</p>');
                    }
                }
                
            } else {

                object.removeAttr('disabled');
                object.closest('td').find('p.error').remove();

                if(rel === "force"){
                    if(data[rel]){                       
                        object.closest('.row').show(); 
                        object.closest('td').find('input[rel="force_chk"]').prop('checked', true).change().trigger('change');
                        object.closest('td').find('input[rel="force"]').removeAttr('disabled');
                    }
                }

                if(object.is(':checkbox')){

                    object.val(1);
                    object.closest('label').removeClass('disabled').removeAttr('disabled').css('cursor', 'pointer');
                    object.closest('label').find('p.error').remove();

                    if(data[rel] == 1){
                        object.prop('checked', true).change().trigger('change');
                        object.attr('checked', true);  
                    } 

                } else if(object.is(':radio')){
                    
                    object.each(function(i,j){

                        object.val($(j).attr('data-content'));

                        $(j).closest('label').removeClass('disabled').removeAttr('disabled').css('cursor', 'pointer');
                        $(j).closest('div').find('p.error').remove();

                        if($(j).val() === data[rel]){
                            $(j).prop('checked', true).change().trigger('change');
                            $(j).attr('checked', true);  
                        }
                    });

                } else {
                    object.show().removeAttr('readonly');
                    object.parent().find('input[type="'+object.attr('type')+'"].disabled').remove();
                    object.val(data[rel]);
                }
            }
        }

    });  
}

function get_parent_value(sku){
    //call ajax 
    return $.ajax({
        url: base_url + "my_shop/get_product_option_by_sku",
        type: 'POST',
        dataType: 'json',
        data: {'sku' : sku},
    }); 

}

function format ( id, main, hidden ) {
    var html = '';
    if(hidden){
        var hidden = 'style="display:none"';
    }
    html = '<div class="row-edit p-10 p-l40" '+hidden+' >'+
               '<table cellpadding="5" cellspacing="5" border="0" class="table_detail" id="'+id+'" width="95%">' +
                    main +
               '</table>'+
           '</div>';
    return html;   
}

function submit_form(e){

    // show submitting
    addStatus(e);

    //call ajax 
    var formObj = $("#product_options_form");
    var formData = $("#product_options_form").serialize();
    $.ajax({
        url: base_url + 'my_shop/save_product_options',
        type: 'POST',
        data:  formData,           
        success: function(data, textStatus, jqXHR)
        {
            successStatus(e);
            reload_ajax();
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            addError(e, jqXHR);
            console.log(jqXHR, textStatus, errorThrown);
        }          
    });
       
}

function reset_product(id_product){
    
    //call ajax 
    $.ajax({
        url: base_url + 'my_shop/reset_product_options',
        type: 'POST',
        dataType: 'json',
        data: {'id_product': id_product},           
        success: function(data, textStatus, jqXHR)
        {
            console.log(data);
            reload_ajax();
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            console.log(jqXHR, textStatus, errorThrown);
        }          
    });
       
}

function reload_ajax(){
    var table = $('#product_options').DataTable();
    table.ajax.reload();
}

function successStatus(e){

    e.attr('disabled', false);
    e.closest('div').find('.status').find('i').removeClass('fa-spinner fa-spin').addClass('fa-check');
    e.closest('div').find('.status').find('p').find('span').html($('#l_Success').val());
}

function addStatus(e){

    e.attr('disabled', true);
    e.addClass('update');
    e.closest('div').addClass('form-group has-update');
    e.closest('div').find('.status').addClass('update').show();
   
    if(e.closest('div').find('.status').find('i').hasClass('fa-check')){
        e.closest('div').find('.status').find('i').addClass('fa-spinner fa-spin').removeClass('fa-check');
        e.closest('div').find('.status').find('p').find('span').html($('#l_Submit').val());
    }
}

function addError(e, r){    
    e.attr('disabled', false);
    e.closest('div').addClass('form-group has-error');
    e.closest('div').find('.status').find('i').removeClass('fa-spinner fa-spin').addClass('fa-minus');
    e.closest('div').find('.status').find('p').find('span').html($('#l_Error').val() + ' ' + r); 
}

function upload_file(e){

    $('#result_product').hide();

    var form = new FormData(e.closest('form'));
    
    //append files
    var file = $(e).closest('form').find('input[type="file"]').get(0).files[0];
    if (file) {   
        form.append('file-upload', file);
    }

    addStatus(e);
    $('.progressbar .progressCondition .progressLine').attr('data-value',5);
    $('.progressbar').removeClass('hidden');
    //call ajax 
    $.ajax({
        url: base_url + "/my_shop/product_options_upload_template",
        type: 'POST',
        data: form,             
        cache: false,
        contentType: false, 
        processData: false, 
        dataType: "json",
        progress: function(e) {
            if(e.lengthComputable) {
                var pct = (e.loaded / e.total) * 100;
                console.log(pct);
                $('.progressbar .progressCondition .progressLine').attr('data-value',pct);
            } else {
                console.warn('Content Length not reported!');
            }
        },
        success: function(d) {

            successStatus(e);
            reload_ajax();

            if(d.pass == true){

                setTimeout(function(){
                    upload_file_process();
                }, 1000);

                /*$('#result_product').show();
                if(d.no_upload){
                    $('#product_summary_upload').html(d.no_upload).parent().show();
                }
                if(d.no_success){
                    $('#product_summary_success').html(d.no_success).parent().show();
                }
                if(d.no_error){
                    $('#product_summary_error').html(d.no_error).parent().show();
                }

                if(d.has_error == true){
                    $('#product_with_error').show();
                    var error_display = $('#error_product');
                    error_display.html('');
                    var num = 0;
                    $.each(d.error_product, function(i,j){
                        //console.log(i,j);
                        if(num <= 10){
                            error_display.append('<p class="true-pink"><i class="fa fa-close"></i> '+i+' - '+j+'</p>');
                        }

                        num++;
                    });

                    if(num > 10){
                        error_display.append('<p class="true-pink">...</p>');
                        error_display.append('<p class="true-pink">...</p>');
                        error_display.append('<p class="true-pink">'+$('#l_please_check').val()+'</p>');
                    }

                } else {
                    //$('#importProductOptions').modal('toggle');
                }*/
            } else {
                if(d.error) {
                    addError(e, d.error);
                } else {
                    addError(e, $('#l_upload_fail').val());
                }
            }
        },        
        error: function(d,e) {
            alert($('#l_Error').val() + ', ' + d + ', ' + e);
        }
    }); 
}

function upload_file_process(){
   
    $.ajax({
        type: "get",
        url: base_url + "tasks/ajax_run_upload_product_options/"+$('#id_shop').val()+"/"+$('#shop_name').val(),
        dataType: 'json',
        timeout:0           
    });  

    setTimeout(function(){
        $('#importProductOptions').modal('toggle');
    }, 2000);
    
}

$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});
