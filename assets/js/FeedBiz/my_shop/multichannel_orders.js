function orderView(e) {
    $('#multichannelOrder .viewResult').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i></div>');
    var order_button = $(e);
    $.ajax({
        type: 'POST',
        url: base_url + 'webservice/view_order/' + $(order_button).val() + '/1/1',
        dataType: 'json',
        success: function (data) {
            //console.log($('#viewResult'), data);  
            $('#multichannelOrder .viewResult').html(data.result);
        },
        error: function (data) {
            console.log(data);    
        }
    });
}

$(document).ready(function() {    
    
    $('#my_orders').dataTable( {
         "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "bAutoWidth": false,
        "ajax": {
            "url": base_url + "my_shop/multichannel_orders_data",
            "type": "POST"
        },
        "columns": [ 
            { "data": "id_orders", "class" : "text-center", render: function(data){
                var id_orders = (data) ? '<button type="button" class="link" data-toggle="modal" data-target="#multichannelOrder" value="' + data + '" onclick="orderView(this);">' + data + '</button>' : '';
                return id_orders;
            } },
            { "data": "seller_order_id", "class" : "p-l20"},
            /*{ "data": "id_marketplace_order_ref",  },*/
            { "data": "sales_channel",}, 
            { "data": "payment_method",}, 
            { "data": "language", "class" : "text-center"}, 
            { "data": "mp_channel",},  

            { "data": null, "class" : "text-center", render: function ( data ) {
                var detail = '';
                var comment = '';
                var style = 'badge badge-status';
                if(data.mp_channel_status == $('#fba_status_submited').val()) {
                    //style = '';
                }
                if(data.mp_channel_status == $('#fba_status_received').val()) {
                    //style = '';
                }
                if(data.mp_channel_status == $('#fba_status_invalid').val()) {
                    style = 'badge badge-cancelled';
                }
                if(data.mp_channel_status == $('#fba_status_planning').val()) {
                    //style = '';
                }
                if(data.mp_channel_status == $('#fba_status_processing').val()) {
                    //style = '';
                }
                if(data.mp_channel_status == $('#fba_status_cancelled').val()) {
                    style = 'badge badge-cancelled';
                }
                if(data.mp_channel_status == $('#fba_status_complete').val()) {
                    style = 'badge badge-complete';
                }
                if(data.mp_channel_status == $('#fba_status_completepartialled').val()) {
                    style = 'badge badge-complete';
                }
                if(data.mp_channel_status == $('#fba_status_unfulfillable').val()) {
                    style = 'badge badge-cancelled';
                }
                detail += '<span class="'+style+'">'+data.mp_channel_status+'</span>';

                if(data.mp_channel_status == '') {
                    if(data.fba_multichannel_auto) { // when sent fba_multichannel_auto fail ; re-send
                        style = 'badge badge-cancelled';         
                        if(data.comment) { 
                            comment = data.comment ; 
                        }                
                        detail += '<span class="'+style+'">' + $('#fba_status_fail').val() + '</span>';
                        detail += '<span class="fa-stack fa-lg" style="width:0px">'
                                +'<i class="fa fa-question-circle tooltips m-l5" title="'+comment+'"></i></span>';

                    }
                }

                return detail;
                
            } },                               
            { "data": "total_products", "class" : "text-right p-r10"}, 
            { "data": null, "class" : "text-right p-r10", render: function ( data ) {
                return '<span class="badge badge-info">'+data.total_paid+'</span>';
            } },
            { "data": "order_date", "class" : "text-center" }, 
                   
            { "data": null, "class" : "text-right p-r10", "bSortable": false, render: function ( data ) {
                var detail = '';

                if(!data.fba_multichannel_auto) { // when automatic send multi channel is inactive

                    if(data.mp_channel_status == '') { // when ready to send
                        if(data.action && data.action == 'updateOrder'){
                            detail += '<button type="button" class="btn-fba btn-danger btn btn-small" onclick="update_order(this,\''+data.country+'\');" value="'+data.seller_order_id+'">' + $('#update_order').val() + '</button>';
                        } else {
                            detail += '<button type="button" class="btn-fba create_fba btn btn-small" onclick="create_fba(this,\''+data.country+'\');" value="'+data.id_orders+'" rel="'+data.id_country+'">' + $('#create_fba').val() + '</button>';
                        }

                    } else { // when sent

                        detail += '<button type="button" class="btn-fba created_fba btn btn-small disabled"><span class="send-status"><i class="fa fa-check"></i> ' + $('#created_fba').val() + '</span></button>';

                    }

                } else { // when automatic send multi channel is active
                    detail += '<button type="button" class="btn-fba btn btn-small disabled">'+$('#automatically').val()+'</button>';                    
                }
                
                return detail;
            } }, 
            { "data": null, "class" : "text-center", "bSortable": false, render: function ( data ) {
                var detail = '';                
                if(data.mp_channel_status != '') { // when sent ; can view / cancel

                    detail += '<button type="button" class="info_fba link" data-toggle="modal" data-target="#infoFbaOrder" value="'+data.id_orders+'" onclick="info_fba(this);">' + $('#info_fba').val() + '</button> | ';

                    if( data.mp_channel_status != $('#fba_status_cancelled').val()) { // cancel

                        detail += '<button type="button" class="cancel_fba link" value="'+data.id_orders+'" onclick="cancel_fba(this,\''+data.country+'\');"><span class="send-status">' + $('#cancel_fba').val() + '</span></button>';

                    } else { // when  status cancelled ; can not cancel

                        detail += '<button type="button" class="cancel_fba link disabled" disabled value="'+data.id_orders+'" onclick="cancel_fba(this,\''+data.country+'\');"><span class="send-status">' + $('#cancel_fba').val() + '</span></button>';
                    }

                } else { // when waiting to send ; can not view / cancel

                    if(data.fba_multichannel_auto) { // when sent fba_multichannel_auto fail ; re-send                        
                        //detail += '<div class="tooltips" title="'+comment+'">' + $('#create_fba_fail').val() + '</div>';
                        detail += '<button type="button" class="btn-fba btn btn-small btn-success" onclick="create_fba(this,\''+data.country+'\');" value="'+data.id_orders+'" rel="'+data.id_country+'"><span class="send-status">' + $('#re_create_fba').val() + '</span></button>';

                    }
                }

                return detail;
            } }, 
           
        ],      
        "order": [/*[8, 'desc'],*/ [0, 'desc']],
        "pagingType": "full_numbers",    
        "drawCallback": function () {
            $('.tooltips').tooltipster({ 
                position:'right', 
                theme:'tooltipster-shadow', 
                contentAsHTML: true,
                animation: 'fade',
                touchDevices: true,
                interactive: true,
                maxWidth: 250,
            });
        },
        "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            if(aData.action && aData.action == 'updateOrder'){
                $(nRow).addClass('tr-red').attr('title', $('#update_not_complete').val()).tooltipster({ 
                //position:'top', 
                theme:'tooltipster-shadow', 
                contentAsHTML: true,
                animation: 'fade',
                touchDevices: true,
                interactive: true,
                maxWidth: 250,
            });                
            }
        }
    } );  

});

function update_order(e, country){   
    var fba_button = $(e);  
    bootbox.confirm( $('#confirm-update-message').val() + ", " + $(fba_button).val() + "?", function(result) {
        if(result){    
            fba_button.html('<i class="fa fa-spinner fa-spin"></i> ' + $('#shipping_fba').val()).addClass('disabled');
            $.ajax({
                type: 'GET',
                url: base_url + 'my_shop/multichannel_orders_update/' + $(fba_button).val(),
                dataType: 'json',
                success: function (data) {   
                    console.log(data);       
                    if(data.pass){
                        get_report(fba_button, 'fba_order_create_' + country) ;
                    } else {
                        addError(fba_button, pass.message);
                    }                  
                   
                },
                error: function (data) {
                    console.log(data);    
                }
            });
        }
    });
}

function create_fba(e, country){   
    var fba_button = $(e);  
    bootbox.confirm( $('#confirm-create-message').val() + ", " + $(fba_button).val() + "?", function(result) {
        if(result){    
            fba_button.html('<i class="fa fa-spinner fa-spin"></i> ' + $('#shipping_fba').val()).addClass('disabled');
            $.ajax({
                type: 'POST',
                url: base_url + 'my_shop/orders_multichannel/save_orders/' + $(fba_button).val() + '/'+ $(fba_button).attr('rel'),
                dataType: 'json',
                success: function (data) {   
                    console.log('fba_order_create_' + country);                            
                    get_report(fba_button, 'fba_order_create_' + country) ;
                },
                error: function (data) {
                    onsole.log(data);    
                }
            });
        }
    });
}

function info_fba(e){

    $('#viewResult').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i></div>');
    var fba_button = $(e);
    $.ajax({
        type: 'POST',
        url: base_url + 'my_shop/orders_multichannel/get_info_fba_order/' + $(fba_button).val(),
        dataType: 'json',
        success: function (data) {
            //console.log($('#viewResult'), data);  
            $('#viewResult').html(data.result);            
        },
        error: function (data) {
            console.log(data);    
        }
    });
    
}

function cancel_fba(e, country){

    var fba_button = $(e);
    bootbox.confirm($('#confirm_cancel_fba_1').val() + ' ' + $(fba_button).val() +' ? <br/> ' + $('#confirm_cancel_fba_2').val(), function(result) {
        if(result) {

            fba_button.attr('disabled', true).addClass('disabled');
            //notification_alert($('#confirm_cancel_fba').val() /*+ $(fba_button).val()  + */, 'good'); 

            $.ajax({
                type: 'POST',
                url: base_url + 'my_shop/orders_multichannel/cancel_fba_order/' + $(fba_button).val(),
                dataType: 'jsonp',
                success: function (data) {
                    get_report(fba_button, 'fba_order_cancel_' + country) ;
                    console.log(data);    
                },
                error: function (data) {
                    console.log(data);    
                }
            });
        }                    
    });
}

var running = true;
function get_report(e, act)
{
    console.log(act); 
    $.ajax({
        url: base_url + "users/ajax_get_sig_process/" + act,
        dataType: "json",
        beforeSend: function (jqXHR) {
            jqXHR.fail(function () {
                jqXHR.abort();
                setTimeout(function () {
                    get_report(act);
                }, 5000);
            });
        },
        success: function (r) {

            var timeout = r.timeout;
            var delay = r.delay;

            if (r.data) {            
                
                var status = r.data.status;
                var msg = r.data.msg;

                if (status == 4)
                { 
                    running = false;
                    addError(e, r.data.err_msg.title);
                    console.log(r.data.err_msg.title)
                }
                else if (status == 9)
                { 
                    running = false; 
                    if(msg.fba_order.error) {
                        console.log(msg.fba_order.error);
                        addError(e, msg.fba_order.error);
                    } else {
                        addStatus(e);
                    }
                }

            } 
            if (running) {
                setTimeout(function () {
                    get_report(act);
                }, delay);
            }
        }, error: function (xhr, status, error) {

            if (xhr.responseText) {
                console.log('Progress status error :', xhr.responseText);
                if (xhr.responseText === 'terminate') {
                    return;
                }
            }
        }
    });
}

function addError(e, msg){
    notification_alert(msg, 'bad');
    $(e).html('<i class="fa fa-warning"></i> ' + $('#fba_status_fail').val()).removeClass('create_fba disabled').addClass('fail_fba');
}

function addStatus(e){
    $(e).html('<i class="fa fa-check"></i> ' + $('#created_fba').val()).removeClass('fail_fba').addClass('create_fba disabled');
}