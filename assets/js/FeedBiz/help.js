$(document).ready(function () {
    function addSourceToVideo(element, src, type) {
        var source = document.createElement('source');

        source.src = src;
        source.type = type;

        element.append(source);
    }

    $('#config').on('show.bs.collapse', function () {
        $('#presents .in').collapse('hide');
        $('#amazonmarket .in').collapse('hide');
        $('#ebaymarket .in').collapse('hide');

        $('#presents a').addClass('collapsed');
        $('#amazonmarket a').addClass('collapsed');
        $('#ebaymarket a').addClass('collapsed');
    });
    $('#presents').on('show.bs.collapse', function () {
        $('#config .in').collapse('hide');
        $('#amazonmarket .in').collapse('hide');
        $('#ebaymarket .in').collapse('hide');

        $('#config a').addClass('collapsed');
        $('#amazonmarket a').addClass('collapsed');
        $('#ebaymarket a').addClass('collapsed');
    });
    $('#amazonmarket').on('show.bs.collapse', function () {
        $('#presents .in').collapse('hide');
        $('#config .in').collapse('hide');
        $('#ebaymarket .in').collapse('hide');

        $('#presents a').addClass('collapsed');
        $('#config a').addClass('collapsed');
        $('#ebaymarket a').addClass('collapsed');
    });
    $('#ebaymarket').on('show.bs.collapse', function () {
        $('#presents .in').collapse('hide');
        $('#amazonmarket .in').collapse('hide');
        $('#config .in').collapse('hide');

        $('#presents a').addClass('collapsed');
        $('#amazonmarket a').addClass('collapsed');
        $('#config a').addClass('collapsed');
    });

    $('.panel a').on('click', function () {
        var video = $(this).parents('.panel').find('video');
        
        if (video.find('source').length <= 0) {
            addSourceToVideo(video, $(this).attr('rel'), 'video/mp4');
        }
    });
});