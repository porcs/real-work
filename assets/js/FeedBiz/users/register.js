$(function() {
    
    //var base_url = location.origin;
    var loc = window.location.pathname;
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    dir = dir.substring(0, dir.lastIndexOf('/'));
    var base_url = dir;
    var user_pass=false;
    var email_pass=false;
    //console.log(dir);
    //console.log(window.location.pathname,location);
    function validateCaptcha(fn_callback)
    {
        var challengeField = $("input#recaptcha_challenge_field").val();
        var responseField = $("input#recaptcha_response_field").val();

        $.ajax({
            type: "POST",
            url: $('#captcha_url').val(),
            data: "recaptcha_challenge_field=" + challengeField + "&recaptcha_response_field=" + responseField,
             
            success: function(html){
                console.log(html);
                if(html === "true")
                {
                    $("#captchaStatus").html('').hide() ;
                    fn_callback();
                    return true;
                }
                else
                {
                    $("#captchaStatus").show();
                    $("#captchaStatus").html( $('#captcha_text_invalid').val() ).show(500);
                    Recaptcha.reload();

                    setTimeout(function(){
                        $("#captchaStatus").hide(500);
                    },5000);
                    return false;
                }
            }
        }); 
        
    }

    function validate()
    {
        $("#registerform").submit(function(e) {
                e.preventDefault();
            }).validate({
            errorElement: 'p',
            errorClass: 'help-inline error',
            focusInvalid: false,
            rules: { 
//                firstname: {
//                    required: true,
//                },
//                lastname: {
//                    required: true,
//                },
//                username: {
//                    required: true,
//                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "/users/check_register_email/",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#email" ).val();
                          }
                        }
                      }
                },
                password: {
                    required: true,
                    minlength: 6
                },
                con_password: {
                    equalTo: "#password"
                }
            },
            messages: {
//                firstname: {
//                    required: $('#firstname-message').val(),
//                },
//                lastname: {
//                    required: $('#lastname-message').val(),
//                },
//                username: {
//                    required: $('#username-message').val(),
//                },
                password: {
                    required: $('#password-message').val(),
                    minlength: $('#password-long-message').val()
                },
                con_password: {
                    equalTo: $('#con_password-message').val(),
                },
                email: {
                    required: $('#email-message').val(),
                    remote: $('#email-duplicate-message').val(),
                }
            },

            submitHandler: function(form) {
//                    if(!email_pass){$("#email").focus();return;}
//                    if(!user_pass){$("#username").focus();return;}
//                     
//                    if($('#agree').is(':checked') ){
//                        if ( validateCaptcha() )
//                        {
//                            form.submit();
//                        }
//                    }else
//                        $('#agree-error').show();
                    fn_callback = function(){
                        form.submit();
                    }
                    validateCaptcha(fn_callback);
                    
//                 form.submit();
            }

        });
    }
        
    Recaptcha.create($('#captcha_key').val(), 'captchadiv', {
        tabindex: 1,
        theme: "white",
        "lang": $('#captcha_lang').val(),
        callback: function() { 
            validate();
        }
    });

    $("#username_return_message").hide();
    $("#email_return_message").hide();
    
//        $("#username").change(function(){
//
//        $("#username_return_message").show();
//            $("#username_return_message").html('<i class="icon-spinner icon-spin orange bigger-125"></i>');
//            var username = $("#username").val();
// 
//            $.ajax({
//                type:"get",
//                url: base_url + "/users/check_register_username/" + username,
//            success: function(data) {
//                if(data === "true")
//                {
//                    $("#username_return_message").addClass("label-success");
//                    $("#username_return_message").removeClass("label-important");
//                    $("#username_return_message").html('<i class="icon-ok bigger-150"></i> ' + $('#username-available-message').val());
//                    user_pass=true;
//                }
//                else
//                {
//                    $("#username_return_message").addClass("label-important");
//                    $("#username_return_message").removeClass("label-success");
//                    $("#username_return_message").html('<i class="icon-warning-sign bigger-150"></i> ' + $('#username-duplicate-message').val());
//                    user_pass=false;
//                 }
//            },
//            error: function(jqXHR, textStatus, errorThrown) {
//                $("#username_return_message").html(errorThrown);
//            }
//            });
// 
//        });
var current_email = '';
$("#email").change(function() {
        //$("#email_return_message").show();
        //$("#email_return_message").html('<i class="icon-spinner icon-spin orange bigger-125"></i> ' + $('#checking').val());
        var email = $("#email").val();
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(email)){
            $.ajax({
                type: "get",
                url: base_url + "/users/check_register_email/" + email,
                success: function(data) {
                    if (data === "true")
                    {
                        /*$("#email_return_message").addClass("label-success");
                        $("#email_return_message").removeClass("label-danger");*/
                        $("#email_return_message").html('<i class="fa fa-check"></i> ' + $('#email-available-message').val()).delay(100).slideDown('fast');
                        email_pass = true;
                        current_email = email;
                    }
                    else
                    {
                        /*$("#email_return_message").removeClass("label-success");
                        $("#email_return_message").addClass("label-danger");
                        $("#email_return_message").html('<i class="fa fa-warning"></i> ' + $('#email-duplicate-message').val());*/
                        $("#email_return_message").hide();
                        email_pass = false;
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#email_return_message").html(errorThrown);
                }
            });
        }
    });
    
    $("#email").keydown(function(){
        if(current_email != $(this).val()){
            $("#email_return_message").hide();
        }
    });
});