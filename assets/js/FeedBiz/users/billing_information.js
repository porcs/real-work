$(function(){
       $('form input').each(function(){ 
        $(this).attr('def',$(this).attr('value'))
    })
    $('#reset_btn').click(function(){ 
        $('#company').attr('value', '');
        $('#addr').find('input').each(function(){ 
            $(this).attr('value', '');
        });
        $('form input').each(function(){
            if($(this).attr('def')!='') $(this).attr('value',$(this).attr('def'));
        })
    });
    $('#billinginfoform').validate({
        errorElement: 'p',
        errorClass: 'help-inline error',
        focusInvalid: false,
        rules: { 
            firstname:{
                required: true
            },
            lastname:{
                required: true
            },
            complement:{
                required: true
            },
            address1:{
                required: true
            },
            stateregion:{
                required: true
            },
            zipcode:{
                required: true
            },
            city:{
                required: true
            },
            country:{
                required: true
            }
        },
        messages: {
           
        },
        submitHandler: function(form) 
        {
            if($('select[name=country]').val()=='' || $('select[name=country]').val()==null){
                $('select[name=country]').parents('.country').addClass('has-error active').append('<p for="city" class="help-inline error"><i class="fa fa-exclamation-circle"></i> This field is required.</p>');
                return false;
            }else{
                $('select[name=country]').parents('.country').removeClass('has-error active').find('.help-inline.error').remove();
            }
            form.submit();
        }
    });
    
    $('select[name=country]').on('change',function(){
            if($('select[name=country]').val()==''){
                $('select[name=country]').parents('.country').addClass('has-error active').append('<p for="city" class="help-inline error"><i class="fa fa-exclamation-circle"></i> This field is required.</p>');
                return false;
            }else{
                $('select[name=country]').parents('.country').removeClass('has-error active').find('.help-inline.error').remove();
            }
    });
    
    
  
    
    });