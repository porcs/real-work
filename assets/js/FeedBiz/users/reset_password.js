$(function() {
    $('#reset_pass_form').validate({
        errorElement: 'p',
        errorClass: 'help-inline error',
        focusInvalid: false,
        rules: {
            new: {
                required: true,
                minlength: 8
            },
            new_confirm: {
                equalTo: "#new"
            }
        },
        messages: {
            new: {
                required: $('#provide_passsword_message').val(),
                minlength: $('#passsword_message').val()
            },
            new_confirm: {
                equalTo: $('#confirm_passsword_message').val(),
            },
        },


        invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-error', $('#reset_pass_form')).show();
        },

    });
});