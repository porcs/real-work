$(function() {
    $('input[name=userfile]').on('change',function(){
        $("#imageform").ajaxForm({
            target: '.profile_picture .icon-big-circle',
            replaceTarget:true,
        }).submit();
    });
  
    if($('#email').val() !== ''){
        var email_pass = true;
    }else{
        var email_pass = false;
    }
    $('.fileSelect').addClass('hide');
    $('.fileSelect').parent().hover(function(){
    	$('.fileSelect', this).removeClass('hide');
    }, function(){
    	$('.fileSelect', this).addClass('hide');
    });
    $.mask.definitions['~']='[+-]';
    $('.input-mask-date').mask('9999-99-99');

    $('#current_password_warning').hide();

    $( "#new" ).keydown(function() {
        if( $('#current').val() === '' )
            $('#current_password_warning').show();
    });

    $( "#current" ).keydown(function() {
        $('#current_password_warning').hide();
    });

    $('#validateSubmitForm').validate({
        errorElement: 'p',
        errorClass: 'help-inline error',
        focusInvalid: false,
        rules: {
            new: {
                minlength: 8
            },
            new_confirm: {
                equalTo: "#new"
            },
            email: {
                required: true,
                email: true
            },
            firstname: {
                    required: true,
                },
                lastname: {
                    required: true,
                },
           
        },
        messages: {
            new: {
                minlength: $('#password-message').val()
            },
            new_confirm: {
                equalTo: $('#password-not-match-message').val()
            },
             firstname: {
                required: $('#firstname-message').val(),
            },
            lastname: {
                required: $('#lastname-message').val(),
            },
            email: $('#email-message').val()
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-error', $('#profileform')).show();
        },
         submitHandler: function(form) {
                if($('#agree').length>0){  
                    if($('#agree').is(':checked') ){ 
                            form.submit(); 
                    }else
                        $('#agree-error').show();
                }else{
                     form.submit();
                }
                     
            }

    });

    $("#email_return_message").hide();

    $("#email").change(function(){  
$("#email_return_message").show();
$("#email_return_message").append('<i class="icon-spinner icon-spin blue"></i>');
var email = $("#email").val();

$.ajax({
type:"get",
url:"check_register_email/" +  email,
success: function(data) {
console.log(data);
                if(data === "true")
                {
                    $("#email_return_message").addClass("label-success");
                    $("#email_return_message").removeClass("label-important");
                    $("#email_return_message").html('<i class="icon-ok"></i>' + $('#email-available-message').val() + '<br>' + $('#email-note-message').val());
                    email_pass = true;
                }
                else
                {
                    $("#email_return_message").removeClass("label-success");
                    $("#email_return_message").addClass("label-important");
                    $("#email_return_message").html('<i class="icon-warning-sign"></i> ' + $('#email-duplicate-message').val());
                    email_pass = false;
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#email_return_message").html(errorThrown);
            }
});
});

    $('#profileform').submit(function(){
        return email_pass;
    })
});