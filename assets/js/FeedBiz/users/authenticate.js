$(document).ready(function()
{
    function alert_msg(type,msg){
        msg+='';
        if(type){//success
            var obj = $('.alert.alert-success');
        }else{
            var obj = $('.alert.alert-error');
        }
        $('.msg',obj).html(msg);
        obj.slideDown('slow');
    }

    var target = base_url+'tasks/ajax_create_profiles/';
    var current_url = base_url + 'users/' ;
    
    if($('#userdata_product').length>0){
        $.ajax({ url: target + 'product',type: 'POST', dataType: "json", cache: false,
             error: function(jqXHR, textStatus, errorThrown) 
             {  var txt = 'can not create product only  '+document.URL;
                if(jqXHR.status==404){
                    var txt = 'can not connect to register serv to create product only ';
                } 
                $.ajax({ url: current_url + 'authenticate_delete_user/', type: 'GET', data:{reason:txt}, cache: false });  
             }
         });
    }
    if($('#userdata_offer').length>0){
        $.ajax({ url: target + 'offer',type: 'POST', dataType: "json", cache: false,
             error: function(jqXHR, textStatus, errorThrown) 
             {  var txt = 'can not create offer only  '+document.URL;
                if(jqXHR.status==404){
                    var txt = 'can not connect to register serv to create offer only ';
                } 
                $.ajax({ url: current_url + 'authenticate_delete_user/', type: 'GET', data:{reason:txt}, cache: false });  
             }
         });
    }
    if($('#userdata_order').length>0){
        $.ajax({ url: target + 'order',type: 'POST', dataType: "json", cache: false,
             error: function(jqXHR, textStatus, errorThrown) 
             {  var txt = 'can not create order only  '+document.URL;
                if(jqXHR.status==404){
                    var txt = 'can not connect to register serv to create order only ';
                } 
                $.ajax({ url: current_url + 'authenticate_delete_user/', type: 'GET', data:{reason:txt}, cache: false });  
             }
         });
    }
    if($('#userdata_log').length>0){
        $.ajax({ url: target + 'order',type: 'POST', dataType: "json", cache: false,
             error: function(jqXHR, textStatus, errorThrown) 
             {  var txt = 'can not create log only  '+document.URL;
                if(jqXHR.status==404){
                    var txt = 'can not connect to register serv to create log only ';
                } 
                $.ajax({ url: current_url + 'authenticate_delete_user/', type: 'GET', data:{reason:txt}, cache: false });  
             }
         });
    }
    
    
    if($('#userdata').length>0 && $('#userdata').val()!=''){
        $.ajax({
             url: target + 'product',
             type: 'POST',
             dataType: "json",
             cache: false,
             success: function(data) 
             {
                if(data.pass === true)
                {
                    $.ajax({
                        url: target + 'offer',
                        type: 'POST',
                        dataType: "json",
                        cache: false,
                        success: function(data) 
                        {
                           if(data.pass === true)
                           {
                                /*****/
                                $.ajax({
                                    url: target + 'order',
                                    type: 'POST',
                                    dataType: "json",
                                    cache: false,
                                    success: function(data) 
                                    {
                                       if(data.pass === true)
                                       {
                                           alert_msg(true,'<b>Congratulation!</b> Registration success.'); 
                                       }
                                       else
                                       {
                                            console.log(data);
                                            alert_msg(false,'<b>Sorry!</b> Incomplete registration. Please contact us.');
                                            $.ajax({
                                               url: current_url + 'authenticate_delete_user/' + id,
                                               type: 'GET',
                                               data:{reason:'can not create order '+document.URL,u:u},
                                               cache: false,
                                               success: function(data) {
                                                   console.log(data);
                                                },
                                               error: function(jqXHR, textStatus, errorThrown) {
                                                   if(window)
                                                       console.log(jqXHR, textStatus, errorThrown);
                                               }
                                           }); 
                                       }
                                    },
                                    error: function(jqXHR, textStatus, errorThrown) 
                                    {
                                        if(window)
                                            console.log(jqXHR, textStatus, errorThrown);
                                        alert_msg(false,'<b>Sorry!</b> Incomplete registration. Please contact us.');
                                        $.ajax({
                                          url: current_url + 'authenticate_delete_user/' + id,
                                          type: 'GET',
                                          data:{reason:'can not connect create offer '+document.URL,u:u},
                                          cache: false,
                                          success: function(data) 
                                          {
                                               if(window)
                                                   console.log(data);

                                        },
                                          error: function(jqXHR, textStatus, errorThrown) 
                                          {
                                              if(window)
                                                  console.log(errorThrown);
                                          }
                                      }); 
                                    }
                                });       
                            }
                           else
                           {
                               alert_msg(false,'<b>Sorry!</b> Incomplete registration. Please contact us.');
                                $.ajax({
                                   url: current_url + 'authenticate_delete_user/' ,
                                   type: 'GET',
                                   data:{reason:'can not create offer '+document.URL},
                                   cache: false,
                                   success: function(data) {
                                       console.log(data);
                                    },
                                   error: function(jqXHR, textStatus, errorThrown) {
                                       if(window)
                                           console.log(jqXHR, textStatus, errorThrown);
                                   }
                               }); 
                           }
                        },
                        error: function(jqXHR, textStatus, errorThrown) 
                        {
                            if(window)
                                console.log(jqXHR, textStatus, errorThrown);
                            alert_msg(false,'<b>Sorry!</b> Incomplete registration. Please contact us.');
                            $.ajax({
                              url: current_url + 'authenticate_delete_user/' ,
                              type: 'GET',
                              data:{reason:'can not connect create offer '+document.URL},
                              cache: false,
                              success: function(data) 
                              {
                                   if(window)
                                       console.log(data);

                            },
                              error: function(jqXHR, textStatus, errorThrown) 
                              {
                                  if(window)
                                      console.log(errorThrown);
                              }
                          }); 
                        }
                    });       
                }
                else
                {
                     alert_msg(false,'<b>Sorry!</b> Incomplete registration. Please contact us.');
                     console.log(data.error);
                     $.ajax({
                        url: current_url + 'authenticate_delete_user/',
                        type: 'GET',
                        data:{reason:'can not create product '+document.URL},
                        cache: false,
                        success: function(data) 
                        {
                            console.log(data);
                        },
                        error: function(jqXHR, textStatus, errorThrown) 
                        {
                            if(window)
                                console.log(jqXHR, textStatus, errorThrown);
                        }
                    }); 
                }
             },
             error: function(jqXHR, textStatus, errorThrown) 
             {
                 if(window)
                     console.log(jqXHR, textStatus, errorThrown);
                  alert_msg(false,'<b>Sorry!</b> Incomplete registration. Please contact us.');

                if(jqXHR.status==404){
                    var txt = 'can not connect to register serv';
                }else{
                    var txt = 'can not create product section  '+document.URL;
                }
                $.ajax({
                   url: current_url + 'authenticate_delete_user/',
                   type: 'GET',
                   data:{reason:txt},
                   cache: false,
                   success: function(data) {
                        if(window)
                            console.log(data);

                    },
                   error: function(jqXHR, textStatus, errorThrown) {
                       if(window)
                           console.log(jqXHR, textStatus, errorThrown);
                   }
               }); 

             }
         });     
    }
 });