$(function() {
        $('#forgot_pass_form').validate({
            errorElement: 'p',
            errorClass: 'help-inline error',
            focusInvalid: false,
            rules: {
                email: {
                        required: true,
                        email:true
                }
            },

            messages: {
                email: {
                        required: $('#valid_email').val(),
                        email: $('#valid_email').val()
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                    $('.alert-error', $('#forgot_pass_form')).show();
            }

        });
    });