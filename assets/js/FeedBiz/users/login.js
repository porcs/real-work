function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}
if(inIframe()){
        $.ajax({url:'/users/clr_original_redirect'});
        setTimeout(function(){window.top.location.href = window.location.href ; },2000)  
    }
$(function() {
    
    $('#exampleInputEmail1').change(function(){
        var email = $(this).val();
        $.ajax({url:'/users/ajax_check_mfa_login',data:{email:email},method:'post',dataType: "json", 
            success: function(r) {
                if(r.result){
                    $('#mfa_otp_row').slideDown('slow');
                }else{
                    $('#mfa_otp_row').slideUp('slow');
                }
            }
        });
    });
    
    
    
        $('#loginform').validate({
            errorElement: 'p',
            errorClass: 'help-inline error',
            focusInvalid: false,
            rules: {
                email: {
                        required: true,
                        email:true
                },
                password: {
                        required: true
                }
            },

            messages: {
                email: {
                        required: $('#valid_email').val(),
                        email: $('#valid_email').val()
                },
                password: {
                        required: $('#specify_password').val()
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                    //$('.alert-error', $('#loginform')).show();
//                for(var i=0;i<validator.errorList.length;i++){
//                    validator.errorList[i].element.parentElement.className += " has-error";
//                    validator.errorList[i].element.className.replace('hidden', '');
//                }
//                for(var j=0;j<validator.successList.length;j++){
//                    validator.errorList[j].element.parentElement.className.replace('has-error', '');
//                    validator.errorList[j].element.className.replace('hidden', '');
//                }
            }

        });
    });