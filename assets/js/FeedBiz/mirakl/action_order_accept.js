$(document).ready(function () {
    
    get_accept_order_db();

    var table = $('#table_accept_order').DataTable();

    $('#btn_lookup').click(function (e) {
        e.preventDefault();

        if ($('#active_api').val() != 1) // true if inactive.
        {
            notification_alert($('#error-inactive-message').val(), 'bad');
            return;
        }

        if ($('#active_file').val() != 1) {
            notification_alert($('#error-active-file').val(), 'bad');
            return;
        }
        
        $obj = $(this);
        
        bootbox.confirm($('#send-accept-confirm').val(), function (result)
        {
            if (result)
            {
               get_accept_order($obj, table);
            }
        });
    });
});

function get_accept_order_db() {

    $('#table_accept_list').dataTable({
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "mirakl/getOrderAcceptList/" + $('#sub_marketplace').val() + "/" + $('#id_country').val() + "/" + $('#id_shop').val(),
            "type": "POST"
        },
        "columns": [
            {"data": "id_order_accept", "bSortable": true, "width": "5%"},
            {"data": "order_id", "bSortable": true},
            {"data": "order_date", "bSortable": true},
            {"data": "customer_name", "bSortable": true},
            {"data": "shipping_price", "bSortable": true},
            {"data": "total_price", "bSortable": true},
            {"data": "order_state", "bSortable": true},
            {"data": "tracking_no", "bSortable": false},
            {"data": "invoice_no", "bSortable": true}
        ],
        "pagingType": "full_numbers"
    });
}

function get_accept_order($obj, table) {

    $obj.find('i').removeClass('fa-search').addClass('fa-spinner fa-spin');
    $obj.attr('disabled', true);

    var option_data = {};
    option_data.sub_marketplace = $('#sub_marketplace').val();
    option_data.id_country = $('#id_country').val();
    option_data.order_id = '';

    var url = base_url + 'tasks/ajax_run_export/mirakl/' + $('#ext').val() + '/' + $('#id_shop').val() + '/accept_order/' + $('#id_user').val() + '/' + encodeURIComponent(JSON.stringify(option_data));
    //console.log(url); //return;
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'JSON',
        success: function (data) {

            if (data.status == 0) {
                notification_alert(data.message, 'bad');
            } else {
                generate_accept_order(data.optional, table);
            }

            $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-search');
            $obj.attr('disabled', false);
        },
        error: function () {
            console.log('Error');
            $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-upload');
            $obj.attr('disabled', false);
        }
    });
}

function generate_accept_order(optional, table) {

    var p = '';
    var row = 0;

    for (var index in optional) {
        p = optional[index];
        row = parseInt(index) + 1;

        var exported = '';

        if (p.export == 0) {
            exported = "<button type='button' class='btn btn-success' onclick=accept_order($(this),'" + p.order_id + "') ><i class='fa fa-upload'></i> " + $("#msg_accept").val() + "</button>";
        } else {
            exported = "<button type='button' class='btn btn-mini' disabled><i class='fa fa-check'></i> " + $("#msg_accept_auto").val() + "</button>";
        }

        table.row.add([
            '+',
            row,
            p.order_id,
            p.order_date,
            p.customer_name,
            p.shipping_price,
            p.total_price,
            p.status,
            exported
        ]).draw(false);

        $("#table_accept_order tbody").find('tr:eq(' + index + ')').find('td:eq(0)').addClass('details-control'); // add class details-control
    }

    $('#table_accept_order tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            $(this).removeClass('minus').addClass('plus').html('+');
        }
        else {
            // Open this row
            var data = row.data();
            var index = parseInt(data[1]) - 1;  // index is index from optional only!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            var row_order_id = data[2]; // index is order_id only!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            var header_detail = table_header_detail(row_order_id);

            row.child(header_detail).show();

            var $obj_table_detail = $('#' + row_order_id);

            if ($obj_table_detail.length > 0)
            {
                row_detail($obj_table_detail, optional[index].detail);
            }

            tr.addClass('shown');
            $(this).removeClass('plus').addClass('minus').html('-');
        }
    });
}

function row_detail($obj_table_detail, detail) {

    if (typeof (detail) === 'undefined') {
        console.log('undefined');
        return;
    }

    for (var index in detail) {
        var tr = $('<tr> </tr>');
        var td1 = $('<td></td>');
        var td2 = $('<td>' + detail[index]['order_line_index'] + '</td>');
        var td3 = $('<td>' + detail[index]['offer_id'] + '</td>');
        var td4 = $('<td>' + detail[index]['product_title'] + '</td>');
        var td5 = $('<td>' + detail[index]['status'] + '</td>');
        var td6 = $('<td>' + detail[index]['offer_sku'] + '</td>');
        var td7 = $('<td>' + detail[index]['shipping_price'] + '</td>');
        var td8 = $('<td>' + detail[index]['quantity'] + '</td>');
        var td9 = $('<td>' + detail[index]['price'] + '</td>');

        tr.append(td1);
        tr.append(td2);
        tr.append(td3);
        tr.append(td4);
        tr.append(td5);
        tr.append(td6);
        tr.append(td7);
        tr.append(td8);
        tr.append(td9);

        $obj_table_detail.append(tr);
    }
}

function accept_order($obj, order_id) {

    $obj.find('i').removeClass('fa-upload').addClass('fa-spinner fa-spin');
    $obj.attr('disabled', true);

    var option_data = {};
    option_data.sub_marketplace = $('#sub_marketplace').val();
    option_data.id_country = $('#id_country').val();
    option_data.order_id = order_id;

    var url = base_url + 'tasks/ajax_run_export/mirakl/' + $('#ext').val() + '/' + $('#id_shop').val() + '/accept_order/' + $('#id_user').val() + '/' + encodeURIComponent(JSON.stringify(option_data));
   // console.log(url);
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'JSON',
        success: function (data) {
            console.log(data);
            $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-upload');
            $obj.attr('disabled', false);
            
            if(data.status == 1){
                notification_alert(data.message, 'good');
                $obj.find('i').removeClass('fa-upload').addClass('fa-check');
                $obj.attr('disabled', true);
            }else{
                notification_alert(data.message, 'bad');
            }
        },
        error: function (error) {
            console.log('error');
            $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-upload');
            $obj.attr('disabled', false);
        }
    });
}

function table_header_detail(table_id) {
    var table_detail = $("#table_accept_order_detail").html();
    var temp_table = '';
    temp_table += '<div class="error_details_wrapper tabboder clearfix">';
    temp_table += '<div>ORDER ACCEPT DETAIL</div>';
    temp_table += '<table id="' + table_id + '" width="90%">';
    temp_table += table_detail;
    temp_table += '</table>';
    temp_table += '</div>';
    return temp_table;
}