function orderView(e) {
    $('#viewResult').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i></div>');
    var order_button = $(e);
    $.ajax({
        type: 'POST',
        url: base_url + 'webservice/view_order/' + $(order_button).val() + '/1',
        dataType: 'json',
        success: function (data) {
            //console.log($('#viewResult'), data);  
            $('#viewResult').html(data.result);
        },
        error: function (data) {
            console.log(data);    
        }
    });
}

$(document).ready(function () {

    var url = base_url + "mirakl/getOrderList/" + $('#sub_marketplace').val() + "/" + $('#id_country').val();
    //console.log(base_url);
    // DataTables initialisation
    $('#statistics').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url,
            "type": "POST"
        },
        "columns": [
            {"data": "id_orders", render: function(data){
                var id_orders = (data) ? '<button type="button" class="link" data-toggle="modal" data-target="#generalOrder" value="' + data + '" onclick="orderView(this);">' + data + '</button>' : '';
                return id_orders;
            }},
            {"data": "id_marketplace_order_ref"},
            {"data": "name"},
            {"data": null, render: function (data) {
                    return '<span class="badge badge-info">' + data.total_paid + '</span>';
                }},
            {"data": null, render: function (data) {
                    var detail = '';
                    if (data.order_status == 'Shipped') {
                        detail = '<span class="order_status shipped">' + data.order_status + '</span>';
                    } else if (data.order_status == 'Unshipped') {
                        detail = '<span class="order_status unshiped">' + data.order_status + '</span>';
                    } else if (data.order_status == 'Canceled') {
                        detail = '<span class="order_status canceled">' + data.order_status + '</span>';
                    } else {
                        detail = data.order_status;
                    }
                    return detail;
                }},
            {"data": "order_date"},
            {"data": "shipping_date"},
            {"data": "tracking_number"},
            {"data": "invoice_no"},
            {"data": null, 'class': 'text-center', render: function (data, i, j, k) {
                    //mirakl_messaging 
                    var detail = '';
                    if ($('#display_messaging').length > 0) {
                        if (data.flag_mail_invoice || data.flag_mail_review) {
                            detail += '<span class="msg" title="' + $('#flag_mail').val() + '"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i></span>';
                        } else {
                            detail += '<span class="msg" title="' + $('#flag_mail_fail').val() + '"><a href><i class="fa fa-envelope fa-lg text-success" aria-hidden="true"></i></a></span>';
                        }
                    }
                    return detail;
                }},
            {"data": null, render: function (data) {
                    var detail = '';
                    if (data.status == 1) {
                        detail = '<button type="button" class="send-orders btn btn-mini" disabled><i class=" fa fa-check"></i>&nbsp;<span class="send-status">' + $('#Sent').val() + '</span></button>';
                    } else {
                        detail = '<button type="button" class="send-orders btn btn-success btn-mini" value="' + data.id_orders + '" onclick="send_order(this);"><i class=" fa fa-upload"></i>&nbsp;<span class="send-status">' + $('#Send').val() + '</span></button>';
                    }
                    /*if(data.status == 'error') {
                     detail += ' <button type="button" class="btn btn-link btn-mini p-0" value="'+data.id_marketplace_order_ref+'" onclick="get_error(this);" style="color: red"><i class="fa fa-warning order-warning"></i></button>';
                     } */
                    return detail;
                }},
        ],
        "order": [[6, 'desc'], [0, 'desc']],
        "pagingType": "full_numbers",
        "columnDefs": [
            {className: "text-right p-r10", "targets": [3]}
        ],
        "initComplete": function () {
            if($('#display_messaging').length <= 0) {
                var api = this.api();            
                var column11 = api.column(9);
                console.log(column11.visible( false ));
            }
        } 
    });

    $('.mirakl-checkbox').on('click', function () {
        $acheckbox = $(this);
        if ($acheckbox.hasClass('checked')) {
            $acheckbox.find('input').prop('checked', false);//.change();
            $acheckbox.removeClass('checked');
        } else {
            $acheckbox.find('input').prop('checked', true); //.change();
            $acheckbox.addClass('checked');
        }

        var status = 'all'; // all orders status
        if (!$('#export-sent').prop('checked')) {
            status = 'error'; //order not send only
        }
        if (!$('#export-send').prop('checked')) {
            status = 1; //order not sent only
        }
        if (!$('#export-send').prop('checked') && !$('#export-sent').prop('checked')) {
            status = 'none'; //null
        }

        filter_table(status);
    });
});

function filter_table(val) {
    var table = $('#statistics').DataTable();
    table.column(9).search(val).draw();
}

function send_order(e)
{
    var $obj = $(e);
    var id_order = $obj.val();

    $obj.find('.send-status').html($('#Sending').val());
    $obj.find('i').removeClass('fa-upload').addClass('fa-spinner fa-spin');

    var url = base_url + "webservice/send_orders/" + id_order;
    //console.log(url);

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        success: function (rdata) {
            $('.validate').parent().hide();
            if (rdata.pass)
            {
                var rdisplay = $('#send-products-message').find('.status').show().find('p');
                rdisplay.html(rdata.output);
                //send.find('.send-status').closest('td').html(rdata.invoice[0]);
                $obj.find('.send-status').closest('td').prev().prev().html(rdata.invoice[0]);
                $obj.find('.send-status').closest('td').html('<button type="button" class="send-orders btn btn-mini" disabled><i class=" fa fa-check"></i>&nbsp;<span class="send-status">' + $('#Sent').val() + '</span></button>');
            }
            else
            {
                var rdisplay = $('#send-products-message').find('.status').show().find('p');
                rdisplay.html(rdata.error);
                $('#send-products-message').find('.status').find('.validate').removeClass('blue').addClass('pink');

                $obj.find('.send-status').html('Send');
                $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-upload');
            }
        },
        error: function (rerror) {
            console.log('error');
            $obj.find('.send-status').html('Send');
            $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-upload');
        }
    });
}

