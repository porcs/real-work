$(document).ready(function ()
{
    $("#download_delete_products").click(function (e) {
        e.preventDefault();

        if ($('#active_api').val() != 1) // true if inactive.
        {
            notification_alert($('#error-inactive-message').val(), 'bad');
            return;
        }

        if ($('#active_file').val() != 1) {
            notification_alert($('#error-active-file').val(), 'bad');
            return;
        }

        var $obj = $(this);
        $obj.find('i').removeClass('fa-download').addClass('fa-spinner fa-spin');
        $obj.attr('disabled', true);

        var option_data = {};
        option_data.reason_delete = $("#reason_delete").is(":checked") ? 1 : 0;


        var url = base_url + "mirakl/download_delete_product/" + $("#sub_marketplace").val() + "/" + $("#id_country").val() + "/" + encodeURIComponent(JSON.stringify(option_data));
        //console.log(url); return;
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                var arr = '';
                if (data[0] == '{') {
                    arr = JSON.parse(data);
                }

                if (typeof (arr.status) != "undefined" && arr.status == 0) {
                    notification_alert(arr.message, 'bad');
                } else {
                    var path_download = url + '/' + data;
                    window.location.href = path_download;
                }

                $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-download');
                $obj.attr('disabled', false);
            },
            error: function (err) {
                console.log('Error.');
                $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-download');
                $obj.attr('disabled', false);
            }
        });
    });
    
    


    $("#delete_products").click(function (e) {
        e.preventDefault();

        var $obj = $(this);
        bootbox.confirm($('#delete-products-confirm').val(), function (result)
        {
            if (result)
            {
                synchronize($obj);
            }
        });
    });



    function synchronize($obj)
    {
        if ($('#active_api').val() != 1) // true if inactive.
        {
            notification_alert($('#error-inactive-message').val(), 'bad');
            return;
        }

        if ($('#active_file').val() != 1) {
            notification_alert($('#error-active-file').val(), 'bad');
            return;
        }

        $obj.find('i').removeClass('fa-upload').addClass('fa-spinner fa-spin');
        $obj.attr('disabled', true);

        var option_data = {};
        option_data.sub_marketplace = $('#sub_marketplace').val();
        option_data.id_country = $('#id_country').val();
        option_data.reason_delete = $("#reason_delete").is(":checked") ? 1 : 0;

        var url = base_url + "tasks/ajax_run_export/mirakl/" + $('#ext').val() + "/" + $('#id_shop').val() + "/delete_products/" + $('#id_user').val() + "/" + encodeURIComponent(JSON.stringify(option_data));
        console.log(url);
        return;
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: function (r) {
                //console.log(r);
                if (r.status == false) {
                    notification_alert(r.txt, 'bad');
                }

                $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-upload');
                $obj.attr('disabled', false);
            },
            error: function (err) {
                console.log('Error.');
                $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-upload');
                $obj.attr('disabled', false);
            }
        });
    }
});
