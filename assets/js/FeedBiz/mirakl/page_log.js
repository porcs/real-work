
var table;

$(document).ready(function () {
    var table_language = {};
    table_language.emptyTable = $('#emptyError').val();
    table_language.info = $('#info').val();
    table_language.infoEmpty = $('#infoEmpty').val();
    table_language.infoFiltered = $('#infoFiltered').val();
    table_language.infoPostFix = $('#infoPostFix').val();
    table_language.thousands = $('#thousands').val();
    table_language.lengthMenu = $('#lengthMenu').val();
    table_language.loadingRecords = $('#loadingRecords').val();
    table_language.processing = $('#processing').val();
    table_language.search = '';
    table_language.zeroRecords = $('#zeroRecords').val();
    table_language.paginate = {};
    table_language.paginate.first = $('#paginate-first').val();
    table_language.paginate.last = $('#paginate-last').val();
    table_language.paginate.next = $('#paginate-next').val();
    table_language.paginate.previous = $('#paginate-previous').val();
    table_language.aria = {};
    table_language.aria.sortAscending = $('#aria-sortAscending').val();
    table_language.aria.sortDescending = $('#aria-sortDescending').val();
    var filter_ignore_inactive = true;
    $('head').append('<link rel="stylesheet" href="' + base_url + 'assets/css/feedbiz/mirakl/logs.css" />');
    $('#product_logs').dataTable({
        "language": table_language,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "mirakl/getLogDetail/" + $('#sub_marketplace').val() + "/" + $('#id_country').val(), // log detail
            "type": "POST",
            "data": {ignore_case: function () {
                    return filter_ignore_inactive;
                }
            }
        },
        "columns": [
            {"data": "batch_id", "bSortable": true, "width": "10%"},
            {"data": "feed_type", "bSortable": true, "width": "10%"},
            {"data": "reference_type", "bSortable": true, "width": "10%"},
            {"data": "reference", "bSortable": true, "width": "10%"},
            {"data": "message", "bSortable": false, "width": "50%"},
            {"data": "date_add", "bSortable": true, "width": "10%"}
        ],
        "pagingType": "full_numbers"
        , "fnDrawCallback": function (settings, data) {
            $('#product_logs_filter').show();
            $('#product_logs_info').show();
            $('#product_logs_paginate').show();
        }
    });

    table = $('#product_logs').DataTable();
    //  table.search($('#batch_id').val()).draw();

//    $('.batch_id').click(function () {
//        table.search(this.value).draw();
//    });

    $("#history_log").on("click", '.batch_id', function () {
        //console.log('s');
        table.search(this.value, this.value).draw();
    });

    $('#chk_ignore_inactive').change(function () {
        filter_ignore_inactive = $(this).is(':checked');
        table.draw();

    });

    $('#history_log').niceScroll({cursoropacitymax: 0.7, horizrailenabled: false, cursorborder: "", cursorcolor: "#919191", cursorwidth: "3px", autohidemode: true});

    scoll_page_down(1);

    $('.logs-filter').on('change', function () {
        if ($('#date_from').val() == "" && $('#date_to').val() == "" && $('#feed_type').val() == "") {
            if (clean_tbody()) {
                get_logs();
            }
        } else {
            get_query_logs();
        }
    });

});

var loading_flag = false;

function scoll_page_down(page)
{
    console.log('xxxx');
    var cal = ($('#history_log').height() + 22 * page);
    $('#history_log').scroll(function () {

        if ($('#history_log').scrollTop() + (cal) > $('#history_log_inner').height()) {
            if (loading_flag == false) {
                loading_flag = true;
                $('#history_log').unbind('scroll');
                get_logs(page);
            }
        }
    });
}

function get_query_logs()
{
    console.log('xxxx');
    var url = base_url + "mirakl/getLog/" + $('#sub_marketplace').val() + "/" + $('#id_country').val() + "/" + $('#id_shop').val();
    var data = $('.logs-filter').serialize();

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: 'json',
        success: function (data) {
            if (data == "") {

                $('#history_log_inner tbody').html('<tr><td colspan="4">' + $('#zeroRecords').val() + '</td></tr>');

            } else {

                if (clean_tbody()) {
                    reder_log_data(data);
                }

                $("#history_log").niceScroll({cursoropacitymax: 0.7, horizrailenabled: false, cursorborder: "", cursorcolor: "#919191", cursorwidth: "3px", autohidemode: true});
                $('#history_log').getNiceScroll().resize();
                loading_flag = true;

            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function get_logs(page)
{
    var pages = '';
    if (page) {
        pages = '/' + page;
    }

    $.ajax({
        type: "POST",
        url: base_url + "mirakl/getLog/" + $('#sub_marketplace').val() + "/" + $('#id_country').val() + "/" + $('#id_shop').val() + pages,
        dataType: 'json',
        success: function (data) {
console.log(data);
            if (data) {

                reder_log_data(data);

                $("#history_log").niceScroll({cursoropacitymax: 0.7, horizrailenabled: false, cursorborder: "", cursorcolor: "#919191", cursorwidth: "3px", autohidemode: true});
                $('#history_log').getNiceScroll().resize();
                loading_flag = false;
                scoll_page_down(page + 1);

            }

        },
        error: function (error) {
            console.log(error);
        }
    });
}

function clean_tbody() {
    $('#history_log_inner tbody').html('');
    return true;
}

function reder_log_data(data) { // render log detail
//console.log('xxxx');
    $.each(data, function (key, value) {
        //console.log(value);
        
        var cloned = $('table#main-logs tr').clone().appendTo($('#history_log_inner'));
        var cron = '';
        var feed_type = '';

        // time    
        cloned.find('.date').html(value.date_add_date);
        cloned.find('.time').html(value.date_add_time);

        //console.log(value);

        // action type
//        if (value.is_cron == "1")
//        {
//            cron = ' (' + $("#cron").val() + ')';
//        }
        cron = value.cron;
        feed_type = value.feed_type.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        cloned.find('.feed_type span').html(feed_type + cron);

        // batch id
        cloned.find('.batch_id_log').html('<button type="button" value="' + value.batch_id + '" class="link batch_id">' + value.batch_id + '</button>');
        cloned.find('.batch_id').click(function () {
            table.search(this.value).draw();
        });
        
        cloned.find('.status_log').html(value.log_status);
        cloned.find('.result_log').html(value.result);


//        if (value.feed_type == $("#FEED_TYPE_ACCEPT").val() || value.feed_type == $("#FEED_TYPE_IMPORT").val() || value.feed_type == $("#FEED_TYPE_UPDATE").val()) {
//            cloned.find('.status_log').html('<p class="logRow logSuccess"><i class="fa fa-check green"></i> ' + $('#success_log').val() + '</p>');
//            var count_process = (value.count_process - value.count_success);
//            var order_result = $('#result_log').val() + ' ' + value.count_success + ' ' + $('#success_order').val() + ', ' + count_process + ' ' + $('#skipped_order').val();
//            cloned.find('.result_log').html(order_result);
//            cloned.find('.result_log').html('<p class="logRow">' + order_result + '</p>');
//        } else {
//            var result = '';
//            if (typeof value.detail !== "undefined") {
//                var details = value.detail;
//
//                if (typeof details.error !== "undefined") {
//                    cloned.find('.status_log').html('<p class="logRow logError"><i class="fa fa-warning red"></i> ' + value.detail.error + '</p>');
//                } else {
//                    cloned.find('.status_log').html('<p class="logRow logSuccess"><i class="fa fa-check green"></i> ' + $('#success_log').val() + '</p>');
//                }
//            } else {
//                cloned.find('.status_log').html('<p class="logRow logSuccess"><i class="fa fa-check green"></i> ' + $('#success_log').val() + '</p>');
//            }
//
//            result = $('#result_log').val() + ' ' + value.count_success + ' ' + $('#items_to_send').val() + ', ' + value.count_skipped + ' ' + $('#skipped_items').val();
//            cloned.find('.result_log').html('<p class="logRow">' + result + '</p>');
//        }
        
        
    });
}