
$(document).ready(function () {
    $(".attribute_mirakl").change(function () {
        get_value_list($(this));
    });

    $('#attribute-mapping').find('select.colorpicker').each(function ()
    {
        $(this).feed_colorpicker();
    });
});

$(window).load(function () {
    //get_mapping_value();
});

function get_value_list($this) {
    var $self = $this;
    var value = $self.val();
    var id_attribute_group = $self.attr('rel');

    if (value == '') {
        $(".form_attribute_" + id_attribute_group).find('.select_attribute').empty();
        return false;
    }

    $.ajax({
        type: 'GET',
        url: base_url + "/mirakl/getValueList/" + value,
        async: false,
        dataType: 'json',
        success: function (data) {
            var select_attribute = $(".form_attribute_" + id_attribute_group).find('div[id^=\'select_attribute_\']');
            select_attribute.each(function () {
                var id_attribute = $(this).attr('rel');
                var html = '';
                html += '<select class="form-control" name="id_attribute_group[' + id_attribute_group + '][mapping][' + id_attribute + '][mirakl_attribute]" rel="feed_id_arrribute_' + id_attribute + '" >';
                for (var index in data) {
                    var p = data[index];
                    html += '<option value="' + p.id_value_list + '">' + p.label + '</option>';
                }
                html += '</select>';
                $(this).html(html);
                $(this).find('select').chosen().trigger("chosen:updated");
            });
        }
    });
}

$(".attribute_with_list").change(function () {
    var $attr_select = $(this);
    var $obj_field_marketplace = $attr_select.parent().parent().find('input[type=hidden]');
    var field_marketplace = $obj_field_marketplace.val();
    var field_shop = $attr_select.val();
    var selected = $attr_select.parents('.attribute_with_list_container').find('.attribute_with_list_value');

    var option_data = {};
    option_data.sub_marketplace = $('#sub_marketplace').val();
    option_data.id_country = $('#id_country').val();
    option_data.field_marketplace = field_marketplace;
    option_data.field_shop = field_shop;

    $.ajax({
        type: 'POST',
        url: base_url + "mirakl/getValueByFieldShop",
        data: option_data,
        success: function (data) {
            console.log(data);
            selected.html(data);
            selected.chosen().trigger("chosen:updated");
        }
    });
});

function more_value_list($obj) {
    //console.log('event more value list');
    var attribute_container = $obj.closest('.attribute_with_list_container');
    var id_config_value = attribute_container.attr('id_config_value');
    var id_config_attribute = attribute_container.attr('id_config_attribute');
    var bbbbb = attribute_container.find('.value_list_container');
    var id_config_value_list = attribute_container.find('.with_value_list:last').attr('id_config_value_list');
    var selected = [];

    attribute_container.find('.attribute_with_list_value:first > option').each(function (index, value) {
        selected[index] = [$(this).prop('value'), $(this).text()];
    });

    var option = {};
    option.id_config_value = id_config_value;
    option.id_config_value_list = id_config_value_list;
    option.search = 'sss';
    option.is_mapping = '';
    option.is_unmapping = '';
    option.start = 0;
    option.limit = 10;
    option.selected = selected;
    option.id_config_attribute = id_config_attribute;

    $.ajax({
        type: 'POST',
        url: base_url + "mirakl/getMapping",
        data: {data: option},
        success: function (returndata) {
            //console.log(returndata);
            bbbbb.append(returndata);
            attribute_container.find('.attribute_with_list_value').chosen().trigger("chosen:updated");
            attribute_container.find('.chosen-single div').remove();
            attribute_container.find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
        }
    });
}
