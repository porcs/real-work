
$(document).ready(function () {
    //Add
    $('#add').click(function () {
        add_model();
    });

    $('.edit_model').unbind('click').on("click", function () {
        edit($(this));
    });

    $('.remove_model').unbind('click').on("click", function () {
        remove($(this));
    });

    change_name(); // insert : name, id, data-content
});

$(window).load(function () {
    change_name();
    $('#count').html($("#tasks").find('.mirakl-model').length);
});

function replicate_string(str) {
    return str.replace(/^a-zA-Z0-9\s_/g, '').replace(/\s/g, '_');
}

// Add new Model
function add_model() {

    var cloned = $('#main').clone().show();
    $('#tasks').prepend(cloned);

    cloned.find('select').addClass('search-select');
    choSelect();

    cloned.attr('id', '');
    cloned.removeClass('showSelectBlock');

    var count_list = parseInt($('#count').text());
    count_list = count_list + 1;
    $('#count').html(count_list);

    cloned.find('.remove').on('click', function () {
        $('#count').html(parseInt($('#count').text()) - 1);
        $(this).parent().parent().remove();
    });

    cloned.find('select[rel=hierarchy_code]').on("change", function () {
        getSubHierarchy($(this));
    });

    cloned.find('input[rel=model_name]').focus();
}

function edit($obj) {
    if ($obj.closest('div.mirakl-models').find('div.mirakl-model').hasClass('showSelectBlock')) {
        $obj.closest('div.mirakl-models').find('div.mirakl-model').removeClass('showSelectBlock');
    } else {
        $obj.closest('div.mirakl-models').find('div.mirakl-model').addClass('showSelectBlock');
    }
}

function remove($obj) {

    // console.log($obj); return;

    var name = $obj.attr('rel');
    var id_model = $obj.attr('value');
    var id_shop = $('#id_shop').val();
    var id_country = $('#id_country').val();
    var sub_marketplace = $('#sub_marketplace').val();

    bootbox.confirm($('#message-delete').val() + ", " + name + "?", function (result) {
        if (result) {
            //var id_model = $($obj).val();

            $.ajax({
                type: "POST",
                url: base_url + "/mirakl/delete_model",
                data: {id_model: id_model, id_shop: id_shop, sub_marketplace: sub_marketplace, id_country: id_country},
                success: function (data)
                {
                    if (data) {
                        notification_alert($('#success-message').val() + ' ' + $('#delete-success-message').val() + ', "' + name + '".', 'good');
                        $('#count').html(parseInt($('#count').text()) - 1);
                        $($obj).closest('.mirakl-models').remove();
                    } else {
                        notification_alert($('#error-message').val() + ' ' + $('#delete-unsuccess-message').val() + ', "' + name + '".', 'bad');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    notification_alert($('#error-message').val() + ' ' + textStatus + ' ' + errorThrown + '.', 'bad');
                }
            });

        }
    });
}


//Change name
function change_name() {

    $('#tasks').on('blur', 'input[rel="model_name"]', function ()
    {
        var content = $(this).closest('.mirakl-models');
        var content_group = $(this).closest('div.form-group');
        var model_name = $.trim($(this).val());
        var id_model = content_group.find('input[rel="id_model"]').val();

        if (model_name)
        {
            if (content_group.hasClass('has-error'))
            {
                content_group.removeClass('has-error');
                content_group.find('.error').hide();
            }

            model_name = replicate_string(model_name);

            content.find('select, input, div, span').each(function ()
            {

                if (!$(this).attr('rel')) {
                    return;
                }

                if ($(this).is('select, input')) { // unlock input
                    if ($(this).attr('data-type') != 'LIST') {
                        $(this).removeAttr('disabled');
                        $(this).closest('.form-group').removeClass('disabled');
                    }
                }

                if ($(this).is('select')) { // unlock select
                    $(this).parent().find('.chosen-single div').remove();
                    $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                    $(this).chosen().trigger("chosen:updated");
                }

                var field_name = replicate_string($(this).attr('rel'));

                if (typeof (id_model) !== 'undefined') {
                    model_name = id_model;
                }

                var variable_prefix = 'model[' + model_name + ']';
                //var id_prefix = 'model_' + name;

                if (field_name == 'model_name') {
                    variable_prefix += '[model_name]';
                } else if (field_name == 'hierarchy_code') {
                    variable_prefix += '[hierarchy_code][]';
                }
                else if (field_name == 'attribute_with_list') {
                    var data_name = replicate_string($(this).attr('data-name')); // This is a id_config_value_list.
                    var data_attr = replicate_string($(this).attr('data-attribute')); // This is a id_config_attribute.

                    variable_prefix += '[attribute_with_hierarchy][' + data_attr + '][attribute_value][' + data_name + ']';
                }
                else if (field_name == 'default_value') {
                    var attr_name = replicate_string($(this).attr('data-name'));
                    variable_prefix += '[attribute_with_hierarchy][' + attr_name + '][default_value]';
                }
                else if (field_name == 'attribute_with_hierarchy') {
                    var attr_name = replicate_string($(this).attr('data-name'));
                    variable_prefix += '[attribute_with_hierarchy][' + attr_name + '][shop_attribute]';
                }

                if (field_name == 'model_name' || field_name == 'hierarchy_code' || field_name == 'attribute_with_list' || field_name == 'attribute_with_hierarchy' || field_name == 'default_value') {
                    $(this).attr('name', variable_prefix);
                    //$(this).attr('id', id_prefix);
                    $(this).attr('data-content', model_name);
                }
            });
        }
        else
        {
            if (!content_group.hasClass('has-error'))
            {
                content_group.addClass('has-error');
                content_group.find('.error').show();
            }

            content.find('select, input, div, span').each(function ()
            {
                if ($(this).is('select, input') && !$(this).is('input[rel="model_name"]')) {
                    $(this).attr('disabled', true);
                    if ($(this).is('select')) {
                        $(this).parent().find('.chosen-single div').remove();
                        $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                        $(this).chosen().trigger("chosen:updated");
                    }
                }
            });
        }
    });
}

function getSubHierarchy($select_hierarchy) {

    var $hierarchy = $select_hierarchy.closest('.hierarchy'); // get class hierarchy, so container all hierachy and attribute with hierarchy.
    var id_config_hierarchy = $select_hierarchy.val(); // get current id_config_hierarchy if you change current hierarchy.
    var model_name = $hierarchy.parent().find('[rel=model_name]').attr('data-content'); // get current name model.
    var is_hierarchy_master = typeof $select_hierarchy.attr('select-level') == 'undefined' ? 1 : 0; // is check hierarchy level = 1
    var select_level = $select_hierarchy.attr('select-level') || ''; // level in select hierarchy


    var id_config_hierarchy_array = [];

    if (is_hierarchy_master == 1) { // if hierarchy is master clear all id_config_hierarchy_array and set id_config_hierarchy_array = id_config_hierarchy (improtant)
        id_config_hierarchy_array[0] = id_config_hierarchy;
    } else {
        $hierarchy.find('[rel=hierarchy_code]').each(function (index) { // get each in hierarchy id ins array for get attribute (where in hierarchy id)
            var id_hierarchy_array = $(this).val();

            if (id_hierarchy_array != '') {
                id_config_hierarchy_array[index] = id_hierarchy_array;
            }

        });
    }

    var sub_marketplace = $('#sub_marketplace').val();

    $.ajax({
        type: 'POST',
        url: base_url + "/mirakl/getSubHierarchy",
        data: {id_config_hierarchy: id_config_hierarchy, id_config_hierarchy_array: id_config_hierarchy_array, sub_marketplace: sub_marketplace},
        dataType: 'json',
        success: function (data) {

            var $sub_hierarchy = $hierarchy.find('.sub_hierarchy'); // get object sub_hierarchy
            var $hierarchy_attribute = $hierarchy.find('.hierarchy_attribute'); // get object hierarchy_attribute
            $hierarchy_attribute.empty();

            if (id_config_hierarchy == '' && is_hierarchy_master == 1) { // clear all if not hierarchy_code and hierarchy level > 1
                $sub_hierarchy.empty();
                return false;
            }

            $sub_hierarchy.find("[class ^= 'sub_hierarchy_level_']").each(function () { // clear sub hierarchy
                var data_level = $(this).attr('data-level') || '';
                if (select_level < data_level) {
                    $(this).remove();
                }
            });

            if (id_config_hierarchy != '') { // add sub hierarchy
                $sub_hierarchy.append(data.result_hierarchy);
            }

            if (data.result_attribute != '') {
                $hierarchy_attribute.append(data.result_attribute);
            }

            /* set select option to class search select in attribute in hierarchy */
            $sub_hierarchy.find('select').addClass('search-select');

            /* set select option to class search select in attribute in hierarchy */
            $hierarchy_attribute.find('select').addClass('search-select');

            /* begin search select */
            choSelect();

            if (model_name != '') {
                set_name($select_hierarchy, model_name);
            }

        }
    });
}

function set_name($obj, name) {

    var content = $obj.closest('.mirakl-models');
    var content_group = $obj.closest('div.form-group');
    var model_name = name;
    var id_model = content.find('input[rel="id_model"]').val();

    if (content_group.hasClass('has-error'))
    {
        content_group.removeClass('has-error');
        content_group.find('.error').hide();
    }

    model_name = replicate_string(model_name);

    content.find('select, input, div, span').each(function ()
    {
        if (!$(this).attr('rel')) {
            return;
        }

        if ($(this).is('select, input')) { // unlock input
            if ($(this).attr('data-type') != 'LIST') {
                $(this).removeAttr('disabled');
                $(this).closest('.form-group').removeClass('disabled');
            }
        }

        if ($(this).is('select')) { // unlock select
            $(this).parent().find('.chosen-single div').remove();
            $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
            $(this).chosen().trigger("chosen:updated");
        }

        var field_name = replicate_string($(this).attr('rel'));

        //console.log(field_name);

        if (typeof (id_model) !== 'undefined') {
            model_name = id_model;
        }

        var variable_prefix = 'model[' + model_name + ']';
        //var id_prefix = 'model_' + name;

        if (field_name == 'model_name') {
            variable_prefix += '[model_name]';
        } else if (field_name == 'hierarchy_code') {
            variable_prefix += '[hierarchy_code][]';
        }
        else if (field_name == 'attribute_with_list') {
            var data_name = replicate_string($(this).attr('data-name')); // This is a id_config_value_list.
            var data_attr = replicate_string($(this).attr('data-attribute')); // This is a id_config_attribute.

            variable_prefix += '[attribute_with_hierarchy][' + data_attr + '][attribute_value][' + data_name + ']';
        }
        else if (field_name == 'default_value') {
            var attr_name = replicate_string($(this).attr('data-name'));
            variable_prefix += '[attribute_with_hierarchy][' + attr_name + '][default_value]';
        }
        else if (field_name == 'attribute_with_hierarchy') {
            var attr_name = replicate_string($(this).attr('data-name'));
            variable_prefix += '[attribute_with_hierarchy][' + attr_name + '][shop_attribute]';
        }

        if (field_name == 'model_name' || field_name == 'hierarchy_code' || field_name == 'attribute_with_list' || field_name == 'attribute_with_hierarchy' || field_name == 'default_value') {
            $(this).attr('name', variable_prefix);
            //$(this).attr('id', id_prefix);
            $(this).attr('data-content', model_name);
        }
    });
}

function getSelectShopAttributeGroup($select_attr_group) { // get attribute group for get value
    var option_data = {};
    option_data.sub_marketplace = $('#sub_marketplace').val();
    option_data.id_country = $('#id_country').val();
    option_data.field_marketplace = '';
    option_data.field_shop = $select_attr_group.val();

    var panel_attribute = $select_attr_group.closest('.panel_attribute');

    $.ajax({
        type: 'POST',
        url: base_url + "mirakl/getValueByFieldShop",
        data: option_data,
        success: function (data) {

            panel_attribute.find('.panel_attribute_value select').each(function () {
                $(this).html(data);
                $(this).chosen().trigger("chosen:updated");
            });
        }
    });
}

//$("#tasks").on("change", "select[rel='attribute_with_list']", function () {
//    
//    var $obj = $(this);
//    var v = $obj.val();
//    var a = $obj.attr('data-name');
//
//    if (v == 'E-default') {
//        $obj.parents('.panel_attribute').find('.panel_attribute_value').each(function () {
//            $(this).find('select').each(function () {
//                if (a != $(this).attr('data-name')) {
//                    $(this).val('');
//                    $(this).chosen().trigger("chosen:updated");
//                }
//            });
//        });
//    }
//
//});