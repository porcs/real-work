var table_error_logs;

$(document).ready(function () {

    // error log
    $('#error_logs').dataTable({
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "mirakl/getLogReportDetail/" + $('#sub_marketplace').val() + "/" + $('#id_country').val(),
            "type": "POST"
        },
        "columns": [
            {"data": "batch_id", "bSortable": true, "width": "10%"},
            {"data": "feed_type", "bSortable": true, "width": "10%"},
            {"data": "reference", "bSortable": true, "width": "10%"},
            {"data": "log_error", "bSortable": false, "width": "30%"},
            {"data": "log_warning", "bSortable": false, "width": "30%"},
            {"data": "date_add", "bSortable": false, "width": "10%"},
        ],
        "pagingType": "full_numbers"
    });

    table_error_logs = $('#error_logs').DataTable();

    // DataTables initialisation
    $('#product_logs').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "mirakl/getLogReport/" + $('#sub_marketplace').val() + "/" + $('#id_country').val(),
            "type": "POST",
//            "success": function(data){
//                console.log('data'); return;
//            }
        },
        "columns": [
            {"data": "batch_id", "bSortable": true, "width": "10%"},
            {"data": "feed_type", "bSortable": true, "width": "10%"},
            {"data": null, "bSortable": false, "width": "10%",
                render: function (data, type, row) {
                    var log_link = data.detail.ReportImportId;
                    return log_link;
                }
            },
            {"data": null, "bSortable": false, "width": "10%",
                render: function (data, type, row) {
                    var log_link = data.detail.ReportMode;
                    return log_link;
                }
            },
            {"data": null, "bSortable": false, "width": "20%",
                render: function (data, type, row) {
                    var message = data.detail.ReportMessage;
                    return message;
                }
            },
            {"data": null, "bSortable": false, "width": "5%",
                render: function (data, type, row) {
                    var log_link = data.detail.ReportProcess;
                    return log_link;
                }
            },
            {"data": null, "bSortable": false, "width": "5%",
                render: function (data, type, row) {
                    var log_link = data.detail.ReportSuccess;
                    return log_link;
                }
            },
            {"data": null, "bSortable": false, "width": "5%",
                render: function (data, type, row) {
                    var log_warning = data.detail.ReportWarning;
                    if(log_warning > 0){
                        return '<span style="color:orange;">' + log_warning + '</span>';
                    }
                    return log_warning;
                }
            },
            {"data": null, "bSortable": false, "width": "5%",
                render: function (data, type, row) {
                    var count_error = data.detail.ReportError;
                    if(count_error > 0){
                        return '<span class="feed_error">' + count_error + '</span>';
                    }
                    return count_error;
                }
            },
            {"data": null, "bSortable": false, "width": "10%",
                render: function (data, type, row) {
                    var date_add = data.detail.ReportDateCreate;
                    return date_add;
                }
            }
        ],
        "order": [[1, 'desc']],
        "pagingType": "full_numbers",
        "initComplete": function () {
        },
    });

    $('#product_logs tbody').on('click', 'tr td', function () {

        if ($(this).closest('tr').hasClass('selected')) {
            $(this).closest('tr').removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).closest('tr').addClass('selected');
        }

        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        //return;
        $('html, body').animate({
            scrollTop: $(".error_logs").offset().top
        }, 2000);


        $('#error_logs').closest('.error_logs').removeAttr('style');
        table_error_logs.search(data.batch_id).draw();
    });

    var table = $('#product_logs').DataTable();
    table.columns().eq(0).each(function (colIdx) {
        $('input', table.column(colIdx).footer()).on('keyup change', function () {
            table
                    .column(colIdx)
                    .search(this.value)
                    .draw();
        });
    });

});