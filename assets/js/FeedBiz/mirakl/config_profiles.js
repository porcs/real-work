var validation = false;
$(document).ready(function(){

        $('#back').on('click', function() {
            if($('#creation').length > 0)
                window.location.replace( base_url + "mirakl/models/" + $('#id_country').val());
            else
                window.location.replace( base_url + "mirakl/parameters/" + $('#id_country').val());
        });       
      
        //Add
        $('#add').click(function(){
            add_profile();
        });
        
        if($('input#popup_status_more').length > 0){
            //console.log($('#popup_more_no_model'));
            if($('#popup_more_no_model').length > 0){
            } else {
                add_profile();
            }
        }
        
        $('#mirakl-form').validate({   
            submitHandler: function(form) {
                if($('#popup_status_more').length > 0) {
                    form.submit();
                } else {
                    bootbox.confirm($('#submit-confirm').val(), function(result) {
                        if(result) {
                            form.submit();
                        }                    
                    });
                }
            }
        });
        
        applyRules();
});
    
function add_profile()
{
    var cloned = $('#main').clone().show();
    $('#tasks').prepend(cloned);

    cloned.attr('id', '');
    cloned.removeClass('showSelectBlock'); 

    var id_count = parseInt($('#count').text());
    var id_count_new = id_count+1;
    $('#count').html(id_count_new);

    cloned.find('.remove').on('click', function(){
        $('#count').html(parseInt($('#count').text()) - 1 );
        $(this).closest('div.mirakl-profiles').remove(); 
    });

    cloned.find('input[rel=name]').focus();

    //Recommended Browse Node
    cloned.find('.addRecommendedBrowseNode').click(function(){
        addRecommendedBrowseNode($(this));
    });
    
    //Price Rule
    cloned.find('.addPriceRule').click(function(){
        addPriceRule($(this));
    }); 
    
    cloned.find('input.price_rule').on('keydown', function(){
        clearPriceError($(this));
    });    
    
    cloned.find('select[rel="type"]').on('change', function(){
        changePriceRuleType($(this));
    });    
    
    cloned.find('input[rel="recommended_browse_node"]').on('blur', function()
    {
        recommended_browse_node($(this));
    });
    
    //Key Product Features
    cloned.find('input[rel="key_product_feature"]').click(function(){
        displayKeyProductFeatures($(this));
    });
    
    cloned.find('.addKeyProductFeatures').click(function(){
        addKeyProductFeatures($(this));
    });
    
    cloned.find('input[rel="out_of_stock"]').on('blur', function()
    {
        checkOutOfStock($(this));       
    });

    cloned.find('input[rel="[code_exemption][chk]"]').on('click', function()
    {
        checkCodeExamtion($(this));
    });

    cloned.find('select[rel="[code_exemption][concerns]"]').on('change', function()
    {
        var parent = $(this).closest('div.mirakl-profiles');

        if($(this).val() === "Manufacturer")
            getManufacturer(parent.find('.code_exemption_concerns_type'),  $(this).attr('data-content'));
        else
            parent.find('.code_exemption_concerns_type').html('');
    });

    //set select chosen
    cloned.find('select').addClass('search-select').chosen();
    
    cloned.find('.mirakl-radio').addClass('cb-radio');
    cloned.find('.mirakl-checkbox').addClass('cb-checkbox');
    cloned.checkBo();

    //update checkbox
    updateAcheckBox();

    //update radio
    updateAradio();    
}

function applyRules(){

    changeName();

    $('.removeRecommendedBrowseNode').unbind('click').on("click",function(){

        var ul = $(this).closest('div.form-group').parent();
        var li = $(this).closest('div.form-group');
        var count =  ul.find('input[rel="recommended_browse_node_count"]');

        li.remove();
        count.val( parseInt(count.val()) - 1 );

    });

    $('.addRecommendedBrowseNode').unbind('click').on("click",function(){
        addRecommendedBrowseNode($(this));
    });
    
    //Price Rule
    $('.addPriceRule').unbind('click').click(function(){
        addPriceRule($(this));
    }); 
    
    $('.removePriceRule').unbind('click').click(function(){
        $(this).closest('.price_rule_li').remove();
    }); 

    $('input.price_rule').unbind('keydown').on('keydown', function(){
        clearPriceError($(this));
    });    
    
    $('select[rel="type"]').unbind('change').on('change', function(){
        changePriceRuleType($(this));
    });    
    
    $('.removeKeyProductFeatures').unbind('click').on("click",function(){

        var ul = $(this).closest('div[rel="key_product_feature_1"]');
        var li = $(this).closest('div.form-group');
        var count =  ul.find('input[rel="key_product_feature_count"]');

        li.remove();
        count.val( parseInt(count.val()) - 1 );

    });

    $('.addKeyProductFeatures').unbind('click').on("click",function(){
        addKeyProductFeatures($(this));
    });

    $('.remove_profile').unbind('click').on("click",function(){
        remove($(this));
    });

    $('input[rel="recommended_browse_node"]').unbind('blur').on('blur', function()
    {
        recommended_browse_node($(this));
    });
    
    $('input[rel="key_product_feature"]').click(function(){
        displayKeyProductFeatures($(this));
    });
    
    $('input[rel="out_of_stock"]').unbind('blur').on('blur', function()
    {
        checkOutOfStock($(this));       
    });
    
    $('input[rel="[code_exemption][chk]"]').unbind('click').on('click', function()
    {
        checkCodeExamtion($(this));
    });
    
    $('select[rel="[code_exemption][concerns]"]').unbind('change').on('change', function()
    {
        var parent = $(this).closest('div.mirakl-profile');
        if($(this).val() === "Manufacturer")
            getManufacturer(parent.find('.code_exemption_concerns_type'), parent.attr('rel'));
        else
            parent.find('.code_exemption_concerns_type').html('');
    });
    
    $('.addSpecificField').unbind('click').on('click', function()
    {
        addManufacturer($(this));
    });
    
    $('.removeSpecificField').on('click', function()
    {
        $(this).closest('div.form-group').remove();
    });

    $('.edit_profile').unbind('click').on("click",function(){
        edit($(this));
    });
}

//Get Manufacturer
function getManufacturer(ele, name){
    
    $.ajax({
            type: 'POST',
            url: base_url + "/mirakl/get_manufacturers/" + name,
            dataType:'json',
            success: function(data) {
                ele.html(data);
                choSelect();
                ele.find('.addSpecificField').on('click', function()
                {
                    addManufacturer($(this));
                });
            },
            error: function(data){
                ele.html(data);
            }
        });
}

//ADD Recommended Browse Node
function addManufacturer(obj){
    var parent = obj.closest('div.mirakl-profiles').find('div.code_exemption_concerns_type');
    var cloned = obj.closest('div.form-group').clone().appendTo(parent).addClass('m-b10');
    
    var selected_select = cloned.find('select') ;
    selected_select.find('option[value=""]').remove() ;
    
    // Remove used options
    $('select[rel^="[code_exemption][manufacturer][]"] option:selected').each(function( index, option ) 
    {
        selected_select.find('option[value="' + option.value + '"]').remove() ;
    });

    if ( !selected_select.find('option').length )
    {
        cloned.remove();
        return(false) ;
    }

    updateChosen(cloned);    

    cloned.find('.addSpecificField').remove();  
    cloned.append('<i class="cb-plus bad removeSpecificField"></i>');
    cloned.find('.removeSpecificField').on('click', function()
    {
        $(this).closest('div.form-group').remove();
    });
}

//Change name
function changeName(){
    
    $('#tasks').on('blur', 'input[rel="name"]', function()
    {
        var content =  $(this).closest('div.mirakl-profiles');
        var content_group = $(this).closest('div.form-group');
        var duplicate = 0;
        var name = $.trim($(this).val()) ;

        if(name)
        {
            if(content_group.hasClass('has-error'))
            {
               content_group.removeClass('has-error');
               content_group.find('.error').hide();
            }

            name = name.replace(/[^a-zA-Z0-9\s_]/g,'').replace(/\s/g,'_');

            if($('input[name="profile[' + name + '][name]"]').length !== 0)
            {
                name = name + '_' + duplicate;
                duplicate++;                    
            } 

            content.find('label').removeClass('disabled');
            content.find('select, input, div, span').each(function() 
            {
                if ($(this).hasClass('disabled-none')){
                    $(this).removeClass('disabled');
                }

                if ( ! $(this).attr('rel') )
                    return ;
                
                if($(this).is('select, input')) {
                    $(this).removeAttr('disabled');
                    $(this).closest('.form-group').removeClass('disabled');
                }

                if($(this).is('select')) {
                    $(this).parent().find('.chosen-single div').remove();
                    $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                    $(this).chosen().trigger("chosen:updated");
                }
                
                var field_name = $(this).attr('rel') ;
                
                if( $(this).attr('rel') != "type" 
                && $(this).attr('rel') != "from" 
                && $(this).attr('rel') != "to" 
                && $(this).attr('rel') != "value")
                {
                    var variable_prefix = 'profile[' + name + '][' + field_name + ']' ;
                    var id_prefix = 'profile_' + name + '_' + field_name ;

                    if(field_name === "recommended_browse_node")
                    {
                        variable_prefix = variable_prefix + '[]';                       
                    }

                    if(field_name === "[code_exemption][chk]" 
                    || field_name === "[code_exemption][feild]" 
                    || field_name === "[code_exemption][concerns]" 
                    || field_name === "[code_exemption][private]"
                    || field_name === "[code_exemption][manufacturer][]"
                    || field_name === "[key_product_features][descriptions]"
                    || field_name === "[key_product_features][features][value][]"
                    || field_name === "[key_product_features][features][set_name]"
                    || field_name === "[recommended_browse_node][item_type]"
                    || field_name === "[price_rules][rounding]"
                    || field_name === "[association][condition_note]"
                    || field_name === "[association][latency]"
                    || field_name === "[category][repricing]")
                    {
                        variable_prefix = 'profile[' + name + ']'+ field_name;
                    }
                    
                    if(field_name.match("additionnals")){ // add for additionnals array
                        variable_prefix = 'profile[' + name + ']'+ field_name;
                    }

                    $(this).attr('name', variable_prefix) ;
                    $(this).attr('id', id_prefix) ;
                    $(this).attr('data-content', name) ;   
                    
                    if(field_name.match("additionnals")){
                        applyAdditionnals(id_prefix);
                    }
                    
                    
                    
                }
            });  
            
            changeNamePriceRules(content, name);
        }
        else
        {
            if(!content_group.hasClass('has-error'))
            {
               content_group.addClass('has-error');
               content_group.find('.error').show();
            }

            content.find('select, input, div, span').each(function() 
            {
                if($(this).is('select, input') && !$(this).is('input[rel="name"]') ) {
                    $(this).attr('disabled', true);

                    if($(this).is('select')) {
                        $(this).parent().find('.chosen-single div').remove();
                        $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                        $(this).chosen().trigger("chosen:updated");
                    }
                }
            });  
        }

        $('#tasks').find('div[rel="variation_data"], div[rel="recommended_data"], div[rel="specific_fields"]').each( function()
        {
            var name = $(this).attr('data-content');
            var validation_rel = $(this).attr('rel');

            setValuseNames(validation_rel, name);
        });

    });
}

function changeNamePriceRules(content, name)
{
    var id = 0;
    var name = name;
    content.find('.price_rule').each(function() 
    {
        if ( ! $(this).attr('rel') )
            return ;

        var field_name = $(this).attr('rel') ;
        
        if(field_name === "type")
            id = $(this).parent().parent().parent().attr('rel');
                
        var variable_prefix = 'profile[' + name + '][price_rules][value]['+ id +'][' + field_name + ']' ;
        var id_prefix = 'profile_' + name + '_price_rules_value_' + id + '_' + field_name ;

        $(this).attr('name', variable_prefix) ;
        $(this).attr('id', id_prefix) ;
        $(this).attr('data-content', name) ;                   
        $(this).attr('data-row', id) ;                   

    }); 
}
//Remove
function remove(obj){

    var name = $(obj).attr('rel');

    bootbox.confirm( $('#message-delete').val() + ", " + name + "?", function(result) {

        if(result){                
            var id_profile = $(obj).val();
            var sub_marketplace = $('#sub_marketplace').val();
            var id_country = $('#id_country').val();
            var id_shop = $('#id_shop').val();

            $.ajax({
                type: "POST",
                url:  base_url + "/mirakl/delete_profile",
                data:{id_profile: id_profile, sub_marketplace: sub_marketplace, id_country: id_country, id_shop: id_shop},
                success: function(data) 
                {
                    if(data)
                    {
                        notification_alert($('#success-message').val() + ' ' + $('#delete-success-message').val() + ', "' + name + '".', 'good'); 
        
                        var id = parseInt($('#count').text()) - 1 ;
                        $('#count').html(id);
                        $(obj).closest('div.mirakl-profiles').remove();

                        $('#main').find('select[id="duplicate-profile"] option').each(function(){
                            if($(this).attr('value') !== "")
                            {
                                if(name === $(this).text())
                                    $(this).remove();
                            }
                        });
                    }
                    else
                    {
                        notification_alert($('#error-message').val() + ' ' + $('#delete-unsuccess-message').val() + ', "' + name + '".', 'bad');                        
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) 
                {
                    notification_alert($('#error-message').val() + ' ' + textStatus + ' ' + errorThrown + '.', 'bad');                    
                }
            });
        }
    });
}

//ADD Recommended Browse Node
function addRecommendedBrowseNode(obj){

    var ul = obj.closest('.recommended_browse_node_main');
    var count =  ul.parent().find('input[rel="recommended_browse_node_count"]');

    if(count && count.val() < 4)
    {
        var cloned = ul.clone().appendTo(ul.parent());
        cloned.removeClass('recommended_browse_node_main');
        cloned.find('.addRecommendedBrowseNode').remove();
        cloned.find('.removeRecommendedBrowseNode').show().click(
            function(){
                cloned.remove();
                count.val( parseInt(count.val()) - 1 );
            });
        count.val( parseInt(count.val()) + 1 );
        cloned.find('.error').hide();
        cloned.removeClass('has-error');
        var cloned_id = cloned.find('input[rel="recommended_browse_node"]').attr('id');
        cloned.find('input[rel="recommended_browse_node"]').attr('id', cloned_id + '_' + count.val());
        cloned.find('input[rel="recommended_browse_node"]').on('blur', function()
        {
            recommended_browse_node($(this));
        }).val('');
    }
}

//ADD Recommended Browse Node
function addPriceRule(obj){

    var ul = obj.closest('div.price_rule_main');
    var li = obj.closest('div.price_rule_li');
    
    //Check value
    if(checkPriceRange(li))
    {
        var count =  ul.find('input[rel="price_rule_count"]');
        var cloned = li.clone().appendTo(ul);
        
        li.find('input[rel="to"]').on('blur',
        function(){
            checkPriceRange(li);
        });
        li.find('input.price_rule').on('keydown', function(){
            clearPriceError($(this));
        }); 
        li.find('.addPriceRule').remove();
        li.find('.removePriceRule').show().click(
        function(){
            li.remove();
        });              
        cloned.find('select[rel="type"]').on('change', function () {
			changePriceRuleType($(this));
		});
        cloned.find('select[rel="type"]').val(li.find('select[rel="type"]').val());
        cloned.find('.addPriceRule').click(
        function(){
            addPriceRule($(this));
        });
        cloned.find('input[rel="from"]').val(parseFloat(li.find('input[rel="to"]').val()) + 1);
        cloned.find('input[rel="to"]').val('').focus();     
        cloned.find('input[rel="value"]').on('blur',
        function(){
            checkPriceRange(cloned);
        }).val('');
        cloned.find('input.price_rule').on('keydown', function(){
            clearPriceError($(this));
        });              
       
        count.val( parseInt(count.val()) + 1 );
        cloned.attr('rel', parseInt(count.val()) + 1 );
        var name = cloned.find('select[rel="type"]').attr('data-content');

        changeNamePriceRules(cloned, name);

        updateChosen(cloned);
    }
}

function recommended_browse_node(obj)
{
    if(!myFunc($(obj))){                    
        $(obj).val("");
        $(obj).closest('div.form-group').addClass('has-error');
        $(obj).closest('div.form-group').find('.error').show();

    } else {
        $(obj).closest('div.form-group').removeClass('has-error');
        $(obj).closest('div.form-group').find('.error').hide();
    }
}

//Allow only digit
function myFunc(txt) {
    var value = txt.val();
    var re = /^([0-9+-]+[\.]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?|[0-9]+)$/g;

    if (!re.test(value)) 
        return false;
    else
        return true;
}

//Key Product Features
function displayKeyProductFeatures(obj){

    obj.closest('div.key_product_features').find('div[rel^="key_product_feature_"]').hide();
    if(obj.closest('div.key_product_features').find('div[rel="key_product_feature_'+ obj.val() +'"]').length > 0)
    {
        obj.closest('div.key_product_features').find('div[rel="key_product_feature_'+ obj.val() +'"]').show();
    }
}

//ADD Key Product Features
function addKeyProductFeatures(obj){

    var ul = obj.closest('div.key_product_feature_1_inner');
    var li = obj.closest('div.key_product_features_main');
    var count =  ul.closest('div[rel="key_product_feature_1"]').find('input[rel="key_product_feature_count"]'); 

    if(count && count.val() < 4)
    {
        var cloned = li.clone().appendTo(ul);
        cloned.removeClass('key_product_features_main');
        cloned.find('.addKeyProductFeatures').remove();
        cloned.find('.removeKeyProductFeatures').show().click(
        function(){
            cloned.remove();
            count.val( parseInt(count.val()) - 1 );
        });
        count.val( parseInt(count.val()) + 1 );      
        
        var selected_select = cloned.find('select[rel="[key_product_features][features][value][]"]');            
        var cloned_id = selected_select.attr('id');
        selected_select.attr('id', cloned_id + '_' + count.val());
        
        // Remove used options
        ul.find('select[rel="[key_product_features][features][value][]"] option:selected').each(function (index, option)
        {
            selected_select.find('option[value="' + option.value + '"]').remove();
        });

        if (!selected_select.find('option').length)
        {
            cloned.remove();
            return(false);
        }
        updateChosen(cloned);        
    }
}

//Check Price From To
function checkPriceRange(obj)
{        
    var price_from = $(obj).find('input[rel="from"]');
    var price_to = $(obj).find('input[rel="to"]');
    var value = $(obj).find('input[rel="value"]');        
    
    if(! value.val().length || ! price_from.val().length || ! price_to.val().length){
        if(! value.val().length){
            value.closest('div.input-group').css('border','1px solid red');            
        }
        if(! price_from.val().length){
            price_from.closest('div.input-group').css('border','1px solid red');            
        }
        if(! price_to.val().length){
            price_to.closest('div.input-group').css('border','1px solid red');            
        }
        $(obj).find('.has-error').show().find('span').html($('#none-value').val());
        validation = true;
        return(false) ;
        
    } else if( parseFloat(price_from.val()) >= parseFloat(price_to.val()) ) {
        
        price_to.parent().css('border','1px solid red');
        $(obj).find('.has-error').show().find('span').html($('#invalid-price-range').val());
        validation = true;
        return(false) ;
        
    } else {
        validation = false;
        return(true);
    }

}

function clearPriceError(obj)
{
    obj.closest('.input-group').removeAttr('style');
    obj.closest('div.price_rule_li').find('.has-error').hide();
}

function changePriceRuleType(obj)
{
    var type = $(obj).val();
    
    if(type == "percent"){
        $(obj).closest('div.price_rule_li').find('input[rel="value"]').closest('.input-group').find('.input-group-addon strong').html('%');
    } else if(type == "value") {
        $(obj).closest('div.price_rule_li').find('input[rel="value"]').closest('.input-group').find('.input-group-addon strong').html($('#currency_sign').val());
    }
}

function updateChosen(obj){
    obj.find('.chosen-container').remove();
    obj.find('.chosen-single div').remove();
    obj.find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
    choSelect();
}

function edit(obj){
    var cloned = $(obj).closest('div.mirakl-profiles').find('div.mirakl-profile');
    if($(obj).closest('div.mirakl-profiles').find('div.mirakl-profile').hasClass('showSelectBlock')) {
        cloned.removeClass('showSelectBlock');
    } else {
        cloned.addClass('showSelectBlock');
    }
    cloned.find('select').addClass('search-select').chosen();
    cloned.find('div').addClass('button-select');
    cloned.find('b').addClass('fa').addClass('fa-angle-down');
    //cloned.find('.mirakl-radio').addClass('cb-radio');
    //cloned.find('.mirakl-checkbox').addClass('cb-checkbox');
    //cloned.checkBo();
    
    
 //   console.log($(".additionnals.button-select").find('select').attr('id'));
    
//    $(".additionnals.button-select").find('[rel]').each(function(){
//        console.log($(this).attr('id'));
//    });

    $(".additionnals.button-select").find('select').each(function(){
        //console.log($(this).attr('id'));
        applyAdditionnals($(this).attr('id'));
    });
    
    
    
    //console.log($(".additionnals:not(.button-select)").find('[rel]').attr('rel'));
    
    //applyAdditionnals();
}

function checkOutOfStock(obj){
    if(!myFunc(obj)){
        obj.val("");
        obj.parent().addClass('has-error').find('.error').show();
    } else {
        obj.parent().removeClass('has-error').find('.error').hide();
    }
}

function checkCodeExamtion(obj){
    var parent = obj.closest('div.mirakl-profiles');
    if(obj.is(':checked'))
    {
        parent.find('.code_exemption_field').show();
        parent.find('.code_exemption_concerns').show();
        parent.find('.code_exemption_concerns_type').show();
    }
    else
    {
        parent.find('.code_exemption_field').hide();
        parent.find('.code_exemption_concerns').hide();
        parent.find('.code_exemption_concerns_type').html('');
    }
}

var acheckBox = function($acheckbox){ 
    if ($acheckbox.hasClass('checked')) {
        $acheckbox.find('input').prop('checked', false).change();
        $acheckbox.removeClass('checked');
    } else {
        $acheckbox.find('input').prop('checked', true).change();
        $acheckbox.addClass('checked');
    }
}

var aRadio = function($acheckbox){ 
    var thisVal = $acheckbox.find('input').val();

    if (!$acheckbox.hasClass('checked')) {        
        $acheckbox.find('input').prop('checked', true).change();
        $acheckbox.addClass('checked');
    }
    $acheckbox.parent().find('.mirakl-radio').each(function(i, j){
        var inputVal = $(j).find('input').val();
        if(inputVal !== thisVal) {
            if ($(j).hasClass('checked')) {
                $(j).find('input').prop('checked', false).change();
                $(j).removeClass('checked');
            }             
        }
    });
}

// update checkbox
var updateAcheckBox = function(){
    if($('input[type="checkbox"]').length > 0) {
        $acheckbox       = $('input[type="checkbox"]');
        $acheckbox.on('click', function(){
            $acb_checkbox    = $(this).closest('.mirakl-checkbox');
            acheckBox($acb_checkbox);
        });
    }
}

// update radio
var updateAradio = function(){
    if($('input[type="radio"]').length > 0) {
        $acheckbox       = $('input[type="radio"]');
        $acheckbox.on('click', function(){
            $acb_checkbox    = $(this).closest('.mirakl-radio');
            aRadio($acb_checkbox);
        });
    }
}

function applyAdditionnals(id_prefix){
//    $('.additionnals').find.on('change', function(){
//        console.log( this );
//    });



    //var id_addition = ConvertValue(id_prefix);
    //var id_addition_text = ConvertValue(id_prefix+"[text]");
    var id_addition_text = ConvertValue(id_prefix.replace('[option]', '[text]'));
    var id_addition_option = ConvertValue(id_prefix.replace('[text]', '[option]'));
    
    //console.log(id_addition_option);
    
    
    $(id_addition_option).on("change", function(){
        if($(this).val() == 'D-9999'){
            
            
            
            $(id_addition_text).show();
            
          //  #profile_ssssse_\[additionnals\]\[service1-delai-mode-livraison-1\]\[option\]\[text\]
            
            //console.log(id_addition_text);
        }else{
            $(id_addition_text).hide();
            
            
        }

    });

//console.log('id_addition');
}

function ConvertValue(id)
{
    var test = id.replace(/[[]/g,'\\[');
    return "#" + test.replace(/]/g,'\\]');
    
}