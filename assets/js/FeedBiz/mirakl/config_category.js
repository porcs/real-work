
var just_cal = false;
var just_check_all = false;
$(document).ready(function () {

    getSelectedAllCategory();
    apply();
    cloneTree();

});

function init_rel_all_row() {
    var i = 1;
    if (just_cal)
        return;
    just_cal = true;
    $('.cp_down_btn').each(function () {
        if ($(this).parents('#main').length > 0)
            return;
        var row = $(this).closest('li');
        if (row.length == 0)
            return;
        row.attr('rel', i);
        i++;
    });
}

function init_rel_all() {
    $('#tree1').find('li').each(function (eleIndex) {
        $(this).attr('rel', (eleIndex + 1));
    });
}

function click_copy_val_sel(obj) {

    showWaiting(true);
    var time = 0;
    var num = $('body').find('select').length; // count selector in right tree.
    if (num > 200) {
        time = 200;
    }

    setTimeout(function () {
        //  init_rel_all_row();
        init_rel_all();
//        var p_row =  obj.closest('li');
//        var cur_pos = p_row.attr('rel');
//        var currentLeafOffset = $(p_row).offset();
//        var cur_sel_val = obj.closest('.treeRight').find('select').val(); 
//        var break_f = false;
//        var break_l = false; 

        var rel_begin = obj.closest("li").attr('rel');
        var val_begin = obj.closest('.treeRight').find('select').val();

        $('#tree1').find('li[rel]').each(function (eleIndex) {
            var index = (eleIndex + 1);
            if (index >= rel_begin) {
                $(this).find('select').each(function () {
                    var sel = $(this);
                    if (sel.val() == '') {
                        sel.val(val_begin);
                    }
                });
            }
        });

        showWaiting(false);
    }, time);


//        $('#tree1').find('li').each(function(eleIndex, eleObject){ 
//            
//            var rel = $(this).attr('rel'); 

//            if(typeof $(this).attr('rel') == 'undefined'){console.log('undef');return;} /* not into */
//            
//            var rel = $(this).attr('rel'); 
//            if(break_f)return; 
//            
//            if(currentLeafOffset.top < $(eleObject).offset().top){
//                var test_val;
//                var l =  $(this).find('select').length;
//                $(this).find('select').each(function(eleIndex){ 
//                         $(this).val(cur_sel_val);
//                    if(eleIndex*1==(l*1)-1){ 
//                         //test_val = $(this).val(); 
//                    }
//                });
//                 
//                if(test_val!=''){  
//                    break_f=true;
//                    //return;
//                }   
//                $(this).find('select').each(function(eleIndex){ 
//                    if(eleIndex*1==(l*1)-1){
//                            //$(this).val(cur_sel_val);
//                            /*$(this).val(cur_sel_val).chosen({
//                                disable_search_threshold: 1,
//                                no_results_text: "Nothing found!"
//                            }).trigger("chosen:updated");; */
//                    }
//                });                        
//                       
//            }else{ return;}

    //     });

}

function apply() {

    setTimeout(function () {
        $('li.expandAll').trigger('click');
    }, 100);

    $('.add_category').unbind('click').on('click', function () {
        expandCollapse($(this));
    });

    $('input[rel="id_category"]').unbind('click').change(function () {//.unbind('click')
        getProfile($(this));
        if (!$('.checkBaAll').hasClass('active'))
            recParCheck($(this));
        if (!just_check_all) {
            just_cal = false;
        }

    });

    function recParCheck(cur) {

        var par = cur.parents('ul').prev().prev();
        $(par).each(function (i, elm) {
            if ($(elm).hasClass('tree_item')) {
                par = $(elm).find('input[rel="id_category"]');//.first();
                if (!par.is(':checked') && cur.is(':checked')) {
                    par.prop('checked', true).change();
                    recParCheck(par);
                }
            }
        });
    }

    $.fn.enableCheckboxRangeSelection = function () {
        var lastCheckbox = null;
        var $spec = this;
        var lastType = null;
        $spec.unbind("click");
        $spec.bind("click", function (e) {

            lastType = $(lastCheckbox).find('input[type=checkbox]').is(':checked');// e.currentTarget.checked;
            var $target = this;//e.target
            if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {

                var $checkbox = $spec.slice(
                        Math.min($spec.index(lastCheckbox), $spec.index(this)),
                        Math.max($spec.index(lastCheckbox), $spec.index(this)) + 1
                        );

                $checkbox.find('input[type=checkbox]').each(function () {

                    $(this).prop({'checked': lastType}).change();
                    var chk = Boolean($(this).find('input').attr('checked'));

                    if ($(this)[0] !== lastCheckbox) {
                        switch (true) {
                            case chk :
                                $(this).parent().parent().find('.treeRight').remove();
                                break;
                            case !chk :
                                getProfile($(this).parent().find($("input[type=checkbox]")));
                                break;
                            default:
                                getProfile($(this).parent().find($("input[type=checkbox]")));
                                break;
                        }
                    }
                });
            }
            lastCheckbox = this;
        });
    };

    $('label.checkBa').enableCheckboxRangeSelection();

}
var elementTree;
function cloneTree() {
    var cloned = document.querySelectorAll("[data-tree][data-cloneable]");

    Array.prototype.forEach.call(cloned, function (element) {
        elementTree = element.cloneNode(true);
    });
    return elementTree = elementTree.outerHTML;
}

//Get Profile
function getProfile(e) {

    var ele = $(e).parents('.tree_item').parent();

    if ($(e).is(':checked'))
    {
        //console.log(ele.find('select.profile'));
        if (ele.find('select').length === 0)
        {
            if (elementTree != null) {
                var div = document.createElement("div");
                div.innerHTML = elementTree;
                ele.append(div);
                //ele.get( 0 ).insertAdjacentHTML('afterend',elementTree);
                //ele.append(elementTree);
            } else {
                ele.get(0).appendChild(cloneTree());
            }
        } else {
            ele.find('select').attr('name', ele.find('select').attr('data-name'));
        }
    } else {
        //ele.find('select').removeAttr('name'); // if uncheck and check all will remove name in select, can not save profile : 3/08/2016
        //ele.find('.treeRight').remove();
    }

    // Remove old chosen
    if (ele.find('div.chosen-container').length > 0) {
        ele.find('div.chosen-container:last').remove();
    }

    //choSelect();   

    return ele.find('select');
}

function getSelectedAllCategory() {
    showWaiting(true);
    var url = base_url + "mirakl/getSelectedAllCategory/" + $('#sub_marketplace').val() + '/' + $('#id_country').val() + '/' + $('#id_shop').val();
    //console.log(url);

//    if ($('#popup_status').length != 0)
//    {
//        url = url + '/' + $('#mode').val();
//    }

    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function (data) {
//            console.log(data);
//            return;
            if (data && data != "")
            {
                $('#has_category').show();
                $('#tree1').html(data);

                tree($('.tree'));

                apply();
                setTimeout(function () {

                    //$(".search-select").chosen();
                    //choSelect();
                    /*$(".search-select").chosen({
                     disable_search_threshold: 1,
                     no_results_text: "Nothing found!"
                     }).trigger("chosen:updated");*/
                    /*$("#tree1 .search-select").each(function(){
                     var size =$('option',this).size();
                     
                     if(size<=1){ 
                     $(this).parent().parent().remove();
                     
                     }else{ 
                     $(this).chosen({
                     disable_search_threshold: 1,
                     no_results_text: "Nothing found!"
                     }).trigger("chosen:updated");
                     }
                     });*/
                    showWaiting(false);
                    $('#page-loaded').show();
                    $('#page-load').hide();
                    //$('.custom-form').checkBo();
                    $('.uncheckBaAll').on('click', function () { // uncheck to clear profile
                        $('select').val('');
                    });
                }, 500);

                $('.chosen-single').find('div').addClass('button-select');
                $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');

            } else
            {
                showWaiting(false);
                $('#has_category').hide();
                $('#none_category').show();
                $('#page-loaded').show();
                $('#page-load').hide();
                $('.tree').hide();
                apply();
            }

        },
        error: function (data, error) {
            console.log(data);
            console.log(error);
        }
    });
}

function expandCollapse(e) {

    if (!e.hasClass('active')) {
        e.parent().parent().parent().find('ul.tree_child').slideDown();
        e.addClass('active');
    } else {
        e.parent().parent().parent().find('ul.tree_child').slideUp();
        e.removeClass('active');
    }
}

function expandCollapseAll(e) {

    var content = e.parent().parent().find('.tree-folder-content');

    if (content.children().length > 1) {
        content.show();
    } else {
        getSelectedAllCategory(e);
    }

}