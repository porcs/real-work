
$("#api_vertify").click(function () {

    notification_alert('Process is running.', 'good');

//    var auto_accept = $(".cb-checkbox").hasClass("checked") ? 1 : 0;
//    var active = $(".cb-state").html() == 'ON' ? 1 : 0;

    var option_data = {};
    option_data.api_key = $.trim($("#api_key").val());
    option_data.auto_accept = $(".cb-checkbox").hasClass("checked") ? 1 : 0;
    option_data.active = $(".cb-state").html() == 'ON' ? 1 : 0;

    var $obj = $(this);
    $obj.find('i').removeClass('fa-plug').addClass('fa-spinner fa-spin');
    $obj.attr('disabled', true);

    $.ajax({
        type: "POST",
        url: base_url + 'mirakl/verify/' + $('#sub_marketplace').val() + '/' + $('#id_country').val() + '/' + $('#marketplace').val(),
        dataType: "json",
        data: {data: option_data},
        success: function (data) {
            //console.log(data);
            if (data.code == 1) {
                if (data.logo != '') {
                    $("#logo").prop('src', data.logo);
                    $("#input_logo").prop('value', data.logo);
                }

                notification_alert(data.msg, 'good');
            } else {
                notification_alert(data.msg, 'bad');
            }

            $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-plug');
            $obj.attr('disabled', false);
        },
        error: function (data, error, state) {
            notification_alert(data, 'bad');
            $("#api_vertify").attr('disabled', false);

            $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-plug');
            $obj.attr('disabled', false);
        }
    });
});

