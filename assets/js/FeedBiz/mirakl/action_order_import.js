$(document).ready(function () {

    /* This page mapping with page_order.js */
    $('#date_start').datepicker({
        dateFormat: 'dd-mm-yy'
    });

    $('#btn_import_order').click(function () {

        if ($('#active_api').val() != 1) // true if inactive.
        {
            notification_alert($('#error-inactive-message').val(), 'bad');
            return;
        }

        if ($('#active_file').val() != 1) {
            notification_alert($('#error-active-file').val(), 'bad');
            return;
        }
        
        var $obj = $(this);

        bootbox.confirm($('#send-order-confirm').val(), function (result)
        {
            if (result)
            {
                
                $obj.find('i').removeClass('fa-download').addClass('fa-spinner fa-spin');
                $obj.attr('disabled', true);

                var option_data = {};
                option_data.sub_marketplace = $('#sub_marketplace').val();
                option_data.id_country = $('#id_country').val();
                option_data.date_start = $('#date_start').val().replace(/\//g, "-");
                option_data.import_status = $('#order_status').val();

                var url = base_url + "tasks/ajax_run_export/mirakl/" + $('#ext').val() + "/" + $('#id_shop').val() + "/import_order/" + $('#id_user').val() + "/" + encodeURIComponent(JSON.stringify(option_data));
                //console.log(url); return;

                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        //console.log(data); return;

                        if (data.status == 0) {
                            notification_alert(data.txt, 'bad');
                        }

                        $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-download');
                        $obj.attr('disabled', false);
                    },
                    error: function (data) {
                        console.log('Error');
                        console.log(data);
                        $obj.find('i').removeClass('fa-spinner fa-spin').addClass('fa-download');
                        $obj.attr('disabled', false);
                    }
                });
            }
        });
    });
});
