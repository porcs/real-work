$(function () {
     
    var step = $('input#wz_step').val(); 
    var $fb_wizard_title = $('input#fb_wizard_title').val();
    if (step) {  
        if (step != 'fin') {
            console.log($('#welcome_wizard_btn'));
            $('#welcome_wizard_btn').on('click',function(){ 
                /* == Colorbox Resize == */ 
                jQuery.colorbox.settings.innerWidth = '80%';
                jQuery.colorbox.settings.innerHeight = '90%';

                jQuery(window).resize(function () {
                    // Resize Colorbox when resizing window or changing mobile device orientation
                    resizeColorBox();
                    window.addEventListener("orientationchange", resizeColorBox, false);
                    console.log(cmd_obj, obj, step);
                });

                var resizeTimer;
                var WIDTH,HEIGHT,innerWidth,innerHeight;
                var checksize = function(){
                    WIDTH = $(window).width(); 
                    HEIGHT = $(window).height();
                    innerWidth = '70%';
                    innerHeight = '80%';

                    if(WIDTH >= 1280 && HEIGHT >= 768){
                        innerWidth = '70%';
                        innerHeight = '80%';
                    }else if(WIDTH >= 800 && HEIGHT >= 600){
                        innerWidth = '95%';
                        innerHeight = '95%';
                    }else{
                        innerWidth = '100%';
                        innerHeight = '100%';
                    }
                };
                    
                checksize();
                    
                function resizeColorBox() {
                    if (resizeTimer) {
                        clearTimeout(resizeTimer);
                    }
                    resizeTimer = setTimeout(function () {
                        if (jQuery('#cboxOverlay').is(':visible')) {
                            //jQuery.colorbox.resize({innerWidth: '80%', innerHeight: '90%'});
                            checksize();
                            jQuery.colorbox.resize({innerWidth: innerWidth, innerHeight: innerHeight});
                        }
                    }, 300);
                }
                
                
                /* == Colorbox End Resize == */
                $.colorbox({
                    iframe: true,
                    href: base_url + 'dashboard/iframe_popup_conf/' + step,
                    innerWidth: innerWidth,
                    innerHeight: innerHeight,
                    onComplete: function () {
                        $('#cboxTitle').text($fb_wizard_title);
                    },
                    onLoad: function () {
                        $('body').css('overflow-y', 'hidden');
                    },
                    onClosed: function () {
                        $('body').css('overflow-y', 'auto');
                    }
                });
            } );
        }
    }
 
});