var actions = document.getElementById('actions').value;
var running = true;
var loading_flag = send_offer = false;    
var count = 0;

$(document).ready(function(){
    $('#page-load').show();
    $('#popup_action').show();        

    get_report('get_report_'+$('#country').val());

    $('#create').on('click', function()
    {       
        var option_data = { };        
        option_data.exclude_manufacturer = ($('#exclude_manufacturer').is(':checked')) ? true : false;        
        option_data.exclude_supplier = ($('#exclude_supplier').is(':checked')) ? true : false;     
        option_data.only_selected_carrier = ($('#only_selected_carrier').is(':checked'))  ? true : false;     
        option_data.send_image = true ;     
        option_data.send_relation_ship = ($('#send_relation_ship').is(':checked')) ? true : false;     
        option_data.send_only_image = ($('#send_only_image').is(':checked')) ? true : false;     

        option_data.limiter = $('#limiter').val();

        if($('#debug').length > 0)
        {
            window.location.href = base_url + '/amazon/debugs/' + $('#id_country').val() + '/' + actions +'/' + encodeURIComponent(JSON.stringify(option_data)) ;
        }
        else
        {
            var running_process = check_process_running();  
            running_process.success(function(r){                 
                $('#page-load').hide();
                if(!r.status)
                {
                    $('#send-message').show();
                    $('#send-message').find('span').html(r.txt);
                    if(r.url && r.url !== "")
                        $('#send-message').append(' &nbsp; <a href="' +  base_url + '/' + r.url + '">' + $('#pay_now').val() + '</a>');
                }
                else
                {
                    create(option_data);  
                }
            });
            running_process.error(function(error){
                console.log(error);
            });  
        }
    });

    $('#back').on('click', function(){
        window.location.href = base_url + '/amazon/category/' + $('#id_country').val() + '/4/2';
    });

});

function check_process_running()
{
    return $.ajax({
        type: "get",
        url: base_url + "/tasks/check_running_process/amazon/"+$('#ext').val()+"/"+$('#id_shop').val()+"/create",
        dataType: 'json',
        timeout:0 
    });  
}

function create(option_data)
{
    running = true;
    $('#creation').hide();
    $('#popup_action').hide();        

    get_report(actions +  '_' + $('#country').val());

    $.ajax({
        type: "get",
        url: base_url + "/tasks/ajax_run_popup_market/amazon/"+$('#ext').val()+"/"+$('#id_shop').val()+"/create/" + encodeURIComponent(JSON.stringify(option_data)),
        dataType: 'json',
        timeout:0           
    });  
}

function get_report_inventory(status)
{
    return $.ajax({
        type: "get",
        url: base_url + "/amazon/get_report_inventory/" + $('#id_country').val(),
        dataType: 'json',   
        async: false,
        success: function(i) 
        { 
            running = false;
            $('#page-load').hide();
            $('#loading').hide();
            $('#loaded').show();
            $('#creation').show();                
            $('#create').show();

            if(status == 8)
            {
                $('#title_message').hide();
                $('#no_product').show();
            }
            else
            {
                if(i.amazon_no_product > 0)
                {
                    $('#title_message').show();
                    $('#no_product').hide();
                }
                else
                {
                    $('#title_message').hide();
                    $('#no_product').show();
                }
            }

            $('#feed_no_product').html(i.feed_no_product);
            $('#amazon_no_product').html(i.amazon_no_product); 
            $('#product_to_create').html(i.product_to_created).parent().parent().addClass('red').show();

            if(i.product_to_created <= 0)
            {
                $('#create').hide();
                $('#send-message').show().find('span').html($('#no_product_to_create').val());
            }
        },
        error: function(error){
            console.log(error);
        }
    });  
}

function get_report(act)
{
    $.ajax({ 
        url: base_url+"/users/ajax_get_sig_process/" + act, 
        dataType: "json",
        beforeSend:function(jqXHR){
            jqXHR.fail(function(){ 
                jqXHR.abort();
                setTimeout(function (){ get_report(act); }, 5000);
            });
        },
        success:function(r){ 
            console.log(r);

            $('#page-load').hide();

            var timeout = r.timeout;
            var delay = r.delay; 

            if(r.data){ 
                var status = r.data.status;
                var err_msg = r.data.err_msg;
                var msg = r.data.msg;

                if(status == 1)
                { /*running*/
                    if(act == actions + '_' + $('#country').val())
                    {
                        $('#page-load').show().find('h4').html('Connecting to Amazon.. ');
                        $('#creation').hide();
                        $('#loading').hide();
                        $('#loaded').hide();
                        $('#popup_action').hide();
                        window.location.href = base_url + "/amazon/send/" + $('#id_country').val() + "/2"; 
                    }
                    else
                    {
                        var loading =  $('#loading').show();
                        loading.find('#message h4').html(msg.title);
                        loading.find('#message h5').html(msg.report.message);
                    }
                }
                else if(status == 4)
                { /*error*/
                    running = false; 
                    $('#page-load').hide();
                    var mess = $('#loading').fadeIn().find('.alert').addClass('alert-danger');
                    if(err_msg.title)
                       mess.find('h4').html(err_msg.title);

                   mess.find('h5').html('Error: ' + err_msg.error);

                   mess.find('img').fadeOut();
                }
                else 
                { /*finished true status 8-9 */    
                    if(act != actions + '_' + $('#country').val())
                        get_report_inventory(status);
                } 

            }else{

                $('#page-load').show();                                    

                if(act == actions +  '_' + $('#country').val())
                {
                    $('#creation').hide();
                    $('#popup_action').hide();
                    window.location.href = base_url + "/amazon/send/" + $('#id_country').val() + "/2"; 
                }
                else
                {
                    count++;
                    $('#page-load').find('h4').html('Processing .. ');

                    if(count == 1)
                    {
                        if(r.error_msg)
                        {
                            get_report_inventory();
                            count = 0;
                        }
                    }
                }
                delay = timeout;
            }
            if(running){
                setTimeout(function (){ get_report(act); }, delay);
            }
        },error: function(xhr, status, error) { 

            $('#load').remove();
            if(xhr.responseText){
                console.log('Progress status error :',xhr.responseText);
                if(xhr.responseText === 'terminate'){
                    return;
                }
            }
        }
    });
}