var flag = false;
var actions = document.getElementById('actions').value;
var popup;
/*document.getElementById('secret_key').onclick = function () {
    var pass = document.getElementById('secret_key');
    pass.type = 'text';
};

document.getElementById('secret_key').onblur = function () {
    var pass = document.getElementById('secret_key');
    pass.type = 'password';
};*/
function popupwindow(url, title, w, h) {
    wLeft = window.screenLeft ? window.screenLeft : window.screenX;
    wTop = window.screenTop ? window.screenTop : window.screenY;

    var left = wLeft + (window.innerWidth / 2) - (w / 2);
    var top = wTop + (window.innerHeight / 2) - (h / 2);
    popup = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
   
    return popup;
}


$(document).ready(function () {

    $('#get_merchant_id').on('click', function () {
        var ext = $('#get_access_url_ext').val();
        var url = 'https://sellercentral.'+ext+'/gp/mws/registration/register.html';
        popupwindow( url, 'AmazonSellercentralRegistration', 775, 645);

        //console.log(popup);
        if($(popup).length > 0){
            $(popup).on('click', function (){
                $(popup).close();
            });
            
        }
        //console.log(window.opener.$("#mwsCredentials"));
        //window.open(url, "AmazonSellercentralRegistration", "width=500,height=200");
        return false;
    });

    var errorMessage = '<p><i class="fa fa-exclamation-circle"></i> ' + $('#field_required').val() + '</p>';

    $('#form-api-setting').validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        errorElement: 'div',
        errorClass: 'error text-left',
        focusInvalid: true,
        rules: {
            merchant_id: {required: true},
            aws_id: {required: true},
            secret_key: {required: true}
        },
        messages: {
            merchant_id: errorMessage,
            aws_id: errorMessage,
            secret_key: errorMessage
        },
        submitHandler: function (form)
        {            
            save_parameters();
        }
    });

    $('#form-api-setting').on('click', '#api_check', function ()
    {
        $('#status-message').find('p').html('');
        $('#api_check').closest('.form-group').removeClass('has-success');
        $('#api_check').closest('.form-group').removeClass('has-error');
        $('#api_check').find('i').removeClass('icon-check').addClass('fa fa-spinner fa-spin');
        $('#api_check').parent().find('.success').hide();
        $('#api_check').find('#check').hide();
        $('#api_check').find('#checking').show();

        $('#form-api-setting').find('.alert-message').hide();
        //$('#form-api-setting').find('.error').remove();
        
        $('#form-api-setting input[type="text"], #form-api-setting input[type="password"]').each(function(index, value){

            if($(value).attr('id') != "auth_token") {
                var thisElement = $(value);
                if(thisElement.val().length <= 0){
                    var parentElement = thisElement.closest('.form-group');
                    parentElement.addClass('has-error');
                    parentElement.append('<div for="merchant_id" class="error text-left">'+errorMessage+'</div>');
                }
            }
        });
        
        var check_ws_process = checkWS('');
        check_ws_process.success( function(data){

            $('#api_check').find('#check').show();
            $('#api_check').find('#checking').hide();
            $('#api_check').find('i').removeClass('fa fa-spinner fa-spin').addClass('icon-check');

            if (data.success)
            {
                $('#status-message').removeClass('error').addClass('success').show().find('p').html('<i class="fa fa-check-circle"></i>' + data.success);
                $('#form-api-setting').find('.error').remove();
                $('#form-api-setting').find('.form-group').removeClass('has-error');
                $('#api_check').parent().find('.success').closest('.form-group').addClass('has-success');
                $('#api_check').addClass('checkIn');

                if ($('#popup_status').length != 0 && flag == false)
                {
                    if ($('#debug').is(':checked'))
                    {
                        if ($('#mode').val().length != 0)
                        {
                            call_ws_report($('#mode').val());
                        }
                        else
                        {
                            call_ws_report(1);
                        }
                    }
                }
            }

            if (data.error)
            {
                $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());
                $('#form-api-setting').find('.alert-code').hide();
                var alert_message = $('#form-api-setting').find('.alert-message').show().find('.code').html('');
                $('#api_check').closest('.form-group').removeClass('has-success');
                $('#api_check').closest('.form-group').addClass('has-error');

                $.each(data.error, function (i, j) {
                    alert_message.append('<p>' + j + ' ...</p>');
                });
            }
            
            if (data.code)
            {
                $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());

                var alert_message = $('#form-api-setting').find('.alert-code').show();
                $('#api_check').closest('.form-group').removeClass('has-success');
                $('#api_check').closest('.form-group').addClass('has-error');

                alert_message.html(data.code);
            }
        });
        
        check_ws_process.error( function(data){
            $('#api_check').find('#check').show();
            $('#api_check').find('#checking').hide();

            $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());
            $('#form-api-setting').find('.alert-code').hide();
            var alert_message = $('#form-api-setting').find('.alert-message').show().find('.code').html('');
            $('#api_check').closest('.form-group').removeClass('has-success');
            $('#api_check').closest('.form-group').addClass('has-error');

            alert_message.append('<p>' + error + ' ...</p>');
        });
    });

    if ($('#popup_status').length == 0)
    {
        $('#key_pairs').on('change', function () {

            var controls = $(this).parent().parent();

            if ($(this).val() !== "" && $(this).val() !== null)
            {
                $.ajax({
                    type: 'GET',
                    url: base_url + "amazon/get_key_pairs/" + $('#key_pairs').val(),
                    dataType: 'json',
                    success: function (data) {

                        if (data != '')
                        {
                            $.each(data, function (h, j) {
                                $('#merchant_id').val(j.merchant_id);
                                $('#aws_id').val(j.aws_id);
                                $('#secret_key').val(j.secret_key);
                                controls.find('.help-block').hide();
                                controls.parent().find('span.help-inline').remove();
                            });
                        }
                        else
                        {
                            $('#merchant_id').val('');
                            $('#aws_id').val('');
                            $('#secret_key').val('');
                            controls.addClass('error');
                            var help_block = controls.find('.help-block').show();
                            help_block.find('.ext').html($('#key_pairs').val());
                            help_block.find('a').attr('href', base_url + "/amazon/parameters/" + $('#key_pairs').find("option:selected").attr("title"));
                        }

                        console.log(data);
                    },
                    error: function (data, error) {

                        console.log(error);
                    }
                });
            }
            else
            {
                controls.find('.help-block').hide();
                $('#merchant_id').val('');
                $('#aws_id').val('');
                $('#secret_key').val('');
            }
        });
    }
    else
    {
        $('#back').on('click', function () {
            window.location.href = base_url + 'amazon/platform/' + $('#mode').val();
        });
    }

    $('.copy_to_clipboard').each(function(i,j){

        $(this).attr("id", 'copy_to_clipboard_' + i );
        var id=$(this).attr("id");  
        var text_value = $('#'+id).find('input[type="hidden"]').val();
        $('#'+id).zclip({   
            path: base_url + 'assets/js/ZeroClipboard.swf',          
            copy:function(){
                return text_value;
            }, 
            afterCopy:function(){  
                $('#'+id).parent().append(' <span><i class="fa fa-check"></i> ' + $('#l_Copied').val() + '</span> ');
                setTimeout(function(){ 
                    if($('#'+id).parent().find('span').length > 0){
                        $('#'+id).parent().find('span').fadeOut(1000); 
                    }
                }, 1000);                
            }
        });
    });  

    $('#general_help_controller').on('click', function(){
        $('#general_help').toggle();
    });   
    
});

function checkWS(actions){

    return $.ajax({
        type: 'POST',
        url: base_url + "amazon/checkWS/"+actions,
        dataType: 'json',
        data: $('#form-api-setting').serialize(),
        /*success: function (data) {

            $('#api_check').find('#check').show();
            $('#api_check').find('#checking').hide();
            $('#api_check').find('i').removeClass('fa fa-spinner fa-spin').addClass('icon-check');

            if (data.success)
            {
                $('#status-message').removeClass('error').addClass('success').show().find('p').html('<i class="fa fa-check-circle"></i>' + data.success);
                $('#form-api-setting').find('.error').remove();
                $('#form-api-setting').find('.form-group').removeClass('has-error');
                $('#api_check').parent().find('.success').closest('.form-group').addClass('has-success');
                $('#api_check').addClass('checkIn');

                if ($('#popup_status').length != 0 && flag == false)
                {
                    if ($('#debug').is(':checked'))
                    {
                        if ($('#mode').val().length != 0)
                        {
                            call_ws_report($('#mode').val());
                        }
                        else
                        {
                            call_ws_report(1);
                        }
                    }
                }
            }

            if (data.error)
            {
                $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());
                $('#form-api-setting').find('.alert-code').hide();
                var alert_message = $('#form-api-setting').find('.alert-message').show().find('.code').html('');
                $('#api_check').closest('.form-group').removeClass('has-success');
                $('#api_check').closest('.form-group').addClass('has-error');

                $.each(data.error, function (i, j) {
                    alert_message.append('<p>' + j + ' ...</p>');
                });
            }
            
            if (data.code)
            {
                $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());

                var alert_message = $('#form-api-setting').find('.alert-code').show();
                $('#api_check').closest('.form-group').removeClass('has-success');
                $('#api_check').closest('.form-group').addClass('has-error');

                alert_message.html(data.code);
            }
        },
        error: function (error) {

            $('#api_check').find('#check').show();
            $('#api_check').find('#checking').hide();

            $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());
            $('#form-api-setting').find('.alert-code').hide();
            var alert_message = $('#form-api-setting').find('.alert-message').show().find('.code').html('');
            $('#api_check').closest('.form-group').removeClass('has-success');
            $('#api_check').closest('.form-group').addClass('has-error');

            alert_message.append('<p>' + error + ' ...</p>');
        }*/
    });
}

function check_process_running()
{
    var act = (actions === "sync") ? "synchronize" : actions;
    return $.ajax({
        type: "get",
        url: base_url + "tasks/check_running_process/amazon/"+$('#ext').val()+"/"+$('#id_shop').val()+"/" + 'report',
        dataType: 'json',
        timeout: 0
    });
}

function call_ws_report(mode)
{
    $.ajax({
        type: "get",
        url: base_url + "tasks/ajax_run_popup_market/amazon/"+$('#ext').val()+"/"+$('#id_shop').val()+"/report/" + mode,
        dataType: 'json',
    });
    flag = true;
}

function save_parameters(){

    $('.checking-code').show();
   
    var check_ws_process = checkWS('save_parameters');
    check_ws_process.success( function(data){

        $('.checking-code').hide();

        if (data.success)
        {
            $.ajax({
                type: "post",
                url: base_url + "amazon/save_parameters",
                data: $('#form-api-setting').serialize(),
                dataType: 'json',
                success: function(data){   
                     
                    if(data)
                    {
                        if ($('#popup_status').length != 0 && flag == false && data.pass)
                        {
                            var running_process = check_process_running();
                            running_process.success( function(r){

                                $('#page-load').hide();
                                if (!r.status)
                                {
                                    $('.alert-message').show();
                                    $('.alert-message').find('.code').html(r.txt);
                                    if (r.url && r.url !== "")
                                        $('#code').append(' &nbsp; <a href="' + base_url + '/' + r.url + '" target="_blank">' + $('#pay_now').val() + '</a>');
                                }
                                else
                                {
                                    $('#save_continue_data').html('<i class="fa fa-spinner fa-spin"></i> ' + $('#loading').val());
                    

                                    if ($('#debug').is(':checked'))
                                    {                                
                                        
                                        if ($('#mode').val().length != 0)
                                        {
                                            call_ws_report($('#mode').val());
                                        }
                                        else
                                        {
                                            call_ws_report(1);
                                        }
                                    }
                                    setTimeout(function () {
                                        console.log(data);
                                        if(data.step && data.mode) {
                                            console.log(data);
                                            window.location.href = base_url + 'amazon/' + data.next_page + '/' + $('#id_country').val() + '/' + data.step + '/' + data.mode ;
                                        } else if(data.step) {
                                            window.location.href = base_url + 'amazon/' + data.next_page + '/' + $('#id_country').val() + '/' + data.step ;
                                        } else {
                                            window.location.href = base_url + 'amazon/'+data.next_page+'/' + $('#id_country').val();
                                        }

                                    }, 2500);
                                }
                            });  
                            running_process.error( function(data, error) {
                                console.log(error);
                                console.log(data);

                                $('.alert-message').show();
                                $('.alert-message').find('.code').html($('#connect-error').val() + '#21-00002');
                            });                  
                        } else {

                            if(data.step && data.mode) {
                                window.location.href = base_url + 'amazon/' + data.next_page + '/' + $('#id_country').val() + '/' + data.step + '/' + data.mode ;
                            } else if(data.step) {
                                window.location.href = base_url + 'amazon/' + data.next_page + '/' + $('#id_country').val() + '/' + data.step ;
                            } else {
                                window.location.href = base_url + 'amazon/'+data.next_page+'/' + $('#id_country').val();
                            }
                        } 
                    } 

                },
                error: function(data, error, message){
                    console.log(data);  console.log(error); console.log(message);
                    $('.alert-message').show();
                    $('.alert-message').find('.code').html($('#connect-error').val() + '#21-00003');
                }
            });  
        }

        if (data.error)
        {
            var alert_message = $('#form-api-setting').find('.alert-message').show().find('.code').html('');
            $('#api_check').closest('.form-group').removeClass('has-success');
            $('#api_check').closest('.form-group').addClass('has-error');

            $.each(data.error, function (i, j) {
                alert_message.append('<p>' + j + ' ...</p>');
            });
        }
        
        if (data.code)
        {
            $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());

            var alert_message = $('#form-api-setting').find('.alert-code').show();
            $('#api_check').closest('.form-group').removeClass('has-success');
            $('#api_check').closest('.form-group').addClass('has-error');

            alert_message.html(data.code);
        }

    });

    check_ws_process.error( function(data){

        $('.checking-code').hide();

        $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());
        $('#form-api-setting').find('.alert-code').hide();
        var alert_message = $('#form-api-setting').find('.alert-message').show().find('.code').html('');
        $('#api_check').closest('.form-group').removeClass('has-success');
        $('#api_check').closest('.form-group').addClass('has-error');

        alert_message.append('<p>' + error + ' ...</p>');
    });
}