$(document).ready(function () {

    $('.collapsible').click(function () {
        if ($(this).parent().parent().parent().hasClass("collapsed")) {
            $(this).parent().parent().parent().removeClass("collapsed");
            $(this).find('i').removeClass(" fa-chevron-down");
            $(this).find('i').addClass(" fa-chevron-up");
        } else {
            $(this).parent().parent().parent().addClass("collapsed");
            $(this).find('i').removeClass(" fa-chevron-up");
            $(this).find('i').addClass(" fa-chevron-down");
        }
    });

//    $('head').append('<link rel="stylesheet" href="' + base_url + 'assets/css2/admin/module.admin.page.widgets.min.css">');

    $('#back').on('click', function () {
        window.location.replace(base_url + "amazon/carriers/" + $('#id_country').val());
    });

    $('.search-select').on('change', function () {
        var val = $(this).val();
        var obj = $(this);
        var name = obj.attr('name');
        //var mk = $(this).parents('.form-group').parent();

        $('.search-select').each(function () {
            var cpm = $(this).val();
            var cpm_name = $(this).attr('name');
            if (val == cpm && name != cpm_name) {
                $(this).val('');
            }
        }).trigger("chosen:updated");
        
//        $('.search-select', mk).each(function () {
//            var cpm = $(this).val();
//            var cpm_name = $(this).attr('name');
//            if (val == cpm && name != cpm_name) {
//                $(this).val('');
//            }
//        });
    });
});       