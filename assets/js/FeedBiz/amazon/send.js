var actions = document.getElementById('actions').value;
var running = true;
var log_skipped = false;
var rdata = [];
rdata.product = false;
rdata.inventory = false;
rdata.price = false;
rdata.image = false;
rdata.relationship = false;
var batchID = '';

$(document).ready(function () {

    $('#loading-content').show();

    get_report(actions + '_' + $('#country').val());
    
    $('.show-skip-details').click(function () {
        if ($(this).find('i').hasClass('fa-caret-down')) {            
            $(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
            $('#skip-details').show();
            if(log_skipped == false)
            {
                $('#skip-details').html('<i class="fa fa-spinner fa-spin"></i>');
                console.log(batchID);
                get_validate('skip', batchID);
            }
            
            log_skipped = true;
            
        } else {
            $(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
            $('#skip-details').hide();
        }
    });    
});

function get_error(sid, ele)
{
    $.ajax({
        url: base_url + "amazon/get_submission_list/" + $('#id_country').val() + '/' + sid,
        dataType: "json",
        success: function (r) {
            console.log(r);
            var value = '';
            $.each(r, function (i, j) {
                value = value + '<tr><td>' + j.sku + '</td><td>' + j.result_message_code + '</td><td>' + j.result_description + '</td></tr>';
            });

            //ele.find('.error_report .widget').attr('data-toggle',"collapse-widget");
            //ele.find('.error_report .widget-body').addClass('collapse in');
            ele.find('.error_report table tbody').html(value);
        },
        error: function (e) {

            console.log(e);
        }
    });
}

function get_validate(act, batchID)
{    
    var url = base_url + "amazon/logs/" + $('#id_country').val() + '/5/' + actions + '/error/' + batchID;
    
    //console.log(url);
    if(act == 'skip'){
        url = base_url + "amazon/logs/" + $('#id_country').val() + '/5/' + actions + '/warning/' + batchID;
    }
    
    $.ajax({
        url: url,
        dataType: "json",
        success: function (r) {
            console.log(r);
            if (r)
            {
                var value = '';
                $.each(r, function (i, j) {
                    console.log(i,j);
                    value = value + '<li class="m-b5"><p class="poor-grey"><i class="fa fa-remove"></i> ' + j.messages + '</p></li>';
                });

                if(act == 'skip') {
                    $('#skip-details').html('<ul class="m-l25">' + value + '</ul>');
                } else if(act && act.length > 0) {
                    $('#msg_' + act).find('.msg_result').addClass('red').html('<ul class="list-unstyled text-warning">' + value + '</ul>');  
                } else {
                    $('#loading-content').hide();
                    $('#amazon-content').append('<ul class="list-unstyled">' + value + '</ul>');
                }
            }
        },
        error: function (e) {
            console.log(e);
        }
    });
}

function renderFeedSubmissionResult(index, value)
{
    //nice_Scroll();
    console.log(value);
    //$('#msg_' + index ).addClass('alert-success');
    $('#msg_' + index).find('.msg_status img').hide();

    var cloned = $('#main_result').clone().fadeIn();
    
    cloned.find('#error_report_block').click(function () {
        if ($(this).closest('.error_report').find('.error_logs').hasClass("showSelectBlock")) {
            $(this).closest('.error_report').find('.error_logs').removeClass("showSelectBlock");
            $(this).removeClass("fa-caret-down").addClass("fa-caret-up");
        } else {
            $(this).closest('.error_report').find('.error_logs').addClass("showSelectBlock");
            $(this).removeClass("fa-caret-up").addClass("fa-caret-down");
        }
    });
    
    cloned.removeAttr('id');

    $('#msg_' + index).find('.msg_result').html(cloned);

    if (value.FeedSubmissionResult.FeedSunmissionId)
        cloned.find('.header span').html(value.FeedSubmissionResult.FeedSunmissionId);

    if (value.FeedSubmissionResult.MessagesProcessed)
        cloned.find('.processed span').html(value.FeedSubmissionResult.MessagesProcessed);

    if (value.FeedSubmissionResult.MessagesSuccessful)
        cloned.find('.success span').html(value.FeedSubmissionResult.MessagesSuccessful);

    if (value.FeedSubmissionResult.MessagesWithWarning)
        cloned.find('.warning span').html(value.FeedSubmissionResult.MessagesWithWarning);

    if (value.FeedSubmissionResult.MessagesWithError)
    {
        cloned.find('.error span').html(value.FeedSubmissionResult.MessagesWithError);

        if (parseInt(value.FeedSubmissionResult.MessagesWithError) > 0)
        {
            cloned.find('.error_report').removeClass('showSelectBlock');
            get_error(value.FeedSubmissionResult.FeedSunmissionId, cloned);
        }
    }
}

function get_report(act)
{
    $.ajax({
        url: base_url + "users/ajax_get_sig_process/" + act,
        //url: base_url + "amazon/debugs/10/send/validation",
        dataType: "json",
        beforeSend: function (jqXHR) {
            jqXHR.fail(function () {
                jqXHR.abort();
                setTimeout(function () {
                    get_report(act);
                }, 5000);
            });
        },
        success: function (r) {

            var timeout = r.timeout;
            var delay = r.delay;

            if (r.data) {

                $('#page-load').hide();
                $('#loading').show();                
                
                var status = r.data.status;
                var msg = r.data.msg;

                if (status == 1)
                {
                    if (msg.title){
                        $('#msg_title').find('h4').html(msg.title);
                    }
                    
                    if(msg.batchID){
                        batchID = msg.batchID
                    }
                    
                    $.each(msg, function (index, value) {
                                                
                        if($('#msg_' + index).length > 0){
                            $('#msg_' + index).fadeIn(1000);
                        }
                        
                        if (index == "error" && msg.error != "")
                        {
                            $('#amazon-content').addClass('alert alert-danger').fadeIn().html('<h5>' + value + '</h5>');
                            get_validate(null, batchID);
                        } else if (index == "no_product") {
                            $("#loading-content").fadeOut(1000, function () {
                                $("#amazon-content").fadeIn(100).addClass('center').html('<h2 class="alert-danger m-t5 p-10">' + value + '</h2>');
                            });
                                   
                            if(msg.result.no_send)
                                $('#skip-content').show().find('#no_send').html(msg.result.no_send);
                            if(msg.result.no_skipped)
                                $('#skip-content').show().find('#no_skipped').show().find('span').html(msg.result.no_skipped);
                           
                        }
                        else
                        {
                            if(msg.result)
                            {
                                if(msg.result.no_send)
                                    $('#skip-content').fadeIn(1000).find('#no_send').html(msg.result.no_send);
                                if(msg.result.no_skipped)
                                    $('#skip-content').fadeIn(1000).find('#no_skipped').show().find('span').html(msg.result.no_skipped);
                            }
                            
                            if (index != "title" && index != "type")
                            {
                                if (index == "product")
                                {
                                    $("#loading-content").fadeOut(1000, function () {
                                        $("#amazon-content").fadeIn(100);
                                    });
                                }
                                if (value.no_product)
                                {
                                    $('#msg_' + index).find('.msg_status img').hide();
                                    $('#msg_' + index).find('.msg_status p').html($('#NothingToSend').val());
                                    $('#msg_' + index).addClass('disabled');
                                }
                                else
                                {
                                    if (value.message)
                                    {
                                        $('#msg_' + index).removeClass('disabled');

                                        //console.log(value.message);
                                        if (value.FeedSubmissionResult)
                                        {
                                            $('#msg_' + index).find('.msg_status p').html(' <i class="fa fa-icon fa-check"></i> ' + value.message);
                                        }
                                        else
                                        {
                                            $('#msg_' + index).find('.msg_status p').html(value.message);
                                        }
                                    }
                                    if (value.error)
                                    {
                                        $('#msg_' + index).removeClass('disabled');
                                        $('#msg_' + index).find('.msg_status p').addClass('red').html(' <i class="fa fa-warning-sign"></i> ' + value.error);
                                        $('#msg_' + index).find('img').hide();                                        
                                    }
                                    if (value.FeedSubmissionResult && rdata[index] == false)
                                    {
                                        $('#msg_' + index).removeClass('disabled');

                                        //console.log(index);
                                        //console.log('Processed : ' + value.FeedSubmissionResult.MessagesProcessed);
                                        if (value.FeedSubmissionResult.MessagesProcessed && value.FeedSubmissionResult.MessagesProcessed == 0)
                                        {
                                            if (index == "product")
                                                running = false;

                                            $('#msg_' + index).find('.msg_status img').hide();
                                            $('#msg_' + index).find('.msg_status p').html($('#NothingToSend').val());
                                        }
                                        else
                                        {
                                            rdata[index] = true;
                                            renderFeedSubmissionResult(index, value);
                                        }
                                    }
                                }                                
                            }
                        }
                    });
                }
                else if (status == 4)
                { /*error*/
                    running = false;

                    if (r.data.err_msg.title)
                        $('#msg_title').find('h4').html(r.data.err_msg.title);

                    $('#amazon-content').addClass('alert alert-danger').fadeIn().html('<h5>' + r.data.err_msg.error + '</h5>');

                    get_validate(null, batchID);
                }
                else if (status == 9)
                { /*finished true*/
                    running = false; // no more loop 
                    console.log(msg);
                    $.each(msg, function (index, value) {
                        
                        if($('#msg_' + index).length > 0){
                            $('#msg_' + index).fadeIn(1000);
                        }
                        
                        if (index == "error" && msg.error != "")
                        {
                            $('#amazon-content').addClass('alert alert-danger').fadeIn().html('<h5>' + value + '</h5>');                            
                            get_validate(null, batchID);                           
                        }
                        else if (index == "no_product")
                        {
                            $("#loading-content").fadeOut(1000, function () {
                                $("#amazon-content").fadeIn(100).addClass('center').html('<h2 class="alert-danger m-t5 p-10">' + value + '</h2>');
                            });
                            
                            if(msg.result.no_skipped > 0) {
                                get_validate('skip', batchID);
                            }   
                            
                            if(msg.result.no_send)
                                $('#skip-content').show().find('#no_send').html(msg.result.no_send);
                            if(msg.result.no_skipped)
                                $('#skip-content').show().find('#no_skipped').show().find('span').html(msg.result.no_skipped);
                        }
                        else
                        {
                            if (index != "title")
                            {
                                if (index == "product")
                                {
                                    $("#loading-content").fadeOut(1000, function () {
                                        $("#amazon-content").fadeIn(100);
                                    });
                                }
                                if (value.no_product)
                                {
                                     $('#msg_' + index).find('.msg_status img').hide();
                                    $('#msg_' + index).find('.msg_status p').html($('#NothingToSend').val());
                                    $('#msg_' + index).addClass('disabled');
                                }
                                else
                                {
                                    if (value.message)
                                    {
                                        $('#msg_' + index).removeClass('disabled');

                                        if (value.FeedSubmissionResult)
                                        {
                                            $('#msg_' + index).find('.msg_status p').html(' <i class="fa fa-icon fa-check"></i> ' + value.message);
                                        }
                                        else
                                        {
                                            $('#msg_' + index).find('.msg_status p').html(value.message);
                                        }
                                    }
                                    if (value.error)
                                    {
                                        //$('#msg_' + index).find('.msg_status p').addClass('red').html(' <i class="icon-warning-sign"></i> ' + value.error);
                                        $('#msg_' + index).removeClass('disabled');
                                        $('#msg_' + index).find('.msg_status p').addClass('red').html(' <i class="fa fa-warning-sign"></i> ' + value.error);
                                        $('#msg_' + index).find('img').hide();
                                        get_validate(index, batchID);                                                                              
                                    }
                                    if (value.FeedSubmissionResult)
                                    {
                                        if (value.FeedSubmissionResult.MessagesProcessed && value.FeedSubmissionResult.MessagesProcessed == 0)
                                        {
                                            if (index == "product")
                                                running = false;

                                            //$('#msg_' + index ).addClass('alert-success');
                                            $('#msg_' + index).find('.msg_status img').hide();
                                            $('#msg_' + index).find('.msg_status  p').html($('#NothingToSend').val());

                                        }
                                        else
                                        {
                                            renderFeedSubmissionResult(index, value);

                                            if ($('#msg_' + index).next().length > 0)
                                                $('#msg_' + index).next().addClass('disabled').find('.msg_status p').html($('#NothingToSend').val());

                                            if ($('#msg_' + index).next().next().length > 0)
                                                $('#msg_' + index).next().next().addClass('disabled').find('.msg_status p').html($('#NothingToSend').val());

                                            if ($('#msg_' + index).next().next().next().length > 0)
                                                $('#msg_' + index).next().next().next().addClass('disabled').find('.msg_status p').html($('#NothingToSend').val());

                                            if ($('#msg_' + index).next().next().next().next().length > 0)
                                                $('#msg_' + index).next().next().next().next().addClass('disabled').find('.msg_status p').html($('#NothingToSend').val());
                                        }
                                    }                                    
                                }
                            }
                        }
                    });

                }

            } else {

                $('#page-load').show().find('h4').html($('#Connecting-to-Amazon').val());
                $('#loading').hide();
                console.log(r.error_msg);
                delay = timeout;
            }

            if (running) {
                setTimeout(function () {
                    get_report(act);
                }, delay);
            }
        }, error: function (xhr, status, error) {

            if (xhr.responseText) {
                console.log('Progress status error :', xhr.responseText);
                if (xhr.responseText === 'terminate') {
                    return;
                }
            }
        }
    });
}