$(document).ready(function ()
{
    
    $('#send-products').on('click', function ()
    {
        var send_offers = $(this);
        bootbox.confirm($('#send-products-confirm').val(), function (result)
        {
            if (result)
            {
                //addStatus(send_offers);
                synchronize(send_offers, $('#sync').val());
            }
        });
    });
    
    $('#send-offers').on('click', function ()
    {
        var send_offers = $(this);
        bootbox.confirm($('#send-offers-confirm').val(), function (result)
        {
            if (result)
            {
                //addStatus(send_offers);
                synchronize(send_offers, $('#sync').val());
            }
        });
    });
    
    $('#import-orders').on('click', function ()
    {
        var get_order = $(this);
        bootbox.confirm($('#get-orders-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(get_order);
                get_amazon_orders(get_order);
            }
        });

    });
    
    $('#update-orders').on('click', function ()
    {
        var update_order = $(this);
        bootbox.confirm($('#update-orders-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(update_order);
                update_amazon_orders(update_order);
            }
        });

    });
    
    $('.order-date').datepicker({
        dateFormat: 'dd/mm/yy'
    });
    
    $('#delete-products').on('click', function ()
    {
        var delete_products = $(this);
        bootbox.confirm($('#delete-products-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(delete_products);
                delete_amazon_product(delete_products);
            }
        });

    });

    $('#send_images_only').on('click', function ()
    {
        var send_images = $(this);
        bootbox.confirm($('#send_images_only-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(send_images);
                send_images_only(send_images);
            }
        });

    });

    $('#send_relations_only').on('click', function ()
    {
        var send_relations = $(this);
        bootbox.confirm($('#send_relations_only-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(send_relations);
                send_relations_only(send_relations);
            }
        });

    });
    
    $('#repricing-analysis').on('click', function ()
    {
        var repricing_analysis = $(this);
        bootbox.confirm($('#repricing-analysis-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(repricing_analysis);
                repricing(repricing_analysis, repricing_analysis.val());
            }
        });

    });

    $('#repricing-export').on('click', function ()
    {
        var repricing_export = $(this);
        bootbox.confirm($('#repricing-export-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(repricing_export);
                repricing(repricing_export, repricing_export.val());
            }
        });

    });

    $('#repricing-trigger').on('click', function ()
    {
        var repricing_trigger = $(this);
        bootbox.confirm($('#repricing-trigger-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(repricing_trigger);
                trigger_reprice(repricing_trigger, repricing_trigger.val());
            }
        });

    });

    $('#fba_estimated').on('click', function ()
    {
        var fba_estimated = $(this);
        bootbox.confirm($('#fba-estimated-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(fba_estimated);
                fba('estimated');
            }
        });

    });
    $('#fba_manager').on('click', function ()
    {
        var fba_manager = $(this);
        bootbox.confirm($('#fba-manager-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(fba_manager);
                fba('manager');
            }
        });

    });

    $('#shipping_groupname').on('click', function ()
    {
        var shipping = $(this);
        console.log(shipping);
        addStatus(shipping, false);
        $('.sum_item').html('<p class="text-center"><i class="fa fa-spin fa-spinner fa-2x"></i></p>');
        shipping_group_name(shipping, shipping.val());    

    });

    $('#fba_orders_status').on('click', function ()
    {
        var fba_orders_status = $(this);
        bootbox.confirm($('#fba-orders-status-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(fba_orders_status);
                fba('orders_status');
            }
        });

    });

    $('#send_shipping_override_only').on('click', function ()
    {
        var send_shipping_override = $(this);
        bootbox.confirm($('#send_shipping_override_only-confirm').val(), function (result)
        {
            if (result)
            {
                addStatus(send_shipping_override);
                send_shipping_override_only(send_shipping_override);
            }
        });

    });

    $('#all_sent').parent().on('click', function ()
    {
        var delete_products = $(this);
        bootbox.confirm('<div class="text-warning error"><b>'+$('#l_warning').val()+'</b><br/><br/>'+$('#delete-all-products-confirm').val()+'</div>', function (result)
        {
            if (!result)
            {
               delete_products.removeClass("checked");
               $('#all_sent').removeAttr('checked');
               console.log(delete_products);
            }
        });

    });

});

function addStatus(e){

    e.attr('disabled', true);
    e.addClass('update');
    e.parent().addClass('form-group has-update');
    e.parent().find('.status').addClass('update').show();
    e.parent().find('.error').hide();
}

function addError(e, r){    

    var rdisplay = $('#' + $(e).val()).find('.error').show();
    rdisplay.parent().addClass('form-group has-error');
    $('#' + $(e).val()).find('.status').hide();
    console.log(r);
    rdisplay.find('span').html(r.txt);
    if (r.url && r.url !== "")
        rdisplay.append(' &nbsp; <a href="' + base_url + r.url + '">' + $('#pay_now').val() + '</a>');
}

function removeStatus(e){
    e.attr('disabled', true);
    e.addClass('update');
    e.parent().addClass('form-group has-update');
    e.parent().find('.status').addClass('update').show();
}

function synchronize(ele, mode)
{
    if ($('#debug').length > 0)
    {
        notification_alert($('#error-inactive-message').val(), 'bad'); 
        //window.location.href = base_url + 'amazon/debugs/' + $('#id_country').val() + '/offers/' + mode;
    }
    else
    {
        if(mode == "update_offer")
        {
            var country = '';
            if($('#country').length > 0) {
                country = $('#country').val();
            }
            var checkproces = check_process('sync_'+country);
            checkproces.success(function(r){ 

                if (r.data) {
                    var status = r.data.status;
                    if (status == 1) {
                        console.log(r);
                        r.txt = $('#offer_running').val();
                        console.log(r);
                        addError(ele, r);
                    } else {
                       run_synchronize(ele, mode);
                    }
                } else {
                    run_synchronize(ele, mode);
                }
            });

            checkproces.error(function(error){
                console.log(error);
            });

        } else {
           run_synchronize(ele, mode);
        }
    }
}

function run_synchronize(ele, mode){
     $.ajax({
        type: "get",
        url: base_url + "tasks/ajax_run_export/amazon/" + $('#ext').val() + "/" + $('#id_shop').val() + "/offers/" + $('#id_user').val() + "/" + mode,
        dataType: 'json',
        timeout: 0,
        success: function (r) {
            //console.log(r);
            if (!r.status)
            {
                addError(ele, r);
            }
        }
    });
}

function get_amazon_orders(e)
{
    var dateFrom = $('#order-date-from').val();
    var dateTo = $('#order-date-to').val();

    var option_data = {};
    option_data.datefrom = dateFrom.replace(/\//g, "-");
    option_data.dateto = dateTo.replace(/\//g, "-");
    option_data.status = $('#order-status').val();

    $.ajax({
        type: "get",
        url: base_url + "tasks/ajax_run_export/amazon/" + $('#ext').val() + "/" + $('#id_shop').val() + "/orders/" + $('#id_user').val() + "/" + encodeURIComponent(JSON.stringify(option_data)),
        dataType: 'json',
        timeout: 0,
        success: function (r) {
            console.log(r);
            if (r.txt)
            {
                addError(e, r);
            }
        }
    });
}

function update_amazon_orders(e)
{
    $.ajax({
        type: "get",
        url: base_url + "tasks/ajax_run_export/amazon/" + $('#ext').val() + "/" + $('#id_shop').val() + "/update/" + $('#id_user').val(),
        dataType: 'json',
        timeout: 0,
        success: function (r) {
            console.log(r);
            if (r.txt)
            {
                addError(e, r);
            }
        }
    });
}

function delete_amazon_product(ele)
{  
    var option_data = {};
    option_data.delete = 
    $('#out_of_stock').is(':checked') ? $('#out_of_stock').val() : 
    ($('#all_sent').is(':checked') ? $('#all_sent').val() : 
    ($('#disabled').is(':checked') ? $('#disabled').val() : null));
    
    if ($('#debug').length > 0){
        notification_alert($('#error-inactive-message').val(), 'bad'); 
        //window.location.href = base_url + 'amazon/debugs/' + $('#id_country').val() + '/delete';
    }
    else
    {
        $.ajax({
            type: "get",
            url: base_url + "tasks/ajax_run_export/amazon/" + $('#ext').val() + "/" + $('#id_shop').val() + "/delete/" + $('#id_user').val() + "/" + encodeURIComponent(JSON.stringify(option_data)),
            dataType: 'json',
            timeout: 0,
            success: function (r) {
                console.log(r);
                if (!r.status)
                {
                    addError(ele, r);
                }
            }
        });
    }
}

function send_images_only(e){

    var option_data = { };        
    option_data.exclude_manufacturer = true;        
    option_data.exclude_supplier = true;     
    option_data.only_selected_carrier = true;     
    option_data.send_image = true ;     
    option_data.send_relation_ship = false;     
    option_data.send_only_image = true;     
    option_data.send_shipping_override = false;

    if($('#debug').length > 0)
    {
        notification_alert($('#error-inactive-message').val(), 'bad'); 
        //option_data.limiter = 10;
        //window.location.href = base_url + 'amazon/debugs/' + $('#id_country').val() + '/create/' + encodeURIComponent(JSON.stringify(option_data)) ;
    }
    else
    {
        var running_process = check_process_running('images');  
        running_process.success(function(r){ 
            if(!r.status)
            {
                addError(e, r);
            }
            else
            {
                create(option_data);  
            }
        });
        running_process.error(function(error){
            console.log(error);
        });  
    }
}

function send_relations_only(e){

    var option_data = { };        
    option_data.exclude_manufacturer = true;        
    option_data.exclude_supplier = true;     
    option_data.only_selected_carrier = true;     
    option_data.send_image = true ;     
    option_data.send_relation_ship = true;     
    option_data.send_only_image = false;     
    option_data.send_shipping_override = false;  

    if($('#debug').length > 0)
    {
        notification_alert($('#error-inactive-message').val(), 'bad'); 
        //option_data.limiter = 10;
        //window.location.href = base_url + 'amazon/debugs/' + $('#id_country').val() + '/create/' + encodeURIComponent(JSON.stringify(option_data)) ;
    }
    else
    {
        var running_process = check_process_running('relations');  
        running_process.success(function(r){ 
            if(!r.status)
            {
                addError(e, r);
            }
            else
            {
                create(option_data);  
            }
        });
        running_process.error(function(error){
            console.log(error);
        });  
    }
}

function send_shipping_override_only(e){

    var option_data = { };        
    option_data.exclude_manufacturer = true;        
    option_data.exclude_supplier = true;     
    option_data.only_selected_carrier = true;     
    option_data.send_image = false ;     
    option_data.send_relation_ship = false;     
    option_data.send_only_image = false;     
    option_data.send_shipping_override = true;     

    if($('#debug').length > 0)
    {
        notification_alert($('#error-inactive-message').val(), 'bad'); 
    }
    else
    {
        var running_process = check_process_running('shipping_override');  
        running_process.success(function(r){ 
            if(!r.status)
            {
                addError(e, r);
            }
            else
            {
                create(option_data);  
            }
        });
        running_process.error(function(error){
            console.log(error);
        });  
    }
}

function check_process_running(actions)
{
    if(actions.length <= 0){
        actions = 'create'
    }

    return $.ajax({
        type: "get",
        url: base_url + "tasks/check_running_process/amazon/"+$('#ext').val()+"/"+$('#id_shop').val()+"/" + actions,
        dataType: 'json',
        timeout:0 
    });  
}

function create(option_data)
{
    $.ajax({
        type: "get",
        url: base_url + "tasks/ajax_run_popup_market/amazon/"+$('#ext').val()+"/"+$('#id_shop').val()+"/create/" + encodeURIComponent(JSON.stringify(option_data)),
        dataType: 'json',
        timeout:0           
    });  
}

function repricing(e, actions)
{   
    var running_process = check_process_running('repricing_' + actions);  
    running_process.success(function(r){ 

        if(!r.status) {
            addError(e, r);
        } else {
            $.ajax({
                type: "get",
                url: base_url + "tasks/ajax_run_popup_market/amazon/"+$('#ext').val()+"/"+$('#id_shop').val() + "/repricing/" + actions,
                dataType: 'json',
                timeout:0           
            });   
        }
    });

    running_process.error(function(error){
        console.log(error);
    });
    
}

function trigger_reprice(e, actions)
{       
    $.ajax({
        type: "get",
        url: base_url + "amazon/" + "repricing/" + actions + "/" + $('#id_country').val(),
        dataType: 'json',
        //timeout:0 ,
        success: function(r){
            if(r.pass){
                notification_alert($('#success-trigger-reprice').val(), 'good'); 

                e.attr('disabled', false);
                e.parent().find('.status').hide();
                e.parent().find('.error').hide();
            }
            console.log(r);
        }        ,
        error: function(error, message){
            notification_alert($('#error-trigger-reprice').val(), 'bad'); 
            console.log(error, message);
        }        
    });       
}

function fba(actions, mode)
{     
    var modes = "";

    if(mode)
        modes = "/" + mode ;

    $.ajax({
        type: "get",
        url: base_url + "tasks/ajax_run_popup_market/amazon/"+$('#ext').val()+"/"+$('#id_shop').val() + "/fba_" + actions + modes,
        dataType: 'json',
        timeout:0           
    });   

}

function shipping_group_name(e, mode)
{     
    $.ajax({
        type: "get",
        url: base_url + "tasks/ajax_run_popup_market/amazon/"+$('#ext').val()+"/"+$('#id_shop').val()+"/shipping_group",
        dataType: 'json',
        success: function(){
            setTimeout(function (){ get_report(e,'shipping_group_'+$('#country').val()); }, 5000);
        }
    });
}

function get_report(e, act)
{
    var running = true;

    $.ajax({ 
        url: base_url+"/users/ajax_get_sig_process/" + act, 
        dataType: "json",
        beforeSend:function(jqXHR){
            jqXHR.fail(function(){ 
                jqXHR.abort();
                setTimeout(function (){ get_report(act); }, 5000);
            });
        },
        success:function(r){ 
            console.log(r);

            $('#page-load').hide();

            var timeout = r.timeout;
            var delay = r.delay; 

            if(r.data){ 
                var status = r.data.status;
                var err_msg = r.data.err_msg;
                var msg = r.data.msg;

                if(status == 1)
                { 
                    running = true;
                } 
                else if(status == 4)
                { /*error*/
                   

                    if(err_msg.error) {
                       $('.sum_item').html('<p class="p-t10 p-l5" style="color: #f74f63;">' + err_msg.error + '</p>');
                    }

                    running = false;
                }
                else if(status == 8 || status == 9 )
                { /*finished true status 8-9 */    
                    if(act == 'get_report_'+$('#country').val()) {
                        get_shipping_group_name(e);
                    }
                    running = false;
                } 

            }

            if(running){
                setTimeout(function (){ get_report(act); }, delay);
            }

        },error: function(xhr, status, error) { 

            $('#load').remove();
            if(xhr.responseText){
                console.log('Progress status error :',xhr.responseText);
                if(xhr.responseText === 'terminate'){
                    return;
                }
            }
        }
    });
}

function check_process(act)
{
    return $.ajax({ 
        url: base_url+"/users/ajax_get_sig_process/" + act, 
        dataType: "json",    
        timeout:0    
    });
}

function get_shipping_group_name(e)
{ 
    removeStatus(e) ;

    return $.ajax({
        type: "get",
        url: base_url + "/amazon/get_shipping_group_name/" + $('#id_country').val() + '/1',
        dataType: 'json',   
        async: false,
        success: function(data) 
        { 
            var result = '';
            var template = $('#main-template').clone();
            template.removeAttr('id');

           $.each(data, function (i, j) {

                template.find('.summaryBlock').show();
                template.find('.poor-gray').html(j);

                result = result + template.html();

            });
            
            $('.sum_item').html(result);
        },

        error: function(error){
            console.log(error);
        }
    });  
}