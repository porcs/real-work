 var table ;
$(document).ready(function(){
    var filter_ignore_inactive = true; 
    
    $('head').append('<link rel="stylesheet" href="'+base_url+'assets/css/feedbiz/amazon/logs.css" />');
    tb_log = $('#product_logs').dataTable( {
         "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         "",
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "processing": true,
        "serverSide": true,
        "order": [[4, 'desc']],
        "ajax": {
            "url": base_url + "amazon/validation_log/" + $('#id_country').val(),
            "type": "POST",
            "data": {ignore_case:function(){
                    
                    return filter_ignore_inactive;
                }
            }
        },
        "columns": [           
            { "data": "batch_id", "bSortable": true, "width": "10%" },
            { "data": "action_process", "bSortable": true, "width": "10%" },
            { "data": "action_type", "bSortable": true, "width": "10%" },
            { "data": "message", "bSortable": false, "width": "50%" },
            { "data": "datetime", "bSortable": true, "width": "20%"  }
        ],       
        "pagingType": "full_numbers"
        ,"fnDrawCallback": function(settings, data){
        	  $('#product_logs_filter').show();
                  $('#product_logs_info').show();
                  $('#product_logs_paginate').show();
          }
    });
    
    table = $('#product_logs').DataTable();
    table.search($('#batch_id').val()).draw();
    $('.batch_id').click( function () {
        table.search( this.value ).draw();
    } );
     
    $('#chk_ignore_inactive').change(function(){
       filter_ignore_inactive = $(this).is(':checked');  
       table.draw(); 
       
    });

    $('#history_log').niceScroll({cursoropacitymax:0.7,horizrailenabled:false,cursorborder:"",cursorcolor:"#919191",cursorwidth:"3px", autohidemode:true});

    scoll_page_down(1);

    $('#logs-filter').on('change', function(){
        if($('#date_from').val() == "" && $('#date_to').val() == "" && $('#action_type').val() == ""){
            if(clean_tbody()){
                get_logs();
            }            
        } else {
            get_query_logs();
        }
    });

});

var loading_flag = false;

function scoll_page_down(page)
{
    var cal = ($('#history_log').height() + 22 * page);
    $('#history_log').scroll(function () { 
        
        if ($('#history_log').scrollTop() + ( cal ) > $('#history_log_inner').height()) {
            if (loading_flag == false) {
                loading_flag = true;                   
                $('#history_log').unbind('scroll');
                get_logs(page);
            }
        }
    });
}

function get_query_logs()
{      
    $.ajax({
        type: "POST",
        url: base_url + "amazon/get_log/" + $('#id_country').val(),
        data: $('#logs-filter').serialize(),
        dataType: 'json',
        success: function (data) {
           
            if(data == "") {

                $('#history_log_inner tbody').html('<tr><td colspan="4">'+$('#zeroRecords').val()+'</td></tr>');

            } else {
               
                if(clean_tbody()){
                    reder_log_data(data);
                }
              
                $("#history_log").niceScroll({cursoropacitymax:0.7,horizrailenabled:false,cursorborder:"",cursorcolor:"#919191",cursorwidth:"3px", autohidemode:true}); 
                $('#history_log').getNiceScroll().resize();                
                loading_flag = true;

            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function get_logs(page)
{   
    var pages = '';
    if(page) {
        pages = '/' + page;
    }

    $.ajax({
        type: "POST",
        url: base_url + "amazon/get_log/" + $('#id_country').val() + pages,
        dataType: 'json',
        success: function (data) {
            
            if(data) {

                reder_log_data(data);

                $("#history_log").niceScroll({cursoropacitymax:0.7,horizrailenabled:false,cursorborder:"",cursorcolor:"#919191",cursorwidth:"3px", autohidemode:true}); 
                $('#history_log').getNiceScroll().resize();
                loading_flag = false;
                scoll_page_down(page+1);

            } 

        },
        error: function (error) {
            console.log(error);
        }
    });
}

function clean_tbody(){
    $('#history_log_inner tbody').html('');
    return true;
}

function reder_log_data(data){

    $.each(data, function(key, value){
        var cloned = $('table#main-logs tr').clone().appendTo($('#history_log_inner'));
        var cron = '';
        var action_type = '';
        
        // time    
        cloned.find('.date').html(value.date_upd_date);
        cloned.find('.time').html(value.date_upd_time);
        
        // action type
        if(value.is_cron == "1")
            cron = ' (cron)';
        action_type = value.action_type.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        cloned.find('.action_type span').html(action_type + cron);
        
        // batch id
        cloned.find('.batch_id_log').html('<button type="button" value="'+value.batch_id+'" class="link batch_id">'+value.batch_id+'</button>');
        cloned.find('.batch_id').click( function () {
            table.search( this.value ).draw();
        } );  

        if(value.action_type == "FBA Order") {
            
            var details = value.detail;            

            if(typeof value.detail !== "undefined" && typeof details.error !== "undefined") {

                cloned.find('.status_log').html('<p class="logRow logError"><i class="fa fa-warning red"></i> '+$('#l_Error').val()+'</p>');
                result = details.error ;

            } else {
                cloned.find('.status_log').html('<p class="logRow logSuccess"><i class="fa fa-check green"></i> '+$('#success_log').val()+'</p>');

                var fba_type = '';
                if(value.detail.type)
                    fba_type = $('#FBA_Order').val() + ' ' + value.detail.type + ' <br/> ';

                 var order_result = $('#result_log').val() + ' ' + (value.no_process - value.no_skipped) + ' ' + $('#success_order').val() + ', ' + value.no_skipped + ' ' + $('#skipped_order').val();

                // Result
                result = fba_type + order_result ;
            }

            
            cloned.find('.result_log').html('<p class="logRow">' + result + '</p>');

        } else if(value.action_type == "FBA Manager") {
            
            var details = value.detail;            

            if(typeof value.detail !== "undefined" && typeof details.error !== "undefined") {

                cloned.find('.status_log').html('<p class="logRow logError"><i class="fa fa-warning red"></i> '+$('#l_Error').val()+'</p>');
                result = details.error ;

            } else {
                cloned.find('.status_log').html('<p class="logRow logSuccess"><i class="fa fa-check green"></i> '+$('#success_log').val()+'</p>');

                var fba_type = '';
                if(value.detail.type)
                    fba_type = $('#behaviour').val() + ' : ' + value.detail.type + ' <br/> ';

                var events = $('#fba_process').val() + ' ' + (value.no_process)  + $('#events').val() ;
                var switching_AFN = (value.no_success)+ ' ' + $('#items_switching_to_AFN').val();
                var switching_MFN = (value.no_error)+ ' ' + $('#items_switching_to_MFN').val();
                var fba_skipped = (value.no_skipped)+ ' ' + $('#skipped_items').val() ;

                // Result
                result = fba_type + events + ', ' /*+ switching_AFN + ', ' + switching_MFN + ', '*/ + fba_skipped;
            }

            
            cloned.find('.result_log').html('<p class="logRow">' + result + '</p>');

        } else if(value.action_type == "Import Orders" && value.detail.order_status) {
            
            cloned.find('.status_log').html('<p class="logRow logSuccess"><i class="fa fa-check green"></i> '+$('#success_log').val()+'</p>');

            // Status
            var order_range = '';
            var order_status = $('#order_status_'+value.detail.order_status).val();
            
            if(value.detail.order_range)
                order_range = $('#form_log').val() + value.detail.order_range;
            
            // Result
            var no_process = (value.no_process - value.no_success) ;
            var order_result = $('#result_log').val() + ' ' + value.no_success + ' ' + $('#success_order').val() + ', ' + no_process + ' ' + $('#skipped_order').val();
            cloned.find('.result_log').html(order_result);
            
            cloned.find('.result_log').html('<p class="logRow">' + order_status + ' ' + order_range + '<br/>' + order_result + '</p>');

        } else {
            
            var result = '';

            if(typeof value.detail !== "undefined" && typeof value.detail.error !== "undefined") {
                //cloned.find('.status_log').html('<p class="logRow logError"><i class="fa fa-warning red"></i> '+value.detail.error+'</p>');
                cloned.find('.status_log').html('<p class="logRow logError"><i class="fa fa-warning red"></i> '+$('#l_Error').val()+'</p>');
                result = value.detail.error;
            } else {

                cloned.find('.status_log').html('<p class="logRow logSuccess"><i class="fa fa-check green"></i> '+$('#success_log').val()+'</p>');

                if(value.action_type == "Export Orders"){       
                    result = value.no_process + ' ' + $('#orders_to_send').val() + ', ' + value.no_success + ' ' + $('#success').val()+ ', ' + value.no_error + ' ' + $('#skipped').val();
                    
                } else {
                    result = value.no_send + ' ' + $('#items_to_send').val() + ', ' + value.no_skipped + ' ' + $('#skipped_items').val();
                }
            }           
            
            // Result
            cloned.find('.result_log').html('<p class="logRow">' + result + '</p>');
        }
        
    });
}