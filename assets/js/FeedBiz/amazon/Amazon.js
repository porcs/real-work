var validations_ok = true;
var ext = $('#ext').val();
ext = ext.replace(".", "");

function showLoading(ele){

    var element = ele.parent().nextAll();
    element.hide();
    element.parent().append('<div class="form-group"><i class="fa fa-spinner fa-spin spin-loading"></i></div>');
}

function hideLoading(ele){
    ele.fadeIn(2000);
    ele.parent().find('i.spin-loading').parent().remove();
    ele.parent().find('p, .form-group').show();
}

function applyRules(data, name){
    
    initSpecificFieldsOccurs();

    remove_error_label();

    changeName();

    setValues(data, name);

    $(".addSpecificField").unbind('click').on("click",function(e){        
        e.preventDefault();
        addSpecificField($(this).parent().find('select[rel="specific_options"]'));
    });
    
    $('.removeSpecificField').unbind("click").on("click",function(e){
        e.preventDefault();
        
        var content = $(this).closest('div.amazon-model').find('input[rel="name"]').attr('data-content');
        var selected = $(this).parent().find('input.amzn_option_name').first().val();
        $(this).closest('div.amazon-model-item').remove();
        
        var option = $("#model_" + content + "_specific_options").find('option[value="' + selected + '"]');
        option.removeAttr("disabled");
        
        $("#model_" + content + "_specific_options").trigger('chosen:updated');

        var maxOccurs = +($(option).attr("maxOccurs"));
        var currentOccurs = +($(option).attr("currentOccurs"));
        if( !isNaN(maxOccurs) && !isNaN(currentOccurs) && maxOccurs > 1){
            var name = $(option).attr("value");
            var found = $("#model_" + content + "_specific_fields").find('[name="model[' + content + '][specific_fields]['+name+'][value]"]').length;
            $(option).attr("currentOccurs",found);
            if(maxOccurs <= found){
                $(option).attr("disabled","disabled");
            }
        }
    });    

    $('#content_'+name).find("select[rel=universe]").unbind('change').on('change',function(){
        showLoading($(this));
        var name = $(this).attr('data-content');//$(this).closest('div.amazon-model').find('input[rel="name"]').attr('data-content');
        var category = $(this).val();

        getProductTypes(name, category);

    });
    console.log($('#content_'+name).find("select[rel=product_type]"));
    $('#content_'+name).find("select[rel=product_type]").unbind('change').on('change',function(){
        showLoading($(this));
        var name = $(this).attr('data-content'); //$(this).closest('div.amazon-model').find('input[rel="name"]').attr('data-content');
        var category = $('#model_' + name + '_universe').val() //$(this).closest('div.amazon-model').find('select[rel="universe"]').val();
        var type = $(this).val();

        getProductSubtype(name, category, type);
        getVariationTheme(name, category, type);
        getMfrPartNumber(name, category, type);       
    });    
        
    $('#content_'+name).find("select[rel='variation_theme']").unbind('change').on('change',function(){
        showLoading($(this));
        var name = $(this).attr('data-content');
        var category = $('#model_' + name + '_universe').val();
        var type = $('#model_' + name + '_product_type').val();
        var variat = $(this).val();
        
        getVariationData(name, category, type, variat);
        //getSpecificOptions(name, category, type);
        
    });
    
    $('#content_'+name).find("select[rel='VariationTheme']").unbind('change').on('change',function(){
        showLoading($(this));
        var name = $(this).attr('data-content');
        var category = $('#model_' + name + '_universe').val();
        var type = $('#model_' + name + '_product_type').val();
        var variat = $(this).val();
        
        getVariationData(name, category, type, variat);
        //getSpecificOptions(name, category, type);
        
    });
    
    $('#content_'+name).find("select[rel=sub_product_type]").unbind('change').on('change',function(){
        showLoading($(this));
        var name = $(this).attr('data-content');
        var category = $('#model_' + name + '_universe').val();
        var type = $('#model_' + name + '_product_type').val();        
        //getVariationTheme(name, category, type);
        getMfrPartNumber(name, category, type);

    });   

    $('#content_'+name).find('div[rel="variation_data"], div[rel="recommended_data"], div[rel="specific_fields"]').each( function()
    {
        var name = $(this).attr('data-content');
        var validation_rel = $(this).attr('rel');
        
        setValuseNames(validation_rel, name);
    });

    /*$('.remove_model').unbind('click').on("click",function(){
        remove($(this));
    });
    $('.edit_model').unbind('click').on("click",function(){
        edit($(this));
    });*/
    
    setName('MfrPartNumber', 'mfr_part_number');
    setName('ProductSubtype', 'sub_product_type');
    setName('VariationTheme', 'variation_theme');
}

function setValues(data, name){
    
    var n = name;
    
    if(data === null){
               
        $('#model_'+n+'_product_subtype_container').html("");
        $('#model_'+n+'_mfr_part_number_container').html("");
        $('#model_'+n+'_variation_theme_container').html("");
        $('#model_'+n+'_recommended_data').html("");
        $('#model_'+n+'_variation_data').html("");
        $('#model_'+n+'_specific_options').html("");
        $('#model_'+n+'_specific_fields').html("");
        
    } else if(typeof data !== "undefined") {
        
        //universe
        if(typeof data.universe !== "undefined"){
            $('#model_'+n+'_universe').html(data.universe);
            hideLoading($('#model_'+n+'_universe').parent().parent());
        }
        
        //product_type        
        if(typeof data.product_types !== "undefined"){            
            $('#model_'+n+'_product_type').html(data.product_types);
            hideLoading($('#model_'+n+'_product_type').parent().parent());
        } 
        
        //MfrPartNumber
        if(typeof data.mfr_part_number !== "undefined"){
            $('#model_'+n+'_mfr_part_number_container').html(data.mfr_part_number);            
            setName('MfrPartNumber', 'mfr_part_number');
            hideLoading($('#model_'+n+'_mfr_part_number_container'));
        }
        
        //product_subtype
        if(typeof data.product_subtype !== "undefined"){
            $('#model_'+n+'_product_subtype_container').html(data.product_subtype);             
            setName('sub_product_type', 'sub_product_type');
            hideLoading($('#model_'+n+'_product_subtype_container'));
        }
        
        //variation_theme
        if(typeof data.variation_theme !== "undefined"){
            
            if(data.variation_theme === "" || data.variation_theme === null){
                $('#model_'+n+'_variation_theme_container').html("");
            }else{                    
                $('#model_'+n+'_variation_theme_container').html('<p class="head_text p-t20 p-b20 b-Top clearfix m-b10" style="display: block;">'+$('#variation').val()+'</p>' + data.variation_theme);                
                setName('VariationTheme', 'variation_theme');
            }
            hideLoading($('#model_'+n+'_variation_theme_container'));
        }
        
        //recommended_data
        if(typeof data.recommended_data !== "undefined"){
            if(data.recommended_data === "" || data.recommended_data === null){
                $('#model_'+n+'_recommended_data').html("");
            }else{
                $('#model_'+n+'_recommended_data').html('<p class="head_text p-t20 p-b20 b-Top clearfix m-b10" style="display: block;">'+$('#recommened_fields').val()+'</p>' + data.recommended_data);
                
                setValuseNames('recommended_data', n);
            }
            
            hideLoading($('#model_'+n+'_recommended_data'));
        }
        
        //variation_data
        if(typeof data.variation_data !== "undefined"){
            
            if(data.variation_data === "" || data.variation_data === null){
                $('#model_'+n+'_variation_data').html("");
            }else{
                $('#model_'+n+'_variation_data').html(data.variation_data);
                
                setValuseNames('variation_data', n);
            }            
            hideLoading($('#model_'+n+'_variation_data'));
        }
        
        if(typeof data.specific_options !== "undefined"){
            if(data.specific_options === "" || data.specific_options === null){
                $('#model_'+n+'_specific_options').html("");
            }else{
                $('#model_'+n+'_specific_options').html(data.specific_options);
            }                
            hideLoading($('.specific_fields_options'));            
        }
        
        if(typeof data.specific_fields !== "undefined"){
            
            if(data.specific_fields === "" || data.specific_fields === null){
                //$('#model_'+n+'_specific_fields').html("");
            }else{
                
                $('#model_'+n+'_specific_fields').append(data.specific_fields);
                
                if(typeof data.duplicate !== "undefined")
                    if(data.duplicate !== "" && data.duplicate !== null && data.duplicate === true)
                        $('#model_'+n+'_specific_fields').html(data.specific_fields);
            }
            setValuseNames('specific_fields', n);
            hideLoading($('#model_'+n+'_specific_fields'));
        }

        // update search select
        $(".search-select").chosen().trigger("chosen:updated");
        $(".search-select").parent().find('.chosen-single div').remove();                    
        $(".search-select").parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');

        // update checkbox
        updateAcheckBox();

        $('.tooltips').tooltipster({ 
            position:'right', 
            theme:'tooltipster-shadow', 
            contentAsHTML: true,
            animation: 'fade',
            touchDevices: true,
            //trigger: 'hover',
            //autoClose: true,
            //hideOnClick: false,
            interactive: true
        });
    }    

}

function custom_value(obj, element, name, attribute){

    if(obj.val() === "CustomValue")
    {
        //var ext = $('#ext').val();
        var product_type = $('#model_' + name + '_universe').val();
        var product_sub_type = ($('#model_' + name + '_product_type').length > 0) ? $('#model_' + name + '_product_type').val(): '';
        
        $.ajax({
            type: 'GET',
            url: base_url + "/amazon/get_valid_value/"+ name +"/"+ element +"/"+ ext +"/"+ product_type +"/"+ attribute +"/"+ product_sub_type,
            dataType:'json',
            beforeSend:function(jqXHR){
                jqXHR.fail(function(){ 
                    jqXHR.abort();
                });
            },
            success: function(data) {
                //amazon-model-item
                obj.closest('div.amazon-model-item').find('.valid_value').show().html(data);
                
                //obj.closest('div.amazon-model-item').find('.form-group.withIcon .chosen-container').attr('style', 'width: 80% !important');
                custom_to_default_value($('select[name="model[' + name + ']['+ element +']['+ attribute +'][CustomValue]"]'));
                choSelect();
            },
            error: function(data){
                console.log(data);
            }
        });
    }
    else
    {
        obj.closest('div.form-group').find('.valid_value').hide().html('');
    }
}

function setName(rel,element){
    var e = element;
    
    $('#tasks').find('select[rel="' + rel + '"], input[rel="' + rel + '"]').each( function()
    {
        if ( !$(this).attr('rel') )
            return ;
        //console.log(rel,element);        
        var n = $(this).closest('.amazon-models').find('input[rel="name"]').attr('data-content');
        $(this).attr('name','model[' + n + '][' + e + ']');
        $(this).attr('id','model_' + n + '_' + e );
        $(this).attr('rel', e );
        $(this).attr('data-content', n);
        
    });
}

function setValuseNames(element, name){
    var e = element;
    var n = name;
    var SpecificFieldsSelect = $('#model_'+n+'_'+e).find('select.search-select');
    
    if(SpecificFieldsSelect.length > 0)
    {
        SpecificFieldsSelect.each(function(i,j)
        {
            var SpecificRel = $(this).attr('rel');
            
            if( !$(this).hasClass('amzn_option_name') && !$(this).hasClass('attribute') && !$(this).hasClass('custom_value'))
            {               

//                if($(this).val() === "CustomValue")
//                {
//                    //console.log($(this).closest('div.amazon-model-item').find('.custom_value').attr('name','55'));
//                    //$(this).closest('div.amazon-model-item').find('.custom_value').attr('name','model[' + n + '][' + e + '][' + SpecificRel + '][CustomValue]');
//                } else {
                    $(this).attr('name','model[' + n + '][' + e + '][' + SpecificRel + '][value]');
                    $(this).attr('id','model_' + n + '_' + e + '_' + SpecificRel + '_value' );
                    $(this).attr('data-content', n);
//                }

                $(this).change(function(){
                    custom_value($(this), e, n, SpecificRel);
                });

                var SpecificDataAttr = $(this).closest('div.form-group').find('.attribute');

                SpecificDataAttr.each(function(){

                   var SpecificAttrRel = $(this).attr('rel');

                   $(this).attr('name','model[' + n + '][' + e + '][' + SpecificRel + '][attr][' + SpecificAttrRel + ']');
                   $(this).attr('id','model_' + n + '_' + e + '_' + SpecificRel + '_attr_' + SpecificAttrRel);
                   $(this).attr('data-content', n);

               });
            } else {
                //Custom Value
                if($(this).hasClass('custom_value')){
                     $(this).attr('name','model[' + n + '][' + e + '][' + SpecificRel + '][CustomValue]');
                }
            }
            
        });
    }

    var SpecificFieldsInput = $('#model_'+n+'_' + e).find('input');
    if(SpecificFieldsInput.length > 0)
    {
        SpecificFieldsInput.each(function()
        {

            if( !$(this).hasClass('amzn_option_name') /*&& !$(this).hasClass('custom_value')*/ )
            {
                var SpecificRel =  $(this).attr('rel');
                
                if(typeof SpecificRel !== "undefined") 
                {
                    $(this).attr('name','model[' + n + '][' + e + '][' + SpecificRel + '][value]');
                    $(this).attr('id','model_' + n + '_' + e + '_' + SpecificRel + '_value' );
                    $(this).attr('data-content', n);
                    
                    var SpecificDataAttr = $(this).closest('div.form-group').find('.attribute');

                    if(typeof SpecificAttrRel !== "undefined") 
                    {
                        SpecificDataAttr.each(function(){

                           var SpecificAttrRel = $(this).attr('rel');
                            
                           $(this).attr('name','model[' + n + '][' + e + '][' + SpecificRel + '][attr][' + SpecificAttrRel + ']');
                           $(this).attr('id','model_' + n + '_' + e + '_' + SpecificRel + '_attr_' + SpecificAttrRel);
                           $(this).attr('data-content', n);

                       });
                    }
                }
            }
        }); 
    }
    
}

function getProductTypes(n, c){
    
    setValues(null, name);
    var category = c;
    var name = n;

    $.ajax({
        type: 'POST',
        url: base_url + "amazon/form_data/" + $('#id_country').val(),
        data: 'product_category=' + category + '&action=product_types' + "&ext=" + ext,
        dataType:'json',

        beforeSend:function(jqXHR){
            jqXHR.fail(function(){ 
                jqXHR.abort();
                setTimeout(function (){ getProductTypes(n, c); }, 3000);
            });
        },
        success: function(data) {

            if(!data.product_types) {   
                getProductSubtype(name, category, null);
                //getMfrPartNumber(name, category, null);   
                //getVariationTheme(name, category, null);
            }
            applyRules(data, name);
        },
        error: function(data){
             setValues(null, name);
             $('#memory_peak').html("--");
            alert(data);
        }
    });
}

function getProductSubtype(n, c, t){
    
    setValues(null, name);
    var category = c;
    var name = n;
    var type = t;
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/form_data/" + $('#id_country').val(),
        data: 'product_category=' + category + '&product_type=' + type + '&action=product_subtype' + "&ext=" + ext,
        dataType:'json',

        beforeSend:function(jqXHR){
            jqXHR.fail(function(){ 
                jqXHR.abort();
                setTimeout(function (){ getProductSubtype(n, c, t); }, 3000);
            });
        },
        success: function(data) {
            ////setValues(data, name);
            console.log(data, name);
            getMfrPartNumber(name, category, t);
            getVariationTheme(name, category, type);
            applyRules(data, name); 
        },
        error: function(data){
            setValues(null, name);
            $('#memory_peak').html("--");
            alert(data);
        }
    });
}

function getMfrPartNumber(n, c, t){
    
    setValues(null, name);
    var category = c;
    var name = n;
    var type = t;
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/form_data/" + $('#id_country').val(),
        data: 'product_category=' + category + '&product_type=' + type + '&action=mfr_part_number' + "&ext=" + ext,
        dataType:'json',

        beforeSend:function(jqXHR){
            jqXHR.fail(function(){ 
                jqXHR.abort();
                setTimeout(function (){ getMfrPartNumber(n, c, t); }, 3000);
            });
        },
        success: function(data) {
            console.log(data);
            ////setValues(data, name);
            getVariationTheme(name, category, type);
            applyRules(data, name); 
        },
        error: function(data){
             setValues(null, name);
             $('#memory_peak').html("--");
            alert(data);
        }
    });
}

function getVariationTheme(n, c, t){
    
    setValues(null, name);
    var category = c;
    var name = n;
    var type = t;
    
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/form_data/" + $('#id_country').val(),
        data: 'product_category=' + category + '&product_type=' + type + '&action=variation_theme' + "&ext=" + ext,
        dataType:'json',

        beforeSend:function(jqXHR){
                jqXHR.fail(function(){ 
                    jqXHR.abort();
                    setTimeout(function (){ getVariationTheme(n, c, t); }, 3000);
                });
            },
        success: function(data) {
            data.specific_options = null;
            data.specific_fields = null;
            getVariationData(name, category, type, null);
            applyRules(data, name);
        },
        error: function(data){
            $('#memory_peak').html("--");
            alert(data);
        }
    });
    
}

function getVariationData(n, c, t, v){
    
    setValues(null, name);
    var category = c;
    var name = n;
    var type = t;
    var variation_theme =  v;
   
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/form_data/" + $('#id_country').val(),
        data: 'product_category=' + category + '&product_type=' + type + '&VariationTheme=' + variation_theme + '&action=variation_data&ext=' + ext,
        dataType:'json',

        beforeSend:function(jqXHR){
                jqXHR.fail(function(){ 
                    jqXHR.abort();
                    setTimeout(function (){ getVariationData(n, c, t, v); }, 3000);
                });
            },
        success: function(data) {
            data.specific_options = null;
            data.specific_fields = null;
            getSpecificOptions(name, category, type, variation_theme);
            applyRules(data, name);
        },
        error: function(data){
            $('#memory_peak').html("--");
            alert(data);
        }
    });
}

function getSpecificOptions(n, c, t, v){
    
    var category = c;
    var name = n;
    var type = t;
    var variation_theme =  v;
    
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/form_data/" + $('#id_country').val(),
        data: 'product_category=' + category + '&product_type=' + type + '&VariationTheme=' + variation_theme + '&action=specific_options' + "&ext=" + ext,
        dataType:'json',

        beforeSend:function(jqXHR){
            jqXHR.fail(function(){ 
                jqXHR.abort();
                setTimeout(function (){ getSpecificOptions(n, c, t, v); }, 3000);
            });
        },
        success: function(data) {
             //setValues(data, name);
             getSpecificFields(name, category, type, variation_theme);
             applyRules(data, name);
        },
        error: function(data){
             setValues(null, name);
             $('#memory_peak').html("--");
            alert(data);
        }
    });
}
            
function getSpecificFields(n, c, t, v){
    
    var category = c;
    var name = n;
    var type = t;
    var variation_theme =  v;
    //console.log( base_url + '/amazon/form_data?product_category=' + category + '&product_type=' + type + '&VariationTheme=' + variation_theme + '&action=specific_options');
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/form_data/" + $('#id_country').val(),
        data: 'product_category=' + category + '&product_type=' + type + '&VariationTheme=' + variation_theme + '&action=specific_options' + "&ext=" + ext,
        dataType:'json',

        beforeSend:function(jqXHR){
            jqXHR.fail(function(){ 
                jqXHR.abort();
                setTimeout(function (){ getSpecificFields(n, c, t, v); }, 3000);
            });
        },
        success: function(data) {
             data.specific_fields = null;
             //setValues(data, name);
             applyRules(data, name);
        },
        error: function(data){
             setValues(null, name);
             $('#memory_peak').html("--");
            alert(data);
        }
        });
}

function addSpecificField(so){
     
    //$(so).parent().find('.addSpecificField').hide();
    $(so).parent().find('.addSpecificField_Loading').show();
                 
                 console.log(so);
    var name = $(so).closest('div.amazon-model').find('input[rel=name]').attr('data-content');
    var category = $(so).closest('div.amazon-model').find('select[rel=universe]').val();
    var type = $(so).closest('div.amazon-model').find('select[rel=product_type]').val();

    var selected = so.val();
    var disable = true;

    //START Validation for elements with maxOccurs > 1
    var option_el = $(so).find("option:selected");
    var maxOccurs = +(option_el.attr("maxOccurs"));
    var currentOccurs = +(option_el.attr("currentOccurs"));
    var multiple = false;

    if(typeof maxOccurs !== "undefined" &&
       typeof currentOccurs !== "undefined" && 
       !isNaN(maxOccurs) && 
       !isNaN(currentOccurs)){

       if(maxOccurs <= currentOccurs){
            $(so).find('option[value="' + selected + '"]').attr("disabled","disabled");
            return;
       }else{
           option_el.attr("currentOccurs", currentOccurs+1);
       }
       multiple = true;
       if( maxOccurs !== (currentOccurs+1)){
           disable = false;
       }

    }
    //ENDS Validation for elements with maxOccurs > 1

    if(typeof selected === "undefined" || selected === ""){
        return;
    }
    if(disable){
        $(so).find('option[value="' + selected + '"]').attr("disabled","disabled");
    }
    
    //console.log(base_url + '/amazon/form_data/product_category=' + category + '&product_type=' + type + '&action=add_specific_field&item='+selected+'&multiple='+multiple+'&selected='+$('#selected_specific_fields').val());
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/form_data/" + $('#id_country').val(),
        data: 'product_category=' + category + '&product_type=' + type + '&action=add_specific_field&item='+selected+'&multiple='+multiple+'&ext=' + ext,
        dataType:'json',

        beforeSend:function(jqXHR){
            jqXHR.fail(function(){ 
                jqXHR.abort();
                setTimeout(function (){ addSpecificField(so); }, 3000);
            });
        },
        success: function(data) {
             $(so).val("");
         
             applyRules(data, name);
             $(so).parent().find('.addSpecificField').show();
             $(so).parent().find('.addSpecificField_Loading').hide();
        },
        error: function(data){
            $(so).val("");
            $('#memory_peak').html("--");
            alert(data);
        }
    });
}

function save_form(){
    
    $('#amazon-form').validate({
        submitHandler: function(form) 
        {     
            if(!valid_data())
            {
                notification_alert($('#error-message-title').val(), 'bad');                 
                return false;
            }
            else
            {
                if($('#popup_more').length > 0){
                    
                    form.submit();
                    
                } else {
                    
                    //bootbox.confirm($('#submit-confirm').val(), function(result) {
                        //if(result) {
                            form.submit();
                        //}                    
                    //});
                }
            }
        }
    });
       
}

function remove_error_label(){
    var has_error = $('.has-error');
    has_error.find("div.error").remove();
    has_error.removeClass('has-error');
    $(".form-group").removeClass("error");
    $('.ajax_notify').hide();
}

/*BEGIN VALIDATIONS*/
function valid_data(){

    remove_error_label();

    validations_ok = true;
    
    //For selects it only validates that a value is selected
    $('#tasks select:not(select[rel="specific_options"]):not(select[rel="VariationTheme"]):not(select[rel="variation_theme"]):not(select[rel="duplicate-model"]):not(div[rel="recommended_data"] select)').each(        
        function(i, item){
            
            var value = $(item).val();
            //console.log($(item));
            var error_msgs = new Array();
            var str = new String(value);
            if(str.length == 0){
                add_error_message(error_msgs, "A value must be set for this element");
            }
            //if exist any error these are shown/appended
            if(error_msgs.length > 0){
                for(var j=0;j<error_msgs.length;j++){
                    append_error_messages(item, error_msgs[j]);
                }
                $(item).closest('div.amazon-models').find('.amazon-model').removeClass('showSelectBlock');
                validations_ok = false;
            }
    });
    
    //For input elements, it verifies values according to XSD definition
    $('div[rel="variation_data"] input.custom_value, div[rel="specific_fields"] input.custom_value').each(function(i, item){
        //var this_value = $(item).val();
        var value = $(item).val() /*parseInt($(item).parent().find('input.custom_value').val());*/

        var item_name = 'p:first';
        var label = $(item).closest('div.amazon-model-item').find(item_name).html();
        if(typeof label == "undefined"){
            label = ' this element';
        }
        var error_msgs = new Array();
        var attr;
        
        //if( this_value == "CustomValue")
        //{
            if($(item).hasClass("negativeInteger")){
                if(!isNegativeInt(value)){
                    add_error_message(error_msgs, label + "&#39;s value must be a valid Negative Integer (..,-2,-1)");
                }
            }
            if($(item).hasClass("nonNegativeInteger")){
                if(!isNonNegativeInt(value)){
                    add_error_message(error_msgs, label + "&#39;s value must be a valid Non Negative Integer (0,1,2,..)");
                }
            }
            if($(item).hasClass("nonPositiveInteger")){
                if(!isNonPositiveInt(value)){
                    add_error_message(error_msgs, label + "&#39;s value must be a valid Non Positive Integer (..,-2,-1,0)");
                }
            }
            if($(item).hasClass("positiveInteger")){
                //console.log(isPositiveInt(value));
                if(!isPositiveInt(value)){
                    add_error_message(error_msgs, label + "&#39;s value must be a valid Positive Integer (1,2,..)");
                }
            }
            if($(item).hasClass("decimal")){
                if(!isNumber(value)){
                    add_error_message(error_msgs, label + "&#39;s value must be a Number");
                }
            }
            //check for validation attributes
            //Maximum length
            attr = $(item).attr("amzn_maxLength");
            if(typeof attr != "undefined" && attr){
                if(isNumber(attr) && value.length > attr){
                    add_error_message(error_msgs, label + "&#39;s Max length must be less or equal to: " + attr);
                }
            }
            //Minimum length
            attr = $(item).attr("amzn_minLength");
            if(typeof attr != "undefined" && attr){
                if(isNumber(attr) && value.length < attr){
                    add_error_message(error_msgs, label + "&#39;s Min length must be grater or equal to: " + attr);
                }
            }
            //String Pattern
            attr = $(item).attr("amzn_pattern");
            if(typeof attr != "undefined" && attr){
                var pattern = new RegExp(attr);
                if(!pattern.test(value)){
                    add_error_message(error_msgs, "Invalid pattern for " + label);
                }
            }
        //}
        
        //if exist any error these are shown/appended
        if(error_msgs.length > 0){
            for(var j=0;j<error_msgs.length;j++){
                append_error_messages(item, error_msgs[j]);
            }
            $(item).closest('div.amazon-models').find('.amazon-model').removeClass('showSelectBlock');
            validations_ok = false;
        }
    });

    return validations_ok;
}

function add_error_message(arr, text){
    arr[arr.length] = text;
}

function append_error_messages(item, msg){
   
    var el = $("<div>").addClass("error text-left").append(
      $('<p>').html('<i class="fa fa-exclamation-circle"></i>' + msg)
    );
      
    $(item).closest('div.form-group').addClass('has-error');      
    $(item).parent().append(el);
}

//checks if variable is a number
function isNumber(value){
    var str = new String(value);
    return  str.length > 0 &&
            typeof value !== 'undefined' &&
            !isNaN(value);
}
//checks if variable is an Integer
function isInt(value) {
    return(isNumber(value) && parseInt(Number(value)) === value);
}
//checks if variable is a negative Integer
function isNegativeInt(value) {
    return(isInt(value) && parseInt(Number(value)) < 0);
}
//checks if variable is a NonNegative Integer
function isNonNegativeInt(value) {
    //console.log(value);
    return(isInt(value) && parseInt(Number(value)) >= 0);
}
//checks if variable is a NonNegative Integer
function isNonPositiveInt(value) {
    return(isInt(value) && parseInt(Number(value)) <= 0);
}
//checks if variable is a positive Integer
function isPositiveInt(value) {
    return(isInt(value) && parseInt(Number(value)) > 0);
}
/// END OF VALIDATIONS

//update number of currentOccurs on load
function initSpecificFieldsOccurs(){
    $("#specific_options option").each(function(idx, option){
        var maxOccurs = +($(option).attr("maxOccurs"));
        var currentOccurs = +($(option).attr("currentOccurs"));
        if( !isNaN(maxOccurs) && !isNaN(currentOccurs) && maxOccurs > 1){
            var name = $(option).attr("value");
            var found = $("#specific_fields").find('[name="specific_fields['+name+'][]"]').length;
            $(option).attr("currentOccurs",found);
            
            if(maxOccurs <= found){
                $(option).attr("disabled","disabled");
            }
        }
    });
}

 //Remove
function delete_model(obj){
    
    var name = $(obj).attr('rel');
    
    bootbox.confirm( $('#confirm-delete-model-message').val() + ", " + name + "?", function(result) {

        if(result){
            var id = $(obj).val();
            var country = $('#id_country').val();
            
            $.ajax({
                type: "POST",
                url:  base_url + "/amazon/delete_data",
                data: 'id_model=' + id + '&id_country=' + country,
                success: function(rdata) 
                {
                    if(rdata === "true")
                    {
                        notification_alert($('#success-message').val() + ' ' + $('#delete-success-message').val() + ', "' + name + '".', 'good'); 

                        var count_id = parseInt($('#count').text()) - 1 ;
                        $('#count').html(count_id);   

                        $(obj).closest('div.amazon-models').remove();
                        
                        $('#main').find('select[id="duplicate-model"] option').each(function(){
                            if($(this).attr('value') !== "")
                            {
                                if(name === $(this).text())
                                    $(this).remove();
                            }
                        });
                    }
                    else
                    {
                        notification_alert($('#error-message').val() + ' ' + $('#delete-unsuccess-message').val() + ', "' + name + '".', 'bad');                         
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) 
                {
                    notification_alert($('#error-message').val() + ' ' +  textStatus + ' ' + errorThrown + '.', 'bad'); 
                }
            });
        }
    });
}

function edit(obj, name){
     
    if($(obj).closest('.amazon-models').find('div[rel="'+name+'"]').length > 0){       
        $('#content_'+name).html('');
    } else {        
        $(obj).find('.content').hide();
        $(obj).find('.edit-loading').show();
        $.ajax({
            type: 'POST',
            url: base_url + "/amazon/get_model/" + $('#id_country').val() + "/" + $(obj).val(),
            //contentType: "application/json; charset=utf-8",
            //dataType:'json',
            contentType: "text/plain",
            success: function(data) {
                $(obj).find('.content').show();
                $(obj).find('.edit-loading').hide();               
                //var response = data.html;
                //response.setCharacterEncoding("UTF-8");
                $('#content_'+name).html(data);

                applyRules(data, name);              
            },
            error: function(data){
                $(obj).find('.content').show();
                $(obj).find('.edit-loading').hide();
                console.log(data);
                //alert(data);
            }
        }); 
    }
   
    /*if($(obj).closest('div.amazon-models').find('div.amazon-model').hasClass('showSelectBlock')) {
        $(obj).closest('div.amazon-models').find('div.amazon-model').removeClass('showSelectBlock');
        $(obj).closest('div.amazon-models').find("select[rel=specific_options]").closest('.form-group').removeClass('disabled');
    } else {
        $(obj).closest('div.amazon-models').find('div.amazon-model').addClass('showSelectBlock');
    }*/
}

//Change name
function changeName(){
    
    $('#tasks').on('blur', 'input[rel="name"]', function()
    {
        var content = $(this).closest('.amazon-models') ;

        var duplicate = 0;
        var name = $.trim($(this).val()) ;
        
        if(name)
        {    
            name = name.replace(/[^a-zA-Z0-9\s_]/g,'').replace(/\s/g,'_');
            
            content.find('.custom-form').attr('id', 'content_'+name);

            if($('input[name="model[' + name + '][name]"]').length !== 0)
            {
                name = name + '_' + duplicate;
                duplicate++;                    
            } 
           
            content.find('select, input, div, span').each(function() 
            {
                if ( ! $(this).attr('rel') )
                    return ;
                
                if($(this).is('select, input')) {
                    $(this).removeAttr('disabled');
                    $(this).parent().removeClass('disabled');
                }
                
                if($(this).is('select')) {
                    $(this).parent().find('.chosen-single div').remove();
                    $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                    $(this).chosen().trigger("chosen:updated");
                }
                
                var field_name = $(this).attr('rel') ;
                var variable_prefix = 'model[' + name + '][' + field_name + ']' ;
                var id_prefix = 'model_' + name + '_' + field_name ;                              
                
                $(this).attr('name', variable_prefix) ;
                $(this).attr('id', id_prefix) ;
                $(this).attr('data-content', name) ;
            });  
        }
        else
        {            
            content.find('select, input, div, span').each(function() 
            {
                if($(this).is('select, input') && !$(this).is('input[rel="name"]') ) {
                    $(this).attr('disabled', true);
                    $(this).parent().addClass('disabled');
                    $(".search-select").trigger("chosen:updated");  
                }
            });  
        }
        
        content.find('div[rel="variation_data"], div[rel="recommended_data"], div[rel="specific_fields"]').each( function()
        {
            var name = $(this).attr('data-content');
            var validation_rel = $(this).attr('rel');

            setValuseNames(validation_rel, name);
        });
        
    });
}

//Duplicate
function getDuplicateModel(obj){
   //console.log(obj);
    if(obj.val() !== "")
    {
        $.ajax({
            type: 'POST',
            url: base_url + "/amazon/get_duplicate_model/" + obj.val() + "/" + $('#id_country').val(),
            dataType:'json',
            success: function(data) {
                //setValues(data, obj.attr('data-content'));
                applyRules(data, obj.attr('data-content'));
                console.log(data);
            },
            error: function(data){
                console.log(data);
                //alert(data);
            }
        });
    }
    else
    {
        setValues(null, obj.attr('data-content'));
        $('#model_'+obj.attr('data-content')+'_universe').html("");
        $('#model_'+obj.attr('data-content')+'_product_type').html("");
    }
}

// Add new Model
function add_model()
{
    var cloned = $('#main').clone().show();
    $('#tasks').prepend(cloned);

    cloned.attr('id', '');       
    cloned.removeClass('showSelectBlock');       

    var id_count = parseInt($('#count').text());
    var id_count_new = id_count+1;
    $('#count').html(id_count_new);

    var name;
    var category;

    cloned.find('select[rel="duplicate-model"]').on("change",function(){
       getDuplicateModel($(this));
    });

    cloned.find('.remove').on('click', function(){
        $('#count').html(parseInt($('#count').text()) - 1 );
        $(this).parent().parent().remove(); 
    });

    cloned.find('select[rel=universe]').on("change",function(){
        showLoading($(this));
        name = $(this).closest('div.amazon-models').find('input[rel="name"]').attr('data-content');
        category = $(this).val();
        getProductTypes(name, category);
    });

    cloned.find('input[rel=name]').focus();

    //set select chosen
    cloned.find('select').addClass('search-select').chosen();

    changeName();
}

$(document).ready(function(){

    //Add
    $('#add').click(function()
    {
        add_model();
    });
    
    if($('input#popup_status_more').length > 0)
    {
        add_model();
    }
    
    $('.remove_model').unbind('click').on("click",function(){
        delete_model($(this));
    });
    
    /*$('#tasks').find('div[rel="variation_data"], div[rel="recommended_data"], div[rel="specific_fields"]').each( function()
    {
        var name = $(this).attr('data-content');
        var validation_rel = $(this).attr('rel');
        
        setValuseNames(validation_rel, name);
    });*/

    initSpecificFieldsOccurs();
    //applyRules();
    save_form();
});

function custom_to_default_value(element){

    $(element).on('change', function(){
        var e = $(this);
       
        if(e.val() === 'CustomValue'){
            
            e.parent().append('<input type="text" id="'+e.attr('id')+'" name="'+e.attr('name')+'" rel="'+e.attr('rel')+'" data-content="'+e.attr('data-content')+'" class="form-control" />');
            e.next().remove();
            e.remove();
            //if($('input#'+e.attr('id')).length > 0){
                //e.removeAttr('name');
                //e.removeAttr('id');
            //}

        } 
        /*else {

            if($('input#'+e.attr('id')).length > 0){
                var name = $('input#'+e.attr('id')).attr('name');
                var id = $('input#'+e.attr('id')).attr('id');
                 console.log($('input#'+e.attr('id')));
                e.attr('name', name);
                e.attr('id', id);
                //$('input#'+e.attr('id')).remove();
            }


        }*/

    });

    //custom_value
}