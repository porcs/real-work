$(document).ready(function(){

   	$('head').append('<link rel="stylesheet" href="'+base_url+'assets/css/feedbiz/amazon/includescript.css">');
 	$('.feedbizcolor').sprite({fps: 15, no_of_frames: 30});
	updateAcheckBox();
	updateAradio();
});

var acheckBox = function($acheckbox){ 
    if ($acheckbox.hasClass('checked')) {
        $acheckbox.find('input').prop('checked', false).change();
        $acheckbox.removeClass('checked');
    } else {
        $acheckbox.find('input').prop('checked', true).change();
        $acheckbox.addClass('checked');
    }
}

var aRadio = function($acheckbox){ 
    var thisVal = $acheckbox.find('input').val();

    if (!$acheckbox.hasClass('checked')) {        
        $acheckbox.find('input').prop('checked', true).change();
        $acheckbox.addClass('checked');
    }
    $acheckbox.parent().find('.amazon-radio').each(function(i, j){
        var inputVal = $(j).find('input').val();
        if(inputVal !== thisVal) {
            if ($(j).hasClass('checked')) {
                $(j).find('input').prop('checked', false).change();
                $(j).removeClass('checked');
            }             
        }
    });
}

// update checkbox
var updateAcheckBox = function(){
    if($('input[type="checkbox"]').length > 0) {
        $acheckbox       = $('input[type="checkbox"]');
        $acheckbox.on('click', function(){
            $acb_checkbox    = $(this).closest('.amazon-checkbox');
            acheckBox($acb_checkbox);
        });
    }
}

// update radio
var updateAradio = function(){
    if($('input[type="radio"]').length > 0) {
        $acheckbox       = $('input[type="radio"]');
        $acheckbox.on('click', function(){
            $acb_checkbox    = $(this).closest('.amazon-radio');
            aRadio($acb_checkbox);
        });
    }
}

jQuery(function (a, b) {
    a.fn.feed_colorpicker = function (c) {        
        var d = a.extend({
            pull_right: false,
            caret: true
        }, c);
        this.each(function () {
            var g = a(this);
            var e = "";
            var f = "";
            a(this).hide().find("option").each(function () {
                var h = "colorpick-btn";
                if (this.selected) {
                    h += " selected";
                    f = this.text;
                }
                // rel="text"
                if (this.title && this.title === "text")
                {
                    e += '<li>';
                    e += '<a class="' + h + '" href="#" data-color="' + this.value + '" title="' + this.value + '" style="width: 100%; text-align: center; background-color:' + this.text + '; font-size: 12px;">' + this.value + '</a>';
                    e += '</li>';
                }
                else
                {
                    e += '<li>';
                    e += '<a class="' + h + '" href="#" style="background-color:' + this.value + ';" data-color="' + this.value + '" title="' + this.value + '"></a>';
                    e += '</li>';
                }
            })
            .end()
            .on("change", function () {
                if (this.options[this.selectedIndex].title && this.options[this.selectedIndex].title === "text")
                {
                    console.log(this.text);
                    a(this).next().find(".btn-colorpicker").removeAttr('style');
                    a(this).next().find(".btn-colorpicker").css('border', '1px solid #ccc').css("background-color", this.options[this.selectedIndex].text).css("height", '22px').css("width", '22px').html('').parent().find('.value-colorpicker').html(this.options[this.selectedIndex].value);
                }
                else
                {
                    a(this).next().find(".btn-colorpicker").css("background-color", this.options[this.selectedIndex].text).css("width", '22px').css("height", '22px').html('');
                }
            })
            /*feed_colorpicker*/
            .after('<div class="dropdown dropdown-colorpicker m-t6 clearfix"><a data-toggle="dropdown" class="dropdown-toggle a-value-colorpicker"><div class="btn-colorpicker pull-left" style="' + (this.options[this.selectedIndex].title && this.options[this.selectedIndex].title === "text" ? "background:" + this.options[this.selectedIndex].text + ";width:22px;height:22px;border: 1px solid #e2e2e2;" : 'background-color:' + f + ';') + '">&nbsp;</div> <div class="value-colorpicker poor-gray pull-left small m-t5 m-l10">' + this.options[this.selectedIndex].value + '</div></a><ul class="dropdown-menu' + (d.caret ? " dropdown-caret" : "") + (d.pull_right ? " pull-right" : "") + '" style="max-width: 130px; min-width: 130px">' + e + '</ul></div>')
            .next()
            .find(".dropdown-menu")
            .on("click", function (j) {
                var h = a(j.target);
                if (!h.is(".colorpick-btn")) {
                    return false;
                }
                h.closest("ul").find(".selected").removeClass("selected");
                h.addClass("selected");
                var i = h.data("color");
                g.val(i).change();
                j.preventDefault();
                return true;
            });
        });
        return this;
    };
});

$('#add').click(function()
{
    if($('#no-data').length > 0){
        $('#no-data').slideUp(500);
    }
});

$('.amazon_trigger_reprice_addSku').click(function()
{
    var cloned = $(this).closest('.form-group').clone();
    $(cloned).find('.amazon_trigger_reprice_addSku').hide();
    $(cloned).find('.amazon_trigger_reprice_removeSku').show().on("click", function (i,j) {
         $(this).closest('.form-group').remove();
    });
    $('#amazon_trigger_reprice_div').append(cloned);
});
