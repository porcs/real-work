var table_error_logs ;

$(document).ready(function () {
    
    // error log
    $('#error_logs').dataTable( {
         "language": {
            "emptyTable":     $('#emptyError').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "amazon/report_error_log/" + $('#id_country').val(),
            "type": "POST"
        },
        "columns": [           
            { "data": "sku", "bSortable": true, "width": "20%" },
            { "data": "result_message_code", "bSortable": true, "width": "15%" },
            { "data": "result_description", "bSortable": false, "width": "55%"  }
        ],       
        "pagingType": "full_numbers"
    } );
    
    table_error_logs = $('#error_logs').DataTable();    
    
    // DataTables initialisation
    $('#product_logs').dataTable( {
         "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "amazon/report_page/" + $('#id_country').val(),
            "type": "POST"
        },
        "columns": [           
            { "data": "batch_id", "bSortable": true, "width": "11%" },
            { "data": "action_type", "bSortable": true, "width": "20%" },
            { "data": "feed_type", "bSortable": true, "width": "15%" },
            { "data": null, "bSortable": false, "width": "15%", 
                 render: function ( data, type, row ) {
                    var log_link = data.feed_sunmission_id;
                    /*if(data.feed_type && data.feed_type != 'OrderFulfillment') {                       
                        log_link = '<button class="link submission_id" type="button" value="'+data.feed_sunmission_id+'">'+data.feed_sunmission_id+'</button>';
                    } else {
                        log_link = '<button class="link submission_id" type="button" value="'+data.feed_sunmission_id+'">'+data.feed_sunmission_id+'</button>';
                    }*/
                    return log_link;
                }
            },
            { "data": "messages_processed", "bSortable": false, "width": "8%", "class" : "text-right p-r10", },
            { "data": "messages_successful" , "bSortable": false, "width": "8%", "class" : "text-right p-r10", },
            //{ "data": "messages_with_error" , "bSortable": false, "width": "8%"},   
            { "data": null, "bSortable": false, "width": "8%", "class" : "text-right p-r15",
                 render: function ( data ) {
                    if(data.messages_with_error > 0) {
                        return '<span class="feed_error">' + data.messages_with_error + '</span>';
                    } else {
                        return data.messages_with_error;
                    }
                }
            },         
            { "data": "date_add", "bSortable": true, "width": "15%"},      
        ],       
        "order": [[7, 'desc']],
        "pagingType": "full_numbers",       
        "initComplete": function () {
            var api = this.api();
            
            var column1 = api.column(1);
            var select1 = $('<select class="search-select"><option value="">--</option></select>')
                .appendTo( $(column1.footer()).empty() )
                .on( 'change', function () {
                    var val =  $(this).val();
                    column1
                        .search( val ? val : '', true, false )
                        .draw();
                } );

            select1.append( '<option value="create">Product Creation</option>' );
            select1.append( '<option value="delete">Delete</option>' );
            select1.append( '<option value="sync">Update Offers</option>' );
            select1.append( '<option value="Orders">Update Orders</option>' );
            select1.append( '<option value="Repricing">Repricing</option>' );
            
            var column2 = api.column(2);
            var select2 = $('<select class="search-select"><option value="">--</option></select>')
                .appendTo( $(column2.footer()).empty() )
                .on( 'change', function () {
                    var val =  $(this).val();
                    column2
                        .search( val ? val : '', true, false )
                        .draw();
                } );

            select2.append( '<option value="Product">Product</option>' );
            select2.append( '<option value="Inventory">Inventory</option>' );
            select2.append( '<option value="Price">Price</option>' );
            select2.append( '<option value="Image">Image</option>' );
            select2.append( '<option value="Relationship">Relationship</option>' );
            select2.append( '<option value="OrderFulfillment">OrderFulfillment</option>' );
            select2.append( '<option value="OrderAcknowledgement">OrderAcknowledgement</option>' );

            choSelect();

        },
        
    } );

    $('#product_logs tbody').on( 'click', 'tr td', function () {

        if ( $(this).closest('tr').hasClass('selected') ) {
            $(this).closest('tr').removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).closest('tr').addClass('selected');
        }

        var tr = $(this).closest('tr');
        var row = table.row( tr );
        var data = row.data();

        $('html, body').animate({
            scrollTop: $(".error_logs").offset().top
        }, 2000);

       
        $('#error_logs').closest('.error_logs').removeAttr('style');
        table_error_logs.search( data.feed_sunmission_id ).draw();
    } );

    $('#product_logs tfoot th').each( function (i) {
        var title = $('#product_logs tfoot th').eq( i ).text();
        if(title != "") {
            $(this).parents('th').attr('data-column', i);
            $(this).html( '<input type="text" class="form-control column_filter" placeholder="Search:" id="col'+i+'_filter" />' );
        }
    } );
    
    var table = $('#product_logs').DataTable();
    table.columns().eq( 0 ).each( function ( colIdx ) {
    $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
        table
            .column( colIdx )
            .search( this.value )
            .draw();
    } );


} );

});