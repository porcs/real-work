$(document).ready(function () {
    $('#my_task_list').DataTable({ searching:false, paging: false});
    var ext = $('#my_task_list').attr('ext');
    var market = $('#my_task_list').attr('market');
    var id_country = $('#id_country').val();
    var count_fail = 0;
    $('.countdown_next').each(function(){      
//        console.log($(this).attr('rel'));
        var main_obj = $(this);
        $(this).countdown({
            timestamp	: (new Date()).getTime()+ $(this).attr('rel')*1000,
            start_timestamp: $(this).attr('rel'),
            callback	: function(days, hours, minutes, seconds){},
            break_sign  : ':',
            timerEnd:function(){
                var row = main_obj.parents('tr');
                var key = row.attr('key');  
                var obj = $(this);
                obj.row = row; 
                 
                if(count_fail>10 || !alive)return;
                
                $.ajax({ method:'post',url: base_url+'tasks/ajax_get_next_time',data:{process_key:key},
                    success:function(r){
                        if(r){
                            obj.row.find('.countdown_next').attr('rel',r); 
                            if(count_fail>0)count_fail--;
                        }else{
                            count_fail++;
                        }
//                        console.log(obj.row.find('.countdown_next'),obj.row.find('.countdown_next').attr('rel'))
                    },
                    error:function(){
                        count_fail++;
                    }
                });
            }
        });
    });
    
    $('table thead tr').addClass("b-Bottom");
    
    $('#my_task_list input').checkBo();
    var skip_ask_q=false;
    setTimeout(function(){
    $('#my_task_list input[type=checkbox]').trigger('change'); 
        $('#my_task_list input[type=checkbox]').change(function(){
            if($(this).hasClass('sel_all')){
                obj = $(this);
                 bootbox.confirm($('#confirm_update_cron_task_txt').val(), function(result) {
                        if(result) {
                            skip_ask_q = true;
                            $('#my_task_list input[type=checkbox].sel_row').prop('checked',obj.is(':checked')).trigger('change');
                            setTimeout(function(){
                                skip_ask_q = false;
                            },300);
                        }else{
//                            var status = obj.is(':checked');
//                            obj.prop('checked',!status);
//                            if(status){
//                                 obj.parents('.cb-checkbox').removeClass('checked');
//                            }else{
//                                obj.parents('.cb-checkbox').addClass('checked');
//                            }
                        }                 
                    });
                
            }else{
                var cid = $(this).attr('crel');
                var status = $(this).is(':checked');
                if(skip_ask_q){
                    $.ajax({ method:'post',url: base_url+'amazon/ajax_update_cron_status',data:{ ext:ext,cid:cid,status:status},
                        success:function(data){
                            console.log(data);
                        }
                    });
                }else{
                    var obj = $(this);
                    bootbox.confirm($('#confirm_update_cron_task_txt').val(), function(result) {
                         if(result) {
                            $.ajax({ method:'post',url: base_url+'amazon/ajax_update_cron_status',data:{ ext:ext,cid:cid,status:status},
                                success:function(data){
                                    console.log(data);
                                }
                            });
                        }else{
                            obj.prop('checked',!status);
                            if(status){
                                 obj.parents('.cb-checkbox').removeClass('checked');
                            }else{
                                obj.parents('.cb-checkbox').addClass('checked');
                            }
                        }
                    });
                }
                
                
            }
            
        });
        $('table th:eq(2)').prepend('<div id="category_options_processing" class="dataTables_processing"></div>');

            if($('.dataTables_processing').length > 0){
                $('.dataTables_processing').html('');
                $('.dataTables_processing').append('<div class="feedbizloader"><div class="feedbizcolor"></div><p class="m-t10 text-info">'+$('#processing').val()+'</p></div>');
                if($('.dataTables_processing').find('.feedbizloader')){
                    $('.dataTables_processing').find('.feedbizloader').find('.feedbizcolor').sprite({fps: 10, no_of_frames: 30});
                }
            }
             if($('.dataTables_processing').length > 0){
                $('.dataTables_processing').show();
            }  
        var run = [];
        $(".task_row").each(function(){
            var rel = $(this).attr('rel');
            if(rel!='order'){
                rel = 'product';
                $(this).attr('rel',rel);
            }
            
            if(!run[rel]){
                run[rel]=true;
                $.ajax({ method:'post',url: base_url+'amazon/ajax_get_serv_status',data:{ /*ext:ext,market:market,*/task:rel,id_country:id_country},dataType:'json',
                    success:function(data){
 
                        var status = data.status;
                        var timestamp = data.timestamp;
                        $('#my_task_list').find('tr.task_row[rel='+rel+']').each(function(){
                           $(this).find('.t_time').html(timestamp);
                           $(this).find('.t_status').addClass(status);
                        });
                        
                        if($('.dataTables_processing').length > 0){
                            $('.dataTables_processing').hide();
                        }
                    }
                });
            }
        });
    },100);
    
    
});