$(document).ready(function() {

    $('head').append('<style>.form-group.disabled:after{content: "";position: absolute;background: none;width: 0;height: 0;top: 0;right: 0;}</style>');

    $('#back').on('click', function() {
        window.location.replace(base_url + "amazon/mappings/" + $('#id_country').val());
    });

    $('#form-mapping').validate({
        submitHandler: function(form)
        {
            form.submit();
        }
    });

    $('.removemappingcarrier').click(function()
    {
        removemappingcarrier($(this));
    });

    $('.addnewmappingcarrier').click(function()
    {
        var type = $(this).attr('rel');        
        var cloned = $('#main-'+type+'-carrier').clone().show().appendTo('#new-mapping-'+type+'-carrier');

        // Remove old chosen
        if(cloned.find('div.chosen-container').length > 0){
            cloned.find('div.chosen-container').remove();
        }

        cloned.removeAttr('id');
        cloned.find('input').val('').attr('disabled', false);
        cloned.find('select').val('').attr('disabled', false);        
        cloned.find('select[rel="'+type+'-carrier-value"] option:first').text('--');
        
        var selected_select = cloned.find('select[rel="'+type+'-carrier-select"]');

        // Remove used options
        $('#new-mapping-'+type+'-carrier').find('select[rel="'+type+'-carrier-select"] option:selected').each(function(index, option)
        {
            selected_select.find('option[value="' + option.value + '"]').remove();
        });

        if (!selected_select.find('option').length)
        {
            cloned.remove();
            return(false);
        }

        var selected_value = selected_select.find('option:first').val();

        selected_select.attr('id', type+'-carrier-select-' + selected_value);
        selected_select.attr('data-content', selected_value);

        cloned.find('select[rel="'+type+'-carrier-value"]').attr('id', type+'-carrier-value-' + selected_value);
        cloned.find('select[rel="'+type+'-carrier-value"]').attr('name', 'carrier['+type+'][' + selected_value + '][id_carrier]');        
        cloned.find('select[rel="'+type+'-carrier-value"]').attr('data-content', selected_value);
        
        selected_select.on('change', function() {           
            var selected_option = $(this).find('option:selected').val();
            $(this).attr('id', type+'-carrier-select-' + selected_option);

            //change attribute value id
            $('#'+type+'-carrier-value-' + selected_value).attr('name', 'carrier['+type+'][' + selected_option + '][id_carrier]');
            $('#'+type+'-carrier-value-' + selected_value).attr('id', type+'-carrier-value-' + selected_option);

            //change attribute id
            selected_select.attr('id', type+'-carrier-select-' + selected_option);
            selected_select.attr('data-content', selected_option);
            cloned.find('select[rel="'+type+'-carrier-value"]').attr('data-content', selected_option);
        });

        cloned.find('.addnewmappingcarrier').remove();
        cloned.find('.removemappingcarrier').show().click(function()
        {
            removemappingcarrier($(this));
        });
        
        cloned.find('select[rel="outgoing-carrier-value"]').on('change', function() {
            var val = $(this);
            if (val.val() == "Other"){
                free_text(val);
            } else {
                val.parent().find('input').remove();
            }
        });

        cloned.find('input[rel="'+type+'-carrier-other"]').remove();
        
        choSelect();

    });
    
    $('#new-mapping-fba-carrier').find('.carrier-select-value').on('change', function() {
        change_name($(this), 'fba');
    });
    $('#new-mapping-fba-carrier').find('.carrier-value').on('change', function() {
        change_name($(this), 'fba');
    });
    
    $('#new-mapping-incoming-carrier').find('.carrier-select-value').on('change', function() {
        change_name($(this), 'incoming');
    });
    $('#new-mapping-incoming-carrier').find('.carrier-value').on('change', function() {
        change_name($(this), 'incoming');
    });
    
    $('#new-mapping-outgoing-carrier').find('.carrier-select-value').on('change', function() {
        change_name($(this), 'outgoing');      
    });
    $('#new-mapping-outgoing-carrier').find('.carrier-value').on('change', function() {
        change_name($(this), 'outgoing');       
    });

    $('select[rel="outgoing-carrier-value"]').unbind('change').on('change', function() {
        var val = $(this);
        if (val.val() == "Other"){
            free_text(val);
        } else {
            val.parent().find('input').remove();
        }
    });
    
    /*$('select[rel="incoming-carrier-value"]').unbind('change').on('change', function() {
        //$(this).hide();
    });*/

});

function change_name(e, type){
    
    var main = $(e).closest('.new-mapping-'+type+'-carrier');
    var carrier_value =  main.find('select[rel="'+type+'-carrier-value"]');
    var selected_option = main.find('.carrier-select-value').find('option:selected').val();

    //change attribute value id
    carrier_value.attr('name', 'carrier['+type+'][' + selected_option + '][id_carrier]');
    carrier_value.attr('id', type+'-carrier-value-' + selected_option);
    carrier_value.attr('data-content', selected_option);

    choSelect();
}

function removemappingcarrier(obj)
{
    $(obj).parent().parent().parent().remove();
}

function free_text(e)
{        
    var id = e.attr('data-content');
    $.ajax({
        type: "get",
        url: base_url + "amazon/carriers/"+$('#id_country').val()+"/"+id,
        dataType: 'json',
        timeout: 0,
        success: function (r) {                     
            e.parent().append(r);
            e.parent().find('input').removeAttr('id');
            e.parent().find('input').attr('rel', 'outgoing-carrier-other');
        }
    });
}