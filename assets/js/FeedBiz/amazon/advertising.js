var table_error_logs ;

var table_language = {};
table_language.emptyTable           = $('#emptyError').val();
table_language.info                 = $('#info').val();
table_language.infoEmpty            = $('#infoEmpty').val();
table_language.infoFiltered         = $('#infoFiltered').val();
table_language.infoPostFix          = $('#infoPostFix').val();
table_language.thousands            = $('#thousands').val();
table_language.lengthMenu           = $('#lengthMenu').val();
table_language.loadingRecords       = $('#loadingRecords').val();
table_language.processing           = $('#processing').val();
table_language.search               = '';
table_language.zeroRecords          = $('#zeroRecords').val();
table_language.paginate = {};
table_language.paginate.first       = $('#paginate-first').val();
table_language.paginate.last        = $('#paginate-last').val();
table_language.paginate.next        = $('#paginate-next').val();
table_language.paginate.previous    = $('#paginate-previous').val();
table_language.aria = {};
table_language.aria.sortAscending   = $('#aria-sortAscending').val();
table_language.aria.sortDescending  = $('#aria-sortDescending').val();

$(document).ready(function () {

    $('head').append('<link rel="stylesheet" href="'+base_url+'assets/css/bootstrap-tagsinput.css">');

    $('#next').on('click', function(){
        var page = $(this).val();
        var next_sub_action = '';
        if($('#next_sub_action').length > 0){
            next_sub_action = '/' + $('#next_sub_action').val();
        }        
        window.location.href = base_url + "amazon/" + page + "/" + $('#id_country').val() + next_sub_action;
    });
    $('#back').on('click', function(){
        var page = $(this).val();
        window.location.href = base_url + "amazon/" + page + "/" + $('#id_country').val();
    });

    $('#next2').on('click', function(){
        var page = $(this).val();
        var next_sub_action = '';
        if($('#next_sub_action').length > 0){
            next_sub_action = '/' + $('#next_sub_action').val();
        }        
        window.location.href = base_url + "amazon/" + page + "/" + $('#id_country').val() + next_sub_action;
    });

    /*$('#RequestAccessToken').on('click', function() {
        $.ajax({
            type: "get",
            url: "https://dev.feed.biz/amazon_handle/authorization/" + $('#code').val(),
            dataType: 'json',
            timeout: 0,
            success: function (r) {
                console.log(r);                   
                if (r)  {
                    var obj = r;
                    $(obj).each(function(i, j){
                        var response = jQuery.parseJSON( j.response );
                        $(response).each(function(k, v){
                            if( k==="access_token"){
                                $('#token_access').val(v);
                            }
                        });
                        $('#amazon-root').append("<hr/>RequestAccessToken<hr/>" + i + " : " +j.response+ "<br/>");
                    });
                } else {
                    $('#amazon-root').html('False');
                }
            }
        });
    });*/
    $('#Register').on('click', function() {
        window.location.href = base_url + "amazon/advertising/authentication/" + $('#id_country').val()+"/3";
        /*$.ajax({
            type: "get",
            url: base_url + "amazon/advertising/authentication/" + $('#id_country').val()+"/3",
            dataType: 'json',
            timeout: 0,
            success: function (r) {
                console.log(r);
                if (r) {
                    var obj = r;
                    $(obj).each(function(i, j){
                        var response = jQuery.parseJSON( j.response );
                        $(response).each(function(k, v){
                            if( k==="registerProfileId"){
                                $('#token_access').val(v);
                            }
                        });
                    $('#amazon-root').append("<hr/>Register<hr/>" + i + " : " +j.response+ "<br/>");
                    });
                } else {
                    $('#amazon-root').html('False');
                }
            }
        });*/
    });
    /*$('#GetProfileStatus').on('click', function() {

        var adData = {};
        adData.token_access = $('#token_access').val();
        adData.register_profile_id = $('#register_profile_id').val();

        $.ajax({
            type: "POST",
            //url: base_url + "amazon_handle/registerProfileStatus/" + $('#token_access').val()+"/" + $('#register_profile_id').val(),
            url: base_url + "amazon/advertising/authentication/" + $('#id_country').val()+"/3",
            dataType: 'json',
            data: adData,
            timeout: 0,
            success: function (r) {
                console.log(r);
                if (r)
                {
                    /*var obj = r;
                    $(obj).each(function(i, j){
                        if(j.response.status === "SUCCESS"){
                            window.location.href = base_url + "amazon/advertising/login/" + $('#id_country').val()+"/3";
                        }
                        //$('#amazon-root').append("<hr/>GetProfileStatus<hr/>" + i + " : " +j.response+ "<br/>");
                    });*/
                /*} else {
                    //$('#amazon-root').html('False');
                }
            }
        });
    });*/

    // profile
    $('#ad_profile').dataTable( {
        "language": table_language,
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "amazon/advertising/get_profiles/" + $('#id_country').val(),
            "type": "POST"
        },
        "columns": [           
            { "data": "profileId", "bSortable": false, "width": "10%" },
            { "data": null, "bSortable": false, "width": "15%",
                 render: function ( data ) {
                    return data.accountInfo.marketplaceStringId;
                }
            }, 
            { "data": null, "bSortable": false, "width": "15%",
                 render: function ( data ) {
                    return data.accountInfo.sellerStringId;
                }
            }, 
            { "data": "timezone", "bSortable": false, "width": "15%"},
            { "data": "countryCode", "bSortable": false, "width": "10%", "class" : "text-right p-r5" },
            { "data": "currencyCode", "bSortable": false, "width": "10%" , "class" : "text-right p-r5"},
            { "data": "dailyBudget", "bSortable": false, "width": "12%", "class" : "text-right p-r5"},
            { "data": null, "bSortable": false, "width": "8%", "class" : "text-right",
                 render: function ( data ) {
                    return '<button type="button" class="link m-r5" style="color: #00ce88;" value="'+data.profileId+'" data-toggle="modal" data-target="#profileDetails" onclick="get_profile(this);"><div class="content"><i class="fa fa-pencil"></i> ' + $('#Edit').val() + '</div></button>';
                }
            },    
        ],       
        "bPaginate": false,
        "bFilter": false,        
        "bInfo": true,
        "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
           return $('#Total').val() + ' : ' + iMax + ' ' + $('#items').val() ;
        },
    } ); 
    
    // Campaigns
    $('#ad_campaigns').dataTable( {
        "language": table_language,
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "amazon/advertising/get_campaigns/" + $('#id_country').val(),
            "type": "POST"
        },
        "columns": [           
            { "data": "campaignId", "bSortable": false, "width": "15%" },
            { "data": "name", "bSortable": false, "width": "15%" },
            { "data": "campaignType", "bSortable": false, "width": "10%"},
            { "data": "targetingType", "bSortable": false, "width": "10%", "class" : "text-right p-r5" },
            { "data": "dailyBudget", "bSortable": false, "width": "10%", "class" : "text-right p-r5"},
            { "data": "startDate", "bSortable": false, "width": "10%" , "class" : "text-right p-r5"},
            { "data": "endDate", "bSortable": false, "width": "10%" , "class" : "text-right p-r5"},
            { "data": "state", "bSortable": false, "width": "8%" , "class" : "text-right p-r5"},
            { "data": null, "bSortable": false, "width": "12%", "class" : "text-right",
                 render: function ( data ) {
                    return '<button type="button" class="link m-r5" style="color: #00ce88;" value="'+data.campaignId+'" data-toggle="modal" data-target="#campaignDetails" onclick="get_campaign(this);">'+
                                '<div class="content"><i class="fa fa-pencil"></i> ' + $('#Edit').val() + '</div>'+
                            '</button> | '+
                            '<button type="button" class="link m-l5" style="color: #f74f63;" value="'+data.campaignId+'" onclick="archive_campaign(this);">'+
                                '<div class="content"><i class="fa fa-archive"></i> ' + $('#Archive').val() + '</div>'+
                            '</button>';
                }
            },  
        ],       
        "pagingType": "full_numbers"
    } );  

    // Groups
    $('#ad_groups').dataTable( {
        "language": table_language,
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "amazon/advertising/get_groups/" + $('#id_country').val(),
            "type": "POST"
        },
        "columns": [           
            { "data": "adGroupId", "bSortable": false, "width": "16%" },
            { "data": "name", "bSortable": false, "width": "13%" },
            { "data": "campaignId", "bSortable": false, "width": "15%"},
            { "data": "campaignName", "bSortable": false, "width": "15%"},           
            { "data": "defaultBid", "bSortable": false, "width": "10%", "class" : "text-right p-r5" },
            { "data": "state", "bSortable": false, "width": "10%" , "class" : "text-right p-r5"},
            { "data": null, "bSortable": false, "width": "15%", "class" : "text-right",
                 render: function ( data ) {
                    return '<button type="button" class="link m-r5" style="color: #00ce88;" value="'+data.adGroupId+'" data-toggle="modal" data-target="#groupDetails" onclick="get_group(this);">'+
                                '<div class="content"><i class="fa fa-pencil"></i> ' + $('#Edit').val() + '</div>'+
                            '</button> | '+
                            '<button type="button" class="link m-l5" style="color: #f74f63;" value="'+data.adGroupId+'" onclick="archive_group(this);">'+
                                '<div class="content"><i class="fa fa-archive"></i> ' + $('#Archive').val() + '</div>'+
                            '</button>';
                }
            },  
        ],       
        "pagingType": "full_numbers"
    } );     
   
    $('#ad_keywords').dataTable( {
        "language": table_language,
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "amazon/advertising/list_ads/" + $('#id_country').val() /*+ "/" + $('#sub_action').val()*/,
            "type": "POST"
        },
        "columns": [           
            { "data": null, "bSortable": false, "width": "30%", 
                 render: function ( data ) {
                    return ' <span class=""><b>'+ $('#AdGroups').val() + " </b></span>: "+data.adGroupName ;
                }
            }, 
            { "data": null, "bSortable": false, "width": "50%", 
                 render: function ( data ) {
                    return ' <span class=""><b>'+ $('#Campaign').val() + "</b></span> : "+data.campaignName ;
                }
            }, 
            { "data": null, "bSortable": false, "width": "20%", "class" : "text-right",
                 render: function ( data ) {
                    return '<button type="button" class="link m-r5 data_control" value="'+data.adGroupId+'"><div class="content"><i class="fa fa-list-alt"></i> ' + $('#Keywords').val() + '</div></button>';
                }
            },  
        ],       
        "pagingType": "full_numbers",
        "bFilter": false       
    } ); 
    
    var ad_keywords = $('#ad_keywords').DataTable();
    $('#ad_keywords tbody').on('click', '.data_control', function () {        
        var tr = $(this).closest('tr');
        var row = ad_keywords.row( tr );
        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');            
        } else {
            var d = row.data();
            var table_clone = keywordFormatDetails(d.adGroupId);            
            row.child( table_clone ).show();
            if($('#biddable_details_'+d.adGroupId).length > 0)
            {
                row_keywords_details(d.adGroupId, 'biddable');
            }
            if($('#negative_content_'+d.adGroupId).length > 0)
            {
                row_keywords_details(d.adGroupId, 'negative');
            }
            row.child().addClass('background-gray'); 
        }
    } );  

    $('#product_ad').dataTable( {
        "language": table_language,
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "amazon/advertising/list_ads/" + $('#id_country').val() ,
            "type": "POST"
        },
        "columns": [    
            { "data": null, "bSortable": false, "width": "30%", 
                 render: function ( data ) {
                    return ' <span class=""><b>'+ $('#AdGroups').val() + " </b></span>: "+data.adGroupName ;
                }
            }, 
            { "data": null, "bSortable": false, "width": "50%", 
                 render: function ( data ) {
                    return ' <span class=""><b>'+ $('#Campaign').val() + "</b></span> : "+data.campaignName ;
                }
            }, 
            { "data": null, "bSortable": false, "width": "20%", "class" : "text-right",
                 render: function ( data ) {
                    return '<button type="button" class="link m-r5 data_control" value="'+data.adGroupId+'"><div class="content"><i class="fa fa-list-alt"></i> ' + $('#productAds').val() + '</div></button>';
                }
            },  
            /*{ "data": "sku", "bSortable": false, "width": "16%" },
            { "data": "asin", "bSortable": false, "width": "13%" },
            { "data": "campaignId", "bSortable": false, "width": "15%"},
            { "data": "adGroupId", "bSortable": false, "width": "15%"},           
            { "data": "adId", "bSortable": false, "width": "10%", "class" : "text-right p-r5" },
            { "data": "state", "bSortable": false, "width": "10%", "class" : "text-right p-r5" },
            { "data": null, "bSortable": false, "width": "15%", "class" : "text-right",
                 render: function ( data ) {
                    return '';
                    return '<button type="button" class="link m-r5" style="color: #00ce88;" value="'+data.adGroupId+'" data-toggle="modal" data-target="#groupDetails" onclick="get_group(this);">'+
                                '<div class="content"><i class="fa fa-pencil"></i> ' + $('#Edit').val() + '</div>'+
                            '</button> | '+
                            '<button type="button" class="link m-l5" style="color: #f74f63;" value="'+data.adGroupId+'" onclick="archive_group(this);">'+
                                '<div class="content"><i class="fa fa-archive"></i> ' + $('#Archive').val() + '</div>'+
                            '</button>';
                }
            },*/
        ],       
        "pagingType": "full_numbers",
        "bFilter": false       
    } ); 
    
    var product_ad = $('#product_ad').DataTable();
    $('#product_ad tbody').on('click', '.data_control', function () {        
        var tr = $(this).closest('tr');
        var row = product_ad.row( tr );
        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');            
        } else {
            var d = row.data();
            
            var table_clone = adFormatDetails(d.adGroupId);            
            row.child( table_clone ).show();
            if($('#details_'+d.adGroupId).length > 0)
            {
                row_ads_details(d.adGroupId);
            }           
            row.child().addClass('background-gray'); 
        }
    } );  

    //showAddNew
    $('.showProfile').on('click', function(){
        var key ;        
        if($('#addNewId').length > 0){
            key = $('#addNewId').val();
        }
        if($(this).val()){
            key = $(this).val();
            console.log(key);
        }
        if(key){
            $('#'+key).modal();
        }
    });

    // get campaign from adGroup
    $('.select_adGroupId').on('change', function(){
        var loading_display = loading_icon();
        var temp = $(this).closest('.custom-form');
        var select_campaignId = $(temp).find('.select_campaignId');
        $(select_campaignId).hide();
        $(loading_display).insertAfter($(select_campaignId));
        var adGroup = $(this).find(":selected");
        var campaignId = $(adGroup).attr('data-campaign');
        var campaign =  get_campaign_data(campaignId);
        campaign.success(function(rdata){   
            if(rdata.data){
                var content_data = rdata.data;
                $(select_campaignId).show();
                $(select_campaignId).next().remove();
                $(select_campaignId).find('input[rel="campaignName"]').val(content_data.campaignId+" ("+content_data.name+")");
                $(select_campaignId).find('input[rel="campaignId"]').val(content_data.campaignId);
            }
        });
    });

    $('.addKeywordText').on('click', function(){
        var cloned = $(this).closest('.form-group').clone();
        $(cloned).find('input').val('');
        $(cloned).find('.addKeywordText').remove();
        $(cloned).find('.removeKeywordText').show().on('click', function(){
            $(this).closest('.form-group').remove();        
        });
        $(cloned).insertAfter($(this).closest('.form-group'));        
    });

    $('.tooltips').tooltipster({ 
        position:'right', 
        theme:'tooltipster-shadow', 
        contentAsHTML: true,
        animation: 'fade',
        touchDevices: true,
        interactive: true
    });
});

function adFormatDetails(table_id) {

    var sOut =  "<form id=\"form_"+table_id+"\" method=\"POST\">";    
    sOut += "<table id=\"details_"+table_id+"\" class=\"amazon-responsive-table hover m-t15\">" + $('#details').html() +
        "</table></form>";

    return "<div class=\"tabboder clearfix col-xs-11\">" + sOut + "</div>";
}

function row_ads_details(table_id) {
           
    var adGroup = {};
    adGroup.adGroupId = table_id;

    var options = { 
        "language": table_language,
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "amazon/advertising/get_product_ad/" + $('#id_country').val(),
            "type": "POST",
            "data": adGroup,
        },
        "columns": [           
            { "data": "adId", "bSortable": false, "width": "15%", "class" : "p-l5"},
            { "data": "adGroupId", "bSortable": false, "width": "15%" },
            { "data": "campaignId", "bSortable": false, "width": "15%" },
            { "data": "sku", "bSortable": false, "width": "15%" , "class" : ""},         
            { "data": "asin", "bSortable": false, "width": "15%" , "class" : ""},         
            { "data": "state", "bSortable": false, "width": "10%" , "class" : ""},         
            { "data": null, "bSortable": false, "width": "20%", "class" : "text-right p-r5",
                 render: function ( data ) {
                    return '<button type="button" class="link m-r5" style="color: #00ce88;" value="'+data.adId+'" data-toggle="modal" data-target="#keywordDetails" onclick="get_keyword(this);">'+
                                '<div class="content"><i class="fa fa-pencil"></i> ' + $('#Edit').val() + '</div>'+
                            '</button> | '+
                            '<button type="button" class="link m-l5" style="color: #f74f63;" value="'+data.adId+'" onclick="archive_keyword(this, \''+table_id+'\');">'+
                                '<div class="content"><i class="fa fa-archive"></i> ' + $('#Archive').val() + '</div>'+
                            '</button>';
                }
            }, 
        ],       
        "pagingType": "full_numbers",
        "bFilter": false,
    };
    
    var table_cloned = $("#details_" + table_id).on( 'processing.dt', function ( e, settings, processing ) {
        var processing_block = $(this).closest('.dataTables_wrapper').find('.dataTables_processing');
        processing_block.css( 'display', processing ? 'block' : 'none' );
        processing_block.html('');
        processing_block.append('<div class="feedbizloader"><div class="feedbizcolor"></div><p class="m-t10 text-info">'+$('#processing').val()+'</p></div>');
        if(processing_block.find('.feedbizloader')){
            processing_block.find('.feedbizloader').find('.feedbizcolor').sprite({fps: 10, no_of_frames: 30});
        }
    } ).dataTable(options);
}

function keywordFormatDetails(table_id) {

    var header = $('#keyword_header').clone();
    $(header).find('.biddable_content').attr('href', '#biddable_content_'+table_id);
    $(header).find('.negative_content').attr('href', '#negative_content_'+table_id);

    var sOut =  "<div class=\"tab-content\">";
    //biddable
    sOut += "<div id=\"biddable_content_"+table_id +"\" class=\"tab-pane active\">" +
        "<form id=\"form_biddable_"+table_id+"\" method=\"POST\">" + 
        "<table id=\"biddable_details_"+table_id+"\" class=\"amazon-responsive-table hover m-t15\">" + $('#biddable_details').html() +
        "</table></form></div>";
    //negative
    sOut += "<div id=\"negative_content_"+table_id +"\" class=\"tab-pane\">" + 
        "<form id=\"form_negative_"+table_id+"\" method=\"POST\">" + 
        "<table id=\"negative_details_"+table_id+"\" class=\"amazon-responsive-table hover m-t15\">" + $('#negative_details').html() +
        "</table></form></div>";
    sOut += "</div>";

    return "<div class=\"tabboder clearfix col-xs-11\">" + header.html() + sOut + "</div>";
}

function row_keywords_details( table_id, sub_action ) {
           
    var adGroup = {};
    adGroup.adGroupId = table_id;

    var Edit_btn = $('#Edit').val();
    if(sub_action == 'biddable'){
        Edit_btn = $('#Edit_Bid').val();
    }
    
    var options = { 
        "language": table_language,
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc']],
        "ajax": {
            "url": base_url + "amazon/advertising/get_keywords/" + $('#id_country').val() + "/" + sub_action,
            "type": "POST",
            "data": adGroup,
        },
        "columns": [           
            { "data": "keywordId", "bSortable": false, "width": "15%", "class" : "p-l5"},
            { "data": "keywordText", "bSortable": false, "width": "35%" },
            { "data": "matchType", "bSortable": false, "width": "10%" },
            { "data": "state", "bSortable": false, "width": "10%" , "class" : ""},         
            { "data": null, "bSortable": false, "width": "10%", 
                 render: function ( data ) {
                    var bid = '';
                    if(data.bid){
                        bid = data.bid;
                    }
                    return bid;
                }
            },       
            { "data": null, "bSortable": false, "width": "20%", "class" : "text-right p-r5",
                 render: function ( data ) {
                    return '<button type="button" class="link m-r5" style="color: #00ce88;" value="'+data.keywordId+'" data-toggle="modal" data-target="#keywordDetails" onclick="get_keyword(this, \''+sub_action+'\');">'+
                                '<div class="content"><i class="fa fa-pencil"></i> ' + Edit_btn + '</div>'+
                            '</button> | '+
                            '<button type="button" class="link m-l5" style="color: #f74f63;" value="'+data.keywordId+'" onclick="archive_keyword(this, \''+sub_action+'\', \''+table_id+'\');">'+
                                '<div class="content"><i class="fa fa-archive"></i> ' + $('#Archive').val() + '</div>'+
                            '</button>';
                }
            }, 
        ],       
        "pagingType": "full_numbers",
        "bFilter": false,
        "initComplete": function () {
            var api = this.api();            
            if(sub_action == 'negative') {
                var column4 = api.column(4);
                column4.visible( false );
            }
        }
    };
    
    var table_cloned = $("#"+sub_action+"_details_" + table_id).on( 'processing.dt', function ( e, settings, processing ) {
        var processing_block = $(this).closest('.dataTables_wrapper').find('.dataTables_processing');
        processing_block.css( 'display', processing ? 'block' : 'none' );
        processing_block.html('');
        processing_block.append('<div class="feedbizloader"><div class="feedbizcolor"></div><p class="m-t10 text-info">'+$('#processing').val()+'</p></div>');
        if(processing_block.find('.feedbizloader')){
            processing_block.find('.feedbizloader').find('.feedbizcolor').sprite({fps: 10, no_of_frames: 30});
        }
    } ).dataTable(options);
}

function get_keyword(e, sub_action){
    var loading_display = loading_icon();
    var button = $(e);    
    var cloned = $('#template_'+sub_action).clone();  
    $('#keywordDetail').html(loading_display);

    var keyword = {};
    keyword.keywordId = button.val();

    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/get_keyword/" + $('#id_country').val()+"/"+sub_action,
        data: keyword,
        dataType: 'json',
        success: function (rdata) {             
            if(rdata.data){
                $('#keywordDetail').html(cloned.html()).checkBo();
                var content_data = rdata.data;
                $.each($('#keywordDetail').find('div, input, textarea'), function(k, inp){

                    if(!$(inp).attr("rel"))
                        return;

                    var input_name = $(inp).attr("rel");

                    if(input_name == "bid"){
                        $(inp).focus();
                    }

                    if(content_data[input_name]){
                        
                        if($(inp).is(':radio')){

                             $(inp).each(function(i,j){
                                if($(j).val() === content_data[input_name]){
                                    $(j).prop('checked', true).change().trigger('change');
                                }
                            });

                        } else if($(inp).is('div')){
                            $(inp).html(content_data[input_name]);
                        } else {
                            $(inp).val(content_data[input_name]);
                        }
                    }

                });
            }
        },
        error: function (rerror) {
            console.log(rerror);
        }
    });
}

function save_keyword(e, sub_action){ 
    saving_btn(e); 
    var input = $('#keywordDetail').find('input, select');
    var id = $('#keywordDetail').find('input[rel="keywordId"]').val();
    var adGroupId = $('#keywordDetail').find('input[rel="adGroupId"]').val();

    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/save_keyword/" + $('#id_country').val() + "/" + sub_action,
        data: input.serialize(),
        dataType: 'json',
        success: function (rdata) { 
            unsaving_btn(e);
            if(rdata.code == "SUCCESS") {
                $('#keywordDetails').modal('hide');
                var table = $('#'+sub_action+'_details_'+adGroupId).DataTable();
                table.ajax.reload();
                notification_alert($('#success-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.keywordId + '". ', 'good');  
            } else {
                var detail = '';
                if(rdata.description){
                    detail = rdata.description ;
                }
                if(rdata.details){
                    detail = rdata.details ;
                }
                notification_alert(detail +' ' + $('#please-try').val() + '.', 'bad');
            }           
        },
        error: function (rerror) {
            unsaving_btn(e);
            notification_alert($('#unsuccess-message').val() + ', '+ $('#id_title').val() +' : "' + id + '". ' + $('#please-try').val() + '.', 'bad'); 
        }
    });
}

function archive_keyword(e, sub_action, adGroupId){

    var id = $(e).val();
    var keyword = {};
    keyword.keywordId = id;

    bootbox.confirm( $('#confirm-archive-keyword-message').val() + ", "+ $('#id_title').val() +" : '" + id + "'?", function(result) {        
        if(result){   
            archiving_btn(e);
            $.ajax({
                type: 'POST',
                url: base_url + "amazon/advertising/archive_keyword/" + $('#id_country').val() + "/" + sub_action,
                data: keyword,
                dataType: 'json',
                success: function (rdata) { 
                    if(rdata.code == "SUCCESS") {
                        var table = $('#'+sub_action+'_details_'+adGroupId).DataTable();
                        table.ajax.reload();
                        notification_alert($('#archive-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.keywordId + '". ', 'good');  
                    } else {
                        var detail = '';
                        if(rdata.description){
                            detail = rdata.description ;
                        }
                        if(rdata.details){
                            detail = rdata.details ;
                        }
                        notification_alert($('#unsuccess-message').val() + '. ' + detail +' ' + $('#please-try').val() + '.', 'bad');
                    }
                   
                },
                error: function (rerror) {
                    notification_alert($('#unsuccess-message').val() + ', '+ $('#id_title').val() +' : "' + id + '". ' + $('#please-try').val() + '.', 'bad'); 
                }
            });
        }
    });
}

function create_keyword(e, sub_action){ 
    saving_btn(e); 
    var input = $('#'+sub_action).find('input, select');
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/create_keyword/" + $('#id_country').val() + "/" + sub_action,
        data: input.serialize(),
        dataType: 'json',
        success: function (rdata) { 
            unsaving_btn(e);
            if(rdata.code == "SUCCESS") {
                $('#'+sub_action).modal('hide');
                var table = $('#ad_keywords').DataTable();
                table.ajax.reload();
                notification_alert($('#success-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.keywordId + '". ', 'good');  
            } else {
                var detail = '';
                if(rdata.details){
                    detail = rdata.details ;
                } 
                if(rdata.description){
                    detail = rdata.description ;
                } 
                notification_alert($('#unsuccess-message').val() + '. ' + detail +' ' + $('#please-try').val() + '.', 'bad');
            }
           
        },
        error: function (rerror) {
            unsaving_btn(e);
            notification_alert($('#unsuccess-message').val() + '. ' + $('#please-try').val() + '.', 'bad'); 
        }
    });
}

function archive_group(e){

    var id = $(e).val();
    var adGroup = {};
    adGroup.adGroupId = id;

    bootbox.confirm( $('#confirm-archive-group-message').val() + ", "+ $('#id_title').val() +" : '" + id + "'?", function(result) {        
        if(result){   
            archiving_btn(e);
            $.ajax({
                type: 'POST',
                url: base_url + "amazon/advertising/archive_group/" + $('#id_country').val(),
                data: adGroup,
                dataType: 'json',
                success: function (rdata) { 
                    if(rdata.code == "SUCCESS") {
                        var table = $('#ad_groups').DataTable();
                        table.ajax.reload();
                        notification_alert($('#archive-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.adGroupId + '". ', 'good');  
                    } else {
                        var detail = '';
                        if(rdata.description){
                            detail = rdata.description ;
                        }
                        if(rdata.details){
                            detail = rdata.details ;
                        }
                        notification_alert($('#unsuccess-message').val() + '. ' + detail +' ' + $('#please-try').val() + '.', 'bad');
                    }
                   
                },
                error: function (rerror) {
                    notification_alert($('#unsuccess-message').val() + ', '+ $('#id_title').val() +' : "' + id + '". ' + $('#please-try').val() + '.', 'bad'); 
                }
            });
        }
    });
}

function get_group(e){
    var loading_display = loading_icon();
    var button = $(e);    
    var cloned = $('#template').clone();  
    $('#groupDetail').html(loading_display);

    var adGroup = {};
    adGroup.adGroupId = button.val();

    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/get_group/" + $('#id_country').val(),
        data: adGroup,
        dataType: 'json',
        success: function (rdata) {             
            if(rdata.data){
                $('#groupDetail').html(cloned.html()).checkBo();
                var content_data = rdata.data;
                $.each($('#groupDetail').find('div, input'), function(k, inp){

                    if(!$(inp).attr("rel"))
                        return;

                    var input_name = $(inp).attr("rel");
                    if(content_data[input_name]){
                        
                        if($(inp).is(':radio')){

                             $(inp).each(function(i,j){
                                if($(j).val() === content_data[input_name]){
                                    $(j).prop('checked', true).change().trigger('change');
                                }
                            });

                        } else if($(inp).is('div')){
                            $(inp).html(content_data[input_name]);
                        } else {
                            $(inp).val(content_data[input_name]);
                        }
                    }

                });
            }
        },
        error: function (rerror) {
            console.log(rerror);
        }
    });
}

function save_group(e){ 
    saving_btn(e); 
    var input = $('#groupDetail').find('input, select');
    var id = $('#groupDetail').find('input[rel="adGroupId"]').val();

    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/save_group/" + $('#id_country').val(),
        data: input.serialize(),
        dataType: 'json',
        success: function (rdata) { 
            unsaving_btn(e);
            if(rdata.code == "SUCCESS") {
                $('#groupDetails').modal('hide');
                var table = $('#ad_groups').DataTable();
                table.ajax.reload();
                notification_alert($('#success-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.adGroupId + '". ', 'good');  
            } else {
                var detail = '';
                if(rdata.description){
                    detail = rdata.description ;
                }
                if(rdata.details){
                    detail = rdata.details ;
                }
                notification_alert(detail +' ' + $('#please-try').val() + '.', 'bad');
            }           
        },
        error: function (rerror) {
            unsaving_btn(e);
            notification_alert($('#unsuccess-message').val() + ', '+ $('#id_title').val() +' : "' + id + '". ' + $('#please-try').val() + '.', 'bad'); 
        }
    });
}

function create_group(e){ 
    saving_btn(e); 
    var input = $('#adGroupCreation').find('input, select');
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/create_group/" + $('#id_country').val(),
        data: input.serialize(),
        dataType: 'json',
        success: function (rdata) { 
            unsaving_btn(e);
            if(rdata.code == "SUCCESS") {
                $('#adGroupCreation').modal('hide');
                var table = $('#ad_groups').DataTable();
                table.ajax.reload();
                notification_alert($('#success-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.adGroupId + '". ', 'good');  
            } else {
                var detail = '';
                if(rdata.details){
                    detail = rdata.details ;
                } 
                if(rdata.description){
                    detail = rdata.description ;
                } 
                notification_alert($('#unsuccess-message').val() + '. ' + detail +' ' + $('#please-try').val() + '.', 'bad');
            }
           
        },
        error: function (rerror) {
            unsaving_btn(e);
            notification_alert($('#unsuccess-message').val() + '. ' + $('#please-try').val() + '.', 'bad'); 
        }
    });
}

function archive_campaign(e){
    var id = $(e).val();
    var campaign = {};
    campaign.campaignId = id;
    bootbox.confirm( $('#confirm-archive-campaign-message').val() + ", "+ $('#id_title').val() +" : '" + id + "'?", function(result) {
        if(result){   
            archiving_btn(e);
            $.ajax({
                type: 'POST',
                url: base_url + "amazon/advertising/archive_campaign/" + $('#id_country').val(),
                data: campaign,
                dataType: 'json',
                success: function (rdata) {                    
                    if(rdata.code == "SUCCESS") {
                        var table = $('#ad_campaigns').DataTable();
                        table.ajax.reload();
                        notification_alert($('#archive-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.campaignId + '". ', 'good');  
                    } else {
                        var detail = '';
                        if(rdata.details){
                            detail = rdata.details ;
                        } 
                        if(rdata.description){
                            detail = rdata.description ;
                        } 
                        notification_alert($('#unsuccess-message').val() + '. ' + detail +' ' + $('#please-try').val() + '.', 'bad');
                    }
                   
                },
                error: function (rerror) {                   
                    notification_alert($('#unsuccess-message').val() + ', '+ $('#id_title').val() +' : "' + id + '". ' + $('#please-try').val() + '.', 'bad'); 
                }
            });
        }
    });
}

function get_campaign(e) {
    var loading_display = loading_icon();
    var button = $(e);    
    var cloned = $('#template').clone();  
    $('#campaignDetail').html(loading_display);

    var campaign =  get_campaign_data(button.val());
    campaign.success(function(rdata){ 
        $('#campaignDetail').html(cloned.html()).checkBo();
        if(rdata.data){
            var content_data = rdata.data;
            $.each($('#campaignDetail').find('div, input'), function(k, inp){

                if(!$(inp).attr("rel"))
                    return;

                var input_name = $(inp).attr("rel");

                if(content_data[input_name]){

                    if($(inp).is(':radio')){

                         $(inp).each(function(i,j){
                            if($(j).val() === content_data[input_name]){
                                $(j).prop('checked', true).change().trigger('change');
                            }
                        });

                    } else if($(inp).is('div')){
                        $(inp).html(content_data[input_name]);
                    } else {
                        $(inp).val(content_data[input_name]);
                    }
                }

            });
        }
    });
    campaign.error(function (rerror){
        console.log(rerror);
    });
}

function get_campaign_data(campaignId) {    
    var campaign = {};
    campaign.campaignId = campaignId;
    return $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/get_campaign/" + $('#id_country').val(),
        data: campaign,
        dataType: 'json',        
    });
}

function save_campaign(e){  
    saving_btn(e); 
    var input = $('#campaignDetail').find('input');
    var id = $('#campaignDetail').find('input[rel="campaignId"]').val();
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/save_campaign/" + $('#id_country').val(),
        data: input.serialize(),
        dataType: 'json',
        success: function (rdata) { 
            unsaving_btn(e);
            if(rdata.code == "SUCCESS") {
                $('#campaignDetails').modal('hide');
                var table = $('#ad_campaigns').DataTable();
                table.ajax.reload();
                notification_alert($('#success-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.campaignId + '". ', 'good');  
            } else {
                var detail = '';
                if(rdata.details){
                    detail = rdata.details ;
                } 
                if(rdata.description){
                    detail = rdata.description ;
                }  
                notification_alert(detail +' ' + $('#please-try').val() + '.', 'bad');
            }
           
        },
        error: function (rerror) {
            unsaving_btn(e);
            notification_alert($('#unsuccess-message').val() + ', '+ $('#id_title').val() +' : "' + id + '". ' + $('#please-try').val() + '.', 'bad'); 
        }
    });
}

function create_campaign(e){   
    saving_btn(e);
    var input = $('#campaignCreation').find('input, select');
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/create_campaign/" + $('#id_country').val(),
        data: input.serialize(),
        dataType: 'json',
        success: function (rdata) { 
            unsaving_btn(e);
            if(rdata.code == "SUCCESS") {
                $('#campaignCreation').modal('hide');
                var table = $('#ad_campaigns').DataTable();
                table.ajax.reload();
                notification_alert($('#success-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.campaignId + '". ', 'good');  
            } else {
                var detail = '';
                if(rdata.details){
                    detail = rdata.details ;
                } 
                if(rdata.description){
                    detail = rdata.description ;
                } 
                notification_alert(detail +' ' + $('#please-try').val() + '.', 'bad');
            }
           
        },
        error: function (rerror) {
            unsaving_btn(e);
            notification_alert($('#unsuccess-message').val() + '. ' + $('#please-try').val() + '.', 'bad'); 
        }
    });
}

function get_profile(e) {
    var loading_display = loading_icon();
    var button = $(e);    
    var cloned = $('#template').clone();  
    $('#profileDetail').html(loading_display);

    var profile = {};
    profile.profileId = button.val();

    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/get_profile/" + $('#id_country').val(),
        data: profile,
        dataType: 'json',
        success: function (rdata) {
            $('#profileDetail').html(cloned.html()); 
            if(rdata.data){
                var content_data = rdata.data;
                $.each($('#profileDetail').find('div, input'), function(k, inp){

                    if(!$(inp).attr("rel"))
                        return;

                    var input_name = $(inp).attr("rel");
                    if(content_data[input_name]){
                        if($(inp).is('div')){
                            $(inp).html(content_data[input_name]);
                        } else {
                            $(inp).val(content_data[input_name]);
                        }
                    }

                });
            }
        },
        error: function (rerror) {
            console.log(rerror);
        }
    });
}

function save_profile(e){   
    saving_btn(e);
    var input = $('#profileDetail').find('input');
    var id = $('#profileDetail').find('input[rel="profileId"]').val();
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/advertising/save_profile/" + $('#id_country').val(),
        data: input.serialize(),
        dataType: 'json',
        success: function (rdata) { 
            unsaving_btn(e);
            if(rdata.code == "SUCCESS") {
                $('#profileDetails').modal('hide');
                var table = $('#ad_profile').DataTable();
                table.ajax.reload();
                notification_alert($('#success-message').val() + ', '+ $('#id_title').val() +' : "' + rdata.profileId + '". ', 'good');  
            } else {
                var detail = '';
                if(rdata.details){
                    detail = rdata.details ;
                } 
                if(rdata.description){
                    detail = rdata.description ;
                }  
                notification_alert(detail +' ' + $('#please-try').val() + '.', 'bad');
            }
           
        },
        error: function (rerror) {
            unsaving_btn(e);
            notification_alert($('#unsuccess-message').val() + ', '+ $('#id_title').val() +' : "' + id + '". ' + $('#please-try').val() + '.', 'bad'); 
        }
    });
}

function loading_icon(){
    return '<div class="row text-center link"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><span class="sr-only"> '+$('#loadingRecords').val()+'</span></div>';
}
function archiving_btn(e){
    $(e).hide();
    $(e).parent().append('<span class="fa-stack fa-lg"><i class="fa fa-spinner fa-pulse fa-fw"></i></span> <span class=""></span>');
}
function saving_btn(e){
    $(e).hide();
    $(e).parent().append('<div class="row"><button class="btn btn-save pull-right m-r25 p-tb0" disabled="disabled"><span class="fa-stack fa-lg"><i class="fa fa-spinner fa-pulse fa-fw"></i></span> <span class=""> '+$('#processing').val()+'</span></button></div>');
}
function unsaving_btn(e){
    console.log(e);
    $(e).show();
    $(e).next().remove();
}

if($('#amazon-root').length > 0) {
    window.onAmazonLoginReady = function() {
        if($('#ad_client_id').length > 0) {
            amazon.Login.setClientId($('#ad_client_id').val());
        }
    };
    (function(d) {
        var a = d.createElement('script'); a.type = 'text/javascript';
        a.async = true; a.id = 'amazon-login-sdk';
        a.src = 'https://api-cdn.amazon.com/sdk/login1.js';
        d.getElementById('amazon-root').appendChild(a);
    })(document);
    document.getElementById('LoginWithAmazon').onclick = function() {
        options = { scope : 'cpc_advertising:campaign_management', response_type : 'code' };
        amazon.Login.authorize(options, base_url+'amazon_handle/login/'+$('#id_country').val());
        return false;
    };
    document.getElementById('Logout').onclick = function() {
        var profile_profileId = $('#profile_profileId').text();
        bootbox.confirm( $('#confirm-logout').val() + " : " +  profile_profileId + "?", function(result) {
            if(result){   
                amazon.Login.logout();
                $.ajax({
                    type: 'GET',
                    url: base_url + "amazon/advertising/remove_authentication/" + $('#id_country').val(),
                    dataType: 'json',
                    success: function (rdata) { 
                        if(rdata) {
                            window.location.href = base_url + "amazon/advertising/authentication/" + $('#id_country').val();
                        } else {
                            notification_alert($('#unsuccess-message').val() + '. ' + $('#please-try').val() + '.', 'bad');
                        }
                    },
                    error: function (rerror) {
                        notification_alert($('#unsuccess-message').val() + '. ' + $('#please-try').val() + '.', 'bad'); 
                    }
                });
            }
        });    

    };
}