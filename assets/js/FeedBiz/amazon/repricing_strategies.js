var validation = false;

$(document).ready(function(){

    //Add
    $('#add').click(function(){
        add_strategy();
    });        
    
    $('#form-repricing-strategy').validate({   
        submitHandler: function(form) {
            bootbox.confirm($('#submit-confirm').val(), function(result) {
                if(result) {
                    form.submit();
                }                    
            });
        }
    });

    $('.edit_strategy').unbind('click').on("click",function(){
        edit($(this));
    });

    $('.remove_strategy').unbind('click').on("click",function(){
        deleted($(this));
    });

    changeName();
    
});
    
function add_strategy()
{
    var cloned = $('#main').clone().show();
    $('#tasks').prepend(cloned);

    cloned.attr('id', '');       
    cloned.removeClass('showSelectBlock'); 

    var id_count = parseInt($('#count').text());
    var id_count_new = id_count+1;
    $('#count').html(id_count_new);

    cloned.find('.remove').on('click', function(){
        $('#count').html(parseInt($('#count').text()) - 1 );
        $(this).closest('div.amazon-strategy').remove(); 
    });

    cloned.find('input[rel=name]').focus();

    //set select chosen
    cloned.find('select').addClass('search-select').chosen();

}

//Change name
function changeName(){
    
    $('#tasks').on('blur', 'input[rel="rs_name"]', function() {

        var content =  $(this).closest('div.amazon-strategy');
        var content_group = $(this).closest('div.form-group');
        var duplicate = 0;
        var name = $.trim($(this).val()) ;

        if(name) {

            if(content_group.hasClass('has-error')) {

               content_group.removeClass('has-error');
               content_group.find('.error').hide();

            }

            name = name.replace(/[^a-zA-Z0-9\s_]/g,'').replace(/\s/g,'_');

            if($('input[name="strategy[' + name + '][rs_name]"]').length !== 0)  {
                name = name + '_' + duplicate;
                duplicate++;    
            } 

            content.find('label').removeClass('disabled');
            content.find('select, input, div, span').each(function()  {

                if ($(this).hasClass('disabled-none')){
                    $(this).removeClass('disabled');
                }

                if ( ! $(this).attr('rel') )
                    return ;
                
                if($(this).is('select, input')) {
                    $(this).removeAttr('disabled');
                    $(this).closest('.form-group').removeClass('disabled');
                }

                if($(this).is('select')) {
                    $(this).parent().find('.chosen-single div').remove();
                    $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                    $(this).chosen().trigger("chosen:updated");
                }
                
                var field_name = $(this).attr('rel') ;                
                
                var variable_prefix = 'strategy[' + name + '][' + field_name + ']' ;
                var id_prefix = 'strategy_' + name + '_' + field_name ;


                $(this).attr('name', variable_prefix) ;
                $(this).attr('id', id_prefix) ;
                $(this).attr('data-content', name) ;                   
            }); 
            
        } else  {

            if(!content_group.hasClass('has-error')) {
               content_group.addClass('has-error');
               content_group.find('.error').show();
            }

            content.find('select, input, div, span').each(function() {

                if($(this).is('select, input') && !$(this).is('input[rel="rs_name"]') ) {
                    $(this).attr('disabled', true);

                    if($(this).is('select')) {
                        $(this).parent().find('.chosen-single div').remove();
                        $(this).parent().find('.chosen-single').append('<div class="button-select"><b class="fa fa-angle-down"></b></div>');
                        $(this).chosen().trigger("chosen:updated");
                    }
                }
            });  
        }        

    });
}

// Edit
function edit(obj){

    console.log($(obj).closest('div.amazon-strategy'));
    if($(obj).closest('div.amazon-strategies').find('div.amazon-strategy').hasClass('showSelectBlock')) {
        $(obj).closest('div.amazon-strategies').find('div.amazon-strategy').removeClass('showSelectBlock');
    } else {
        $(obj).closest('div.amazon-strategies').find('div.amazon-strategy').addClass('showSelectBlock');
    }

}

//Remove
function deleted(obj){

    var name = $(obj).attr('rel');

    bootbox.confirm( $('#confirm-delete-message').val() + ", " + name + "?", function(result) {

        if(result){

            var id_strategy = $(obj).val();
            var id_shop = $('#id_shop').val();
            var id_country = $('#id_country').val();

            $.ajax({
                type: "GET",
                url:  base_url + "/amazon/delete_repricing_strategies/" + id_strategy + "/" + id_shop + "/" + id_country,
                dataType:'json',
                success: function(rdata) 
                {
                    if(rdata.pass === true)
                    {
                        notification_alert($('#success-message').val() + ' ' + $('#delete-success-message').val() + ', "' + name + '".', 'good'); 
        
                        var id = parseInt($('#count').text()) - 1 ;
                        $('#count').html(id);
                        $(obj).closest('div.amazon-strategies').remove();
                       
                    }
                    else
                    {
                        notification_alert($('#error-message').val() + ' ' + $('#delete-unsuccess-message').val() + ', "' + name + '".', 'bad');                        
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) 
                {
                    notification_alert($('#error-message').val() + ' ' + textStatus + ' ' + errorThrown + '.', 'bad');                    
                }
            });
        }
    });
}