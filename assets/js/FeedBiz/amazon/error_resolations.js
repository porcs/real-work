var table_offer, option ;
var selected = [];
var detailsTableHtml;
var offer_process = false;

$(document).ready(function() {
	
	//acheckBox()
	updateAcheckBox();

    detailsTableHtml = $("#error_details").html();

    /* Offers */
    $('#error_resolutions').dataTable({       
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "asStripeClasses": [ 'b-Top' ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "amazon/error_data/" + $('#error_type').val() + '/' + $('#id_country').val() + '/' + $('#message_type').val(),
            "type": "POST"
        },
        "columns": [         	     
           	{ "data": null, "bSortable": false, "width": "5%", "className": 'text-center',
                render: function () {
                    return '<a class="data_control plus">+</a>';
                }
            },
           	{ "data": "code", "bSortable": true, "width": "10%", "className": 'p-l10'  },
            { "data": "description", "bSortable": true, "width": "70%", "className": 'p-l10' },
            { "data": "rows", "bSortable": true, "width": "15%", "className": 'text-right p-r10' },           
        ],       
        "createdRow": function( row, data, dataIndex ) {
            if (data.important) {
                $(row).addClass('message_important');
            }
        },
        "pageLength": 10,
        "bFilter" : true,
        "paging": true,
        "pagingType": "full_numbers" ,  
        "order": [[1, 'asc']],   
        "drawCallback": function() {

            offer_process = true;
            var api = this.api();
            var search = api.search();
            
            $.each( api.rows().data(), function ( i,j ) {

                // If Search By SKU
                if(j.sku){
                    var tr = api.row(i).node();
                    var row = api.table().row( tr );

                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        $(tr).removeClass('shown');                        
                        $(tr).find('.data_control').removeClass('minus').addClass('plus').html('+');

                    } else {
                        var d = row.data();                       
                        var iTableCounter = 'amazon_error_resolutions_' + d.code;
                        var table_clone = fnFormatDetails(iTableCounter, detailsTableHtml);            
                        
                        // Open this row
                        row.child( table_clone ).show();

                        if($('#'+iTableCounter).length > 0)
                        {
                            row_details(d, $('#message_type').val(), search);
                        }

                        row.child().find('select').addClass('search-select');
                        row.child().addClass('background-gray');    

                        $(".search-select").chosen({
                            disable_search_threshold: 1,
                            no_results_text: $('#l_Nothing_found').val()
                        }).trigger("chosen:updated");   

                        $('.chosen-single').find('div').addClass('button-select');
                        $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');                        
                        $(".dataTables_wrapper .chosen-search").remove();

                        $(tr).addClass('shown');            
                        $(tr).find('.data_control').removeClass('plus').addClass('minus').html('-');
                    }
                }
            });    
        }  	
    } ); 

    $('#error_resolutions_filter').hide();
    table_offer = $('#error_resolutions').DataTable();

    $('#search_messgae_code_resolutions').on('keyup', function () {       
        table_offer.search( this.value ).draw();
    } );

    $('#search_sku_resolutions').on('change', function () {       
        table_offer.search( 'sku=' + this.value ).draw();

    } );

	$('#error_resolutions tbody').on('click', '.data_control', function () {		
        var tr = $(this).closest('tr');
        var row = table_offer.row( tr );
        //console.log(row);
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');            
            $(this).removeClass('minus').addClass('plus').html('+');

        } else {

            var d = row.data();

            console.log(d);
            //row_details(row.data(), $('#message_type').val(), tr, row, $(this));
            var iTableCounter = 'amazon_error_resolutions_' + d.code;
            var table_clone = fnFormatDetails(iTableCounter, detailsTableHtml);            
            
            // Open this row
            row.child( table_clone ).show();

            //td.css('background-color','white');
            if($('#'+iTableCounter).length > 0)
            {
                row_details(d, $('#message_type').val());
            }
			
			/* set */
			row.child().find('select').addClass('search-select');
			row.child().addClass('background-gray');	

			$(".search-select").chosen({
				disable_search_threshold: 1,
				no_results_text: $('#l_Nothing_found').val()
			}).trigger("chosen:updated");	

			$('.chosen-single').find('div').addClass('button-select');
			$('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
			
			$(".dataTables_wrapper .chosen-search").remove();

            tr.addClass('shown');            
            $(this).removeClass('plus').addClass('minus').html('-');
			
			row.child().find('.p-l10 .text-center').addClass('checkboSettings');
			
        }
    } );

});

function successStatus(e){

    if(e.closest('.upload-template').find('label.file_upload').length > 0){
        e.closest('.upload-template').find('label.file_upload').attr('disabled', false);
    }

    e.attr('disabled', false);
    e.closest('div').find('.status').find('i').removeClass('fa-spinner fa-spin').addClass('fa-check');
    e.closest('div').find('.status').find('p').find('span').html($('#l_Success').val());
}

function addStatus(e){

    if(e.closest('.upload-template').find('label.file_upload').length > 0){
        e.closest('.upload-template').find('label.file_upload').attr('disabled', true);
    }

    e.attr('disabled', true);
    e.addClass('update');
    e.closest('div').addClass('form-group has-update');
    e.closest('div').find('.status').addClass('update').show();
    e.closest('div').find('.error').hide();

    if(e.closest('div').find('.status').find('i').hasClass('fa-check')){
        e.closest('div').find('.status').find('i').addClass('fa-spinner fa-spin').removeClass('fa-check');
        e.closest('div').find('.status').find('p').find('span').html($('#l_Submit').val());
    }

    if(e.closest('div').find('.status').find('i').hasClass('fa-check')){
        e.closest('div').find('.status').find('i').addClass('fa-spinner fa-spin').removeClass('fa-check');
        e.closest('div').find('.status').find('p').find('span').html($('#l_Submit').val());
    }
}

function addError(e, r){    
    var rdisplay = e.closest('div').find('.error').show();
    e.closest('div').addClass('form-group has-error');
    e.closest('div').find('.status').hide();    
    rdisplay.find('span').html(r);    
}

function upload_file( e, d, type ){

    var form = new FormData(e.closest('form'));
    //append files
    var file = $(e).get(0).files[0];
    if (file) {   
        form.append('file-upload', file);
    }

    addStatus(e);


    //call ajax 
    $.ajax({
        url: base_url + "/amazon/error_data_upload_template/" + $('#id_country').val() + "/" + d.code + "/" + type,
        type: 'POST',
        data: form,             
        cache: false,
        contentType: false, //must, tell jQuery not to process the data
        processData: false, //must, tell jQuery not to set contentType
        success: function(d) {

            successStatus(e);

            var data = jQuery.parseJSON(d);            
            if(data.success){
                var sku_success = data.success;
                $.each(sku_success, function(i,j){
                    //console.log($(e).closest('table').find('input[value="'+i+'"]'));
                    $(e).closest('table').find('input[value="'+i+'"]').prop('checked', false);
                    $(e).closest('table').find('input[value="'+i+'"]').closest('tr').fadeOut(1000);
                });
            }  

            if(data.error){

                var sku_error = data.error;
                $.each(sku_error, function(i,j){
                    console.log($(e).closest('table').find('input[value="'+i+'"]'));
                    $(e).closest('table').find('input[value="'+i+'"]').prop('checked', false);
                    $(e).closest('table').find('input[value="'+i+'"]').closest('tr').addClass('error_details_item');
                });

                addError(e, $('#l_Error').val());                
            }
        },
        complete: function(XMLHttpRequest) {
            var data = XMLHttpRequest.responseText;
            if(data.success){
                var sku_success = data.success;
                $.each(sku_success, function(i,j){

                    console.log($(e).closest('table').find('input[value="'+j+'"]'));
                    $(e).closest('table').find('input[value="'+j+'"]').prop('checked', false);
                    $(e).closest('table').find('input[value="'+j+'"]').closest('tr').fadeOut(1000);
                });
            }  
        },
        error: function(d,e) {
            alert($('#l_Error').val() + ', ' + d + ', ' + e);
        }
    }); 
}

function row_details( d, type, search ) {
    
    var sku = '';   

    if(search){
        sku = '/' + search.replace("sku=", "");
    }
    
    var options = { "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "asStripeClasses": [ '' ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "amazon/error_resolution_details/" + $('#id_country').val() + "/" + d.code + "/" + type + "/" + d.show_message + sku,
            "type": "POST"
        },
        "columns": [ 
                 
            { "data": null, "bSortable": false, "width": "5%", "className": 'p-l10 text-center checkboSettings',
                render: function (data) {
                    /*if(data.result_code == "Resolved"){
                        return '<i class="fa fa-check"></i>';  
                        //return '<label class="amazon-checkbox disabled"><span class="amazon-inner"><i><input type="checkbox" disabled="disabled"/></i></span></label>'; 
                    } else {*/
                        return '<label class="amazon-checkbox"><span class="amazon-inner"><i><input type="checkbox" value="'+data.sku+'" name="sku['+data.key+']" /></i></span></label> ';                     
                    //}
                }
            },
            { "data": null, "bSortable": true, "width": "20%", "className": 'p-l10' ,
                render: function (data) {
                    if(data.sku != null) {
                        return data.sku;                     
                    } else {
                        return '';
                    }
                }
            },
            { "data": null, "bSortable": false, "width": "45%", "className": 'text-left',
                render: function (data) {
                    var amz_msg = '';
                    if(data.override){
                        amz_msg = '<p class="true-green"><b><i class="fa fa-check"></i> '+$('#l_edited_data').val()+'</b></p>';
                        $.each(data.override, function(override_key, override_data){
                            amz_msg += '<div><b>'+override_key+'</b> : '+override_data+'</div>';
                        });
                    } else if(data.message){
                        amz_msg = data.message;
                    } else {
                        if(data.standard_id) { 
                            amz_msg = data.standard_id;
                        } 
                    }
                    return '<div class="tb_amz_message">'+amz_msg+'</div>';                    
                }
            },
            { "data": "price", "bSortable": false, "width": "10%", "className": 'p-l10'  },
            { "data": "quantity", "bSortable": false, "width": "5%", "className": 'p-l10' },
            { "data": null, "bSortable": false, "width": "15%", "className": 'text-center asin p-r10' , "style":"display:none", 
                render: function (data) {
                    if(data.asin){
                        table_cloned.find('.asin').show();
                        table_cloned.find('.asin').show();

                        if(data.link) {

                            $('.tooltips').tooltipster({ 
                                position:'right', 
                                theme:'tooltipster-shadow', 
                                contentAsHTML: true,
                                animation: 'fade',
                                touchDevices: true,
                                interactive: true
                            });

                            if(data.override_asin){
                                return '<p style="color: #00CE88"><i class="fa fa-check"></i> '+'' + data.asin + '</p>'; 
                            } else {
                                return '<a href="'+data.link+'" target="_blank" class="" title="'+$('#l_view').val()+'" style="color: red"><i class="fa"></i> ' + data.asin + '</a>' +
                                '<input type="hidden" name="asin['+data.key+']" value="'+data.asin+'"/>'; 
                            }

                        } else {
                            return data.asin; 
                        }
                    } else {
                        table_cloned.find('.asin').css('display','none');
                        table_cloned.find('.asin').css('display','none');
                    }  
                    return null;                 
                }
            },
        ],      
        "bFilter" : false,
        "bInfo" : false,
        "paginate": true,
        "pagingType": "simple_numbers" ,   
        "order": [[1, 'asc']],
		"initComplete": function(){
		    updateAcheckBox();
            /*if(d.important){
                //$("#amazon_error_resolutions_" + d.code).closest('tr').find('.error_details_wrapper.tabboder').removeClass('tabboder');
                //$("#amazon_error_resolutions_" + d.code).closest('tr').find('.error_details_wrapper.tabboder').addClass('message_important');
            }*/
        }};

    var table_cloned = $("#amazon_error_resolutions_" + d.code).on( 'processing.dt', function ( e, settings, processing ) {

        var processing_block = $(this).closest('.dataTables_wrapper').find('.dataTables_processing');
        processing_block.css( 'display', processing ? 'block' : 'none' );
        processing_block.html('');
        processing_block.append('<div class="feedbizloader"><div class="feedbizcolor"></div><p class="m-t10 text-info">'+$('#processing').val()+'</p></div>');
        if(processing_block.find('.feedbizloader')){
            processing_block.find('.feedbizloader').find('.feedbizcolor').sprite({fps: 10, no_of_frames: 30});
        }
    } ).dataTable(options);

    var cloned = table_cloned.closest('td');

    //show message
    if(d.show_message == 1){  
        cloned.find('.msg').html($('#l_Message').val());  
    }

    //creation
    if(d.creation){ 
        cloned.find('.creation_error').closest('div').show();
    }

    // override ean
    if(d.override){
        cloned.find('.override_error').closest('div').show();
        cloned.find('.override_error').on('click', function(){
            edit_error($(this), d, type, d.override);
        });
    }

    //edit_attribute
    if(d.edit_attribute){ 

        cloned.find('.confirm_asin').closest('div').show();
        cloned.find('.confirm_asin').on('click', function(){
            confirm_asin($(this), d, type)
        });

        cloned.find('.edit_error').closest('div').show();
        cloned.find('.edit_error').on('click', function(){
            edit_error($(this), d, type);
        });

        cloned.find('.download_template').closest('div').show();
        cloned.find('.download_template').on('click', function(){
            download_template(d, type);
        });
        
        cloned.find('.upload-template-div').show();
        cloned.find('.file_upload').closest('div').find('label').attr('for', 'file-upload-' + d.code);
        cloned.find('.file_upload').closest('div').find('input[type="file"]').attr('id', 'file-upload-' + d.code);
    }    

    //recreate
    if(d.recreate){ 
        cloned.find('.recreate_error').closest('div').show();
        cloned.find('.recreate_error').on('click', function(){
            solved_error($(this), d, type, $(this).attr('rel') );
        });
    }  

    //solved
    if(d.solved){ 
        cloned.find('.accept_error').closest('div').show();
        cloned.find('.accept_error').on('click', function(){
            solved_error($(this), d, type );
        });
    }    

    if(d.resolutions && d.resolutions != ''){   
        cloned.find('.solutions').append(d.resolutions).parent().show();
    } 

    cloned.find('.file-upload').on( 'change', function () {
        upload_file($(this), d, type);
    });

 	$(cloned).on('click', 'tbody>tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

		acheckBox($(this).find('input').closest('.amazon-checkbox'));
		updateAcheckBox();
        
        $(this).toggleClass('selected');
		
    } );

    //updateAcheckBox();
 	check_all(cloned.find('.check-all'));
}

function check_all(d){	

    $(d).on('click', function () {

    	var th = $(this);
    	$.each($(th).closest('table').find('input[type="checkbox"]:not(:first)'), function(){

    		$acb_checkbox    = $(this).closest('.amazon-checkbox');    		
    		if ($(th).closest('.amazon-checkbox').hasClass('checked')) {    			
    			$acb_checkbox.removeClass('checked');
    			$(this).closest('tr').removeClass('selected');
    		} else {
    			$acb_checkbox.addClass('checked');
    			$(this).closest('tr').addClass('selected');    			
    		}
    		$acb_checkbox.find('input').prop('checked',  $(th).prop('checked')).change();
    			
    	});
	
    });	
}

/*function creation_error(e, action){

    var tabled = $(e).closest('td').find('#form_amazon_error_' + action + '_8541');
    var val = tabled.serialize();    
    var url = base_url + 'amazon/error_edit_data/' + $('#id_country').val() + '/8541/' + action ;
    var obj = $(this);

    $.post( url, val, function(data) {
        $.colorbox({ 
            //iframe: data , 
            html: data, 
            innerWidth:'70%', 
            innerHeight:'80%',
            width:'90%', 
            height:'80%',
            escKey: true, //true
            overlayClose: false, //true
            onLoad: function() {
                $('body').css('overflow-y','hidden');
            },
            onClosed:function(){
                $('body').css('overflow-y','auto');
            },
            
        });
    });

    $(window).resize(function(){
        obj.colorbox.resize({innerWidth:'70%', innerHeight:'80%',width:'90%', height:'80%'});        
    });   
}

function recreate_error(e, d, action){

    var tabled = $(e).closest('td').find('#form_amazon_error_' + action + '_' + d.code);

    addStatus(e);

    $.ajax({
        type: 'POST',
        url: base_url + "/amazon/solved_error/"+ $('#id_country').val() + "/" + d.code + "/" + action ,
        data: tabled.serialize(),
        dataType:'json',
        success: function(data) {

            successStatus(e);

            if(data.detail.success){
                var sku_success = data.detail.success;
                $.each(sku_success, function(i,j){                    
                    $(e).closest('table').find('input[value="'+i+'"]').prop('checked', false);
                    $(e).closest('table').find('input[value="'+i+'"]').closest('tr').fadeOut(1000);
                });
            }  

            if(data.detail.error){
                var sku_error = data.detail.error;
                $.each(sku_error, function(i,j){
                    //console.log($(e).closest('table').find('input[value="'+i+'"]'));
                    $(e).closest('table').find('input[value="'+i+'"]').prop('checked', false);
                    $(e).closest('table').find('input[value="'+i+'"]').closest('tr').addClass('error_details_item');
                });

                addError(e, 'Error');                
            }
        },
        error: function(data){
            console.log(data);
        }
    });
}*/

function edit_error(e, d, type, override_field_specific){

    if($(e).closest('.background-gray').find('.feedbizloader')){       
        $(e).closest('.background-gray').find('.feedbizloader').closest('.dataTables_processing').show();
        //$(e).closest('.background-gray').find('.feedbizloader').find('.feedbizcolor').destroy();
        //$(e).closest('.background-gray').find('.feedbizloader').find('.feedbizcolor').sprite({fps: 5, no_of_frames: 50});
    }     

    var override_field = '';

    if(override_field_specific){
        override_field = '/' + override_field_specific;
    }

    var tabled = $(e).closest('td').find('#form_amazon_error_resolutions_' + d.code);

    var val = tabled.serialize();    
    var url = base_url + 'amazon/error_edit_data/' + $('#error_type').val() + '/' + $('#id_country').val() + '/' + d.code + '/' + type + override_field;
    var obj = $(this);

    $.post( url, val, function(data) {
        var html_data = '';
        if(data === "false"){
            html_data = $('#msg_empty_sku').html();
        } else {
            html_data = data;
        }
        $.colorbox({ 
            //iframe: data , 
            html: html_data, 
            innerWidth:'70%', 
            innerHeight:'80%',
            width:'90%', 
            height:'80%',
            escKey: true, //true
            overlayClose: false, //true
            onLoad: function() {
                $('body').css('overflow-y','hidden');
            },
            onClosed:function(){
                $('body').css('overflow-y','auto');
                if($(e).closest('.background-gray').find('.feedbizloader')){       
                    $(e).closest('.background-gray').find('.feedbizloader').closest('.dataTables_processing').hide();
                } 
            },
        }); 
});

    $(window).resize(function(){
        obj.colorbox.resize({innerWidth:'70%', innerHeight:'80%',width:'90%', height:'80%'});        
    });   
}

function download_template(d, type){

    var url = base_url + 'amazon/error_data_download_template/' + $('#id_country').val() + '/' + d.code + '/' + type ;
    window.location = url;
    /*$.ajax({
        type: 'GET',
        url: url,
        success: function(data) {
        }
    });*/ 
}

function solved_error(e, d, type, actions){

    var action_type = '';
    if(actions){
        action_type = '/' + actions;
    }

    //var tabled = $(e).closest('td').find('table');
    var tabled = $(e).closest('td').find('#form_amazon_error_resolutions_' + d.code);

    //console.log(tabled.find('input').serialize());
    addStatus(e);

    $.ajax({
        type: 'POST',
        url: base_url + "/amazon/solved_error/"+ $('#id_country').val() + "/" + d.code + "/" + type + action_type,
        data: tabled.serialize(),
        dataType:'json',
        success: function(data) {

            successStatus(e);

            if(data.detail.success){
                var sku_success = data.detail.success;
                $.each(sku_success, function(i,j){                    
                    $(e).closest('table').find('input[value="'+i+'"]').prop('checked', false);
                    $(e).closest('table').find('input[value="'+i+'"]').closest('tr').fadeOut(1000);
                });
            }  

            if(data.detail.error){
                var sku_error = data.detail.error;
                $.each(sku_error, function(i,j){
                    //console.log($(e).closest('table').find('input[value="'+i+'"]'));
                    $(e).closest('table').find('input[value="'+i+'"]').prop('checked', false);
                    $(e).closest('table').find('input[value="'+i+'"]').closest('tr').addClass('error_details_item');
                });

                addError(e, 'Error');                
            }
        },
        error: function(data){
            console.log(data);
        }
    });
}

function confirm_asin(e, d, type){

    var tabled = $(e).closest('td').find('#form_amazon_error_resolutions_' + d.code);

    addStatus(e);

    $.ajax({
        type: 'POST',
        url: base_url + "/amazon/confirm_asin/"+ $('#id_country').val() + "/" + d.code + "/" + type,
        data: tabled.serialize(),
        dataType:'json',
        success: function(data) {
            
            if(!data.pass && data.detail.empty_data){
                addError(e, $('#l_empty').val());
            }

            if(data.detail.success){

                var sku_success = data.detail.success;
                
                $.each(sku_success, function(i,j){  

                    var asin = $(e).closest('table').find('input[name="asin['+i+']"]').val();
                    $(e).closest('table').find('input[name="asin['+i+']"]').closest('td').html('<p style="color: #00CE88"><i class="fa fa-check"></i> '+'' + asin+ '</p>' );                       

                    //$(e).closest('table').find('input[value="'+i+'"]').prop('checked', false);
                    //$(e).closest('table').find('input[value="'+i+'"]').closest('tr').fadeOut(1000);
                });

                successStatus(e);
            }  

            if(data.detail.error){
                var sku_error = data.detail.error;
                $.each(sku_error, function(i,j){
                    //console.log($(e).closest('table').find('input[value="'+i+'"]'));
                    $(e).closest('table').find('input[value="'+i+'"]').prop('checked', false);
                    $(e).closest('table').find('input[value="'+i+'"]').closest('tr').addClass('error_details_item');
                });

                addError(e, 'Error');                
            }          
        },
        error: function(data){
            console.log(data);
        }
    });
}

function fnFormatDetails(table_id, html) {

    var header = $('#error_header').html();
    var sOut =  "<div class=\"error_details_wrapper tabboder clearfix\"><div class=\"error_details error_details_header\">" + header + "</div>" +
                "<form id=\"form_"+table_id+"\" method=\"POST\">" +
                "<table id=\""+table_id+"\" class=\"amazon-responsive-table error_details allCheckBo\">";
    sOut += html;
    sOut += "</table></form></div>";
    return sOut;
}