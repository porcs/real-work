$(document).ready(function () {

    $('head').append('<link rel="stylesheet" href="'+base_url+'assets/css/bootstrap/bootstrap-colorpicker.css">');

    $('#back').on('click', function () {
        window.location.replace(base_url + "amazon/category/" + $('#id_country').val() + '/' + 3 + '/' + $('#mode').val() );
    });

    $('#form-mapping').validate({
        submitHandler: function (form)
        {
            var flag = true;
            
            if (flag === true)
            {
                if($('#popup_status_more').length > 0) {
                    form.submit();
                } else {
                    //bootbox.confirm($('#submit-confirm').val(), function(result) {
                        //if(result) {
                            form.submit();
                        //}                    
                    //});
                }
              
            } else {
                $("html, body").animate({
                    scrollTop: 0
                }, 1000); 
            }
        }
    });

    $('#attribute-mapping').find('select.colorpicker').each(function ()
    {
        $(this).feed_colorpicker();
    });

    $('.attr-link').click(function () {
        var rel = $(this).attr('rel');
        console.log($(this).closest('div.none_valid_value').find('.mapping-valid-value'));
        $(this).closest('div.none_valid_value').find('.mapping-valid-value').toggle();
        if(rel == 1){
            $(this).attr('rel', 2);
            $(this).html($('#attr-link-2').val());
        } else {
            $(this).attr('rel', 1);
            $(this).html($('#attr-link-1').val());
        }
    });
    
    $('.addnewmappingcarrier').click(function ()
    {
        var cloned = $('#main-carrier').clone().appendTo('#new-mapping-carrier');

        cloned.removeClass('bg-color');
        if (cloned.prev().length > 0)
        {
            if (cloned.prev().hasClass('bg-color'))
            {
                cloned.removeClass('bg-color');
            } else {
                cloned.addClass('bg-color');
            }
        } else {
            cloned.addClass('bg-color');
        }

        cloned.css('padding', '5px 0');
        cloned.removeAttr('id');
        cloned.find('input').val('').attr('disabled', false);
        cloned.find('select').val('').attr('disabled', false).css('background', '#fff');

        var selected_select = cloned.find('select[rel="carrier-select"]');

        // Remove used options
        $('#new-mapping-carrier').find('select[rel="carrier-select"] option:selected').each(function (index, option)
        {
            selected_select.find('option[value="' + option.value + '"]').remove();
        });

        if (!selected_select.find('option').length)
        {
            cloned.remove();
            return(false);
        }

        var selected_value = selected_select.find('option:first').val();

        selected_select.attr('id', 'carrier-select-' + selected_value);
        selected_select.attr('data-content', selected_value);

        cloned.find('select[rel="carrier-value"]').attr('id', 'carrier-value-' + selected_value);
        cloned.find('select[rel="carrier-value"]').attr('name', 'carrier[' + selected_value + '][id_carrier]');

        selected_select.on('change', function () {

            var selected_option = $(this).find('option:selected').val();
            $(this).attr('id', 'carrier-select-' + selected_option);

            //change attribute value id
            $('#carrier-value-' + selected_value).attr('name', 'carrier[' + selected_option + '][id_carrier]');
            $('#carrier-value-' + selected_value).attr('id', 'carrier-value-' + selected_option);

            //change attribute id
            selected_select.attr('id', 'carrier-select-' + selected_option);
            selected_select.attr('data-content', selected_option);
        });

        cloned.find('.addnewmappingcarrier').remove();
        cloned.find('.removemappingcarrier').show().
                click(function ()
                {
                    removemappingcarrier($(this));
                });
    });

    //carrier-select-value
    $('#carrier-mapping').find('.carrier-select-value').on('change', function () {

        var data_contdent = $(this).attr('data-content');
        var selected_value = data_contdent;

        var selected_option = $(this).find('option:selected').val();
        console.log(selected_value);
        //change attribute value id
        $('#carrier-value-' + selected_value).attr('name', 'carrier[' + selected_option + '][id_carrier]');
        $('#carrier-value-' + selected_value).attr('id', 'carrier-value-' + selected_option);

        //change attribute id
        $(this).attr('id', 'carrier-select-' + selected_option);
        $(this).attr('data-content', selected_option);
    });

    function removemappingcarrier(obj)
    {
        var rel = $(obj).attr('rel');
        $(obj).parent().remove();

        if ($('#new-mapping-feature-' + rel).children().length === 0)
        {
            $('#feature-group-' + rel).find('input[rel="is_color"]').attr('disabled', false);
            $('#feature-group-' + rel).find('input[rel="is_color"]').parent().find('span.lbl').css('color', '#32a3ce');
        }
    }

});