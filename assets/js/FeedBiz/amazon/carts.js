$(document).ready(function () {
        
    // DataTables initialisation
    $('#carts').DataTable( {
        "columnDefs": [
            { "visible": false,  "targets": 0 },
            { "visible": true},
            { "visible": true},
            { "visible": true},
            //{ "visible": true,},
        ],
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },        
        "asStripeClasses": [ '' ],
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "ajax": {
            "url": base_url + "amazon/carts_page/" + $('#id_country').val(),
            "type": "POST"
        },
        "columns": [ 
            { "data": "mp_order_id", "bSortable": false, }, 
            { "data": "reference", "class":"p-l20" , "width" : "40%"},  
            { "data": null, "class":"p-l10 text-center" , "width" : "15%",
                render: function (data) {
                    return '';
                }
            },          
            /*{ "data": null, "class":"p-l10 text-center" , "width" : "15%",
                render: function (data) {
                    return '<span class="order_status badge-info">' + $('#'+data.status).val() + '</span>';
                }
            }, */
            { "data": "quantity", "class":"p-r10 text-right" , "width" : "15%"},            
            { "data": "timestamp", "class":"p-r10 text-right" , "width" : "20%"},            
        ],      
        "bFilter" : true,
        "paging": true,
        "pagingType": "full_numbers" ,  
        "order": [[3, 'asc']],    
        "drawCallback": function ( ) {

            var api = this.api();
            var rows = api.rows().nodes();
            var last = null;

            api.column(0, {page:'current'} ).data().each( function ( group, i ) {

                var row_data = api.rows(i).data();
                var order_status = null;
                $(row_data).each(function(key,value){
                    order_status = (value.status);
                });

                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group">'+                            
                            '<td colspan="1" class="text-uc dark-gray montserrat">'+
                                '<b>'+ $("#OrderIDRef").val() + " : " + group+'</b>'+
                            '</td>'+
                            '<td colspan="1" class="text-center">'+
                                '<span class="order_status badge-info">' + $('#'+order_status).val() + '</span>' +
                            '</td>'+ 
                            '<td colspan="1"></td>'+
                            '<td colspan="1"></td>'+
                        '</tr>'
                    );
                    last = group;  
                }

            });   

        },
    } );
 

});