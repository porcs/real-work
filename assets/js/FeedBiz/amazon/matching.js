var actions = document.getElementById('actions').value;
var running = true;
var message = '';
var loading_flag = send_offer = false;
var page = count = 0;
var action_mode = '';
var count = 0;

$(document).ready(function () {

    $('#page-load').show();
    //$('#popup_action').show();
    
    get_report('get_report_' + $('#country').val());

    // Create Product 
    $('#create_product').on('click', function (){
        window.location.href = base_url + 'amazon/category/' + $('#id_country').val() + '/2/2'; //($country = null, $popup = null, $mode = 1)      
    });

    //close matching page
    $('#prev').on('click', function (){
        page = 0;
        var loaded = $('#loaded');
        var popup_action = $('#popup_action');
        loaded.find('#sync_match').fadeOut(500);
        popup_action.fadeIn(1000);
        if(action_mode == $('#mode_sync').val() || action_mode == $('#mode_sync_match').val()){
            loaded.find('#sync').fadeIn(2000);
            $('#amazon-automaton-matching-products').html('');
            $('#loadmore').parent().hide();
            $('#synchronization_match').html($('#synchronization_match').attr('title') );
            $('#synchronization_only').html($('#synchronization_only').attr('title') + ' ' + $('#product_to_sync').html() + ' item(s)');
            $('#prev').hide();
        } else if (action_mode == $('mode_match').val()){
            loaded.find('#match').fadeIn(2000);
            $('#match_only').html($('#match_only').attr('title'));
            $('#prev').hide();
        }        
    });
    
    //synchronization_only
    $('#synchronization_only').on('click', function ()
    {
        action_mode = $('#mode_sync').val();
        $('#popup_prev').show();
        
        if ($('#debug').length > 0){
            window.location.href = base_url + 'amazon/debugs/' + $('#id_country').val() + '/' + actions + '/' + $('#mode_sync').val();
        } else {
            var sm = $(this);
            sm.html('<i class="icon-spinner icon-spin"></i> '+$('#please_wait').val()+' ..');

            var running_process = check_process_running();
            running_process.success(function (r) {
                $('#page-load').hide();
                if (!r.status)
                {
                    sm.html(sm.attr('title') + ' ' + $('#product_to_sync').html() + ' item(s)');
                    $('#sync-send-message').show().find('span').html(r.txt);
                    $('#sync').show();

                    if (r.url && r.url !== "")
                        $('#sync-send-message').find('span').append(' &nbsp; <a href="' + base_url + '/' + r.url + '">' + $('#pay_now').val() + '</a>');
                }
                else
                {
                    $('#page-load').show();
                    $('#sync').hide();
                    sm.attr('disabled', true);
                    $('#synchronization_match').attr('disabled', true);
                    synchronize($('#mode_sync').val());
                }
            });
            running_process.error(function (error) {
                console.log(error);
            });
        }
    });

    //synchronization_match
    $('#synchronization_match').on('click', function ()
    {        
        action_mode = $('#mode_sync_match').val();
        $('#popup_prev').show();

        var sm = $(this);
        sm.html('<i class="icon-spinner icon-spin"></i> '+$('#please_wait').val()+' ..');

        var running_process = check_process_running();
        running_process.success(function (r) {
            $('#page-load').hide();
            if (!r.status)
            {
                sm.html(sm.attr('title'));
                $('#sync-send-message').show().find('span').html(r.txt);
                $('#sync').show();

                if (r.url && r.url !== "")
                    $('#sync-send-message').find('span').append(' &nbsp; <a href="' + base_url + '/' + r.url + '">' + $('#pay_now').val() + '</a>');
            }
            else
            {
                $('#page-load').hide();
                $('#sync').hide();
                $('#sync_match').show();
                $('#popup_action').hide();                
                MatchingWizardProcessProducts($('#mode_sync_match').val(), page);
            }
        });
        running_process.error(function (error) {
            console.log(error);
        });
    });

    //match
    $('#match_only').on('click', function ()
    {
        action_mode = $('mode_match').val();
        $('#popup_prev').show();
        
        var sm = $(this);
        sm.html('<i class="icon-spinner icon-spin"></i> '+$('#please_wait').val()+' ..');

        var running_process = check_process_running();
        running_process.success(function (r) {
            $('#page-load').hide();
            if (!r.status)
            {
                sm.html(sm.attr('title'));
                $('#match-send-message').show().find('span').html(r.txt);
                $('#match').show();

                if (r.url && r.url !== "")
                    $('#match-send-message').find('span').append(' &nbsp; <a href="' + base_url + '/' + r.url + '">' + $('#pay_now').val() + '</a>');
            }
            else
            {
                $('#page-load').hide();
                $('#match').hide();
                $('#sync_match').show();
                $('#popup_action').hide();                
                MatchingWizardProcessProducts($('mode_match').val(), page);
            }
        });
        running_process.error(function (error) {
            console.log(error);
        });
    });
    
    $('#back').on('click', function () {
        window.location.href = base_url + 'amazon/category/' + $('#id_country').val() + '/4';
    });

    $('#amazon-automaton-matching-products').delegate('.amazon-automaton-matching-product', 'click', function ()
    {
        $(this).find('.selection.selectable').toggleClass('selected');
    });

    $('#amazon-automaton-matching-action-reject').click(function ()
    {
        $(".amazon-automaton-matching-product[rel=matched] div.selected").each(function () {
            if (!$(this).hasClass('amazon-automaton-matching-product-processing-ok'))
            {
                $(this).addClass('amazon-automaton-matching-product-processing-reject');
                $(this).parent().delay(800).fadeOut();
                $(this).find('input').remove();
            }
        });
    });

    $('#amazon-automaton-matching-action-confirm').click(function ()
    {
        confirm_product();
    });

    $('#finish').click(function () {
        $('#page-load').show();
        $('#sync').hide();
        $('#amazon-automaton-matching-header').hide();
        $('#amazon-automaton-matching-body').hide();
        if ($('#debug').length > 0)
        {
            window.location.href = base_url + 'amazon/debugs/' + $('#id_country').val() + '/' + actions + '/' + $('#mode_sync_match').val();
        } else {
            synchronize($('#mode_sync_match').val());
        }
    });

    display_unmatched();
    select_all();
});

function check_process_running()
{
    return $.ajax({
        type: "get",
        url: base_url + "/tasks/check_running_process/amazon/" + $('#ext').val() + "/" + $('#id_shop').val() + "/synchronize",
        dataType: 'json',
        timeout: 0
    });
}

function synchronize(mode)
{
    running = true;
    $('#loadmore').hide();

    get_report(actions + '_' + $('#country').val());

    $.ajax({
        type: "get",
        url: base_url + "/tasks/ajax_run_popup_market/amazon/" + $('#ext').val() + "/" + $('#id_shop').val() + "/synchronize/" + mode,
        dataType: 'json'
    });
}

function display_unmatched()
{
    $('#matching-display-unmatched').click(function ()
    {
        if ($(this).is(':checked'))
            $('div[id^="pi-"][rel=unmatched]').show();
        else
            $('div[id^="pi-"][rel=unmatched]').hide();
    });
}

function select_all()
{
    $('#matching-display-selectall').click(function ()
    {
        if ($(this).is(':checked'))
            $('.amazon-automaton-matching-product .selection.selectable:not(".selected")').addClass('selected');
        else
            $('.amazon-automaton-matching-product .selection.selectable.selected').removeClass('selected');
    });
}


function get_report_inventory()
{
    return $.ajax({
        type: "get",
        url: base_url + "/amazon/get_report_inventory/" + $('#id_country').val(),
        dataType: 'json',   
        async: false,
        success: function(i) 
        { 
            running = false;
            $('#page-load').hide();
            $('#loading').hide();
            $('#loaded').show();
            $('#sync').show();
             $('#match').hide();                        
             
            $('#feed_no_product').html(i.feed_no_product);
            $('#amazon_no_product').html(i.amazon_no_product);
            $('#product_to_sync').html(i.product_to_sync).parent().parent().addClass('red').show();
            $('#synchronization_only').append(i.product_to_sync + ' item(s)');
            
            if(i.product_to_sync <= 0) {
                $('#synchronization_only').hide();
            }
        },
        error: function(error){
            console.log(error);
        }
    });  
}

function get_report(act)
{
    $('#sync').hide();
    //console.log(base_url + "users/ajax_get_sig_process/" + act);
    $.ajax({
        url: base_url + "users/ajax_get_sig_process/" + act,
        dataType: "json",
        beforeSend: function (jqXHR) {
            jqXHR.fail(function () {
                jqXHR.abort();
                setTimeout(function () {
                    get_report(act);
                }, 5000);
            });
        },
        success: function (r) {
            //console.log(r);
            if (act == actions + '_' + $('#country').val())
            {
                $('#match').hide();
                $('#sync').hide();
                $('#loaded').hide();
                $('#loading').hide();
                $('.no_item_found').hide();
                $('#page-load').show();
                window.location.href = base_url + "/amazon/send/" + $('#id_country').val() + "/1";
            }
            $('#page-load').hide();
            var timeout = r.timeout;
            var delay = r.delay;
            
            if (r.data) {
                var status = r.data.status;
                var err_msg = r.data.err_msg;
                var msg = r.data.msg;
                //var status_text = r.data.status_txt;

                if (status == 1)
                { /*running*/
                    message = msg;
                    $('#loaded').hide();
                    $('#match').hide();
                    $('#sync').hide();

                    var loading = $('#loading').show();
                    loading.find('#message h4').html(msg.title);
                    loading.find('#message h5').html(msg.report.message);
                }
                else if (status == 4)
                { /*error*/
                    running = false;

                    var mess = $('#loading').fadeIn();
                    if (err_msg.title) {
                        mess.find('h4').html(err_msg.title);
                    }

                    mess.find('h5').html('Error: ' + err_msg.error);
                    mess.find('img').fadeOut();
                }
                else if (status == 8)
                { /*finished false*/
                    running = false;
                    $('#loading').hide();
                    $('#loaded').show();
                    $('#match').show();
                    $('#sync').hide();
                }
                else if (status == 9)
                { /*finished true*/                   
                    get_report_inventory();
                }
            } else {

                $('.wizard-steps').show();
                $('#match').hide();
                $('#sync').hide();

                //console.log(count);
                if (act != 'get_report_' + $('#country').val())
                {
                    $('#page-load').show().find('h4').html($('#Connecting-to-Amazon').val() + '.. ');
                }
                else
                {
                    count++;
                    $('#page-load').show().find('h4').html($('#processing').val());

                    if(count == 3)
                    {
                        if (r.error_msg)
                        {
                            get_report_inventory();
                        }
                    }
                }

                console.log(r.error_msg);
                delay = timeout;
            }
            if (running) {
                setTimeout(function () {
                    get_report(act);
                }, delay);
            }
        }, error: function (xhr, status, error) {

            $('#load').remove();
            if (xhr.responseText) {
                console.log('Progress status error :', xhr.responseText);
                if (xhr.responseText === 'terminate') {
                    return;
                }
            }
        }
    });
}

function MatchingWizardProcessProducts(mode, page)
{
    if ($('#autoload').is(':checked')) {
        $('#auto_loading').show();
    } else {
        $('#auto_loading').hide();
        loading = $('#amazon-automaton-matching-products-loader').clone().show().appendTo($('#amazon-automaton-matching-products'));
        loading.attr('rel', 'active');
    }

    //console.log(page);
    $.ajax({
        type: "POST",
        url: base_url + "amazon/matching_products/" + $('#id_country').val() + '/' + mode + '/' + page,
        dataType: 'json',
        //data: product_array,
        /*async: true,*/
        success: function (data) {

            $('.amazon-automaton-matching-products-loader[rel=active]').remove();            

            if (window.console) {
                console.log(data);
            }

            if (data.error && data.errors) {
                error_target = $('#amazon-automaton-matching-error');

                $.each(data.errors, function (e, errormsg) {
                    error_target.append(errormsg + '<br />');
                });
                error_target.show();

                return(false);
            }

            if (data == null || data.products == null || data.products == false) {
                $('#auto_loading').hide();

                if (data.products == false) {
                    $('#loadmore').parent().show().addClass('center no_item_found').find('.wizardItem').html($('#No-items-found').val());
                }

                return(false);
            }

            // Function returned products, process it 
            if (typeof (data.products) == 'object' && Object.keys(data.products).length) {
                $.each(data.products, function (p, product) {

                    if (!product.checked) {
                        return(true);
                    }

                    cloned = $('#amazon-automaton-matching-product-model').clone().appendTo($('#amazon-automaton-matching-products')).attr('id', 'pi-' + p).show();
                    cloned.addClass('amazon-automaton-matching-product');

                    if (!product.matched) {
                        if ($('#matching-display-unmatched').is(':checked')) {
                            cloned.attr('rel', 'unmatched');
                            cloned.find('.selection').removeClass('selectable');
                        } else {
                            cloned.hide();
                        }
                    } else {
                        cloned.attr('rel', 'matched');
                        cloned.find('.selection').append('<input type="hidden" name="matched[' + product.reference + '][amazon_item]" value="' + product.amazon.asin + '" />');
                        cloned.find('.selection').append('<input type="hidden" name="matched[' + product.reference + '][id_product]" value="' + product.id_product + '" />');
                        cloned.find('.selection').append('<input type="hidden" name="matched[' + product.reference + '][asin]" value="' + product.amazon.asin + '"/>');

                        if (product.id_product_attribute && product.id_product_attribute != 'undefined')
                            cloned.find('.selection').append('<input type="hidden" name="matched[' + product.reference + '][id_product_attribute]" value="' + product.id_product_attribute + '" />');
                        if (product.brand_mismatch)
                            cloned.find('.mismatch').show();
                    }

                    cloned.find('span[rel="name"]').html(product.name);
                    cloned.find('span[rel="reference"]').html(product.reference);
                    cloned.find('span[rel="manufacturer"]').html(product.manufacturer);
                    cloned.find('span[rel="code"]').html(product.ean13);

                    if (product.matched) {
                        cloned.find('span[rel="amazon_name"]').html(product.amazon.name);
                        cloned.find('span[rel="amazon_brand"]').html(product.amazon.brand);
                        cloned.find('span[rel="amazon_asin"]').html(product.amazon.asin);
                    } else {
                        cloned.find('.matching-product-right .content').css('visibility', 'hidden');
                    }

                    if (product.matched && $('#matching-load-image').is(':checked')) {
                        addImage(cloned.find('td[rel=image]'), product.image_url, product.name, product.ean13);
                        addImage(cloned.find('td[rel=amazon_image]'), product.amazon.image_url, product.amazon.name, product.amazon.asin);
                    } else {
                        cloned.find('td.image img[rel=loader]').hide();
                        cloned.find('td.image img[rel=nope]').show();
                    }

                    if ($('#matching-load-image').is(':checked')) {
                        $('.image').show();
                        $('#amazon-automaton-matching-products .selectable').css('height', '90px');
                        $('.amazon-automaton-matching-product').css('height', '95px');
                    } else {
                        $('.image').hide();
                        $('#amazon-automaton-matching-products .selectable').css('height', 'auto');
                        $('.amazon-automaton-matching-product').css('height', 'auto');
                    }
                });

                if ($('#autoload').is(':checked')) {
                    $(window).unbind('scroll');
                    setTimeout(function () {
                        MatchingWizardProcessProducts(mode, (page + 1));
                    }, 2000);
                } else {
                    $('#auto_loading').hide();
                    scoll_page_down(mode, (page + 1));
                }
               
                display_unmatched();
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function confirm_product()
{
    data = $(".amazon-automaton-matching-product[rel=matched] div.selected").find('input').serialize();

    $(".amazon-automaton-matching-product[rel=matched] div.selected").each(function () {
        if (!$(this).hasClass('amazon-automaton-matching-product-processing-reject'))
            $(this).addClass('amazon-automaton-matching-product-processing-ok');
    });

    if (!data || !data.length)
        return(false);

    $.ajax({
        type: "POST",
        url: base_url + "/amazon/confirm_products/" + $('#id_country').val(),
        dataType: 'json',
        data: data,
        async: true,
        success: function (data)
        {
            var confirm_count = 0;
            if (typeof (data.products) == 'object' && Object.keys(data.products).length)
            {
                $.each(data.products, function (p, product) {
                    console.log(p);
                    $('#pi-' + p).fadeOut();
                    confirm_count++;
                });

                $('#finish').removeAttr('disabled').html($('#Send').val() + ' ' + confirm_count + ' ' + $('#product_s').val());
            }
            else
            {
                $(".amazon-automaton-matching-product[rel=matched] div.selected")
                        .toggleClass('amazon-automaton-matching-product-processing-ok')
                        .toggleClass('amazon-automaton-matching-product-processing-error');
            }
        },
        error: function (data)
        {
            console.log(data);
        }
    });
}

function scoll_page_down(mode, page)
{
    loading_flag = false;
    var thisIframesHeight = window.parent.$("iframe.cboxIframe").height();
    /*console.log(thisIframesHeight);
    console.log('popup-wrapper - ' + $('.popup-wrapper').height());
    console.log(window.parent.$("iframe.cboxIframe").scrollTop());*/
    if (thisIframesHeight < $('.popup-wrapper').height())  //user scrolled to bottom of the page?
    {
        $(window).scroll(function () { //detect page scrolls
            console.log('scroll');
            if ($(window).scrollTop() + $(window).height() > $('.popup-wrapper').height())  //user scrolled to bottom of the page?
            {
                if (loading_flag == false) //there's more data to load
                {
                    loading_flag = true; //prevent further ajax loading                    
                    $(window).unbind('scroll');
                    MatchingWizardProcessProducts(mode, page);
                }
            }
        });
    }
    else
    {
        $('#loadmore').parent().show();
        $('#loadmore').unbind().on('click', function () {
            $('#loadmore').parent().hide();
            MatchingWizardProcessProducts(mode, page);
        });
    }
}

function addImage(target, url, alt, suffix)
{
    iId = 'p-img-' + suffix;

    if (url == null)
    {
        console.log(url);
        target.find('img[rel=loader]').fadeOut();
        target.find('img[rel=nope]').fadeIn();
        return;
    }
    target.append('<img src="' + url + '" rel="picture" alt="' + alt + '" id="' + iId + '" style="display:none;" />').find('img[rel=picture]').load(function ()
    {
        $(this).parent().find('img[rel=loader]').fadeOut();
        $(this).fadeIn();
    });
}