function orderView(e) {
    $('#viewResult').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i></div>');
    var order_button = $(e);
    $.ajax({
        type: 'POST',
        url: base_url + 'webservice/view_order/' + $(order_button).val() + '/1',
        dataType: 'json',
        success: function (data) {
            //console.log($('#viewResult'), data);  
            $('#viewResult').html(data.result);
        },
        error: function (data) {
            console.log(data);    
        }
    });
}

$(document).ready(function () {

    $('.order-date').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    $('#import-orders').on('click', function () {

        $('.validate').parent().hide();
        $('#actions').find('.status').show();
        get_amazon_orders();
    });

    $('#send-orders').on('click', function () {
        $('.validate').parent().hide();
        $('#actions').find('.status').show();
        send_orders();
    });

    // DataTables initialisation
    $('#statistics').dataTable({
        "language": {
            "emptyTable": $('#emptyTable').val(),
            "info": $('#info').val(),
            "infoEmpty": $('#infoEmpty').val(),
            "infoFiltered": $('#infoFiltered').val(),
            "infoPostFix": $('#infoPostFix').val(),
            "thousands": $('#thousands').val(),
            "lengthMenu": $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing": $('#processing').val(),
            "search": '',
            "zeroRecords": $('#zeroRecords').val(),
            "paginate": {
                "first": $('#paginate-first').val(),
                "last": $('#paginate-last').val(),
                "next": $('#paginate-next').val(),
                "previous": $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending": $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "amazon/orders_page/" + $('#id_country').val(),
            "type": "POST"
        },
        //"bFilter": false,
        "columns": [
            //{ "data": "id_orders" },
            {"data": null, "width": "5%", render: function (data) {
                    if (data.id_orders)
                        return '<button type="button" class="link" data-toggle="modal" data-target="#amazonOrder" value="' + data.id_orders + '" onclick="orderView(this);">' + data.id_orders + '</button>';
                    else
                        return '';
                }},
            {"data": "id_marketplace_order_ref", "width": "15%", 'class': 'p-l10'},
            {"data": null, "width": "10%", render: function (data) {
                    if (data.address_name)
                        return '<a class="address_name" onclick="editAddressName(this,' + data.id_orders + ')">' + data.address_name + '</a>';
                    else
                        return '<input type="text" class="form-control address_name_input" onblur="changeAddressName(' + data.id_orders + ',this)" />';
                }},
            {"data": null, "width": "10%", 'class': 'text-right p-r10', render: function (data) {
                    return '<span class="badge badge-info">' + data.total_paid + '</span>';
                }},
            {"data": null, "width": "10%", 'class': 'text-left p-l5', render: function (data) {
                    var detail = '';

                    if (data.order_status == 'Shipped') {
                        detail = '<span class="order_status shipped">' + data.order_status + '</span>';
                    } else if (data.order_status == 'Unshipped') {
                        detail = '<span class="order_status unshiped">' + data.order_status + '</span>';
                    } else if (data.order_status == 'Canceled') {
                        detail = '<span class="order_status canceled">' + data.order_status + '</span>';
                    } else {
                        detail = data.order_status;
                    }
                    return detail;
                }},
            {"data": "mp_channel", "width": "5%", 'class': 'p-l10'},
            {"data": "order_date", "width": "10%"},
            {"data": "shipping_date", "width": "10%"},
            {"data": "tracking_number", "width": "10%"},
            {"data": "seller_order_id", "width": "10%", 'class': 'p-l10'},
            {"data": "invoice_no", "width": "5%"},
            {"data": null, "width": "5%", 'class': 'text-center', render: function (data, i, j, k) {
                    //amazon_messaging 
                    var detail = '';
                    if ($('#display_messaging').length > 0) {
                        if (data.flag_mail_invoice || data.flag_mail_review) {
                            detail += '<span class="msg" title="' + $('#flag_mail').val() + '"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i></span>';
                        } else {
                            detail += '<span class="msg" title="' + $('#flag_mail_fail').val() + '"><a href><i class="fa fa-envelope fa-lg text-success" aria-hidden="true"></i></a></span>';
                        }
                    }
                    return detail;
                }},
            {"data": null, "width": "5%", render: function (data) {
                    var detail = '';
                    if (data.comment && data.status == 0) {
                        detail = '<button type="button" class="send-orders btn btn-error btn-mini" data-toggle="modal" data-target="#orderDetails" onclick="get_detail(this);" value="' + data.id_orders + '"><i class=" fa fa-warning"></i>&nbsp;<span class="send-status">' + $('#Error').val() + '</span></button>';
                    } else if (data.status == 1) {
                        detail = '<button type="button" class="send-orders btn btn-mini" disabled><i class=" fa fa-check"></i>&nbsp;<span class="send-status">' + $('#Sent').val() + '</span></button>';
                    } else {
                        detail = '<button type="button" class="send-orders btn btn-success btn-mini" value="' + data.id_orders + '" onclick="send_order(this);"><i class=" fa fa-upload"></i>&nbsp;<span class="send-status">' + $('#Send').val() + '</span></button>';
                    }
                    return detail;
                }},
        ],
        "order": [[6, 'desc'], [0, 'desc']],
        "pagingType": "full_numbers",
        "initComplete": function () {
            var api = this.api();
            if ($('#display_messaging').length <= 0) {
                var column11 = api.column(11);
                column11.visible(false);
            }
        }
    });

    $('#statistics tfoot tr.tr_search th').each(function (i) {
        var title = $('#statistics tfoot tr.tr_search th').eq(i).text();
        if (title != "") {
            $(this).parents('th').attr('data-column', i);
            $(this).html('<input type="text" class="form-control column_filter search" placeholder="' + title + '" id="col' + i + '_filter" title="' + title + '"/>');
        }
    });

    var table = $('#statistics').DataTable();

    table.columns().eq(0).each(function (colIdx) {
        $('input', table.column(colIdx).footer()).on('blur', function () {
            table
                    .column(colIdx)
                    .search(this.value)
                    .draw();
        });
    });

    $('.amazon-checkbox').on('click', function () {
        $acheckbox = $(this);
        if ($acheckbox.hasClass('checked')) {
            $acheckbox.find('input').prop('checked', false).change();
            $acheckbox.removeClass('checked');
        } else {
            $acheckbox.find('input').prop('checked', true).change();
            $acheckbox.addClass('checked');
        }

        var status = {};

        if ($('#export-error').prop('checked')) {
            status[0] = '0';
        }
        if ($('#export-sent').prop('checked')) {
            status[1] = '1';
        }

        if ($('#export-send').prop('checked')) {
            status[2] = '';
        }

        if (status) {
            var is_null = false;
            var status_in = '';
            for (var p in status) {
                if (status.hasOwnProperty(p)) {
                    if (status[p] == '') {
                        is_null = true;
                    } else {
                        if (status_in.length > 0) {
                            status_in += " OR ";
                        }
                        status_in += "o.status = '" + status[p] + "' ";
                    }
                }
            }
            var statusin = status_in;
            if (is_null) {
                if (statusin.length > 0) {
                    statusin += " OR ";
                }
                statusin += 'o.status IS NULL '
            }

            if (statusin.length > 0) {
                filter_table(11, '(' + statusin + ')');
            }
        }

        if (statusin.length == 0) {
            filter_table(11, "(o.status = '0' OR o.status = '1' OR o.status IS NULL)");
        }
    });

});

function editAddressName(e, id) {
    $(e).hide();
    $(e).parent().append('<input type="text" class="form-control address_name_input" value="' + $(e).text() + '" onblur="changeAddressName(' + id + ',this)" />');
}

function changeAddressName(id, e) {

    bootbox.confirm($('#confirm-message').val() + " " + $(e).val() + ", order id : " + id + "?", function (result) {

        if (result) {

            $.ajax({
                type: 'POST',
                url: base_url + "amazon/changeOrderAddressName/" + id,
                data: {'name': $(e).val()},
                dataType: 'json',
                success: function (rdata) {
                    if (rdata.success == true) {
                        $(e).hide();
                        $(e).parent().append('<a class="address_name" onclick="editAddressName(this,' + id + ')">' + $(e).val() + '</a>');
                        notification_alert($('#success-message').val() + ', Order ID : "' + id + '". ', 'good');
                    } else {
                        notification_alert($('#unsuccess-message').val() + ', Order ID : "' + id + '". ' + $('#please-try').val() + '.', 'bad');
                    }
                },
                error: function (rerror) {
                    notification_alert($('#unsuccess-message').val() + ', Order ID : "' + id + '". ' + $('#please-try').val() + '.', 'bad');
                }
            });

        }
    });
}

function filter_table(col, val) {
    var table = $('#statistics').DataTable();
    table.column(col).search(val).draw();
}

//get_error
function get_detail(e)
{
    var button = $(e);
    var order_ref = button.val();
    var content_data = null;
    var flag_error = false;
    var cloned = $('#order_details_template').clone();
    $('#orderDetail').html(cloned.html());

    $.ajax({
        type: 'POST',
        url: base_url + "amazon/get_orders_detail/" + order_ref,
        dataType: 'json',
        success: function (rdata) {

            $.each(rdata, function (key, data) {
                content_data = data;
            });

            $.each($('#orderDetail').find('div, input'), function (k, inp) {

                if (!$(inp).attr("rel"))
                    return;

                var input_name = $(inp).attr("rel");

                if (content_data[input_name]) {

                    if (/[`~!<>;?=+()@#"/[\]Â°{}_$%:.*&|]/.test(content_data[input_name])) {
                        $(inp).addClass('help-inline error');
                        flag_error = true;
                    }

                    if ($(inp).is('div')) {
                        $(inp).html(content_data[input_name]);
                    } else {
                        $(inp).val(content_data[input_name]);
                    }

                } else if (content_data['buyer'][input_name]) {

                    if (input_name == "phone") {
                        if (/[a-zA-Z`~!<>;?=@#"/[\]Â°{}_$%:.*&|]/.test(content_data['buyer'][input_name])) {
                            $(inp).addClass('help-inline error');
                            flag_error = true;
                        }
                    } else {
                        if (/[`~!<>;?=+()@#"/[\]Â°{}_$%:.*&|]/.test(content_data['buyer'][input_name])) {
                            $(inp).addClass('help-inline error');
                            flag_error = true;
                        }
                    }

                    if (input_name == "country_code") {
                        var country_code = content_data['buyer'][input_name];
                        if (country_code.length != 2) {
                            $(inp).addClass('help-inline error');
                            flag_error = true;
                        }
                    }

                    if ($(inp).is('div')) {
                        $(inp).html(content_data['buyer'][input_name]);
                    } else {
                        $(inp).val(content_data['buyer'][input_name]);
                    }
                }
            });

            if (flag_error) {
                console.log($('#orderDetail').find('.flag_error'));
                $('#orderDetail').find('.flag_error').show();
            }
        },
        error: function (rerror) {

        }
    });
}

function save_detail() {
    //form_submit
    var input = $('#orderDetail').find('.custom-form').find('input');
    var id = $('#orderDetail').find('input[rel="id_orders"]').val();
    $.ajax({
        type: 'POST',
        url: base_url + "amazon/save_orders_detail",
        data: input.serialize(),
        dataType: 'json',
        success: function (rdata) {
            $('#orderDetails').modal('hide');
            var table = $('#statistics').DataTable();
            table.search(id).draw();
            notification_alert($('#success-message').val() + ', Order ID : "' + id + '". ', 'good');
        },
        error: function (rerror) {
            notification_alert($('#unsuccess-message').val() + ', Order ID : "' + id + '". ' + $('#please-try').val() + '.', 'bad');
        }
    });
}

function send_order(e)
{
    var send = $(e);
    var id_order = send.val();

    send.find('.send-status').html($('#Sending').val());
    send.find('i').removeClass('fa-upload').addClass('fa-spinner fa-spin');

    $.ajax({
        type: 'POST',
        url: base_url + "/webservice/send_orders/" + id_order,
        dataType: 'json',
        success: function (rdata) {
            $('.validate').parent().hide();
            if (rdata.pass)
            {
                var rdisplay = $('#send-products-message').find('.status').show().find('p');
                rdisplay.html(rdata.output);
                //send.closest('td').html(rdata.invoice);

                if (rdata.order_number) {
                    $.ajax({
                        type: "get",
                        url: base_url + "/amazon/orders_acknowledgement/" + $('#id_country').val() + '/' + id_order + '/' + rdata.order_number,
                        dataType: 'json',
                        timeout: 0,
                    });
                }

                send.removeClass('btn-success').attr('disabled', true);
                send.find('.send-status').html('Sent');
                send.find('i').removeClass('fa-spinner fa-spin').addClass('fa-check');

            } else
            {
                var rdisplay = $('#send-products-message').find('.status').show().find('p');
                rdisplay.html(rdata.error);
                $('#send-products-message').find('.status').find('.validate').removeClass('blue').addClass('pink');

                send.find('.send-status').html('Send');
                send.find('i').removeClass('fa-spinner fa-spin').addClass('fa-upload');
            }

        },
        error: function (rerror) {
            $('.validate').parent().hide();
            var rdisplay = $('#send-products-message').find('.status').show().find('p');
            rdisplay.html(rerror);
            $('#send-products-message').find('.status').find('.validate').removeClass('blue').addClass('yellow');

            send.find('.send-status').html('Send');
            send.find('i').removeClass('fa-spinner fa-spin').addClass('fa-upload');
        }
    });

}

//ajax_run_send_orders($market_type, $id_marketplace, $ext, $site, $id_shop, $shop_name, $id_user, $cron_send_orders = 1)
function send_orders()
{
    var market_type = $('#tag').val();
    var id_marketplace = $('#id_marketplace').val();
    var ext = $('#ext').val();
    var site = $('#id_country').val();
    var id_shop = $('#id_shop').val();
    var shop_name = $('#shop_name').val();
    var id_user = $('#id_user').val();
    var cron_send_orders = $('#cron_send_orders').val();

    $.ajax({
        type: "get",
        url: base_url + "/tasks/ajax_run_send_orders/" + market_type + "/" + id_marketplace + "/" + ext + "/" + site + "/" + id_shop + "/" + shop_name + "/" + id_user + "/" + cron_send_orders,
        dataType: 'json',
        timeout: 0,
        success: function (r) {

            var rdisplay = $('#actions').find('.status').show().find('p');

            if (r.txt)
            {
                $('#actions').find('.status').find('i').removeClass('fa fa-spinner fa-spin').addClass('note');
                $('#actions').find('.status').find('.validate').removeClass('blue').addClass('yellow');

                rdisplay.html(r.txt);
                if (r.url && r.url !== "")
                    rdisplay.append(' &nbsp; <a href="' + base_url + '/' + r.url + '">' + $('#pay_now').val() + '</a>');
            }

        }
    });
}

function get_amazon_orders()
{
    var dateFrom = $('#order-date-from').val();
    var dateTo = $('#order-date-to').val();

    var option_data = {};
    option_data.datefrom = dateFrom.replace(/\//g, "-");
    option_data.dateto = dateTo.replace(/\//g, "-");
    option_data.status = $('#order-status').val();

    $.ajax({
        type: "get",
        url: base_url + "/tasks/ajax_run_export/amazon/" + $('#ext').val() + "/" + $('#id_shop').val() + "/orders/" + $('#id_user').val() + "/" + encodeURIComponent(JSON.stringify(option_data)),
        dataType: 'json',
        timeout: 0,
        success: function (r) {

            var rdisplay = $('#actions').find('.status').show().find('p');

            if (r.txt)
            {
                $('#actions').find('.status').find('i').removeClass('fa fa-spinner fa-spin').addClass('note');
                $('#actions').find('.status').find('.validate').removeClass('blue').addClass('yellow');

                rdisplay.html(r.txt);
                if (r.url && r.url !== "")
                    rdisplay.append(' &nbsp; <a href="' + base_url + '/' + r.url + '">' + $('#pay_now').val() + '</a>');
            }

            if (r.debug) {

                $('#actions').find('.status').find('i').removeClass('fa fa-spinner fa-spin').addClass('note');
                $('#actions').find('.status').find('.validate').removeClass('blue').addClass('yellow');

                rdisplay.html($('#error-inactive-message').val());
            }

        }
    });
}
