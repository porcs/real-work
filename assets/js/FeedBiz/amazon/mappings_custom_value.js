$(document).ready(function () {

    $('head').append('<link rel="stylesheet" href="'+base_url+'assets/css/bootstrap-tagsinput.css">');

    $('#save_data').on('click', function(){
        submit_custom_value();
    });
});

function submit_custom_value(){
    $.ajax({
        url: base_url + "/amazon/save_mappings_custom_value",
        type: 'POST',
        data: $("#form-mapping-custom-value").serialize(),             
        dataType: "json",       
        success: function(d) {
            $.each(d, function(universe, value){
                $.each(value, function(product_type, val){
                     $.each(val, function(attribute, v){
                        //console.log(v);
                        var ele = window.parent.$('#attribute-mapping').find('select[name*="['+universe+']['+product_type+']['+attribute+']"]');                         
                        if(ele.length > 0){
                            $.each(ele, function(){

                                var element =  $(this);   

                                if(element.hasClass('search-select')){  

                                    //element.find('optgroup').remove();
                                    if(element.find('optgroup').length <= 0){
                                        element.append('<optgroup label="'+$('#l_Custom_Value').val()+'"></optgroup>');
                                    }                                   
                                    $.each(v, function(i,custom_value){
                                        if(element.find('optgroup').length > 0 && element.find('option[value="' + custom_value + '"]').length <= 0){
                                            element.find('optgroup').append('<option value="'+custom_value+'" class="custom_value">'+custom_value+'</option>');
                                        }                                        
                                    });
                                    updateChosen(element);
                                }

                                if(element.hasClass('colorpicker')){
                                    $.each(v, function(i,custom_value){
                                        if(element.find('option[value="' + custom_value + '"]').length <= 0) {
                                            element.append('<option value="'+custom_value+'" title="text">'+custom_value+'</option>');
                                        }
                                    });

                                    $(element).parent().find('.dropdown-colorpicker').remove();
                                    $(element).feed_colorpicker();
                                }
                            });
                        }
                    });
                });
            });
                      
            parent.$.fn.colorbox.close();            
        },        
        error: function(i,j,k) {
            console.log(i);console.log(j);console.log(k);
        }
    }); 
}

function updateChosen(obj){
    
    $(obj).parent().find('.chosen-container').remove();
    $(obj).parent().find('.chosen-single div').remove();

    $(obj).chosen();
    $(obj).parent().find('.chosen-single').find('div').addClass('button-select');
    $(obj).parent().find('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
    
}