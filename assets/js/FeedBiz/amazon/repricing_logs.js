 var table ;
$(document).ready(function(){

    $('#fba-logs').dataTable( {
         "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         "",
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },
        "processing": true,
        "serverSide": true,
        "order": [[0, 'desc'],[1, 'desc']],
        "ajax": {
            "url": base_url + "amazon/repricing/logs_page/" + $('#id_country').val(),
            "type": "POST"
        },
        "createdRow": function ( row, data, index ) {
            //console.log(row, data, index);
            if ((data.repricing_action == 'reprice' && data.process_type == 'repricing') || 
                (data.repricing_action == 'update_option') ) {
                $(row).addClass('highlight');
            }
        },
        "columns": [           
            { "data": "sku", "bSortable": true, "width": "25%", 'class' : 'p-l25' },
            { "data": "date_upd", "bSortable": true, "width": "15%"  },
            { "data": null, "bSortable": true, "width": "20%", 
                render: function (data, display, row, e) {
                    if(data.repricing_action)
                    {
                        var repricing_action = '';

                        if(data.repricing_action == 'export' && data.process_type == 'repricing'){
                            repricing_action = 'SQS:Feedbiz-' + $('#shop_name').val() + '-Out-' + $('#ext_key').val()+'';

                        } else if(data.repricing_action == 'reprice' && data.process_type == 'repricing'){
                            repricing_action = 'SQS:Feedbiz-' + $('#shop_name').val() + '-In-' + $('#ext_key').val();

                        } else if(data.repricing_action == 'export'){
                            repricing_action = 'Feed.biz';

                        } else if(data.repricing_action == 'update_option'){
                            repricing_action = $('#l_update_option').val();

                        } else {
                            repricing_action = data.repricing_action;
                        }

                        return repricing_action;
                    } else {
                        return '';
                    }
                }
            },
            { "data": null, "bSortable": true, "width": "20%", 
                render: function ( data ) {
                    if(data.process_type)
                    {
                        var process_type = '';

                        if(data.process_type == 'repricing'){
                            process_type = $('#l_repricing_automaton').val();
                        } else if(data.process_type == 'sync'){
                            process_type = $('#l_sync').val();
                        }else if(data.process_type == 'create'){
                            process_type = $('#l_create').val();
                        } else {
                            process_type = data.process_type;
                        }

                        return process_type;
                    } else {
                        return '';
                    }
                }
            },
            { "data": "repricing_mode", "bSortable": true, "width": "10%"  },
            /*{ "data": null, "bSortable": true, "width": "30%", 
                render: function ( data, type, row ) {

                    var repricing_date = new Date(data.repricing_date);
                   
                    var formattedDate = new Date(data.repricing_date);
                    var d = formattedDate.getDate();
                    var m =  formattedDate.getMonth();
                    m += 1;  // JavaScript months are 0-11
                    var y = formattedDate.getFullYear();
                    var h = formattedDate.getHours();
                    var i = formattedDate.getMinutes();
                    var s = formattedDate.getSeconds();

                    repricing_date = y + "-" + m + "-" + d + " " + h + ":" + i + ":" + s ;

                    return repricing_date;
                }
            },*/
            { "data": "reprice", "bSortable": true, "width": "10%" },
        ],       
        "pageLength": 10,
        "pagingType": "full_numbers",
        "drawCallback": function ( ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="6"><b>'+group+'</b></td></tr>'
                    );
 
                    last = group;
                }
            } );
        }

    });
    
    table = $('#fba-logs').DataTable();

});