var errorMessage = '<p><i class="fa fa-exclamation-circle"></i> ' + $('#field_required').val() + '</p>';

$(document).ready(function () {
    
    $('#aws_secret_key').focus(function(){
        $(this).get(0).type = 'text';
    });
    $('#aws_secret_key').focusout(function(){
        $(this).get(0).type = 'password';
    });

    $('#form-repricing').validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        errorElement: 'div',
        errorClass: 'error text-left',
        focusInvalid: true,
        rules: {
            aws_key_id: {required: true},
            aws_secret_key: {required: true}
        },
        messages: {
            aws_key_id: errorMessage,
            aws_secret_key: errorMessage
        },
        submitHandler: function (form)
        {            
            form.submit();
        }
    });

    $('#form-repricing').on('click', '#api_check', function ()
    {
        repricing_checking();
       
        if(check_require_fields()){            
            repricing_check_ws()
        } else {
            repricing_checked();
        }      
       
    });

    $('#key_pair').on('change', function(){
        get_parameters($(this).val());
    });

    $('#form-repricing').checkBo();

    $('#key_pair_checkbox').change(function(){
        $('#key_pair_contain').toggle();            
    }).trigger('change');

});

function get_parameters(id_country){

    if(id_country){
        $('#api_key_loading').show();
    }

    $.ajax({
        type: 'GET',
        url: base_url + "amazon/repricing/parameters/" + id_country + "/0/1",
        dataType: 'json',
        success: function (data) {
            $.each(data, function(i,j){
                if($('#'+i).length > 0){
                    $('#'+i).val(j);
                }
            });
            $('#api_key_loading').hide();
        },
        error: function (error) {
            var alert_message = $('#form-repricing').find('.alert-message').show().find('.code').html('');
            alert_message.append('<p>' + error + ' ...</p>');
        }
    });
}
function repricing_check_ws(){
    $('#form-repricing').find('.alert-message').hide();
     $.ajax({
            type: 'POST',
            url: base_url + "amazon/repricing/check/" + $('#id_country').val(),
            dataType: 'json',
            data: $('#form-repricing').serialize(),
            success: function (data) {

                repricing_checked();

                if (data.pass && data.pass == true)//connect-success
                {
                    $('#status-message').removeClass('error').addClass('success').show().find('p').html('<i class="fa fa-check-circle"></i>'+ $('#connect-success').val());
                    $('#form-repricing').find('.error').remove();
                    $('#form-repricing').find('.form-group').removeClass('has-error');
                    $('#api_check').parent().find('.success').closest('.form-group').addClass('has-success');
                    $('#api_check').addClass('checkIn');

                } else {


                    $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());
                    $('#form-repricing').find('.alert-code').hide();
                    $('#api_check').closest('.form-group').removeClass('has-success');
                    $('#api_check').closest('.form-group').addClass('has-error');
                    var alert_message = $('#form-repricing').find('.alert-message').show().find('.code').html('');
                    $.each(data.result, function(i,j){
                        if(j !== '' && jQuery.type( j ) !== "boolean") {
                            alert_message.append('<p class="m-t5">' + i + ': ' + j + '</p>');
                        }
                    });
                }                
                
            },
            error: function (error) {

                $('#api_check').find('#check').show();
                $('#api_check').find('#checking').hide();

                $('#status-message').removeClass('success').addClass('error').show().find('p').html('<i class="fa fa-exclamation-circle"></i>' + $('#connect-fail').val());
                $('#form-repricing').find('.alert-code').hide();
                $('#api_check').closest('.form-group').removeClass('has-success');
                $('#api_check').closest('.form-group').addClass('has-error');

                var alert_message = $('#form-repricing').find('.alert-message').show().find('.code').html('');
                alert_message.append('<p>' + error + ' ...</p>');
            }
        });
}
function repricing_checking(){
    $('#status-message').find('p').html('');
    $('#api_check').closest('.form-group').removeClass('has-success');
    $('#api_check').closest('.form-group').removeClass('has-error');
    $('#api_check').find('i').removeClass('icon-check').addClass('fa fa-spinner fa-spin');
    $('#api_check').parent().find('.success').hide();
    $('#api_check').find('#check').hide();
    $('#api_check').find('#checking').show();
}

function repricing_checked(){
    $('#api_check').find('i').removeClass('fa fa-spinner fa-spin').addClass('icon-check');
    $('#api_check').find('#check').show();
    $('#api_check').find('#checking').hide();
}

function check_require_fields(){
    var pass = true;
    $('#form-repricing input[type="text"], #form-repricing input[type="password"]').each(function(index, value){
        var thisElement = $(value);
        console.log(typeof thisElement.attr('name'));
        if(typeof thisElement.attr('name') === 'undefined' ){
            return;
        }
        if(thisElement.val().length <= 0){
            var parentElement = thisElement.closest('.form-group');
            parentElement.addClass('has-error');
            parentElement.append('<div for="'+thisElement.attr('id')+'" class="error text-left">'+errorMessage+'</div>');
            pass = false;         
        } 
    });

    if(pass){
        return pass;
    }
}