$(document).ready(function () {

    $('#fba_master_platform').on('click', function ()
    {         
        if($(this).prop('checked'))
         {

            bootbox.confirm($('#set-fba-confirm').val(), function (result)
            {
                if (result)
                {

                    set_parameters('s');

                } else {

                    $('#fba_master_platform').prop('checked', false);
                    $('#fba_master_platform').closest('.amazon-checkbox').removeClass('checked');

               }
            });

        } else {

            bootbox.confirm($('#unset-fba-confirm').val(), function (result)
            {
                if (result)
                {

                    set_parameters('r');

                } else {

                    $('#fba_master_platform').prop('checked', true);
                    $('#fba_master_platform').closest('.amazon-checkbox').addClass('checked');

                }
            });

        }

    });

});

function set_parameters(action){
    $.ajax({
        type: "post",
        url: base_url + "amazon/fba/set_master_platform/" + $('#id_country').val(),
        data: $('#form-fba-setting').serialize(),
        dataType: 'json',
        success: function(data){            

            if(data)
            {
                if(action == 's') {
                    $('#fba_set_success').show();
                    $('#fba_remove_success').hide();
                    $('.master_platform').html( $('#l_Master-Platform').val() + ' : ' + $('#country').val()).addClass('true-green').removeClass('true-pink');

                } else {
                    $('#fba_set_success').hide();
                    $('#fba_remove_success').show();
                    $('.master_platform').html($('#l_No-master-platform').val()).addClass('true-pink').removeClass('true-green');
                }
            } else {
                notification_alert($('#error-message-title').val(), 'bad');
            }

        },
        error: function(data, error, message){
            console.log(data);  
            console.log(error); 
            console.log(message);
            notification_alert($('#error-message-title').val(), 'bad');   
        }
    });  
}

function show_percentage(e){


    //if()
    //$(e).closest('.'+$(e).attr('name')).find('.fees_value').show();
    $(e).closest('.'+$(e).attr('rel')).find('.fees_value').toggle();
    $(e).closest('.'+$(e).attr('rel')).find('.fees_value').find('select').val('');
    //$(e).closest('.'+$(e).attr('rel')).find('.fees_value').find('.chosen-container').removeClass('chosen-disabled');
    //chosen-container chosen-container-single chosen-disabled
    //console.log($(e).closest('.'+$(e).attr('rel')).find('.fees_value').find('select'));
}