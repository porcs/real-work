
var just_cal = false;
var just_check_all = false;
$(document).ready(function () {

    getSelectedAllCategory();
    apply();

    $('#amazon-propagate').on('click', function(){

        var conten_icon = $('#amazon-propagate').html();

        bootbox.confirm($('#propagate-confirm').val(), function(result) {
            if(result) {  

                $('#amazon-propagate').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> ' + $('#processing').val());
                $.ajax({
                    type: 'POST',
                    url: base_url + "amazon/duplicate_category/",
                    data: $("#amazon-category").serialize(),
                    dataType: 'json',
                    success: function (rdata) {   
                        $('#amazon-propagate').html(conten_icon);          
                        notification_alert($('#duplicate-success').val(), 'good');
                    },
                    error: function (rerror) {
                        $('#amazon-propagate').html(conten_icon); 
                        notification_alert($('#duplicate-error').val(), 'bad');
                    }
                });
            }                    
        });
    });

});

function init_rel_all_row() {
    var i = 1;
    if (just_cal)
        return;
    just_cal = true;
    $('.cp_down_btn').each(function () {
        if ($(this).parents('#main').length > 0)
            return;
        var row = $(this).closest('li');
        if (row.length == 0)
            return;
        row.attr('rel', i);
        i++;
    });
}

/*function click_copy_val_sel(obj){
 
 showWaiting(true);
 var time =0;
 var num = $('body').find('select').length;
 if(num > 200){
 time =200;
 } 
 
 setTimeout(function(){
 init_rel_all_row();
 var p_row =  obj.closest('li');
 var cur_pos = p_row.attr('rel');
 var currentLeafOffset = $(p_row).offset();
 var cur_sel_val = obj.closest('.treeRight').find('select').val(); 
 var break_f = false;
 var break_l = false; 
 
 $('#tree1').find('li').each(function(eleIndex, eleObject){ 
 
 if(typeof $(this).attr('rel') == 'undefined')return;
 var rel = $(this).attr('rel'); 
 if(break_f)return; 
 if(currentLeafOffset.top < $(eleObject).offset().top){
 var test_val;
 var l =  $(this).find('select').length ;
 $(this).find('select').each(function(eleIndex){ 
 if(eleIndex*1==(l*1)-1){ 
 test_val = $(this).val(); 
 }
 });
 
 if(test_val!=''){  
 break_f=true;
 return;
 }   
 $(this).find('select').each(function(eleIndex){ 
 if(eleIndex*1==(l*1)-1){ 
 $(this).val(cur_sel_val).chosen({
 disable_search_threshold: 1,
 no_results_text: "Nothing found!"
 }).trigger("chosen:updated");; 
 }
 });                        
 
 }else{ return;}
 
 });
 
 showWaiting(false);
 
 },time);
 
 }*/

function click_copy_val_sel(obj) {
    showWaiting(true);
    var time = 0;
    var num = $('body').find('select').length;
    if (num > 200) {
        time = 200;
    }

    setTimeout(function () {
        var currentTree = $(obj).closest('.tree_point');
        var currentLeaf = $(obj).closest('.treeRight');
        var currentLeafOffset = $(currentLeaf).offset();
        var currentValue = $('select', currentLeaf).val();
        $('.treeRight', currentTree).each(function (eleIndex, eleObject) {
            if ($(eleObject).parent().find('input[type="checkbox"]').prop('checked'))
                if (currentLeafOffset.top < $(eleObject).offset().top) {
                    //case blank
                    var test_val = $('select', eleObject).val();
//                        console.log($('select', eleObject),test_val);
                    if (test_val == '')
                        if (currentValue == '' || currentValue == undefined) {
                            $('select', eleObject).val('');//.trigger("chosen:updated");
                        }//case value
                        else if ($('select', eleObject).val() == '' || $('select', eleObject).val() == undefined) {
                            $('select', eleObject).val(currentValue);//.trigger("chosen:updated");
                        } else if ($('select', eleObject).val() > 0) {
                            $('select', eleObject).val(currentValue);//.trigger("chosen:updated");
                        }
                }
        });
        showWaiting(false);

    }, time);
}

function apply() {

    setTimeout(function () {
        $('li.expandAll').trigger('click');
    }, 100);

    $('.add_category').unbind('click').on('click', function () {
        expandCollapse($(this));
    });

    $('input[rel="id_category"]').unbind('click').change(function () {//.unbind('click')
        getProfile($(this));
        if (!$('.checkBaAll').hasClass('active'))
            recParCheck($(this));
        if (!just_check_all) {
            just_cal = false;
        }
    });

    function recParCheck(cur) {
        var par = cur.parents('ul').prev().prev();
        $(par).each(function (i, elm) {
            if ($(elm).hasClass('tree_item')) {
                par = $(elm).find('input[rel="id_category"]');//.first();
                if (!par.is(':checked') && cur.is(':checked')) {
                    par.prop('checked', true).change();
                    recParCheck(par);
                }
            }
        });
    }

    $.fn.enableCheckboxRangeSelection = function () {
        var lastCheckbox = null;
        var $spec = this;
        var lastType = null;
        $spec.unbind("click");
        $spec.bind("click", function (e) {

            lastType = $(lastCheckbox).find('input[type=checkbox]').is(':checked');// e.currentTarget.checked;
            var $target = this;//e.target
            if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {

                var $checkbox = $spec.slice(
                        Math.min($spec.index(lastCheckbox), $spec.index(this)),
                        Math.max($spec.index(lastCheckbox), $spec.index(this)) + 1
                        );

                $checkbox.find('input[type=checkbox]').each(function () {

                    $(this).prop({'checked': lastType}).change();
                    var chk = Boolean($(this).find('input').attr('checked'));

                    if ($(this)[0] !== lastCheckbox) {
                        switch (true) {
                            case chk :
                                $(this).parent().parent().find('.treeRight').remove();
                                break;
                            case !chk :
                                getProfile($(this).parent().find($("input[type=checkbox]")));
                                break;
                            default:
                                getProfile($(this).parent().find($("input[type=checkbox]")));
                                break;
                        }
                    }
                });
            }
            lastCheckbox = this;
        });
    };

    $('label.checkBa').enableCheckboxRangeSelection();

    if ($('#popup_status').length != 0)
    {
        $('#back').on('click', function () {
            window.location.href = base_url + 'amazon/parameters/' + $('#id_country').val() + '/2/' + $('#mode').val();
        });
    } else
    {
        $('#reset_data').on('click', function () {
            window.location.href = base_url + 'amazon/profiles/' + $('#id_country').val();
        });
    }
    $('.tooltips').tooltipster({ 
        position:'top', 
        theme:'tooltipster-shadow', 
        contentAsHTML: true,
        animation: 'fade',
        touchDevices: true,
        interactive: true
    });
}

//Get Profile
/*function getProfile(e) {
 
 var select;
 var ele = $(e).parents('.tree_item').parent();
 
 if($(e).is(':checked'))
 {
 if(ele.find('select.profile').length === 0)
 {
 var cloned = $('#main').find('.treeRight').clone();
 
 cloned.appendTo(ele);
 cloned.find('select.profile').attr('name', 'category[' + $(e).val() + '][id_profile]');
 cloned.find('.cp_down_btn').click(function(){
 click_copy_val_sel($(this));
 });
 
 ele.find('select').attr('name', 'category[' + $(e).val() + '][id_profile]');
 ele.find('select').attr('id', 'category' + $(e).val() + '_id_profile');
 
 select = cloned.find('select.profile');                 
 }  else {
 select = ele.find('select.profile');
 }
 } else {
 ele.closest('.treeRight').remove();
 }
 
 // Remove old chosen
 if(ele.find('div.chosen-container').length > 0){
 ele.find('div.chosen-container:last').remove();
 }
 
 //choSelect();   
 
 return select;
 }*/

function getProfile(e) {

    var select;
    var ele = $(e).parents('.tree_item').parent();

    if ($(e).is(':checked'))
    {
        if (ele.children('.treeRight').length === 1) {
            ele.children('.treeRight').removeClass('hide');
        }
        if (ele.find('select').length === 0)
        {
            var cloned = $('#main').find('.pull-right').clone();
            cloned.appendTo(ele);
            //console.log($(e).val());
            cloned.find('select').attr('name', 'category[' + $(e).val() + '][id_profile]');
            //cloned.find('select.profile').attr('style', 'background : #DAFFF0 !important;');

            cloned.find('.cp_down_btn').click(function () {
                click_copy_val_sel($(this));
            });

            ele.find('select').attr('name', 'category[' + $(e).val() + '][id_profile]');
            ele.find('select').attr('id', 'category' + $(e).val() + '_id_profile');

            select = cloned.find('select');
        } else {
            ele.find('select').attr('name', ele.find('select').attr('data-name'));
            select = ele.find('select');
        }
    } else {
        ele.find('select').attr('data-name', ele.find('select').attr('name'));
        ele.find('select').removeAttr('name');
        ele.children('.treeRight').addClass('hide');
        //ele.find('.pull-right').remove();
    }

    /*cloned.find('select').select2({
     placeholder: "Select a Option",
     allowClear: true
     });
     
     ele.find('select').select2();*/

    return select;

}

function getSelectedAllCategory() {
    showWaiting(true);
    var url = base_url + "amazon/getSelectedAllCategory/" + $('#id_country').val();

    if ($('#popup_status').length != 0)
    {
        url = url + '/' + $('#mode').val();
    }

    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function (data) {

            if (data && data != "")
            {
                $('#has_category').show();
                $('#tree1').html(data);

                tree($('.tree'));

                apply();
                setTimeout(function () {
                    $('label.checkBa').enableCheckboxRangeSelection();
                    //$(".search-select").chosen();
                    //choSelect();
                    /*$(".search-select").chosen({
                     disable_search_threshold: 1,
                     no_results_text: "Nothing found!"
                     }).trigger("chosen:updated");*/
                    /*$("#tree1 .search-select").each(function(){
                     var size =$('option',this).size();
                     
                     if(size<=1){ 
                     $(this).parent().parent().remove();
                     
                     }else{ 
                     $(this).chosen({
                     disable_search_threshold: 1,
                     no_results_text: "Nothing found!"
                     }).trigger("chosen:updated");
                     }
                     });*/
                    showWaiting(false);
                    $('#page-loaded').show();
                    $('#page-load').hide();
                    $('.custom-form').checkBo();
                }, 500);

                $('.chosen-single').find('div').addClass('button-select');
                $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');

            } else
            {
                showWaiting(false);
                $('#has_category').hide();
                $('#none_category').show();
                $('#page-loaded').show();
                $('#page-load').hide();
                $('.tree').hide();
                apply();
            }

        },
        error: function (data, error) {
            console.log(data);
            console.log(error);
        }
    });
}

function expandCollapse(e) {

    if (!e.hasClass('active')) {
        e.parent().parent().parent().find('ul.tree_child').slideDown();
        e.addClass('active');
    } else {
        e.parent().parent().parent().find('ul.tree_child').slideUp();
        e.removeClass('active');
    }
}

function expandCollapseAll(e) {

    var content = e.parent().parent().find('.tree-folder-content');

    if (content.children().length > 1) {
        content.show();
    } else {
        getSelectedAllCategory(e);
    }

}