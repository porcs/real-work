$(document).ready(function() {

    choSelect();

    $('#save_data').on('click', function(){
        $('#btn_save_attribute').hide();
        $('#btn_saving_attribute').show();
    });

    // on submit {$base_url}amazon/save_error_edit_data
    $('#amazon-error-resolation').validate({   
        submitHandler: function(form) {
            $.ajax({
                type: 'POST',
                url: base_url + "/amazon/save_error_edit_data",
                data: $(form).serialize(),
                dataType:'json',
                success: function(data) {
                    $('#btn_save_attribute').show();
                    $('#btn_saving_attribute').hide();
                    if(data.pass){
                        if(data.detail.success){
                            var sku_success = data.detail.success;
                            $.each(sku_success, function(sku_key, sku_data){
                                //window.parent.$('input[value="'+e+'"]').closest('tr').fadeOut(1000);
                                window.parent.$('input[value="'+sku_key+'"]').prop('checked', false);
                                window.parent.$('input[value="'+sku_key+'"]').closest('.amazon-checkbox').removeClass('checked');
                                var tb_amz_message = window.parent.$('input[value="'+sku_key+'"]').closest('tr').find('.tb_amz_message');
                                tb_amz_message.html('<p class="true-green"><i class="fa fa-check"></i>  <b>'+$('#l_edited_data').val()+'</b></p>');
                                $.each(sku_data, function(e,d){
                                    tb_amz_message.append('<div><b>'+e+'</b> : '+d+'</div>');
                                });
                            });
                        }  
                        if(data.detail.error){
                            var sku_error = data.detail.error;
                            $.each(sku_error, function(sku_key, sku_data){
                                var tb_amz_message = window.parent.$('input[value="'+sku_key+'"]').closest('tr').find('.tb_amz_message');
                                tb_amz_message.css('border', '1px solid red');
                            });
                        }
                        parent.$.fn.colorbox.close();
                    } 
                },
                error: function(data){
                    $('#btn_save_attribute').show();
                    $('#btn_saving_attribute').hide();
                    console.log(data);
                }
            });
        }
    });

	$('.add_more_attribute').on('click', function(){
		var cloned = $(this).closest('div.attribute-overide').clone().appendTo($(this).closest('div.attribute-overide').parent());
		cloned.find('.remove_attribute').show().on('click', function(){
			$(this).closest('.attribute-overide').remove();
		});
		cloned.find('.add_more_attribute').hide();

		var selected_select = cloned.find('select[rel="attribute_field"]');

        // Remove used options
        $(this).closest('div.attribute-overide').parent().find('select[rel="attribute_field"] option:selected').each(function(index, option)
        {
            selected_select.find('option[value="' + option.value + '"]').remove();
        });

        if (!selected_select.find('option').length)
        {
            cloned.remove();
            return(false);
        }

        //onchange
        cloned.find('select[rel="attribute_field"]').removeAttr('name').on('change', function(){
            changename($(this));
        });
        cloned.find('input[rel="override_value"]').removeAttr('name').val('').on('blur', function(){
            changename($(this));
        });

        // Remove old chosen
        if(cloned.find('div.chosen-container').length > 0){
            cloned.find('div.chosen-container').remove();
        }

        choSelect();
	});

    $('select[rel="attribute_field"]').on('change', function(){
        changename($(this));
    });

    $('input[rel="override_value"]').on('blur', function(){
        changename($(this));
    });

    $('.remove_attribute').on('click', function(){
            $(this).closest('.attribute-overide').remove();
    });
});

function changename(e){
    var main = $(e).closest('.attribute-overide');
    var attribute_field = main.find('select[rel="attribute_field"]');
    var override_value = main.find('input[rel="override_value"]');
    //console.log(attribute_field);
    var name = main.closest('.attribute').find('input[rel="key"]').val();

    attribute_field.attr('name', 'attribute_overide[' + name + '][attribute][' + attribute_field.val() + '][attribute_field]');
    override_value.attr('name', 'attribute_overide[' + name + '][attribute][' + attribute_field.val() + '][override_value]');

}