var errorMessage = '<p><i class="fa fa-exclamation-circle"></i> ' + $('#field_required').val() + '</p>';

$(document).ready(function () {
    
    $('#repricing-service-check').on('click', function(){
        repricing_service_settings($(this), 'repricing-service-check-message');
    });
    
    $('#repricing-service-cancel').on('click', function(){
        repricing_service_settings($(this), 'repricing-service-check-message');
    });
    
    $('#repricing-queue-check').on('click', function(){
        repricing_service_settings($(this), 'repricing-queue-check-message');
    });

    $('#repricing-queue-purge').on('click', function(){
        repricing_service_settings($(this), 'purge-queue-section-message');
    });
});


function repricing_service_settings(element, element_id_display_message){

    var data_action = {};    

    repricing_checking(element, element_id_display_message);

    if( element_id_display_message == 'purge-queue-section-message' ){       
        data_action = $('#purge-queue-form').serialize();
    } else {
        data_action.action = element.val();
    }

    console.log(data_action);

    $.ajax({
        type: 'POST',
        url: base_url + "amazon/repricing/service_settings/" + $('#id_country').val(),
        dataType: 'json',
        //data: {'action' : element.val(), },data_action
        data: data_action,
        success: function (data) {
            console.log(data);
            if(data.status === 'success'){

                repricing_success(element, data.message, element_id_display_message);

                if( element_id_display_message == 'purge-queue-section-message' && data.queues){
                    $.each(data.queues, function (n, name) {
                        $('input[name="purge_queue['+name+']"]').parents('tr').css('opacity','0.4');
                        $('input[name="purge_queue['+name+']"]').attr('disabled', true).attr('checked', true).prop('checked', $('input[name="purge_queue['+name+']"]').prop('checked')).change();
                        $('input[name="purge_queue['+name+']"]').parents('.cb-checkbox').checkBo();
                    });
                } 

            } 
            if(data.status === 'error'){
                repricing_error(element, data.message, element_id_display_message);
            }             
        },
        error: function (error, status) {
            console.log(error, status);
            repricing_error(element, error + ' ' + status, element_id_display_message);
        }
    });
}

function repricing_success(element, messages, element_id_display_message){
    $('#' + element_id_display_message).show();
    repricing_checked(element, $('#connect-success').val());
    
    $('#' + element_id_display_message).find('.validate').addClass('blue');
    $('#' + element_id_display_message + '-content').append(messages);

    if( element_id_display_message == 'repricing-queue-check-message' ){
        $('#repricing-queue-check-message').checkBo().trigger('change');
        $('#purge-queue-section').show();
    } 
    
    else {
        $('#' + element_id_display_message).find('i').addClass('fa-check-circle');
    } 
}

function repricing_error(element, messages, element_id_display_message){
    $('#' + element_id_display_message).show();
    repricing_checked(element, $('#connect-fail').val());
    $('#' + element_id_display_message).find('i').addClass('fa-warning');
    $('#' + element_id_display_message).find('.validate').addClass('pink');

    if( element_id_display_message == 'repricing-queue-check-message' ){
        $('#' + element_id_display_message + '-content').append('<p class="error">' + messages + '</p>');
    } else {
        $('#' + element_id_display_message + '-content').append(messages);
    }
}

function repricing_checking(e, element_id_display_message){
    $(e).parent().find('.status-message').find('p').removeClass('success').hide().html('');    
    $(e).find('i').addClass('fa-spinner fa-spin');
   
    $(e).find('.check').hide();
    $(e).find('.checking').show();

    $('#' + element_id_display_message).hide();
    $('#' + element_id_display_message).find('i').removeClass('fa-warning fa-check-circle');
    $('#' + element_id_display_message).find('.validate').removeClass('pink blue');
    $('#' + element_id_display_message + '-content').html('');
}

function repricing_checked(e, status){
    $(e).parent().find('.status-message').find('p').addClass('success').show().html(status);    
    $(e).find('i').removeClass('fa-spinner fa-spin');
    $(e).find('.check').show();
    $(e).find('.checking').hide();
}

function check_require_fields(){
    var pass = true;
    $('#form-repricing input[type="text"], #form-repricing input[type="password"]').each(function(index, value){
        var thisElement = $(value);
        console.log(typeof thisElement.attr('name'));
        if(typeof thisElement.attr('name') === 'undefined' ){
            return;
        }
        if(thisElement.val().length <= 0){
            var parentElement = thisElement.closest('.form-group');
            parentElement.addClass('has-error');
            parentElement.append('<div for="'+thisElement.attr('id')+'" class="error text-left">'+errorMessage+'</div>');
            pass = false;         
        } 
    });

    if(pass){
        return pass;
    }
}