        
        var data                            = new Array();
        var cloned                          = $("div.close").clone();
        var page                                    = $("#current-page").val();
        var offset  = 0;
        var limit   = 10;
        var first   = 0;
        var ColVis_first = 0;
        var showChar = 80;
        var ellipsestext = "...";
        var moretext = "more";
        var lesstext = "less";
        var filter_ignore_inactive = true; 
        function morelink(ele) {
                $(ele).click(function(e){
                    e.preventDefault();
                    if($(this).hasClass("less")) {
                        $(this).removeClass("less");
                        $(this).html(moretext);
                    } else {
                        $(this).addClass("less");
                        $(this).html(lesstext);
                    }
                    $(this).parent().prev().toggle();
                    $(this).prev().toggle();
                    return false;
                });
        }
        
        var oTable = $('#statistics').dataTable( {
            "processing": true,
            "serverSide": true,
            "searching": true,
            "bStateSave": true,
            "sDom": 'C<"clear">lfrtip',
            "oColVis": {
                aiExclude: [0, 7],
//                bRestore: true,
//                activate: "mouseover",
                bShowAll: $('.show-all').val(),
            },
            "ajax": {
                "url": base_url + "ebay/statistic_processing?limit=" + limit + '&offset=' + offset + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val()+'&ignore_case='+ filter_ignore_inactive,
                "type": "GET",
            }, 
            "fnRowCallback": function( nRow, aData, iDisplayIndex ) { 
                    $(nRow).find('td').each(function() {
                            if ( $(this).hasClass('message-show') ) {
                                    var message_text = $(this).text();
                                    if( message_text.length > showChar) {
                                        var c = message_text.substr(0, showChar);
                                        var h = message_text.substr(showChar, message_text.length - showChar);
                                        var html = c + '<span class="moreellipses">' + ellipsestext+ ' </span><span class="morecontent"><span>' + h + '</span>  <a href="" class="morelink red">' + moretext + '</a></span>';
                                        $(this).html(html);
                                        morelink($(this).find('.morelink'));
                                    }
//                                    if ( message_text !== '' ) {
//                                            $(this).html('<span id="tooltip" title="'+message_text+'"><i class="icon-warning"></i></span>');
//                                            $(this).find('#tooltip').tooltipster({ position:'right', theme:'tooltipster-shadow'});
//                                    }
                            }
                    })
            },
            fnPreDrawCallback : function( oSettings ) {
                    $('#statistics_processing').hide();
                    limit   = oSettings._iDisplayLength;
                    offset  = oSettings._iDisplayStart;
                    if ( offset % limit > 0 ) {
                            offset = Math.floor(offset / limit);
                    }
                    var batch_id            = $('input[rel="batch_id"]').val();
                    var id_product          = $('input[rel="id_product"]').val();
                    var product_sku          = $('input[rel="product_sku"]').val();
                    var product_ean13          = $('input[rel="product_ean13"]').val();
                    var date_add            = $('input[rel="date_add"]').val();
                    var name                = $('input[rel="name"]').val();
                    var type                = $('input[rel="type"]').val();
                    var response            = $('input[rel="response"]').val();
                    var message             = $('input[rel="message"]').val();
                    var all                 = $('.dataTables_filter input').val();
                    
                    aSorting                = [];
                    aSorting.column         = oSettings.aaSorting[0][0];
                    aSorting.by             = oSettings.aaSorting[0][1];
                    dataFilter = {
                        'batch_id'      : batch_id,
                        'id_product'    : id_product,
                        'product_sku'    : product_sku,
                        'product_ean13'    : product_ean13,
                        'response'      : response,
                        'date_add'      : date_add,
                        'type'          : type,
                        'name_product'  : name,
                        'message'       : message,
                        'sort_name'     : aSorting.column,
                        'sort_by'       : aSorting.by,
                        'all'           : all,
                    };
                    var count = 0
                    $.each(dataFilter, function(index, val) {
                            if ( count === aSorting.column) {
                                    dataFilter.sort_name        = index;
                            }
                            count++;
                    });
                    oSettings._iDisplayStart    = offset;
                    oSettings.iDraw             = Math.floor(oSettings._iDisplayStart / limit);
                    oSettings.ajax              = base_url + "ebay/statistic_processing?limit=" + limit + '&offset=' + offset + '&' + $.param(dataFilter) + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val()+'&ignore_case='+ filter_ignore_inactive;
                    
                    if (first == 0) {
                            $('#statistics_wrapper')
                                    .append("<div class='row'><div class='col-xs-4'></div><div class='col-xs-8'></div></div>");
                            $('.dataTables_paginate')
                                    .appendTo("#statistics_wrapper > .row > .col-xs-8:last");
                            $('#statistics_wrapper')
                                    .prepend("<div class='row separator bottom'><div class='col-md-6 form-horizontal'></div><div class='col-md-6'></div></div>");
                            $('#statistics_length')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length')
                                    .append("<div class='col-xs-6 col-md-6'></div>")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length > label > select')
                                    .addClass('.selectpicker')
                                    .appendTo("#statistics_length > .col-xs-6.col-md-6")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length > label:first')
                                    .addClass('col-xs-2 col-md-2 control-label')
                                    .text('Per page');
                            $('.row.separator.bottom').find('.col-md-6:first').append('<div class="dataTables_header"><label class="control-label">'+$('.title-statistics').val()+'</label></div>');
                            $('#statistics_filter')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6.form-horizontal:first")
                                    .parents("#statistics_wrapper")
                                    .find("#statistics_filter")
                                    .append("<div class='col-md-8'></div>")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_filter > label > input')
                                    .addClass('form-control').attr('placeholder', $('.hidden-search').val())
                                    .appendTo("#statistics_filter > .col-md-8")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_filter > label:first')
                                    .addClass('col-md-6 control-label').hide();
                            $('.ColVis')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last");
                            $('.dataTables_info')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last");
                            $('.check_filter_cv')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last").show();
                            first++;
                            $(".prev a").each(function() {
                                        $(this).text($(this).text().replace("← ", ''));
                            });
                            $(".next a").each(function() {
                                        $(this).text($(this).text().replace(" → ", ''));
                            });
                    }
            },
            fnDrawCallback: function( oSettings ) {
                if ( ColVis_first === 0 ) {
                        $('.ColVis_Button').trigger('click');
                        $('.ColVis_collection').find('li').each(function() {
                                var $li                 = $(this);
                                if ( $(this).find('.cb-checkbox').length === 0 ) {
                                        if ( $(this).find('span').length > 0 ) {
                                                if ( $(this).find('input').prop('checked') === true ) {
                                                        $(this).find('label').addClass('cb-checkbox cb-sm checked');
                                                }
                                                else {
                                                        $(this).find('label').addClass('cb-checkbox cb-sm');
                                                }
                                                $(this).checkBo();
                                                var checkbox = $(this);
                                                $(this).find('.cb-checkbox').unbind('click');
                                                $(this).find('i').unbind('click').on('click', function(e) {
                                                        if ( e.target.tagName.toString() === 'INPUT') {
                                                                return false;
                                                        }
                                                        e.preventDefault();
                                                        $(this).parents('li').trigger('click');
                                                });
                                                $(this).find('span:last').on('click', function(e) {
                                                        e.preventDefault();
                                                        $(this).parents('li').trigger('click');
                                                });
                                                $(this).on('click', function(e) {
                                                        if ( $(this).find('input').is(':checked') ) {
                                                                $(this).find('label.cb-checkbox').addClass('checked');
                                                        }
                                                        else if ( $(this).find('input').is(':checked') === false ) {
                                                                $(this).find('label.cb-checkbox').removeClass('checked');
                                                        }
                                                });

                                                setTimeout(function() {
                                                        if ( oTable.width() < 1400 ) {
                                                                if ( $.trim($li.find('span:last').text()) === $('.user').val() && $li.find('input').is(':checked') ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                        else if ( oTable.width() >= 1400  ) {
                                                                if ( $.trim($li.find('span:last').text()) === $('.user').val() && $li.find('input').is(':checked') === false ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                        if ( oTable.width() < 1300 ) {
                                                                if ( $.trim($li.find('span:last').text()) === $('.export_date').val() && $li.find('input').is(':checked') ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                        else if ( oTable.width() >= 1300  ) {
                                                                if ( $.trim($li.find('span:last').text()) === $('.export_date').val() && $li.find('input').is(':checked') === false ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                        if ( oTable.width() < 1100 ) {
                                                                if ( $.trim($li.find('span:last').text()) === $('.message').val() && $li.find('input').is(':checked') ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                        else if ( oTable.width() >= 1100  ) {
                                                                if ( $.trim($li.find('span:last').text()) === $('.message').val() && $li.find('input').is(':checked') === false ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                        if ( oTable.width() < 700 ) {
                                                                if ( $.trim($li.find('span:last').text()) === $('.type').val() && $li.find('input').is(':checked') ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                        else if ( oTable.width() >= 700  ) {
                                                                if ( $.trim($li.find('span:last').text()) === $('.type').val() && $li.find('input').is(':checked') === false ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                }, 100);
                                        }
                                        if ( $(this).text() === $('.show-all').val() ) {
                                                var checkbox = $(this);
                                                $(this).find('.cb-checkbox').unbind('click');
                                                $(this).unbind('click').click(function() {
                                                        $('.ColVis_collection').find('input').each(function() {
                                                                if ( $(this).prop('checked') === false ) {
                                                                        $(this).parents('li').trigger('click');
                                                                }
                                                        });
                                                })
                                        }
                                }
                        });
                }
                ColVis_first++;
                $('#statistics tr').each(function () {
                        var td_index = 2;
                        $('#statistics thead tr th').each(function(index) {
                                if ( $(this).attr('rel') === 'response' ) {
                                        td_index = index;
                                }
                        });
                        $(this).find('td:eq('+ td_index +')').filter(':contains("Error")').html('<span class="label label-important label-show label-show-error">Error</span>');
                        $(this).find('td:eq('+ td_index +')').filter(':contains("Warning")').html('<span class="label label-warning label-show label-show-warning">Warning</span>');
                        $(this).find('td:eq('+ td_index +')').filter(':contains("Success")').html('<span class="label label-success label-show label-show-success">Success</span>');
                        $(this).find('td:eq('+ td_index +')').filter(':contains("Failure")').html('<span class="label label-inverse label-show label-show-failure">Failure</span>');
                });
            },
            "columns": [
                { "data": "batch_id", "bSortable": false, "sClass": "center" },
                { "data": "id_product", "bSortable": false, "sClass": "center" },
                { "data": "product_sku", "bSortable": false, "sClass": "center" },
                { "data": "product_ean13", "bSortable": false, "sClass": "center" },
                { "data": "response", "bSortable": false, "sClass": "center" },
                { "data": "date_add", "sClass": "center sorting_desc"  },
                { "data": "type", "sClass": "center" },
                { "data": "name_product" },
                { "data": "message", "bSortable": false, "sClass": "message-show" },
            ],
            "oLanguage": {
                "sInfo": "<strong>_START_</strong> - <strong>_END_</strong> of <strong>_TOTAL_</strong>",
                "sInfoEmpty": "",
                "oPaginate": {
                    "sFirst": "First page",
                },
            },
            "aaSorting": [[ 5, "desc" ]],
            "aLengthMenu": [
                [10, 25, 50, 100, 200, -1],
                [10, 25, 50, 100, 200, "All"]
            ], iDisplayLength: 10,
        } );
        

        $(document).mouseup(function (e) {
                if ($('.ColVis_collection').has(e.target).length === 0){
                        $('.ColVis_collection').hide();
                        $('.ColVis_catcher').hide();
                }
        });

        
     
        $('#chk_ignore_inactive').change(function(){
           filter_ignore_inactive = $(this).is(':checked');   
           oTable.fnDraw();
        });
        
        $('input').on('keyup', function() {
                if($(this).attr('id')=='chk_ignore_inactive')return;
                oTable.fnDraw();
        });
