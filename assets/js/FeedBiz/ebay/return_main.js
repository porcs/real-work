
        var postal_valid;
        var postal_valid_res                = 1;
        var chk_value                       = '';
        var data                            = new Array();
        var wizard_data                     = new Array();
        var category_id                     = 0;
                
        function show_option(element) {
                $('#returns-policy-days').parents('.form-group').first().find('.policy-days').toggleClass('no_accepted');
                $('#returns-policy-pays').parents('.form-group').first().find('.policy-pays').toggleClass('no_accepted');
                $('#holiday-return').parents('.form-group').first().toggleClass('no_accepted');
        }
        
        $('#returns-policy-type').unbind('change').change(function() {
                show_option($(this));
        });
        
        $('#returns-policy-type option').each(function() {
                if ( $(this).is(':selected') && $(this).val() === 'ReturnsNotAccepted' ) {
                        show_option($(this));
                }
        });
        
        function updateCountdown() {
                var remaining = 1000 - $('#returns-information').val().length;
                $('.countdown-textarea').text($('.remaining').val() + ': ' + remaining);
        }

        updateCountdown();
        $('#returns-information').change(updateCountdown);
        $('#returns-information').keyup(updateCountdown);

        $("#form-submit").on('submit', function(e) {
                e.preventDefault();
                data                            = [];
                
                data_selected                   = {
                        'id_profile'                        : $('.id_profile').val(),
                        'returns_policy'                    : $("#returns-policy-type").val(),
                        'returns_within'                    : $("#returns-policy-days").val(),
                        'returns_pays'                      : $("#returns-policy-pays").val(),
                        'returns_information'               : $("#returns-information").val(),
                        'holiday_return'                    : $("#holiday-return").is(':checked') ? 1 : 0,
                        'id_site'                           : $("#id-site").val(),
                        'id_packages'                       : $("#id-packages").val(),
                        'step'                              : $("#popup-step").val(),
                };
                data.push(data_selected);
                
                if ($.isEmptyObject(data)) {
                        data_value              = {
                                'id_site'               : $("#id-site").val(),
                                'id_packages'           : $("#id-packages").val(),
                        };
                        data.push(data_value);
                }
                
                var data_selected = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'id_popup'      : $("#popup-step").val(),
                };
                wizard_data.push(data_selected);

                ajax_save(e);
        });
