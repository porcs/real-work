            var data                            = new Array();
            var form_next                       = $('#form-submit').attr('action');
            var inputfile;
            
            function click_deleted(ele) {
                    bootbox.confirm($('.are-you-sure').val(), function(result) {
                            if(result) {
                                    template_deleted(ele);
                            }
                    });
            }
            
            $(".bootbox-confirm").on('click', function() {
                    click_deleted($(this))
            });
            
            $('.default-download .btn').on("click", function(e) {
                    e.preventDefault();
                    template_default();
            });
            
            
            $('.file-preview').on("click", function() {
                    template_preview($(this));
            });
            
            $('select#form-file').on('change', function() {
                    $('#FileInput').trigger('click');
            });
            
            $('.file-download').on("click", function(e) {
                    e.preventDefault();
                    template_download($(this));
            });
            
            $('.file-click').unbind('click').on('click', function(e) {
                    e.preventDefault();
                    inputfile           = $('#form-file option:selected').val();
                    var options = { 
                            success     :  afterSuccess,
                            resetForm   :  true,
                    }; 
                    $("#form-submit").attr('action', base_url + "ebay/templates_upload");
                    $("#form-submit").ajaxSubmit(options);  
                    return false; 
            }); 
            
            $('#form-submit').on('submit', function(e) {
                    e.preventDefault();
                    location.href   = form_next;
            }); 
   
            function afterSuccess(responseText) {
                    var obj = jQuery.parseJSON(responseText);
                    if ( obj.Response == 'Success' && (inputfile == 'New') ) {
                            $.each (obj.Data.upload_data, function(key, val) {
                                    if ( key == 'file_name' ) {
                                            $("#custom-templates").show().removeClass('field-hide');
                                            $(".templates-main").show().removeClass('field-hide');
                                            var cloned = $(".list-templates").find('.form-group:first').clone();
                                            cloned.appendTo('.list-templates').show();
                                            cloned.find('.lbl').text(val);
                                            cloned.find('input').val(val);
                                            cloned.find('input').radio('check');
                                            cloned.find('.bootbox-confirm').click(function() {
                                                    click_deleted($(this))
                                            });
                                            cloned.find('.file-preview').click(function() {
                                                    template_preview(cloned.find('.file-preview'));
                                            });
                                            cloned.find('.file-download').click(function(e) {
                                                    e.preventDefault();
                                                    template_download(cloned.find('.file-download'));
                                            });
                                            $("#form-file").append('<option value="'+val+'">Edit > '+val+'</option>');
                                            notification_alert($(".update-" + page + "-add-success").val() );
                                    }
                            });
                    }
                    else if ( obj.Response == 'Success' && (inputfile != 'New') ) {
                            notification_alert($(".update-" + page + "-success").val() );
                    }
                    else {
                            notification_alert(obj.Error.error.replace('<p>','').replace('</p>', ''), 'bad');
                    }
            }
            
            function template_deleted(objText) {
                    data                = [];
                    var text            = '';
                    var $checked;
                    $('.list-templates').find('.custom-name').each(function() {
                            if ( $(this).is(':checked') ) {
                                    text    = $(this).val();
                                    $checked = $(this);
                            }
                    });
                    
                    if ( text === '' ) {
                            return false;
                    }
                    
                    data_value                  = {
                            'id_site'           : $("#id-site").val(),
                            'id_packages'       : $("#id-packages").val(),
                            'template_name'     : text,
                    };
                    
                    data.push(data_value);
                    
                    var page = $("#current-page").val();
                    $.ajax ({
                            type        : "POST",
                            url         : base_url + "ebay/templates_deleted",
                            data        : { data : data },
                            error       : function(jqXHR, textStatus, errorThrown) {
                                        notification_alert(errorThrown, 'bad');
                            },
                            success     : function(result) {
                                        if ( $.trim(result) == 'Success' ) {   
                                                notification_alert($(".update-" + page + "-delete-success").val() );
                                                $checked.parents('.form-group:first').remove();
                                                $("select#form-file option").each(function() {
                                                        if ( $(this).val() == text ) {
                                                                $(this).remove();
                                                        }
                                                });
                                                
                                                if ( $("select#form-file option").length === 2 ) {
                                                        $("#custom-templates").addClass('field-hide');
                                                        $(".templates-main").eq(1).addClass('field-hide');
                                                }
                                        }
                                        else {    
                                                notification_alert($(".update-" + page + "-delete-fail").val(), 'bad');
                                        }
                            }
                    });
            }
            
            function template_preview(objText) {
                    data                = [];
                    var text            = '';
                    $('.list-templates').find('.custom-name').each(function() {
                            if ( $(this).is(':checked') ) {
                                    text    = $(this).val();
                            }
                    });
                    
                    if ( text === '' ) {
                            return false;
                    }
                    
                    data_value                  = {
                            'id_site'           : $("#id-site").val(),
                            'id_packages'       : $("#id-packages").val(),
                            'template_name'     : text,
                    };
                    data.push(data_value);
                    
                    var page = $("#current-page").val();
                    var wind = window.open('about:blank', 'sharegplus', 'height=1000,width=1000,left=100,top=100');

                    $.ajax ({
                            type        : "POST",
                            url         : base_url + "ebay/templates_preview",
                            data        : { data : data },
                            error       : function(jqXHR, textStatus, errorThrown) {
                                        notification_alert(errorThrown, 'bad');
                            },
                            success     : function(results) { 
                                        var obj = jQuery.parseJSON(results);

                                        if ( $.trim(obj.Response) == 'Success' ) {   
                                                notification_alert($(".update-" + page + "-preview-success").val() );
                                                wind.document.write($.trim(atob(obj.Result)));
                                        }
                                        else {    
                                                notification_alert($(".update-" + page + "-preview-fail").val(), 'bad');
                                        }
                            }
                    });
            }
            
            function template_download(objText) {
                    var text            = '';
                    $('.list-templates').find('.custom-name').each(function() {
                            if ( $(this).is(':checked') ) {
                                    text    = $(this).val();
                            }
                    });
                    
                    if ( text === '' ) {
                            return false;
                    }
                    var url             = base_url + 'ebay/templates_download/'+ $("#id-packages").val() +'/' + $("#id-site").val() + '/' + text;
                    window.open(url, '_blank');
            }
            
            function template_default() {
                    window.open(base_url + 'ebay/templates_download/'+ $("#id-packages").val() +'/' + $("#id-site").val() + '/default.tpl', '_blank');
            }