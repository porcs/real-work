        var just_cal = false;
        var just_check_all = false;
        var old_select;
        var first_click;
        var data                            = new Array();
        var cloned                          = $("div.close").clone();
        var page                            = $("#current-page").val();
    
        $(document).ready(function() {
            $('a').css({'text-decoration' : 'none'});
            $('[data-rel=popover]').popover({container:'body', html : true});
            $('#loading-result').hide();
            
            back();
            function back() {
                    $('button[type="back"]').on("click", function(e) {
                            e.preventDefault();
                            location.href   = base_url + "ebay/mapping/mapping_categories/" + packages;
                    });
            }
            
            $.fn.chosenDestroy = function () {
                    $(this).show().removeClass('chzn-done').removeAttr('id');
                    $(this).next().remove();
            }
            
            function removeChecked(obj) {
                    $obj                = $(obj).parents('.form-group');
                    $(obj).parents('.attribute-task, .new-attribute-task').remove();
                    $obj_task           = $obj.find('.new-attribute-task');
                    if ( $obj_task.length == 1 ) {
                            $obj_task.find('select[name="attribute_main"] option:disabled').removeAttr('disabled');
                            $obj_task.find('select[name="attribute_main_ebay"] option:disabled').removeAttr('disabled');
                            $obj_task.find('select[name="attribute_main_ebay"]').select2();
                            $obj_task.find('select[name="attribute_main"], select[name="attribute_main_ebay"]').change(function() {
                                    if ( $obj_task.find('select[name="attribute_main"] option:selected').val() === '0' || $obj_task.find('select[name="attribute_main_ebay"] option:selected').val() === '0') {
                                            $obj_task.find('.attr-new, .edit-new').css({'visibility' : 'hidden'});
                                    }
                                    else {
                                            $obj_task.find('.attr-new, .edit-new').css({'visibility' : 'visible'});
                                    }
                            });
                    }
            }
            
            function removeAttribute(obj) {
                    removeChecked(obj);
                    var id = parseInt($('#count').val()) - 1 ;
                    $('#count').val(id);
            }
            
            $('.form-group').each(function() {
                    $obj_task           = $(this).find('.new-attribute-task');
                    if ( $obj_task.length == 1 ) {
                            $obj_task.find('select[name="attribute_main"] option:disabled').removeAttr('disabled');
                            $obj_task.find('select[name="attribute_main_ebay"] option:disabled').removeAttr('disabled');
                            $obj_task.find('select[name="attribute_main_ebay"]').select2();
                            //$('.chzn-select').chosenDestroy('chzn-done');
                            //$('.chzn-select').chosen();
                            $obj_task.find('select[name="attribute_main"], select[name="attribute_main_ebay"]').change(function() {
                                    if ( $obj_task.find('select[name="attribute_main"] option:selected').val() === '0' || $obj_task.find('select[name="attribute_main_ebay"] option:selected').val() === '0') {
                                            $obj_task.find('.attr-new, .edit-new').css({'visibility' : 'hidden'});
                                    }
                                    else {
                                            $obj_task.find('.attr-new, .edit-new').css({'visibility' : 'visible'});
                                    }
                            });
                    }
            });

            $('.remove').click(function() {
                    removeAttribute(this);
            });
            
            $('.edit-new').bind('click', function() {
                    //$(this).parents('.widget-main').find('[name="attribute_main_ebay"]').toggleClass('show-toggle');
                    if ( $(this).find('i').hasClass('fa-pencil') ) {
                            $(this).removeClass('btn-primary').addClass('btn-danger');
                            $(this).find('i').removeClass('fa-pencil').addClass('fa-eraser');
                            var widget = $(this).parents('.widget-main');
                            widget.find('input[name="attribute_main_ebay"]').val(widget.find('input[name="attribute_main"]').val()).removeAttr('readonly');
                            widget.find('.down-icon').css({'visibility': 'hidden'});
                    }
                    else {  
                            $(this).removeClass('btn-danger').addClass('btn-primary');
                            $(this).find('i').removeClass('fa-eraser').addClass('fa-pencil');
                            var t = $(this).parents('.widget-main').find('input[name="attribute_main_ebay"]').attr('data-value');
                            $(this).parents('.widget-main').find('input[name="attribute_main_ebay"]').val(t).attr('readonly', 'readonly');
                            $(this).parents('.widget-main').find('.down-icon').css({'visibility': 'visible'});
                    }
            });
            
            $('.attr-new').click(function() {
                    //$('.chzn-select').chosenDestroy('chzn-done');
                    var parent          = $(this).parents('.form-group');
                    var cloned          = parent.find('.attribute-task').filter('div:first').clone();
                    cloned.appendTo(parent).show();
                    var id_count        = parseInt($('#count').val());
                    var id_count_new    = id_count + 1;
                    var count_attr      = parseInt(parent.find('.attribute-task').filter('div:first').attr("rel"));
                    var count_attr_new  = count_attr + 1
                    $('#count').val(id_count_new);

                    parent.find('.attribute-task').filter('div:first').attr("rel", count_attr_new);
                    cloned.attr("rel", count_attr_new);
                    var removeClass     = cloned.find('.attr-new');
                    removeClass.removeClass('attr-new');
                    removeClass.addClass('remove');
                    removeClass.addClass('align-center');
                    removeClass.find('.icon-plus-sign').addClass('red icon-minus-sign bigger-100');
                    removeClass.find('.icon-plus-sign').removeClass('green icon-plus-sign bigger-110');

                    removeClass.show().click(function(){
                        $(this).parents('.attribute-task, .new-attribute-task').remove();
                        var id          = parseInt($('#count').val()) - 1 ;
                        $('#count').val(id);
                    });

                    selected_select     = cloned.find('select');
                    selected_select.select2();
                    //selected_select.addClass('chzn-select');
                    //$('.chzn-select').chosen();
                    selected_select.prop('disabled', false).trigger("liszt:updated");

                    $('select[name="attribute_main"] option:selected').each(function( index, option ) {
                            selected_select.find('option[value="' + option.value + '"]').remove();
                            selected_select.trigger("liszt:updated");
                    });
  
                    if ( !cloned.find('select[name="attribute_main"] option').length ) {
                            $.each($('input[name="attribute_main_ebay"]'), function(key, val) {
                                    if ( $(val).is(":visible") == true ) {
                                        $(val).parents('.widget-main').find('.chzn-container').last().toggle();
                                    } 
                            });
                            cloned.remove();
                            var id = parseInt($('#count').val()) - 1 ;
                            $('#count').val(id);
                            return(false) ;
                    }
                    
                    $.each($('input[name="attribute_main_ebay"]'), function(key, val) {
                            if ( $(val).is(":visible") == true ) {
                                $(val).parents('.widget-main').find('.chzn-container').last().toggle();
                            } 
                    });
                    
                    cloned.find('.edit-new').click(function() {
                            var parent          = $(this).parents('.widget-main').find('[name="attribute_main_ebay"]');
                            $(this).parents('.widget-main').find('.chzn-container').last().toggle();
                            $(this).parents('.widget-main').find('[name="attribute_main_ebay"]').toggle();
                    });
            });
            
            ////////////////////////////////////////////////////////
            $('.mapping-save').on('submit', function(e) {
                    $('#loading-result').show();
                    $("#success").text("").hide();
                    $("#error").text("").hide();
                    e.preventDefault();
                    var data_attribute                      = new Array();
                    var data_count                          = 0;
                    var data_count_minus                    = $('#tasks .row-fluid').length;
                    var data_no_empty                       = false;
                    $("div[class*='attribute-task']").each(function() {
                            data_count++;
                            var id_attribute_group          = $(this).find("input[name='attribute_main']").attr('rel');
                            var id_attribute                = $(this).find("input[name='attribute_main']").attr('data-key');
                            var attribute_name              = $(this).find("input[name='attribute_main']").val();
                            var attribute_name_ebay         = $(this).find('input[name="attribute_main_ebay"]').val();
                            if ( id_attribute !== "0" && attribute_name_ebay !== 'Choose eBay Attributes' ) {
                                    data_value              = {
                                        'id_attribute_group'    : id_attribute_group,
                                        'id_attribute'          : id_attribute,
                                        'name_attribute'        : attribute_name,
                                        'name_attribute_ebay'   : attribute_name_ebay,
                                    };
                                    data_attribute.push(data_value);
                            }
                    });
                    if ( data_attribute.length === 0 ) {
                            data_attribute                  = {
                                'empty'                     : true,
                            };
                    }

                    if ( data_attribute.length === (data_count - data_count_minus) ) {
                            data_no_empty                   = true;
                    }
                    console.log(data_attribute.length);
                    console.log(data_count - data_count_minus);
                    $('html, body').animate({ scrollTop: 0 }, 0);
                    $.ajax ({
                            type        : "POST",
                            url         : base_url + "ebay/attribute_ebay_value_save",
                            data        : { data_attribute : data_attribute, data_no_empty : data_no_empty },
                            error       : function(jqXHR, textStatus, errorThrown) {
                                        $('#loading-result').hide();
                                        $("#error").text("").show().append(errorThrown);
                            },
                            success     : function(result) {
                                        $('#loading-result').hide();
                                        console.log($.trim(result));
                                        if ( $.trim(result) == 'Success' ) {
                                                $('#loading-result').show();
                                                $(".error_mapping").hide();
                                                $("#success").text("").show().append('<button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button><i class="fa fa-check"></i> ' + "<strong>" + $.trim(result) +"</strong> : Update Attribute Value Success");
                                                $('.mapping-save').unbind('submit');
                                                $('.mapping-save').submit();
                                        }
                                        else if ( $.trim(result) == 'Clear' ) {
                                                $('#loading-result').hide();
                                                $(".error_mapping").hide();
                                                $("#success").text("").show().append('<button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button><i class="fa fa-check"></i> ' + "<strong>" + $.trim(result) +"</strong> : Clear Attribute Value Success");
                                        }
                                        else {     
                                                $("#error").text("").show().append('<button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button><i class="fa fa-exclamation-triangle"></i>'+" Update Attribute Value Failure");
                                        }
                            }
                    });
            });
            
            $("#attr-node").click(function(e) {
                    $('.mapping-save').unbind('submit');
                    $('.mapping-save').submit();
            });
            
            $("#reset_attributes").unbind('click');
            $("#reset_attributes").click(function(e) {
                    $('.attribute-task').each(function() {
                            $(this).find('input[name="attribute_main_ebay"]').attr({'value': 'Choose eBay Attributes', 'data-value': '0'});
                    }); 
            });
            
            var list_name;
            var first_click;
            var old_select;
            
            (function($) {
                    $.fn.enableCheckboxRangeSelection = function() {
                            var lastCheckbox = null;
                            var $spec = this;
                            $spec.unbind("click.checkboxrange");
                            $spec.bind("click.checkboxrange", function(e) {
                                    if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {
                                            var slice_spec = $spec.slice(
                                                    Math.min($spec.index(lastCheckbox), $spec.index(e.target)),
                                                    Math.max($spec.index(lastCheckbox), $spec.index(e.target)) + 1
                                            );
                                            $.each(slice_spec, function() {
                                                    if ( ($.trim($(this).parents('.form-group:first').find('.widget-main > h4').text()) === list_name) && !$(this).parent().find('i').hasClass("icon-eraser") ) {
                                                            $(this).addClass('selected_category');
                                                    } 
                                            });
                                    }
                                    lastCheckbox = e.target;
                            });
                    };
            })(jQuery);
			
			function select_click($this) {
                    $select                     = $this;
                    var attribute_task          = $select.parents('.attribute-task:first');
                                    list_name       = $.trim(attribute_task.parents('.form-group:first').find('.widget-main > h4').text());
                                    if ( typeof(first_click) === 'undefined') {
                                            first_click         = {
                                                    value       : attribute_task.find('.attribute_main').val(),
                                                    rel         : attribute_task.find('.attribute_main').attr('rel'),
                                            };
                                            old_select          = $select;
                                    }
                                    
                                    
                                            var control_group   = $select.parents('.form-group');
                                            var cloned          = $select.parents('.form-group').find('.attribute-task:first select[name="attribute_main_ebay"]:first').clone();

                                            cloned.removeAttr('disabled');
                                            var default_select  = $select.parents('.widget-main').find('.select');
                                            $select.parents('.widget-main').find('.select').hide().after(cloned);
                                            cloned.select2();
                                            $('.chosen-container > .chosen-single').css({'background' : '#F5FFFB', 'color' : '#16a085', 'border' : '1px solid #1abc9c'});

                                            cloned.on('change',function() {
                                                    $('.down-icon').css({'visibility': 'visible'});
                                                    var selected_value = this.value;
                                                    var selected_name = this.options[this.selectedIndex].innerHTML;
                                                    var category_list = $('.selected_category');
													$(this).parents('.widget-main').find('.attribute_main_val').show().val(selected_value).attr('data-value', selected_name).show();
                                                    $(this).select2('destroy').remove();
																	
                                                    list_name = 'SELECTED';
                                                    first_click = 'SELECTED';
                                                    $('.select').unbind('click');
                                                    $('.select').click(function() {
                                                            select_click($(this));   
                                                    });
                                            });
            }
            
            $('.select').css({'cursor' : 'pointer'});
            $('.select').unbind('click');
            $('.select').click(function() {
                     select_click($(this));   
            });
            
            $('.attribute_main_val').click(function(e) {
                    $this                       = $(this);
                                    list_name       = $.trim($(this).parents('.form-group:first').find('.widget-main > h4').text());
                                    if ( typeof(first_click) === 'undefined') {
                                            first_click         = {
                                                    value       : $(this).val(),
                                                    rel         : $(this).attr('rel'),
                                            };
                                            old_select          = $(this);
                                    }
                                    
                                    if (($('.attribute_main.selected_category').length === 1) && ($('.attribute_main.selected_category').parent().find('select').length === 0)) {
                                            var control_group   = $(this).parents('.form-group');
                                            var cloned          = $(this).parents('.form-group').find('.attribute-task:first select[name="attribute_main_ebay"]:first').clone();

                                            cloned.removeAttr('disabled');
                                            $(this).parents('.widget-main').find('.select').replaceWith(cloned);
                                            cloned.select2();
                                            $('.chosen-container > .chosen-single').css({'background' : '#F5FFFB', 'color' : '#16a085', 'border' : '1px solid #1abc9c'});
                                            $('.chosen-container').css({'width': '100%'});

                                            cloned.on('change',function() {
                                                    $('.down-icon').css({'visibility': 'visible'});
                                                    var selected_value = this.value;
                                                    var selected_name = this.options[this.selectedIndex].innerHTML;
                                                    var category_list = $('.selected_category');

                                                    $(category_list).each(function() {
                                                        $(this).removeClass('selected_category');
                                                        if ( $(this).parents('.widget-main').find('.select option').length > 0 ) {
                                                                $(this).parents('.widget-main').find('select').select2('destroy');
                                                                $(this).parents('.widget-main').find('.select').replaceWith(old_select);
                                                        }
                                                        $(this).parents('.widget-main').find('.select').val(selected_value).attr('data-value', selected_name);
                                                    });
													
                                                    $('.attribute_main').attr('style', 'width: 100%;');
                                                    list_name = undefined;
                                                    first_click = undefined;
                                                    
                                                    $('.select').unbind('click');
                                                    $('.select').click(function() {
                                                            select_click($(this));   
                                                    });
                                            });
                                    }
            });
			
			
			
			
			
			
			
			
			
			
			
			
            
            

            $('.attribute_main').enableCheckboxRangeSelection();
    });