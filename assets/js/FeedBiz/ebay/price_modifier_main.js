        
        var data                            = new Array();
        var wizard_data                     = new Array();
        var rules                           = new Object();
        var greater_message                 = $('input[name="validate-price-modifier-range"]').val();
        var between_message                 = $('input[name="validate-price-modifier-between"]').val();
        var positive_message                 = $('input[name="validate-price-modifier-positive"]').val();
        validation_number();
        set_validation();
        console.log("AAA");
        function check_validation() {
                var valid = true;
                var count = 1;
                $('.list-regular li.price-rule-main').each(function() {
                        if (  ($('.list-regular li.price-rule-main').length !== count) 
                           || ($('.list-regular li.price-rule-main').length !== 1) 
                           || ($('.list-regular li.price-rule-main').length === 1 && $('.list-regular li.price-rule-main').find('input[rel="from"]').valid() && $('.list-regular li.price-rule-main').find('input[rel="to"]').valid() && $('.list-regular li.price-rule-main').find('input[rel="value"]').valid() ) 
                           ) {
                                $(this).find('input.price_rule').each(function() {
                                        if ( valid !== true || $(this).valid() === false ) {
                                                valid = false;
                                        }
                                });
                                count++;
                        }
                        else if (  ($('.list-regular li.price-rule-main').length !== count) 
                           || ($('.list-regular li.price-rule-main').length !== 1) 
                           || (($('.list-regular li.price-rule-main').length === 1 && $('.list-regular li.price-rule-main').find('input[rel="from"]').valid()) || $('.list-regular li.price-rule-main').find('input[rel="from"]').val() < 0 ) 
                           ) {
                                $(this).find('input.price_rule').each(function() {
                                        if ( valid !== true || $(this).valid() === false ) {
                                                valid = false;
                                        }
                                });
                                count++;
                        }
                });
                if ( valid ) {
                        $('label.error').remove();
                        $('.input-group.group-error').removeClass('group-error');
                }
                return valid;
        }
        
        function validation_number() {
                $(".price_rule").unbind('keyup');
                $(".price_rule").on("keyup", function(){
                        var time = 0;
                        var e = window.event || e;
                        val = this.value;
                        var keyUnicode = e.charCode || e.keyCode;
                        var valid = /^\-?[0-9][0-9]*(\.\d{0,2})?$/.test(this.value)
                        if(!valid){
                            console.log("Invalid input!");
                            this.value = val.substring(0, val.length - 1);
                        }
                        if (e !== undefined) {
                                switch (keyUnicode) {
                                        case 16: break; // Shift
                                        case 17: break; // Ctrl
                                        case 18: break; // Alt
                                        case 27: this.value = ''; break; // Esc: clear entry
                                        case 35: break; // End
                                        case 36: break; // Home
                                        case 37: break; // cursor left
                                        case 38: break; // cursor up
                                        case 39: break; // cursor right
                                        case 40: break; // cursor down
                                        case 78: break; // N (Opera 9.63+ maps the "." from the number key section to the "N" key too!) (See: http://unixpapa.com/js/key.html search for ". Del")
                                        case 110: break; // . number block (Opera 9.63+ maps the "." from the number block to the "N" key (78) !!!)
                                        case 190: break; // .
                                        default: $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1, eventOnDecimalsEntered: true,symbol:'',groupDigits:false });
                                }
                        }
                });
        }
        
        
        function set_validation() {
                $("input.price_rule").each(function() {
                        if ( $(this).attr('rel') === 'to' ) {
                                rules[this.name] = { required: true, both: true, between: true, greater: true, positive: true };
                        }
                        else if ( $(this).attr('rel') === 'from' ) {
                                rules[this.name] = { required: true, both: true, between: true, positive: true };
                        }
                        else {
                                rules[this.name] = { required: true, both: true };
                        }
                });
                
                $.validator.addMethod("both", function(value, element) {
//                        $(element).parents('.price-rule-main:first').find('input').valid();
                        return this.optional(element) || value !== '' && $(element).parents('.price-rule-main:first').find('input[rel="from"]').val() >= 0 && $(element).parents('.price-rule-main:first').find('input[rel="to"]').val() >= 0;
                        }, 
                        $('input[name="validate-price-modifier-required"]').val()
                );
        
                $.validator.addMethod(
                        "requiredLine",
                        function(value, element) {
                            var fromVal = $(element).closest('li').find('input[rel="from"]').val();
                            var toVal = $(element).closest('li').find('input[rel="to"]').val();
                            var valueVal = $(element).closest('li').find('input[rel="value"]').val();
                            return (fromVal == '' && toVal == '' && valueVal == '') || ($(element).val() != '');
                        },
                        $('input[name="validate-price-modifier-required"]').val()
                );
                
                $.validator.addMethod(
                        "greater",
                        function(value, element) {
                            var validator = this;
                            var from = $(element).parents('.price-rule-main:first').find('input[rel="from"]').val();
                            greater_message = $('input[name="validate-price-modifier-range"]').val().replace('{1}', from);
                            $.validator.messages.greater = greater_message;
                            return this.optional(element) || parseFloat(value) > parseFloat(from);
                        },
                        greater_message
                );
                
                $.validator.addMethod(
                        "positive",
                        function(value, element) {
                            var validator = this;
                            var from = $(element).parents('.price-rule-main:first').find('input[rel="from"]').val();
                            positive_message = $('input[name="validate-price-modifier-positive"]');
                            $.validator.messages.greater = positive_message;
                            return parseFloat(value) >= 0;
                        },
                        positive_message
                );
        
                $.validator.addMethod(
                        "between",
                        function(value, element) {
                            var rel = $(element).parents('.price-rule-main:first').attr('rel');
                            var ul = $(element).parents('.list-regular:first');
                            var check_all = true;
                            $(ul).find('.price-rule-main').each(function() {
                                    if ( $(this).attr('rel') !== rel ) {
                                            var from    = $(this).find('input[rel="from"]').val();
                                            var to      = $(this).find('input[rel="to"]').val();
                                            if ( parseFloat(from) <= value && value <= parseFloat(to) ) {
                                                    check_all = false;
                                                    between_message = $('input[name="validate-price-modifier-between"]').val().replace('{1}', from).replace('{2}', to);
                                                    $.validator.messages.between = between_message;
                                            }
                                    }
                            });
                            return this.optional(element) || check_all;
                        },
                        between_message
                );
        }
        
        $("#form-submit").validate({
//        		ignore: '*:not([name])', 
                rules: rules,
                errorPlacement: function(error, element) {
                        $(element).addClass('error');
                        error.insertAfter( $(element) );
                },
                unhighlight: function (element) {
                        $(element).removeClass('error');
                },
                debug: true
        });
        
        $('.addPriceRule').click(function(){
            addPriceRule($(this));
        }); 
        
        $('select[rel="type"]').unbind('change').on('change', function(){
                changePriceRuleType($(this));
        }); 
        
        $('select[rel="type"]').each(function(){
                changePriceRuleType($(this));
        }); 
        
        $('.removePriceRule').unbind('click').click(function() {
                $(this).parents('.price-rule-li:first').remove();
        });
        
        function clearLabelError(obj) {
                var obj     = $(obj);
                obj.find('label.error').remove();
        }
        
        function checkPriceValid(obj) {
                var valid   = true;
                var obj     = $(obj);
                var from    = obj.find('input[rel="from"]').val();
                var to    = obj.find('input[rel="to"]').val();
                obj.find('input.price_rule').each(function() {
                        if ( valid !== true || $(this).valid() === false ) {
                                valid = false;
                        }
                });
                return valid;
        }
          
        
        function addPriceRule(obj){
            var ul = obj.parents('.list-regular:first');
            var li = obj.parents('.price-rule-li:first');

            //Check value
            if(checkPriceValid(li))
            {
                var count =  ul.parent().find('input[rel="price_rule_count"]');
                ul.find('select.price_rule').select2("destroy");
                
                var cloned = li.clone().insertAfter(obj.parents('.price-rule-li:last'));

                li.find('input[rel="to"]').on('blur',
                function(){
                    checkPriceValid(li);
                });            
                li.find('input.price_rule').on('keydown', function(){
                    clearLabelError($(this));
                }); 
                li.find('.addPriceRule').remove();
                li.find('.removePriceRule').show().click(
                function(){
                    li.remove();
                });  
            

                cloned.find('select[rel="type"]').val(li.find('select[rel="type"]').val());
                cloned.find('.addPriceRule').click(
                function(){
                    addPriceRule($(this));
                });
                cloned.find('input[rel="from"]').val(parseFloat(li.find('input[rel="to"]').val()) + 1);
                cloned.find('input[rel="to"]').val('').focus();     
                cloned.find('input[rel="value"]').on('blur',
                function(){
                    checkPriceValid(cloned);
                }).val('');
                
                cloned.find('select[rel="type"]').unbind('change').on('change', function(){
                        changePriceRuleType($(this));
                }); 
                
                ul.find('select.price_rule').select2();
                var new_count   = parseInt(count.val()) + 1;
                count.val( new_count );
                cloned.attr('rel', new_count );
                cloned.find('input.price_rule').each(function() {
                        var variable_prefix = 'price_rules[value]['+ new_count +'][' + $(this).attr('rel') + ']' ;
                        var id_prefix = 'profile_price_rules_value_' + new_count + '_' + $(this).attr('rel') ;
                        $(this).attr('name', variable_prefix) ;
                        $(this).attr('id', id_prefix) ;                
                        $(this).attr('data-row', new_count) ;    
                        if ( $(this).attr('rel') === 'to' ) {
                                $(this).rules('add', { required: true, both: true, between: true, greater: true });
                        }
                        else if ( $(this).attr('rel') === 'from' ) {
                                $(this).rules('add', { required: true, both: true, between: true });
                        }
                        else {
                                $(this).rules('add', { required: true, both: true });
                        }
                });
    
                validation_number();
            }
        }
        
        function changePriceRuleType(obj) {
                var type = $(obj).val();
                if(type == "percent"){
                        $(obj).parents('.price-rule-li:first').find('input[rel="value"]').next().html('%');
                } else if(type == "value") {
                        $(obj).parents('.price-rule-li:first').find('input[rel="value"]').next().html($('.currency-symbol').val());
                }
        }
        
        $("#form-submit").on('submit', function(e) {
                var dataChecked;
                data                        = [];
                wizard_data                 = [];

                $('input[name="form-price-rounding"]').each(function() {
                        if ( $(this).is(":checked") ) {
                                dataChecked = $(this).val();
                        }
                });

                $("li.price-rule-main").each(function() {
                        if (  $(this).find('input[rel="from"]').val().length
                           && $(this).find('input[rel="to"]').val().length
                           && $(this).find('input[rel="value"]').val().length
                           ) {
                                data_selected = {
                                    'id_profile'    : $('.id_profile').val(),
                                    'price_type'    : $(this).find('select[rel="type"]').val(),
                                    'price_from'    : $(this).find('input[rel="from"]').val(),
                                    'price_to'      : $(this).find('input[rel="to"]').val(),
                                    'price_value'   : $(this).find('input[rel="value"]').val(),
                                    'rounding'      : dataChecked,
                                    'rel'           : $(this).attr('rel'),
                                    'id_site'       : $("#id-site").val(),
                                    'id_packages'   : $("#id-packages").val(),
                                    'tab'           : 1,
                                    'step'          : $("#popup-step").val(),
                                };

                                data.push(data_selected);
                        }
                });
                
                if ($.isEmptyObject(data)) {
                        data_value                  = {
                                'id_profile'        : $('.id_profile').val(),
                                'id_site'           : $("#id-site").val(),
                                'id_packages'       : $("#id-packages").val(),
                                'tab'               : 1,
                                'step'              : $("#popup-step").val(),
                        };
                        data.push(data_value);
                }
                
                var data_selected = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'id_popup'      : $("#popup-step").val(),
                };
                wizard_data.push(data_selected);
                
                if ( check_validation() ) {
                        ajax_save(e);
                }
        });