
    var data                            = new Array();
    var cloned                          = $("div.close").clone();
    var page                            = $("#current-page").val();
    var txt_show						= '+';//'['+$("input:hidden[name='show']").val()+']';
    var txt_hide						= '-';//'['+$("input:hidden[name='hide']").val()+']';
    var txt_sku							= $("input:hidden[name='sku']").val();
    var txt_product_sku = $("input:hidden[name='product_sku']").val();
    var txt_product_name = $("input:hidden[name='product_name']").val();
    var txt_combination_sku = $("input:hidden[name='varian_sku']").val();
    var txt_message = $("input:hidden[name='message']").val();
    var txt_batch_id = $("input:hidden[name='batch_id']").val();
    var txt_cause = $("input:hidden[name='cause']").val();
    var txt_solution = $("input:hidden[name='solution']").val();
    var txt_help = '[' + $("input:hidden[name='help']").val() + ']';
    var txt_solved = $("input:hidden[name='solved']").val();
    
    var offset  = 0;
    var limit   = 10;
    var first   = 0;
    var ColVis_first = 0;
    var showChar = 80;
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    var oTable = '';
    
$(document).ready(function() {

    function morelink(ele) {
            $(ele).click(function(e){
                e.preventDefault();
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
    }
    
    oTable = $('#statistics').DataTable( {
        "processing": true,
        "serverSide": true,
	    "fnPreDrawCallback" : function( oSettings ) {
	    	limit   = oSettings._iDisplayLength;
            offset  = oSettings._iDisplayStart;
            aSorting                = [];
            aSorting.column         = oSettings.aaSorting[0][0];
            aSorting.by             = oSettings.aaSorting[0][1];
            oSettings._iDisplayStart    = offset;
            oSettings.iDraw             = Math.floor(oSettings._iDisplayStart / limit);
	    	oSettings.ajax              = base_url + "ebay/error_resolutions_processing?limit=" + limit + '&offset=' + offset + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val();
	    },
        "columns": [
            { "data": null, "bSortable": false, "sClass": "center",
            	render : function(){
            		return '<a class="data_control plus" href="javascript:return false;" onclick="addRowProducts(this);" >'+txt_show+'</a>';
            	}
            },
            { "data": "code", "bSortable": false, "sClass": "center" },
            { "data": "message", "bSortable": false },
//            { "data": "cnt", "bSortable": false, "sClass": "center" },
        ]
    } );
    $('.dataTables_filter input').attr("placeholder", txt_batch_id);
});


    var PREFIX_TR_ERROR_CONTENT = 'tr-error-content';
    var PREFIX_TABLE_PRODUCT_CONTENT = 'table_product_content';
    function addRowProducts(button){
		var tr = $(button).closest('tr');
    	var selected_code = $('td:eq(1)', tr).html();
    	var tr_error_content_id = PREFIX_TR_ERROR_CONTENT+selected_code;
    		
    	if($(button).html() == txt_show){
    		$(button).html(txt_hide).removeClass('plus').addClass('minus');
    		
    		if($('.'+tr_error_content_id).length == 0){
    			$.ajax({
    				url : base_url + "ebay/error_resolution_content?code="+selected_code,
    				dataType: 'json',
    				success: function(data){
    					if(data != '' && data != undefined){
    						box_solution = '<div class="error_details"><b> '+txt_cause+' :</b>'+$('<span/>').html(data.error_details).text()+'<b>'+txt_solution+' :</b> '+$('<span/>').html(data.error_resolution).text()+'</div>';
    						$(box_solution).prependTo('.'+tr_error_content_id+' div.tabboder');
    					}
    				},
    				error: function(jqXHR, textStatus, errorThrown){
    					console.log(jqXHR);
    				}
    			});
    			
        		var tr = $('<tr class="'+tr_error_content_id+'"><td colspan="5"><div class="tabboder"><table id="'+PREFIX_TABLE_PRODUCT_CONTENT+selected_code+'" class="table-error-products" code="'+selected_code+'"><thead><tr><th>'+txt_solved+'</th><th>'+txt_product_sku+'</th><th>'+txt_product_name+'</th><th>'+txt_combination_sku+'</th><th>'+txt_message+'</th></tr></thead><tbody></tbody></table></div></td></tr>')
        		.insertAfter(tr);
        		var product_table = $('table', tr).addClass('dynamicTable colVis table');
        		loadProducts(product_table);
    		}
    		else{
    			$('.'+tr_error_content_id).removeClass('hide');
    		}
    	}
    	else{
    		$(button).html(txt_show).removeClass('minus').addClass('plus');;
    		$('tr.'+tr_error_content_id).addClass('hide');
    	}
    }

    function loadProducts(table){
    	var errorCode = $(table).attr('code');
    	var batch_id = $('#statistics_filter input').val();
    	
    	$(table).DataTable( {
            "processing": true,
            "serverSide": true,
    	    "fnPreDrawCallback" : function( oSettings ) {
    	    	limit   = oSettings._iDisplayLength;
                offset  = oSettings._iDisplayStart;
                aSorting                = [];
                aSorting.column         = oSettings.aaSorting[0][0];
                aSorting.by             = oSettings.aaSorting[0][1];
                oSettings._iDisplayStart    = offset;
                oSettings.iDraw             = Math.floor(oSettings._iDisplayStart / limit);
    	    	oSettings.ajax              = base_url + "ebay/error_resolutions_products_processing?batch_id=" + batch_id + "&code=" + errorCode + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val() + '&limit=' + limit + '&offset=' + offset;
    	    },
            "columns": [
	            { "data": "ProductID", render : function(data, type, row){
	            	var checked = row['Resolved'] == 1 ? ' checked="checked" ' : '';
	        		return '<label class="cb-checkbox"><input type="checkbox" batch="'+batch_id+'" code="'+errorCode+'" product="'+row['ProductID']+'" '+checked+' combination="'+row['CombinationID']+'" '+checked+'/></label>';
	        	}},
	            { "data": "ProductSKU", "bSortable": false, "sClass": "left" },
	            { "data": "ProductName", "bSortable": false, "sClass": "left" },
	            { "data": "CombinationSKU", "bSortable": false, "sClass": "left" },
	            { "data": "Message", "bSortable": false, "sClass": "left" },
	        ],
	        "fnDrawCallback": function(oSettings) {
                $('body').checkBo();
            },
            "fnCreatedRow" : function( nRow, aData, iDisplayIndex ) { 
	        	if($(nRow).find('input:checkbox').length > 0){
	        		$(nRow).addClass('product-row');
	        	}    	            	
	        	if(aData.combinations != null){
	        		$.each(aData.combinations, function(key, value){
	        			 $(table).DataTable().row.add(value);
	        		});
	        	}
	        },
        } );
    	
        $('<button type="button" class="btn btn-save m-b0 m-l5 edit_error pull-right" onclick="override_product_attribute(this);">Edit Attribute</button>').insertAfter('#'+$(table).attr('id')+'_wrapper .dataTables_length');
    }
    
//    function toggleSolved(checkbox){
//    	var batch_id = $(checkbox).attr('batch');
//    	var error_code = $(checkbox).attr('code');
//    	var product = $(checkbox).attr('product');
//    	var value = $(checkbox).is(':checked') ? 'solved' : 'unsolved';
//    	$.ajax({
//    		url: base_url + "ebay/error_resolutions_products_processing?batch_id=" + batch_id + "&code=" + error_code + '&resolved=' + value + '&product=' + product + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val(),
//    		dataType: 'json',
//    		success: function(data){
//    			$('span.resolved_cnt[batch="'+data.batch_id+'"][code="'+data.error_code+'"]').html(data.resolved_count);
//    		},
//    		error: function(){
//    			console.log('Cannot load data.');
//    		}
//    	});
//    }
    
    function show_solution(link){
    	if($(link).parent().find('.show_solution').is(':visible')){
    		$(link).parent().find('.show_solution').addClass('hide');
    		$(link).html(txt_help);
    	}
    	else{
    		$(link).parent().find('.show_solution').removeClass('hide');
    		$(link).html(txt_hide);
    	}
    }
    
    var rendered = {};
    function override_product_attribute(button){
    	var data = []
    	$.each($(button).closest('.dataTables_wrapper').find('input:checked'), function(i, v){
    		var line = {};
    		line['product'] = $(v).attr('product')
    		line['combination'] = $(v).attr('combination')
    		data.push(line);
    	});
    	$.ajax({
    		url: base_url+'ebay/override_product_attribute/'+$('#id-packages').val(),
    		data: {products: data},
    		success: function(html){
    	    	$.colorbox({ 
    	            html: html, 
    	            innerWidth:'70%', 
    	            innerHeight:'80%',
    	            width:'90%', 
    	            height:'80%',
    	            escKey: true, //true
    	            overlayClose: false, //true
    	            onLoad: function() {
    	                $('body').css('overflow-y','hidden');
    	            },
    	            onClosed:function(){
    	                $('body').css('overflow-y','auto');
    	            },
    	        });
    	    	    	    	    	    	
    	    	$.each(override_datasource, function(productID, productObject){
    	    		$.each(productObject, function(combinationID, combinationObject){
    	    			if(combinationObject.override != undefined){
    	    				$.each(combinationObject.override, function(index, overrideObject){
    	    					addOverride(productID, combinationID, overrideObject.attribute_field, overrideObject.override_value);
    	    				});
    	    			}
    	    			else{
    	    				addOverride(productID, combinationID, 0, '');
    	    			}
    	    		});
    	    	});
    		}
    	});  
    }
    
    function addOverride(product, combination, key, value){
    	//all attributes are overrided
    	if($('.body-override[product="'+product+'"][combination="'+combination+'"] select').length == ($('#row_template').find('option').length - 1)){
    		return false;
    	}
    	
    	//clone template row
    	var cloned = $('#row_template').clone();
    	$(cloned).attr('id', '').removeClass('hide').appendTo('.body-override[product="'+product+'"][combination="'+combination+'"]');
    	$(cloned).find('select').val(key);
    	$(cloned).find('input:text').val(value);
    	$('.body-override[product="'+product+'"][combination="'+combination+'"] select').each(function(index, select){
    		$(cloned).find('select option[value="'+$(select).val()+'"]').remove();
    	});
    	
    	$('.dd-ebay-attribute', cloned).chosen();
    	
    	if($('.body-override[product="'+product+'"][combination="'+combination+'"] select').length == 1){
    		$('i.addSpecificField', cloned).attr('onclick', "addOverride("+product+", "+combination+", '', '')");
    	}
    	else{
    		$('i.addSpecificField', cloned).removeClass('cb-plus').addClass('cb-minus').attr('onclick', "$(this).closest('.attribute-row').remove();");
    	}
    	
    	if($(cloned).find('select').val() == null ||
    			$(cloned).find('select').val() == undefined ||
    			$(cloned).find('select').val() == ''){
    		var value = $(cloned).find("select option:first").val();
    		$(cloned).find('select').val(value);
    		$(cloned).find('select').trigger("chosen:updated");
    	}
    }
    
    function save_override(){
    	$('.body-override').each(function(){
    		var productID = $(this).attr('product');
    		var combinationID = $(this).attr('combination');
    	});
    }