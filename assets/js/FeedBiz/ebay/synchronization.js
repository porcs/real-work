var cloned = $("div.close").clone();
$(document).ready(function() {           
        
        $(".btn-synchronization").on("click", function() {
                var $obj = $('#synchronization-page-load');
                $obj.show();
                showWaiting(true);
                var time =0;
                var num = $('body').find('input').length;
                if(num > 2){
                        time = 200;
                } 
                setTimeout(function(){
                        $.ajax({
                                type        : "POST",
                                url         : base_url + "tasks/run_export_ebay_by_node",
                                data        : { siteid : $("#id-site").val(), method : "synchronization", link : "products"},
                                timeout     : 0,
                                success     : function(result) {
                                            $obj.hide();
                                            console.log($.trim(result));
                                            showWaiting(false);
                                            if ( $.trim(result) == 'Success' ) {
                                                    $("#success").text("").show().append(cloned.css('visibility', 'visible').find('i:last').removeClass().addClass('fa fa-check-circle').parents('div.close').find('i').css('visibility', 'visible').parent().html() + " " + $(".update-synchronization-success").val());
                                                    location.href   = $('#form-submit').attr('action');
                                            }
                                            else {
                                                    $("#error").text("").show().append(cloned.css('visibility', 'visible').find('i:last').removeClass().addClass('fa fa-times-circle').parents('div.close').find('i').css('visibility', 'visible').parent().html() + " " + $(".update-synchronization-fail").val());
                                            }
                                            dismiss($("#error"));
                                },
                                error       : function(){
                                    $("#error").text("").show().append(cloned.css('visibility', 'visible').find('i:last').removeClass().addClass('fa fa-times-circle').parents('div.close').find('i').css('visibility', 'visible').parent().html() + " " + $(".update-synchronization-fail").val());
                                    dismiss($("#error"));
                                    $obj.hide();
                                    showWaiting(false);
                                }
                        });
                        setTimeout(function(){showWaiting(false);}, 15000);
                },time);
        });
});
    