        
        function ajax_save(e) {
                var page = $("#current-page").val();
                e.preventDefault();
                
                $.ajax ({
                        type        : "POST",
                        url         : base_url + "ebay/" + $('#ajax-url').val(),
                        data        : { data : data },
                        error       : function(jqXHR, textStatus, errorThrown) {
                                    notification_alert(errorThrown, 'bad');
                        },
                        success     : function(results) {
                                    $('#loading-result').hide();
                                    if ( $.trim(results) === 'Success' ) {  
                                            if ( typeof(module) !== 'undefined' && module === 1 ) {
                                                    wizard_start();
                                            }
                                            else {
                                                    notification_alert($(".update-" + page + "-success").val() );
                                                    setTimeout(function(){
                                                        location.href   = $('#form-submit').attr('action');
                                                    }, 1200);
                                            }
                                    }
                                    else {    
                                            notification_alert($(".update-" + page + "-fail").val(), 'bad');
                                    }
                        }
                });
        }