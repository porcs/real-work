        var page = $("#current-page").val();
        
        function wizard_start() {
                $.ajax ({
                        type        : "POST",
                        url         : base_url + "ebay/ebay_wizard_start",
                        data        : { data : wizard_data },
                        success     : function(start) {
                                    if ( start === 'Success' ) {
                                            notification_alert($(".update-" + page + "-success").val() );
                                            setTimeout(function(){
                                                location.href   = $('#form-submit').attr('action');
                                            }, 1200);
                                    }
                        }
                });
        }
        
        function export_from_wizard() {
                $.ajax({
                        type        : "POST",
                        url         : base_url + "ebay/export_from_wizard",
                        data        : { data : from_wizard_data },
                        success     : function(result) {
                                    console.log($.trim(result));
                                    setTimeout(function(){
                                        location.href   = $('#form-submit').attr('action');
                                    }, 1200);
                        },
                });
        }