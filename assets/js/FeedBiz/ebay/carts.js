        
        var data                            = new Array();
        var cloned                          = $("div.close").clone();
        var offset  = 0;
        var limit   = 10;
        var first   = 0;
        var ColVis_first = 0;
        var showChar = 80;
        var ellipsestext = '...';
        var moretext = "more";
        var lesstext = "less";
        function morelink(ele) {
            $(ele).click(function(e){
                e.preventDefault();
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
    }
    console.log('0');    
        var oTable = $('#statistics').dataTable( {
            "columnDefs": [
            { "visible": false,  "targets": 0 },
            { "visible": true},
            { "visible": true},
            { "visible": true},
            //{ "visible": true,},
        ],
        "language": {
            "emptyTable":     $('#emptyTable').val(),
            "info":           $('#info').val(),
            "infoEmpty":      $('#infoEmpty').val(),
            "infoFiltered":   $('#infoFiltered').val(),
            "infoPostFix":    $('#infoPostFix').val(),
            "thousands":      $('#thousands').val(),
            "lengthMenu":     $('#lengthMenu').val(),
            "loadingRecords": $('#loadingRecords').val(),
            "processing":     $('#processing').val(),
            "search":         '',
            "zeroRecords":    $('#zeroRecords').val(),
            "paginate": {
                "first":      $('#paginate-first').val(),
                "last":       $('#paginate-last').val(),
                "next":       $('#paginate-next').val(),
                "previous":   $('#paginate-previous').val()
            },
            "aria": {
                "sortAscending":  $('#aria-sortAscending').val(),
                "sortDescending": $('#aria-sortDescending').val()
            }
        },        
        "asStripeClasses": [ '' ],
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "ajax": {
            "url": base_url + "ebay/carts_processing?limit=" + limit + '&offset=' + offset + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val(),
        },
        "columns": [ 
            { "data": "mp_order_id", "bSortable": false, }, 
            { "data": "reference", "bSortable": false, "class":"p-l20" , "width" : "40%"},  
            { "data": null, "bSortable": false, "class":"p-l10 text-center" , "width" : "15%",
                render: function (data) {
                    return '';
                }
            },          
            { "data": "quantity", "bSortable": false, "class":"p-r10 text-right" , "width" : "15%"},            
            { "data": "timestamp", "bSortable": false, "class":"p-r10 text-right" , "width" : "20%"},            
        ],      
        "bFilter" : true,
        "paging": true,
        "pagingType": "full_numbers" ,     
        "drawCallback": function ( ) {

            var api = this.api();
            var rows = api.rows().nodes();
            var last = null;
            
            if (first == 0) {
                    $('#statistics_wrapper')
                            .append("<div class='row'><div class='col-xs-4'></div><div class='col-xs-8'></div></div>");
                    $('.dataTables_paginate')
                            .appendTo("#statistics_wrapper > .row > .col-xs-8:last");
                    $('#statistics_wrapper')
                            .prepend("<div class='row separator bottom'><div class='col-md-6 form-horizontal'></div><div class='col-md-6'></div></div>");
                    $('.row.separator.bottom').find('.col-md-6:first').append('<div class="dataTables_header"><label class="control-label">'+$('.title-orders').val()+'</label></div>');
                    $('.dataTables_info')
                            .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last");
                    first++;
            }
            api.column(0, {page:'current'} ).data().each( function ( group, i ) {

                var row_data = api.rows(i).data();
                var order_status = null;
                $(row_data).each(function(key,value){
                    order_status = (value.status);
                });

                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group">'+                            
                            '<td colspan="1" class="text-uc dark-gray montserrat">'+
                                '<b>'+ $("#OrderIDRef").val() + " : " + group+'</b>'+
                            '</td>'+
                            '<td colspan="1" class="text-center">'+
                                '<span class="order_status badge-info">' + $('#'+order_status).val() + '</span>' +
                            '</td>'+ 
                            '<td colspan="1"></td>'+
                            '<td colspan="1"></td>'+
                        '</tr>'
                    );
                    last = group;  
                }

            });   

        },
        } );
        
        $('input').on('keyup', function() {
                oTable.fnDraw();
        });
