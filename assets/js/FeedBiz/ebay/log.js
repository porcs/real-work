        
        var data                            = new Array();
        var cloned                          = $("div.close").clone();
        var offset  = 0;
        var limit   = 10;
        var first   = 0;
        var ColVis_first = 0;
        var oTable = $('#statistics').dataTable( {
            "processing": true,
            "serverSide": true,
            "searching": true,
            "bStateSave": true,
            "sDom": 'C<"clear">lfrtip',
            "oColVis": {
                aiExclude: [0, 8],
//                bRestore: true,
//                activate: "mouseover",
                bShowAll: $('.show-all').val(),
            },
            "ajax": base_url + "ebay/log_processing?limit=" + limit + '&offset=' + offset + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val(),

            fnPreDrawCallback : function( oSettings ) {
                    $('#statistics_processing').hide();
                    limit   = oSettings._iDisplayLength;
                    offset  = oSettings._iDisplayStart;
                    if ( offset % limit > 0 ) {
                            offset = Math.floor(offset / limit);
                    }
                    var batch_id            = $('input[rel="batch_id"]').val();
                    var source              = $('input[rel="source"]').val();
                    var type                = $('input[rel="type"]').val();
                    var total               = $('input[rel="total"]').val();
                    var success             = $('input[rel="success"]').val();
                    var warning             = $('input[rel="warning"]').val();
                    var error               = $('input[rel="error"]').val();
                    var date_add            = $('input[rel="date_add"]').val();
                    var all                 = $('.dataTables_filter input').val();
                    
                    aSorting                = [];
                    aSorting.column         = oSettings.aaSorting[0][0];
                    aSorting.by             = oSettings.aaSorting[0][1];
                    dataFilter = {
                        'batch_id'      : batch_id,
                        'source'        : source,
                        'type'          : type,
                        'total'         : total,
                        'success'       : success,
                        'warning'       : warning,
                        'error'         : error,
                        'date_add'      : date_add,
                        'sort_name'     : aSorting.column,
                        'sort_by'       : aSorting.by,
                        'all'           : all,
                    };
                    var count = 0
                    $.each(dataFilter, function(index, val) {
                            if ( count === aSorting.column) {
                                    dataFilter.sort_name        = index;
                            }
                            count++;
                    });
                    oSettings._iDisplayStart    = offset;
                    oSettings.iDraw             = Math.floor(oSettings._iDisplayStart / limit);
                    oSettings.ajax              = base_url + "ebay/log_processing?limit=" + limit + '&offset=' + offset + '&' + $.param(dataFilter) + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val();
                    
                    if (first == 0) {
                            $('#statistics_wrapper')
                                    .append("<div class='row'><div class='col-xs-4'></div><div class='col-xs-8'></div></div>");
                            $('.dataTables_paginate')
                                    .appendTo("#statistics_wrapper > .row > .col-xs-8:last");
                            $('#statistics_wrapper')
                                    .prepend("<div class='row separator bottom'><div class='col-md-6 form-horizontal'></div><div class='col-md-6'></div></div>");
                            $('#statistics_length')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length')
                                    .append("<div class='col-xs-6 col-md-6'></div>")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length > label > select')
                                    .addClass('.selectpicker')
                                    .appendTo("#statistics_length > .col-xs-6.col-md-6")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length > label:first')
                                    .addClass('col-xs-2 col-md-2 control-label')
                                    .text('Per page');
                            $('.row.separator.bottom').find('.col-md-6:first').append('<div class="dataTables_header"><label class="control-label">'+$('.title-log').val()+'</label></div>');
                            $('#statistics_filter')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6.form-horizontal:first")
                                    .parents("#statistics_wrapper")
                                    .find("#statistics_filter")
                                    .append("<div class='col-md-8'></div>")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_filter > label > input')
                                    .addClass('form-control').attr('placeholder', $('.hidden-search').val())
                                    .appendTo("#statistics_filter > .col-md-8")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_filter > label:first')
                                    .addClass('col-md-6 control-label').hide();
                            $('.ColVis')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last");
                            $('.dataTables_info')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last");
                            first++;
                            $(".prev a").each(function() {
                                        $(this).text($(this).text().replace("← ", ''));
                            });
                            $(".next a").each(function() {
                                        $(this).text($(this).text().replace(" → ", ''));
                            });
                    }
            },
            fnDrawCallback: function( oSettings ) {
                    if ( ColVis_first === 0 ) {
                            $('.ColVis_Button').trigger('click');
                            $('.ColVis_collection').find('li').each(function() {
                                    var $li                 = $(this);
                                    if ( $(this).find('.cb-checkbox').length === 0 ) {
                                            if ( $(this).find('span').length > 0 ) {
                                                    if ( $(this).find('input').prop('checked') === true ) {
                                                            $(this).find('label').addClass('cb-checkbox cb-sm checked');
                                                    }
                                                    else {
                                                            $(this).find('label').addClass('cb-checkbox cb-sm');
                                                    }
                                                    $(this).checkBo();
                                                    var checkbox = $(this);
                                                    $(this).find('.cb-checkbox').unbind('click');
                                                    $(this).find('i').unbind('click').on('click', function(e) {
                                                            if ( e.target.tagName.toString() === 'INPUT') {
                                                                    return false;
                                                            }
                                                            e.preventDefault();
                                                            $(this).parents('li').trigger('click');
                                                    });
                                                    $(this).find('span:last').on('click', function(e) {
                                                            e.preventDefault();
                                                            $(this).parents('li').trigger('click');
                                                    });
                                                    $(this).on('click', function(e) {
                                                            if ( $(this).find('input').is(':checked') ) {
                                                                    $(this).find('label.cb-checkbox').addClass('checked');
                                                            }
                                                            else if ( $(this).find('input').is(':checked') === false ) {
                                                                    $(this).find('label.cb-checkbox').removeClass('checked');
                                                            }
                                                    });

                                                    setTimeout(function() {
//                                                            var settings = oTable.settings();
//                                                            if ( settings[0].oInstance[0].clientWidth < 1800 ) {
//                                                                    if ( $.inArray(column[$.trim($li.find('span:last').text())], settings[0].oInit.oColVis.aiExclude) === -1 && $li.find('input').is(':checked') ) {
//                                                                            $li.trigger('click');
//                                                                    }
//                                                            }
//                                                            else if ( settings[0].oInstance[0].clientWidth >= 1800  ) {
//                                                                    if ( $.inArray(column[$.trim($li.find('span:last').text())], settings[0].oInit.oColVis.aiExclude) === -1 && $li.find('input').is(':checked') === false ) {
//                                                                            $li.trigger('click');
//                                                                    }
//                                                            }
                                                            if ( oTable.width() < 1200 ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.type').val() && $li.find('input').is(':checked') ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            else if ( oTable.width() >= 1200  ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.type').val() && $li.find('input').is(':checked') === false ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            if ( oTable.width() < 1000 ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.source').val() && $li.find('input').is(':checked') ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            else if ( oTable.width() >= 1000  ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.source').val() && $li.find('input').is(':checked') === false ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            if ( oTable.width() < 800 ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.export_date').val() && $li.find('input').is(':checked') ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            else if ( oTable.width() >= 800  ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.export_date').val() && $li.find('input').is(':checked') === false ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            if ( oTable.width() < 700 ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.message').val() && $li.find('input').is(':checked') ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            else if ( oTable.width() >= 700  ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.message').val() && $li.find('input').is(':checked') === false ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                    }, 100);
                                            }
                                            if ( $(this).text() === $('.show-all').val() ) {
                                                    var checkbox = $(this);
                                                    $(this).find('.cb-checkbox').unbind('click');
                                                    $(this).unbind('click').click(function() {
                                                            $('.ColVis_collection').find('input').each(function() {
                                                                    if ( $(this).prop('checked') === false ) {
                                                                            $(this).parents('li').trigger('click');
                                                                    }
                                                            });
                                                    })
                                            }
                                    }
                            });
                    }
                    ColVis_first++;
                    $('#statistics_wrapper .table tbody tr').each(function(index) {
                            if ( !$('td:last', this).hasClass("dataTables_empty") ) {
                                    $('td:last', this).text('')
                                            .append("<a href='#' class='batch_id' data-batch-id=''><i class='fa fa-download'></i></a>")
                                            .find('.batch_id', this)
                                            .attr('data-batch-id', $('td:first', this).text());
                            }
                    });
                    log_download();
            },
            "columns": [
                { "data": "batch_id", "bSortable": false },
                { "data": "source", "bSortable": false },
                { "data": "type", "bSortable": false, "sClass": "center"   },
                { "data": "total", "sClass": "center"   },
                { "data": "success", "sClass": "center"   },
                { "data": "warning", "sClass": "center"   },
                { "data": "error", "sClass": "center" },
                { "data": "date_add", "sClass": "center sorting_desc"  },
                { "data": "download", "bSortable": false, "sClass": "center"  }
            ],
            "oLanguage": {
                "sInfo": "<strong>_START_</strong> - <strong>_END_</strong> of <strong>_TOTAL_</strong>",
                "sInfoEmpty": "",
                "oPaginate": {
                    "sFirst": "First page",
                },
            },
            "aaSorting": [[ 7, "desc" ]],
            "aLengthMenu": [
                [10, 25, 50, 100, 200, -1],
                [10, 25, 50, 100, 200, "All"]
            ], iDisplayLength: 10,
        } );
        
        $('input').on('keyup', function() {
                oTable.fnDraw();
        });
        
        
        $(document).mouseup(function (e) {
                if ($('.ColVis_collection').has(e.target).length === 0){
                        $('.ColVis_collection').hide();
                        $('.ColVis_catcher').hide();
                }
        });

   
        function log_download() {
                $(".batch_id").unbind("click").click(function() {
                        window.open(base_url + 'ebay/log_download/'+ $("#id-packages").val() +'/' + $("#id-site").val() + '/' + $(this).attr("data-batch-id"), '_blank');
                });
        }