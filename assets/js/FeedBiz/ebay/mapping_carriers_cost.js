    var previous;
    var rules_controller;
    var counterWeight = 1;
    var counterPrice = 1;
    var chk_value = '';
    var postal_valid_res = 1; 
    var data                            = new Array();
    var wizard_data                     = new Array();
    var addLocal                        = 0;
    var addInter                        = 0;
    var greater_message                 = $('input[name="validate-price-modifier-range"]').val();
    var between_message                 = $('input[name="validate-price-modifier-between"]').val();

    $(document).ready(function() { 
            var lgcurrency = $('.rules-main-cost').attr('placeholder').substring(6, 9);
            $('input[name^="cost"], input[name^="additionals"], input[name^="min-price"], input[name^="max-price"]').each(function(){
                $(this).parents(".input-group:first").find('strong').text(lg_current[0][lgcurrency]);
            });
            
            $("input[name='carrier-configuration']").change(function() {
                    carrier_configuration($(this).val());
            });
            
            var collapse = $("input[name='carrier-configuration']:checked").val();
            carrier_configuration(collapse);
            
            function carrier_configuration(val) {
                    if ( val == 1 ) {
                            $(".collapse-swap").addClass('field-hide'); 
                            $("#collapse-surcharge").removeClass('field-hide'); 
                            rules_controller = 'surcharge';
                   }else if ( val == 2 ) {
                            $(".collapse-swap").addClass('field-hide'); 
                            $("#collapse-weight").removeClass('field-hide'); 
                           rules_controller = 'weight';
                   }else if ( val == 3 ) {
                            $(".collapse-swap").addClass('field-hide'); 
                            $("#collapse-item").removeClass('field-hide'); 
                            rules_controller = 'items';
                    }else if ( val == 4 ) {
                            $(".collapse-swap").addClass('field-hide'); 
                            $("#collapse-price").removeClass('field-hide'); 
                            rules_controller = 'prices';
                    }
                    set_validation();
                    validation_number();
                    addRules();
                    eventSetShippingRules();
                    eventSetAllValue();
                    eventSetWeightValue();
                    removeRules();
                    $('label.error').remove();
            }

            function removeRulesKey() {
                    delete choose_value[previous];
                    $(".rules-carriers-body").find('.rules-label:contains("'+ previous +' : ")').parent().remove();
                    //setShippingRules();
            }
            
            function eventSetShippingRules() {
                    $("select[name^='domestic-carriers']").unbind('change').change(function() {
                            $(this).blur();
                            removeRulesKey();
                    });
            }
            
            function setArrayValue(focus) {
                    var rule_body = $(focus).parents(".rules-carriers-body").find('.rules-label').text().replace(' : ', '');
                    var name = $(focus).attr('name');
                    var rel = $(focus).parents(".rules-body").attr('rel');
                    
                    $(".rules-main-cost").valid();
                    $(".rules-additionals").valid();
                    $(".rules-min-weight").valid();
                    $(".rules-max-weight").valid();
                    $(".rules-min-price").valid();
                    $(".rules-max-price").valid();
                    
                    if ( rules_controller === 'items' ) {
                            if (typeof(choose_value[rule_body][rules_controller][rel]) === 'undefined') {
                                    var item = {
                                            'cost'          : '0.00',
                                            'additionals'   : '0.00',
                                    };
                                    choose_value[rule_body][rules_controller][rel] = item;
                            }
                    }
                    else if ( rules_controller === 'weight' ) {
                            if (typeof(choose_value[rule_body][rules_controller][rel]) === 'undefined') {
                                    var item = {
                                            'cost'          : '0.00',
                                            'additionals'   : '0.00',
                                            'min-weight'    : '0.00',
                                            'max-weight'    : '0.00',
                                            'operation'     : 1
                                    };
                                    choose_value[rule_body][rules_controller][rel] = item;
                            }
                    }
                    else if ( rules_controller === 'surcharge' ) {
                            if (typeof(choose_value[rule_body][rules_controller][rel]) === 'undefined') {
                                    var item = {
                                            'cost'          : '0.00',
                                            'max-surcharge' : '0.00',
                                            'operation'     : 7
                                    };
                                    choose_value[rule_body][rules_controller][rel] = item;
                            }
                    }
                    else if ( rules_controller === 'prices' ) {
                            if (typeof(choose_value[rule_body][rules_controller][rel]) === 'undefined') {
                                     var item = {
                                            'cost'          : '0.00',
                                            'additionals'   : '0.00',
                                            'min-price'     : '0.00',
                                            'max-price'     : '0.00',
                                            'operation'     : 1
                                    };
                                    choose_value[rule_body][rules_controller][rel] = item;
                            }
                    }
                    choose_value[rule_body][rules_controller][rel][name] = parseFloat($(focus).val());
            }
            
            function eventSetDot($this, field) {
                    var main_value = $this.val();
                    if ( $this.val().substr(-1, 1) === '.' || $this.val().substr(-1, 1).charCodeAt() === 46 ) {
                            main_value = $this.val() + '00';
                    }
                    else if ( $this.val().indexOf('.') === -1 ) {
                            main_value = $this.val() + '.00';
                            if ($this.val() === '') {
                                    main_value = '0.00';
                            }
                    }
                    else if ( $this.val().indexOf('.') !== -1 ) {
                            if ( $this.val().slice($this.val().indexOf('.') + 1, $this.val().length).length === 1) {
                                    main_value = $this.val() + '0';
                            }
                    }
                    $(field).val(main_value);

                    if ($(field).val() === '') {
                            $(field).val('0.00');
                    }
            }
              
            function eventSetAllValue() {
                    $('.rules-main-cost').unbind('change');
                    $('.rules-additionals').unbind('change');
                    $(".rules-main-cost").valid();
                    $(".rules-additionals").valid();
                    var root = $('.collapse-swap').filter(':visible').find(".rules-body");
                    root.each(function() {
                            var deep_root = $(this);
                            deep_root.find('.rules-main-cost:first').unbind('focusout');
                            deep_root.find('.rules-main-cost:first').on('focusout', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-main-cost').not(':first').each(function() {
                                            eventSetDot($this, this);
                                            $(this).valid();
                                    });     
                                    $this.val('');
                            });


                            deep_root.find('.rules-main-cost').not(':first').unbind('focusout');
                            deep_root.find('.rules-main-cost').not(':first').on('focusout', function() {
                                    eventSetDot($(this), this);
                                    $(this).valid();
                            });


                            deep_root.find('.rules-additionals:first').unbind('focusout');
                            deep_root.find('.rules-additionals:first').on('focusout', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-additionals').not(':first').each(function() {
                                            eventSetDot($this, this);
                                            $(this).valid();
                                    });            
                                    $this.val('');
                            });


                            deep_root.find('.rules-additionals').not(':first').unbind('focusout');
                            deep_root.find('.rules-additionals').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                                    $(this).valid();
                            });
                    });
            }
            
            function eventSetWeightValue() {
                    $('.rules-min-weight').unbind('change');
                    $('.rules-max-weight').unbind('change');
                    $('.rules-min-price').unbind('change');
                    $('.rules-max-price').unbind('change');
                    $('.rules-surcharge').unbind('change');
                    $(".rules-min-weight").valid();
                    $(".rules-max-weight").valid();
                    $(".rules-min-price").valid();
                    $(".rules-max-price").valid();
                    var root = $('.collapse-swap').filter(':visible').find(".rules-body");
                    root.each(function() {
                            var deep_root = $(this);
                            deep_root.find('.rules-min-weight:first').unbind('focusout');
                            deep_root.find('.rules-min-weight:first').on('focusout', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-min-weight').not(':first').each(function() {
                                            eventSetDot($this, this);
                                            $(this).valid();
                                    });                            
                                    $this.val('');
                            });

                            deep_root.find('.rules-min-weight').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                                    $(this).valid();
                            });

                            deep_root.find('.rules-max-weight:first').on('change', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-max-weight').not(':first').each(function() {
                                            if ( !this.disabled ) {
                                                    eventSetDot($this, this);
                                                    $(this).valid();
                                            }
                                    });            
                                    $this.val('');
                            });

                            deep_root.find('.rules-max-weight').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                                    $(this).valid();
                            });
                            
                            deep_root.find('.rules-min-price:first').on('change', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-min-price').not(':first').each(function() {
                                            eventSetDot($this, this);
                                            $(this).valid();
                                    });                            
                                    $this.val('');
                            });

                            deep_root.find('.rules-min-price').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                                    $(this).valid();
                            });

                            deep_root.find('.rules-max-price:first').on('change', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-max-price').not(':first').each(function() {
                                            if ( !this.disabled ) {
                                                    eventSetDot($this, this);
                                                    $(this).valid();
                                            }
                                    });            
                                    $this.val('');
                            });

                            deep_root.find('.rules-max-price').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                                    $(this).valid();
                            });
                    });
            }
            
            function removeRules() {
                    $('.removerulecost').click(function() {
                            $(this).parents('.rules-body').remove();
                    });
            }

            function addRules() {
                    $('.addrulecost').unbind('click').click(function() {
                            var root                    = $(this).parents('.rules-main').find('.rules-body').filter(':first');
                            var main                    = $(this).parents('.rules-main').find('.transparent');
                            var last                    = $(this).parents('.rules-main').find('.rules-body').filter(':last');
                            var icon                    = root.find('i').attr('class');
                            var rel                     = last.attr('rel');
                            var new_rel                 = parseInt(rel) + 1;

                            last.find('select.operation').select2("destroy");

                            if (   $(".rules-main-cost").valid() 
                                && $(".rules-additionals").valid() 
                                && $(".rules-min-weight").valid() 
                                && $(".rules-max-weight").valid() 
                                && $(".rules-min-price").valid() 
                                && $(".rules-max-price").valid() ) {

                                    var root    = $(".collapse-swap").filter(':visible').find(".rules-body[rel='"+ rel +"']").filter(':visible');
                                    cloned                      = last.clone().appendTo(main);
                                    cloned.show();
                                    cloned.find('label.error').remove();
                                    cloned.find('select, input').attr('disabled', false) ;   
                                    cloned.attr('rel', new_rel);

                                    cloned.find('.addrulecost > i').removeClass().addClass(icon);
                                    cloned.children('.rules-carriers-body').find('.rules-label').each(function() {
                                            var $cloned                 = $(this);
                                            $(root).children('.rules-carriers-body').find('.rules-label').each(function() {
                                                    if ( $(this).text() === $cloned.text() ) {
                                                            var to          = $(this).parents('.rules-carriers-body:first').find('input[rel="to"]').val();
                                                            var label_name  = $cloned.text().replace(' : ', '');
                                                            $cloned.parents('.rules-carriers-body:first').find('.rules-main-cost').attr('placeholder', '0.00').val('').attr('name', 'cost-' + label_name + '-' +rules_controller + '-' + new_rel);
                                                            $cloned.parents('.rules-carriers-body:first').find('.rules-additionals').attr('placeholder', '0.00').val('').attr('name', 'additionals-' + label_name + '-' +rules_controller + '-' + new_rel);

                                                            if ( rules_controller === 'prices') {
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-min-price').attr('placeholder', '0.00').val('').attr('name', 'min-price-' + label_name + '-' +rules_controller + '-' + new_rel).attr('rel', 'from');
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-price').attr('placeholder', '0.00').val('').attr('name', 'max-price-' + label_name + '-' +rules_controller + '-' + new_rel).attr('rel', 'to');
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-min-price').rules('add', { required: true, none: true, minlength: true, between: true });
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-price').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                            }
                                                            if ( rules_controller === 'weight') {
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-min-weight').attr('placeholder', '0.00').val('').attr('name', 'min-weight-' + label_name + '-' +rules_controller + '-' + new_rel).attr('rel', 'from');
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-weight').attr('placeholder', '0.00').val('').attr('name', 'max-weight-' + label_name + '-' +rules_controller + '-' + new_rel).attr('rel', 'to');
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-weight').rules('add', { required: true, none: true, minlength: true, between: true });
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-weight').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                            }
                                                            $cloned.parents('.rules-carriers-body:first').find('.rules-main-cost').rules('add', { required: true, none: true, minlength: true });
                                                            $cloned.parents('.rules-carriers-body:first').find('.rules-additionals').rules('add', { required: true, none: true, minlength: true });
                                                            $cloned.parents('.rules-carriers-body:first').find('input[rel="from"]').val(parseFloat(to) + 1);
                                                            eventSetDot($cloned.parents('.rules-carriers-body:first').find('input[rel="from"]'), $cloned.parents('.rules-carriers-body:first').find('input[rel="from"]'));
                                                    }
                                            });
                                    });


                                    last.find('select.operation').select2();
                                    cloned.find('select.operation').select2();

                                    cloned.find('.addrulecost > i').removeClass().addClass('fa fa-minus-square-o').parent().removeClass().addClass('icons-plus removerulecost');//icon
                                    cloned.find('.operation').filter('option').removeAttr('selected').filter('option[value="0"]').prop('selected', true).css({'color': '#858585'});

                                    eventSetShippingRules();
                                    eventSetAllValue();
                                    eventSetWeightValue();
                                    removeRules();
                            }
                    });
            }

            function cost_rate(ele) {
                    var start_string = $(ele).first().attr('placeholder').search(" ");
                    var start_string = parseInt(start_string) + 2 ;
                    var last_string = parseInt(start_string) + 3 ;
                    var lgcurrency = $(ele).first().attr('placeholder').substring(start_string, last_string);
                    $(ele).parents(".input-group:first").find('strong').text(lg_current[0][lgcurrency]);
                    $(ele).unbind('focusout');
                    $(ele).on('focusout', function() {
                            var $this = $(this);
                            eventSetDot($this, this);
                    });
            }
 
            function validation_number() {
                        $(".rules-main-cost, .rules-additionals, .rules-min-weight, .rules-max-weight, .rules-min-price, .rules-max-price, .additional-rate, .cost-rate").unbind('keyup');
                        $(".rules-main-cost, .rules-additionals, .rules-min-weight, .rules-max-weight, .rules-min-price, .rules-max-price, .additional-rate, .cost-rate").on("keyup", function(){
                            var valid = /^\+?[0-9][0-9]*(\.\d{0,2})?$/.test(this.value),
                            val = this.value;

                            if(!valid){
                                console.log("Invalid input!");
                                this.value = val.substring(0, val.length - 1);
                            }
                            
                            var e = window.event || e;
				var keyUnicode = e.charCode || e.keyCode;
				if (e !== undefined) {
					switch (keyUnicode) {
						case 16: break; // Shift
						case 17: break; // Ctrl
						case 18: break; // Alt
						case 27: this.value = ''; break; // Esc: clear entry
						case 35: break; // End
						case 36: break; // Home
						case 37: break; // cursor left
						case 38: break; // cursor up
						case 39: break; // cursor right
						case 40: break; // cursor down
						case 78: break; // N (Opera 9.63+ maps the "." from the number key section to the "N" key too!) (See: http://unixpapa.com/js/key.html search for ". Del")
						case 110: break; // . number block (Opera 9.63+ maps the "." from the number block to the "N" key (78) !!!)
						case 190: break; // .
						default: $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1, eventOnDecimalsEntered: true,symbol:'',groupDigits:false });
					}
				}
                        });
                }

                function set_validation() {
                        var rules = new Object();
                        var messages = new Object();

                        $.validator.addMethod(
                                "regex",
                                function(value, element, regexp) {
                                    var re = new RegExp(regexp);
                                    return this.optional(element) || re.test(value);
                                },
                                $('input[name="validate-form-postal-code-required"]').val()
                        );
                
                        $.validator.addMethod("both", function(value, element) {
                                return this.optional(element) || value != 0;
                                }, 
                                $('input[name="validate-carrier_rule-select"]').val()
                        );
                
                        $.validator.addMethod("none", function(value, element) {
                                return this.optional(element) || value !== '';
                                }, 
                                $('input[name="validate-price-modifier-required"]').val()
                        );
                
                        $.validator.addMethod(
                                "greater",
                                function(value, element) {
                                    var validator = this;
                                    var from = $(element).parents('.rules-carriers-body:first').find('input[rel="from"]').val();
                                    greater_message = $('input[name="validate-price-modifier-range"]').val().replace('{1}', from);
                                    if ( rules_controller === 'weight') {
                                            greater_message = $('input[name="validate-price-modifier-range-weight"]').val().replace('{1}', from);
                                    }
                                    $.validator.messages.greater = greater_message;
                                    return this.optional(element) || parseFloat(value) > parseFloat(from);
                                },
                                greater_message
                        );

                        $.validator.addMethod(
                                "between",
                                function(value, element) {
                                    var rel = $(element).parents('.rules-body:first').attr('rel');
                                    var label = $(element).parents('.rules-carriers-body:first').find('.rules-label').text();
                                    var check_all = true;
                                    var root    = $(".collapse-swap").filter(':visible').find(".rules-body").filter(':visible');
                                    $(root).each(function() {
                                            if ( $(this).attr('rel') !== rel ) {
                                                    $(this).children('.rules-carriers-body').find('.rules-label').each(function() {
                                                            if ( $(this).text() === label  ) {
                                                                    var from    = $(this).parents('.rules-carriers-body:first').find('input[rel="from"]').val();
                                                                    var to      = $(this).parents('.rules-carriers-body:first').find('input[rel="to"]').val();
                                                                    if ( parseFloat(from) <= value && value <= parseFloat(to) ) {
                                                                            check_all = false;
                                                                            between_message = $('input[name="validate-price-modifier-between"]').val().replace('{1}', from).replace('{2}', to);
                                                                            if ( rules_controller === 'weight') {
                                                                                    between_message = $('input[name="validate-price-modifier-between-weight"]').val().replace('{1}', from).replace('{2}', to);
                                                                            }
                                                                            $.validator.messages.between = between_message;
                                                                    }
                                                            }
                                                    });
                                            }
                                    });
                                    return this.optional(element) || check_all;
                                },
                                between_message
                        );
                
                        $('input[name^="cost-"]').each(function() {
                                rules[this.name] = { required: true, none: true, minlength: true };
                        });
                        
                        $('input[name^="additionals-"]').each(function() {
                                rules[this.name] = { required: true, none: true, minlength: true };
                        });
                
                        $('input[name^="min-price-"]').each(function() {
                                rules[this.name] = { required: true, none: true, minlength: true, between: true };
                        });
                        
                        $('input[name^="max-price-"]').each(function() {
                                rules[this.name] = { required: true, none: true, minlength: true, between: true, greater: true };
                        });
                        
                        $("#form-submit").validate({
//                        	ignore: 'input[type=hidden]',
                                rules: rules,
                                messages: messages,
                                errorPlacement: function(error, element) {
                                        if ($(element).hasClass("rules-main-cost") 
                                         || $(element).hasClass("rules-additionals")   
                                         || $(element).hasClass("rules-min-weight")   
                                         || $(element).hasClass("rules-max-weight")   
                                         || $(element).hasClass("rules-min-price")   
                                         || $(element).hasClass("rules-max-price")   
                                         || $(element).hasClass("additional-rate")   
                                         || $(element).hasClass("cost-rate")   
                                        ) {
                                                $(element).parent('.input-group').addClass('group-error');
                                                error.insertAfter( $(element).parent('.input-group') );
                                        } 
                                        else {
                                                error.appendTo($(element).parent('.input-group'));
                                        }
                                },
                                unhighlight: function (element) {
                                        if ($(element).hasClass("rules-main-cost") 
                                         || $(element).hasClass("rules-additionals")   
                                         || $(element).hasClass("rules-min-weight")   
                                         || $(element).hasClass("rules-max-weight")   
                                         || $(element).hasClass("rules-min-price")   
                                         || $(element).hasClass("rules-max-price")   
                                         || $(element).hasClass("additional-rate")   
                                         || $(element).hasClass("cost-rate")   
                                        ) {
                                                $(element).parent('.input-group').removeClass('group-error');
                                        }
                                },
                                debug: true
                        });
                }

                $("#form-submit").on("submit", function(e) {
                    
                    if (   $(".rules-main-cost").valid() 
                        && $(".rules-additionals").valid() 
                        && $(".rules-min-weight").valid() 
                        && $(".rules-max-weight").valid() 
                        && $(".rules-min-price").valid() 
                        && $(".rules-max-price").valid() 
                       ) {
                    
                            var page = $("#current-page").val();
                            e.preventDefault();
                            $('html, body').animate({ scrollTop: 0 }, 0);
                            var data_rules                      = new Array();
                            var carrier_operation               = 'is between';
                            var carrier_rule_id                 = $('.carrier-configuration:checked').val();
                            if ( typeof(carrier_rule_id) !== 'undefined' && carrier_rule_id == 3 ) {
                                    carrier_operation           = 0;
                            }
                            data                        = [];
                            wizard_data                 = [];
                            $(".collapse-swap").filter(":visible").find('.rules-carriers-real').each(function( index ) {
                                    var carrier_id          = $(this).attr('data-service');
                                    var carrier_ebay_id     = $(this).attr('data-ebay-service');
                                    var carrier_is          = $(this).attr('data-internation');
                                    var country_service     = $(this).attr('data-country');
                                    var carrier_rel         = $(this).parents('.rules-body').attr('rel');
                                            if (typeof(carrier_id) !== "undefined" && carrier_id !== '0' && typeof(carrier_ebay_id) !== "undefined" && carrier_ebay_id !== '0' ) {
                                                    data_input                      = {
                                                            'id_profile'            : $('.id_profile').val() ? $('.id_profile').val() : 0,
                                                            'carrier_rule_id'       : carrier_rule_id,
                                                            'carrier_id'            : carrier_id,
                                                            'carrier_ebay_id'       : carrier_ebay_id,
                                                            'carrier_is'            : (carrier_is) ? carrier_is : 0,
                                                            'country_service'       : (country_service) ? country_service : 0,
                                                            'carrier_cost'          : $(this).find('.rules-main-cost').val(),
                                                            'carrier_additionals'   : $(this).find('.rules-additionals').val(),
                                                            'carrier_min_weight'    : $(this).find('.rules-min-weight').val(),
                                                            'carrier_max_weight'    : $(this).find('.rules-max-weight').filter(':enabled').val(),
                                                            'carrier_min_price'     : $(this).find('.rules-min-price').val(),
                                                            'carrier_max_price'     : $(this).find('.rules-max-price').filter(':enabled').val(),
                                                            'carrier_surcharge'     : $(this).find('.rules-surcharge').val(),
                                                            'carrier_rel'           : carrier_rel,
                                                            'carrier_operation'     : carrier_operation,
                                                            'postcode'              : $('.postal_code').val(),
                                                            'dispatch_time'         : $('.dispatch_time').val(),
                                                            'tab'                   : 1,
                                                            'step'                  : $("#popup-step").val(),
                                                    };
                                                    data_rules.push(data_input);
                                            }
                            });


                            data_selected = {
                                'id_site'       : $("#id-site").val(),
                                'id_packages'   : $("#id-packages").val(),
                            };

                            data.push(data_selected);

                            $.ajax ({
                                    type        : "POST",
                                    url         : base_url + "ebay/" + $('#ajax-url').val(),
                                    data        : { data: data, 'data_rules' : data_rules },
                                    error       : function(jqXHR, textStatus, errorThrown) {
                                                notification_alert(errorThrown,'bad');
                                    },
                                    success     : function(result) {
                                                if ( (result.search(/Fail/) != 0) && (result.search(/Fail/) != 24) ) {   
                                                        if ( typeof(module) !== 'undefined' && module === 1 ) {
                                                                wizard_start();
                                                        }
                                                        else {
                                                                notification_alert($(".update-" + page + "-success").val() );
                                                                setTimeout(function(){
                                                                    location.href   = $('#form-submit').attr('action');
                                                                }, 3200);
                                                        }
                                                }
                                                else {    
                                                        notification_alert($(".update-" + page + "-fail").val(),'bad');
                                                }
                                    }
                            });
                    }
            });
    });