    var module                          = 1;
    var chk_value = '';
    var postal_valid_res = 1; 
    var data                            = new Array();
    var wizard_data                     = new Array();

    $(document).ready(function() {             
            $("#form-submit").on("submit", function(e) {             
                    if ( $("#form-postal-code").valid() ) {
                            e.preventDefault();
                            $('html, body').animate({ scrollTop: 0 }, 0);
                            var dataValue                   = [];
                            if ( typeof(module) !== 'undefined' && module === 1 ) {
                                    data_selected                   = {
                                            'email'                 : '',
                                            'city'                  : '',
                                            'postal'                : $('#form-postal-code').val(),
                                            'dispatch_time'         : $("#dispatch_time").val(),
                                            'returns_policy'        : $(".returns-policy").val() ? $(".returns-policy").val() : 'ReturnsAccepted',
                                            'returns_within'        : $(".returns-within").val() ? $(".returns-within").val() : 'Days_14',
                                            'returns_pays'          : $(".returns-pays").val() ? $(".returns-pays").val() : 'Buyer',
                                            'returns_information'   : $(".returns-information").val() ? $(".returns-information").val() : 'Return within 14 Days',
                                            'listing_duration'      : 'Days_5',
                                            'id_site'               : $("#id-site").val(),
                                            'id_packages'           : $("#id-packages").val(),
                                            'template'              : '1',
                                            'no_image'              : $('.no-image').val() ? $('.no-image').val() : 0,
                                            'discount'              : $('.discount').val() ? $('.discount').val() : 0,
                                            'sync'                  : 0,
                                            'just_orders'           : $('.just-orders').val() ? $('.just-orders').val() : 0,
                                    };
                                    dataValue.push(data_selected);

                                    $.ajax ({
                                            type        : "POST",
                                            url         : base_url + "ebay/auth_security_save",
                                            data        : {data : dataValue},
                                            success     : function(results) {
                                                        if ( $.trim(results) === 'Success' ) {  
                                                                if ( typeof(module) !== 'undefined' && module === 1 ) {
                                                                        wizard_start();
                                                                }
                                                                else {
                                                                        notification_alert($(".update-" + page + "-success").val() );
                                                                        setTimeout(function(){
                                                                            location.href   = $('#form-submit').attr('action');
                                                                        }, 1200);
                                                                }
                                                        }
                                                        else {    
                                                                notification_alert($(".update-" + page + "-fail").val(), 'bad');
                                                        }
                                            }
                                    });
                                    
                                    var data_selected = {
                                            'id_site'       : $("#id-site").val(),
                                            'id_packages'   : $("#id-packages").val(),
                                            'id_popup'      : $("#popup-step").val(),
                                    };
                                    wizard_data.push(data_selected);
                            }
                    }
                    
            });

            set_validation();


            function set_validation() {
                    var rules = new Object();
                    var messages = new Object();

                    rules['form-postal-code'] = { 
                            required        : true, 
                            regex           : $('#form-postal-code').attr('pattern') ,
                            postal_valid    : $('#form-postal-code').attr('data-code') 
                    };

                    messages['form-postal-code'] = { 
                            required        : $('input[name="validate-form-postal-code-required"]').val(),
                            regex           : $('input[name="validate-form-postal-code-regex"]').val(),
                            postal_valid    : $('input[name="validate-form-postal-code-postal-valid"]').val()
                    };
                    $.validator.addMethod(
                            "regex",
                            function(value, element, regexp) {
                                var re = new RegExp(regexp);
                                return this.optional(element) || re.test(value);
                            },
                            $('input[name="validate-form-postal-code-required"]').val()
                    );


                    $("#form-submit").validate({
                            ignore: 'input[type=hidden]',
                            rules: rules,
                            messages: messages,
                            errorPlacement: function(error, element) {
                                    if ($(element).hasClass("form-postal-code") ) {
                                            $(element).parent().find('input').addClass('group-error');
                                            error.insertAfter( $(element) );
                                    } 
                                    else {
                                            error.appendTo($(element).parent());
                                    }
                            },
                            unhighlight: function (element) {
                                    if ($(element).hasClass("form-postal-code") ) {
                                            $(element).parent().find('input').removeClass('group-error');
                                    }
                            },
                            debug: true
                    });
            }

            $.validator.addMethod(
                    "postal_valid",
                    function(value, element, zip) {
                            data_valid      = {
                                postal_code : value,
                                zip         : zip
                            };
                            if ( zip !== 'IE' ) {
                                    spinAdd();
                                    $.ajax ({
                                            type       : "POST",
                                            url        : base_url + "ebay/get_postalcode_valid",
                                            data       : {postal_valid : data_valid},
                                            error      : function(jqXHR, textStatus, errorThrown) {
                                                       notification_alert(errorThrown,'bad');
                                           },
                                           success     : function(save) {
                                                       if ( save === 'Success') {
                                                                postal_valid_res = 1;
                                                                $('.form-postal-code').removeClass('error');
                                                       }
                                                       else {
                                                                postal_valid_res = 0;
                                                                if(chk_value!=value){
                                                                     $(".form-postal-code").unbind('blur').blur();
                                                                }
                                                                chk_value = value;
                                                       }
                                           }
                                    });
                            }
                            else {
                                    postal_valid_res = 1;
                            }
                        return this.optional(element) || postal_valid_res === 1;
                    },
                    $('input[name="validate-form-postal-code-required"]').val()
            );

            $('#form-postal-code').focusout(function() {
                    $('#form-postal-code').valid();
            });
    });