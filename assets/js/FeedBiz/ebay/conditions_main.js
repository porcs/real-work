
        var postal_valid;
        var postal_valid_res                = 1;
        var chk_value                       = '';
        var data                            = new Array();
        var wizard_data                     = new Array();
        var category_id                     = 0;
        
        $('.widget-tabs li').unbind('click').click(function() {
                $tab_li     = $(this);
                active_li   = $('a', this).attr('href').replace('#tab-', '');
        });
        
        function updateCountdown() {
                $('.condition-description').each(function() {
                        var remaining = 1000 - $(this).val().length;
                        $(this).parent().parent().find('.countdown-textarea').text($('.remaining').val() + ' : ' + remaining);
                });
        }
        
        var active_li = $('.widget-tabs li.active').find('a', this).attr('href').replace('#tab-', '');

        updateCountdown();
        $('.condition-description').change(updateCountdown);
        $('.condition-description').keyup(updateCountdown);
console.log('start condition mapping');
        $("#form-submit").on('submit', function(e) {
                e.preventDefault();
                data                                        = [];
                var data_mapping                            = [];
                $('html, body').animate({ scrollTop: 0 }, 0);
                $('input[name="condition-values-mapping"]').each(function() {
                        var selected                        = $(this).parents('.condition-main-values').find('select[name="condition-values-mapping"]');
                        if($(selected).filter('option:selected').val() != 0){
                            var mapping                         = {
                                    'name'                      : $(selected).find('option:selected').val(),
                                    'mapping_value'             : $(this).attr('data-key'),
                                    'mapping_name'              : $(this).attr('data-value'),
//                                    'name'                      : $(this).attr('data-value'),
//                                    'mapping_value'             : $(selected).val(),
//                                    'mapping_name'              : $(selected).find('option:selected').text(),
                            };
                            data_mapping.push(mapping);
                        }
                });
                
                data_selected                               = {
                        'id_profile'                        : $('.id_profile').val(),
                        'data_mapping'                      : data_mapping,
                        'condition_value'                   : $('select[name="condition-values"]').val(),
                        'condition_name'                    : $('select[name="condition-values"] option:selected').text(),
                        'condition_description'             : $("#condition-description").val(),
                        'condition_description_new'         : $("#condition-description-new").val(),
                        'condition_description_good'        : $("#condition-description-good").val(),
                        'condition_description_used'        : $("#condition-description-used").val(),
                        'condition_description_refurbished' : $("#condition-description-refurbished").val(),
                        'condition_description_acceptable'  : $("#condition-description-acceptable").val(),
                        'tab'                               : active_li,
                        'id_site'                           : $("#id-site").val(),
                        'id_packages'                       : $("#id-packages").val(),
                        'step'                              : $("#popup-step").val(),
                };
                data.push(data_selected);
                
                if ($.isEmptyObject(data)) {
                        var data_value              = {
                                'id_site'               : $("#id-site").val(),
                                'id_packages'           : $("#id-packages").val(),
                        };
                        data.push(data_value);
                }
                
                var data_selected = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'id_popup'      : $("#popup-step").val(),
                };
                wizard_data.push(data_selected);

                ajax_save(e);
        });
