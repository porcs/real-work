function dismiss($this) {
        $($this).find('.close').unbind('click').click(function() {
                $($this).text('').hide();
        })
}

function spinAdd() {
        $('.btn-savechanges i').removeClass().addClass('fa fa-spinner fa-spin').parents('.form-actions').find('.btn').attr('disabled', 'disabled');
}

function spinAddBack() {
        $('.btn-clearchanges i').removeClass().addClass('fa fa-spinner fa-spin').parents('.form-actions').find('.btn').attr('disabled', 'disabled');
}

function spinRemove() {
        $('.btn-savechanges i').removeClass().addClass('fa fa-arrow-right').parents('.form-actions').find('.btn').removeAttr('disabled');
}

function spinRemoveBack() {
        $('.btn-clearchanges i').removeClass().addClass('fa fa-arrow-left').parents('.form-actions').find('.btn').removeAttr('disabled');
}

$(document).ready(function() {
        $('button[type="back"]').on("click", function(e) {
                e.preventDefault();
                if ( typeof(module) !== 'undefined' && module === 1 ) {
                        var data            = [];
                        var data_selected   = {
                                'id_site'       : $("#id-site").val(),
                                'id_packages'   : $("#id-packages").val(),
                                'id_popup'      : $("#popup-step").val(),
                        };

                        data.push(data_selected);
                        
                        $.ajax({
                                type       : "POST",
                                url        : base_url + "ebay/ebay_wizard_clear",
                                data       : {data : data},
                                error       : function(jqXHR, textStatus, errorThrown) {
                                            notification_alert(errorThrown, 'bad');
                                },
                                success     : function(result) {
                                            if ( $.trim(result) == 'Success' ) {   
                                                    location.href   = $('#form-submit').attr('action');
                                            }
                                            else {
                                                    notification_alert(errorThrown, 'bad');
                                            }
                                }
                        });
                }
                else {
                        location.href   = $(this).attr('data-page');
                }
        });
        
        if (typeof $.fn.select2 != 'undefined' && ((typeof(page) === 'undefined') || ((page !== 'mapping_categories') && (page !== 'error_resolutions') && (page !== 'log') && (page !== 'explode_products') && (page !== 'advance_profile_categories_mapping') && (page !== 'synchronization_matching') && (page !== 'advance_categories_main') &&  (page !== 'advance_products_main') && (page !== 'statistics') && (page !== 'orders') && (page !== 'orders_shipped') && (page !== 'orders_cancelled') && (page !== 'orders_awaiting_payment') && (page !== 'carts') && (page !== 'mapping_templates') && (page !== 'wizard_categories'))) ) {
                $("select").select2({
                        allowClear: true,
                        cache: "true",
                });
        }
});