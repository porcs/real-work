(function(a,c){
    var duplicate= 0;
    
    var b=function(e,d){
        
        this.$element=a(e);
        this.options=a.extend({},a.fn.tree.defaults,d);
        
        this.$element.on("click",".tree-item-name",a.proxy(function(f){
            this.selectItem(f.currentTarget.offsetParent);
            this.addSelect(f.currentTarget.offsetParent);
        },this));
        
        this.$element.on("click",".tree-folder-name, .icon-plus",a.proxy(function(f){
            this.selectFolder(f.currentTarget.offsetParent);
            this.addSelect(f.currentTarget.offsetParent);
        },this));
        
        this.render();
    };
    
    b.prototype={
        
        constructor:b,
        addSelect:function(e,j){
            var d=a(e);
//            console.log(d);
            var select = this.$element.parent().find('#main .pull-right').clone();
            
            if(d.find('input.checbox-tree-folder').prop('checked', true))
            {
                if(!d.find('.pull-right').length)
                {
                    d.append(select.removeAttr('id'));
                    
                    d.find('select.profile-price').each(function(g,f){
                        var b=a(f);
                        b.css('width','100%');
                        b.css('height','25px');
                        b.css('font-size','12px');
                        b.css('border-radius','3px');
                        b.attr('name', 'category[' + d.find('input[rel="id_category"]').val() + '][id_profile]');
                    });            
                    if(j !== '')
                        select.find('select.profile-price option[value="' + j+ '"]').prop('selected',true);            
                        
                }
            }
            else if(d.find('input.checbox-tree-folder').prop('checked', false))
                if(d.find('.pull-right').length)
                    d.find('.pull-right').remove();
            
        },
        
        render:function(){
            this.populate(this.$element);
           
        },
        populate:function(f){
            var e=this;
            var d=f.parent().find(".tree-loader:eq(0)");
            d.show();
            this.options.dataSource.data(f.data(),function(g){
                d.hide();
                a.each(g.data,function(h,j){
                    var i;
                       
                    if(j.type==="folder"){
                        
                        i=e.$element.find(".tree-folder:eq(0)").clone().show();
                        
                        var category_name = j.name ;
            
                        category_name = category_name.replace(/[^a-zA-Z0-9\s_]/g,'').replace(/\s/g,'_');

                        if(i.find('input[name="category[' + j.name + ']"]').length !== 0)
                        {
                            category_name = category_name + '_' + duplicate;
                            duplicate++;                    
                        } 
                        i.find(".tree-folder-name").html('<input type="checkbox" rel="id_category" class="checbox-tree-folder ace-checkbox-2" name="category[' + j.id + '][id_category]" value="' + j.id + '"><span class="lbl">' + j.name + '</label>');
                        
                        i.find(".tree-loader").html(e.options.loadingHTML);
                        var k=i.find(".tree-folder-header");
                        k.data(j);
                        if("icon-class" in j){
                            k.find('[class*="icon-"]').addClass(j["icon-class"]);
                        }
                        
                                    console.log(j);
                        if(j.additionalParameters)
                        {
                            a.each(j.additionalParameters,function(hh,jj)
                            {
                                if(hh === "id_profile")
                                    e.addSelect(k, jj);
                            });
                        }
                            
                    }else{
                        if(j.type==="item"){
                            
                             
                            i=e.$element.find(".tree-item:eq(0)").clone().show();
                            
                            var category_name = j.name ;
            
                            category_name = category_name.replace(/[^a-zA-Z0-9\s_]/g,'').replace(/\s/g,'_');

                            if(i.find('input[name="category[' + j.name + ']"]').length !== 0)
                            {
                                category_name = category_name + '_' + duplicate;
                                duplicate++;                    
                            } 
                            
                            i.find(".tree-item-name").html('<input type="checkbox" rel="id_category" class="checbox-tree-item ace-checkbox-2" name="category[' + j.id + '][id_category]" value="' + j.id + '"><span class="lbl">' + j.name + '</label>');
                            i.data(j);
                            
                            if(j.additionalParameters)
                            {
                                a.each(j.additionalParameters,function(hh,jj)
                                {
                                    if(hh === "id_profile")
                                        e.addSelect(i, jj);
                                });
                            }
                        }
                    }
                    
                    //console.log(f.hasClass("tree-folder-header"));
                    if(f.hasClass("tree-folder-header")){
                        f.parent().find(".tree-folder-content:eq(0)").append(i);
                    }else{
                        f.append(i);
                    }
                });
                
                e.$element.trigger("loaded");
            });
        },
       
        selectItem:function(e){
            
            var d=a(e);
            var g=this.$element.find(".tree-selected");
            var f=[];
            
            
            if(this.options.multiSelect){
                a.each(g,function(i,j){
                    var h=a(j);
                    if(h[0]!==d[0]){
                        f.push(a(j).data());
                    }
                });
                
            }else{
                if(g[0]!==d[0]){
                    g.removeClass("tree-selected").find("i").removeClass(this.options["selected-icon"]).addClass(this.options["unselected-icon"]);
                    f.push(d.data());
                }
            }

            if(d.hasClass("tree-selected")){
                
                d.find(".tree-item-name input.checbox-tree-item").prop('checked', false);
                d.removeClass("tree-selected");
              
                
            }else{
                d.find(".tree-item-name input.checbox-tree-item").prop('checked', true);
                d.addClass("tree-selected");
                
                if(this.options.multiSelect){
                    f.push(d.data());
                }
                
            }
           
            if(f.length){
                this.$element.trigger("selected",{info:f});
            }
        },
        selectFolder:function(e){
            var d=a(e);
            var f=d.parent();
            
            if(d.find("."+this.options["close-icon"]).length){
                 
                //console.log(f.find(".tree-folder-content").children());
                if(f.find(".tree-folder-content").children().length){
                    f.find(".tree-folder-content").show();
                    
                }else{
                    
                    this.populate(d);
                }

                d.find(".tree-folder-name input.checbox-tree-folder").prop('checked', true);
                f.find("."+this.options["close-icon"]+"").removeClass(this.options["close-icon"]).addClass(this.options["open-icon"]);
                this.$element.trigger("opened",d.data());
                
            }else{
                
                if(this.options.cacheItems){
                    f.find(".tree-folder-content").hide();
                    
                }else{
                    f.find(".tree-folder-content").empty();
                }
                 
                d.find(".tree-folder-name input.checbox-tree-folder").prop('checked', false); 
                f.find("."+this.options["open-icon"]+"").removeClass(this.options["open-icon"]).addClass(this.options["close-icon"]);
                this.$element.trigger("closed",d.data());
            }
        },
        selectedItems:function(){
            
            var e=this.$element.find(".tree-selected");
            var d=[];
            a.each(e,function(f,g){
                d.push(a(g).data());
            });
            return d;
        },
        collapse: function () {

            // find open folders
            this.$element.find("."+this.options["open-icon"]).each(function () {
                // update icon class
                var $this = $(this)
                    .removeClass('icon-minus icon-plus')
                    .addClass('icon-plus');

                // "close" or empty folder contents
                var $parent = $this.parent().parent();
                
                //console.log($parent);
                var $folder = $parent.children('.tree-folder-content');

                $folder.hide();
            });
                
        },
        expandAll:function(e){
            var d=a(e);
            var f=d.parent();
            var c = this;
            if(d.find("."+this.options["close-icon"]).length){
                 
                if(f.find(".tree-folder-content").children().length){
                    f.find(".tree-folder-content").each(function()
                    {
                        console.log(e);
                        //this.populate(f.find(".tree-folder-content"));
                        f.find(".tree-folder-content").show();
//                        a.each(e,function(f,g){
//                            d.push(a(g).data());
//                        });
//                         f.one("loaded", function () {
//                            if(folder.additionalParameters.children)
//                            {   
//                                $.each(folder.additionalParameters.children, function (i, item) {
//                                    expandTreeFolder($treeEl, item, $parentEl);
//                                });
//                            }
//                        });
                    });
                    
                }else{
                    
                    f.find(".tree-folder-content").each(function(n, m)
                    {
                        var o = a(m);
                        var x = o.parent().find("div.tree-folder-header");
                        c.populate(x);
                        //f.find(".tree-folder-content").show();
                    });
//                    this.populate(d);
                }
                
                f.find("."+this.options["close-icon"]+"").removeClass(this.options["close-icon"]).addClass(this.options["open-icon"]);
                this.$element.trigger("opened",d.data());
                
            }
        }
    };
    
    a.fn.tree=function(e,g){
        var f;
        var d=this.each(function(){
            var j=a(this);
            var i=j.data("tree");
            var h=typeof e==="object"&&e;
            if(!i){
                j.data("tree",(i=new b(this,h)));
            }
            
            if(typeof e==="string"){
                f=i[e](g);
            }
        });
        
        return(f===c)?d:f;
    };
    
    a.fn.tree.defaults={
        multiSelect:false,
        loadingHTML:"<div>Loading...</div>",
        cacheItems:true
    };
    
    a.fn.tree.Constructor=b;
    
})(window.jQuery);
/*(function(a,c){
    
    var b=function(e,d){
        
        this.$element=a(e);
        this.options=a.extend({},a.fn.tree.defaults,d);
        
//        this.$element.on("click",".tree-item",a.proxy(function(f){
//            this.selectItem(f.currentTarget);
//        },this));
//        
//        this.$element.on("click",".tree-folder-header",a.proxy(function(f){
//            this.selectFolder(f.currentTarget);
//            
//        },this));
                
        this.render();
    };
    
    b.prototype={
        
        constructor:b,
        
        render:function(){
            this.populate(this.$element);
           
        },
        populate:function(f){
            var e=this;
            var d=f.parent().find(".tree-loader:eq(0)");
            d.show();
//            console.log(f);//tree1
            
            this.options.dataSource.data(f.data(),function(g){
                d.hide();
                a.each(g.data,function(h,j)
                {
                    var i, ii;
                    if(j.type==="folder")
                    {
                        i = f.parent().find("#main .tree-folder").clone().show();
                        i.find('.tree-folder-name').html('<input type="checkbox" class="checbox-tree-folder ace-checkbox-2" name="category[]" value="' + j.id + '"><span class="lbl">' + j.name + '</span>');
                        
                        if(j.additionalParameters.children)
                        {
                            a.each(j.additionalParameters.children,function(hh,jj)
                            {
                                //e.subCategory(i,jj);
                            });
                        }
                    }
                    else
                    {
                        if(j.type==="item")
                        {
                            i = f.parent().find("#main .tree-item").clone().show();
                            i.find('.tree-item-name').html('<input type="checkbox" class="checbox-tree-folder ace-checkbox-2" name="category[]" value="' + j.id + '"><span class="lbl">' + j.name + '</span>');
                        }
                    }
                    
                    if(f.hasClass("tree-folder-header")){
                        f.parent().find(".tree-folder-content").append(i);
                        f.parent().find(".tree-folder-content").show();
                    }else{
                        f.append(i);
                    }
                    
                    
                });
            });
        },
        subCategory:function(f,d){

            var e=this;
            var i;
            if(d.type==="folder")
            {
                i = this.$element.parent().find('#main .tree-folder').clone().show();
                i.find('.tree-folder-name').html('<input type="checkbox" class="checbox-tree-folder ace-checkbox-2" name="category[]" value="' + d.id + '"><span class="lbl">' + d.name + '</span>');
                
                if(d.additionalParameters.children)
                {
                    a.each(d.additionalParameters.children,function(hh,jj)
                    {
                        //e.subCategory(i,jj);
                    });
                }
            }
            else
            {
                if(d.type==="item")
                {
                    i = this.$element.parent().find("#main .tree-item").clone().show();
                    i.find('.tree-item-name').html('<input type="checkbox" class="checbox-tree-folder ace-checkbox-2" name="category[]" value="' + d.id + '"><span class="lbl">' + d.name + '</span>');
                }
            }
            
            f.find(".tree-folder-content").append(i);

        },
        selectedItems:function(){
            
            var e=this.$element.find(".tree-selected");
            var d=[];
            a.each(e,function(f,g){
                d.push(a(g).data());
            });
            return d;
        },
        
    };
    
    a.fn.tree=function(e,g){
        var f;
        var d=this.each(function(){
            var j=a(this);
            var i=j.data("tree");
            var h=typeof e==="object"&&e;
            if(!i){
                j.data("tree",(i=new b(this,h)));
            }
            
            if(typeof e==="string"){
                f=i[e](g);
            }
        });
        
        return(f===c)?d:f;
    };
    
    a.fn.tree.defaults={
        multiSelect:false,
        loadingHTML:"<div>Loading...</div>",
        cacheItems:true
    };
    
    a.fn.tree.Constructor=b;
    
})(window.jQuery);*/