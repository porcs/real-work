
        var data                            = new Array();
        var wizard_data                     = new Array();
        var insert_variation;
        var previous;
        
        $('.widget-tabs li').unbind('click').click(function() {
                $tab_li     = $(this);
                active_li   = $('a', this).attr('href').replace('#tab-', '');
        });

        if ( $('.widget-tabs .tab-content li').length > 0 ) {
                    var active_li               = $('.widget-tabs .tab-content li.active').find('a', this).attr('href').replace('#tab-', '');
        }
        
        $('.edit-new').bind('click', function() {
                $(this).parents('.value-type-group').find('.input-main-type').toggleClass('style-hide');
                $(this).parents('.value-type-group').find('.select-main-type').toggleClass('style-hide');
                if ( $(this).find('i').hasClass('fa-pencil') ) {
                        $(this).removeClass('btn-primary').addClass('btn-danger');
                        $(this).find('i').removeClass('fa-pencil').addClass('fa-eraser');
                        var widget = $(this).parents('.value-type-group');
                        widget.find('input[name^="attribute-main-ebay"]').val(widget.find('input[name^="variations-value-mapping"]').attr('data-value')).removeAttr('readonly');
                        widget.find('.down-icon').css({'visibility': 'hidden'});
                }
                else {  
                        $(this).removeClass('btn-danger').addClass('btn-primary');
                        $(this).find('i').removeClass('fa-eraser').addClass('fa-pencil');
                        var t = $(this).parents('.value-type-group').find('input[name^="attribute-main-ebay"]').attr('data-value');
                        $(this).parents('.value-type-group').find('input[name^="attribute-main-ebay"]').val(t).attr('readonly', 'readonly');
                        $(this).parents('.value-type-group').find('.down-icon').css({'visibility': 'visible'});
                }
        });

        $('.sp-edit-new').each(function() {
                spEditNew(this);
        });
        
        function spEditNew(element) {
                $(element).bind('click', function() {
                        $(this).parents('.sp-value-type-group').find('.sp-input-main-type').toggleClass('style-hide');
                        $(this).parents('.sp-value-type-group').find('.sp-select-main-type').toggleClass('style-hide');
                        if ( $(this).find('i').hasClass('fa-pencil') ) {
                                $(this).removeClass('btn-primary').addClass('btn-danger');
                                $(this).find('i').removeClass('fa-pencil').addClass('fa-eraser');
                                var widget = $(this).parents('.sp-value-type-group');
                                widget.find('input[name^="sp-attribute-main-ebay"]').val(widget.find('input[name^="sp-variations-value-mapping"]').attr('data-value')).removeAttr('readonly');
                                widget.find('.down-icon').css({'visibility': 'hidden'});
                        }
                        else {  
                                $(this).removeClass('btn-danger').addClass('btn-primary');
                                $(this).find('i').removeClass('fa-eraser').addClass('fa-pencil');
                                var t = $(this).parents('.sp-value-type-group').find('input[name^="sp-attribute-main-ebay"]').attr('data-value');
                                $(this).parents('.sp-value-type-group').find('input[name^="sp-attribute-main-ebay"]').val(t).attr('readonly', 'readonly');
                                $(this).parents('.sp-value-type-group').find('.down-icon').css({'visibility': 'visible'});
                        }
                });
        }
        
        $('.label-insert-plus').unbind('click').on('click', function() {
                var parent_object       = $(this).parents('.sp-variation-main');
                var object_name         = parent_object.find('input[name="sp-variations-name-mapping"]').attr('data-value');
                var object_old          = parent_object.find('.sp-type-value-sub').last();
                if ( parent_object.find('.label-remove-plus').hasClass('style-hide') ) {
                        parent_object.find('.label-remove-plus').toggleClass('style-hide');
                }
                parent_object.find('label.error').remove();
                object_old.find('.sp-variations-values-ebay').select2('destroy');
                var object_ole_rel      = object_old.attr('rel');
                var object_clone        = object_old.clone();
                var object_clone_rel    = parseInt(object_ole_rel) + 1;
                object_clone.find('select.sp-variations-values-ebay option').remove();
                object_clone.find('select').append("<option value='0'>"+$('.choose-variation-value').val()+"</option>");
                object_clone.attr('rel', object_clone_rel);
                object_clone.find('select.sp-variations-values-ebay').attr('name', 'sp-variations-values-ebay-mapping-'+ object_name + '-' + object_clone_rel);
                object_clone.find('select.sp-variations-values-ebay').attr('id', 'sp-variations-values-ebay-'+ object_name + '-' + object_clone_rel);
                object_clone.find('.sp-attribute-main-ebay').attr({'name': 'sp-attribute-main-ebay-'+ object_name + '-' + object_clone_rel, 'value': '', 'data-value': 0, 'rel': 0, 'data-key': 0});
                object_old.after(object_clone);
                object_old.find('.sp-variations-values-ebay').select2();
                object_clone.find('.sp-variations-values-ebay').select2();
                object_clone.find('select.sp-variations-values-ebay').rules('add', { required: true, both: true });
                object_clone.find('input.sp-attribute-main-ebay').rules('add', { required: true });
                clone_opening();
                object_clone.find('.sp-edit-new').each(function() {
                        spEditNew(this);
                });
        });
        
        $('.label-remove-plus').unbind('click').on('click', function() {
                var parent_object       = $(this).parents('.sp-variation-main');
                if ( parent_object.find('.sp-select-main-type').length > 1 ) {
                        parent_object.find('.sp-type-value-sub').last().remove();
                }
                if ( parent_object.find('.sp-select-main-type').length == 1 && !parent_object.find('.label-remove-plus').hasClass('style-hide') ) {
                        parent_object.find('.label-remove-plus').toggleClass('style-hide');
                }
        });
        
        
        function removeFocusKey(focus, rel) {
                var input = $(focus);
                var sclass = $(focus).attr('class').split(' ');
                sclass = sclass[0];
                $(input).find('option').show();
                if ( sclass === 'sp-variations-values-ebay' ) {
                        var sname = $(focus).attr('name').replace('-' + rel, '');
                        $('select[name^="'+sname+'"] option').removeAttr('disabled');
                        $('select[name^="'+sname+'"] option:selected').each(function( index, option ) {
                                if ( previous !== $(option).text() ) {
                                        $(input).find("option[value=\'" + option.value + "\']").attr({'disabled' : 'disabled'});
                                }
                                else if ( $(option).val() !== '0' ) {
                                        $(input).find("option[value=\'" + option.value + "\']").attr({'disabled' : 'disabled'});
                                }
                        });
                }
                else if ( sclass === 'variations-name-ebay' ) {
                        var sname = $(focus).attr('name').slice(0, -1);
                        $('select[name^="'+sname+'"] option').removeAttr('disabled');
                        $('select[name^="'+sname+'"] option:selected').each(function( index, option ) {
                                if ( previous !== $(option).text() ) {
                                        $(input).find("option[value=\'" + option.value + "\']").attr({'disabled' : 'disabled'});
                                }
                                else if ( $(option).val() !== '0' ) {
                                        $(input).find("option[value=\'" + option.value + "\']").attr({'disabled' : 'disabled'});
                                }
                        });
                }
                $(input).find('option:selected').removeAttr('disabled');
        }
        
        function match_value(parent, group) {
                parent.find('.value-type-group').each(function() {
                        if ( !$(this).find('.select-main-type').hasClass('.style-hide') ) {
                                var search_current_value    = $(this).find('.root-value input[name="variations-value-mapping"]').attr('data-value');
                                if ( search_current_value == $('select#variations-values-ebay-select-' + group).find('option[value="'+search_current_value+'"]').val() && typeof($(this).find('select.variations-values-ebay option[value="'+search_current_value+'"]').val()) === 'undefined' ) {
                                        $(this).find('select.variations-values-ebay').select2('destroy'); 
                                        $(this).find('select.variations-values-ebay').append("<option value='"+search_current_value+"'>"+search_current_value+"</option>").val(search_current_value);
                                        $(this).find('select.variations-values-ebay').select2();
                                }
                                else if ( search_current_value == $('select#variations-values-ebay-select-' + group).find('option[value="'+search_current_value+'"]').val() ) {
                                        $(this).find('select.variations-values-ebay').select2('destroy');
                                        $(this).find('select.variations-values-ebay').val(search_current_value);
                                        $(this).find('select.variations-values-ebay').select2();
                                }
                                else if ( typeof($(this).find('select.variations-values-ebay option:selected').val()) !== 'undefined' && $(this).find('select.variations-values-ebay option:selected').text() !== $('.choose-variation-value').val() ) {
                                        return;
                                }
                                else {
                                        $(this).find('select.variations-values-ebay').select2('destroy');
                                        $(this).find('select.variations-values-ebay').val(0);
                                        $(this).find('select.variations-values-ebay').select2();
                                }
                        }
                });
        }
        
        $('select.variations-name-ebay').each(function() {
                var type_group                              = $(this).val();
                var parent_group                            = $(this).parents('.variation-main').first();
                if ($(this).val() !== '0' ) {
                        match_value(parent_group, type_group);
                        if ( $(this).find('option:selected').attr('mode') === '2' ) {
                                parent_group.find('.edit-new').hide();
                        }
                        else {
                                parent_group.find('.edit-new').show();
                        }
                }
                else {
                        $(this).parents('.variation-main').find('.edit-new').filter(':visible').each(function() {
                                if ( $(this).children('.fa-eraser').length === 0 ) {
                                        $(this).trigger('click');
                                }
                        });
                }
        });
        
        
        clone_opening();
        function clone_opening() {
                $('select.variations-values-ebay').unbind('click').on("select2-opening", function () {
                        var type_group                                      = $(this).parents('.variation-main').first().find('select.variations-name-ebay').val();
                        if ( !$(this).hasClass('active-click') || $(this).attr('rel') !== type_group) {
                                var old_select                              = $(this).clone();
                                var object                                  = $(this);
                                var object_value                            = $(this).val();
                                var object                                  = $(this).addClass('active_click').attr('rel', type_group);
                                var object_parent                           = object.parent();
                                var cloned                                  = $('select#variations-values-ebay-select-' + type_group).find('option').clone();
                                var parent_group                            = $(this).parents('.variation-main').first();
                                object_parent.find('option').remove();
                                object_parent.find('select').append("<option value='0'>"+$('.choose-variation-value').val()+"</option>").find('option').replaceWith(cloned);
                                object_parent.find('select option').filter(":containsIN('"+object_value+"')").each(function() {
                                        if ( object_value.toLowerCase() === $(this).text().toLowerCase() ) {
                                                object_parent.find('select').val(object_value).find('option[value="'+object_value+'"]').attr('selected', 'selected');
                                                return;
                                        }
                                });
                        }
                        var previous_rel                                    = $(this).parents('.sp-type-value-sub').attr('rel');
                        removeFocusKey(this, previous_rel);
                });
                
                $.extend($.expr[":"], {
                        "containsIN": function(elem, i, match, array) {
                        return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                        }
                });
                
                $('select.variations-name-ebay').unbind('click').on("select2-opening", function () {
                        previous                                            = $(this).find('option:selected').text();
                        $(this).parent().find('.select2-search').addClass('insert-variation-label');
                        $parent                                             = $(this);
                        $(this).parent().find('.insert-variation-label').unbind('click').click(function() {
                                if ( $(this).find('input').val() !== '' ) {
                                        $children                           = $(this).find('input');
                                        var no_pass
                                        $parent.find('option').filter(":containsIN('"+$(this).find('input').val()+"')").each(function() {
                                                if ( $children.val().toLowerCase() === $(this).text().toLowerCase() ) {
                                                        no_pass             = 1;
                                                        return;
                                                }
                                        });
                                        if ( typeof(no_pass) === 'undefined' ) {
                                                var insert_value            = $(this).find('input').val();
                                                $('.select2-drop').unbind('blur').blur();
                                                bootbox.confirm($('.are-you-sure-insert').val() + " " + $(this).find('input').val() + " ?", function(result) {
                                                        if(result) {
                                                                $('.type-name').find('select.variations-name-ebay').append("<option mode='-1' value='"+insert_value+"'>"+insert_value+"</option>");
                                                                $('select.variations-name-ebay').select2('close');
                                                                $parent.select2('val', insert_value);
                                                        }
                                                });
                                        }
                                        no_pass = null;
                                }
                        });
                        var previous_rel                                    = $(this).parents('.sp-type-value-sub').attr('rel');
                        removeFocusKey(this, previous_rel);
                });
                
                $('select.sp-variations-values-ebay').unbind('click').on("select2-opening", function () {
                        previous                                            = $(this).find('option:selected').text();
                        var type_group                                      = $(this).parents('.sp-variation-main').first().attr('rel');
                        if ( !$(this).hasClass('active-click') || $(this).attr('rel') !== type_group) {
                                var old_select                              = $(this).clone();
                                var object                                  = $(this);
                                var object_value                            = $(this).val();
                                var object                                  = $(this).addClass('active_click').attr('rel', type_group);
                                var object_parent                           = object.parent();
                                var cloned                                  = $('select#variations-values-ebay-select-' + type_group).find('option').clone();
                                var parent_group                            = $(this).parents('.sp-variation-main').first();
                                object_parent.find('option').remove();
                                object_parent.find('select').append("<option value='0'>"+$('.choose-variation-value').val()+"</option>").find('option').replaceWith(cloned);
                                object_parent.find('select option').filter(":containsIN('"+object_value+"')").each(function() {
                                        if ( object_value.toLowerCase() === $(this).text().toLowerCase() ) {
                                                object_parent.find('select').val(object_value).find('option[value="'+object_value+'"]').attr('selected', 'selected');
                                                return;
                                        }
                                });
                                
                        }
                        var previous_rel                                    = $(this).parents('.sp-type-value-sub').attr('rel');
                        removeFocusKey(this, previous_rel);
                });
                
                
                $('select.variations-name-ebay').unbind('change').on('change', function() {
                        var type_group                              = $(this).val();
                        var parent_group                            = $(this).parents('.variation-main').first();
                        parent_group.find('select.variations-values-ebay option').remove();
                        parent_group.find('select.variations-values-ebay').attr('rel', type_group).append("<option value='0'>"+$('.choose-variation-value').val()+"</option>");
                        if ($(this).val() !== '0' ) {
                                match_value(parent_group, type_group);
                                if ( $(this).find('option:selected').attr('mode') === '2' ) {
                                        parent_group.find('.edit-new').hide();
                                }
                                else {
                                        parent_group.find('.edit-new').show();
                                }
                        }
                });
        }
        
        $('.li-exclude-type').unbind('click').click(function() {
                if ( $(this).hasClass('active') ) {
                        $('.li-exclude-type').removeClass('active');
                }
                else {
                        $('.li-exclude-type').removeClass('active');
                        $(this).toggleClass('active');
                }
        });
        
        $('.li-include-type').unbind('click').click(function() {
                if ( $(this).hasClass('active') ) {
                        $('.li-include-type').removeClass('active');
                }
                else {
                        $('.li-include-type').removeClass('active');
                        $(this).toggleClass('active');
                }
        });
        
        $('.sp-exchange-type-right').unbind('click').click(function() {
                $(this).parents('.ul-exclude-type').removeClass('group-error').find('label.error').remove();
                $('.li-exclude-type.active').appendTo('.ul-include-type');
                var group = $('.li-exclude-type.active')
                        .removeClass('li-exclude-type')
                        .addClass('li-include-type')
                        .removeClass('active')
                        .attr('data-group');
                
                $('.sp-variation-main').filter('[rel="'+group+'"]').toggleClass('style-hide');
                if ( $('.sp-variation-main.style-hide').length === $('.sp-variation-main').length ) {
                        $('.variation-value-title').removeClass('style-hide').addClass('style-hide');
                }
                else {
                        $('.variation-value-title').removeClass('style-hide');
                }
        });
        
        $('.sp-exchange-type-left').unbind('click').click(function() {
                $('.li-include-type.active').appendTo('.ul-exclude-type');
                var group = $('.li-include-type.active')
                        .removeClass('li-include-type')
                        .addClass('li-exclude-type')
                        .removeClass('active')
                        .attr('data-group');
                
                $('.sp-variation-main').filter('[rel="'+group+'"]').toggleClass('style-hide');
                if ( $('.sp-variation-main.style-hide').length === $('.sp-variation-main').length ) {
                        $('.variation-value-title').removeClass('style-hide').addClass('style-hide');
                }
        });
        
        $('.sp-help').each(function() {
                var help_text                               = $(this).text();
                var first_value                             = $(this).parents('.sp-variation-main').find('input[name="sp-variations-name-mapping"]').attr('data-value');
                var first_rel                               = $(this).parents('.sp-variation-main').attr('rel');
                var second_value                            = 'B';
                var second_object                           = $(this).parents('.sp-value-type-group').find('.sp-select-main-type');
                if ( !second_object.hasClass('.style-hide') ) {
                        second_object_value                 = $('#variations-values-ebay-select-'+first_rel+' option:last').text();
                        second_value                        = second_object_value !== $('.choose-variation-value').val() ? second_object_value : second_value;
                }
                $(this).text(help_text.replace(/\{A\}/g, first_value).replace(/\{B\}/g, second_value));
        });

        $("#form-submit").on('submit', function(e) {
                e.preventDefault();
                data                                        = [];
                var data_groups                             = [];
                var data_values                             = [];
                var data_fixed                              = [];

                    
                    
                        $('html, body').animate({ scrollTop: 0 }, 0);
                    
                        $('.variation-main').each(function() {
                                if ( $(this).find('select.variations-name-ebay option:selected').text() !== $('.choose-variation-type').val() && typeof($(this).find('select.variations-name-ebay option:selected').attr('mode')) !== 'undefined' ) {
                                        var data_group                               = {
                                                'id_profile'                        : $('.id_profile').val(),
                                                'id_attribute_group'                : $(this).attr('rel'),
                                                'name_attribute'                    : $(this).find('input[name="variations-name-mapping"]').attr('data-value'),
                                                'name_attribute_ebay'               : $(this).find('select.variations-name-ebay option:selected').text(),
                                                'mode'                              : $(this).find('select.variations-name-ebay option:selected').attr('mode'),
                                                'tab'                               : active_li,
                                                'id_site'                           : $("#id-site").val(),
                                                'id_packages'                       : $("#id-packages").val(),
                                                'step'                              : $("#popup-step").val(),
                                        };
                                        data.push(data_group);
                                }
                        });
                        
                        if ($.isEmptyObject(data)) {
                                var data_value              = {
                                        'id_site'               : $("#id-site").val(),
                                        'id_packages'           : $("#id-packages").val(),
                                };
                                data.push(data_value);
                        }

                        var data_selected = {
                                'id_site'       : $("#id-site").val(),
                                'id_packages'   : $("#id-packages").val(),
                                'id_popup'      : $("#popup-step").val(),
                        };
                        wizard_data.push(data_selected);
                        
                        ajax_save(e);
        });
