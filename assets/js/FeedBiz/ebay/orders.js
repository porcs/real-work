function orderView(e) {
    $('#viewResult').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i></div>');
    var order_button = $(e);
    $.ajax({
        type: 'POST',
        url: base_url + 'webservice/view_order/' + $(order_button).val() + '/1',
        dataType: 'json',
        success: function (data) {
            //console.log($('#viewResult'), data);  
            $('#viewResult').html(data.result);
        },
        error: function (data) {
            console.log(data);    
        }
    });
}

        var data                            = new Array();
        var cloned                          = $("div.close").clone();
        var offset  = 0;
        var limit   = 10;
        var first   = 0;
        var ColVis_first = 0;
        var showChar = 80;
        var ellipsestext = '...';
        var moretext = "more";
        var lesstext = "less";
        function morelink(ele) {
            $(ele).click(function(e){
                e.preventDefault();
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
    }
    console.log('0');    
        var oTable = $('#statistics').dataTable( {
            "processing": true,
            "serverSide": true,
            "searching": true,
            "bStateSave": true,
            "sDom": 'C<"clear">lfrtip',
            "oColVis": {
                aiExclude: [0, 9],
//                bRestore: true,
//                activate: "mouseover",
                bShowAll: $('.show-all').val(),
            },
            "ajax": base_url + "ebay/orders_processing?limit=" + limit + '&offset=' + offset + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val(),

            fnPreDrawCallback : function( oSettings ) {
                    $('#statistics_processing').hide();
                    limit   = oSettings._iDisplayLength;
                    offset  = oSettings._iDisplayStart;
                    if ( offset % limit > 0 ) {
                            offset = Math.floor(offset / limit);
                    }
                    var id_order            = $('input[rel="id_order"]').val();
                    var id_order_ref        = $('input[rel="id_order_ref"]').val();
                    var id_buyer            = $('input[rel="id_buyer"]').val();
                    var order_status        = $('input[rel="order_status"]').val();
                    var country_name        = $('input[rel="country_name"]').val();
                    var mp_channel          = $('input[rel="mp_channel"]').val();
                    var payment_method      = $('input[rel="payment_method"]').val();
                    var total_paid          = $('input[rel="total_paid"]').val();
                    var order_date          = $('input[rel="order_date"]').val();
                    var invoice_no          = $('input[rel="invoice_no"]').val();
//                    var all                 = $('.dataTables_filter input').val();
                    
                    aSorting                = [];
                    aSorting.column         = oSettings.aaSorting[0][0];
                    aSorting.by             = oSettings.aaSorting[0][1];
                    dataFilter = {
                        'id_orders'         : id_order,
                        'id_order_ref'      : id_order_ref,
                        'id_buyer'          : id_buyer,
                        'order_status'      : order_status,
                        'country_name'      : country_name,
                        'mp_channel'        : mp_channel,
                        'payment_method'    : payment_method,
                        'total_paid'        : total_paid,
                        'order_date'        : order_date,
                        'invoice_no'        : invoice_no,
                        'sort_name'         : aSorting.column,
                        'sort_by'           : aSorting.by,
//                        'all'               : all,
                    };
                    var count = 0
                    $.each(dataFilter, function(index, val) {
                            if ( count === aSorting.column) {
                                    dataFilter.sort_name        = index;
                            }
                            count++;
                    });
                    oSettings._iDisplayStart    = offset;
                    oSettings.iDraw             = Math.floor(oSettings._iDisplayStart / limit);
                    oSettings.ajax              = base_url + "ebay/orders_processing?limit=" + limit + '&offset=' + offset + '&' + $.param(dataFilter) + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val() + '&sort_status=' + $('.order_status').val();
                    
                    if (first == 0) {
                            $(".head_text").parent().append("<button class='btn btn-small btn-primary import-orders' id='download-orders' type='button'><span class='send'>Import Orders</span><span class='sending' style='display:none;'><i></i> Import..</span></button>");
                            $('#statistics_wrapper')
                                    .append("<div class='row'><div class='col-xs-4'></div><div class='col-xs-8'></div></div>");
                            $('.dataTables_paginate')
                                    .appendTo("#statistics_wrapper > .row > .col-xs-8:last");
                            $('#statistics_wrapper')
                                    .prepend("<div class='row separator bottom'><div class='col-md-6 form-horizontal'></div><div class='col-md-6'></div></div>");
                            $('#statistics_length')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length')
                                    .append("<div class='col-xs-6 col-md-6'></div>")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length > label > select')
                                    .addClass('.selectpicker')
                                    .appendTo("#statistics_length > .col-xs-6.col-md-6")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length > label:first')
                                    .addClass('col-xs-2 col-md-2 control-label')
                                    .text('Per page');
                            $('.row.separator.bottom').find('.col-md-6:first').append('<div class="dataTables_header"><label class="control-label">'+$('.title-orders').val()+'</label></div>');
                            $('#statistics_filter').hide();
                            $('.ColVis')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last");
                            $('.dataTables_info')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last");
                            first++;
                            $(".prev a").each(function() {
                                        $(this).text($(this).text().replace("← ", ''));
                            });
                            $(".next a").each(function() {
                                        $(this).text($(this).text().replace(" → ", ''));
                            });
                    }
                    
            },
            fnDrawCallback: function( oSettings ) {
                    if ( ColVis_first === 0 ) {
                            $('.ColVis_Button').trigger('click');
                            $('.ColVis_collection').find('li').each(function() {
                                    var $li                 = $(this);
                                    if ( $(this).find('.cb-checkbox').length === 0 ) {
                                            if ( $(this).find('span').length > 0 ) {
                                                    if ( $(this).find('input').prop('checked') === true ) {
                                                            $(this).find('label').addClass('cb-checkbox cb-sm checked');
                                                    }
                                                    else {
                                                            $(this).find('label').addClass('cb-checkbox cb-sm');
                                                    }
                                                    $(this).checkBo();
                                                    var checkbox = $(this);
                                                    $(this).find('.cb-checkbox').unbind('click');
                                                    $(this).find('i').unbind('click').on('click', function(e) {
                                                            if ( e.target.tagName.toString() === 'INPUT') {
                                                                    return false;
                                                            }
                                                            e.preventDefault();
                                                            $(this).parents('li').trigger('click');
                                                    });
                                                    $(this).find('span:last').on('click', function(e) {
                                                            e.preventDefault();
                                                            $(this).parents('li').trigger('click');
                                                    });
                                                    $(this).on('click', function(e) {
                                                            if ( $(this).find('input').is(':checked') ) {
                                                                    $(this).find('label.cb-checkbox').addClass('checked');
                                                            }
                                                            else if ( $(this).find('input').is(':checked') === false ) {
                                                                    $(this).find('label.cb-checkbox').removeClass('checked');
                                                            }
                                                    });

                                                    setTimeout(function() {
                                                            if ( oTable.width() < 1200 ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.country-name').val() && $li.find('input').is(':checked') ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            else if ( oTable.width() >= 1200  ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.country-name').val() && $li.find('input').is(':checked') === false ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            if ( oTable.width() < 1100 ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.id-buyer').val() && $li.find('input').is(':checked') ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            else if ( oTable.width() >= 1100  ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.id-buyer').val() && $li.find('input').is(':checked') === false ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            if ( oTable.width() < 1000 ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.order-date').val() && $li.find('input').is(':checked') ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            else if ( oTable.width() >= 1000  ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.order-date').val() && $li.find('input').is(':checked') === false ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            if ( oTable.width() < 800 ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.payment-method').val() && $li.find('input').is(':checked') ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            else if ( oTable.width() >= 800  ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.payment-method').val() && $li.find('input').is(':checked') === false ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            if ( oTable.width() < 650 ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.total-paid').val() && $li.find('input').is(':checked') ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                            else if ( oTable.width() >= 650  ) {
                                                                    if ( $.trim($li.find('span:last').text()) === $('.total-paid').val() && $li.find('input').is(':checked') === false ) {
                                                                            $li.trigger('click');
                                                                    }
                                                            }
                                                    }, 100);
                                            }
                                            if ( $(this).text() === $('.show-all').val() ) {
                                                    var checkbox = $(this);
                                                    $(this).find('.cb-checkbox').unbind('click');
                                                    $(this).unbind('click').click(function() {
                                                            $('.ColVis_collection').find('input').each(function() {
                                                                    if ( $(this).prop('checked') === false ) {
                                                                            $(this).parents('li').trigger('click');
                                                                    }
                                                            });
                                                    })
                                            }
                                    }
                            });
                    }
                    ColVis_first++;
                
                    $('#statistics_wrapper .table tbody tr').each(function(index) {
                            if ( !$('td:last', this).hasClass("dataTables_empty") ) {
                                    var status = $('td:last', this).text();
                                    var id_orders = $('td:first', this).text();
                                    if ( status === 'completed_send') {
                                            $('td:last', this).text('')
                                                    .append('<button type="button" class="send-orders btn btn-label send-orders-label" disabled value=""><span class="send-status">Sent</span></button>')
                                                    .find('.send-orders', this)
                                                    .val(id_orders);
                                    }
                                    else if ( status === 'error') {
                                            $('td:last', this).text('')
                                                    .append('<button type="button" class="send-orders btn btn-label" value=""><span class="send-status">Sent</span></button>')
                                                    .find('.send-orders', this)
                                                    .val(id_orders);
                                    }
                                    else if ( status === 'completed_not_send') {
                                            $('td:last', this).text('')
                                                    .append('<button type="button" class="send-orders btn btn-label" value=""><span class="send-status">Sent</span></button>')
                                                    .find('.send-orders', this)
                                                    .val(id_orders);
                                    }
                            }
                    });
                    sendOrder();
            },
            fnRowCallback: function( nRow, aData, iDisplayIndex ) {
            	var message_text = aData.comment;
            	var invoiceTD = $('td.invoice', nRow);
            	if(aData.status == 'error' && message_text != undefined)
            	{            		
                    if( message_text.length > showChar) {
                        var c = message_text.substr(0, showChar);
                        var h = message_text.substr(showChar-1, message_text.length - showChar);
                        var html = c + '<span class="moreellipses">' + ellipsestext+ ' </span><span class="morecontent"><span>' + h + '</span>  <a href="" class="morelink red">' + moretext + '</a></span>';
                        $(invoiceTD).html(html);
                        morelink($(invoiceTD).find('.morelink'));
                    }
                    else{
                        $(invoiceTD).html(message_text);
                    }
            	}
            },
            "columns": [
                { "data": "id_orders", "sClass": "center",
                    render: function(data){
                        var id_orders = (data) ? '<button type="button" class="link" data-toggle="modal" data-target="#ebayOrder" value="' + data + '" onclick="orderView(this);">' + data + '</button>' : '';
                        return id_orders;
                    }
                },
                { "data": null, "bSortable": false, "sClass": "center",
                    render: function (data) {
                        var id_marketplace_order_ref =  (data.id_marketplace_order_ref) ? data.id_marketplace_order_ref : '';
                        return id_marketplace_order_ref;                   
                    }
                },
                { "data": null, "bSortable": false, "sClass": "center",
                    render: function (data) {
                        var id_buyer =  (data.id_buyer) ? data.id_buyer : '';
                        
                        return id_buyer;                   
                    }
                },
                { "data": null, "bSortable": false, "sClass": "center",
                    render: function (data) {
                        if ( data.order_status === 'Completed') {
                                var order_status = '<span class="label label-success label-show label-show-success">' + data.order_status + '</span>';
                        }
                        else if ( data.order_status === 'Active') {
                                var order_status = '<span class="label label-info label-show label-show-info">' + data.order_status + '</span>';
                        }
                        else if ( data.order_status === 'Shipped') {
                                var order_status = '<span class="label label-warning label-show label-show-warning">' + data.order_status + '</span>';
                        }
                        else if ( data.order_status === 'Cancelled') {
                                var order_status = '<span class="label label-danger label-show label-show-alert">' + data.order_status + '</span>';
                        }
                        return order_status;                   
                    }
                },
                { "data": null, "bSortable": false, "sClass": "center",
                    render: function (data) {
                        var country_name =  (data.country_name) ? data.country_name : '';
                        return country_name;                   
                    }
                },
                { "data": null, "bSortable": false, "sClass": "center",
                    render: function (data) {
                        var mp_channel =  (data.mp_channel) ? data.mp_channel : '';
                        return mp_channel;                   
                    }
                },
                { "data": null, "sClass": "center",
                    render: function (data) {
                        var payment_method =  (data.payment_method) ? data.payment_method : '';
                        return payment_method;                   
                    }
                },
                { "data": null, "sClass": "center",
                    render: function (data) {
                        var total_paid =  (data.total_paid) ? data.total_paid : '';
                        return total_paid;                   
                    }
                },
                { "data": null, "sClass": "center",
                    render: function (data) {
                        var order_date =  (data.order_date) ? data.order_date : '';
                        return order_date;                   
                    }
                },
                { "data": null, "bSortable": false, "sClass": "center invoice",
                    render: function (data) {
                        var invoice_no =  (data.invoice_no) ? data.invoice_no : '';
                        return invoice_no;                   
                    }
                },
                { "data": null, "bSortable": false, "sClass": "center",
                    render: function (data) {
                        var status =  (data.status) ? data.status : '';
                        if ( status === 'ClosedWithoutPayment') {
                                status = 'Closed with no payment received from the buyer.'
                        }
                        else if ( status === 'ClosedWithPayment') {
                                status = 'Closed with payment received from the buyer.'
                        }
                        else if ( status === 'ClosedWithoutPayment') {
                                status = 'Closed without payment'
                        }
                        else if ( status === 'ClosedWithoutPayment') {
                                status = 'Closed without payment'
                        }
                        else if ( data.order_status === 'Completed' && status === '1') {
                                status = 'completed_send'
                        }
                        else if ( data.order_status === 'Completed' && status === 'error') {
                                status = 'error'
                        }
                        else if ( data.order_status === 'Completed' && status === '') {
                                status = 'completed_not_send'
                        }
                        return status;                   
                    }
                }
            ],
            "oLanguage": {
                "sInfo": "<strong>_START_</strong> - <strong>_END_</strong> of <strong>_TOTAL_</strong>",
                "sInfoEmpty": "",
                "oPaginate": {
                    "sFirst": "First page",
                },
            },
            "aaSorting": [[ 8, "desc" ]],
            "aLengthMenu": [
                [10, 25, 50, 100, 200, -1],
                [10, 25, 50, 100, 200, "All"]
            ], iDisplayLength: 10,
        } );
        
        $('input').on('keyup', function() {
                oTable.fnDraw();
        });
        
        
        $(document).mouseup(function (e) {
                if ($('.ColVis_collection').has(e.target).length === 0){
                        $('.ColVis_collection').hide();
                        $('.ColVis_catcher').hide();
                }
        });

        
        $('#download-orders').on('click', function() {
                var $obj = $(this);
                $(this).attr('disabled', 'disabled');
                $obj.find('.send').hide();
                $obj.find('.sending').show();
                var data = [];
                var data_selected = {
                    'id_site'       : $("#id-site").val(),
                    'id_packages'   : $("#id-packages").val(),
                    'order_status'  : 'All',
                };

                data.push(data_selected);
                
                $.ajax ({
                        type        : "POST",
                        url         : base_url + "ebay/get_orders",
                        data        : {data : data},
                        success     : function(result) {
                                    $obj.removeAttr('disabled');
                                    $obj.find('.send').show();
                                    $obj.find('.sending').hide();
                                    console.log($.trim(result));
                                    if ( $.trim(result) == 'Success' ) {
                                            oTable.fnDraw();
                                    }
                        }
                });
        });
        
        
        function sendOrder() {
                $('.send-orders').on('click', function(){
                    var send = $(this);
                    var id_order = send.val();

                    //update invoice
                    updateInvoice(id_order, '');
                                        
                    var strdata = new Object();
                    strdata.id_order    = id_order;
                    strdata.id_user     = $('.id_user').val();


                    $.ajax({
                        type: 'POST',
                        url:  base_url + "webservice/send_orders/" + id_order,
                        data: strdata,
                        success: function(rdata) {
                                    jdata = $.parseJSON(rdata);
                            console.log(rdata);
                            if(!jdata.error && jdata.invoice[0].trim() != '')
                            {
                                send.find('.send-status').html('Sent');
                                send.find('i').removeClass('icon-spinner icon-spin').addClass('icon-check');
                                send.attr('disabled', true);

                                //update invoice
                                updateInvoice(jdata.order, jdata.invoice[0]);
                            }
                            else
                            {
                                send.find('.send-status').html('Send');
                                send.find('i').removeClass('icon-spinner icon-spin').addClass('icon-upload');
                                $('#send-products-message').show().addClass('alert alert-danger').html(rdata.error);

                                //update failuer
                                if(jdata.error.trim() != ''){
                                    updateInvoice(jdata.order, jdata.error);
                                }
                                else{
                                    updateInvoice(jdata.order, 'Failure');
                                }
                            }
                        },
                        error: function(rerror){
                            send.find('.send-status').html('Send');
                            send.find('i').removeClass('icon-spinner icon-spin').addClass('icon-upload');
                            $('#send-products-message').show().addClass('alert alert-danger').html(rerror);
                            //console.log(error);
                        }
                    });


                });
            }

        function updateInvoice(order, message){
        	$('#statistics tbody tr').filter(function(){
            	return $('td:first', this).html() == order;
            }).children('td.invoice').html(message);
        }