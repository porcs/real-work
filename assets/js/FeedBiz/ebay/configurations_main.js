
        var profile_name                    = 1;
        var chk_value                       = '';
        var data                            = new Array();
        var category_id                     = 0;
                
        $('.btn-get-store').unbind('click').on('click', function() {
                    showWaiting(true);
                    var time =0;
                    var num = $('body').find('input').length;
                    if(num > 0){
                            time =3000;
                    } 
                    setTimeout(function(){
                        var dataValue           = {
                                'id_site'       : $("#id-site").val(),
                                'id_packages'   : $("#id-packages").val(),
                                'get_store'     : 1,
                        };
                        data.push(dataValue);

                        $.ajax({
                                type            : 'POST',
                                url             : base_url + "ebay/setJsonAdvanceAllStoreCategory/", 
                                data            : { data : data },
                                success         : function(response) {  
                                        showWaiting(false);
                                }   
                        }); 
                 },time);
        });
        
        $('.btn-primary-categories').unbind('click').click(function() {
                var element                 = $(this);
                $('.btn-primary-categories').colorbox({
                        iframe          :true,
                        href            :base_url + 'ebay/search_primary_categories/'+ $('#id-packages').val()+'/9889',
                        innerWidth      :'90%',
                        innerHeight     :'90%',
                        escKey          : false,
                        overlayClose    : false,
                        onClosed:function(ele){
                                if ( $('.category-jquery-full-name').val() !== '' || $('.category-jquery-name').val() !== '' || $('.category-jquery-id').val() !== '' ) {
                                        var category_current   = $('.category-jquery-full-name').val();
                                        var name               = $('.category-jquery-name').val();
                                        var id                 = $('.category-jquery-id').val();
                                        $('.category-jquery-full-name').val('');
                                        $('.category-jquery-name').val('');
                                        $('.category-jquery-id').val('');
                                        element.parent().parent().find('input').first().val(name);
                                        element.parent().parent().find('input').last().val(id);
//                                        $('#profile-name').val(category_current);
                                 }
                        },
                });
        });
        
        $('.btn-secondary-categories').unbind('click').click(function() {
                var element                 = $(this);
                $('.btn-secondary-categories').colorbox({
                        iframe          :true,
                        href            :base_url + 'ebay/search_primary_categories/'+ $('#id-packages').val()+'/9889',
                        innerWidth      :'90%',
                        innerHeight     :'90%',
                        escKey          : false,
                        overlayClose    : false,
                        onClosed:function(ele){
                                 var category_current   = $('.category-jquery-full-name').val();
                                 var name               = $('.category-jquery-name').val();
                                 var id                 = $('.category-jquery-id').val();
                                 element.parent().parent().find('input').first().val(name);
                                 element.parent().parent().find('input').last().val(id);
                        },
                });
        });
        
        $('.btn-primary-store-categories').unbind('click').click(function() {
                var element                 = $(this);
                $('.btn-primary-store-categories').colorbox({
                        iframe          :true,
                        href            :base_url + 'ebay/search_store_categories/'+ $('#id-packages').val()+'/9889',
                        innerWidth      :'90%',
                        innerHeight     :'90%',
                        escKey          : false,
                        overlayClose    : false,
                        onClosed:function(ele){
                                 var category_current   = $('.category-jquery-full-name').val();
                                 var name               = $('.category-jquery-name').val();
                                 var id                 = $('.category-jquery-id').val();
                                 element.parent().parent().find('input').first().val(name);
                                 element.parent().parent().find('input').last().val(id);
                        },
                });
        });
        
        $('.btn-secondary-store-categories').unbind('click').click(function() {
                var element                 = $(this);
                $('.btn-secondary-store-categories').colorbox({
                        iframe          :true,
                        href            :base_url + 'ebay/search_store_categories/'+ $('#id-packages').val()+'/9889',
                        innerWidth      :'90%',
                        innerHeight     :'90%',
                        escKey          : false,
                        overlayClose    : false,
                        onClosed:function(ele){
                                 var category_current   = $('.category-jquery-full-name').val();
                                 var name               = $('.category-jquery-name').val();
                                 var id                 = $('.category-jquery-id').val();
                                 element.parent().parent().find('input').first().val(name);
                                 element.parent().parent().find('input').last().val(id);
                        },
                });
        });
        
        function style_hide(ele) {
                $(ele).parent().parent().parent().find('.clear').toggleClass('style-hide');
                if ( $(ele).is(':checked') === true ) {
                        $(ele).prop('checked', true);
                }
                else {
                        $(ele).prop('checked', false);
                }
        }
        
        $('input[rel="category_setup"]').each(function() {
                if ( $(this).parents('.form-group').find('.checkbox-custom > input').is(':checked') === true ) {
                        style_hide($(this).parents('.form-group').find('.checkbox-custom > input'));
                }
        });
        
        $('input[name="form-secondary-categories-checkbox"').unbind('click').click(function() {
                style_hide($(this));
        });
        
        $('input[name="form-primary-store-categories-checkbox"').unbind('click').click(function() {
                style_hide($(this));
        });
        
        $('input[name="form-secondary-store-categories-checkbox"').unbind('click').click(function() {
                style_hide($(this));
        });
        
        $.validator.addMethod("both", function(value, element) {
                return this.optional(element) || value != '';
                }, 
                $('input[name="validate-out-of-stock-required"]').val()
        );

        $.validator.addMethod(
                "minvalue",
                function(value, element) {
                    return this.optional(element) || (value >= 0 && value <= 1000);
                },
                $('input[name="validate-out-of-stock-minvalue"]').val()
        );
        $.validator.addMethod(
                "maxvalue",
                function(value, element) {
                    return this.optional(element) || (value > 0 && value <= 1000);
                },
                $('input[name="validate-out-of-stock-minvalue"]').val()
        );

        $.validator.addMethod(
                "profile_name",
                function(value, element) {
                        data                            = [];
                        var data_selected                   = {
                                'id_profile'                        : $('.id_profile').val(),
                                'id_site'                           : $("#id-site").val(),
                                'id_packages'                       : $("#id-packages").val(),
                                'profile_name'                      : $.trim(value),
                        };
                        data.push(data_selected);
                        if(chk_value !== value) {
                                $.ajax ({
                                        type       : "POST",
                                        url        : base_url + "ebay/get_profile_name_valid",
                                        data       : {data : data},
                                        error      : function(jqXHR, textStatus, errorThrown) {
                                                   notification_alert(errorThrown,'bad');
                                       },
                                       success     : function(save) {
                                                   if(chk_value !== value){
                                                            if ( save === 'Success') {
                                                                     profile_name = 1;
                                                                     chk_value = value;
                                                                     $('#profile-name-error').remove();
                                                                     $('#profile-name').removeClass('group-error');
                                                                     $('#profile-name').removeClass('error');
                                                            }
                                                            else {
                                                                     profile_name = 0;
                                                                     if(chk_value !== value){
                                                                            chk_value = value;
                                                                            profile_name = 0;
                                                                            $("#profile-name").unbind('blur').blur();
                                                                     }
                                                            }
                                                   }
                                       }
                                });
                        }
                        return this.optional(element) || profile_name === 1;
                },
                $('input[name="validate-profile-name-checked"]').val()
        );

        $("#form-submit").validate({
                rules: {
                        'profile-name' : {
                                required: true,
                                both: true,
                                profile_name: true,
                        },
                        'primary-categories' : {
                                required: true,
                                both: true,
                        },
                        'id-ebay-categories' : {
                                required: true,
                                both: true,
                        },
                        'maximum-quantity' : {
                                required: true,
                                maxvalue: 1000,
                        },
                        'out-of-stock' : {
                                required: true,
                                minvalue: 0,
                        }
                },
                ignore: [],
                messages: {
                        'profile-name' : {
                                required: $('input[name="validation-profile-name-required"]').val(),
                                both: $('input[name="validate-out-of-stock-required"]').val(),
                                profile_name: $('input[name="validate-profile-name-checked"]').val(),
                        },
                        'primary-categories' : {
                                required: $('input[name="validate-out-of-stock-required"]').val(),
                                both: $('input[name="validate-out-of-stock-required"]').val(),
                        },
                        'id-ebay-categories' : {
                                required: $('input[name="validate-out-of-stock-required"]').val(),
                                both: $('input[name="validate-out-of-stock-required"]').val(),
                        },
                        'out-of-stock' : {
                                required: $('input[name="validate-out-of-stock-required"]').val(),
                                minvalue: $('input[name="validate-out-of-stock-minvalue"]').val(),
                        },
                        'maximum-quantity' : {
                                required: $('input[name="validate-maximum-quantity-required"]').val(),
                                maxvalue: $('input[name="validate-maximum-quantity-maxvalue"]').val(),
                        }
                },
                errorPlacement: function(error, element) {
                        $('body').removeClass('group-error');
                        if ($(element).attr("id") == 'primary-categories' || $(element).attr("id") == 'id-ebay-categories' || $(element).attr("id") == 'profile-name') {
                                $(element).addClass('group-error');
                                error.insertAfter( $(element) );
                        }
                        else {
                                error.insertAfter($(element));
                        }
                },
                unhighlight: function (element) {
                        if ($(element).attr("id") == 'primary-categories' || $(element).attr("id") == 'id-ebay-categories' || $(element).attr("id") == 'profile-name') {
                                $(element).removeClass('group-error');
                        }
                },
                debug: true
        });
        
        validation_number();
        function validation_number() {
                $("#profile-name").valid();
                $("#profile-name").unbind('keyup').on("keyup", function() {
                        var valid = /^[a-zA-Z0-9\s]*$/.test(this.value),
                        val = this.value;

                        if(!valid){
                            console.log("Invalid input!");
                            this.value = val.substring(0, val.length - 1);
                        }

                        var e = window.event || e;
                            var keyUnicode = e.charCode || e.keyCode;
                            if (e !== undefined) {
                                    switch (keyUnicode) {
                                            case 16: break; // Shift
                                            case 17: break; // Ctrl
                                            case 18: break; // Alt
                                            case 27: this.value = ''; break; // Esc: clear entry
                                            case 35: break; // End
                                            case 36: break; // Home
                                            case 37: break; // cursor left
                                            case 38: break; // cursor up
                                            case 39: break; // cursor right
                                            case 40: break; // cursor down
                                            case 78: break; // N (Opera 9.63+ maps the "." from the number key section to the "N" key too!) (See: http://unixpapa.com/js/key.html search for ". Del")
                                            case 110: break; // . number block (Opera 9.63+ maps the "." from the number block to the "N" key (78) !!!)
                                            case 190: break; // .
                                    }
                        }
                });
        }
        
        $("#form-submit").on('submit', function(e) {
                e.preventDefault();
                if (    $("#out-of-stock").valid() 
                    && ($('.id_profile').val() === '0' || $("#primary-categories").valid()) 
                    && ($('.id_profile').val() === '0' || $("#id-ebay-categories").valid()) 
                    && ($('.id_profile').val() === '0' || $("#profile-name").valid()) 
                    && $('#maximum-quantity').valid() ) {
                        data                            = [];

                        data_selected                   = {
                                'id_profile'                        : $('.id_profile').val(),
                                'profile_name'                      : $('#profile-name').val(),
                                'profile_type'                      : $('.profile-type').val(),
                                'out_of_stock_min'                  : $.trim($("#out-of-stock").val()),
                                'maximum_quantity'                  : $.trim($("#maximum-quantity").val()),
                                'identifier_with_items'             : "ItemID",
                                'categories_name'                   : $("#primary-categories")  .val(),
                                'id_categories'                     : $("#id-ebay-categories").val(),
                                'secondary_categories_name'         : $("#secondary-categories").val(),
                                'id_secondary_categories'           : $("#id-ebay-secondary-categories").val(),
                                'store_categories_name'             : $("#primary-store-categories").val(),
                                'id_store_categories'               : $("#id-ebay-store-categories").val(),
                                'secondary_store_categories_name'   : $("#secondary-store-categories").val(),
                                'id_secondary_store_categories'     : $("#id-ebay-secondary-store-categories").val(),
                                'country_name'                      : $("#country-name").val(),
                                'currency'                          : $("#currency-only").attr('data-value'),
                                'title_format'                      : 0,
                                'id_site'                           : $("#id-site").val(),
                                'id_packages'                       : $("#id-packages").val(),
                                'auto_pay'                          : $("#auto-pay").val(),
                                'gallery_plus'                      : $("#gallery-plus").is(':checked') ? 1 : 0,
                                'listing_duration'                  : $("#listing-duration-days").val(),
                                'visitor_counter'                   : $("#visitor-counter").val(),
                                'step'                              : $("#popup-step").val(),
                                'email'                             : '',
                                'city'                              : '',
                                'template'                          : $('input[name="form-template-checkbox"]').val(),
                                'no_image'                          : $('input[name="form-product-configuration-image"]:checked').val() ? $('input[name="form-product-configuration-image"]:checked').val() : 0,
                                'discount'                          : $('input[name="form-product-configuration-discount"]:checked').val() ? $('input[name="form-product-configuration-discount"]:checked').val() : 0,
                                'sync'                              : 0,
                                'just_orders'                       : $('input[name="form-product-configuration-orders"]:checked').val() ? $('input[name="form-product-configuration-orders"]:checked').val() : 0,
                        };
                        data.push(data_selected);
                        
                        ajax_save(e);
                }
        });
        
