    var previous;
    var rules_controller;
    var counterWeight = 1;
    var counterPrice = 1;
    var firstload       = {
        surcharge : 0,
        weight : 0,
        items : 0,
        prices : 0,
    };
    
    $('.option_mode').checkBo();
    
    var chk_value = '';
    var postal_valid_res = 1; 
    var data                            = new Array();
    var wizard_data                     = new Array();
    var addLocal                        = 0;
    var addInter                        = 0;
    var greater_message                 = $('input[name="validate-price-modifier-range"]').val();
    var between_message                 = $('input[name="validate-price-modifier-between"]').val();
    console.log('carriers_main.js');
    
    angular.module('ebayApp', [])
    .config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }])
    .controller('CarrierMain', function() {
        this.tab_datas = app_data;
        this.switch_checked = true;
        this.current_tab = this.tab_datas.current_active;

        this.switchChange = function() {
            if ( this.tab_datas.current_active === 1 && this.tab_datas.tab_data[1].active ) {
                this.tab_datas.current_active = 2;
                this.tab_datas.tab_data[1].active = 0;
                this.tab_datas.tab_data[2].active = 1;
            } 
            else if ( this.tab_datas.current_active === 1 && !this.tab_datas.tab_data[1].active ) {
                this.tab_datas.current_active = 1;
                this.tab_datas.tab_data[1].active = 1;
                this.tab_datas.tab_data[2].active = 0;
            } 
            else if ( this.tab_datas.current_active === 2 && this.tab_datas.tab_data[2].active ) {
                this.tab_datas.current_active = 1;
                this.tab_datas.tab_data[1].active = 1;
                this.tab_datas.tab_data[2].active = 0;
            } 
            else if ( this.tab_datas.current_active === 2 && !this.tab_datas.tab_data[2].active ) {
                this.tab_datas.current_active = 2;
                this.tab_datas.tab_data[1].active = 0;
                this.tab_datas.tab_data[2].active = 1;
            }
            this.tab_help = this.tab_datas.tab_data[this.current_tab].active ? this.tab_datas.tab_data[this.current_tab].tab_deactive_help : this.tab_datas.tab_data[this.current_tab].tab_active_help;
            active_li = this.tab_datas.current_active;
        }
        
        this.init = function(tabNumber) {
            this.current_tab = tabNumber;
            active_li = this.tab_datas.current_active;
            this.tab_help = this.tab_datas.tab_data[tabNumber].active ? this.tab_datas.tab_data[tabNumber].tab_deactive_help : this.tab_datas.tab_data[tabNumber].tab_active_help;
            $('#form-app-mode').prop( "checked" , this.tab_datas.tab_data[tabNumber].active );
            this.getChecked(this.tab_datas.tab_data[tabNumber].active);
        }
        
        this.getChecked = function(checked) {
            (checked === 1) ? $('.option_mode > .cb-switcher').addClass('checked').find('.cb-state').text('ON') : $('.option_mode > .cb-switcher').removeClass('checked').find('.cb-state').text('OFF');
        }
    });
    
    
    $(document).ready(function() {           
            
            $("#form-submit").on("submit", function(e) {
                    var d_carrier = true;

                    var sp_carrier = true;
                    var sp_inter_carrier = true;

                    $("select[name^='country-name']").each(function() {
                            $name       = $(this).parents('.international-carriers-body:first').find('select.international-carriers');
                            $name2      = $(this).parents('.international-carriers-body:first').find('select.international-ebay-carriers');
                            $country    = $(this);
                            if ( $(this).attr('name') !== 'country-name-1' && $(this).find('option:selected').val() !== '0' ) {
                                    if ( d_carrier === false || $name.valid() === false || $name2.valid() === false || $(this).find('option:selected').val() === '0' ) {
                                            d_carrier = false;
                                    }
                            }
                            else if ( $name.find('option:selected').val() !== '0' ) {
                                    if ( d_carrier === false || $name2.valid() === false || $(this).find('option:selected').val() === '0' ) {
                                            d_carrier = false;
                                    }
                            }
                            else if ( $name2.find('option:selected').val() !== '0' ) {
                                    if ( d_carrier === false || $name.valid() === false || $(this).find('option:selected').val() === '0' ) {
                                            d_carrier = false;
                                    }
                            }
                    });
                    
                    $("select[name^='sp-domestic-ebay-carriers']").each(function() {
                            if ( $(this).attr('name') !== 'sp-domestic-ebay-carriers-1' ) {
                                    if ( sp_carrier === false || $(this).valid() === false || $(this).find('option:selected').val() === '0' ) {
                                            sp_carrier = false;
                                    }
                            }
                    });
                    
                    $("select[name^='sp-international-ebay-carriers']").each(function() {
                            if ( $(this).attr('name') !== 'sp-international-ebay-carriers-1' ) {
                                    $country       = $(this).parents('.sp-international-carriers-body').first().find('select.sp-country-name');
                                    if ( sp_carrier === false || $(this).valid() === false || $country.valid() === false || $(this).find('option:selected').val() === '0' ) {
                                            sp_inter_carrier = false;
                                    }
                            }
                    });
                    
                    if (   d_carrier && active_li === 1
                        && $("select[name^='domestic-carriers']").valid()
                        && $("select[name^='domestic-ebay-carriers']").valid()
                        && $("#form-postal-code").valid() ) {
                    
                            var page = $("#current-page").val();
                            e.preventDefault();
                            $('html, body').animate({ scrollTop: 0 }, 0);
                            var data_carriers                   = new Array();
                            var data_international_carriers     = new Array();
                            var data_country                    = new Array();
                            var data_rules                      = new Array();
                            var data_fixed                      = new Array();
                            var carrier_operation               = 'is between';
                            var data_dispatch_time              = $("#dispatch_time").val();
                            var carrier_rule_id                 = ($('.carrier-configuration').val()) ? $('.carrier-configuration').val() : 0;
                            if ( typeof(carrier_rule_id) !== 'undefined' && carrier_rule_id == 3 ) {
                                    carrier_operation           = 0;
                            }
                            data                        = [];
                            wizard_data                 = [];
                            $(".domestic-body").each(function( index ) {
                                    var carrier_id          = $(this).find("select.domestic-carriers option:selected").val();
                                    var carrier_name        = $(this).find("select.domestic-carriers option:selected").attr('data-value');
                                    var carrier_service     = $(this).find("select.domestic-ebay-carriers option:selected").data('service');
                                    var carrier_ebay_id     = $(this).find("select.domestic-ebay-carriers option:selected").val();
                                    var carrier_ebay_name   = $(this).find("select.domestic-ebay-carriers option:selected").text();
                                    var pattern_name        = $(this).find("select.domestic-carriers option:selected").attr('data-value') + " - " +$(this).find("select.domestic-ebay-carriers option:selected").text();

                                    if (typeof(carrier_id) !== "undefined" && carrier_id !== '0' && carrier_name !== '' && typeof(carrier_ebay_id) !== "undefined" && carrier_ebay_id !== '0' && carrier_ebay_name !== '' ) {
                                            data_value                  = {
                                                    'id_profile'            : $('.id_profile').val() ? $('.id_profile').val() : 0,
                                                    'carrier_id'            : carrier_id,
                                                    'carrier_name'          : carrier_name,
                                                    'carrier_service'       : carrier_service,
                                                    'carrier_ebay_id'       : carrier_ebay_id,
                                                    'carrier_ebay_name'     : carrier_ebay_name,
                                                    'carrier_rule_id'       : carrier_rule_id,
                                                    'postcode'              : $('#form-postal-code').val(),
                                                    'dispatch_time'         : data_dispatch_time,
                                            };

                                            data_carriers.push(data_value);
                                    }
                            });
                            
                            $(".carrier-body").each(function( index ) {
                                    var carrier_service     = $(this).find("select.domestic-ebay-carriers option:selected").data('service');
                                    var carrier_ebay_id     = $(this).find("select.domestic-ebay-carriers option:selected").val();
                                    var carrier_ebay_name   = $(this).find("select.domestic-ebay-carriers option:selected").text();
                                    var is_default          = $(this).find("input[name='form-carrier-default']").prop("checked");

                                    if (typeof(carrier_ebay_id) !== "undefined" && carrier_ebay_id !== '0' && carrier_ebay_name !== '' ) {
                                            data_value                  = {
                                                    'id_profile'            : $('.id_profile').val() ? $('.id_profile').val() : 0,
                                                    'carrier_service'       : carrier_service,
                                                    'carrier_ebay_id'       : carrier_ebay_id,
                                                    'carrier_ebay_name'     : carrier_ebay_name,
                                                    'carrier_rule_id'       : carrier_rule_id,
                                                    'is_default'            : is_default ? 1 : 0,
                                                    'postcode'              : $('#form-postal-code').val(),
                                                    'dispatch_time'         : data_dispatch_time,
                                            };

                                            data_rules.push(data_value);
                                    }
                            });

                            $('.international-body').each(function( index ) {
                                    $carrier_main           = $(this);
                                    var carrier_id          = $(this).find("select.international-carriers option:selected").val();
                                    var carrier_name        = $(this).find("select.international-carriers option:selected").text();
                                    var carrier_in_id       = $(this).find("select.international-ebay-carriers option:selected").val();
                                    var carrier_in_service  = $(this).find("select.international-ebay-carriers option:selected").data('service');
                                    var carrier_in_name     = $(this).find("select.international-ebay-carriers option:selected").text();
                                    var pattern_name        = $(this).find("select.international-carriers option:selected").text() + " - " +$(this).find("select.international-ebay-carriers option:selected").text() + " ( " + $(this).find("select.country-name option:selected").text() + " )";
                                    var country_name        = $(this).find("select.country-name option:selected").text();
                                    var country_service     = $(this).find("select.country-name option:selected").val();

                                            if (country_name !== '' && country_name !== $(".choose-a-country").val() && country_service !== '0' && typeof(carrier_id) !== "undefined" && carrier_id !== '0' && typeof(carrier_in_id) !== "undefined" && carrier_in_id !== '0' && carrier_in_name !== '' && carrier_in_name !== $(".ebay-carrier").val() && carrier_name !== '' && carrier_name !== $(".associate-carrier").val()) {
                                                    data_value                  = {
                                                            'id_profile'            : $('.id_profile').val() ? $('.id_profile').val() : 0,
                                                            'carrier_id'            : carrier_id,
                                                            'carrier_ebay_id'       : carrier_in_id,
                                                            'carrier_service'       : carrier_in_service,
                                                            'carrier_name'          : carrier_name,
                                                            'carrier_ebay_name'     : carrier_in_name,
                                                            'carrier_rule_id'       : carrier_rule_id,
                                                            'country_service'       : country_service,
                                                            'postcode'              : $('#form-postal-code').val(),
                                                            'dispatch_time'         : data_dispatch_time,
                                                    };

                                                    data_check                  = {
                                                            'id_profile'            : $('.id_profile').val() ? $('.id_profile').val() : 0,
                                                            'carrier_id'            : carrier_in_id,
                                                            'country_service'       : country_service,
                                                            'country_name'          : country_name
                                                    };

                                                    data_country.push(data_check);
                                                    data_international_carriers.push(data_value);
                                            }
                            });

                            data_selected = {
                                'id_site'       : $("#id-site").val(),
                                'id_packages'   : $("#id-packages").val(),
                            };

                            data.push(data_selected);

                            $.ajax ({
                                    type        : "POST",
                                    url         : base_url + "ebay/" + $('#ajax-url').val(),
                                    data        : { data: data, data_carriers : data_carriers, 'data_international_carriers' : data_international_carriers, 'data_country' : data_country, 'data_rules' : data_rules, 'data_fixed' : data_fixed},
                                    error       : function(jqXHR, textStatus, errorThrown) {
                                                notification_alert(errorThrown,'bad');
                                    },
                                    success     : function(result) {
                                                if ( (result.search(/Fail/) != 0) && (result.search(/Fail/) != 24) ) {   
                                                        if ( typeof(module) !== 'undefined' && module === 1 ) {
                                                                wizard_start();
                                                        }
                                                        else {
                                                                notification_alert($(".update-" + page + "-success").val() );
                                                                setTimeout(function(){
                                                                    location.href   = $('#form-submit').attr('action');
                                                                }, 3200);
                                                        }
                                                }
                                                else {    
                                                        notification_alert($(".update-" + page + "-fail").val(),'bad');
                                                }
                                    }
                            });
                    }
                    else if (   active_li === 2
                            && $("select[name^='sp-domestic-ebay-carriers']").valid()
                            && $("select[name^='sp-international-ebay-carriers']").valid()
                            && $("select[name^='sp-country-name']").valid()
                            && $(".cost-rate").valid() 
                            && $(".additional-rate").valid() 
                            && $("#sp-form-postal-code").valid() ) {

                                var page = $("#current-page").val();
                                e.preventDefault();
                                $('html, body').animate({ scrollTop: 0 }, 0);
                                var data_carriers                   = new Array();
                                var data_international_carriers     = new Array();
                                var data_country                    = new Array();
                                var data_rules                      = new Array();
                                var data_fixed                      = new Array();
                                var carrier_operation               = 'is between';
                                var data_dispatch_time              = $("#sp-dispatch-time").val();

                                data                        = [];
                                wizard_data                 = [];
                                $(".sp-domestic-body").each(function( index ) {
                                        var carrier_service     = $(this).find("select.sp-domestic-ebay-carriers option:selected").data('service');
                                        var carrier_ebay_id     = $(this).find("select.sp-domestic-ebay-carriers option:selected").val();
                                        var carrier_ebay_name   = $(this).find("select.sp-domestic-ebay-carriers option:selected").text();
                                        var carrier_cost        = $(this).find(".cost-rate").val();
                                        var carrier_additionals = $(this).find(".additional-rate").val();

                                        if (   typeof(carrier_service) !== "undefined" 
                                            && typeof(carrier_ebay_name) !== "undefined" 
                                            && typeof(carrier_ebay_id) !== "undefined" 
                                            && carrier_service !== '' 
                                            && carrier_ebay_id !== '0' 
                                            && carrier_ebay_name !== '' ) {

                                                data_input                  = {
                                                        'id_profile'            : $('.id_profile').val() ? $('.id_profile').val() : 0,
                                                        'carrier_service'       : carrier_service,
                                                        'carrier_ebay_id'       : carrier_ebay_id,
                                                        'carrier_ebay_name'     : carrier_ebay_name,
                                                        'carrier_cost'          : carrier_cost,
                                                        'carrier_additionals'   : carrier_additionals,
                                                        'postcode'              : $('#sp-form-postal-code').val(),
                                                        'dispatch_time'         : data_dispatch_time,
                                                        'tab'                   : active_li,
                                                        'step'                  : $("#popup-step").val(),
                                                };

                                                data_fixed.push(data_input);
                                        }
                                });

                                $('.sp-international-body').each(function( index ) {
                                        var carrier_in_id       = $(this).find("select.sp-international-ebay-carriers option:selected").val();
                                        var carrier_in_service  = $(this).find("select.sp-international-ebay-carriers option:selected").data('service');
                                        var carrier_in_name     = $(this).find("select.sp-international-ebay-carriers option:selected").text();
                                        var country_name        = $(this).find("select.sp-country-name option:selected").text();
                                        var country_service     = $(this).find("select.sp-country-name option:selected").val();
                                        var carrier_cost        = $(this).find(".cost-rate").val();
                                        var carrier_additionals = $(this).find(".additional-rate").val();


                                        if (country_name !== '' 
                                         && typeof(carrier_in_id) !== "undefined" 
                                         && typeof(carrier_in_service) !== "undefined" 
                                         && typeof(carrier_in_name) !== "undefined" 
                                         && typeof(country_name) !== "undefined" 
                                         && typeof(country_service) !== "undefined" 
                                         && typeof(carrier_cost) !== "undefined" 
                                         && typeof(carrier_additionals) !== "undefined" 
                                         && country_name !== $(".choose-a-country").val() 
                                         && country_service !== '0' 
                                         && carrier_in_id !== '0' 
                                         && carrier_in_name !== '' 
                                         && carrier_in_name !== $(".ebay-carrier").val()) {
                                                data_value                  = {
                                                        'id_profile'            : $('.id_profile').val() ? $('.id_profile').val() : 0,
                                                        'carrier_ebay_id'       : carrier_in_id,
                                                        'carrier_service'       : carrier_in_service,
                                                        'carrier_ebay_name'     : carrier_in_name,
                                                        'country_service'       : country_service,
                                                        'country_name'          : country_name,
                                                        'carrier_cost'          : carrier_cost,
                                                        'carrier_additionals'   : carrier_additionals,
                                                        'postcode'              : $('#sp-form-postal-code').val(),
                                                        'dispatch_time'         : data_dispatch_time,
                                                        'tab'                   : active_li,
                                                        'step'                  : $("#popup-step").val(),
                                                };

                                                data_fixed.push(data_value);
                                        }
                                });

                                data_selected = {
                                    'id_site'       : $("#id-site").val(),
                                    'id_packages'   : $("#id-packages").val(),
                                };

                                data.push(data_selected);

                                $.ajax ({
                                        type        : "POST",
                                        url         : base_url + "ebay/" + $('#ajax-url').val(),
                                        data        : { data: data, data_carriers : data_carriers, 'data_international_carriers' : data_international_carriers, 'data_country' : data_country, 'data_rules' : data_rules, 'data_fixed' : data_fixed},
                                        error       : function(jqXHR, textStatus, errorThrown) {
                                                    notification_alert(errorThrown,'bad');
                                        },
                                        success     : function(result) {
                                                    if ( (result.search(/Fail/) != 0) && (result.search(/Fail/) != 24) ) {   
                                                            if ( typeof(module) !== 'undefined' && module === 1 ) {
                                                                    wizard_start();
                                                            }
                                                            else {
                                                                    notification_alert($(".update-" + page + "-success").val() );
                                                                    setTimeout(function(){
                                                                        location.href   = (result.search(/Skip/) != -1) ?
	                                                                        				$('.skip-url').val() :
	                                                                        				$('#form-submit').attr('action');
                                                                    }, 3200);
                                                            }
                                                    }
                                                    else {    
                                                            notification_alert($(".update-" + page + "-fail").val(),'bad');
                                                    }
                                        }
                                });
                        }
            });

            set_validation();
            validation_number();
            removeMapping();
            spRemoveMapping();
            spInterRemoveMapping();
            removeMapping();
            removeRules();
            specify_cost_rate();
            
            function carrier_configuration(val) {
                    $(".collapse-swap").hide();
                    if ( val == 1 ) {
                           $("#collapse-surcharge").show();
                           firstload['surcharge']++;
                           rules_controller = 'surcharge';
                   }else if ( val == 2 ) {
                           $("#collapse-weight").show();
                           firstload['weight']++;
                           rules_controller = 'weight';
                   }else if ( val == 3 ) {
                           $("#collapse-item").show(); 
                           firstload['items']++;
                           rules_controller = 'items';
                    }else if ( val == 4 ) {
                           $("#collapse-price").show(); 
                           firstload['prices']++;
                           rules_controller = 'prices';
                    }
                    $('label.error').remove();
                    //setShippingRules();
            }
            
            
            function setShippingRules() {
//                    if ( $(".collapse-swap").filter(':visible').find(".rules-body").is(':visible') ) {
                            var default_item = {
                                items  : {},
                                weight  : {},
                                prices  : {},
                                surcharge  : {}
                            };

                            $(".domestic-body").each(function() {
                                    var domestic_body       = $(this); 

                                    if ( domestic_body.is(":visible") ) {
                                            var associate_carriers  = domestic_body.find("select.domestic-carriers option:selected"); 
                                            var ebay_carrier        = domestic_body.find("select.domestic-ebay-carriers option:selected"); 
                                            if ( associate_carriers.attr('data-value') !== $(".associate-carrier").val() && ebay_carrier.text() !== $(".ebay-carrier").val() && jQuery.inArray(associate_carriers.attr('data-value'), choose_value) < 0 ) {
                                                    choose_value = ($.isEmptyObject(choose_value)) ? {} : choose_value;
                                                    var name_value   = associate_carriers.attr('data-value') + ' - ' + ebay_carrier.text();
                                                    choose_value[name_value] = typeof(choose_value[name_value]) === 'undefined' ? default_item : choose_value[name_value];
                                            }
                                    }
                            });

                            $(".international-body").each(function() {
                                    var international_body  = $(this); 
                                    if ( international_body.is(":visible") ) {
                                            var country             = international_body.find("select.country-name option:selected"); 
                                            var associate_carriers  = international_body.find("select.international-carriers option:selected"); 
                                            var ebay_carrier        = international_body.find("select.international-ebay-carriers option:selected"); 

                                            if ( ($(country).text() !== $(".choose-a-country").val()) && ($(associate_carriers).text() != $(".associate-carrier").val()) && ($(ebay_carrier).text() != $(".ebay-carrier").val()) && (jQuery.inArray(country.text(), choose_value) < 0) ) {
                                                    choose_value = ($.isEmptyObject(choose_value)) ? {} : choose_value;
                                                    var name_value   = associate_carriers.text() + ' - ' + ebay_carrier.text() + ' ( ' + country.text() + ' )';
                                                    choose_value[name_value] = typeof(choose_value[name_value]) === 'undefined' ? default_item : choose_value[name_value];
                                            }
                                    }
                            });

                            //setShippingRulesField();
            }
            
            function setShippingRulesField() {
                    var root = $(".collapse-swap[rel='"+rules_controller+"']").find(".rules-body").filter(':visible');
                    var disroot = $(".collapse-swap[rel='"+rules_controller+"']").find(".rules-body:first");
                    $(".error_mapping").hide();
                    if ( rules_controller === 'items' ) {
                            $('#collapse-item').show();
                    }
                    if ( !jQuery.isEmptyObject(choose_value) ) {
                            $('.rules-label-warning').remove();
                            for ( var key in choose_value ) {}
                            counterWeight = 1;
                            while (counterWeight < Object.keys(choose_value[key][rules_controller]).length && Object.keys(choose_value[key][rules_controller]).length > 1 && firstload[rules_controller] === 1) {
                                    counterWeight++;
                                    $(disroot).find('.addrulecost').trigger('click');
                            } 
                            $.each(choose_value, function(i, value) {
                                    firstload[rules_controller]++;
                                    if ( !jQuery.isEmptyObject(value[rules_controller]) ) {
                                            $.each(value[rules_controller], function(rel, val) {
                                                    if ( !jQuery.isEmptyObject(val) ) {
                                                            var deep_root = $(".collapse-swap[rel='"+rules_controller+"']").find(".rules-body").filter("[rel='"+ parseInt(rel) +"']");
                                                            if ( deep_root.find('.rules-label:contains("'+ i +'")').val() === undefined ) {
                                                                    var deeproot_shipping = deep_root.find('.rules-carriers-body:first');
                                                                    var lgcurrency = disroot.find('.rules-main-cost').attr('placeholder').substring(6, 9);
                                                                    cloned = deeproot_shipping.clone().appendTo(deep_root);
                                                                    cloned.find('hr').remove();
                                                                    cloned.find('.rules-label').text(i+" : ");
                                                                    cloned.find('.rules-main-cost').attr('placeholder', '0.00').val('').attr('name', 'cost-' +i+ '-' + rules_controller + '-' + rel);
                                                                    cloned.find('.rules-additionals').attr('placeholder', '0.00').val('').attr('name', 'additionals-' +i+ '-' + rules_controller + '-' + rel);
                                                                    cloned.find('.rules-min-weight').attr('placeholder', '0.00').val('').attr('name', 'min-weight-' +i+ '-' + rules_controller + '-' + rel).attr('rel', 'from');
                                                                    cloned.find('.rules-max-weight').attr('placeholder', '0.00').val('').attr('name', 'max-weight-' +i+ '-' + rules_controller + '-' + rel).attr('rel', 'to');
                                                                    cloned.find('.rules-min-price').attr('placeholder', '0.00').val('').attr('name', 'min-price-' +i+ '-' + rules_controller + '-' + rel).attr('rel', 'from');
                                                                    cloned.find('.rules-max-price').attr('placeholder', '0.00').val('').attr('name', 'max-price-' +i+ '-' + rules_controller + '-' + rel).attr('rel', 'to');
                                                                    cloned.find('.rules-surcharge').attr('placeholder', '0.00').val('').attr('name', 'surcharge-' +i+ '-' + rules_controller + '-' + rel);
                                                                    cloned.find('.addrulecost').remove();
                                                                    cloned.find('.removerulecost').remove();
                                                                    
                                                                    deep_root.find('input[name^="cost"], input[name^="additionals"], input[name^="min-price"], input[name^="max-price"]').each(function(){
                                                                        $(this).parents(".input-group:first").find('strong').text(lg_current[0][lgcurrency]);
                                                                    });
                                                                    
                                                                    cloned.find('.rules-main-cost').rules('add', { required: true, none: true, minlength: true });
                                                                    cloned.find('.rules-additionals').rules('add', { required: true, none: true, minlength: true });
                                                                    if ( rules_controller === 'prices') {
                                                                            cloned.find('.rules-min-price').rules('add', { required: true, none: true, minlength: true, between: true, });
                                                                            cloned.find('.rules-max-price').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                                    }
                                                                    if ( rules_controller === 'weight') {
                                                                            cloned.find('.rules-min-weight').rules('add', { required: true, none: true, minlength: true, between: true, });
                                                                            cloned.find('.rules-max-weight').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                                    }
                                                            }
                                                    
                                                    }
                                                    else {
                                                            var deep_root = $(".collapse-swap[rel='"+rules_controller+"']").find(".rules-body").filter("[rel='"+ parseInt(rel) +"']");
                                                            if ( deep_root.find('.rules-label:contains("'+ i +'")').val() === undefined ) {
                                                                    var deeproot_shipping = deep_root.find('.rules-carriers-body:first');
                                                                    var lgcurrency = disroot.find('.rules-main-cost').attr('placeholder').substring(6, 9);
                                                                    cloned = deeproot_shipping.clone().appendTo(deep_root);
                                                                    cloned.find('hr').remove();
                                                                    cloned.find('.rules-label').text(i+" : ");
                                                                    cloned.find('.rules-main-cost').attr('placeholder', '0.00').val('').attr('name', 'cost-' +i+ '-' + rules_controller + '-' + rel);
                                                                    cloned.find('.rules-additionals').attr('placeholder', '0.00').val('').attr('name', 'additionals-' +i+ '-' + rules_controller + '-' + rel);
                                                                    cloned.find('.rules-min-weight').attr('placeholder', '0.00').val('').attr('name', 'min-weight-' +i+ '-' + rules_controller + '-' + rel).attr('rel', 'from');
                                                                    cloned.find('.rules-max-weight').attr('placeholder', '0.00').val('').attr('name', 'max-weight-' +i+ '-' + rules_controller + '-' + rel).attr('rel', 'to');
                                                                    cloned.find('.rules-min-price').attr('placeholder', '0.00').val('').attr('name', 'min-price-' +i+ '-' + rules_controller + '-' + rel).attr('rel', 'from');
                                                                    cloned.find('.rules-max-price').attr('placeholder', '0.00').val('').attr('name', 'max-price-' +i+ '-' + rules_controller + '-' + rel).attr('rel', 'to');
                                                                    cloned.find('.rules-surcharge').attr('placeholder', '0.00').val('').attr('name', 'surcharge-' +i+  '-' + rules_controller +'-' + rel);
                                                                    cloned.find('.addrulecost').remove();
                                                                    cloned.find('.removerulecost').remove();
                                                                    
                                                                    deep_root.find('input[name^="cost"], input[name^="additionals"], input[name^="min-price"], input[name^="max-price"]').each(function(){
                                                                        $(this).parent().find('strong').text(lg_current[0][lgcurrency]);
                                                                    });
                                                                    
                                                                    cloned.find('.rules-main-cost').rules('add', { required: true, none: true, minlength: true });
                                                                    cloned.find('.rules-additionals').rules('add', { required: true, none: true, minlength: true });
                                                                    if ( rules_controller === 'prices') {
                                                                            cloned.find('.rules-min-price').rules('add', { required: true, none: true, minlength: true, between: true, });
                                                                            cloned.find('.rules-max-price').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                                    }
                                                                    if ( rules_controller === 'weight') {
                                                                            cloned.find('.rules-min-weight').rules('add', { required: true, none: true, minlength: true, between: true, });
                                                                            cloned.find('.rules-max-weight').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                                    }
                                                            }
                                                    }
                                            });
                                    }
                                    else {
                                            if ( root.find('.rules-label:contains("'+ i +'")').val() === undefined ) {
                                                    $(root).each(function() {
                                                            var root_shipping = $(this).find('.rules-carriers-body').first();
                                                            var lgcurrency = root_shipping.find('.rules-main-cost').attr('placeholder').substring(6, 9);
                                                            var rel_none = $(this).attr('rel');
                                                            cloned = root_shipping.clone().appendTo($(this));
                                                            cloned.find('hr').remove();
                                                            cloned.find('.rules-label').text(i+" : ");
                                                            cloned.find('.rules-main-cost').removeAttr('disabled');
                                                            cloned.find('.rules-additionals').removeAttr('disabled');
                                                            cloned.find('.rules-min-weight').removeAttr('disabled');
                                                            cloned.find('.rules-max-weight').removeAttr('disabled');
                                                            cloned.find('.rules-min-price').removeAttr('disabled');
                                                            cloned.find('.rules-max-price').removeAttr('disabled');
                                                            cloned.find('.rules-surcharge').removeAttr('disabled');
                                                            cloned.find('.rules-main-cost').attr('placeholder', '0.00').val('').attr('name', 'cost-' +i+ '-' + rules_controller + '-' + rel_none);
                                                            cloned.find('.rules-additionals').attr('placeholder', '0.00').val('').attr('name', 'additionals-' +i+ '-' + rules_controller + '-' + rel_none);
                                                            cloned.find('.rules-min-weight').attr('placeholder', '0.00').val('').attr('name', 'min-weight-' +i+ '-' + rules_controller + '-' + rel_none).attr('rel', 'from');
                                                            cloned.find('.rules-max-weight').attr('placeholder', '0.00').val('').attr('name', 'max-weight-' +i+ '-' + rules_controller + '-' + rel_none).attr('rel', 'to');
                                                            cloned.find('.rules-min-price').attr('placeholder', '0.00').val('').attr('name', 'min-price-' +i+ '-' + rules_controller + '-' + rel_none).attr('rel', 'from');
                                                            cloned.find('.rules-max-price').attr('placeholder', '0.00').val('').attr('name', 'max-price-' +i+ '-' + rules_controller + '-' + rel_none).attr('rel', 'to');
                                                            cloned.find('.rules-surcharge').attr('placeholder', '0.00').val('').attr('name', 'surcharge-' +i+ '-' + rules_controller + '-' + rel_none);
                                                            cloned.find('.addrulecost').remove();
                                                            cloned.find('.removerulecost').remove();

                                                            disroot.find('input[name^="cost"], input[name^="additionals"], input[name^="min-price"], input[name^="max-price"]').each(function(){
                                                                $(this).parent().find('strong').text(lg_current[0][lgcurrency]);
                                                            });

                                                            cloned.find('.rules-main-cost').rules('add', { required: true, none: true, minlength: true });
                                                            cloned.find('.rules-additionals').rules('add', { required: true, none: true, minlength: true });
                                                            if ( rules_controller === 'prices') {
                                                                    cloned.find('.rules-min-price').rules('add', { required: true, none: true, minlength: true, between: true, });
                                                                    cloned.find('.rules-max-price').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                            }
                                                            if ( rules_controller === 'weight') {
                                                                    cloned.find('.rules-min-weight').rules('add', { required: true, none: true, minlength: true, between: true, });
                                                                    cloned.find('.rules-max-weight').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                            }
                                                    });
                                            }
                                    }
                            });
                    }
                    else {
                            $('.rules-label-warning').remove();
                            if($('.rules-carriers-main').parent().find('.error_mapping').length <= 0)   {
                                    $('#collapse-item').hide();
                            }
                    }

                    validation_number();
                    setShippingValue(choose_value);
                    eventSetOperation();
                    eventSetAllValue();
                    eventSetWeightValue();
                    console.log(choose_value);
            }
            
            function setShippingValue(value) {
                    var root = $('.collapse-swap[rel="'+rules_controller+'"]').find(".rules-body");
                    if ( typeof(value) !== 'undefined' ) {
                            $.each(value, function(i, val) {
                                    var rules_label = root.find('.rules-label:contains("'+ i +' : ")');
                                    if ( rules_label.val() !== undefined ) {
                                            $.each(val[rules_controller], function(rel, input_val) {
                                                    var deep_root = $(".collapse-swap[rel='"+rules_controller+"']").find(".rules-body").filter("[rel='"+ parseInt(rel) +"']");//+1
                                                    var v_cost = deep_root.find('.rules-label').filter(':contains("'+ i +' : ")').parent().find('.rules-main-cost');
                                                    var v_additionals = deep_root.find('.rules-label').filter(':contains("'+ i +' : ")').parent().find('.rules-additionals');
                                                    var v_min_weight = deep_root.find('.rules-label').filter(':contains("'+ i +' : ")').parent().find('.rules-min-weight');
                                                    var v_max_weight = deep_root.find('.rules-label').filter(':contains("'+ i +' : ")').parent().find('.rules-max-weight');
                                                    var v_min_price = deep_root.find('.rules-label').filter(':contains("'+ i +' : ")').parent().find('.rules-min-price');
                                                    var v_max_price = deep_root.find('.rules-label').filter(':contains("'+ i +' : ")').parent().find('.rules-max-price');
                                                    if ( rules_controller === 'items' ) {
                                                            v_cost.val( ((v_cost.val() !== '0.00') && (v_cost.val() !== '') ? v_cost.val() : ( (val[rules_controller][rel]['cost'] !== undefined) ? val[rules_controller][rel]['cost'] : '0.00')) );
                                                            v_additionals.val( ((v_additionals.val() !== '0.00') && (v_additionals.val() !== '') ? v_additionals.val() : ( (val[rules_controller][rel]['additionals'] !== undefined) ? val[rules_controller][rel]['additionals'] : '0.00')) );
                                                    }
                                                    if ( rules_controller === 'weight' ) {
                                                            v_cost.val( ((v_cost.val() !== '0.00') && (v_cost.val() !== '') ? v_cost.val() : ( (val[rules_controller][rel]['cost'] !== undefined) ? val[rules_controller][rel]['cost'] : '0.00')) );
                                                            v_additionals.val( ((v_additionals.val() !== '0.00') && (v_additionals.val() !== '') ? v_additionals.val() : ( (val[rules_controller][rel]['additionals'] !== undefined) ? val[rules_controller][rel]['additionals'] : '0.00')) );
                                                            v_min_weight.val( ((v_min_weight.val() !== '0.00') && (v_min_weight.val() !== '') ? v_min_weight.val() : ( (val[rules_controller][rel]['min-weight'] !== undefined) ? val[rules_controller][rel]['min-weight'] : '0.00')) );
                                                            v_max_weight.val( ((v_max_weight.val() !== '0.00') && (v_max_weight.val() !== '') ? v_max_weight.val() : ( (val[rules_controller][rel]['max-weight'] !== undefined) ? val[rules_controller][rel]['max-weight'] : '0.00')) );

                                                            if ( deep_root.children('.rules-carriers-body').find('.operation option:selected').val() >= 3 ) {
                                                                    deep_root.children('.rules-carriers-body').find('.rules-max-weight').attr({'disabled' : 'disabled'}).css({'color' : '#EEE'});
                                                            }
                                                    };
                                                    if ( rules_controller === 'prices' ) {
                                                            v_cost.val( ((v_cost.val() !== '0.00') && (v_cost.val() !== '') ? v_cost.val() : ( (val[rules_controller][rel]['cost'] !== undefined) ? val[rules_controller][rel]['cost'] : '0.00')) );
                                                            v_additionals.val( ((v_additionals.val() !== '0.00') && (v_additionals.val() !== '') ? v_additionals.val() : ( (val[rules_controller][rel]['additionals'] !== undefined) ? val[rules_controller][rel]['additionals'] : '0.00')) );
                                                            v_min_price.val( ((v_min_price.val() !== '0.00') && (v_min_price.val() !== '') ? v_min_price.val() : ( (val[rules_controller][rel]['min-price'] !== undefined) ? val[rules_controller][rel]['min-price'] : '0.00')) );
                                                            v_max_price.val( ((v_max_price.val() !== '0.00') && (v_max_price.val() !== '') ? v_max_price.val() : ( (val[rules_controller][rel]['max-price'] !== undefined) ? val[rules_controller][rel]['max-price'] : '0.00')) );
 
                                                            if ( deep_root.children('.rules-carriers-body').find('.operation option:selected').val() >= 3 ) {
                                                                    deep_root.children('.rules-carriers-body').find('.rules-max-price').attr({'disabled' : 'disabled'}).css({'color' : '#EEE'});
                                                            }
                                                    };
                                            });
                                    }
                            });
                    }
            }
            
            function removeFocusKey(focus) {
                    var input = $(focus);
                    var sname = $(focus).attr('class').split(' ');
                    sname = sname[0];
                    $(input).find('option').show();
                    if ( sname === 'domestic-carriers' ) {
                            $('select[name^="'+sname+'"] option').removeAttr('disabled');
                            $('select[name^="'+sname+'"] option:selected').each(function( index, option ) {         
                                    if ( previous !== $(option).text() ) {
                                            $(input).find('option[value="' + option.value + '"]').attr({'disabled' : 'disabled'});
                                    }
                                    else if ( $(option).val() !== '0' ) {
                                            $(input).find('option[value="' + option.value + '"]').attr({'disabled' : 'disabled'});
                                    }
                            });
                    }
                    if ( sname === 'domestic-ebay-carriers' ) {
                            $('select[name^="'+sname+'"] option').removeAttr('disabled');
                            $('select[name^="'+sname+'"] option:selected').each(function( index, option ) {         
                                    if ( previous !== $(option).text() ) {
                                            $(input).find('option[value="' + option.value + '"]').attr({'disabled' : 'disabled'});
                                    }
                                    else if ( $(option).val() !== '0' ) {
                                            $(input).find('option[value="' + option.value + '"]').attr({'disabled' : 'disabled'});
                                    }
                            });
                    }
                    $(input).find('option:selected').removeAttr('disabled');
            }
            
            function removeRulesKey() {
                    delete choose_value[previous];
                    $(".rules-carriers-body").find('.rules-label:contains("'+ previous +' : ")').parent().remove();
                    //setShippingRules();
            }
            
            function eventSetShippingRules() {
                    $("select[name^='domestic-carriers']").unbind('change').change(function() {
                            $(this).blur();
                            removeRulesKey();
                    });
            }
            
            function eventRemoveShippingRules(focus) {
                    var root    = $(focus).parents('div[class*="-carriers-body"]');
                    var sname   = root.attr('class');
                    previous    = $(focus).attr('data-value');
                    removeRulesKey();
            }
            
            function eventRemoveColumnShippingRules(focus) {
                    var root    = $(focus).parents('div[class*="-carriers-body"]');
                    var sname   = root.attr('class');
                    if ( sname.search(/domestic-carriers-body/i) !== -1 ) {
                            previous = root.find("select[name^='domestic-carriers'] option:selected").text() + " - " + root.find("select[name^='domestic-ebay-carriers'] option:selected").text();
                    }
                    else if ( sname.search(/international-carriers-body/i) !== -1 ) {
                            previous = root.find("select[name^='international-carriers'] option:selected").text() + " - " + root.find("select[name^='international-ebay-carriers'] option:selected").text() + " ( " + root.find('select[name^="country-name"] option:selected').text() + " )";//$(this).attr('data-value', $(this).parents('.international-carriers-body').find('select[name^="international-carriers"] option:selected').text() + " - " + $(this).parents('.international-carriers-body').find('select[name^="international-ebay-carriers"] option:selected').text() + " ( " + $(this).parents('.international-carriers-body').find('select[name^="country-name"] option:selected').text() + " )");
                    }
                    removeRulesKey();
            }
            
            function setArrayValue(focus) {
                    var rule_body = $(focus).parents(".rules-carriers-body").find('.rules-label').text().replace(' : ', '');
                    var name = $(focus).attr('name');
                    var rel = $(focus).parents(".rules-body").attr('rel');
                    
                    $(".rules-main-cost").valid();
                    $(".rules-additionals").valid();
                    $(".rules-min-weight").valid();
                    $(".rules-max-weight").valid();
                    $(".rules-min-price").valid();
                    $(".rules-max-price").valid();
                    
                    if ( rules_controller === 'items' ) {
                            if (typeof(choose_value[rule_body][rules_controller][rel]) === 'undefined') {
                                    var item = {
                                            'cost'          : '0.00',
                                            'additionals'   : '0.00',
                                    };
                                    choose_value[rule_body][rules_controller][rel] = item;
                            }
                    }
                    else if ( rules_controller === 'weight' ) {
                            if (typeof(choose_value[rule_body][rules_controller][rel]) === 'undefined') {
                                    var item = {
                                            'cost'          : '0.00',
                                            'additionals'   : '0.00',
                                            'min-weight'    : '0.00',
                                            'max-weight'    : '0.00',
                                            'operation'     : 1
                                    };
                                    choose_value[rule_body][rules_controller][rel] = item;
                            }
                    }
                    else if ( rules_controller === 'surcharge' ) {
                            if (typeof(choose_value[rule_body][rules_controller][rel]) === 'undefined') {
                                    var item = {
                                            'cost'          : '0.00',
                                            'max-surcharge' : '0.00',
                                            'operation'     : 7
                                    };
                                    choose_value[rule_body][rules_controller][rel] = item;
                            }
                    }
                    else if ( rules_controller === 'prices' ) {
                            if (typeof(choose_value[rule_body][rules_controller][rel]) === 'undefined') {
                                     var item = {
                                            'cost'          : '0.00',
                                            'additionals'   : '0.00',
                                            'min-price'     : '0.00',
                                            'max-price'     : '0.00',
                                            'operation'     : 1
                                    };
                                    choose_value[rule_body][rules_controller][rel] = item;
                            }
                    }
                    choose_value[rule_body][rules_controller][rel][name] = parseFloat($(focus).val());
            }
            
            function eventSetDot($this, field) {
                    var main_value = $this.val();
                    if ( $this.val().substr(-1, 1) === '.' || $this.val().substr(-1, 1).charCodeAt() === 46 ) {
                            main_value = $this.val() + '00';
                    }
                    else if ( $this.val().indexOf('.') === -1 ) {
                            main_value = $this.val() + '.00';
                            if ($this.val() === '') {
                                    main_value = '0.00';
                            }
                    }
                    else if ( $this.val().indexOf('.') !== -1 ) {
                            if ( $this.val().slice($this.val().indexOf('.') + 1, $this.val().length).length === 1) {
                                    main_value = $this.val() + '0';
                            }
                    }
                    $(field).val(main_value);
                    setArrayValue(field);

                    if ($(field).val() === '') {
                            $(field).val('0.00');
                    }
            }
            
            
            function setDot($this, field) {
                    var main_value = $this.val();
                    if ( $this.val().substr(-1, 1) === '.' || $this.val().substr(-1, 1).charCodeAt() === 46 ) {
                            main_value = $this.val() + '00';
                    }
                    else if ( $this.val().indexOf('.') === -1 ) {
                            main_value = $this.val() + '.00';
                            if ($this.val() === '') {
                                    main_value = '0.00';
                            }
                    }
                    else if ( $this.val().indexOf('.') !== -1 ) {
                            if ( $this.val().slice($this.val().indexOf('.') + 1, $this.val().length).length === 1) {
                                    main_value = $this.val() + '0';
                            }
                    }
                    $(field).val(main_value);

                    if ($(field).val() === '') {
                            $(field).val('0.00');
                    }
            }           
            
            function eventSetAllValue() {
                    $('.rules-main-cost').unbind('change');
                    $('.rules-additionals').unbind('change');
                    $(".rules-main-cost").valid();
                    $(".rules-additionals").valid();
                    var root = $('.collapse-swap').filter(':visible').find(".rules-body");
                    root.each(function() {
                            var deep_root = $(this);
                            deep_root.find('.rules-main-cost:first').unbind('focusout');
                            deep_root.find('.rules-main-cost:first').on('focusout', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-main-cost').not(':first').each(function() {
                                            eventSetDot($this, this);
                                    });     
                                    $this.val('');
                            });


                            deep_root.find('.rules-main-cost').not(':first').unbind('focusout');
                            deep_root.find('.rules-main-cost').not(':first').on('focusout', function() {
                                    eventSetDot($(this), this);
                            });


                            deep_root.find('.rules-additionals:first').unbind('focusout');
                            deep_root.find('.rules-additionals:first').on('focusout', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-additionals').not(':first').each(function() {
                                            eventSetDot($this, this);
                                    });            
                                    $this.val('');
                            });


                            deep_root.find('.rules-additionals').not(':first').unbind('focusout');
                            deep_root.find('.rules-additionals').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                            });
                    });
            }
            
            function eventSetWeightValue() {
                    $('.rules-min-weight').unbind('change');
                    $('.rules-max-weight').unbind('change');
                    $('.rules-min-price').unbind('change');
                    $('.rules-max-price').unbind('change');
                    $('.rules-surcharge').unbind('change');
                    $(".rules-min-weight").valid();
                    $(".rules-max-weight").valid();
                    $(".rules-min-price").valid();
                    $(".rules-max-price").valid();
                    var root = $('.collapse-swap').filter(':visible').find(".rules-body");
                    root.each(function() {
                            var deep_root = $(this);
                            deep_root.find('.rules-min-weight:first').unbind('focusout');
                            deep_root.find('.rules-min-weight:first').on('focusout', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-min-weight').not(':first').each(function() {
                                            eventSetDot($this, this);
                                    });                            
                                    $this.val('');
                            });

                            deep_root.find('.rules-min-weight').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                            });

                            deep_root.find('.rules-max-weight:first').on('change', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-max-weight').not(':first').each(function() {
                                            if ( !this.disabled ) {
                                                    eventSetDot($this, this);
                                            }
                                    });            
                                    $this.val('');
                            });

                            deep_root.find('.rules-max-weight').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                            });

                            deep_root.find('.rules-surcharge:first').on('change', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-surcharge').not(':first').each(function() {
                                            if ( !this.disabled ) {
                                                    eventSetDot($this, this);
                                            }
                                    });            
                                    $this.val('');
                            });

                            deep_root.find('.rules-surcharge').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                            });
                            
                            deep_root.find('.rules-min-price:first').on('change', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-min-price').not(':first').each(function() {
                                            eventSetDot($this, this);
                                    });                            
                                    $this.val('');
                            });

                            deep_root.find('.rules-min-price').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                            });

                            deep_root.find('.rules-max-price:first').on('change', function() {
                                    var $this = $(this);
                                    deep_root.find('.rules-max-price').not(':first').each(function() {
                                            if ( !this.disabled ) {
                                                    eventSetDot($this, this);
                                            }
                                    });            
                                    $this.val('');
                            });

                            deep_root.find('.rules-max-price').not(':first').on('change', function() {
                                    eventSetDot($(this), this);
                            });
                    });
            }
            
            function setWeightInputField(focus, option) {
                    $(focus).removeAttr('selected').select2('val',option).trigger('change');
                    if ( option >= 3 ) {
                            $(focus).parents('.col-md-10').find('.rules-max-weight').attr({'disabled' : 'disabled'}).css({'color' : '#EEE'});
                            $(focus).parents('.col-md-10').find('.rules-max-price').attr({'disabled' : 'disabled'}).css({'color' : '#EEE'});
                    } else {
                            $(focus).parents('.col-md-10').find('.rules-max-weight').removeAttr('disabled').css({'color' : '#848484'});
                            $(focus).parents('.col-md-10').find('.rules-max-price').removeAttr('disabled').css({'color' : '#848484'});
                    }
            }
            
            function eventSetOperation() {
                    $('select.operation').unbind('change');
                    var root = $('.collapse-swap').filter(':visible').find(".rules-body");
                    root.each(function() {
                            var deep_root = $(this);
                            deep_root.find('select.operation:first').on('change', function() {
                                    var $this = $(this);
                                    var option = $(this).find('option:selected').val();
                                    deep_root.find('select.operation').not(':first').each(function() {
                                            setWeightInputField(this, option);
                                            setArrayValue(this);
                                    });                            
                                    $this.blur().find('option[value="1"]').attr({'selected': 'selected'});
                            });

                            deep_root.find('select.operation').not(':first').on('change', function() {
                                    var $this = $(this);
                                    var option = $(this).find('option:selected').val();
                                    setWeightInputField(this, option);    
                                    setArrayValue(this);
                                    $this.blur();
                            });
                    });
            }
            
            function addRules() {
                    $('.addrulecost').unbind('click').click(function() {
                            var root                    = $(this).parents('.rules-main').find('.rules-body').filter(':first');
                            var main                    = $(this).parents('.rules-main').find('.transparent');
                            var last                    = $(this).parents('.rules-main').find('.rules-body').filter(':last');
                            var icon                    = root.find('i').attr('class');
                            var rel                     = last.attr('rel');
                            var new_rel                 = parseInt(rel) + 1;

                            last.find('select.operation').select2("destroy");

                            if (   $(".rules-main-cost").valid() 
                                && $(".rules-additionals").valid() 
                                && $(".rules-min-weight").valid() 
                                && $(".rules-max-weight").valid() 
                                && $(".rules-min-price").valid() 
                                && $(".rules-max-price").valid() ) {

                                    var root    = $(".collapse-swap").filter(':visible').find(".rules-body[rel='"+ rel +"']").filter(':visible');
                                    cloned                      = last.clone().appendTo(main);
                                    cloned.show();
                                    cloned.find('label.error').remove();
                                    cloned.find('select, input').attr('disabled', false) ;   
                                    cloned.attr('rel', new_rel);

                                    cloned.find('.addrulecost > i').removeClass().addClass(icon);
                                    cloned.children('.rules-carriers-body').find('.rules-label').each(function() {
                                            var $cloned                 = $(this);
                                            $(root).children('.rules-carriers-body').find('.rules-label').each(function() {
                                                    if ( $(this).text() === $cloned.text() ) {
                                                            var to          = $(this).parents('.rules-carriers-body:first').find('input[rel="to"]').val();
                                                            var label_name  = $cloned.text().replace(' : ', '');
                                                            $cloned.parents('.rules-carriers-body:first').find('.rules-main-cost').attr('placeholder', '0.00').val('').attr('name', 'cost-' + label_name + '-' +rules_controller + '-' + new_rel);
                                                            $cloned.parents('.rules-carriers-body:first').find('.rules-additionals').attr('placeholder', '0.00').val('').attr('name', 'additionals-' + label_name + '-' +rules_controller + '-' + new_rel);

                                                            if ( rules_controller === 'prices') {
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-min-price').attr('placeholder', '0.00').val('').attr('name', 'min-price-' + label_name + '-' +rules_controller + '-' + new_rel).attr('rel', 'from');
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-price').attr('placeholder', '0.00').val('').attr('name', 'max-price-' + label_name + '-' +rules_controller + '-' + new_rel).attr('rel', 'to');
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-min-price').rules('add', { required: true, none: true, minlength: true, between: true });
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-price').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                            }
                                                            if ( rules_controller === 'weight') {
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-min-weight').attr('placeholder', '0.00').val('').attr('name', 'min-weight-' + label_name + '-' +rules_controller + '-' + new_rel).attr('rel', 'from');
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-weight').attr('placeholder', '0.00').val('').attr('name', 'max-weight-' + label_name + '-' +rules_controller + '-' + new_rel).attr('rel', 'to');
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-weight').rules('add', { required: true, none: true, minlength: true, between: true });
                                                                    $cloned.parents('.rules-carriers-body:first').find('.rules-max-weight').rules('add', { required: true, none: true, minlength: true, between: true, greater: true });
                                                            }
                                                            $cloned.parents('.rules-carriers-body:first').find('.rules-main-cost').rules('add', { required: true, none: true, minlength: true });
                                                            $cloned.parents('.rules-carriers-body:first').find('.rules-additionals').rules('add', { required: true, none: true, minlength: true });
                                                            $cloned.parents('.rules-carriers-body:first').find('input[rel="from"]').val(parseFloat(to) + 1);
                                                            setDot($cloned.parents('.rules-carriers-body:first').find('input[rel="from"]'), $cloned.parents('.rules-carriers-body:first').find('input[rel="from"]'));
                                                    }
                                            });
                                    });


                                    last.find('select.operation').select2();
                                    cloned.find('select.operation').select2();

                                    cloned.find('.addrulecost > i').removeClass().addClass('fa fa-minus-square-o').parent().removeClass().addClass('icons-plus removerulecost');//icon
                                    cloned.find('.operation').filter('option').removeAttr('selected').filter('option[value="0"]').prop('selected', true).css({'color': '#858585'});

                                    eventSetShippingRules();
                                    eventSetOperation();
                                    eventSetAllValue();
                                    eventSetWeightValue();
                                    removeMapping();
                                    removeRules();
                            }
                    });
            }

            $('.addlocalmapping').click(function() {
                    var root                    = $(this).parents('.domestic-main').find('.domestic-body').filter(':first');
                    var last                    = $(this).parents('.domestic-main').find('.domestic-body').filter(':last');
                    var icon                    = root.find('i').attr('class');
                    var rel                     = last.attr('rel');
                    var new_rel                 = parseInt(rel) + 1;
                    
                    root.find('select.domestic-carriers').select2("destroy");
                    root.find('select.domestic-ebay-carriers').select2("destroy");
                    
                    cloned                      = root.clone().appendTo('.domestic-main > .transparent');
                    cloned.show();
                    cloned.find('select').attr('disabled', false) ;   
                    cloned.attr('rel', new_rel);
                    cloned.find('select:first').val(0);
                    cloned.find('select:last').val(0).attr('name', 'domestic-ebay-carriers-' + new_rel);
                    cloned.find('select:first').attr('id', 'domestic-carriers-' + new_rel).attr('name', 'domestic-carriers-' + new_rel);
                    cloned.find('label:first').attr('for', 'domestic-carriers-' + new_rel);
                    cloned.find('.addlocalmapping > i').removeClass().addClass('fa fa-minus-square-o').parent().removeClass().addClass('icons-plus removemapping');//icon
                    cloned.find('.addlocalmapping').prependTo(root.find('col-xs-2 col-md-1'));
                    cloned.find('select:first').rules('add', {'required': true, both: true });
                    cloned.find('select.domestic-ebay-carriers').rules('add', {'required': true, both: true });
                    
                    root.find('select.domestic-carriers').select2();
                    root.find('select.domestic-ebay-carriers').select2();
                    cloned.find('select.domestic-carriers').select2();
                    cloned.find('select.domestic-ebay-carriers').select2();
                    cloned.find('label.error').remove();
                    cloned.find('input[name="form-carrier-default"]').checkbox();
                    
                    
                    $('select[name^="domestic-carriers"] option:selected').each(function( index, option ) {
                            cloned.find('select[name^="domestic-carriers"]').select2("destroy");
                            cloned.find('select[name^="domestic-carriers"] option[value="' + option.value + '"]').attr({'disabled' : 'disabled'});
                            cloned.find('select[name^="domestic-carriers"]').select2();
                    });
                    
                    cloned.find('select[name^="domestic-carriers"] option').each(function( index, option ) {
                            if ( $(this).val() === $('select[name^="domestic-carriers"]').last().find('option').not('[disabled="disabled"]').first().val() ) {
                                cloned.find('select[name^="domestic-carriers"]').select2("destroy");
                                $(this).attr('selected', 'selected');
                                cloned.find('select[name^="domestic-carriers"]').select2();
                            }
                    });
                    
                    if ( $('select[name^="domestic-carriers"] option:selected').length >= $('select[name^="domestic-carriers"]').first().find('option').length ) {
                            cloned.select2("destroy");
                            cloned.remove();
                            return(false) ;
                    }

                    international_set();
                    removeMapping();
            });
            
            $('.addintermapping').click(function() {
                    var root                    = $(this).parents('.international-main').find('.international-body').filter(':first');
                    var last                    = $(this).parents('.international-main').find('.international-body').filter(':last');
                    var icon                    = root.find('i').attr('class');
                    var rel                     = last.attr('rel');
                    var new_rel                 = parseInt(rel) + 1;
                    
                    root.find('select.country-name').select2("destroy");
                    root.find('select.international-carriers').select2("destroy");
                    root.find('select.international-ebay-carriers').select2("destroy");
                    
                    
                    cloned                      = root.clone().appendTo('.international-main > .transparent');
                    cloned.show();
                    cloned.find('select').attr('disabled', false);   
                    cloned.attr('rel', new_rel);
                    cloned.find('select.international-ebay-carriers').val(0).attr('name', 'international-ebay-carriers-' + new_rel);
                    cloned.find('select.international-carriers').attr('id', 'international-carriers-' + new_rel).attr('name', 'international-carriers-' + new_rel);
                    cloned.find('label.choose-carriers').attr('for', 'international-carriers-' + new_rel);
                    cloned.find('select.country-name').val(0).attr('id', 'country-name-' + new_rel).attr('name', 'country-name-' + new_rel);
                    cloned.find('select.country-name option').each(function() {
                            if ( $(this).text() === '' ) {
                                    $(this).remove();
                            }
                    });
                    cloned.find('label.choose-country').attr('for', 'country-name-' + new_rel);
                    cloned.find('.addintermapping > i').removeClass().addClass('fa fa-minus-square-o').parent().removeClass().addClass('icons-plus removemapping');//icon
                    
                    root.find('select.country-name').select2();
                    root.find('select.international-carriers').select2();
                    root.find('select.international-ebay-carriers').select2();
                    cloned.find('select.country-name').select2();
                    cloned.find('select.international-carriers').select2();
                    cloned.find('select.international-ebay-carriers').select2();
                    cloned.find('select').removeAttr('data-value', false).removeAttr('data-change', false);   
                    cloned.find('label.error').remove();
                    cloned.find('select[name="international-carriers-' + new_rel +'"]').rules('add', {'required': true, both: true });
                    cloned.find('select[name="international-ebay-carriers-' + new_rel +'"]').rules('add', {'required': true, both: true });
                    

                    selected_first = cloned.find('select[name^="country-name"]') ;
                    
                    cloned.find('select[name^="country-name"] option').each(function( index, option ) {
                            if ( $(this).val() === $('select[name^="country-name"]').last().find('option').not('[disabled="disabled"]').first().val() ) {
                                cloned.find('select[name^="country-name"]').select2("destroy");
                                $(this).attr('selected', 'selected');
                                cloned.find('select[name^="country-name"]').select2();
                            }
                    });
                    
                    if ( $('select[name^="country-name"] option:selected').length >= $('select[name^="country-name"]').first().find('option').length ) {
                            cloned.select2("destroy");
                            cloned.remove();
                            return(false) ;
                    }

                    international_set();
                    removeMapping();
                    set_validation();
            });
            
            
            $('.sp-addlocalmapping').click(function() {
                    var root                    = $(this).parents('.sp-domestic-main').find('.sp-domestic-body').filter(':first');
                    var last                    = $(this).parents('.sp-domestic-main').find('.sp-domestic-body').filter(':last');
                    var icon                    = root.find('i').attr('class');
                    var rel                     = last.attr('rel');
                    var new_rel                 = parseInt(rel) + 1;
                    
                    
                    if (  $(".sp-domestic-body").find("select.sp-domestic-ebay-carriers").valid() && $(".sp-domestic-body").find(".cost-rate").valid() && $(".sp-domestic-body").find(".additional-rate").valid() ) {
                            
                                root.find('select.sp-domestic-ebay-carriers').select2("destroy");
                                cloned                      = root.clone().appendTo('.sp-domestic-main > .transparent');
                                cloned.show();
                                cloned.find('select').attr('disabled', false) ;   
                                cloned.attr('rel', new_rel);
                                cloned.find('select:last').val(root.find('select:last').val()).attr('name', 'sp-domestic-ebay-carriers-' + new_rel);
                                cloned.find('.sp-addlocalmapping > i').removeClass().addClass('fa fa-minus-square-o').parent().removeClass().addClass('icons-plus sp-removemapping');//icon
                                cloned.find('.sp-addlocalmapping').prependTo(root.find('col-xs-2 col-md-1'));
                                cloned.find('select:last').rules('add', {'required': true, both: true });

                                root.find('select.sp-domestic-ebay-carriers').select2();
                                cloned.find('select.sp-domestic-ebay-carriers').select2();
                                cloned.find('label.error').remove();

                                $('select[name^="sp-domestic-ebay-carriers"] option:selected').each(function( index, option ) {
                                        cloned.find('select[name^="sp-domestic-ebay-carriers"]').select2("destroy");
                                        cloned.find('select[name^="sp-domestic-ebay-carriers"]').select2();
                                        cloned.find('select[name^="sp-domestic-ebay-carriers"] option[value="' + option.value + '"]').attr({'disabled' : 'disabled'});
                                });

                                cloned.find('select[name^="sp-domestic-ebay-carriers"] option').each(function( index, option ) {
                                        if ( $(this).val() === $('select[name^="sp-domestic-ebay-carriers"]').last().find('option').not('[disabled="disabled"]').first().val() ) {
                                            cloned.find('select[name^="sp-domestic-ebay-carriers"]').select2("destroy");
                                            $(this).attr('selected', 'selected');
                                            cloned.find('select[name^="sp-domestic-ebay-carriers"]').select2();
                                        }
                                });

                                if ( $('select[name^="sp-domestic-ebay-carriers"] option:selected').length >= $('select[name^="sp-domestic-ebay-carriers"] option') || addLocal >= 4 ) {
                                        cloned.select2("destroy");
                                        cloned.remove();
                                        return(false) ;
                                }
                                addLocal++;

                                cloned.find('.cost-rate').attr('name', 'cost-rate-' +new_rel).val('');
                                cloned.find('.additional-rate').attr('name', 'additionals-rate-' +new_rel).val('');
                                cost_rate(cloned.find('.cost-rate'));
                                cost_rate(cloned.find('.additional-rate'));
                                cloned.find('.cost-rate').rules('add', { required: true, none: true, });
                                cloned.find('.additional-rate').rules('add', { required: true, none: true, });

                                cloned.find('select.sp-domestic-ebay-carriers').on("select2-opening", function () {
                                        previous = $(this).find('option:selected').text();
                                        removeFocusKey(this);
                                });

                                international_set();
                                spRemoveMapping();
                                validation_number();
                    }
            });
            
            
            $('.sp-addintermapping').click(function() {
                    var root                    = $(this).parents('.sp-international-main').find('.sp-international-body').filter(':first');
                    var last                    = $(this).parents('.sp-international-main').find('.sp-international-body').filter(':last');
                    var icon                    = root.find('i').attr('class');
                    var rel                     = last.attr('rel');
                    var new_rel                 = parseInt(rel) + 1;
                    
                    if (  $(".sp-international-body").find("select.sp-international-ebay-carriers").valid() && $(".sp-international-body").find("select.sp-country-name").valid() && $(".sp-international-body").find(".cost-rate").valid() && $(".sp-international-body").find(".additional-rate").valid() ) {
                            root.find('select.sp-country-name').select2("destroy");
                            root.find('select.sp-international-ebay-carriers').select2("destroy");


                            cloned                      = root.clone().appendTo('.sp-international-main > .transparent');
                            cloned.show();
                            cloned.find('select').attr('disabled', false);   
                            cloned.attr('rel', new_rel);
                            cloned.find('select:first').val(0).attr('name', 'sp-international-ebay-carriers-' + new_rel);
                            cloned.find('select:first').rules('add', {'required': true, both: true });
                            cloned.find('label.sp-choose-carriers').attr('for', 'sp-international-carriers-' + new_rel);
                            cloned.find('select.sp-country-name').val(0).attr('id', 'sp-country-name-' + new_rel).attr('name', 'sp-country-name-' + new_rel);
                            cloned.find('select.sp-country-name option').each(function() {
                                    if ( $(this).text() === '' ) {
                                            $(this).remove();
                                    }
                            });
                            cloned.find('select.sp-country-name').rules('add', {'required': true, both: true });
                            cloned.find('label.sp-choose-country').attr('for', 'sp-country-name-' + new_rel);
                            cloned.find('.sp-addintermapping > i').removeClass().addClass('fa fa-minus-square-o').parent().removeClass().addClass('icons-plus sp-interRemovemapping');//icon

                            root.find('select.sp-country-name').select2();
                            root.find('select.sp-international-ebay-carriers').select2();
                            cloned.find('select.sp-country-name').select2();
                            cloned.find('select.sp-international-ebay-carriers').select2();
                            cloned.find('select').removeAttr('data-value', false).removeAttr('data-change', false);   
                            cloned.find('label.error').remove();

                            cloned.find('select[name^="sp-international-ebay-carriers"] option').each(function( index, option ) {
                                    if ( $(this).val() === $('select[name^="sp-international-ebay-carriers"]').last().find('option').not('[disabled="disabled"]').first().val() ) {
                                        cloned.find('select[name^="sp-international-ebay-carriers"]').select2("destroy");
                                        $(this).attr('selected', 'selected');
                                        cloned.find('select[name^="sp-international-ebay-carriers"]').select2();
                                    }
                            });

                            if ( $('select[name^="sp-international-ebay-carriers"] option:selected').length >= $('select[name^="sp-international-ebay-carriers"] option') || addInter >= 4 ) {
                                    cloned.select2("destroy");
                                    cloned.remove();
                                    return(false) ;
                            }
                            addInter++;

                            cloned.find('.cost-rate').attr('name', 'cost-rate-inter-' +new_rel).val('');
                            cloned.find('.additional-rate').attr('name', 'additionals-rate-inter-' +new_rel).val('');
                            cost_rate(cloned.find('.cost-rate'));
                            cost_rate(cloned.find('.additional-rate'));
                            cloned.find('.cost-rate').rules('add', { required: true, none: true, });
                            cloned.find('.additional-rate').rules('add', { required: true, none: true, });

                            cloned.find('select.sp-international-ebay-carriers').on("select2-opening", function () {
                                    previous = $(this).find('option:selected').text();
                                    removeFocusKey(this);
                            });

                            international_set();
                            spInterRemoveMapping();
                            validation_number();
                            set_validation();
                    }
            });
            
            function removeMapping() {
                    $('.removemapping').click(function() {
                            $(this).parents('.domestic-body').remove();
                            $(this).parents('.international-body').remove();
                            eventRemoveColumnShippingRules(this);
                    });
            }
            
            function spRemoveMapping() {
                    $('.sp-removemapping').unbind('click').click(function() {
                            $(this).parents('.sp-domestic-body').remove();
                            addLocal--;
                    });
            }
            
            function spInterRemoveMapping() {
                    $('.sp-interRemovemapping').unbind('click').click(function() {
                            $(this).parents('.sp-international-body').remove();
                            addInter--;
                    });
            }
 
            function removeRules() {
                    $('.removerulecost').click(function() {
                            $(this).parents('.rules-body').remove();
                    });
            }
            
            function cost_rate(ele) {
                    var start_string = $(ele).first().attr('placeholder').search(" ");
                    var start_string = parseInt(start_string) + 2 ;
                    var last_string = parseInt(start_string) + 3 ;
                    var lgcurrency = $(ele).first().attr('placeholder').substring(start_string, last_string);
                    $(ele).parents(".input-group:first").find('strong').text(lg_current[0][lgcurrency]);
                    $(ele).unbind('focusout');
                    $(ele).on('focusout', function() {
                            var $this = $(this);
                            setDot($this, this);
                    });
            }
            
            function specify_cost_rate() {
                    $('.sp-domestic-body').find('.cost-rate').each(function() {
                            cost_rate($(this));
                            $(this).rules('add', { required: true, none: true, });
                    });

                    $('.sp-domestic-body').find('.additional-rate').each(function() {
                            cost_rate($(this));
                            $(this).rules('add', { required: true, none: true, });
                    });
                    
                    $('.sp-international-body').find('.cost-rate').each(function() {
                            cost_rate($(this));
                    });
                    
                    $('.sp-international-body').find('.additional-rate').each(function() {
                            cost_rate($(this));
                    });
                    
                    $('.sp-domestic-body').find('select.sp-domestic-ebay-carriers').on("select2-opening", function () {
                            previous = $(this).find('option:selected').text();
                            removeFocusKey(this);
                    });
                    
                    $('.sp-international-body').find('select.sp-international-ebay-carriers').on("select2-opening", function () {
                            previous = $(this).find('option:selected').text();
                            removeFocusKey(this);
                    });
                    
                    $('select.sp-international-ebay-carriers').each(function() {
                            $name1   = $(this).parents('.sp-international-carriers-body:first').find('select.sp-international-ebay-carriers');
                            $name2   = $(this).parents('.sp-international-carriers-body:first').find('select.sp-country-name');
                            if ( $name2.attr('name') === 'sp-country-name-2' && $name2.find('option:selected').val() === '0' && $name1.find('option:selected').val() === '0' ) {
                                $(this).parents('.sp-international-carriers-body:first').find($name1).rules('remove');
                                $(this).parents('.sp-international-carriers-body:first').find($name2).rules('remove');
                                $(this).parents('.sp-international-carriers-body:first').find('input[name="cost-rate-inter-2"]').rules('remove');
                                $(this).parents('.sp-international-carriers-body:first').find('input[name="additional-rate-inter-2"]').rules('remove');
                            }
                    });
            }
            
            function validation_number() {
                        $(".rules-main-cost, .rules-additionals, .rules-min-weight, .rules-max-weight, .rules-min-price, .rules-max-price, .additional-rate, .cost-rate").unbind('keyup');
                        $(".rules-main-cost, .rules-additionals, .rules-min-weight, .rules-max-weight, .rules-min-price, .rules-max-price, .additional-rate, .cost-rate").on("keyup", function(){
                            var valid = /^\+?[0-9][0-9]*(\.\d{0,2})?$/.test(this.value),
                            val = this.value;

                            if(!valid){
                                console.log("Invalid input!");
                                this.value = val.substring(0, val.length - 1);
                            }
                            
                            var e = window.event || e;
				var keyUnicode = e.charCode || e.keyCode;
				if (e !== undefined) {
					switch (keyUnicode) {
						case 16: break; // Shift
						case 17: break; // Ctrl
						case 18: break; // Alt
						case 27: this.value = ''; break; // Esc: clear entry
						case 35: break; // End
						case 36: break; // Home
						case 37: break; // cursor left
						case 38: break; // cursor up
						case 39: break; // cursor right
						case 40: break; // cursor down
						case 78: break; // N (Opera 9.63+ maps the "." from the number key section to the "N" key too!) (See: http://unixpapa.com/js/key.html search for ". Del")
						case 110: break; // . number block (Opera 9.63+ maps the "." from the number block to the "N" key (78) !!!)
						case 190: break; // .
						default: $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1, eventOnDecimalsEntered: true,symbol:'',groupDigits:false });
					}
				}
                        });
                }
                
                $.validator.addMethod("both", function(value, element) {
                        return this.optional(element) || value != 0 || $(element).is('input');
                        }, 
                        $('input[name="validate-carrier_rule-select"]').val()
                );
                
                $.validator.addMethod(
                        "regex",
                        function(value, element, regexp) {
                            var re = new RegExp(regexp);
                            return this.optional(element) || re.test(value);
                        },
                        $('input[name="validate-carrier_rule-input"]').val()
                );
        
                
                international_set();
                function international_set() {
                        $('select[name^="country-name"]').each(function() {
                                if ( this.name !== 'country-name-1' && $(this, 'option:selected').val() !== '0' ) {
                                    $name   = $(this).parents('.international-carriers-body:first').find('select.international-carriers');
                                    $name2   = $(this).parents('.international-carriers-body:first').find('select.international-ebay-carriers');

                                    $(this).parents('.international-carriers-body:first').find($name).rules('add', {'required': true, both: true });
                                    $(this).parents('.international-carriers-body:first').find($name2).rules('add', {'required': true, both: true });
                                }
                        });
                        
                        $('select[name^="domestic-ebay-carriers"]').unbind('focus');
                        $('select[name^="domestic-ebay-carriers"]').on("select2-opening", function () {
                                $(this).attr('data-value', $(this).parents('.domestic-carriers-body').find('select[name^="domestic-carriers"] option:selected').text() + " - " + $(this).find('option:selected').text());
                                previous = $(this).attr('data-value');
                                removeFocusKey(this);
                        });
                        
                        $('select[name^="domestic-carriers"]').unbind('focus');
                        $('select[name^="domestic-carriers"]').on("select2-opening", function () {
                                $(this).attr('data-value', $(this).find('option:selected').text() + " - " + $(this).parents('.domestic-carriers-body').find('select[name^="domestic-ebay-carriers"] option:selected').text());
                                previous = $(this).attr('data-value');
                                removeFocusKey(this);
                        });
                        
                        $('select[name^="country-name"]').unbind('focus');
                        $('select[name^="country-name"]').on("select2-opening", function () {
                                $(this).attr('data-value', $(this).parents('.international-carriers-body').find('select[name^="international-carriers"] option:selected').text() + " - " + $(this).parents('.international-carriers-body').find('select[name^="international-ebay-carriers"] option:selected').text() + " ( " + $(this).find('option:selected').text() + " )");
                                previous = $(this).attr('data-value');
                                removeFocusKey(this);
                        });
                        
                        $('select[name^="international-ebay-carriers"]').unbind('focus');
                        $('select[name^="international-ebay-carriers"]').on("select2-opening", function () {
                                $(this).attr('data-value', $(this).parents('.international-carriers-body').find('select[name^="international-carriers"] option:selected').text() + " - " + $(this).find('option:selected').text() + " ( " + $(this).parents('.international-carriers-body').find('select[name^="country-name"] option:selected').text() + " )");
                                previous = $(this).attr('data-value');
                                removeFocusKey(this);
                        });
                        
                        $('select[name^="international-carriers"]').unbind('focus');
                        $('select[name^="international-carriers"]').on("select2-opening", function () {
                                $(this).attr('data-value', $(this).find('option:selected').text() + " - " + $(this).parents('.international-carriers-body').find('select[name^="international-ebay-carriers"] option:selected').text() + " ( " + $(this).parents('.international-carriers-body').find('select[name^="country-name"] option:selected').text() + " )");
                                previous = $(this).attr('data-value');
                                removeFocusKey(this);
                        });
                        
                        $('select[name^="country-name"]').unbind('change');
                        $('select[name^="country-name"]').on('change', function() {
                                $(this).parents('.international-carriers-body:first').find('label.error').remove();
                                $(this).attr('data-change', $(this).find('option:selected').text());
                                if ( this.name !== 'country-name-1' && $(this, 'option:selected').val() !== '0' ) {
                                    $name   = $(this).parents('.international-carriers-body:first').find('select.international-carriers');
                                    $name2   = $(this).parents('.international-carriers-body:first').find('select.international-ebay-carriers');
                                    $name3   = $(this).parents('.international-carriers-body:first').find('select.country-name');
                                    $(this).parents('.international-carriers-body:first').find($name).rules('add', {'required': true, both: true });
                                    $(this).parents('.international-carriers-body:first').find($name2).rules('add', {'required': true, both: true });
                                    $(this).parents('.international-carriers-body:first').find($name3).rules('add', {'required': true, both: true });
                                }
                                eventRemoveShippingRules(this);
                                $(this).attr('data-value', $(this).parents('.international-carriers-body').find('select[name^="international-carriers"] option:selected').text() + " - " + $(this).parents('.international-carriers-body').find('select[name^="international-ebay-carriers"] option:selected').text() + " ( " + $(this).parents('.international-carriers-body').find('select[name^="country-name"] option:selected').text() + " )");
                                if ( this.name.slice(0, this.name.length - 1) === 'country-name-' && $(this, 'option:selected').val() === '0' && $name.find('option:selected').val() === '0' && $name2.find('option:selected').val() === '0' ) {
                                    $(this).parents('.international-carriers-body:first').find($name).rules('remove');
                                    $(this).parents('.international-carriers-body:first').find($name2).rules('remove');
                                    $(this).parents('.international-carriers-body:first').find($name3).rules('remove');
                                }
                                $(this).parents('.international-carriers-body').find($name).valid();
                                $(this).parents('.international-carriers-body').find($name2).valid();
                                $(this).parents('.international-carriers-body').find($name3).valid();
                        });
                        
                        $('select[name^="international-ebay-carriers"], select[name^="international-carriers"]').unbind('change').on('change', function() {
                                $(this).parents('.international-carriers-body:first').find('label.error').remove();
                                $(this).attr('data-change', $(this).find('option:selected').text());
                                var parent_root = $(this).parents('.international-carriers-body');
                                $name   = $(this).parents('.international-carriers-body:first').find('select.international-carriers');
                                $name2   = $(this).parents('.international-carriers-body:first').find('select.international-ebay-carriers');
                                $name3   = $(this).parents('.international-carriers-body:first').find('select.country-name');
                                $(this).parents('.international-carriers-body:first').find($name).rules('add', {'required': true, both: true });
                                $(this).parents('.international-carriers-body:first').find($name2).rules('add', {'required': true, both: true });
                                $(this).parents('.international-carriers-body:first').find($name3).rules('add', {'required': true, both: true });
                                if ( this.name === 'international-ebay-carriers-2' && $(this, 'option:selected').val() === '0' && $name.find('option:selected').val() === '0' && $name2.find('option:selected').val() === '0' ) {
                                    $(this).parents('.international-carriers-body:first').find($name).rules('remove');
                                    $(this).parents('.international-carriers-body:first').find($name2).rules('remove');
                                    $(this).parents('.international-carriers-body:first').find($name3).rules('remove');
                                }
                                $(this).parents('.international-carriers-body').find($name).valid();
                                $(this).parents('.international-carriers-body').find($name2).valid();
                                $(this).parents('.international-carriers-body').find($name3).valid();
                        });
                        
                        $('select[name^="domestic-ebay-carriers"]').unbind('change').on('change', function() {
                                $(this).parents('.domestic-carriers-body:first').find('label.error').remove();
                        });
                        
                        $('select[name^="sp-international-ebay-carriers"]').unbind('change').on('change', function() {
                                $(this).parents('.sp-international-carriers-body:first').find('label.error').remove();
                                $name   = $(this).parents('.sp-international-carriers-body:first').find('select.sp-country-name');
                                $name2   = $(this).parents('.sp-international-carriers-body:first').find('select.sp-international-ebay-carriers');
                                if ( this.name === 'sp-international-ebay-carriers-2' && $(this, 'option:selected').val() === '0' && $name.find('option:selected').val() === '0' ) {
                                    $(this).parents('.sp-international-carriers-body:first').find($name).rules('remove');
                                    $(this).parents('.sp-international-carriers-body:first').find($name2).rules('remove');
                                    $(this).parents('.sp-international-carriers-body:first').find('input[name="cost-rate-inter-2"]').rules('remove');
                                    $(this).parents('.sp-international-carriers-body:first').find('input[name="additional-rate-inter-2"]').rules('remove');
                                }
                                else {
                                    $(this).parents('.sp-international-carriers-body:first').find($name).rules('add', {'required': true, both: true });
                                    $(this).parents('.sp-international-carriers-body:first').find($name2).rules('add', {'required': true, both: true });
                                    $(this).parents('.sp-international-carriers-body:first').find('input[name^="cost-rate-inter-"]').rules('add', {'required': true, both: true });
                                    $(this).parents('.sp-international-carriers-body:first').find('input[name^="additional-rate-inter-"]').rules('add', {'required': true, both: true });
                                }
                                $(this).parents('.sp-international-carriers-body:first').find($name).valid();
                                $(this).parents('.sp-international-carriers-body:first').find($name2).valid();
                                $(this).parents('.sp-international-carriers-body:first').find('input[name="cost-rate-inter-2"]').valid();
                                $(this).parents('.sp-international-carriers-body:first').find('input[name="additional-rate-inter-2"]').valid();
                        });
                        
                        $('select[name^="sp-country-name"]').unbind('change').on('change', function() {
                                $(this).parents('.sp-international-carriers-body:first').find('label.error').remove();
                                $name   = $(this).parents('.sp-international-carriers-body:first').find('select.sp-country-name');
                                $name2   = $(this).parents('.sp-international-carriers-body:first').find('select.sp-international-ebay-carriers');
                                if ( this.name === 'sp-country-name-2' && $(this, 'option:selected').val() === '0' && $name2.find('option:selected').val() === '0' ) {
                                    $(this).parents('.sp-international-carriers-body:first').find($name).rules('remove');
                                    $(this).parents('.sp-international-carriers-body:first').find($name2).rules('remove');
                                    $(this).parents('.sp-international-carriers-body:first').find('input[name="cost-rate-inter-2"]').rules('remove');
                                    $(this).parents('.sp-international-carriers-body:first').find('input[name="additional-rate-inter-2"]').rules('remove');
                                }
                                else {
                                    $(this).parents('.sp-international-carriers-body:first').find($name).rules('add', {'required': true, both: true });
                                    $(this).parents('.sp-international-carriers-body:first').find($name2).rules('add', {'required': true, both: true });
                                    $(this).parents('.sp-international-carriers-body:first').find('input[name="cost-rate-inter-2"]').rules('add', {'required': true, both: true });
                                    $(this).parents('.sp-international-carriers-body:first').find('input[name="additional-rate-inter-2"]').rules('add', {'required': true, both: true });
                                }
                                $(this).parents('.sp-international-carriers-body:first').find($name).valid();
                                $(this).parents('.sp-international-carriers-body:first').find($name2).valid();
                                $(this).parents('.sp-international-carriers-body:first').find('input[name="cost-rate-inter-2"]').valid();
                                $(this).parents('.sp-international-carriers-body:first').find('input[name="additional-rate-inter-2"]').valid();
                        });
                }

                function set_validation() {
                        var rules = new Object();
                        var messages = new Object();
                        
                        rules['form-postal-code'] = { 
                                required        : true, 
                                regex           : $('#form-postal-code').attr('pattern') ,
                                postal_valid    : $('#form-postal-code').attr('data-code') 
                        };

                        messages['form-postal-code'] = { 
                                required        : $('input[name="validate-form-postal-code-required"]').val(),
                                regex           : $('input[name="validate-form-postal-code-regex"]').val(),
                                postal_valid    : $('input[name="validate-form-postal-code-postal-valid"]').val()
                        };
                        $.validator.addMethod(
                                "regex",
                                function(value, element, regexp) {
                                    var re = new RegExp(regexp);
                                    return this.optional(element) || re.test(value);
                                },
                                $('input[name="validate-form-postal-code-required"]').val()
                        );
                
                        $.validator.addMethod("both", function(value, element) {
                                return this.optional(element) || value != 0 || $(element).is('input');
                                }, 
                                $('input[name="validate-carrier_rule-select"]').val()
                        );
                
                        $.validator.addMethod("choose_default", function(value, element) {
                                return this.optional(element) || (($('select[name="domestic-carriers-2"]').val() !== "0" && $('select[name="domestic-ebay-carriers-2"]').val() !== '0') || ($('select[name="domestic-ebay-carriers-1"]').val() !== '0' && $('input[name="form-carrier-default"]').prop("checked")));
                                }, 
                                $('input[name="validate-price-modifier-required"]').val()
                        );
                
                        $.validator.addMethod("none", function(value, element) {
                                return this.optional(element) || value !== '';
                                }, 
                                $('input[name="validate-price-modifier-required"]').val()
                        );
                
                        $.validator.addMethod(
                                "greater",
                                function(value, element) {
                                    var validator = this;
                                    var from = $(element).parents('.rules-carriers-body:first').find('input[rel="from"]').val();
                                    greater_message = $('input[name="validate-price-modifier-range"]').val().replace('{1}', from);
                                    if ( rules_controller === 'weight') {
                                            greater_message = $('input[name="validate-price-modifier-range-weight"]').val().replace('{1}', from);
                                    }
                                    $.validator.messages.greater = greater_message;
                                    return this.optional(element) || parseFloat(value) > parseFloat(from);
                                },
                                greater_message
                        );

                        $.validator.addMethod(
                                "between",
                                function(value, element) {
                                    var rel = $(element).parents('.rules-body:first').attr('rel');
                                    var label = $(element).parents('.rules-carriers-body:first').find('.rules-label').text();
                                    var check_all = true;
                                    var root    = $(".collapse-swap").filter(':visible').find(".rules-body").filter(':visible');
                                    $(root).each(function() {
                                            if ( $(this).attr('rel') !== rel ) {
                                                    $(this).children('.rules-carriers-body').find('.rules-label').each(function() {
                                                            if ( $(this).text() === label  ) {
                                                                    var from    = $(this).parents('.rules-carriers-body:first').find('input[rel="from"]').val();
                                                                    var to      = $(this).parents('.rules-carriers-body:first').find('input[rel="to"]').val();
                                                                    if ( parseFloat(from) <= value && value <= parseFloat(to) ) {
                                                                            check_all = false;
                                                                            between_message = $('input[name="validate-price-modifier-between"]').val().replace('{1}', from).replace('{2}', to);
                                                                            if ( rules_controller === 'weight') {
                                                                                    between_message = $('input[name="validate-price-modifier-between-weight"]').val().replace('{1}', from).replace('{2}', to);
                                                                            }
                                                                            $.validator.messages.between = between_message;
                                                                    }
                                                            }
                                                    });
                                            }
                                    });
                                    return this.optional(element) || check_all;
                                },
                                between_message
                        );
                    
                        $('select.domestic-carriers').each(function() {
                            if ( this.name !== 'domestic-carriers-1' && this.name !== 'domestic-carriers-2' ) {
                                rules[this.name] = { required: true, both: true };
                                messages[this.name] = { required: $('input[name="validate-carrier_rule-required"]').val() };
                            }
                            
                            if ( this.name == 'domestic-carriers-2' ) {
                                rules[this.name] = { choose_default: true };
                                messages[this.name] = { required: $('input[name="validate-carrier_rule-required"]').val() };
                            }
                        });
                        $('select.domestic-ebay-carriers').each(function() {
                            if ( this.name !== 'domestic-ebay-carriers-1' && this.name !== 'domestic-ebay-carriers-2') {
                                rules[this.name] = { required: true, both: true };
                                messages[this.name] = { required: $('input[name="validate-carrier_rule-required"]').val() };
                            }
                            
                            if ( this.name == 'domestic-ebay-carriers-2' ) {
                                rules[this.name] = { choose_default: true };
                                messages[this.name] = { required: $('input[name="validate-carrier_rule-required"]').val() };
                            }
                        });
                        $('select.country-name').each(function() {
                            if ( this.name !== 'country-name-1' && $(this, 'option:selected').val() !== '0' ) {
                                $name   = $(this).parents('international-carriers-body:first').find('select.international-carriers').attr('name');
                                $name2   = $(this).parents('.international-carriers-body:first').find('select.international-ebay-carriers').attr('name');
                                rules[$name] = { required: true, both: true };
                                messages[$name] = { required: $('input[name="validate-carrier_rule-required"]').val() };
                                rules[$name2] = { required: true, both: true };
                                messages[$name2] = { required: $('input[name="validate-carrier_rule-required"]').val() };
                            }
                        });

                        $('select.country-name').each(function() {
                            if ( this.name !== 'country-name-1' && $(this, 'option:selected').val() !== '0' ) {
                                $name   = $(this).parents('international-carriers-body:first').find('select.international-carriers').attr('name');
                                $name2   = $(this).parents('.international-carriers-body:first').find('select.international-ebay-carriers').attr('name');
                                rules[$name] = { required: true, both: true };
                                messages[$name] = { required: $('input[name="validate-carrier_rule-required"]').val() };
                                rules[$name2] = { required: true, both: true };
                                messages[$name2] = { required: $('input[name="validate-carrier_rule-required"]').val() };
                            }
                        });
                        
                        $('#tab-2 input[name="form-postal-code"]').attr({'name': 'sp-form-postal-code', id : 'sp-form-postal-code'});
                        rules['sp-form-postal-code'] = { required: true, regex : $('input[name="sp-form-postal-code"]').attr('pattern'), postal_valid : $('input[name="sp-form-postal-code"]').attr('data-code')  };
                        messages['sp-form-postal-code'] = { 
                                required        : $('input[name="validate-form-postal-code-required"]').val(),
                                regex           : $('input[name="validate-form-postal-code-regex"]').val(),
                                postal_valid    : $('input[name="validate-form-postal-code-postal-valid"]').val()
                        };
                        
                        $('select.sp-domestic-ebay-carriers').each(function() {
                                if ( this.name !== 'sp-domestic-ebay-carriers-1') {
                                    rules[this.name] = { required: true, both: true };
                                    messages[this.name] = { required: $('input[name="validate-carrier_rule-required"]').val() };
                                }
                        });

                        
                        $("#form-submit").validate({
                        	ignore: 'input[type=hidden]',
                                rules: rules,
                                messages: messages,
                                errorPlacement: function(error, element) {
                                        if ($(element).hasClass("domestic-carriers") 
                                         || $(element).hasClass("domestic-ebay-carriers")   
                                         || $(element).hasClass("international-carriers")   
                                         || $(element).hasClass("international-ebay-carriers")   
                                         || $(element).hasClass("country-name")   
                                         || $(element).hasClass("sp-domestic-ebay-carriers")   
                                         || $(element).hasClass("sp-international-ebay-carriers")   
                                        ) {
                                                $(element).parent().find('.select2-container a').addClass('group-error');
                                                error.insertAfter( $(element) );
                                        } 
                                        else if ($(element).hasClass("form-postal-code") ) {
                                                $(element).parent().find('input').addClass('group-error');
                                                error.insertAfter( $(element) );
                                        } 
                                        else {
                                                error.appendTo($(element).parent());
                                        }
                                },
                                unhighlight: function (element) {
                                        if ($(element).hasClass("domestic-carriers") 
                                         || $(element).hasClass("domestic-ebay-carriers")   
                                         || $(element).hasClass("international-carriers")   
                                         || $(element).hasClass("international-ebay-carriers")   
                                         || $(element).hasClass("country-name")   
                                         || $(element).hasClass("sp-domestic-ebay-carriers")   
                                         || $(element).hasClass("sp-international-ebay-carriers")   
                                        ) {
                                                $(element).parent().find('.select2-container a').removeClass('group-error');
                                        }
                                        else if ($(element).hasClass("form-postal-code") ) {
                                                $(element).parent().find('input').removeClass('group-error');
                                        }
                                },
                                debug: true
                        });
                }

                $.validator.addMethod(
                        "postal_valid",
                        function(value, element, zip) {
                                data_valid      = {
                                    postal_code : value,
                                    zip         : zip
                                };
                                if ( zip !== 'IE' ) {
                                        spinAdd();
                                        $.ajax ({
                                                type       : "POST",
                                                url        : base_url + "ebay/get_postalcode_valid",
                                                data       : {postal_valid : data_valid},
                                                error      : function(jqXHR, textStatus, errorThrown) {
                                                           notification_alert(errorThrown,'bad');
                                               },
                                               success     : function(save) {
                                                           if ( save === 'Success') {
                                                                    postal_valid_res = 1;
                                                                    $('.form-postal-code').removeClass('error');
                                                           }
                                                           else {
                                                                    postal_valid_res = 0;
                                                                    if(chk_value!=value){
                                                                         $(".form-postal-code").unbind('blur').blur();
                                                                    }
                                                                    chk_value = value;
                                                           }
                                               }
                                        });
                                }
                                else {
                                        postal_valid_res = 1;
                                }
                            return this.optional(element) || postal_valid_res === 1;
                        },
                        $('input[name="validate-form-postal-code-required"]').val()
                );
        
                $('#form-postal-code').focusout(function() {
                        $('#form-postal-code').valid();
                });
                
                $('#sp-form-postal-code').focusout(function() {
                        $('#sp-form-postal-code').valid();
                });
    });