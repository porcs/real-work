
        var postal_valid;
        var postal_valid_res                = 1;
        var chk_value                       = '';
        var data                            = new Array();
        var wizard_data                     = new Array();
        var cloned                          = $("div.close").clone();
        var category_id                     = 0;
        
        function updateCountdown() {
                var remaining = 500 - $('#payment-instructions').val().length;
                $('.countdown-textarea').text($('.remaining').val() + ': ' + remaining);
        }

        updateCountdown();
        $('#payment-instructions').change(updateCountdown);
        $('#payment-instructions').keyup(updateCountdown);


        $("#form-submit").on('submit', function(e) {
                e.preventDefault();
                data                            = [];
                var dataChecked                 = [];
                $('input[name="form-payment-checkbox"]').each(function() {
                        if ( $(this).is(":checked") ) {
                                dataChecked.push($(this).val());
                        }
                });

                data_selected                   = {
                        'id_profile'                        : $('.id_profile').val(),
                        'payment_method'                    : dataChecked,
                        'payment_instructions'              : $("#payment-instructions").val(),
                        'id_site'                           : $("#id-site").val(),
                        'id_packages'                       : $("#id-packages").val(),
                        'step'                              : $("#popup-step").val(),
                };
                data.push(data_selected);
                
                if ($.isEmptyObject(data)) {
                        data_value              = {
                                'id_site'               : $("#id-site").val(),
                                'id_packages'           : $("#id-packages").val(),
                        };
                        data.push(data_value);
                }
                
                var data_selected = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'id_popup'      : $("#popup-step").val(),
                };
                wizard_data.push(data_selected);

                ajax_save(e);
        });
