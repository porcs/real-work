        var data                            = new Array();
        var wizard_data                     = new Array();
        var insert_variation;
        var previous;
        
        var app = angular.module('ebayApp', [])
        .config(['$interpolateProvider', function ($interpolateProvider) {
            $interpolateProvider.startSymbol('[[');
            $interpolateProvider.endSymbol(']]');
        }])
        .controller('VariationValue', function ($scope, $parse) {
            document.querySelector('#attribute-mapping').classList.remove('hide');
            $scope.defaultVariationString = [];
            $scope.mappingVariationString = [];
            $scope.mappingData = [];
            $scope.showDefault = [];
            $scope.pushMapping = {label: 'mapping', decription: document.querySelector('#push_mapping').value, inverse: 'default'};
            $scope.pushDefault = {label: 'default', decription: document.querySelector('#push_default').value, inverse: 'mapping'};
            $scope.appData = JSON.parse(JSON.stringify(app_data));
            
            $scope.addVariationValue = function (id, value) {
                if ( value.attribute_name !== null )
                    $scope.mappingVariationString[id] = {name: value.attribute_name};
                else 
                    $scope.mappingVariationString[id] = {name: value.name};
            }
            
            $scope.isArray = function(arr) {
                return angular.isArray(arr);
            }
            
            $scope.isObject = function(obj) {
                return angular.isObject(arr);
            }

            $scope.setVariationDefault = function (id) {
                $scope.mappingData[id] = $scope.pushDefault;
            }

            $scope.setVariationMapping = function (id) {
                $scope.mappingData[id] = $scope.pushMapping;
            }

            $scope.switchVarianOption = function (id, option) {
                $scope.showDefault[id] = !$scope.showDefault[id];
                ( option === 'default') ? $scope.setVariationDefault(id) : $scope.setVariationMapping(id);
            }
            
            $scope.checkMapping = function (id, attributes) {
                $scope.showDefault[id] = true;
                $scope.mappingData[id] = $scope.pushDefault;
                if ( attributes.attribute_name !== null) {
                    $scope.switchVarianOption(id, 'mapping');
                }
            }

        });
        
        
        
        $("#form-submit").on('submit', function(e) {
                e.preventDefault();
                data                                        = [];

                $('html, body').animate({ scrollTop: 0 }, 0);

                $('.attribute-row').each(function() {
                        var $group                                  = $(this).attr('data-group');
                        var ebay_value                              = $(this).find('input.ebay-row').val();
                        var visibility                              = $(this).is(':visible');
                        var mode                                    = 0;

                        if ( ebay_value !== '' && visibility) {
                                var data_value                               = {
                                        'id_profile'                        : $('.id_profile').val(),
                                        'id_attribute_group'                : $group,
                                        'name_attribute'                    : $(this).find('input.fixs-row').val(),
                                        'name_attribute_ebay'               : ebay_value,
                                        'mode'                              : 0,
                                        'tab'                               : 1,
                                        'id_site'                           : $("#id-site").val(),
                                        'id_packages'                       : $("#id-packages").val(),
                                        'step'                              : $("#popup-step").val(),
                                };
                                data.push(data_value);
                        }
                });

                if ($.isEmptyObject(data)) {
                        var data_value              = {
                                'id_site'               : $("#id-site").val(),
                                'id_packages'           : $("#id-packages").val(),
                        };
                        data.push(data_value);
                }

                var data_selected = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'id_popup'      : $("#popup-step").val(),
                };
                wizard_data.push(data_selected);

                ajax_save(e);
        });
