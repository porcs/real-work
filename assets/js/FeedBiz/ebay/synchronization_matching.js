        
        var data                            = new Array();
        var cloned                          = $("div.close").clone();
        var offset  = 0;
        var limit   = 5;
        var first   = 0;
        var second   = 0;
        var ColVis_first = 0;
        var ColVis_second = 0;
        var bootbox_confirm_all = 0;
        var bootbox_skip_all = 0;

        var column = {};
        column[$('.product-image-lang').val()] = 1;
        column[$('.product-item-lang').val()] = 2;
        column[$('.product-content-lang').val()] = 3;
        column[$('.product-information-lang').val()] = 4;
        column[$('.product-detail-lang').val()] = 5;
        column[$('.product-active-lang').val()] = 6;
        column[$('.profile-name').val()] = 7;
        
        var oTable = $('#statistics').DataTable( {
            "processing": true,
            "serverSide": true,
            "searching": true,
            "bStateSave": true,
            "sDom": 'C<"clear">lfrtip',
            "oColVis": {
                aiExclude: [0, 1, 3, 4],
//                bRestore: true,
//                activate: "mouseover",
                bShowAll: $('.show-all').val(),
            },
            "ajax": base_url + "ebay/profile_categories_processing?limit=" + limit + '&offset=' + offset + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val(),
            "fnRowCallback": function( nRow, aData, iDisplayIndex ) { 
                    var row = oTable.row( nRow );
                    function product_combination(data, parent){
                            var out = '<table class="dynamicTable colVis table table-combination">';  
                            var i=0;
                            $.each(data,function(k,d){  
                                var attribute_name = '';
                                $.each(d,function(ki,di){  
                                        attribute_name += "<div class='td_content'><div class='td_attribute_type'>"+di.attribute_name + " :</div> " + di.attribute_value +"</div>";
                                }); 
                                    out+=
                                        '<tr class="combinations">'+
                                        "<td class='center check-all' rel='check_all' data-rel='0'></td>"+
                                        "<td class='center product-image' rel='product_image' data-rel='1'></td>"+
                                        "<td class='center id-product' rel='id_product' data-rel='2'><i class='fa fa-level-up lb'></i></td>"+
                                        "<td class='product-content' rel='product_content' data-rel='3'>"+attribute_name+"</td>"+
//                                        "<td class='profile-name' rel='profile_name' data-rel='7'></td>"+
                                        "<td class='product-information' rel='product_information' data-rel='4'><div class='td_information'>"+($.isEmptyObject(d[0].sku) === false ? "<div class='td_sku'>" + $('.product-sku').val()+" :</div> "+d[0].sku : '')+"</div><div class='td_information'>"+($.isEmptyObject(d[0].ean13) === false ? "<div class='td_ean13'>" + $('.product-ean13').val()+" :</div> "+d[0].ean13 : '')+"</div><div class='td_information'>"+($.isEmptyObject(d[0].upc) === false ? "<div class='td_upc'>" + $('.product-upc').val()+" :</div> "+d[0].upc : '')+"</div><div class='td_information'>"+($.isEmptyObject(d[0].reference) === false ? "<div class='td_reference'>" + $('.product-reference').val()+" :</div> "+d[0].reference : '')+"</div></td>"+
                                        "<td class='product-detail' rel='product_detail' data-rel='5'><div class='td_detail'><div class='td_price'>"+$('.product-price').val()+" :</div> "+ d[0].price+ " " + lg_current[0][parent.iso_code] +"</div><div class='td_detail'><div class='td_quantity'>"+$('.product-quantity').val()+" :</div> "+d[0].quantity+"</div><div class='td_detail'><div class='td_quantity'>"+$('.condition-name').val()+" :</div> "+parent.condition_name+"</div></td>"+
                                        "<td class='center product-active' rel='active' data-rel='6'></td>"+
//                                        "<td class='center date_add' rel='date_add'>"+d[0].date_add+"</td>"+
//                                        "<td class='center date_upd' rel='date_upd'>"+($.isEmptyObject(d[0].date_upd) === false ? d[0].date_upd : '')+"</td>"+
                                        '</tr>'; 
                                    i++;
                            }); 
                            out+='</table>';
                            return out;
                    }
                    
                    function product_detail(data, row){
                            $(row).find('.product-image').html('');
                            if ( typeof(data.product) !== 'undefined' && data.product.length !== 0 ) {
                                    $(row).find('.product-image').addClass('haveChild').html('<i class="fa fa-plus-square"></i>');
                                    var out = '<table class="dynamicTable colVis table table-product">';  
                                    var i=0;
                                    var comb = '';
                                    $.each(data.product,function(k,d){  
                                        $(row).attr({'data-synchronize': d.product_name, 'data-product': d.id_product});
                                        d.price = d.price.replace(/\{B\}/g, '<b>').replace(/\{\/B\}/g, '</b>');
                                        d.quantity = d.quantity.replace(/\{B\}/g, '<b>').replace(/\{\/B\}/g, '</b>');
                                        out+=
                                                '<tr role="field">'+
                                                "<td class='center check-all' rel='check_all' data-rel='0'></td>"+
                                                "<td class='center product-image' rel='product_image' data-rel='1'><a href='"+d.image_url+"'><img src='"+d.image_url+"'></a></td>"+
                                                "<td class='center id-product' rel='id_product' data-rel='2'>"+d.id_product+"</td>"+
                                                "<td class='product-content' rel='product_content' data-rel='3'><div class='td_content td_content_title'><div class='td_title'>"+$('.product-title').val()+" :</div> "+d.product_name+"</div><div class='td_content'><div class='td_manufacturer'>"+$('.product-manufacturer').val()+" :</div> "+d.manufacturer_name+"</div><div class='td_content'><div class='td_supplier'>"+$('.product-supplier').val()+" :</div> "+d.supplier_name+"</div></td>"+
//                                                "<td class='profile-name' rel='profile_name' data-rel='7'></td>"+
                                                "<td class='product-information' rel='product_information' data-rel='4'>"+(( $.isEmptyObject(d.combinations) === true ) ? "<div class='td_information'>"+($.isEmptyObject(d.sku) === false ? "<div class='td_sku'>" + $('.product-sku').val()+" :</div> "+d.sku : '')+"</div><div class='td_information'>"+($.isEmptyObject(d.ean13) === false ? "<div class='td_ean13'>" + $('.product-ean13').val()+" :</div> "+d.ean13 : '')+"</div><div class='td_information'>"+($.isEmptyObject(d.upc) === false ? "<div class='td_upc'>" + $('.product-upc').val()+" :</div> "+d.upc : '')+"</div><div class='td_information'>"+($.isEmptyObject(d.reference) === false ? "<div class='td_reference'>" + $('.product-reference').val()+" :</div> "+d.reference : '')+"</div>" : '') + "</td>"+
                                                "<td class='product-detail' rel='product_detail' data-rel='5'>"+(( $.isEmptyObject(d.combinations) === true ) ? "<div class='td_detail'><div class='td_price'>"+$('.product-price').val()+" :</div> "+ d.price+ " " + lg_current[0][d.iso_code] +"</div><div class='td_detail'><div class='td_quantity'>"+$('.product-quantity').val()+" :</div> "+d.quantity+"</div><div class='td_detail'><div class='td_quantity'>"+$('.condition-name').val()+" :</div> "+d.condition_name+"</div>" : '') + "</td>"+
                                                "<td class='center product-active' rel='active' data-rel='6'>"+((d.active === '1') ? '<i class="fa fa-toggle-on active"></i>' : '<i class="fa fa-toggle-off"></i>')+"</td>"+
                                                '</tr>'; 
                                        i++;
                                            if ( $.isEmptyObject(d.combinations) === false ) {
                                                    comb = product_combination(d.combinations, d); 
                                            }
                                    }); 
                                    out+=comb+='</table>';
                                    return out;
                            }
                            return;
                    }
                    var image = aData.img

                    $(nRow).find('td').each(function() {
                            if ( $(this).hasClass('check-all') ) {
                                    $(this).attr('data-rel', 0).html('<div class="checkbox_profile"><label class="cb-checkbox cb-sm"><input class="checkbox_row" type="checkbox"></label></div>');
                            }
                            else if ( $(this).hasClass('product-image') ) {
                                    $(this).addClass('td_plus').attr('data-rel', 1);
                            }
                            else if ( $(this).hasClass('id-product') ) {
                                    $(this).attr('data-rel', 2).html('');
                            }
                            else if ( $(this).hasClass('product-content') ) {
                                    $(this).attr('data-rel', 3).html('<strong>' + $('.synchronize-name').val() + ' :</strong> ' + $(this).html());
                            }
                            else if ( $(this).hasClass('product-information') ) {
                                    $(this).parents('tr:first').attr('data-reference', aData.reference);
                                    $(this).attr('data-rel', 4).html("<div class='td_sku'>" + $('.product-sku').val()+" :</div>" + $(this).text());
                            }
                            else if ( $(this).hasClass('product-detail') ) {
                                    $(this).attr('data-rel', 5).html('');
                            }
                            else if ( $(this).hasClass('product-active') ) {
                                    $(this).attr('data-rel', 6).html(($(this).text() === '1') ? '<i class="fa fa-toggle-on active"></i>' : '<i class="fa fa-toggle-off"></i>');
                            }
                    })
                            .parents('tr').attr('rel', aData.id_category)
                            .attr('data-value', aData.category_name)
                            .attr('data-enabled', aData.active);

                    var ch = row.child('', 'no-padding ch_row c_'+iDisplayIndex) ;
                    var txt = product_detail(aData, nRow);
                    ch.show();
                    setTimeout(function(){ 
                        $('#statistics').find('td.c_'+iDisplayIndex).html(txt);
                        if ( !$('.checkbox_profile > .cb-checkbox').children().hasClass('cb-inner') ) {
                                $('.checkbox_profile').checkBo();
                                $('.checkbox_profile').enableCheckboxRangeSelection();
                                $('.checkbox_profile').clickCheckboxChange();
                        }
                        $('#statistics').find('td.c_'+iDisplayIndex).parent().prev().find(".td_plus").each(function() {
                                if ( $(this).hasClass("haveChild") ) {
                                        $(this).unbind('click').click(function() {
                                                if ( $(this).hasClass("td_minus") ) {
                                                        $(this).removeClass('td_minus').addClass('td_plus').html('<i class="fa fa-plus-square"></i>');
                                                        $(this).parents('tr[role="row"]').next('tr').find('.table-combination').hide();
                                                        $(this).parents('tr[role="row"]').next('tr').find('.table-product').hide();
                                                }
                                                else if ( $(this).hasClass("td_plus") ) {
                                                        $(this).removeClass('td_plus').addClass('td_minus').html('<i class="fa fa-minus-square"></i>');
                                                        $(this).parents('tr[role="row"]').next('tr').find('.table-combination').show();
                                                        $(this).parents('tr[role="row"]').next('tr').find('.table-product').show();
                                                }
                                        });
                                        
                                        $(this).find('i').unbind('click').click(function() {
                                                $('.ColVis_collection').find('li').each(function() {
                                                        var rel = column[$.trim($(this).text())];
                                                        if ( !$(this).find('input').is(':checked') ) {
                                                                $('tbody td[data-rel="'+rel+'"]').hide();
                                                        }
                                                });
                                        });
                                }
                        });
                    },1);
            },
            fnPreDrawCallback : function( oSettings, aData ) {
//                    console.log(oSettings);
                    $('#statistics_processing').hide();
                    limit   = oSettings._iDisplayLength;
                    offset  = oSettings._iDisplayStart;
                    if ( offset % limit > 0 ) {
                            offset = Math.floor(offset / limit);
                    }
                    var id_product          = $('input[rel="id_product"]').val();
                    var product_content     = $('input[rel="product_content"]').val();
                    var product_information = $('input[rel="product_information"]').val();
                    var product_detail      = $('input[rel="product_detail"]').val();
                    var active              = $('select.product-active').val() !== '-1' ? $('select.product-active').val() : '';
                    var insert_key          = ($('input.insert-key-name').is(':visible') && $('input.insert-key-name').val()) ? $('input.insert-key-name').val() : '';
                    var reference_key       = ($('input.reference-key-name').is(':visible') && $('input.reference-key-name').val()) ? $('input.reference-key-name').val() : '';
                    var choose_ref          = $('select.reference').val();
                    var show_list           = $('select.synchronize-select').val();
                    var all                 = $('.dataTables_filter input').val();
                    
                    aSorting                = [];
                    aSorting.column         = oSettings.aaSorting[0][0];
                    aSorting.by             = oSettings.aaSorting[0][1];
                    dataFilter = {
                        'id_product'            : id_product,
                        'product_content'       : product_content,
                        'product_information'   : product_information,
                        'product_detail'        : product_detail,
                        'active'                : active,
                        'insert_key'            : insert_key,
                        'reference_key'         : reference_key,
                        'show_list'             : show_list,
                        'choose_ref'            : choose_ref,
                        'sort_name'             : aSorting.column,
                        'sort_by'               : aSorting.by,
                        'all'                   : all,
                    };
                    dataSorting = {
                        2                       : 'id_product',
                    };

                    $.each(dataSorting, function(index, val) {
                            if ( index == aSorting.column) {
                                    dataFilter.sort_name        = val;
                            }
                    });
                    oSettings._iDisplayStart    = offset;
                    oSettings.iDraw             = Math.floor(oSettings._iDisplayStart / limit);
                    oSettings.ajax              = base_url + "ebay/synchronization_matching_processing?limit=" + limit + '&offset=' + offset + '&' + $.param(dataFilter) + '&id_site=' + $('#id-site').val() + '&id_packages=' + $('#id-packages').val();
                    
                    if (first == 0) {
                            $('#statistics_wrapper')
                                    .append("<div class='row'><div class='col-xs-4'></div><div class='col-xs-8'></div></div>");
                            $('.dataTables_paginate')
                                    .appendTo("#statistics_wrapper > .row > .col-xs-8:last");
                            $('#statistics_wrapper')
                                    .prepend("<div class='row separator bottom'><div class='col-md-6 form-horizontal'></div><div class='col-md-6'></div></div>");
                            $('#statistics_length')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length')
                                    .append("<div class='col-xs-6 col-md-6'></div>")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length > label > select')
                                    .addClass('.selectpicker')
                                    .appendTo("#statistics_length > .col-xs-6.col-md-6")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_length > label:first')
                                    .addClass('col-xs-2 col-md-2 control-label')
                                    .text('Per page');
                            $('.row.separator.bottom').find('.col-md-6:first').append('<div class="dataTables_header"><label class="control-label">'+$('.category-mapping').val()+'</label></div>');
                            $('#statistics_filter')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6.form-horizontal:first")
                                    .parents("#statistics_wrapper")
                                    .find("#statistics_filter")
                                    .append("<div class='col-md-8'></div>")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_filter > label > input')
                                    .addClass('form-control').attr('placeholder', $('.hidden-search').val())
                                    .appendTo("#statistics_filter > .col-md-8")
                                    .parents("#statistics_wrapper")
                                    .find('#statistics_filter > label:first')
                                    .addClass('col-md-6 control-label').hide();
                            $('.ColVis')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last");
                            $('.dataTables_info')
                                    .appendTo("#statistics_wrapper > .row.separator.bottom > .col-md-6:last");
                            first++;
                            $(".prev a").each(function() {
                                        $(this).text($(this).text().replace("← ", ''));
                            });
                            $(".next a").each(function() {
                                        $(this).text($(this).text().replace(" → ", ''));
                            });
                    }
            },
            fnDrawCallback: function( oSettings ) {               
                if ( ColVis_first === 0 ) {
                        $('.ColVis_Button').trigger('click');
                        $('.ColVis_collection').find('li').each(function() {
                                var $li                 = $(this);
                                if ( $(this).find('.cb-checkbox').length === 0 ) {
                                        if ( $(this).find('span').length > 0 ) {
                                                if ( $(this).find('input').prop('checked') === true ) {
                                                        $(this).find('label').addClass('cb-checkbox cb-sm checked');
//                                                        $(this).wrapInner('<label class="cb-checkbox cb-sm checked"></label>');
                                                }
                                                else {
                                                        $(this).find('label').addClass('cb-checkbox cb-sm');
//                                                        $(this).wrapInner('<label class="cb-checkbox cb-sm"></label>');
                                                }
                                                $(this).checkBo();
                                                var checkbox = $(this);
                                                $(this).find('.cb-checkbox').unbind('click');
                                                $(this).find('i').unbind('click').on('click', function(e) {
                                                        if ( e.target.tagName.toString() === 'INPUT') {
                                                                return false;
                                                        }
                                                        e.preventDefault();
                                                        $(this).parents('li').trigger('click');
                                                });
                                                $(this).find('span:last').on('click', function(e) {
                                                        e.preventDefault();
                                                        $(this).parents('li').trigger('click');
                                                });
                                                $(this).on('click', function(e) {
                                                        if ( $(this).find('input').is(':checked') ) {
                                                                $(this).find('label.cb-checkbox').addClass('checked');
                                                                var rel = column[$.trim($(this).text())];
                                                                if ( $(this).find('input').is(':checked') ) {
                                                                        $('tbody td[data-rel="'+rel+'"]').show();
                                                                        if ( rel === 6 ) {
                                                                                $('.table.dataTable > tbody > tr').find('td.product-detail').html('');
                                                                        }
                                                                        else if ( rel === 7 ) {
                                                                                $('.table.dataTable > tbody > tr > td.product-active').each(function() {
                                                                                        if ($(this).text() === '1') {
                                                                                                $(this).html('<i class="fa fa-toggle-on active"></i>');
                                                                                        }
                                                                                        else if ($(this).text() === '0') {
                                                                                                $(this).html('<i class="fa fa-toggle-off"></i>');
                                                                                        }
                                                                                        else if ($(this).html() === '<i class="fa fa-toggle-off"></i>') {
                                                                                                $(this).html('<i class="fa fa-toggle-off"></i>');
                                                                                        }
                                                                                        else if ($(this).html() === '<i class="fa fa-toggle-on active"></i>') {
                                                                                                $(this).html('<i class="fa fa-toggle-on active"></i>');
                                                                                        }
                                                                                        else {
                                                                                                $(this).html('<i class="fa fa-toggle-off"></i>');
                                                                                        }
                                                                                });
                                                                        }
                                                                }
                                                        }
                                                        else if ( $(this).find('input').is(':checked') === false ) {
                                                                $(this).find('label.cb-checkbox').removeClass('checked');
                                                                var rel = column[$.trim($(this).text())];
                                                                if ( !$(this).find('input').is(':checked') ) {
                                                                        $('tbody td[data-rel="'+rel+'"]').hide();
                                                                }
                                                        }
                                                });
                                                
                                                setTimeout(function() {
                                                        var settings = oTable.settings();
                                                        if ( settings[0].oInstance[0].clientWidth < 1800 ) {
                                                                if ( $.inArray(column[$.trim($li.find('span:last').text())], settings[0].oInit.oColVis.aiExclude) === -1 && $li.find('input').is(':checked') ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                        else if ( settings[0].oInstance[0].clientWidth >= 1800  ) {
                                                                if ( $.inArray(column[$.trim($li.find('span:last').text())], settings[0].oInit.oColVis.aiExclude) === -1 && $li.find('input').is(':checked') === false ) {
                                                                        $li.trigger('click');
                                                                }
                                                        }
                                                }, 200);
                                        }
                                        if ( $(this).text() === $('.show-all').val() ) {
                                                var checkbox = $(this);
                                                $(this).find('.cb-checkbox').unbind('click');
                                                $(this).unbind('click').click(function() {
                                                        $('.ColVis_collection').find('input').each(function() {
                                                                if ( $(this).prop('checked') === false ) {
                                                                        $(this).parents('li').trigger('click');
                                                                }
                                                        });
                                                })
                                        }
                                }
                        });
                }
                ColVis_first++;
                showWaiting(false);
            },
            "oLanguage": {
                "sInfo": "<strong>_START_</strong> - <strong>_END_</strong> of <strong>_TOTAL_</strong>",
                "sInfoEmpty": "",
                "oPaginate": {
                    "sFirst": "First page",
                },
            },
            "columns": [
                { "data": "id_category_default", "bSortable": false, "sClass": "center check-all" },
                { "data": "image_url", "bSortable": false, "sClass": "center product-image" },
                { "data": "id_category", "sClass": "center id-product" },
                { "data": "category_name", "bSortable": false, "sClass": "product-content" },
//                { "data": "profile_name", "bSortable": false, "sClass": "profile-name" },
                { "data": "reference", "bSortable": false, "sClass": "product-information" },
                { "data": "price", "bSortable": false, "sClass": "product-detail"  },
                { "data": "active", "bSortable": false, "sClass": "center product-active" },
            ],
            "aaSorting": [[ 2, "DESC" ]],
            "aLengthMenu": [
                [1, 10, 25, 50, 100, 200, -1],
                [1, 10, 25, 50, 100, 200, "All"]
            ], iDisplayLength: 10,
        } );
        
        
        $('#reference').select2();
        $('.synchronize-select').select2();
        
        $('#statistics').find('input').on('keyup', function() {
                oTable.draw();
        });
        
        $('#statistics').find('select').on('change', function() {
                oTable.draw();
        });
        
        $('.btn-synchronization').unbind('click').on('click', function() {
                showWaiting(true);
                var time =0;
                var num = $('body').find('input').length;
                if(num > 2){
                        time = 200;
                } 
                setTimeout(function(){
                        oTable.draw();
                        setTimeout(function() { showWaiting(false); }, 5000)
                },time);
        });
        
        $('#statistics').checkBo({
            checkAllButton : '#checkAllButton',      // Default: null
            checkAllTarget : '.checkbox_profile',      // Default: null
        });
        
        
        $(document).mouseup(function (e) {
                if ($('.ColVis_collection').has(e.target).length === 0){
                        $('.ColVis_collection').hide();
                        $('.ColVis_catcher').hide();
                }
        });
        
        $('select.reference').on('change', function() {
                if ( $('select.reference option:selected').val() === 'custom' ) {
                    $('.insert-key-group').toggleClass('insert-key-group-hide')
                }
                else {
                    $('.insert-key-group').removeClass('insert-key-group-hide').addClass('insert-key-group-hide')
                }
        });
        
        (function($) {
            $.fn.enableCheckboxRangeSelection = function() {
                    var lastCheckbox = null;
                    var $spec = this;
                    var lastType = null;

                    $spec.unbind("click");
                    $spec.bind("click", function(e) {
                            var lastTypeElement = e.currentTarget.getElementsByTagName('input');
                            lastType = $(lastTypeElement).prop('checked');
                            if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {
                                    var checked = $spec.slice(
                                            Math.min($spec.index(lastCheckbox), $spec.index($(this))),
                                            Math.max($spec.index(lastCheckbox), $spec.index($(this))) + 1
                                    ); 
                                    if ( lastType == true ) {
//                                            checked.each(function() {
//                                                $(this).find('input').prop('checked', true).change();
////                                                $(this).checkBo();
//                                            });
                                            checked.find('input').prop('checked', true).change();
                                    } 
                                    else {
                                            checked.find('input').prop('checked', false).change();
                                    }
                                    lastCheckbox = null; 
                            }
                            else {
                                    lastCheckbox = $(this);
                            }
                    });
            };
        })(jQuery);
        
        (function($) {
            $.fn.bootboxCheckboxSelection = function() {
                    var lastCheckbox = null;
                    var $spec = this;
                    var lastType = null;

                    $spec.unbind('change').change(function(e) {
                            $change = $(this);
                            var lastTypeElement = e.currentTarget.getElementsByTagName('input');
                            lastType = $(lastTypeElement).prop('checked');
                            var lastClass = $(lastTypeElement).attr('class');
//                            bootbox_confirm_all
                            if ( lastClass === 'confirm_bootbox_row' && lastType == true ) {
                                    bootbox_skip_all = 0;
                                    bootbox_confirm_all = 1;
                            }
                    });
            };
        })(jQuery);
        
        (function($) {
                $.fn.clickCheckboxChange =  function() {
                        var lastCheckbox = null;
                        var $spec = this;
                        var lastType = null;

                        $spec.unbind('change').change(function(e) {
                                if ( !$(this).hasClass('click_check') ) {
                                        $(this).addClass('click_check');
                                        $change = $(this);
                                        var lastTypeElement = e.currentTarget.getElementsByTagName('input');
                                        lastType = $(lastTypeElement).prop('checked');

                                        if ( bootbox_confirm_all === 0 ) { 
                                                    if ( bootbox_skip_all === 0 ) {
                                                            bootbox.confirm({
                                                                    title: $('.confirm-auto-map').val(),
                                                                    message: '<div class="bootbox-body-title">'+$('.are-you-sure').val().replace(/\{A\}/g, $(this).parents('tr').attr('data-value')).replace(/\{B\}/g, '<B>').replace(/\{\/B\}/g, '</B>').replace(/\{D\}/g, $(this).parents('tr').attr('data-synchronize')) + '</div>' +
                                                                             '<div class="checkbox_bootbox"><label class="cb-checkbox cb-sm"><input class="confirm_bootbox_row" type="checkbox" value="'+bootbox_confirm_all+'" '+((bootbox_confirm_all) ? 'checked="checked"' : '')+'>'+$('.confirm-all').val()+'</label></div>' + 
                                                                             '<script>$(".checkbox_bootbox").checkBo()</script>' +
                                                                             '<script>$(".checkbox_bootbox").bootboxCheckboxSelection()</script>',
                                                                    callback: function(result) {   
                                                                            $change.find('input').prop('checked', false).parents('label').removeClass('checked');
                                                                            if (result === true) {                                             
                                                                                    replace_save($change);                          
                                                                            }
                                                                    }
                                                            });
                                                    }
                                        }
                                        else {
                                                replace_save($change);
                                        }

                                        setTimeout(function() { 
                                                $spec.removeClass('click_check'); 
                                        }, 100);
                                }
                        });
                }
        })(jQuery);
        
        function replace_save($change) {
                data                                = [];
                var data_selected                   = {
                        'sku'                               : $change.parents('tr').attr('data-reference'),
                        'id_product'                        : $change.parents('tr').attr('data-product'),
                        'synchronize_name'                  : $change.parents('tr').attr('data-value'),
                        'id_site'                           : $("#id-site").val(),
                        'id_packages'                       : $("#id-packages").val(),
                };

                data.push(data_selected);
                var page = $("#current-page").val();
                spinAdd();
                $('#loading-result').show();
                $("#success").text("").hide();
                $("#error").text("").hide();
                $.ajax ({
                        type        : "POST",
                        url         : base_url + "ebay/" + $('#ajax-url').val(),
                        data        : { data : data },
                        error       : function(jqXHR, textStatus, errorThrown) {
                                    spinRemove();
                                    $('#loading-result').hide();
                                    $("#error").text("").show().append(cloned.css('visibility', 'visible').find('i:last').removeClass().addClass('fa fa-times-circle').parents('div.close').find('i').css('visibility', 'visible').parent().html() + " " + errorThrown);
                                    dismiss($("#error"));
                        },
                        success     : function(results) {
                                    setTimeout(function() { 
                                    spinRemove();
                                    $('#loading-result').hide();
                                    if ( $.trim(results) === 'Success' ) {  
                                            $("#success").text("").show().append(cloned.css('visibility', 'visible').find('i:last').removeClass().addClass('fa fa-check-circle').parents('div.close').find('i').css('visibility', 'visible').parent().html() + " " + $(".update-" + page + "-success").val());
                                            dismiss($("#success"));
                                            oTable.draw();
                                    }
                                    else {    
                                            $("#error").text("").show().append(cloned.css('visibility', 'visible').find('i:last').removeClass().addClass('fa fa-times-circle').parents('div.close').find('i').css('visibility', 'visible').parent().html() + " " + $(".update-" + page + "-fail").val());
                                            dismiss($("#error"));
                                    }
                                    }, 100);
                        }
                });
        }

//        $('.checkbox_row').enableCheckboxRangeSelection();
//        $('.TableTools_collection').checkBo();
        

