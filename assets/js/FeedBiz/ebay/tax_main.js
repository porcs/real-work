        
        var data                            = new Array();
        var wizard_data                     = new Array();
        
        $("#form-submit").on('submit', function(e) {
                var dataChecked;
                data                        = [];
                wizard_data                 = [];
                $('input[name="form-price-rounding"]').each(function() {
                        if ( $(this).is(":checked") ) {
                                dataChecked = $(this).val();
                        }
                });

                $(".tax-type option:selected").each(function() {
                        data_selected = {
                            'id_tax'        : $(this).val(),
                            'tax_name'      : $(this).text(),
                            'tax_decimal'   : dataChecked,
                            'id_site'       : $("#id-site").val(),
                            'id_packages'   : $("#id-packages").val(),
                        };

                        data.push(data_selected);
                });
                
                var data_selected = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'id_popup'      : $("#popup-step").val(),
                };
                wizard_data.push(data_selected);
                
                ajax_save(e);
        });