        var just_cal = false;
        var just_check_all = false;
        var old_select;
        var first_click;
        var active_li                               = '1';
        var active_count                            = 0;
        var data                                    = new Array();
        var wizard_data                             = new Array();
        var cloned                                  = $("div.close").clone();
        var global_cloned;
        var page                                    = $("#current-page").val();

        $('.widget-tabs li').unbind('click').click(function() {
                $('#page-load').show();    
                $tab_li     = $(this);
                active_li   = $('a', this).attr('href').replace('#tab-', '');
//                if ( (active_li === '3' || active_li === '4') && active_count === 0 ) {
//                        $('.getStore').trigger('click');
//                        active_count++;
//                }
//                else{
//                	$('.getStore').hide();
//                }
                if ( $(this).attr('open') !== 'open' ) {
                        getSelectedAllCategory();
                        apply();
                        $tab_li.attr('open', 'open');
                }
                else {
                        $('#page-load').hide();    
                }
        });
    
        $('form').on('reset', function(e) {
            setTimeout(function() {
                $('.checkbox-custom > input[type=checkbox]').each(function() {
                    if ($(this).is(':checked') == false) {
                        $(this).prop('checked', false);
                        $(this).closest('ul li.tree-folder-name').find('.treeRight').hide();
                        $(this).checkbox('uncheck');
                    }
                });
            });
        });
    
        $(".checkedAll").unbind('click').click(function () {
                showWaiting(true);
                var time =0;
                var num = $('body').find('input').length;
                if(num > 2){
                        time =200;
                } 
                setTimeout(function(){ 
                        $("#tab-" + active_li).find("input[type=checkbox]").each(function(index) {
                                if(!$(this).prop( "checked" )){
                                        $(this).prop('checked', true).change();
//                                        $(this).checkbox('check');
//                                        getProfile($(this));
                                        if(!just_check_all){
                                            just_cal=false;
                                            just_check_all = true;
                                        }
                                        $(this).closest('ul li.tree-folder-name').find('input.select-main').attr('rel', index).parents('.treeRight:first').show();
                                        select_click();
                                }
                        });
                        showWaiting(false);
                },time);
        });
        
        $(".uncheckedAll").unbind('click').click(function () {
            showWaiting(true);
            var time =0;
            var num = $('body').find('input').length;
            if(num > 2){
                time =200;
            } 
            setTimeout(function() {
                    $("#tab-" + active_li).find("input[type=checkbox]").each(function(){
                        $(this).prop('checked', false).change();
                        $(this).closest('ul li.tree-folder-name').find('.treeRight').hide();
//                        $(this).checkbox('uncheck');
                    });
                    showWaiting(false);
             },time);
        });

        getSelectedAllCategory();
        apply();
   
        
        function init_rel_all_row() {
            var i=1;
            if(just_cal) return;
            just_cal = true;
            $('.cp_down_btn').each(function(){
                if($(this).parents('#main').length>0)return;
                var row = $(this).parent().parent().parent();//$(this).parents('.tree-folder-header');
                if(row.length == 0)return;
                row.attr('rel',i); 
                i++;
            });
        }
        
        function showWaiting(type) {   
            if(type){
                $.blockUI();
            }else{
                $.unblockUI();
            }
        }
        
        function click_copy_val_sel(obj){
        	showWaiting(true);
        	var currentTree = $(obj).closest('.tree_point');
        	var currentLeaf = $(obj).closest('.treeRight');
        	var currentLeafOffset = $(currentLeaf).offset();
        	var currentValue = $('input', currentLeaf).val();
        	var currentKey = $('input', currentLeaf).attr('data-key');
        	$('.treeRight', currentTree).each(function(eleIndex, eleObject){        	
        		if(currentLeafOffset.top < $(eleObject).offset().top){
        			//case blank
        			if(currentValue == 0 || currentValue == '' || currentValue == undefined) {
                                        if ( active_li === '3' || active_li === '4' ) {
                                                $('input', eleObject).val($(".choose-ebay-store-categories").val()).attr('data-value', 0).attr('data-key', 0);
                                        }
                                        else {
                                                $('input', eleObject).val($(".choose-ebay-categories").val()).attr('data-value', 0).attr('data-key', 0);
                                        }
        			}//case value
        			else if($('input', eleObject).val() == 0 || $('input', eleObject).val() == '' || ($('input', eleObject).val() == $(".choose-ebay-categories").val() && currentValue != $(".choose-ebay-categories").val()) || $('input', eleObject).val() == undefined ||
                                        $('input', eleObject).val() == 0 || $('input', eleObject).val() == '' || ($('input', eleObject).val() == $(".choose-ebay-store-categories").val() && currentValue != $(".choose-ebay-store-categories").val()) || $('input', eleObject).val() == undefined ){
        				$('input', eleObject).val(currentValue).attr('data-value', currentValue).attr('data-key', currentKey);
        			}
        			else if(currentValue == $(".choose-ebay-categories").val() && $('input', eleObject).val() != $(".choose-ebay-categories").val() && $('input', eleObject).val() != '' && $('input', eleObject).val() != 0 && $('input', eleObject).val() != undefined){
        				$('input', eleObject).val($(".choose-ebay-categories").val()).attr('data-value', 0).attr('data-key', 0);
        			}
        			else if(currentValue == $(".choose-ebay-store-categories").val() && $('input', eleObject).val() != $(".choose-ebay-store-categories").val() && $('input', eleObject).val() != '' && $('input', eleObject).val() != 0 && $('input', eleObject).val() != undefined){
        				$('input', eleObject).val($(".choose-ebay-store-categories").val()).attr('data-value', 0).attr('data-key', 0);
        			}
        			else if(currentValue == $(".choose-ebay-categories").val() && $('input', eleObject).val() == $(".choose-ebay-categories").val() || currentValue == $(".choose-ebay-store-categories").val() && $('input', eleObject).val() == $(".choose-ebay-store-categories").val()){
        				return false;
        			}
        		}
        	});
        	showWaiting(false);
        	
        }
   
        function apply() {             
            $('.getStore').unbind('click').on('click', function() {
                    showWaiting(true);
                    var time =0;
                    var num = $('body').find('input').length;
                    if(num > 0){
                            time =3000;
                    } 
                    setTimeout(function(){
                        var dataValue           = {
                                'id_site'       : $("#id-site").val(),
                                'id_packages'   : $("#id-packages").val(),
                                'id_tab'        : active_li,
                        };
                        data.push(dataValue);

                        $.ajax({
                                type            : 'POST',
                                url             : base_url + "ebay/getStoreCategory/", 
                                data            : { data : data },
                                success         : function(response) {  
                                    $("#main-store .treeRight").replaceWith(response);
                                    showWaiting(false);
                                }   
                        }); 
                 },time);
            });
        
            $('.add_category').unbind('click').on('click', function(){
                expandCollapse($(this)); 
            });
        
            $('input[rel="id_category"]').unbind('click').on('change', function(){
                getProfile($(this));
                recParCheck($(this));
                if(!just_check_all){
                    just_cal=false; 
                }
//               select_click();  
            });
            
            function recParCheck(cur){

                var par = cur.closest('ul li.tree-folder-name');

                if(!par.hasClass('tree-folder')) return;
                par = par.find('.tree-folder-header');
                par = par.find('input[rel="id_category"]').first();
                if(!par.is(':checked') && cur.is(':checked')){
                   par.trigger('click'); 
                   recParCheck(par);
                }             
            }
        
        
            $.fn.enableCheckboxRangeSelection = function() {
                var lastCheckbox = null;
                var $spec = this; 
                $spec.unbind("click");
                $spec.bind("click", function(e) { 
                    var obj  =$(this)
                    if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {

                        $spec.slice(
                            Math.min($spec.index(lastCheckbox), $spec.index(e.target)),
                            Math.max($spec.index(lastCheckbox), $spec.index(e.target)) + 1
                        ).each(function () {
                            var chk = Boolean($(this).parent().find('input').attr('checked'));
                            if(!$(this).is(lastCheckbox) && !$(this).is(obj)){
                                switch(true){
                                    case chk : $(this).parent().find('input').prop('checked',false).change();//.checkbox('uncheck');
                                        $(this).parent().parent().find('.pull-right').remove(); break;
                                    case !chk : $(this).parent().find('input').prop('checked',true).change();//.checkbox('check');
                                        getProfile($(this).parent().find($("input[type=checkbox]"))); break;
                                    default: $(this).parent().find('input').prop('checked',true).change();//.checkbox('check');
                                        getProfile($(this).parent().find($("input[type=checkbox]"))); break;
                                }
                            }
                        });
                         
                    } 
                    lastCheckbox = e.target;
                     
                });
            };

            $('.cb-checkbox   i').enableCheckboxRangeSelection();
        }
 
        function getProfile(e){

            var select;
            var ele = $(e).parents('.tree-folder-name:first');
            var ele_value = $(e).val();

            if($(e).is(':checked'))
            {
                if(ele.find('input.select-main[name="category['+ele_value+'][id_profile]"]').length === 0)
                {
                    var cloned = $('#main').find('.treeRight').clone();
                    cloned.appendTo(ele);
                    cloned.find('input.select-main').attr('name', 'category[' + $(e).val() + '][id_profile]');
                    if ( active_li === '3' || active_li === '4' ) {
                            cloned.find('input.select-main').addClass('select-store');
                            cloned.find('input.select-main').val($(".choose-ebay-store-categories").val());
                    }

                    cloned.find('.cp_down_btn').click(function(){
                        click_copy_val_sel($(this));
                    });

                     ele.find('select').attr('name', 'category[' + $(e).val() + '][id_profile]');
                     ele.find('select').attr('id', 'category' + $(e).val() + '_id_profile');

                    select = cloned.find('input.select-main');                 
                }
                else {
                    select = ele.find('input.select-main[name="category['+ele_value+'][id_profile]"]');
                    select.parents('.treeRight:first').show();
                    if ( active_li === '3' || active_li === '4' ) {
                            ele.find('input.select-main').removeClass('select-store').addClass('select-store');
                    }
                }
            }
            else
                ele.find('.treeRight:first').hide();

             //ele.find('select').select2();
            return select;
        }

//        function getProfile(e){
//
//            var select;
//            var ele = $(e).closest('ul li.tree-folder-name') ;
//            $('.tree_show').show();
//            if($(e).is(':checked'))
//            {   ele.children('.treeRight').remove();
//                if(ele.children('.treeRight').find('select.select-main').length === 0)
//                {
//                    var mainID = '#' + $('.widget-tabs li.active').attr('rel');
//                    mainID = '#main_selectbox';
//                    if ( active_li === '3' || active_li === '4' ) {
//                        mainID = '#main-store';
//                    }
//                    var cloned = $(mainID).find('.treeRight').clone(); 
//                    
//                    cloned.find('.chosen-container').remove();
//                    cloned.appendTo(ele);
//                    cloned.find('.tree_show').show();
//                    cloned.find('select').removeClass('select-main').addClass('select-main');
//                    cloned.find('select.select-main').attr('name', 'category[' + $(e).val() + '][id_profile]');
//                    if ( active_li === '3' || active_li === '4' ) {
//                            cloned.find('select.select-main').addClass('select-store');
//                            cloned.find('select.select-main').val(0);
//                    }
//
//                    cloned.find('.cp_down_btn').click(function(){
//                        click_copy_val_sel($(this));
//                    });
//                    
//
//                     ele.find('select').attr('name', 'category[' + $(e).val() + '][id_profile]');
//                     ele.find('select').attr('id', 'category' + $(e).val() + '_id_profile');
//
//                    select = cloned.find('select.select-main');                 
//                }
//                else
//                    select = ele.children('.treeRight').find('select.select-main');
//            }
//            else
//                ele.children('.treeRight').remove();

//            ele.find('select').chosen();//.select2();
//            return select;
//        }
        
    //Get All Selected Category
        function getSelectedAllCategory(ele){
            var dataValue           = {
                    'id_site'       : $("#id-site").val(),
                    'id_packages'   : $("#id-packages").val(),
                    'switch'        : 'mapping_categories',
                    'id_tab'        : active_li,
            };
            data.push(dataValue);

            $.ajax({
                    type            : 'POST',
                    url             : base_url + "ebay/setJsonAllCategory/", 
                    data            : { data : data },
                    success         : function(response) {      

                    if(response && response != "") {
                        $('#has_category').show();
                        if ( (active_li === '1' || typeof(active_li) === 'undefined') ) {
                                active_li = '1';
                                $('.widget-tabs li:first').attr('open', 'open');
                                $('#tree' + active_li).html(response);
                                $('#tree' + active_li).checkBo();
                                apply();
                                $("#main-selected select option").each(function() {                  
                                        $("#tree" + active_li).find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]').prop('checked',true).change();
                                        $input = $("#tree"  + active_li).find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]').parents('.tree-folder-name:first').find('input.select-main[rel="'+$(this).attr('rel')+'"]');
                                        $input.attr({rel: $(this).attr('rel'), 'data-key' : $(this).val(), 'data-value' : $(this).text()}).val($(this).text());
                                        if ( $input.val() === $(".choose-ebay-categories").val()) {
                                                $input.remove();
                                        }
                                        console.log($input);
                                });
                        }
                        else if ( active_li === '2' ) {
                                $('#tree' + active_li).html(response);
                                $('#tree' + active_li).checkBo();
                                apply();
                                $("#main-secondary-selected select option").each(function() {
                                        $("#tree" + active_li).find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]').prop('checked',true).change();
                                        $input = $("#tree"  + active_li).find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]').parents('.tree-folder-name:first').find('input.select-main[rel="'+$(this).attr('rel')+'"]');
                                        $input.attr({rel: $(this).attr('rel'), 'data-key' : $(this).val(), 'data-value' : $(this).text()}).val($(this).text());
                                        if ( $input.val() === $(".choose-ebay-categories").val()) {
                                                $input.remove();
                                        }
                                });
                        }
                        else if ( active_li === '3' ) {
                                $('#tree' + active_li).html(response);
                                $('#tree' + active_li).find('input.select-main').val($(".choose-ebay-store-categories").val());
                                $('#tree' + active_li).checkBo();
                                apply();
                                $("#main-store-selected select option").each(function() {
                                        $("#tree" + active_li).find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]').prop('checked',true).change();
                                        $input = $("#tree"  + active_li).find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]').parents('.tree-folder-name:first').find('input.select-main[rel="'+$(this).attr('rel')+'"]');
                                        $input.attr({rel: $(this).attr('rel'), 'data-key' : $(this).val(), 'data-value' : $(this).text()}).val($(this).text());
                                        if ( $input.val() === $(".choose-ebay-store-categories").val()) {
                                                $input.remove();
                                        }
                                        $input.addClass('select-store');
                                });
                        }
                        else if ( active_li === '4' ) {
                                $('#tree' + active_li).html(response);
                                $('#tree' + active_li).checkBo();
                                $('#tree' + active_li).find('input.select-main').val($(".choose-ebay-store-categories").val());
                                apply();
                                $("#main-secondary-store-selected select option").each(function() {
                                        $("#tree" + active_li).find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]').prop('checked',true).change();
                                        $input = $("#tree"  + active_li).find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]').parents('.tree-folder-name:first').find('input.select-main[rel="'+$(this).attr('rel')+'"]');
                                        $input.attr({rel: $(this).attr('rel'), 'data-key' : $(this).val(), 'data-value' : $(this).text()}).val($(this).text());
                                        if ( $input.val() === $(".choose-ebay-store-categories").val()) {
                                                $input.remove();
                                        }
                                        $input.addClass('select-store');
                                });
                        }
                        else {
                                $('#tree' + active_li).html(response);
                        }
                        
                    }
                    else {
                        $('#none_category').show();
                    }
                    $('#page-load').hide();           

                    $("input[type=checkbox]").each(function(index){
                        $(this).closest('ul li.tree-folder-name').find('input.select-main').attr('rel', index);
//                        $(this).checkbox();
                    });

                    select_click();   
                    apply();
                },
                error: function(response, error){             
                    console.log(response);
                    console.log(error);
                }
            });     
        }
    
        function expandCollapse(e){
            if(!e.hasClass('active')){
                 e.parent().parent().parent().find('ul.tree_child').slideDown();
                 e.addClass('active');
            }else{
                 e.parent().parent().parent().find('ul.tree_child').slideUp();
                 e.removeClass('active');
            }
        }
    
        function expandCollapseAll(e){
        }
    

    
        function select_click() {
            $('.select-main').css({ 'cursor' : 'pointer'});
            $('.select-main').unbind('click');
            $('.select-main').click(function() {
                    first_click = undefined;
                    var $selected            = $(this);
                    var $checkbox_rel           = $selected.attr('name').slice(9, 10)
                    var $tree_name           = $selected.closest('ul li.tree-folder-name')//.parent().parent().parent();//$selected.parents('li');
                    if ( typeof(first_click) === 'undefined' ) {
                            first_click         = {
                                    value       : $selected.val(),
                                    rel         : $selected.attr('rel'),
                            };
                            old_select          = $selected;
                    }
                    if ( $selected.hasClass('select-store') ) {
                        global_cloned      = $('#main-store').find('.treeRight').clone();
                        
                    }
                    else {
                        global_cloned      = $('#main_selectbox').find('.treeRight').clone();
                       
                    }
                    global_cloned.show();
                    $selected.parents('.treeRight').replaceWith(global_cloned);

                    global_cloned.find('.tree_show').show();
                    global_cloned.find('.chosen-container').remove();
                    var cloned_sel = global_cloned.find('select');
                    cloned_sel.val('').chosen().change(function() {
                            var selected_value  = $.trim($(cloned_sel).find('option:selected').val());
                            var selected_name   = $.trim($(cloned_sel).find('option:selected').text());
                            selected_name       = selected_name.split(' -> ');
                            
                            cloned_sel.chosen('destroy');
                            $tree_name.find('select').replaceWith(old_select);
                            if ( selected_name[0] !== $(".choose-ebay-categories").val() || selected_name[0] !== $(".choose-ebay-store-categories").val()) {
                                    $tree_name.find('input.select-main[rel="'+first_click.rel+'"]').val(selected_name[selected_name.length - 1]).attr('data-value', selected_name[selected_name.length - 1]).attr('data-key', selected_value);
                            }
                            else {
                                    $tree_name.find('input.select-main[rel="'+first_click.rel+'"]').val($(".choose-ebay-categories").val()).attr('data-value', 0).attr('data-key', 0);
//                                    $tree_name.find('input[name="category['+$checkbox_rel+'][id_category]"]').prop('checked',false).change();
                            }
                            list_name = undefined;
                            first_click = undefined;
                            old_select = undefined;
                            select_click();
                    });
                    
//                    global_cloned.find('.chosen-container').mouseleave(function(){
//                            cloned_sel.chosen('destroy');
//                            global_cloned.find('select').replaceWith(old_select);
//                            list_name = undefined;
//                            first_click = undefined;
//                            old_select = undefined;
//                            select_click();
//                    });
                    setTimeout(function(){
                        global_cloned.find('.chosen-container').trigger('mousedown')},1);
                        $tree_name.find('.cp_down_btn').click(function(){
                        click_copy_val_sel($(this));
                    });
            });
        }
        
        
        $(document).mouseup(function (e) {
                var current = e.target;
                if ($('.chosen-container').find(e.target).length === 0 && !jQuery.isEmptyObject(global_cloned) ) {
                        var cloned_sel = global_cloned.find('select');
                        cloned_sel.chosen('destroy');
                        global_cloned.find('select').replaceWith(old_select);
                        list_name = undefined;
                        first_click = undefined;
                        old_select = undefined;
                        global_cloned = undefined;
                        select_click();
                }
        });
    
        $('#form-submit').on('submit', function(e) {
                $('html, body').animate({ scrollTop: 0 }, 0);
                e.preventDefault();
                data                        = [];
                wizard_data                 = [];
                dataname                    = [];
                for ( var i = 1; i < 5; i++ ) {
                        dataname['key' + i]                = [];
                        $("#tree" + i).find("input[rel='id_category']").each(function() {
                                if ( $(this).is(":checked") ) {
                                        var $line = $(this).closest('ul li.tree-folder-name');
                                        var $folder_input               = $line.find("input.select-main")//.parents(".tree-folder-name").find("input.select-main");
                                        var $folder                     = $line;//$(this).parents(".tree-folder-name");
                                        var category_status             = $(this).prop("checked");
                                        var category_id                 = $line.children('.tree_item').find('input[rel="id_category"]').val();
                                        var category_name               = $line.children('.tree_item').find('.lbl').text();
                                        var category_ebay_id            = $line.children('.treeRight').find('input.select-main').attr('data-key');
                                        var category_ebay_list          = $line.children('.treeRight').find('input.select-main').attr('data-value');

                                        if ( typeof(category_ebay_list) === 'undefined' || category_ebay_list === '0' ) {
                                                return;
                                        }
                                        else {
                                                var category_listing_duration   = '';
                                                var template_selected           = '';
                                                /*data_value              = {
                                                        'tab_name'              : i,//$.trim($('a[href="#tab-'+ i +'"]').text()),
                                                        //'category_status'       : category_status,
                                                        'category_id'           : category_id,
                                                        //'category_name'         : $.trim(category_name),
                                                        'category_ebay_id'      : category_ebay_id,
                                                        //'category_ebay_name'    : $.trim(category_ebay_list),
                                                        //'listing_duration'      : $.trim(category_listing_duration),
                                                        //'template_selected'     : $.trim(template_selected),
                                                        //'id_site'               : $("#id-site").val(),
                                                        'id_packages'           : $("#id-packages").val(),
                                                };*/
                                                if(category_ebay_id > 0){
                                                    data_value = category_id + ':' + category_ebay_id;
                                                    dataname['key' + i].push(data_value);
                                                }
                                        }
                                }
                        });
                        data.push(dataname['key' + i]);
                }
                
                
                //if ($.isEmptyObject(data)) {
                        data_value              = {
                                'id_site'               : $("#id-site").val(),
                                'id_packages'           : $("#id-packages").val(),
                        };
                        data.push(data_value);
                //}
                
                var data_selected = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'id_popup'      : $("#popup-step").val(),
                };
                wizard_data.push(data_selected);
                
                ajax_save(e);
        });