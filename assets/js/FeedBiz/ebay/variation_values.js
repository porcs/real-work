        var data                            = new Array();
        var wizard_data                     = new Array();
        var insert_variation;
        var previous;
        
        var app = angular.module('ebayApp', [])
        .config(['$interpolateProvider', function ($interpolateProvider) {
            $interpolateProvider.startSymbol('[[');
            $interpolateProvider.endSymbol(']]');
        }])
        .controller('VariationValue', function ($scope, $parse) {
            document.querySelector('#attribute-mapping').classList.remove('hide');
            $scope.defaultVariationString = [];
            $scope.mappingVariationString = [];
            $scope.mappingData = [];
            $scope.showDefault = [];
            $scope.pushMapping = {label: 'mapping', decription: document.querySelector('#push_mapping').value, inverse: 'default'};
            $scope.pushDefault = {label: 'default', decription: document.querySelector('#push_default').value, inverse: 'mapping'};
            $scope.appData = JSON.parse(JSON.stringify(app_data));
            
            $scope.isArray = function(arr) {
                return angular.isArray(arr);
            }
            
            $scope.addVariationValue = function (id, value) {
                if ( value.attribute_value !== null )
                    $scope.mappingVariationString[value.$$hashKey] = value.attribute_value;
                else 
                    $scope.mappingVariationString[value.$$hashKey] = value.name;
            }
            
            $scope.isObject = function(obj) {
                return angular.isObject(arr);
            }
            
            $scope.foreachVariation = function(id) {
                var object = {};
                angular.forEach($scope.appData[id].attribute, function (value, index) {
                    if ( typeof(object.string) === 'undefined' )
                        object.string = value.name;
                    else 
                        object.string += ',' + value.name;
                });
                $scope.defaultVariationString[id] = object;
            }

            $scope.setVariationDefault = function (id) {
                var dataPush = $scope.pushDefault;
                $scope.foreachVariation(id);
                $scope.mappingData[id] = dataPush;
            }

            $scope.setVariationMapping = function (id) {
                var dataPush = $scope.pushMapping;
                $scope.mappingData[id] = dataPush;
            }

            $scope.switchVarianOption = function (id, option) {
                $scope.showDefault[id] = !$scope.showDefault[id];
                ( option === 'default') ? $scope.setVariationDefault(id) : $scope.setVariationMapping(id);
            }
            
            $scope.checkMapping = function (id, attributes) {
                $scope.showDefault[id] = true;
                var dataPush = $scope.pushDefault;
                $scope.foreachVariation(id);
                
                angular.forEach(attributes, function (value, index) {
                    if ( value.attribute_value !== null ) {
                        $scope.showDefault[id] = false;
                        dataPush = $scope.pushMapping;
                    }
                });
                
                $scope.mappingData[id] = dataPush;
            }

        });
        
        
        
        $("#form-submit").on('submit', function(e) {
                e.preventDefault();
                data                                        = [];

                $('html, body').animate({ scrollTop: 0 }, 0);

                $('.attribute-row').each(function() {
                        var $group                                  = $(this).attr('data-group');
                        var ebay_value                              = $(this).find('input.ebay-row').val();
                        var visibility                              = $(this).is(':visible');
                        var mode                                    = 0;

                        if ( ebay_value !== '' && visibility) {
                                var data_value                               = {
                                        'id_profile'                        : $('.id_profile').val(),
                                        'id_attribute_group'                : $group,
                                        'id_attribute'                      : $(this).find('input.fixs-row').attr('data-attrid'),
                                        'name_attribute'                    : $(this).find('input.fixs-row').val(),
                                        'name_attribute_ebay'               : ebay_value,
                                        'mode'                              : 0,
                                        'tab'                               : 1,
                                        'id_site'                           : $("#id-site").val(),
                                        'id_packages'                       : $("#id-packages").val(),
                                        'step'                              : $("#popup-step").val(),
                                };
                                data.push(data_value);
                        }
                });

                if ($.isEmptyObject(data)) {
                        var data_value              = {
                                'id_site'               : $("#id-site").val(),
                                'id_packages'           : $("#id-packages").val(),
                        };
                        data.push(data_value);
                }

                var data_selected = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'id_popup'      : $("#popup-step").val(),
                };
                wizard_data.push(data_selected);

                ajax_save(e);
        });
