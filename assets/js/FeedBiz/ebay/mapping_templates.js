        var just_cal = false;
        var just_check_all = false;
        var old_select;
        var first_click;
        var data                            = new Array();
        var cloned                          = $("div.close").clone();
        var page                            = $("#current-page").val();
    
        $('form').on('reset', function(e) {
            setTimeout(function() {
                $('.checkbox-custom > input[type=checkbox]').each(function() {
                    if ($(this).is(':checked') == false) {
                        $(this).prop('checked', false);
                        $(this).parent().parent().find('.treeRight').remove();
                        $(this).checkbox('uncheck');
                    }
                });
            });
        });
    
        $(".checkedAll").unbind('click').click(function () {
                showWaiting(true);
                var time =0;
                var num = $('body').find('input').length;
                if(num > 2){
                        time =200;
                } 
                setTimeout(function(){
                        $("input[type=checkbox]").each(function(index) {
                                if(!$(this).prop( "checked" )){
                                        $(this).prop('checked', true).change();
                                        if(!just_check_all){
                                            just_cal=false;
                                            just_check_all = true;
                                        }
                                        $(this).parents('.tree-folder-name').find('input.select-main').attr('rel', index);
                                }
                        });
                        showWaiting(false);
                },time);
        });
        
        $(".uncheckedAll").unbind('click').click(function () {
            showWaiting(true);
            var time =0;
            var num = $('body').find('input').length;
            if(num > 2){
                time =200;
            } 
            
            setTimeout(function() {
                    $("input[type=checkbox]").each(function(){
                        $(this).prop('checked', false).change();
                        $(this).parent().parent().find('.treeRight').remove();
                         
                    });
                    showWaiting(false);
             },time);
        });

        getSelectedAllCategory();
        //apply();
   
        
        function init_rel_all_row() {
            var i=1;
            if(just_cal) return;
            just_cal = true;
            $('.cp_down_btn').each(function(){
                if($(this).parents('#main').length>0)return;
                var row = $(this).parents('.tree-folder-header');
                if(row.length == 0)return;
                row.attr('rel',i); 
                i++;
            });
        }
                
        function click_copy_val_sel123(obj){
            showWaiting(true);
            var time =0;
            var num = $('body').find('input').length;
            if(num > 200){
                time =200;
            } 

                setTimeout(function(){
                    init_rel_all_row();
                    var p_row =  obj.parents('.tree-folder-header');
                    var cur_pos = p_row.attr('rel');
                    var cur_sel_val = p_row.find('select').val(); 
                    $('body').find('.tree-folder-header').each(function(){
                        var rel = $(this).attr('rel');

                        if(rel*1 > cur_pos*1){
                            $(this).find('select').val(cur_sel_val).trigger("change");
                        }
                    });
                    showWaiting(false);
                },time);
        }
        function click_copy_val_sel(obj){
        	showWaiting(true);
        	var currentTree = $('#tree1');
        	var currentLeaf = $(obj).closest('.treeRight');
        	var currentLeafOffset = $(currentLeaf).offset();
        	var currentValue = $('select', currentLeaf).val();
        	$('.treeRight', currentTree).each(function(eleIndex, eleObject){        	
        		if(currentLeafOffset.top < $(eleObject).offset().top){
        			//case blank
        			if(currentValue == 0 || currentValue == '' || currentValue == undefined){
        				$('select', eleObject).val(currentValue).trigger("chosen:updated");
        			}//case value
        			else if($('select', eleObject).val() == 0 || $('select', eleObject).val() == '' || $('select', eleObject).val() == undefined){
        				$('select', eleObject).val(currentValue).trigger("chosen:updated");
        			}
        		}
        	});
        	showWaiting(false);
        	
        }
   
        function apply() {
         
//            $('.collapseAll').unbind('click').on('click', function() {
//                showWaiting(true);
//                var time =0;
//                var num = $('body').find('input').length;
//                if(num > 2){
//                 time =200;
//                } 
//                setTimeout(function(){
//                        $('input[rel="id_category"]').each(function(){
//                            if($(this).parent().parent().parent().find('.icon-minus').length > 0){
//                                $(this).parent().parent().parent().parent().find('.tree-folder-content').hide();
//                                $(this).parent().parent().parent().find('.icon-minus').removeClass('icon-minus').addClass('icon-plus');
//                            }
//                        }); 
//                        showWaiting(false);
//                 },time);
//            });
//        
//            $('.expandAll').unbind('click').on('click', function(){
//                showWaiting(true);
//                var time =0;
//                var num = $('body').find('input').length;
//                if(num > 2){
//                 time =200;
//                } 
//                setTimeout(function(){
//                        $('.add_category').each(function(){
//                            $(this).parent().parent().parent().parent().find('.tree-folder-content').show();
//                            $(this).parent().parent().parent().find('.icon-plus').removeClass('icon-plus').addClass('icon-minus');
//                        }); 
//                showWaiting(false);
//                 },time);
//            });
//        
            $('.add_category').unbind('click').on('click', function(){
                expandCollapse($(this)); 
            });
        
            $('input[rel="id_category"]').unbind('click').on('change', function(){
                
                getProfile($(this));
                recParCheck($(this));
                $(this).parents('.tree-folder-name:first').find('select.select-main').chosen('destroy');//.select2('destroy');
                $(this).parents('.tree-folder-name:first').find('select.select-main').chosen('destroy');//.select2();
                if(!just_check_all){
                    just_cal=false; 
                    //$('select.select-main').select2();
                }
//                $(this).parent().parent().parent().find('select').select2({
//                       placeholder: $(".select-a-option").val(),
//                       allowClear: true
//                   });
            });
            
            function recParCheck(cur){

                var par = cur.parent().parent().parent().parent().parent().parent();

                if(!par.hasClass('tree-folder')) return;
                par = par.find('.tree-folder-header');
                par = par.find('input[rel="id_category"]').first();
                if(!par.is(':checked') && cur.is(':checked')){
                   par.trigger('click'); 
                   recParCheck(par);
                }             
            }
        
        
            $.fn.enableCheckboxRangeSelection = function() {
                var lastCheckbox = null;
                var $spec = this;

                $spec.unbind("click");
                $spec.bind("click", function(e) {
                    var obj  =$(this)
                    if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {
                         
                        $spec.slice(
                            Math.min($spec.index(lastCheckbox), $spec.index(e.target)),
                            Math.max($spec.index(lastCheckbox), $spec.index(e.target)) + 1
                        ).each(function () {
                            var chk = Boolean($(this).parent().find('input').attr('checked'));
                            if(!$(this).is(lastCheckbox) && !$(this).is(obj)){
                                switch(true){
                                    case chk : $(this).parent().find('input').prop('checked',false).change();//.checkbox('uncheck');
                                        $(this).parent().parent().find('.pull-right').remove(); break;
                                    case !chk : $(this).parent().find('input').prop('checked',true).change();//.checkbox('check');
                                        getProfile($(this).parent().find($("input[type=checkbox]"))); break;
                                    default: $(this).parent().find('input').prop('checked',true).change();//.checkbox('check');
                                        getProfile($(this).parent().find($("input[type=checkbox]"))); break;
                                }
                            }
                        });
                         
                    } 
                    lastCheckbox = e.target;
                });
            };

            $('.cb-checkbox i').enableCheckboxRangeSelection();
        }
 

        function getProfile(e){

            var select;
            var ele = $(e).closest('ul li.tree-folder-name') ;
//            ele.find('select').parent().chosen('destroy');
            if($(e).is(':checked'))
            {
                ele.find('select').chosen('destroy');//.select2();
                if(ele.find('input.select-main').length === 0)
                {
                    var cloned = $('#main').find('.treeRight').clone(); 
                    cloned.find('.chosen-container').remove();
                    cloned.appendTo(ele);
                    cloned.find('.tree_show').show();
                    cloned.find('input.select-main').attr('name', 'category[' + $(e).val() + '][id_profile]');

                    cloned.find('.cp_down_btn').click(function(){
                        click_copy_val_sel($(this));
                    });
                    

                     ele.find('select').attr('name', 'category[' + $(e).val() + '][id_profile]');
                     ele.find('select').attr('id', 'category' + $(e).val() + '_id_profile');

                    select = cloned.find('input.select-main');                 
                }
                else
                    select = ele.find('input.select-main');
            }
            else
                ele.children('.treeRight').remove();

            ele.find('select').chosen();//.select2();
            return select;
        }
        
    //Get All Selected Category
        function getSelectedAllCategory(ele){
            var dataValue           = {
                    'id_site'       : $("#id-site").val(),
                    'id_packages'   : $("#id-packages").val(),
            };
            data.push(dataValue);

            $.ajax({
                    type            : 'POST',
                    url             : base_url + "ebay/setJsonAllCategory/", 
                    data            : { data : data },
                    success         : function(response) {               
                    if(response && response != "") {
                        $('#has_category').show();
                        $('#tree1').html(response).checkBo();
                        apply();
                        if ( $("#main-selected").length !== 0 ) {
                                $("#main-selected select option").each(function() {
                                        $this = $("#tree1").find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]');
                                        $this.parents('label').trigger('click');
                                        
//                                        getProfile($this);
                                        $select = $("#tree1").find('input[rel="id_category"]').filter('[value="'+$(this).attr('rel')+'"]').parents('.tree-folder-name:first').find('select.select-main option').filter('[value="'+$(this).val()+'"]').first();
                                        $select.parent().chosen('destroy');//.select2('destroy');
                                        $select.attr({rel: $(this).attr('rel'), selected: "selected"}).val($(this).text());
//                                        $select.parent().chosen('destroy');//.select2('destroy');
                                        $select.parent().chosen();//.select2();
                                });
                        }
                    }
                    else {
                        $('#none_category').show();
                    }

                    $('#page-loaded').show();
                    $('#page-load').hide();           

                    $("input[type=checkbox]").each(function(index){
                        $(this).parents('.tree-folder-name').find('input.select-main').attr('rel', index);
//                        $(this).checkbox();
                    });
 
//                    apply();
                },
                error: function(response, error){             
                    console.log(response);
                    console.log(error);
                }
            });     
        }
    
        function expandCollapse(e){

            var content = e.parent().parent().find('.tree-folder-content');

            if(e.hasClass('icon-plus'))
            {
                if(content.children().length > 1){
                    content.show();
                    content.find('.icon-spinner').remove();
                }
                e.removeClass('icon-plus').addClass('icon-minus');
            }
            else
            {
                content.hide();
                e.removeClass('icon-minus').addClass('icon-plus');
            }
        }
    
        function expandCollapseAll(e){
            var content = e.parent().parent().find('.tree-folder-content');
            if(content.children().length > 1){
                content.show();                
            }else{
                getSelectedAllCategory(e);
            }
        }
    
        $('#form-submit').on('submit', function(e) {
            $('html, body').animate({ scrollTop: 0 }, 0);
            e.preventDefault();
            data                    = [];
            $("input[rel='id_category']").each(function() {
                    if ( $(this).is(":checked") ) {
                            var $folder_input               = $(this).parents(".tree-folder-name").first().children('.treeRight').find("select.select-main option:selected");
                            var $folder                     = $(this).parents(".tree-folder-name");
                            var category_status             = $(this).prop("checked");
                            var category_id                 = $(this).val();
                            var category_name               = $folder.find('.tree_item:first .lbl').text();
                            var template_id                 = $folder_input.val();
                            var template_selected           = $.trim($folder_input.text());
                            if ( typeof(template_selected) === 'undefined' || $.trim(template_selected) === $('.default-template').val() ) {
                                    return;
                            }
                            else {
                                    data_value              = {
                                            'category_status'       : category_status,
                                            'category_id'           : category_id,
                                            'category_name'         : $.trim(category_name),
                                            'template_selected'     : $.trim(template_selected),
                                            'id_site'               : $("#id-site").val(),
                                            'id_packages'           : $("#id-packages").val(),
                                    };
                                    data.push(data_value);
                            }
                    }
            });
            
            if ($.isEmptyObject(data)) {
                    data_value              = {
                            'id_site'               : $("#id-site").val(),
                            'id_packages'           : $("#id-packages").val(),
                    };
                    data.push(data_value);
            }
            
            ajax_save(e);
        });
