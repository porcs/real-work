       
        var data                            = new Array();
        var wizard_data                     = new Array();
        var module                          = 1;
        
        $("#form-field-select li").click(function() {
                $("#form-field-select li").removeClass('active').filter(this).addClass('active');
                $("#id-site").val($(this).attr('value'));
                $("#id-packages").val($(this).attr('data-key'));
        });

        $("#form-submit").on('submit', function(e) {
                data                = [];
                wizard_data         = [];
                var data_selected   = {
                    'id_site'       : $("#id-site").val(),
                    'id_packages'   : $("#id-packages").val(),
                    'id_popup'      : $("#popup-step").val(),
                };

                data.push(data_selected);
                wizard_data.push(data_selected);

                ajax_save(e);
        });
