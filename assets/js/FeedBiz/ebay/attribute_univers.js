        (function($) {
            $.fn.enableCheckboxRangeSelection = function() {
                    var lastCheckbox = null;
                    var $spec = this;
                    var lastType = null;

                    $spec.unbind("click");
                    $spec.bind("click", function(e) {
                            lastType = $(lastCheckbox).find('input[type=checkbox]').is(':checked');// e.currentTarget.checked;
                            var $target = this;//e.target
                            if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {
                                    
                                    var checked = $spec.slice(
                                            Math.min($spec.index(lastCheckbox), $spec.index($target)),
                                            Math.max($spec.index(lastCheckbox), $spec.index($target)) + 1
                                    ); 
                                     
                                    checked.find('input[type=checkbox]').each(function() { 
                                        if($(this).is($($target).find('input[type=checkbox]')))return;
                                        $(this).prop({ 'checked' : lastType}).change();
                                    });
                                    lastCheckbox = null; 
                            }
                            else {
                                    lastCheckbox = $target;
                            }
                    });
            };
        })(jQuery);

        $('#univers_list li label').enableCheckboxRangeSelection();

        $('#unCheckAll').on('click',function(){
            $("#univers_list input.status:checked").each(function() {
                $(this).prop('checked',false)
            });
        })

        $('#checkAll').on('click',function(){
            $("#univers_list input.status").each(function() {
                $(this).prop('checked',true)
            });
        })
        
        var data                            = new Array();
        var wizard_data                     = new Array();

        $("#form-submit").on('submit', function(e) {
                e.preventDefault();
                data                        = [];
                wizard_data                 = [];
                $("#univers_list input.status:checked").each(function() {
                        var data_selected = {
                            'attribute_group_id'    : $(this).val(),
                            'attribute_name'        : $(this).attr('title'),
                            'id_site'               : $("#id-site").val(),
                            'id_packages'           : $("#id-packages").val(),
                        };

                        data.push(data_selected);
                });
                
                if ($.isEmptyObject(data)) {
                        data_value              = {
                                'id_site'               : $("#id-site").val(),
                                'id_packages'           : $("#id-packages").val(),
                        };
                        data.push(data_value);
                }
                
                var data_selected = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'id_popup'      : $("#popup-step").val(),
                };
                wizard_data.push(data_selected);

                ajax_save(e);
        });
