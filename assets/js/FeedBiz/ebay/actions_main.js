var cloned = $("div.close").clone();
var proc_count  = 0;
var proc_error_count  = 0;
var proc_status_count  = 0;
var proc_running = [];
var from_wizard_data = [];
$(document).ready(function() {    
        console.log('start action');
        function localeMessage(message){
                console.log('localeMessage::'+message);
                if(message == undefined)
                        return 'PHP code error.';
                var findID = 'input[name="' + message.trim().replace(/[^\w\s]/gi, '').replace(/\s+/g, '_').toLowerCase() + '"]';
                var out = $(findID).length == 1 ? $(findID).val() : message;

                return out;
        }
console.log('start action main');
        function checkProc(type) {
                  type = type + "_site_" + $("#id-site").val();
                if ( typeof(proc_running[type]) !== 'undefined' && proc_running[type] === 1 ) {
                        return;
                }
                else if ( typeof(proc_running[type]) === 'undefined' ) { proc_running[type] = 1; }
                
                $.ajax ({
                        type        : "POST",
                        url         : base_url + "users/ajax_get_sig_process/" + type,
                        success     : function(result) {
                                    var currentPanel = $('#send-offers').parent();
                                    if(type == 'export_products' + "_site_" + $("#id-site").val() ){
                                            currentPanel = $('#send-products').parent();
                                    }
                                    if(type == 'delete_products' + "_site_" + $("#id-site").val() ){
                                            currentPanel = $('#delete-products').parent();
                                    }
                        			
                                    console.log($.trim(result));
                                    var proc = jQuery.parseJSON($.trim(result));
                                    if ( proc.found === false && typeof(proc) !== 'undefined' ) {
                                            console.log(proc_count);
                                            proc_count++;
                                            console.log(proc.data, proc_running);
                                            if ( proc_count > 5 ) {
                                                    proc_display = 'Stop process : connection have a long time.'
                                                    console.log(localeMessage(proc_display));
                                                    proc_running[type] = 0;
                                                    return;
                                            }
                                            setTimeout (function () { proc_running[type] = 0; checkProc(type) }, 4000);
                                            return;
                                    }
                                    else if ( typeof(proc.data) !== 'undefined' && ( proc.data.status === 1 || proc.data.status === 9)  && ( proc.data.comments === 'compress' || proc.data.comments === 'upload' ) || ( proc.data.status === 1 && proc.data.comments === 'download') ) {
                                            console.log(proc.data, proc_running);
                                            proc_display = proc.data.msg;

                                            	$('div[name="message_error"]', currentPanel).hide();
                                            	notification_alert(localeMessage(proc_display));
                                                                                    	
                                            proc_running[type] = 1;
                                            setTimeout (function () { proc_running[type] = 0; checkProc(type) }, 4000);
                                            return;
                                    }
                                    else if ( typeof(proc.data) !== 'undefined' && proc.data.status === 9 && proc.data.comments === 'download' ) {
                                            console.log(proc.data, proc_running);
                                            proc_display = $(".update-"+ page +"-send-mail").val();
                                            proc_display = proc_display == undefined ? $(".update-"+ page.replace('_', '-') +"-send-mail").val() : proc_display;
                                            
                                            if ( typeof(module) !== 'undefined' && module === 1 ) {
                                                    from_wizard_data = [];
                                                    var data_selected = {
                                                        'id_site'       : $("#id-site").val(),
                                                        'id_packages'   : $("#id-packages").val(),
                                                        'id_popup'      : $("#popup-step").val(),
                                                        'from_wizard'   : 1,
                                                        'from_success'  : 1,
                                                    };

                                                    from_wizard_data.push(data_selected);
                                                    export_from_wizard();
                                            }
                                            else {
                                                    from_wizard_data = [];
                                                    var data_selected = {
                                                        'id_site'       : $("#id-site").val(),
                                                        'id_packages'   : $("#id-packages").val(),
                                                        'id_popup'      : 1,
                                                        'from_wizard'   : 0,
                                                        'from_success'  : 0,
                                                    };

                                                    from_wizard_data.push(data_selected);
                                                    export_from_wizard();
                                            }
	                                        	$('div[name="message_error"]', currentPanel).hide();
	                                        	notification_alert(localeMessage(proc_display));
                                            
                                            proc_running[type] = 0;
                                            return;
                                    }
                                    else if ( typeof(proc.data) !== 'undefined' || typeof(proc.data.err_msg) !== 'undefined' || proc.data.status === 4 || proc.data.status === 8 || proc.data.err_msg !== '' ) {
                                            console.log(proc.data, proc_running);
                                            proc_display = '';
                                            if ( proc.data.status === 4 ) {
                                                    proc_display = $(".update-"+ page +"-export-fail").val();
                                                    proc_display = proc_display == undefined ? $(".update-"+ page.replace('_', '-') +"-export-fail").val() : proc_display;
                                            }
                                            else if ( proc.data.status === 8 ) {
                                                    proc_display = $(".update-"+ page +"-error").val();
                                                    proc_display = proc_display == undefined ? $(".update-"+ page.replace('_', '-') +"-error").val() : proc_display;
                                            }
                                            if ( proc.data.err_msg !== '' ) {
                                                    proc_display = proc.data.err_msg;
                                            }
                                            if(proc_display != ''){
	                                            	$('div[name="message_success"]', currentPanel).hide();
                                                        notification_alert(localeMessage(proc_display), 'bad');
                                                
		                                        proc_error_count++;
		                                        console.log(proc.data, proc_running);
		                                        if ( proc_error_count > 3 ) {
		                                                proc_display = 'Stop process : connection have a long time.'
		                                                console.log(localeMessage(proc_display));
		                                                proc_running[type] = 0;
		                                                return;
		                                        }
		                                        setTimeout (function () { proc_running[type] = 0; checkProc(type) }, 4000);
                                            }
                                            return;
                                    }
                        }
                });
        }
        
        function get_orders(url, target) {
                var $obj = $(target).parents('.import-order-button');
                $obj.find('.status').show();
                $('div[name=success]').hide();
                $obj.find('div[name="message_success"]').hide();
                $obj.find('div[name="message_error"]').hide();
                var data = [];
                var data_selected = {
                    'id_site'       : $("#id-site").val(),
                    'id_packages'   : $("#id-packages").val(),
                    'order_status'  : typeof($('select.order-status option:selected').val()) !== 'undefined' ? $('select.order-status option:selected').val() : 'Completed',
                };

                data.push(data_selected);

                $.ajax ({
                        type        : "POST",
                        url         : base_url + "ebay/" + url,
                        data        : {data : data},
                        timeout     : 0,
                        error       : function(jqXHR, textStatus, errorThrown) {
                                     console.log(jqXHR, textStatus, errorThrown);
                                     if(jqXHR.status == 404){
                                            $obj.find('div[name="message_error"]').show().find('span').html('').append($(".update-actions-error").val());
                                            $obj.find('.status').hide();
                                     }
                        },
                        success     : function(result) {
                                    $obj.find('.status').hide();
                                    console.log($.trim(result));
                                    if ( $.trim(result) != 'Failured' ) {
                                            $obj.find('div[name="message_success"]').show().find('span').html('').append($(".update-actions_orders-success").val());
                                            
                                            if(url == 'get_orders_debug'){
                                            	load_debug_orders_xml();
                                            }
                                    }
                                    else {
                                            $obj.find('div[name="message_error"]').show().find('span').html('').append($(".update-actions_orders-fail").val());
                                    }
                        }
                });
        }
        
        
        function load_debug_orders_xml(){
        	if($('#debug-orders').length > 0){
            	$.ajax({
            		dataType : "JSON",
                	url : base_url + "ebay/get_orders_debug_xml/"+$("#id-packages").val(),
                	success : function(data){
                		$('#table-debug-orders').remove();
                		var table = $('<table id="table-debug-orders" class="table"></table>').insertAfter($('#debug-orders').closest('.col-md-6'));
                		var thead = $('<thead></thead>').appendTo(table)
                		var tr = $('<tr></tr>');
                		$('<th>'+$('.hdd-id_order').val()+'</th>').appendTo(tr);
                		$('<th>'+$('.hdd-profile_status').val()+'</th>').appendTo(tr);
                		$('<th>'+$('.hdd-created_date').val()+'</th>').appendTo(tr);
                		$('<th>'+$('.hdd-item_id_only').val()+'</th>').appendTo(tr);
                		$('<th>'+$('.hdd-sku').val()+'</th>').appendTo(tr);
                		$('<th>'+$('.hdd-product').val()+'</th>').appendTo(tr);
                		$('<th>'+$('.hdd-site').val()+'</th>').appendTo(tr);
                		$(tr).appendTo(thead);

                		var tbody = $('<tbody></tbody>').appendTo(table)
                		$.each(data, function(orderInex, order){
                			$.each(order.Items, function(itemIndex, item){
                				var foundOrder = $('tr[order="'+order.OrderID+'"]').length > 0;
                        		var tr = $('<tr order="'+order.OrderID+'"></tr>');
                        		$('<td>'+(foundOrder ? '' : order.OrderID)+'</td>').appendTo(tr);
                        		$('<td>'+(foundOrder ? '' : order.OrderStatus)+'</td>').appendTo(tr);
                        		$('<td>'+(foundOrder ? '' : order.CreatedTime)+'</td>').appendTo(tr);
                        		$('<td>'+item.ItemID+'</td>').appendTo(tr);
                        		$('<td>'+item.SKU+'</td>').appendTo(tr);
                        		$('<td>'+item.Product+'</td>').appendTo(tr);
                        		$('<td>'+item.Site+'</td>').appendTo(tr);
                        		$(tr).appendTo(tbody);
                			});
                		});
                	}
                });
        	}
        }
        
        $('#download-orders').on('click', function(e){
                e.preventDefault();
                get_orders('get_orders', this);
        });
        
        $('#modifier-orders').on('click', function(e){
                e.preventDefault();
                get_orders('get_modifier_orders', this);
        });
        
        $('#debug-orders').on('click', function(e){
                e.preventDefault();
                get_orders('get_orders_debug', this);
        });
        
//        checkProc('export_products');
        $(".send-products").on("click", function(e) {
                e.preventDefault();
                var $obj = $(this).parents('.export-product-button');
                var link = 'products';
                var method = 'compress';
                $obj.find('.status').show();
                $obj.find('div[name="message_success"]').hide();
                $obj.find('div[name="message_error"]').hide();
                if ( typeof(module) !== 'undefined' && module === 1 ) {
                        link = 'wizard';
                }
                if ( $(this).attr('id') === 'verify-product' ) {
                        method = 'verify_product';
                }
                else if ( $(this).attr('id') === 'revise-product' ) {
                        method = 'revise_product';
                }
                
                $.ajax({
                        type        : "POST",
                        url         : base_url + "tasks/run_export_ebay_by_node",
                        data        : { siteid : $("#id-site").val(), method : method, link : link},
                        timeout     : 0,
                        error       : function(jqXHR, textStatus, errorThrown) {
                                     console.log(jqXHR, textStatus, errorThrown);
                                     if(jqXHR.status == 404){
                                            $obj.find('div[name="message_error"]').show().find('span').html('').append($(".update-actions-products-export-fail").val());
                                            $obj.find('.status').hide();
                                     }
                                     checkProc('export_products');
                        },
                        success     : function(result) {
                                    $obj.find('.status').hide();
                                    console.log($.trim(result));
                                    if ( $.trim(result) == 'Success' ) {
                                            $obj.find('div[name="message_success"]').show().find('span').html('').append($(".update-actions-products-export-success").val());
                                            
                                            if ( typeof(module) !== 'undefined' && module === 1 ) {
	                                                from_wizard_data = [];
	                                                var data_selected = {
	                                                    'id_site'       : $("#id-site").val(),
	                                                    'id_packages'   : $("#id-packages").val(),
	                                                    'id_popup'      : $("#popup-step").val(),
	                                                    'from_wizard'   : 1,
	                                                    'from_success'  : 1,
	                                                };
	
	                                                from_wizard_data.push(data_selected);
	                                                export_from_wizard();
	                                        }
                                    }
                                    else{
                                        checkProc('export_products');
                                    }
                        },
                });
        });
        
        $("#compress-file").on("click", function(e) {
                e.preventDefault();
                var $obj = $(this).parents('.compress-product-button');
                var link = $('#current-page').val().split('_').slice(1).toString();
                $obj.find('.status').show();
                $obj.find('div[name="message_success"]').hide();
                $obj.find('div[name="message_error"]').hide();
                if ( typeof(module) !== 'undefined' && module === 1 ) {
                        link = 'wizard';
                }
                $.ajax({
                        type        : "POST",
                        url         : base_url + "tasks/run_export_ebay_by_node",
                        data        : { siteid : $("#id-site").val(), method : "compress_file", link : link},
                        timeout     : 0,
                        error       : function(jqXHR, textStatus, errorThrown) {
                                     console.log(jqXHR, textStatus, errorThrown);
                                     $obj.find('div[name="message_error"]').show().find('span').html('').append($(".actions_compress_file_products_fail").val());
                                     $obj.find('.status').hide();
                        },
                        success     : function(result) {
                                    $obj.find('.status').hide();
                                    console.log($.trim(result));
                                    if ( $.trim(result) == 'Success' ) {
                                            $obj.find('div[name="message_success"]').show().find('span').html('').append($(".actions_compress_file_products_success").val());
                                    }
                                    else {
                                        $obj.find('div[name="message_error"]').show().find('span').html('').append($(".actions_compress_file_products_fail").val());
                                 }
                        },
                });
        });

//        checkProc('export_offers');
        $("#send-offers").on("click", function(e) {
                e.preventDefault();
                var $obj = $(this).parents('.export-offer-button');
                var link = 'offers';
                $obj.find('.status').show();
                $obj.find('div[name="message_success"]').hide();
                $obj.find('div[name="message_error"]').hide();
                if ( typeof(module) !== 'undefined' && module === 1 ) {
                        link = 'wizard';
                }
                $.ajax({
                        type        : "POST",
                        url         : base_url + "tasks/run_export_ebay_by_node",
                        data        : { siteid : $("#id-site").val(), method : "compress", link : link},
                        timeout     : 0,
                        success     : function(result) {
                                    $obj.find('.status').hide();
                                    console.log($.trim(result));
                                    if ( $.trim(result) == 'Success' ) {
                                            $obj.find('div[name="message_success"]').show().find('span').html('').append($(".update-actions-offers-success").val());
                                    }
                                    else {
                                        checkProc('export_offers');
                                    }
                        },
                        error       : function(){
                            $obj.find('div[name="message_error"]').show().find('span').html('').append($(".update-actions-offers-fail").val());
                            $obj.find('.status').hide();
                            checkProc('export_offers');
                        }
                });
        });
        
        $("#delete-products").on("click", function(e) {
                e.preventDefault();
                var $obj = $(this).parents('.export-delete-button');
                $obj.find('.status').show();
                $obj.find('div[name="message_success"]').hide();
                $obj.find('div[name="message_error"]').hide();
                $('.delete-stock:checked').val();
                var link = 'ends';
                var method = 'compress';
                if ( $('.delete-stock:checked').val() === '0') {
                        method = 'deactivate';
                }
                else if ( $('.delete-stock:checked').val() === '1') {
                        method = 'stock';
                }
                else if ( $('.delete-stock:checked').val() === '2') {
                        method = 'compress';
                }
                else if ( $('.delete-stock:checked').val() === '3') {
                        method = 'full';
                }
                $.ajax({
                        type        : "POST",
                        url         : base_url + "tasks/run_export_ebay_by_node",
                        data        : { siteid : $("#id-site").val(), method : method, link : link},
                        timeout     : 0,
                        success     : function(result) {
                                    $obj.find('.status').hide();
                                    console.log($.trim(result));                                     
                                    if ( $.trim(result) == 'Success' ) {
                                            $obj.find('div[name="message_success"]').show().find('span').html('').append($(".update-actions-products-delete-success").val());
                                    }
                                    else {
                                        checkProc('delete_products');
                                    }
                                    
                                    if ( $('.delete-stock:checked').val() === '2') {
                                    	setTimeout (function () { window.location.reload(); }, 2000);
                                    }
                        },
                        error       : function(){
                            $obj.find('div[name="message_error"]').show().find('span').html('').append($(".update-actions-error").val());
                            $obj.find('.status').hide();
                            checkProc('delete_products');
                        }
                });
        });
        
        $("#export-orders").on("click", function(e) {
                e.preventDefault();
                var $obj = $(this).parents('.export-order-button');
                var link = 'shipment';
                var method = 'compress';
                $obj.find('.status').show();
                $obj.find('div[name="message_success"]').hide();
                $obj.find('div[name="message_error"]').hide();
                
                $.ajax({
                        type        : "POST",
                        url         : base_url + "tasks/run_export_ebay_by_node",
                        data        : { siteid : $("#id-site").val(), method : method, link : link},
                        timeout     : 0,
                        error       : function(jqXHR, textStatus, errorThrown) {
                                     console.log(jqXHR, textStatus, errorThrown);
                                     if(jqXHR.status == 404){
                                            $obj.find('div[name="message_error"]').show().find('span').html('').append($(".update-actions-orders-export-fail").val());
                                            $obj.find('.status').hide();
                                     }
                                     checkProc('import_orders');
                        },
                        success     : function(result) {
                                    $obj.find('.status').hide();
                                    console.log($.trim(result));
                                    if ( $.trim(result) == 'Success' ) {
                                            $obj.find('div[name="message_success"]').show().find('span').html('').append($(".update-actions-orders-export-success").val());
                                    }
                                    else{
                                        checkProc('import_orders');
                                    }
                        },
                });
        });
});
    