        var dataValue;
        var wi;
        var data                            = new Array();
        var wizard_data                     = new Array();
        var page                            = $("#current-page").val();

        function checkToken() {
        	if(wi.closed){
                $.ajax({
                        type    : "POST",
                        url     : base_url + "ebay/auth_security_token",
                        data    : { data : data },
                        cache: false,
                        success: function(data) {
                                if (data === 'OK') {
                                        wi.close();
                                        setTimeout (function(){ console.log("redirect"); }, 2000);
                                        if ( typeof(module) !== 'undefined' && module === 1 ) {
                                                    wizard_start();
                                        }
                                        else {
                                                $.ajax({
                                                        type        : "POST",
                                                        url         : base_url + "tasks/run_export_ebay_by_node",
                                                        data        : { siteid : $("#id-site").val(), method : 'synchronization_matching', link : 'products'},
                                                        timeout     : 0
                                                });
                                        	notification_alert($(".update-authentication-success").val() );
                                                setTimeout(function(){
                                                    location.href   = $('#form-submit').attr('action');
                                                }, 1200);
                                        }
                                }
                                else if ( data === 'Duplicated') {
                                        wi.close();
                                        notification_alert($(".user-not-available").val(), 'bad');
                                }
                                else {
                                        notification_alert($(".update-authentication-fail").val(), 'bad');
                                }
                        }
                });
        	}
        	else{
        		setTimeout ("checkToken()", 1500);
        	}
        }

        $('button[type="reset"]').on("click", function(e) {
                e.preventDefault();
                $("#form-user-id").val('');
        });
        
        $('.debug_mode').checkBo();

        $("#form-submit").on("submit", function(e) {
                var $obj = $('#synchronization-page-load');
                $obj.show();
                e.preventDefault();
                data                        = [];
                wizard_data                 = [];
                var x = screen.width/3 - 500;
                var y = screen.height/3 - 500;
                x = x<0?0:x;
                y = x<0?0:y; 
                wi = window.open('about:blank', 'sharegplus', 'height=1000,width=1000,left='+x+',top='+y);
                dataSelected   = {
                        'id_site'       : $("#id-site").val(),
                        'id_packages'   : $("#id-packages").val(),
                        'mode'          : (typeof($("#form-app-mode").prop( "checked" )) === 'undefined' || $("#form-app-mode").prop( "checked" ) === false),
                };
                data.push(dataSelected);

                if ( typeof(module) !== 'undefined' && module === 1 ) {
                        var data_selected = {
                                'id_site'       : $("#id-site").val(),
                                'id_packages'   : $("#id-packages").val(),
                                'id_popup'      : $("#popup-step").val(),
                        };
                        wizard_data.push(data_selected);
                }

                $.ajax({
                        type      : "POST",
                        url       : base_url + "ebay/auth_security_session",
                        data      : { data : data},
                        error     : function(jqXHR, textStatus, errorThrown) {
                                    notification_alert(errorThrown,'bad');
                        },
                        success   : function(result) {
                                    $obj.hide();
                                    console.log(result);
                                    try
                                    {
                                        var json = $.parseJSON(result);
                                        notification_alert(json['response'],'bad');
                                        wi.close();
                                    }
                                    catch(e) {
                                       if ( $.trim(result) != 'Fail' ) {
                                                wi.location.href = $.trim(result);
                                                data        = new Array();
                                                dataValue   = {
    //                                                'userID'        : $("#form-user-id").val(),
                                                    'id_site'       : $("#id-site").val(),
                                                    'id_packages'   : $("#id-packages").val(),
                                                    'mode'          : (typeof($("#form-app-mode").prop( "checked" )) === 'undefined' || $("#form-app-mode").prop( "checked" ) === false) ? true : false,
                                                    'result'        : result,
                                                };
                                                data.push(dataValue);
                                                checkToken();
                                        }
                                        else {
                                                notification_alert($(".update-" + page + "-fail").val(),'bad');
                                                wi.close();
                                        }
                                    }
                                    
                        }
                });
        });
