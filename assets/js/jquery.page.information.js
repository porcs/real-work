var ajaxChecked;
$(function(){
    $('.btnPrev').click(function(){
        event.preventDefault();
        showWaiting(true);
        window.location.href = siteurl+"service/packages";
    });
    
    /*$('.btnNext').click(function(){
        if($('#frmInfo').validationEngine('validate'))
        {
            showWaiting();
            $('#frmInfo').submit();
        }
        return false;
    });*/
    
    $('#frmInfo').validate({
            errorElement: 'p',
            errorClass: 'help-inline error',
            focusInvalid: false,
            rules: {
                "info[firstname]": {
                        required: true
                },
                "info[lastname]": {
                        required: true
                },
                "info[address1]": {
                        required: true
                },
//                "info[address2]": {
//                        required: true
//                },
                "info[zipcode]": {
                        required: true
                },
                "info[city]": {
                        required: true
                },
                "info[complement]": {
                        required: true
                },
                "info[stateregion]": {
                        required: true
                },
                "info[country]": {
                        required: true
                }
            },

            /*messages: {
                email: {
                        required: $('#valid_email').val(),
                        email: $('#valid_email').val()
                },
                password: {
                        required: $('#specify_password').val()
                }
            }*/
            submitHandler: function(form) {
                if($('#country').val()=='' || $('#country').val()==null){
                    $('#country').parents('.country').addClass('has-error active').append('<p for="city" class="help-inline error"><i class="fa fa-exclamation-circle"></i> This field is required.</p>');
                    return false;
                }else{
                    $('#country').parents('.country').removeClass('has-error active').find('.help-inline.error').remove();
                }
                showWaiting(true);
                form.submit();
            }
            
    });
    
    $('#country').on('change',function(){
            if($('#country').val()==''){
                $('#country').parents('.country').addClass('has-error active').append('<p for="city" class="help-inline error"><i class="fa fa-exclamation-circle"></i> This field is required.</p>');
                return false;
            }else{
                $('#country').parents('.country').removeClass('has-error active').find('.help-inline.error').remove();
            }
    });
    
    $('#form-check').click(function(e){
        if($(this).is(':checked'))
        {
            ajaxChecked = $.ajax({
                type: 'POST',
                data: $.param({
                    'isAddr': 'true'
                }),
                url: siteurl+'service/address',
                beforeSend: function(){
                    try
                    {
                        ajaxChecked.abort();
                    }catch(Exception){}
                },
                success: function(xhr){
                    if(xhr != '')
                    {
                        var jsonVar = $.parseJSON(xhr);

                        $('#firstname').val(jsonVar['first_name']);
                        $('#lastname').val(jsonVar['last_name']);
                        $('#complement').val(jsonVar['complement']);
                        $('#address1').val(jsonVar['address1']);
                        $('#address2').val(jsonVar['address2']);
                        $('#state_region').val(jsonVar['stateregion']);
                        $('#zipcode').val(jsonVar['zipcode']);
                        $('#city').val(jsonVar['city']);
                        $('#country').val(jsonVar['country']);
                    }
                }
            });
        }
    });
});