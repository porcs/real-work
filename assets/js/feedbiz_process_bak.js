
(function($) {
    $.easyPieChart = function(el, options) {
        var addScaleLine, animateLine, drawLine, easeInOutQuad, rAF, renderBackground, renderScale, renderTrack,
                _this = this;
        this.el = el;
        this.$el = $(el);
        this.$el.data("easyPieChart", this);
        this.init = function() {
            var percent, scaleBy;
            _this.options = $.extend({}, $.easyPieChart.defaultOptions, options);
            percent = parseInt(_this.$el.data('percent'), 10);
            _this.percentage = 0;
            _this.canvas = $("<canvas width='" + _this.options.size + "' height='" + _this.options.size + "'></canvas>").get(0);
            _this.$el.append(_this.canvas);
            if (typeof G_vmlCanvasManager !== "undefined" && G_vmlCanvasManager !== null) {
                G_vmlCanvasManager.initElement(_this.canvas);
            }
            _this.ctx = _this.canvas.getContext('2d');
            if (window.devicePixelRatio > 1) {
                scaleBy = window.devicePixelRatio;
                $(_this.canvas).css({
                    width: _this.options.size,
                    height: _this.options.size
                });
                _this.canvas.width *= scaleBy;
                _this.canvas.height *= scaleBy;
                _this.ctx.scale(scaleBy, scaleBy);
            }
            _this.ctx.translate(_this.options.size / 2, _this.options.size / 2);
            _this.ctx.rotate(_this.options.rotate * Math.PI / 180);
            _this.$el.addClass('easyPieChart');
            _this.$el.css({
                width: _this.options.size,
                height: _this.options.size,
                lineHeight: "" + _this.options.size + "px"
            });
            _this.update(percent);
            return _this;
        };
        this.update = function(percent) {
            percent = parseFloat(percent) || 0;
            if (_this.options.animate === false) {
                drawLine(percent);
            } else {
                animateLine(_this.percentage, percent);
            }
            return _this;
        };
        renderScale = function() {
            var i, _i, _results;
            _this.ctx.fillStyle = _this.options.scaleColor;
            _this.ctx.lineWidth = 1;
            _results = [];
            for (i = _i = 0; _i <= 24; i = ++_i) {
                _results.push(addScaleLine(i));
            }
            return _results;
        };
        addScaleLine = function(i) {
            var offset;
            offset = i % 6 === 0 ? 0 : _this.options.size * 0.017;
            _this.ctx.save();
            _this.ctx.rotate(i * Math.PI / 12);
            _this.ctx.fillRect(_this.options.size / 2 - offset, 0, -_this.options.size * 0.05 + offset, 1);
            _this.ctx.restore();
        };
        renderTrack = function() {
            var offset;
            offset = _this.options.size / 2 - _this.options.lineWidth / 2;
            if (_this.options.scaleColor !== false) {
                offset -= _this.options.size * 0.08;
            }
            _this.ctx.beginPath();
            _this.ctx.arc(0, 0, offset, 0, Math.PI * 2, true);
            _this.ctx.closePath();
            _this.ctx.strokeStyle = _this.options.trackColor;
            _this.ctx.lineWidth = _this.options.lineWidth;
            _this.ctx.stroke();
        };
        renderBackground = function() {
            if (_this.options.scaleColor !== false) {
                renderScale();
            }
            if (_this.options.trackColor !== false) {
                renderTrack();
            }
        };
        drawLine = function(percent) {
            var offset;
            renderBackground();
            _this.ctx.strokeStyle = $.isFunction(_this.options.barColor) ? _this.options.barColor(percent) : _this.options.barColor;
            _this.ctx.lineCap = _this.options.lineCap;
            _this.ctx.lineWidth = _this.options.lineWidth;
            offset = _this.options.size / 2 - _this.options.lineWidth / 2;
            if (_this.options.scaleColor !== false) {
                offset -= _this.options.size * 0.08;
            }
            _this.ctx.save();
            _this.ctx.rotate(-Math.PI / 2);
            _this.ctx.beginPath();
            _this.ctx.arc(0, 0, offset, 0, Math.PI * 2 * percent / 100, false);
            _this.ctx.stroke();
            _this.ctx.restore();
        };
        rAF = (function() {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
                return window.setTimeout(callback, 1000 / 60);
            };
        })();
        animateLine = function(from, to) {
            var anim, startTime;
            _this.options.onStart.call(_this);
            _this.percentage = to;
            Date.now || (Date.now = function() {
                return +(new Date);
            });
            startTime = Date.now();
            anim = function() {
                var currentValue, process;
                process = Date.now() - startTime;
                if (process < _this.options.animate) {
                    rAF(anim);
                }
                _this.ctx.clearRect(-_this.options.size / 2, -_this.options.size / 2, _this.options.size, _this.options.size);
                renderBackground.call(_this);
                currentValue = [easeInOutQuad(process, from, to - from, _this.options.animate)];
                _this.options.onStep.call(_this, currentValue);
                drawLine.call(_this, currentValue);
                if (process >= _this.options.animate) {
                    return _this.options.onStop.call(_this, currentValue, to);
                }
            };
            rAF(anim);
        };
        easeInOutQuad = function(t, b, c, d) {
            var easeIn, easing;
            easeIn = function(t) {
                return Math.pow(t, 2);
            };
            easing = function(t) {
                if (t < 1) {
                    return easeIn(t);
                } else {
                    return 2 - easeIn((t / 2) * -2 + 2);
                }
            };
            t /= d / 2;
            return c / 2 * easing(t) + b;
        };
        return this.init();
    };
    $.easyPieChart.defaultOptions = {
        barColor: '#ef1e25',
        trackColor: '#f2f2f2',
        scaleColor: '#dfe0e0',
        lineCap: 'round',
        rotate: 0,
        size: 110,
        lineWidth: 3,
        animate: false,
        onStart: $.noop,
        onStop: $.noop,
        onStep: $.noop
    };
    $.fn.easyPieChart = function(options) {
        return $.each(this, function(i, el) {
            var $el, instanceOptions;
            $el = $(el);
            if (!$el.data('easyPieChart')) {
                instanceOptions = $.extend({}, options, $el.data());
                return $el.data('easyPieChart', new $.easyPieChart(el, instanceOptions));
            }
        });
    };
    return void 0;
})(jQuery);

$(document).ready(function() {

    function initial_progress_bar() {
        //console.log('xxxx');
        /*if($('.progress_cv').length==0){
         var obj = $('<div class="progress_cv"><div class="progress_small"><div class="running_task" style="display:none;"><div class="running_icon" style="display: inline-block;position: absolute;left: 0;bottom: 0px;"><i class="icon_chk icon-spinner icon-spin orange bigger-300"  ></i></div><div class="running_text" style="width:165px;display: inline-block;margin: 0 0 10px 40px;float: left;"></div></div><div class="easy-pie-chart percentage" data-percent="0" data-size="46"  ><span class="percent">0</span>%</div></div><div class="progress_full"><div class="close_pfull active"></div><ul></ul></div></div>');
         $(obj).appendTo('body');
         
         }*/
    }

    initial_progress_bar();
    var timeout = 5000;
    var pfin = false;
    var pst = false;
    var delay = 2000;
    var sum_percent = 0;
    var charts = $('.progress_cv .progress_small .percentage');
    var running_box = $('.progress_cv .running_task');
    var oldie = /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase());
    var complete_count = 0
    var lost_count = 0;
    var max_lost = 100;
    function call_update() {
        online = window.navigator.onLine;
        if (!online) {
            console.log('Browser offline!');
            lost_count++;
            if (lost_count < max_lost) {
                setTimeout(call_update, timeout);
            }
            return;
        }
//        try{
        $.ajax({url: base_url+"users/ajax_update_process", dataType: "json", timeout: timeout,
            beforeSend: function(jqXHR, setting) {
                jqXHR.fail(function() {
                    jqXHR.abort();
                    lost_count++;
                    if (lost_count < max_lost) {
                        setTimeout(call_update, timeout);
                    }
                })
            },
            success: function(r) {
                lost_count = 0;
                var running = 0;
                complete_count = 0;
                if (r.length != 0) {
                    var sum_progress = 0;
                    var i = 0;
                    //var proc_id = []; 
                    if (r.proc) {
                        pst = true;
                        pfin = false;
                        var proc = r.proc;
                        $.each(proc, function(id, info) {
                            //proc_id.push(id);
                            add_process(info);
                            sum_progress += info.progress;
                            i++;
                            if (info.status == '1')
                                running++;
                        });
                    }
                    //console.log(i,complete_count,running,typeof callback_when_running_process,jQuery.isFunction(callback_when_running_process));
                    if (i == complete_count && complete_count != 0) {
                        if (typeof callback_after_done_process !== 'undefined' && jQuery.isFunction(callback_after_done_process)) { // if define this function they will run after all process complete
                            callback_after_done_process();
                        }
                    } else {

                        if (running > 0) {
                            if (typeof callback_when_running_process !== 'undefined' && jQuery.isFunction(callback_when_running_process)) { // if define this function they will run after all process complete
                                callback_when_running_process();
                            }
                        }
                    }
                    //console.log(i,complete_count,typeof callback_after_done_process);
                    if (i == 0)
                        i++;
                    sum_percent = sum_progress / i;
                    set_sum_prog(sum_percent);
                }


                if (!r.proc && !pst && pfin) {
                    console.log('fire');
                    $.ajax({url: base_url+"users/ajax_update_process", data: {clear_all_process: 'all'}, async: false});
                    $('.progress_cv').slideUp("slow", function() {
                        $('.progress_cv .progress_full ul li').each(function() {
                            $(this).hide().remove();
                        });
                    });
                    pfin = false;
                    pst = false;
                }
                //console.log(running,pfin,pst,delay,timeout,r.length);
                if (running == 0) { //all process is done
                    if (pfin || pst) {
                        delay = timeout;
                        pfin = true;
                        pst = false;
                    }

                }
                else if (r.length > 0) { // process at running

                    delay = r.delay;
                    timeout = r.timeout;

                }
                setTimeout(call_update, delay);
            },
            error: function(xhr, status, error) {
                if (xhr.responseText) {
                    console.log('Progress status error :', xhr.responseText);
                    lost_count++;
//                    if(xhr.responseText === 'terminate'){
//                        return;
//                    }
                    if (lost_count < max_lost) {
                        setTimeout(call_update, timeout);
                    }
                }

                //setTimeout(call_update, delay);   
            }
        });
//        }catch(e){
//            console.log('Error',e);
//            lost_count++;
////                     
//            if(lost_count < 10){
//                setTimeout(call_update, timeout);
//            }
//        }
    }
    $(window).bind('beforeunload', function() {
        $.unloading = true;
        console.log('Event catch');
    });
//    window.onerror = function(message, filename, linenumber) {
//        // Perform error reporting here, like logging error message 
//        // with file name and line number on the server
//        // for later processing. 
//        console.log(message, filename, linenumber);
//        lost_count++;
////                     
//        if (lost_count < 100) {
//            setTimeout(call_update, timeout);
//        }
//        return true; // The exception is handled, don't show to the user.
//    }
    function set_sum_prog(perc) {
        var o = $('.progress_cv .progress_small .percentage');
        if (o.data('easyPieChart'))
            o.data('easyPieChart').update(perc);
        o.find('span').text(perc);
    }
    function add_process(info) {
        var hide = true;
        if (info.action === 'import') {
            c = 'progress-warning';
        } else {
            c = 'progress-warning';
        }
        if ($('.progress_cv').is(":visible")) {
            hide = false;
        }
//          var old_obj = $('.progress_cv .progress_full ul li[data-act="'+info.process+'"]');
//          if(old_obj.length>0){
//            old_obj.each(function(){
//              var o_bid = $(this).attr('data-pid');
//              var type = $(this).attr('data-type');
//              if(o_bid!=info.pid && type == 'import'){
//                  $.ajax({url:"/users/ajax_update_process",data:{clear_process:o_bid},async:false});
//
//                  $(this).slideUp('slow',function(){
//                      $(this).remove(); 
//                  })
//              }
//            });
//          }

        var obj = $('.progress_cv .progress_full ul li[data-act="' + info.process + '"]');
        var shop_name = '';
        if (info.name != '') {
            shop_name = ' (<span>' + info.name + '</span>)';
        }
        var priority = (obj).attr('data-priority');
        var new_line = false;
        if (obj.length === 0) {
            new_line = true;
            obj = '<li style="display:none;" data-bid="' + info.bid + '" data-pid="' + info.pid + '" data-sid="' + info.sid + '" data-act="' + info.process + '" data-type="' + info.action + '" data-stat="' + info.status + '" data-priority="' + info.priority + '"><div class="progress_row"><div class="progress_message ' + info.action + '" >' + info.msg + shop_name + '</div><div class="progress ' + c + ' progress-small progress-striped active" data-percent="' + info.progress + '%"><div class="bar" style="width: ' + info.progress + '%;"></div></div></div></li>';
            $('.progress_full ul').append(obj);
            var obj = $('.progress_cv .progress_full ul li[data-act="' + info.process + '"]');
        } else {
//                if(info.priority > priority ){
//                    obj.find('.progress_message').html(info.msg+shop_name);
//                     obj.attr('data-priority',info.priority);
//                }
            obj.find('.progress_message').html(info.msg + shop_name);
        }
//            var priority = obj.attr('data-priority');
//            if(info.priority < priority ) return; 
        var msg = '';
        if (info.err_msg != '') {
            msg = info.err_msg;
        } else if (info.proc_msg != '') {
            msg = info.proc_msg;
        }

        if (typeof info.proc_msg === 'object' || info.popup_display === false)
        {
            var ori_msg;
            if (info.err_msg != '')
            {
                ori_msg = encodeURIComponent(JSON.stringify(info.err_msg));
            }
            else if (info.proc_msg != '')
            {
                ori_msg = encodeURIComponent(JSON.stringify(info.proc_msg));
            }

            var mkp = info.marketplace;

            $.ajax({
                url: base_url+"tasks/display_popup_template/" + mkp + "/" + ori_msg,
                success: function(r) {
                    //Amazon
                    if ($('#actions').length > 0)
                    {
                        var div_status = $('#actions').find('.status');
                        $.each(div_status, function() {
                            if ($(this).show())
                            {
                                $(this).hide();
                                $(this).parent().find('button').attr('disabled', 'disabled').parent().parent().show();
                            }
                        });
                    }

                    obj = $('.progress_cv .progress_full ul li[data-act="' + info.process + '"]');

                    $(obj).html(r);
                    running_box.find('.running_text').html(info.msg);
                    running_box.show();
                    charts.hide();

                    if (hide) {
                        $(".progress_cv").delay(300).slideDown('slow');
                    }

                    var obj = $('.progress_cv .progress_full ul li[data-bid="' + info.bid + '"]');
                    $(obj).delay(500).slideDown('slow');
                }
            });

        } else {
            msg = capitaliseFirstLetter(msg);

            if (info.s_status === 'complete') {
                obj.find('.progress').removeClass(c).removeClass('active').addClass('progress-success');
                obj.find('.progress').attr('data-percent', 'Completed !!');
                obj.find('.progress .bar').css('width', '100%');
                complete_count++;
            } else if (info.status === 4 || info.status === 8) {
                obj.find('.progress').removeClass(c).removeClass('active').removeClass('progress-success').addClass('progress-pink');
                if (msg != '') {
                    obj.find('.progress').attr('data-percent', msg);
                } else {
                    obj.find('.progress').attr('data-percent', 'Error!! Please try again. ' + msg);
                }
                obj.find('.progress .bar').css('width', '100%');
            } else {
                obj.find('.progress').addClass(c).removeClass('progress-pink').removeClass('progress-success')
                if (!obj.find('.progress').hasClass('active'))
                    obj.find('.progress').addClass('active')
                if (msg && msg != '' && info.progress * 1 == 0) {
                    obj.find('.progress').attr('data-percent', msg + ' ');
                    obj.find('.progress .bar').css('width', '100%');
                } else if (msg != '') {
                    obj.find('.progress').attr('data-percent', msg + ' : ' + info.progress + '%');
                    obj.find('.progress .bar').css('width', info.progress + '%');
                } else {
                    obj.find('.progress').attr('data-percent', info.progress + '%');
                    obj.find('.progress .bar').css('width', info.progress + '%');
                }

            }

            if (hide) {
                $(".progress_cv").delay(300).slideDown('slow');
            }
            var obj = $('.progress_cv .progress_full ul li[data-bid="' + info.bid + '"]');
            $(obj).delay(500).slideDown('slow');
        }
    }


    var initCharts = function() {
        charts.easyPieChart({
            barColor: '#87B87F',
            trackColor: '#EEEEEE',
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: 3,
            animate: oldie ? false : 1000,
            size: $(this).data('size'),
            onStep: function(value) {
                this.$el.find('span').text(~~value);
            }
        });

        $('.progress_cv .progress_small').click(function() {
            if (!$('.progress_cv .close_pfull').hasClass('active')) {
                $('.progress_cv .close_pfull').addClass('active');
                $('.progress_cv .progress_small').fadeOut('slow', 'linear')
                $('.progress_cv .progress_full').slideDown('slow', 'swing')
            }
        });
        $('.progress_cv .close_pfull').on('click', function(e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                if (pst && !pfin) {
                    $(this).removeClass('active');
                    $('.progress_cv .progress_small').show('fast', 'linear')
                    $('.progress_cv .progress_full').hide('slow', 'linear')
                    charts.each(function() {
                        $(this).data('easyPieChart').update(sum_percent);
                    });
                } else if (!pst && pfin) {
                    $.ajax({url: base_url+"users/ajax_update_process", data: {clear_all_process: 'all'}, async: false});
                    $('.progress_cv').slideUp("slow", function() {
                        $('.progress_cv .progress_full ul li').each(function() {
                            $(this).hide().remove();
                            pfin = false;
                            pst = false;
                            //console.log($(this));
                        });
                    });
                }
            }
        });

    }
    initCharts();
    call_update();

    function capitaliseFirstLetter(string)
    {
        if (!string)
            return false;
        string = replaceAll('_', ' ', string);
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    function replaceAll(find, replace_txt, str) {

        if (str && str != undefined) {
            return str.replace(new RegExp(find, 'g'), replace_txt);
        } else {
            return '';
        }
    }
});


