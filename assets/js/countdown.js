(function($){

	var days	= 24*60*60,
		hours	= 60*60,
		minutes	= 60;
	
	$.fn.countdown = function(prop){
		
		var options = $.extend({
			callback	: function(){},
			timestamp	: 0,
                        start_timestamp : 0,
                        timerEnd        : function(){},
                        justEnd         : false,
                        this_obj        : null,
		},prop);
		
		var left, d, h, m, s, positions;
                
		init(this, options);
		
		positions = this.find('.position');
		options.this_obj = this;
		(function tick(){
//                    console.log(obj_countdown,obj_countdown.timestamp)
                        if(options.this_obj.attr('rel')!=options.start_timestamp && options.this_obj.attr('rel')>0){ 
                            var r = options.this_obj.attr('rel');
                            options.timestamp = (new Date()).getTime()+ r*1000 ;
                            options.this_obj.attr('rel',options.start_timestamp);
                        }
			left = Math.floor((options.timestamp - (new Date())) / 1000);
			
			if(left < 0){
				left = 0;
			}

			d = Math.floor(left / days);
			updateDuo(0, 1, d);
			left -= d*days;

			h = Math.floor(left / hours);
			updateDuo(2, 3, h);
			left -= h*hours;
			
			m = Math.floor(left / minutes);
			updateDuo(4, 5, m);
			left -= m*minutes;

			s = left;
			updateDuo(6, 7, s);
			
			options.callback(d, h, m, s);
                         
                        if(d==0 && 0==m && 0==s && options.start_timestamp != 0 && options.justEnd == false){
                            options.justEnd = true;
                            options.timerEnd();
                            setTimeout(function(){
                                options.justEnd = false;
                            },20000);
                            
                        }
			
			setTimeout(tick, 1000);
		})();
				
		function updateDuo(minor,major,value){
			switchDigit(positions.eq(minor),Math.floor(value/10)%10,minor);
			switchDigit(positions.eq(major),value%10,major);
		}
		
		return this;
	};


	function init(elem, options){
		elem.addClass('countdownHolder');
                var add='';
                
		// Создаем разметку внутри контейнера
		$.each(['Days','Hours','Minutes','Seconds'],function(i){
                        
                        if(this=='Days'){add='<span class=" break_sign">  </span>';}
                        else{
                            if(options.break_sign){
                                add = '<span class=" break_sign">'+options.break_sign+'</span>';
                            }
                        }
                        if(this=='Seconds')add='';
			$('<span class="count'+this+'">').html(
				'<span class="position">\
					<span class="digit static">0</span>\
				</span>\
				<span class="position">\
					<span class="digit static">0</span>\
				</span>'+add
			).appendTo(elem);
			
			if(this!="Seconds"){
				elem.append('<span class="countDiv countDiv'+i+'"></span>');
			}
		});

	}
	
	function switchDigit(position,number,pos){
		
		var digit = position.find('.digit')
		
		if(digit.is(':animated')){
			return false;
		}
		
		if(position.data('digit') == number){

			return false;
		}
		
		position.data('digit', number);
		if(pos==0||pos==1 && number==0){
                    number='';
                }
                if(pos==1 && number !='' ){
                    number+=' d';
                }
		var replacement = $('<span>',{
			'class':'digit',
			css:{
				opacity:0
			},
			html:number
		});

		digit
			.before(replacement)
			.removeClass('static')
			.animate({opacity:0},'fast',function(){
				digit.remove();
			})

		replacement
			.delay(100)
			.animate({opacity:1},'fast',function(){});
	}
})(jQuery);