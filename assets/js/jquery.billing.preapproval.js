$(function(){
    $('.btn-savechanges').click(function(e){
        showWaiting(true);
        $('#frmAgree').submit();
    });
    
    $('.btn-cancel').click(function(e){
        e.preventDefault();
        window.location.href = siteurl+'billing/configuration';
    });
});