$(function(){
    var data = [
                ['Bear', 8],
                ['Racoon', 9],
                ['Wolf', 3],
                ['Mouse', 6],
                ['Horse', 7],
                ['Man', 4],
                ['Hightingale', 7],
                ['Cow', 3]
            ];
    
    $('#column').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        exporting:{
            buttons: {
                contextButton: {
                    enabled:false
                }
            }
        },
        legend: {
            enabled: false
        },
        scrollbar:{
            enabled:false
        },
        navigator:{
            enabled:false
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: "Value: {point.y:,.1f} "
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        xAxis: {
            enabled: false
          /*gridLineWidth: 1,
          lineColor: '#e2e2e2',
          labels: {
             style: {
                fill: '#e2e2e2',
                font: '11px Trebuchet MS, Verdana, sans-serif'
             }
          },
          title: {
             style: {
                fill: 'red',
                fontWeight: 'bold',
                fontSize: '12px',
                fontFamily: 'Trebuchet MS, Verdana, sans-serif'

             }
          }*/
       },
        yAxis: {
            enabled: false
            /*labels:{
                style: {
                    fill: '#e2e2e2',
                    'font-family':'Open Sans',
                    fontSize:'12px',
                    fontWeight:'bold'
                }
            }*/
        },       
        series: [{
            name: 'Browser share',
             data: data
        }] 
    });
});