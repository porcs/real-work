$(function(){
    String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = minutes+':'+seconds;
    return time;
    }
    
    $('.gauge3 .box').css('-webkit-transform', 'rotate('+$degree+'deg)')
    $('.gauge2 .box').css('-webkit-transform', 'rotate('+$degree_all+'deg)')
    $('span#num_prods').countTo({ to:$count_to  , speed: 2000, refreshInterval: 50});
    $('#feedbiz_icon').sprite({fps: 15, no_of_frames: 30});
    $('.countdown').each(function(){
        var val = $(this).attr('rel');
        
        if(val== -1){
            $(this).html($process_not_found).parent().addClass('disabled');
            return;
        }else if(val== -2){
            $(this).html($service_comming_soon).parent().addClass('disabled');
            return;
        }
        var qnt = parseFloat(val).toString().toHHMMSS();
        //console.log(qnt.toHHMMSS());
        console.log($(this));
        var objcount = $(this);
        objcount.html('');
        objcount.countdown({
          image: "assets/images/digits.png",
          format: "mm:ss",
          digitImages: 6,
          digitWidth: 38,
          digitHeight: 50,
          startTime: qnt
        });
    });
}); 

/* Ini Donut 2 Chart */
var datax = [{ /*"Free space":$freeuse_precent,"Inactive":$inuse_precent,"Active":$use_percent */}];
                    var data_order = [
                    { "Ebay":0.7574,"Amazon":0.2426 },
                    { "Ebay":0.6414,"Amazon":0.3586 },
                    { "Ebay":0.1486,"Amazon":0.8514 }];

                    var svg = d3.select("#chartdonut")
                            .append("svg")
                            .append("g");
                    var svg_order = d3.select("#chartdonut_order")
                            .append("svg")
                            .append("g");

                    var w = window,
                        d = document,
                        e = d.documentElement,
                        g = d.getElementsByTagName('body')[0],
                        x = w.innerWidth || e.clientWidth || g.clientWidth,
                        y = w.innerHeight|| e.clientHeight|| g.clientHeight;

                    var width = $('#chartdonut').width(),
                    width_order = $('#chartdonut_order').width(),
                    height = 200,
                    radius = Math.min(width, height) / 2;

                    svg.append("g")
                            .attr("class", "slices");
                    svg.append("g")
                            .attr("class", "labels");
                    svg.append("g")
                            .attr("class", "percent");
                    svg.append("g")
                            .attr("class", "lines");
                    svg_order.append("g")
                            .attr("class", "slices");
                    svg_order.append("g")
                            .attr("class", "labels");
                    svg_order.append("g")
                            .attr("class", "percent");
                    svg_order.append("g")
                            .attr("class", "lines");

                    var pie = d3.layout.pie()
                            .sort(null)
                            .value(function(d) {
                                    return d.value;
                            });

                    var arc = d3.svg.arc()
                                    .outerRadius(radius * 0.7)
                                    .innerRadius(radius * 0.4);

                    var outerArc = d3.svg.arc()
                                            .innerRadius(radius * 0.7)
                                            .outerRadius(radius * 0.9);

                    function updateWindow(){
                        x = w.innerWidth || e.clientWidth || g.clientWidth;
                        y = w.innerHeight|| e.clientHeight|| g.clientHeight;
                        svg.attr("transform", "translate(" + $('#chartdonut').width() / 2 + "," + height / 2 + ")");
                        svg_order.attr("transform", "translate(" + $('#chartdonut_order').width() / 2 + "," + height / 2 + ")");                          
                    }
                    window.onresize = updateWindow;

                    svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
                    svg_order.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                    var key = function(d){ return d.data.label; };

                    var color = d3.scale.category10()
                                            .domain(["Inactive","Free space", "Active"])
                                            .range(["#ff4646", "#398bff", "#f99b01"]);
                    var color_order = d3.scale.category10()
                                            .domain(["Ebay", "Amazon"])
                                            .range(["#ff4646", "#398bff"]);

                    function randomData (q,c){
                            var labels = c.domain();
                            return labels.map(function(label){
                                    return { label: label, value: q[label]/*Math.random()*/ }
                            }).filter(function() {
                                    
                                    return 1;
                            }).sort(function(a,b) {
                                return d3.ascending(a.label, b.label);
                            });
                    }

                    change(svg,randomData(datax[0],color));
                    change(svg_order,randomData(data_order[2],color_order));

                  
                    d3.select(".day_order")
                            .on("click", function(){
                                    change(svg_order,randomData(data_order[0],color_order));
                            });

                    d3.select(".day7_order")
                            .on("click", function(){
                                    change(svg_order,randomData(data_order[1],color_order));
                            });

                    d3.select(".day30_order")
                            .on("click", function(){
                                    change(svg_order,randomData(data_order[2],color_order));
                            });

                    function mergeWithFirstEqualZero(first, second){
                            var secondSet = d3.set(); second.forEach(function(d) { secondSet.add(d.label); });

                            var onlyFirst = first
                                    .filter(function(d){ return !secondSet.has(d.label) })
                                    .map(function(d) { return { label: d.label, value: 0}; });
                            return d3.merge([ second, onlyFirst ])
                                    .sort(function(a,b) {
                                            return d3.ascending(a.label, b.label);
                                    });
                    }

                    function change(svgobj,data) {
                            var duration = 2500;
                            var data0 = svgobj.select(".slices").selectAll("path.slice")
                                    .data().map(function(d) { /*console.log(d.data);*/ return d.data });
                            if (data0.length == 0) data0 = data;
                            var was = mergeWithFirstEqualZero(data, data0);
                            var is = mergeWithFirstEqualZero(data0, data);

                            /* ------- SLICE ARCS -------*/

                            var slice = svgobj.select(".slices").selectAll("path.slice")
                                    .data(pie(was), key);

                            slice.enter()
                                    .insert("path")
                                    .attr("class", "slice")
                                    .style("fill", function(d) { /*console.log(color(d.data.label));*/return color(d.data.label); })
                                    .each(function(d) {
                                            this._current = d;
                                    });

                            slice = svgobj.select(".slices").selectAll("path.slice")
                                    .data(pie(is), key);

                            slice		
                                    .transition().duration(duration)
                                    .attrTween("d", function(d) {
                                            var interpolate = d3.interpolate(this._current, d);
                                            var _this = this;
                                            return function(t) {
                                                    _this._current = interpolate(t);
                                                    return arc(_this._current);
                                            };
                                    });

                            slice = svgobj.select(".slices").selectAll("path.slice")
                                    .data(pie(data), key);

                            slice
                                    .exit().transition().delay(duration).duration(0)
                                    .remove();

                            /* ------- TEXT LABELS -------*/
                            var text = svgobj.select(".labels").selectAll("text")
                                    .data(pie(was), key);

                            text.enter()
                                    .append("text")
                                    .attr("dy", "1em")
                                    .style("opacity", 0)
                            .style("font-size", "12px")
                                    .text(function(d) {
                                var out = d.data.label;
                                switch(d.data.label){
                                    case 'Active': out = $active; break;
                                    case 'Inactive': out = $inactive; break;
                                    case 'Space free': out = $spacefree; break;
                                }
                                            return out;
                                    })
                                    .each(function(d) {
                                            this._current = d;
                                    });

                            function midAngle(d){
                                    return d.startAngle + (d.endAngle - d.startAngle)/2;
                            }

                            text = svgobj.select(".labels").selectAll("text")
                                    .data(pie(is), key);

                            text.transition().duration(duration)
                                    .style("opacity", function(d) {
                                            return d.data.value == 0 ? 0 : 1;
                                    })
                                    .attrTween("transform", function(d) {
                                            var interpolate = d3.interpolate(this._current, d);
                                            var _this = this;
                                            return function(t) {
                                                    var d2 = interpolate(t);
                                                    _this._current = d2;
                                                    var pos = outerArc.centroid(d2);
                                                    pos[0] = radius * (midAngle(d2) < Math.PI ? 1.2 : -1.2);
                                    return "translate("+ pos +")";
                                            };
                                    })
                                    .styleTween("text-anchor", function(d){
                                            var interpolate = d3.interpolate(this._current, d);
                                            return function(t) {
                                                    var d2 = interpolate(t);
                                                    return midAngle(d2) < Math.PI ? "start":"end";
                                            };
                                    });

                            text = svgobj.select(".labels").selectAll("text")
                                    .data(pie(data), key);

                            text
                                    .exit().transition().delay(duration)
                                    .remove();

                        /* ------- TEXT LABELS PERCENT % -------*/
                            
                            var textpercent = svgobj.select(".percent").selectAll("text")
                                    .data(pie(was), key);

                            textpercent.enter()
                                    .append("text")
                                    .attr("dy", "-0.5em")
                                    .style("opacity", 0)
                            .style("font-size", "18px")
                                    .text(0)
                                    .each(function(d) {
                                            this._current = d;
                                    });

                            textpercent = svgobj.select(".percent").selectAll("text")
                                    .data(pie(is), key);


                            textpercent.transition().duration(duration)
                                    .style("opacity", function(d) {
                                            return d.data.value == 0 ? 0 : 1;
                                    })
                                    .attrTween("transform", function(d) {
                                            var interpolate = d3.interpolate(this._current, d);
                                            var _this = this;
                                            return function(t) {
                                                    var d2 = interpolate(t);
                                                    _this._current = d2;
                                                    var pos = outerArc.centroid(d2);
                                                    pos[0] = radius * (midAngle(d2) < Math.PI ? 1.2 : -1.2);

                                    return "translate("+ pos +")";
                                            };
                                    })
                                    .styleTween("text-anchor", function(d){
                                            var interpolate = d3.interpolate(this._current, d);
                                            return function(t) {
                                                    var d2 = interpolate(t);
                                                    return midAngle(d2) < Math.PI ? "start":"end";
                                            };
                                    })
                                    
                                    .tween("text", function(d) {
                                            var a = parseFloat(d.data.value)*100;
                                            var b = parseFloat(this.textContent);
                                var i = d3.interpolate(b, a),
                                    prec = (a + "").split("."),
                                    round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
                                                    //console.log(b);
                                return function(t) {			
                                    this.textContent = (Math.round(i(t) * round) / round).toFixed(2)+"%";
                                };
                            })                                    

                            textpercent = svgobj.select(".percent").selectAll("text")
                                    .data(pie(data), key);

                            textpercent
                                    .exit().transition().delay(duration)
                                    .remove();

                            /* ------- SLICE TO TEXT POLYLINES -------*/

                            var polyline = svgobj.select(".lines").selectAll("polyline")
                                    .data(pie(was), key);

                            polyline.enter()
                                    .append("polyline")
                                    .style("opacity", 0)
                                    .each(function(d) {
                                            this._current = d;
                                    });

                            polyline = svgobj.select(".lines").selectAll("polyline")
                                    .data(pie(is), key);

                            polyline.transition().duration(duration)
                                    .style("opacity", function(d) {
                                            return d.data.value == 0 ? 0 : 0.7;
                                    })
                                    .attrTween("points", function(d){
                                            this._current = this._current;
                                            var interpolate = d3.interpolate(this._current, d);
                                            var _this = this;
                                            return function(t) {
                                                    var d2 = interpolate(t);
                                                    _this._current = d2;
                                                    var pos = outerArc.centroid(d2);
                                                    pos[0] = radius * 1.5 * (midAngle(d2) < Math.PI ? 1 : -1);
                                                    return [arc.centroid(d2), outerArc.centroid(d2), pos];
                                            };			
                                    });

                            polyline = svgobj.select(".lines").selectAll("polyline")
                                    .data(pie(data), key);

                            polyline
                                    .exit().transition().delay(duration)
                                    .remove();
                    };
                    
var randomScalingFactor = function(){ return Math.round(Math.random()*1500)};
		var lineChartData = {
			labels : ["January","February","March","April","May","June","July"],
			datasets : [
				{
					label: "Ebay",
					fillColor : "rgba(151,187,205,0.2)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				},
				{
					label: "Amazon",
					fillColor : "rgba(220,151,151,0.2)",
					strokeColor : "rgba(220,151,151,1)",
					pointColor : "rgba(220,151,151,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,151,151,1)",
					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				}
			]

		};
		var lineChartData7 = {
			labels : ["January","February","March","April","May","June","July"],
			datasets : [
				{
					label: "Ebay",					
					fillColor : "rgba(151,187,205,0.2)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				},
				{
					label: "Amazon",
                                        fillColor : "rgba(220,151,151,0.2)",
					strokeColor : "rgba(220,151,151,1)",
					pointColor : "rgba(220,151,151,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,151,151,1)",
					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				}
			]

		};
		var lineChartData30 = {
			labels : ["January","February","March","April","May","June","July"],
			datasets : [
				{
					label: "Ebay",					
					fillColor : "rgba(151,187,205,0.2)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				},
				{
					label: "Amazon",
                                        fillColor : "rgba(220,151,151,0.2)",
					strokeColor : "rgba(220,151,151,1)",
					pointColor : "rgba(220,151,151,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,151,151,1)",
					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				}
			]

		};

    var ctx = document.getElementById("chart_sale").getContext("2d");
	window.onload = function(){
		window.myLine = new Chart(ctx).Line(lineChartData30, {
			responsive: true
		});
	};
        
        function loaddatasale(a){
		window.myLine = new Chart(ctx).Line(a, {
			responsive: true
		});
	};
        
        $('#sale_day1').click(function(){
            loaddatasale(lineChartData);
        });
        $('#sale_day7').click(function(){
            loaddatasale(lineChartData7);
        });
        $('#sale_day30').click(function(){
            loaddatasale(lineChartData30);
        });
        
        $('button').on('click', function(){
                $(this).parent().find('button').removeClass('active').addClass('btn-default').removeClass('btn-success');
                $(this).addClass('active').removeClass('btn-default').addClass('btn-success');               
        });
