<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from Common-Services Co., Ltd.
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the SARL SMC is strictly forbidden.
 * In order to obtain a license, please contact us: contact@common-services.com
 * ...........................................................................
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe Common-Services Co., Ltd.
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la Common-Services Co. Ltd. est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter Common-Services Co., Ltd. a l'adresse: contact@common-services.com
 *
 * @package   Jet.Com
 * @author    Olivier Baquet
 * @copyright Copyright (c) 2011-2015 Common Services Co Ltd - 90/25 Sukhumvit 81 - 10260 Bangkok - Thailand
 * @license   Commercial license
 *  Support by mail  :  support.jet@common-services.com
 */

require_once(dirname(__FILE__).'/env.php');
require_once(dirname(__FILE__).'/../jet.php');

require_once(dirname(__FILE__).'/../classes/jet.tools.class.php');
require_once(dirname(__FILE__).'/../classes/jet.product.class.php');
require_once(dirname(__FILE__).'/../classes/jet.context.class.php');
require_once(dirname(__FILE__).'/../classes/jet.form.class.php');
require_once(dirname(__FILE__).'/../classes/jet.support.class.php');
require_once(dirname(__FILE__).'/../classes/jet.batch.class.php');
require_once(dirname(__FILE__).'/../classes/jet.batches.class.php');

require_once(dirname(__FILE__).'/../classes/api/JetMarketplaceBase.php');
require_once(dirname(__FILE__).'/../classes/api/Transport/JetMarketplaceTransport.php');
require_once(dirname(__FILE__).'/../classes/api/Security/JetMarketplaceSecurity.php');
require_once(dirname(__FILE__).'/../classes/api/FileUpload/JetMarketplaceFileUpload.php');

/**
 * Class JetProducts
 */
class JetProducts extends Jet
{
    /**
     * @var array
     */
    public static $errors = array();
    /**
     * @var array
     */
    public static $warnings = array();
    /**
     * @var array
     */
    public static $messages = array();
    /**
     * @var array
     */
    public static $products = array();

    /**
     * @var int
     */
    public static $id_warehouse = 0;
    /**
     * @var string
     */
    public static $product_feed_type = Jet::FILE_MERCHANT_SKU;
    /**
     * @var null
     */
    public static $fullfillment_nodes = null;

    /**
     * @var
     */
    public $export;
    /**
     * @var string
     */
    private $ps_images;

    /**
     * @var int
     */
    protected $batch_timestart = 0;
    /**
     * @var int
     */
    protected $max_execution_time = 0;
    /**
     * @var int|string
     */
    protected $memory_limit = 0;
    /**
     * @var bool|int
     */
    protected $php_limits = 0;
    /**
     * @var int
     */
    protected $start_time = 0;

    /**
     * JetProducts constructor.
     *
     * @param $start_time
     */
    public function __construct()
    {
        parent::__construct();
        parent::loadGeneralModuleConfig();

        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            $cart = $this->context->cart;

            $current_currency = Currency::getDefaultCurrency();
            $cart->id_currency = $current_currency->id_currency;
        } else {
            $employee = null;

            $id_employee = Configuration::get('JET_ID_EMPLOYEE');

            if ($id_employee) {
                $employee = new Employee($id_employee);
            } else {
                $employee = new Employee(1);
            }

            if (!Validate::isLoadedObject($employee)) {
                die($this->l('Wrong Employee, please save the module configuration'));
            }
            if (version_compare(_PS_VERSION_, '1.5', '>=')) {
                $id_group = Configuration::get('PS_CUSTOMER_GROUP');
            } else {
                $id_group = (int)_PS_DEFAULT_CUSTOMER_GROUP_;
            }
            $this->context->customer->is_guest = true;
            $this->context->customer->id_default_group = $id_group;
            $this->context->cart = new Cart();
            $this->context->employee = $employee;
            $this->context->currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
        }

        $this->batch_timestart = time();

        $this->max_execution_time = (int)ini_get('max_execution_time');
        $this->memory_limit = ini_get('memory_limit');
        $this->php_limits = $this->max_execution_time || $this->memory_limit ? true : false;
        $this->start_time = microtime(true);

        if (Tools::getValue('debug') || Jet::$debug_mode) {
            $this->debug = true;
            @ini_set('display_errors', 'on');
            @error_reporting(E_ALL | E_STRICT);
        }
        $this->ps_images = (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').__PS_BASE_URI__.'img/p/';
        JetContext::restore($this->context);
    }

    /**
     *
     */
    public static function jsonDisplayExit()
    {
        $result = trim(ob_get_clean());

        if (!empty($result)) {
            JetProducts::$warnings[] = trim($result);
        }

        $data = array(
            'count' => count(JetProducts::$products),
            'products' => JetProducts::$products,
            'error' => (count(JetProducts::$errors) ? true : false),
            'errors' => JetProducts::$errors,
            'warning' => (count(JetProducts::$warnings) ? true : false),
            'warnings' => JetProducts::$warnings,
            'message' => count(JetProducts::$messages),
            'messages' => JetProducts::$messages
        );


        $json = Tools::jsonEncode(JetTools::fixEncoding($data));

        if (($callback = Tools::getValue('callback'))) {
            echo (string)$callback.'('.$json.')';
        } else {
            echo "<pre>\n";
            echo JetTools::jsonPrettyPrint($json);
            echo "<pre>\n";
        }
    }

    /**
     *
     */
    public function dispatch()
    {
        ob_start();
        register_shutdown_function(array('JetProducts', 'jsonDisplayExit'));

        //  Check Access Tokens
        //
        $jet_token = Configuration::get('JET_PS_TOKEN', null, 0, 0);

        if ($jet_token !== Tools::getValue('jet_token', rand())) {
            self::$errors[] = $this->l('Wrong Token');
            die;
        }

        $this->export = $this->path.'export/';

        switch ($action = Tools::getValue('action')) {
            case 'check':
            case 'export':
                $this->productsList(false, $action);
                break;
            case 'cron':
                $this->productsList(true, $action);
                break;
            default:
                self::$errors[] = 'Missing Parameter';
                die;
        }
    }


    /**
     * @return null
     */
    private function cleanup()
    {
        $now = time();

        $output_dir = sprintf('%s/', rtrim($this->export, '/'));

        if (!is_dir($output_dir)) {
            return null;
        }

        $files = glob($output_dir.'*.json');


        if (!is_array($files)) {
            $files = array();
        }


        $gzs = glob($output_dir.'*.gz');

        if (is_array($gzs)) {
            $files = array_merge($files, $gzs);
        }

        if (!is_array($files) || !count($files)) {
            return null;
        }

        // Sort by date
        foreach ($files as $file) {
            if (filemtime($file) < $now - (86400 * 3)) {
                unlink($file);
            }
        }
    }

    /**
     * @param bool|false $cron
     * @param string $action
     */
    public function productsList($cron = false, $action = 'export')
    {
        $product_feed_type = Tools::getValue('product_feed_type');

        if ($product_feed_type && in_array($product_feed_type, array(Jet::FILE_MERCHANT_SKU, Jet::FILE_VARIATION, Jet::FILE_PRICE, Jet::FILE_INVENTORY))) {
            self::$product_feed_type = $product_feed_type;
        } else {
            die('Unknow Feed Type');
        }

        if ($cron) {
            $batches = new JetBatches('JET_BATCH_CREATE');
            if ($last_export = $batches->getLast(self::$product_feed_type)) {
                $date_create_from = strstr($last_export, ' ', true);
                $date_create_to = date('Y-m-d');
            } else {
                $date_create_from = strstr(JetProduct::oldest(), ' ', true);
                $date_create_to = date('Y-m-d');
            }
        } else {
            $date_create_from = Tools::getValue('date-create-from', null);
            $date_create_to = Tools::getValue('date-create-to', null);
        }

        $count_products = 0;
        $count_combinations = 0;
        $i = 0;


        if ($date_create_from) {
            $date_create_from .= ' 00:00:00';
        }

        if ($date_create_to) {
            $date_create_to .= ' 23:59:59';
        }

        $loop_start_time = microtime(true);

        $config_parameters = parent::getConfig('JET_PARAMETERS');
        $config_filters = parent::getConfig('JET_FILTERS');

        if (empty($this->config['credentials']['user']) || empty($this->config['credentials']['secret'])) {
            self::$errors[] = sprintf($this->l('You must configure your keypairs first'), $this->export);
            die;
        }

        $export_method = $config_parameters['import_method'] == 'ID' ? 'ID' : 'SKU';

        $private_comment = sprintf(
            '%s %s',
            $this->l('Product creation from'),
            JetTools::getFriendlyUrl(Configuration::get('PS_SHOP_NAME'))
        );

        $id_warehouse = (int)$config_parameters['warehouse'] ? $config_parameters['warehouse'] : null;

        if (!is_dir($this->export)) {
            if (!mkdir($this->export)) {
                self::$errors[] = sprintf($this->l('Unable to create directory: %s'), $this->export);
                die;
            }
        }

        if (!JetTools::isDirWriteable($this->export)) {
            self::$errors[] = sprintf($this->l('Unable to write in the directory: %s'), $this->export);
            die;
        }

        $this->cleanup();

        $fullfillment_nodes = JetTools::unSerialize(Configuration::get('JET_FULFILLMENT_NODES'), true);

        if ($action == 'export' && !is_array($fullfillment_nodes) || !count($fullfillment_nodes)) {
            self::$errors[] = sprintf(
                $this->l(
                    'You must at least to perform a connection test with success in authentication tab from the module configuration one time to be able to send a feed !'
                )
            );
            die;
        }
        self::$fullfillment_nodes = $fullfillment_nodes;

        $file_basename = JetTools::getFriendlyUrl(Configuration::get('PS_SHOP_NAME'));
        $file_date_time = date('Ymd-His');

        if (self::$product_feed_type == Jet::FILE_MERCHANT_SKU) {
            $filename = $this->export.sprintf('%s-products-%s.json', $file_date_time, $file_basename);
        } elseif (self::$product_feed_type == Jet::FILE_VARIATION) {
            $filename = $this->export.sprintf('%s-variations-%s.json', $file_date_time, $file_basename);
        } elseif (self::$product_feed_type == Jet::FILE_PRICE) {
            $filename = $this->export.sprintf('%s-prices-%s.json', $file_date_time, $file_basename);

            $current_currency = Currency::getDefaultCurrency();
            if ($current_currency->iso_code != 'USD') {
                die('Currency conversion is not yet implemented, please contact us.');
            }
        } elseif (self::$product_feed_type == Jet::FILE_INVENTORY) {
            $filename = $this->export.sprintf('%s-inventory-%s.json', $file_date_time, $file_basename);
        }

        $active = Tools::getValue('active', true);
        $in_stock = Tools::getValue('in-stock', true);

        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            $get_combination = 'getAttributeCombinaisons';
        } else {
            $get_combination = 'getAttributeCombinations';
        }

        $profiles = JetProfiles::getAll();

        if (!is_array($profiles) || !count($profiles)) {
            self::$errors[] = sprintf($this->l('You must configure profiles first'));
            die;
        }

        $id_categories_checked = JetCategories::getAll();

        if (!is_array($id_categories_checked) || !count($id_categories_checked)) {
            self::$errors[] = sprintf($this->l('You must select categories first'));
            die;
        }

        // FORM: profile2category[id_category] = profile_name
        $profile2category = JetProfiles2Categories::getAll();

        if (!is_array($profile2category) || !count($profile2category)) {
            self::$errors[] = sprintf($this->l('You must select profiles first'));
            die;
        }

        //Remove unselected profiles2categories
        foreach ($profile2category as $key => $val) {
            if (!in_array($key, $id_categories_checked)) {
                unset($profile2category[$key]);
            }
        }

        //Reindex Profiles
        foreach ($profiles as $key => $val) {
            unset($profiles[$key]);
            $val['profile_id'] = $key;
            $profiles[$val['name']] = $val;
        }


        $products_history = array();
        $ean_history = array();
        $sku_history = array();
        $jet_product_count = 0;
        $count_export = 0;

        foreach (array_unique($profile2category) as $profile_name) {
            // Get id_categories for this profile
            $id_categories = array_keys($profile2category, $profile_name);

            if (!array_key_exists($profile_name, $profiles)) {
                continue;
            }

            $profile = $profiles[$profile_name];
            $profile['name_with_attributes'] = true;

            if ($this->debug) {
                echo '<hr />';
                printf('%s(#%d): New Profile: ', basename(__FILE__), __LINE__, $profile['name']);
                var_dump($profile);
            }

            if (in_array(self::$product_feed_type, array(Jet::FILE_MERCHANT_SKU, Jet::FILE_VARIATION))) {
                $products = JetProduct::getCreateProducts(
                    $id_categories,
                    $active,
                    $in_stock,
                    $date_create_from,
                    $date_create_to,
                    $this->debug
                );
            } else {
                $products = JetProduct::getUpdateProducts(
                    $id_categories,
                    $active,
                    $in_stock,
                    $date_create_from,
                    $date_create_to,
                    $this->debug
                );
            }

            if (!count($products) && $this->debug) {
                printf(
                    '%s(#%d): No Products for: %s/%s',
                    basename(__FILE__),
                    __LINE__,
                    $profile_name,
                    implode(',', $id_categories)
                );
                continue;
            }

            foreach ($products as $product_list_entry) {
                if ($this->php_limits) {
                    $loop_average = (microtime(true) - $loop_start_time) / ++$i;
                    if ($this->max_execution_time && (($loop_start_time - $this->start_time) + $loop_average * $i * 1.3) >= $this->max_execution_time) {
                        self::$errors[] = $this->l('PHP max_execution_time is about to be reached, process interrupted.');
                        die;
                    }
                    if ($this->memory_limit != -1 && memory_get_peak_usage() * 1.3 > Tools::convertBytes($this->memory_limit)) {
                        self::$errors[] = $this->l('PHP memory_limit is about to be reached, process interrupted.');
                        die;
                    }
                }

                $id_product = (int)$product_list_entry['id_product'];

                if (isset($products_history[$id_product])) {
                    continue;
                }
                $products_history[$id_product] = true;

                $product = new Product($id_product, false, $this->id_lang);

                if (!Validate::isLoadedObject($product)) {
                    continue;
                }

                // Filtering Manufacturer & Supplier
                //
                if ($product->id_manufacturer) {
                    if (is_array($config_filters['manufacturers']) && in_array($product->id_manufacturer, $config_filters['manufacturers'])) {
                        continue;
                    }
                }
                if ($product->id_supplier) {
                    if (is_array($config_filters['suppliers']) && in_array($product->id_supplier, $config_filters['suppliers'])) {
                        continue;
                    }
                }

                if ($export_method == 'SKU') {
                    $reference = trim($product->reference);
                } else {
                    $reference = $id_product;
                }

                $manufacturer_name = null;

                if (self::$product_feed_type == Jet::FILE_MERCHANT_SKU) {
                    $manufacturer_name = Manufacturer::getNameById((int)$product->id_manufacturer);
                }

                JetProducts::$products[$id_product] = array();
                JetProducts::$products[$id_product][0]['id_product'] = $id_product;
                JetProducts::$products[$id_product][0]['reference'] = $reference;
                JetProducts::$products[$id_product][0]['active'] = (bool)$product->active;
                JetProducts::$products[$id_product][0]['ean'] = (int)$product->ean13 ? $product->ean13 : null;
                JetProducts::$products[$id_product][0]['upc'] = (int)$product->upc ? sprintf('%013s', $product->upc) : null;
                JetProducts::$products[$id_product][0]['condition'] = $product->condition;
                JetProducts::$products[$id_product][0]['name'] = $product->name;
                JetProducts::$products[$id_product][0]['status'] = null;
                JetProducts::$products[$id_product][0]['id_category'] = $product->id_category_default;
                JetProducts::$products[$id_product][0]['export'] = true;
                JetProducts::$products[$id_product][0]['has_children'] = false;
                JetProducts::$products[$id_product][0]['supplier_name'] = null;
                JetProducts::$products[$id_product][0]['supplier_reference'] = null;
                JetProducts::$products[$id_product][0]['manufacturer_name'] = null;
                JetProducts::$products[$id_product][0]['weight'] = null;

                $images = array();

                foreach (JetTools::getProductImages($id_product, null, $this->id_lang) as $image) {
                    $file_image = _PS_PROD_IMG_DIR_.$image;

                    if (!file_exists($file_image)) {
                        self::$warnings[] = sprintf($this->l('Unable to find image %s in %s'), $image, _PS_PROD_IMG_DIR_);
                        //continue;
                    }

                    $images[] = $this->ps_images.$image;
                }
                if (count($images)) {
                    JetProducts::$products[$id_product][0]['product_images'] = $images;
                }

                // Products Option
                //
                $product_options = JetProduct::getProductOptions($id_product, null, $this->id_lang);

                if (is_array($product_options) && count($product_options)) {
                    $product_option = reset($product_options);
                } else {
                    $product_option = array_fill_keys(JetProduct::getProductOptionFields(), null);
                }

                if ($this->debug) {
                    echo '<hr />';
                    printf('%s(#%d): Product: ', basename(__FILE__), __LINE__, JetProducts::$products[$id_product]);
                    printf('%s(#%d): Product Options: ', basename(__FILE__), __LINE__, $product_option);
                }
                $quantity_override = null;
                $price_override = null;

                $disabled = (bool)$product_option['disable'];
                $force = (int)$product_option['force'];
                $comment = $product_option['text'] ? trim(Tools::substr($product_option['text'], 0, 200)) : $this->default_comment;

                if ($disabled) {
                    $product->active = false;
                    $quantity_override = 0;
                } elseif ($force) {
                    $product->active = true;
                    $quantity_override = $force;
                }

                if (!empty($product_option['price']) && is_numeric((float)$product_option['price'])) {
                    $price_override = (float)$product_option['price'];
                }


                // Children
                if ($product->hasAttributes()) {
                    $combinations = $product->{$get_combination}($this->id_lang);
                    $count_products++;

                    JetProducts::$products[$id_product][0]['has_children'] = true;

                    foreach ($combinations as $key => $combination) {
                        $id_product_attribute = (int)$combination['id_product_attribute'];

                        $combination_options = JetProduct::getProductOptions($id_product, $id_product_attribute, $this->id_lang);

                        if (is_array($combination_options) && count($combination_options)) {
                            $combination_option = reset($combination_options);
                        } else {
                            $combination_option = $product_option;
                        }

                        $quantity_override = null;
                        $disabled = (bool)$combination_option['disable'];
                        $price_override = (float)$combination_option['price'] ? (float)$combination_option['price'] : null;
                        $quantity_override = (int)$combination_option['force'] ? (int)$combination_option['force'] : null;
                        $comment = $combination_option['text'] ? trim(Tools::substr($combination_option['text'], 0, 200)) : $this->default_comment;

                        if ($disabled) {
                            $product->active = false;
                            $quantity_override = 0;
                        } elseif ($force) {
                            $product->active = true;
                            $quantity_override = $force;
                        }

                        if ($export_method == 'SKU') {
                            $combination_reference = trim($combination['reference']);
                        } else {
                            $combination_reference = sprintf('%d_%d', $id_product, $id_product_attribute);
                        }

                        if (!empty($combination_reference)) {
                            $product_identifier = sprintf('%s "%s"', $this->l('Product'), $combination_reference);
                        } else {
                            $product_identifier = sprintf('%s: %d/%d', $this->l('Product ID/Combination'), $id_product, $id_product_attribute);
                        }

                        if (!isset(JetProducts::$products[$id_product][$id_product_attribute])) {
                            $duplicate = false;
                            $code_check = true;

                            $product_code = Tools::strlen($combination['upc']) ? $combination['upc'] : $combination['ean13'];

                            if (!(int)$product_code || !(int)$product_code) {
                                self::$warnings[] = sprintf('%s - %s', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_MISSING_EAN, $this->l('Missing EAN/UPC Code')));
                                $code_check = false;
                            }

                            if ((int)$product_code && !JetTools::codeCheck($product_code) || JetTools::codeIsPrivate($product_code)) {
                                self::$warnings[] = sprintf('%s - %s - "%s"', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_WRONG_EAN, $this->l('Wrong EAN/UPC Code')), $product_code);
                                $code_check = false;
                            }

                            if ((int)$product_code) {
                                if (isset($ean_history[$product->condition.$product_code])) {
                                    self::$warnings[] = sprintf('%s - %s - "%s"', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_DUPLICATE_EAN, $this->l('Duplicated EAN/UPC Code')), $product_code);
                                    $duplicate = true;
                                }
                                $ean_history[$product->condition.$product_code] = true;
                            }

                            if ($export_method == 'SKU') {
                                if (!empty($combination_reference) && isset($sku_history[$combination_reference])) {
                                    self::$warnings[] = sprintf('%s - %s - "%s"', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_DUPLICATE, $this->l('Duplicated Reference')), $combination_reference);
                                    $duplicate = true;
                                }
                                $sku_history[$combination_reference] = true;
                            }

                            JetProducts::$products[$id_product][$id_product_attribute] = array();
                            JetProducts::$products[$id_product][$id_product_attribute]['export'] = true;
                            JetProducts::$products[$id_product][$id_product_attribute]['id_product'] = $id_product;
                            JetProducts::$products[$id_product][$id_product_attribute]['reference'] = trim($product->reference);
                            JetProducts::$products[$id_product][$id_product_attribute]['combination_reference'] = $combination_reference;
                            JetProducts::$products[$id_product][$id_product_attribute]['combination_ean'] = (int)$combination['ean13'] ? sprintf('%013s', $combination['ean13']) : null;
                            JetProducts::$products[$id_product][$id_product_attribute]['combination_upc'] = (int)$combination['upc'] ? sprintf('%012s', $combination['upc']) : null;
                            JetProducts::$products[$id_product][$id_product_attribute]['condition'] = $product->condition;
                            JetProducts::$products[$id_product][$id_product_attribute]['attributes_list'] = null;
                            JetProducts::$products[$id_product][$id_product_attribute]['has_attributes'] = true;
                            JetProducts::$products[$id_product][$id_product_attribute]['name'] = $product->name;
                            JetProducts::$products[$id_product][$id_product_attribute]['weight'] = $product->weight;
                            JetProducts::$products[$id_product][$id_product_attribute]['id_category'] = $product->id_category_default;

                            $supplier_reference = null;
                            $supplier_name = null;

                            if (self::$product_feed_type == Jet::FILE_MERCHANT_SKU && $product->id_supplier) {
                                if (version_compare(_PS_VERSION_, '1.5', '>=')) {
                                    $supplier_reference = ProductSupplier::getProductSupplierReference($id_product, $id_product_attribute, $product->id_supplier);
                                    $supplier = new Supplier($product->id_supplier, $this->id_lang);
                                    if (Validate::isLoadedObject($supplier)) {
                                        $supplier_name = $supplier->name;
                                    }
                                } else {
                                    $supplier_reference = $product->supplier_reference;
                                }
                            }

                            if ($quantity_override === null) {
                                if (version_compare(_PS_VERSION_, '1.5', '<')) {
                                    $quantity = Product::getQuantity((int)$id_product, $id_product_attribute);
                                } else {
                                    $quantity = Product::getRealQuantity($id_product, $id_product_attribute, $id_warehouse);
                                }

                                $outofstock
                                    = isset($config_filters['outofstock']) && (int)$config_filters['outofstock'] ? (int)$config_filters['outofstock'] : 0;
                                if ($outofstock > $quantity) {
                                    $quantity = 0;
                                }
                            } else {
                                $quantity = $quantity_override;
                            }

                            JetProducts::$products[$id_product][$id_product_attribute]['quantity'] = $quantity > -1 ? $quantity : 0;

                            if ($this->config['credentials']['test']) {
                                JetProducts::$products[$id_product][$id_product_attribute]['status'] = 'active';
                            } elseif ($duplicate) {
                                JetProducts::$products[$id_product][$id_product_attribute]['export'] = false;
                                JetProducts::$products[$id_product][$id_product_attribute]['status'] = 'na';
                            } elseif (!$code_check) {
                                JetProducts::$products[$id_product][$id_product_attribute]['export'] = false;
                                JetProducts::$products[$id_product][$id_product_attribute]['status'] = 'na';
                            } elseif (empty(JetProducts::$products[$id_product][$id_product_attribute]['combination_reference'])) {
                                JetProducts::$products[$id_product][$id_product_attribute]['export'] = false;
                                JetProducts::$products[$id_product][$id_product_attribute]['status'] = 'na';
                                self::$warnings[] = sprintf('%s - %s', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_EMPTY_REFERENCE, $this->l('Missing Reference')));
                            } elseif ($quantity && $product->active) {
                                JetProducts::$products[$id_product][$id_product_attribute]['status'] = 'active';
                            } elseif (!$product->active) {
                                JetProducts::$products[$id_product][$id_product_attribute]['status'] = 'inactive';
                            } elseif (!$quantity) {
                                JetProducts::$products[$id_product][$id_product_attribute]['export'] = false;
                                JetProducts::$products[$id_product][$id_product_attribute]['status'] = 'oos';
                            } else {
                                JetProducts::$products[$id_product][$id_product_attribute]['status'] = 'na';
                            }

                            if (JetProducts::$products[$id_product][$id_product_attribute]['export']) {
                                $count_export++;
                            }

                            // List Price
                            $regular_price = $product->getPrice($config_parameters['taxes'], $id_product_attribute, 2, null, false, false);

                            JetProducts::$products[$id_product][$id_product_attribute]['price'] = sprintf('%.02f', Tools::ps_round($regular_price, 2));

                            $new_price = null;
                            if (!$price_override) {
                                $price = $product->getPrice($config_parameters['taxes'], $id_product_attribute, 2, null, false, $product->on_sale || $config_parameters['specials']);
                                if (array_key_exists('price_rule', $profile) && is_array($profile['price_rule'])) {
                                    $new_price = JetTools::priceRule($price, $profile['price_rule']);
                                }
                            } else {
                                $price = $price_override;
                            }

                            $images = array();

                            foreach (JetTools::getProductImages($id_product, $id_product_attribute, $this->id_lang) as $image) {
                                $file_image = _PS_PROD_IMG_DIR_.$image;
                                if (file_exists($file_image)) {
                                    $images[] = $this->ps_images.$image;
                                } else {
                                    self::$warnings[] = sprintf($this->l('Unable to find image %s in %s'), $image, _PS_PROD_IMG_DIR_);
                                }
                            }
                            if (count($images)) {
                                JetProducts::$products[$id_product][$id_product_attribute]['product_images'] = $images;
                            }

                            JetProducts::$products[$id_product][0]['supplier_name'] = $supplier_name;
                            JetProducts::$products[$id_product][0]['manufacturer_name'] = $manufacturer_name;
                            JetProducts::$products[$id_product][$id_product_attribute]['manufacturer_name'] = $manufacturer_name;
                            JetProducts::$products[$id_product][$id_product_attribute]['supplier_reference'] = $supplier_reference;
                            JetProducts::$products[$id_product][$id_product_attribute]['supplier_name'] = $supplier_name;
                            JetProducts::$products[$id_product][$id_product_attribute]['final_price'] = sprintf('%.02f', Tools::ps_round($new_price ? $new_price : $price, 2));
                            JetProducts::$products[$id_product][$id_product_attribute]['comment'] = $comment;
                            JetProducts::$products[$id_product][$id_product_attribute]['private_comment'] = $private_comment;
                            $count_combinations++;
                        }
                        $id_attribute = (int)$combination['id_attribute'];
                        JetProducts::$products[$id_product][$id_product_attribute]['attributes'][$id_attribute]['id_attribute_group'] = $combination['id_attribute_group'];
                        JetProducts::$products[$id_product][$id_product_attribute]['attributes'][$id_attribute]['group_name'] = $combination['group_name'];
                        JetProducts::$products[$id_product][$id_product_attribute]['attributes'][$id_attribute]['attribute_name'] = $combination['attribute_name'];
                        JetProducts::$products[$id_product][$id_product_attribute]['attributes'][$id_attribute]['is_color_group'] = $combination['is_color_group'];

                        if (JetProducts::$products[$id_product][$id_product_attribute]['attributes_list']) {
                            JetProducts::$products[$id_product][$id_product_attribute]['attributes_list'] .= ' - '.$combination['attribute_name'];
                        } else {
                            JetProducts::$products[$id_product][$id_product_attribute]['attributes_list'] = $combination['attribute_name'];
                        }

                        JetProducts::$products[$id_product][$id_product_attribute]['weight'] += $combination['weight'];

                        if (isset($profile['name_with_attributes']) && $profile['name_with_attributes'] && JetProducts::$products[$id_product][$id_product_attribute]['attributes_list']) {
                            JetProducts::$products[$id_product][$id_product_attribute]['name'] .= ' - '.$combination['attribute_name'];
                        }
                    }
                } else {
                    JetProducts::$products[$id_product][0]['export'] = true;
                    JetProducts::$products[$id_product][0]['has_attributes'] = false;

                    if ($quantity_override === null) {
                        if (version_compare(_PS_VERSION_, '1.5', '<')) {
                            $quantity = Product::getQuantity((int)$id_product, null);
                        } else {
                            $quantity = Product::getRealQuantity($id_product, null, $id_warehouse);
                        }
                    } else {
                        $quantity = $quantity_override;
                    }

                    JetProducts::$products[$id_product][0]['quantity'] = $quantity > -1 ? $quantity : 0;

                    if (!empty($combination_reference)) {
                        $product_identifier = sprintf('%s "%s"', $this->l('Product'), $reference);
                    } else {
                        $product_identifier = sprintf('%s: %d', $this->l('Product ID'), $id_product);
                    }

                    $duplicate = false;
                    $code_check = true;

                    $product_code = Tools::strlen($product->upc) ? $product->upc : $product->ean13;

                    if (!(int)$product_code || !(int)$product_code) {
                        self::$warnings[] = sprintf('%s - %s', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_MISSING_EAN, $this->l('Missing EAN/UPC Code')));
                        $code_check = false;
                    }

                    if ((int)$product_code && !JetTools::codeCheck($product_code) || JetTools::codeIsPrivate($product_code)) {
                        self::$warnings[] = sprintf('%s - %s - "%s"', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_WRONG_EAN, $this->l('Wrong EAN/UPC Code')), $product_code);
                        $code_check = false;
                    }

                    if ((int)$product_code) {
                        if (isset($ean_history[$product->condition.$product_code])) {
                            self::$warnings[] = sprintf('%s - %s - "%s"', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_DUPLICATE_EAN, $this->l('Duplicated EAN/UPC Code')), $product_code);
                            $duplicate = true;
                        }
                        $ean_history[$product->condition.$product_code] = true;
                    }

                    if (empty($manufacturer_name)) {
                        if (isset($ean_history[$product->condition.$product_code])) {
                            self::$warnings[] = sprintf('%s - %s - "%s"', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_MISSING_MANUFACTURER, $this->l('Missing Manufacturer')), $product_code);
                            $duplicate = true;
                        }
                        $ean_history[$product->condition.$product_code] = true;
                    }

                    if ($export_method == 'SKU') {
                        if (!empty($reference) && isset($sku_history[$reference])) {
                            self::$warnings[] = sprintf('%s - %s - "%s"', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_DUPLICATE, $this->l('Duplicated Reference')), $reference);
                            $duplicate = true;
                        }
                        $sku_history[$reference] = true;
                    }

                    if ($this->config['credentials']['test']) {
                        JetProducts::$products[$id_product][0]['status'] = 'active';
                    } elseif ($duplicate) {
                        JetProducts::$products[$id_product][0]['export'] = false;
                        JetProducts::$products[$id_product][0]['status'] = 'na';
                    } elseif (!$code_check) {
                        JetProducts::$products[$id_product][0]['export'] = false;
                        JetProducts::$products[$id_product][0]['status'] = 'na';
                    } elseif (empty(JetProducts::$products[$id_product][0]['reference'])) {
                        JetProducts::$products[$id_product][0]['export'] = false;
                        JetProducts::$products[$id_product][0]['status'] = 'na';
                        self::$warnings[] = sprintf('%s - %s', $product_identifier, JetSupport::message(JetSupport::FUNCTION_EXPORT_EMPTY_REFERENCE, $this->l('Missing Reference')));
                    } elseif ($quantity && $product->active) {
                        JetProducts::$products[$id_product][0]['status'] = 'active';
                    } elseif (!$product->active) {
                        JetProducts::$products[$id_product][0]['status'] = 'inactive';
                    } elseif (!$quantity) {
                        JetProducts::$products[$id_product][0]['export'] = false;
                        JetProducts::$products[$id_product][0]['status'] = 'oos';
                    } else {
                        JetProducts::$products[$id_product][0]['status'] = 'na';
                    }

                    if (JetProducts::$products[$id_product][0]['export']) {
                        $count_export++;
                    }

                    // List Price
                    $regular_price = $product->getPrice($config_parameters['taxes'], null, 2, null, false, false);
                    $new_price = null;

                    JetProducts::$products[$id_product][0]['price'] = $regular_price;

                    if (!$price_override) {
                        $price = $product->getPrice($config_parameters['taxes'], null, 2, null, false, $product->on_sale || $config_parameters['specials']);
                        if (array_key_exists('price_rule', $profile) && is_array($profile['price_rule'])) {
                            $new_price = JetTools::priceRule($price, $profile['price_rule']);
                        }
                    } else {
                        $price = $price_override;
                    }

                    $images = array();

                    foreach (JetTools::getProductImages($id_product, null, $this->id_lang) as $image) {
                        $file_image = _PS_PROD_IMG_DIR_.$image;

                        if (!file_exists($file_image)) {
                            self::$warnings[] = sprintf($this->l('Unable to find image %s in %s'), $image, _PS_PROD_IMG_DIR_);
                            //continue;
                        }

                        $images[] = $this->ps_images.$image;
                    }
                    if (count($images)) {
                        JetProducts::$products[$id_product][0]['product_images'] = $images;
                    }


                    $supplier_reference = null;
                    $supplier_name = null;

                    if (self::$product_feed_type == Jet::FILE_MERCHANT_SKU && $product->id_supplier) {
                        if (version_compare(_PS_VERSION_, '1.5', '>=')) {
                            $supplier_reference = ProductSupplier::getProductSupplierReference($id_product, 0, $product->id_supplier);
                            $supplier = new Supplier($product->id_supplier, $this->id_lang);
                            if (Validate::isLoadedObject($supplier)) {
                                $supplier_name = $supplier->name;
                            }
                        } else {
                            $supplier_reference = $product->supplier_reference;
                        }
                    }

                    JetProducts::$products[$id_product][0]['manufacturer_name'] = $manufacturer_name;
                    JetProducts::$products[$id_product][0]['supplier_reference'] = $supplier_reference;
                    JetProducts::$products[$id_product][0]['supplier_name'] = $supplier_name;
                    JetProducts::$products[$id_product][0]['weight'] = $product->weight;
                    JetProducts::$products[$id_product][0]['combination_reference'] = $reference;

                    JetProducts::$products[$id_product][0]['combination_ean'] = (int)$product->ean13 ? $product->ean13 : null;
                    JetProducts::$products[$id_product][0]['combination_upc'] = (int)$product->upc ? sprintf('%013s', $product->upc) : null;
                    JetProducts::$products[$id_product][0]['final_price'] = sprintf('%.02f', Tools::ps_round($new_price ? $new_price : $price, 2));
                    JetProducts::$products[$id_product][0]['comment'] = $comment;
                    JetProducts::$products[$id_product][0]['private_comment'] = $private_comment;

                    $count_products++;
                }
                if (self::$product_feed_type == Jet::FILE_MERCHANT_SKU) {
                    $description = JetTools::cleanStripTags($product->description);
                    if (Tools::strlen($description) > Jet::LENGTH_DESCRIPTION) {
                        $product->description = trim($description);
                    } else {
                        $product->description = sprintf('%s...', trim(Tools::substr($description, 0, Jet::LENGTH_DESCRIPTION)));
                    }
                }

                $jet_product_count += $this->convertForJet($profile, $product, JetProducts::$products[$id_product]);
            }
        }
        $json = null;
        $pass = false;

        if ($jet_product_count) {
            $json = $this->toJetJson();
            $pass = true;
        }

        if ($pass && file_put_contents($filename, $json) === false) {
            self::$messages[] = sprintf('%s: %S', $this->l('Unable to write in the outpout file'), $filename);
            $pass = false;
        }

        $jet_token = Configuration::get('JET_PS_TOKEN', null, 0, 0);
        $export_url = sprintf('%sfunctions/download.php?jet_token=%s', $this->url, $jet_token);
        $href_link = sprintf('<a href="%s&filename=%s" target="_blank">%s/%s</a>', $export_url, basename($filename), preg_replace('/(?<=^.{16}).{4,}(?=.{16}$)/', '...', $export_url), basename($filename));

        if ($count_products && $count_combinations && $jet_product_count) {
            self::$messages[] = sprintf('%s - %d %s, %d %s', $href_link, $count_products, $this->l('products'), $count_combinations, $this->l('combinations'));
        } elseif ($count_products && $jet_product_count) {
            self::$messages[] = sprintf('%s - %d %s', $href_link, $count_products, $this->l('products'));
        } else {
            $pass = false;
            self::$warnings[] = $this->l('Nothing to export');
        }

        if ($pass && ($action == 'export' || $cron)) {
            $jet_file_id = null;
            $jetMarketplaceFileUpload = null;

            if ((bool)$this->config['credentials']['test']) {
                $result = JetTools::getDemoData(__CLASS__, __FUNCTION__, 'success');

                if ($result) {
                    $jetMarketplaceFileUpload = JetTools::unSerialize($result, true);
                }
            } else {
                switch (self::$product_feed_type) {
                    case Jet::FILE_MERCHANT_SKU:
                        $jetMarketplaceFileUpload = $this->postFile($filename, Jet::FILE_MERCHANT_SKU);
                        break;
                    case Jet::FILE_VARIATION:
                        $jetMarketplaceFileUpload = $this->postFile($filename, Jet::FILE_VARIATION);
                        break;
                    case Jet::FILE_PRICE:
                        $jetMarketplaceFileUpload = $this->postFile($filename, Jet::FILE_PRICE);
                        break;
                    case Jet::FILE_INVENTORY:
                        $jetMarketplaceFileUpload = $this->postFile($filename, Jet::FILE_INVENTORY);
                        break;
                    default:
                        die('unknown error');
                }
            }

            if ($jetMarketplaceFileUpload instanceof JetMarketplaceFileUpload) {
                $jet_file_id = $jetMarketplaceFileUpload->jet_file_id;

                JetTools::saveDemoData(__CLASS__, __FUNCTION__, serialize($jetMarketplaceFileUpload), 'success');
            } elseif ($jetMarketplaceFileUpload instanceof JetMarketplaceError) {
                self::$errors[] = sprintf('%s(%d)- API Error: status: %s code: %s message: %s', basename(__FILE__), __LINE__, $jetMarketplaceFileUpload->status, $jetMarketplaceFileUpload->code, $jetMarketplaceFileUpload->message);

                return (false);
            }

            if ($jet_file_id) {
                $batches = new JetBatches('JET_BATCH_CREATE');
                $batch = new JetBatch($this->batch_timestart);
                $batch->id = $jet_file_id;
                $batch->type = self::$product_feed_type;
                $batch->timestop = time();
                $batch->created = 0;
                $batch->updated = $count_export;
                $batch->deleted = 0;
                $batch->file = basename($filename);
                $batches->add($batch);
                $batches->save();

                self::$messages[] = sprintf('%s: #<b>%s</b>', $this->l('File successfully submitted to Jet, Import ID'), $jet_file_id);
            } elseif (is_array($result) && array_key_exists('error', $result)) {
                $message = sprintf('API Error: %s - %s', $result['error']['code'], $result['error']['message']);
                if (array_key_exists('details', $result['error'])) {
                    $message .= ' - '.$result['error']['details']['detail'];
                }

                self::$errors[] = $message;
                die;
            } else {
                self::$errors[] = sprintf('API Error: %s', $this->l('Error while sending to Jet'));
                die;
            }
        }
    }

    /**
     * @param $filename
     * @param $type
     *
     * @return bool
     */
    private function postFile($filename, $type)
    {
        $user = trim($this->config['credentials']['user']);
        $secret = trim($this->config['credentials']['secret']);

        $jetMarketplace = new JetMarketplaceBase(Jet::$debug_mode);

        $config = $jetMarketplace->getConfig($user, $secret);

        if (Jet::$debug_mode) {
            echo "<pre>\n";
            printf('%s - %s::%s - line #%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
            echo "config: ".print_r($config, true);
            echo "</pre>\n";
        }

        if ($this->config['credentials']['test']) {
            $token = JetTools::getDemoData('jettools', 'auth');
        } else {
            $token = JetTools::auth($config);

            JetTools::saveDemoData('jettools', 'auth', $token);
        }

        if ($token == null) {
            self::$errors[] = sprintf('API Error: %s', $this->l('Unable to obtain a security token from Jet.com'));

            return (false);
        }
        $transport = new JetMarketplaceTransport(Jet::$debug_mode);
        $security = new JetMarketplaceSecurity($transport, $config);
        $security->setToken($token);

        $fileUploadClient = new JetMarketplaceFileUpload($transport, $config, $security->id_token);

        if ($this->config['credentials']['test']) {
            $result = JetTools::getDemoData(__CLASS__, __FUNCTION__, $type);

            if ($result) {
                $fileUploadClientResult = JetTools::unSerialize($result, true);
            } else {
                $fileUploadClientResult = null;
            }
        } else {
            // Get an upload token
            $fileUploadClientResult = $fileUploadClient->getUploadToken();

            JetTools::saveDemoData(__CLASS__, __FUNCTION__, serialize($fileUploadClientResult), $type);
        }

        if ($fileUploadClientResult instanceof JetMarketplaceFileUpload) {
            $url = $fileUploadClientResult->file_url;
            $alias = basename($filename);

            if ($this->config['credentials']['test']) {
                $result = JetTools::getDemoData(__CLASS__, __FUNCTION__, sprintf('%s-putFile', $type));

                if ($result) {
                    $fileUploadClientResult = JetTools::unSerialize($result, true);
                } else {
                    $fileUploadClientResult = null;
                }
            } else {
                // Send the file to the cloud
                $fileUploadClientResult = $fileUploadClient->putFile($url, $filename, $alias);

                JetTools::saveDemoData(__CLASS__, __FUNCTION__, serialize($fileUploadClientResult), sprintf('%s-putFile', $type));
            }


            if ($fileUploadClientResult instanceof JetMarketplaceFileUpload) {
                JetTools::saveDemoData(__CLASS__, __FUNCTION__, serialize($fileUploadClientResult), sprintf('%s-putFile', $type));

                if ($this->config['credentials']['test']) {
                    $result = JetTools::getDemoData(__CLASS__, __FUNCTION__, sprintf('%s-postUploaded', $type));

                    if ($result) {
                        $fileUploadClientResult = JetTools::unSerialize($result, true);
                    } else {
                        $fileUploadClientResult = null;
                    }
                } else {
                    // Notify Jet.com the file has been sent to the cloud
                    $fileUploadClientResult = $fileUploadClient->postUploaded($type);
                }

                if ($fileUploadClientResult instanceof JetMarketplaceFileUpload) {
                    JetTools::saveDemoData(__CLASS__, __FUNCTION__, serialize($fileUploadClientResult), sprintf('%s-postUploaded', $type));

                    if ($fileUploadClientResult->jet_file_id) {
                        return ($fileUploadClientResult);
                    } else {
                        self::$errors[] = sprintf('%s(%d)- API Error: status: %s code: %s message: %s', basename(__FILE__), __LINE__, $fileUploadClientResult->status, $fileUploadClientResult->code, $fileUploadClientResult->message);
                    }
                } else {
                    self::$errors[] = sprintf('%s(%d)- API Error: status: %s code: %s message: %s', basename(__FILE__), __LINE__, $fileUploadClientResult->status, $fileUploadClientResult->code, $fileUploadClientResult->message);

                    return (false);
                }
            } else {
                self::$errors[] = sprintf('%s(%d)- API Error: status: %s code: %s message: %s', basename(__FILE__), __LINE__, $fileUploadClientResult->status, $fileUploadClientResult->code, $fileUploadClientResult->message);

                return (false);
            }
        } else {
            self::$errors[] = sprintf('%s(%d)- API Error: status: %s code: %s message: %s', basename(__FILE__), __LINE__, $fileUploadClientResult->status, $fileUploadClientResult->code, $fileUploadClientResult->message);

            return (false);
        }

        return (false);
    }

    /**
     * @return bool|string
     */
    private function toJetJson()
    {
        $jet_results = array();

        switch (self::$product_feed_type) {
            case Jet::FILE_MERCHANT_SKU:
                if (is_array(JetProducts::$products) && count(JetProducts::$products)) {
                    foreach (JetProducts::$products as $products) {
                        if (is_array($products) && count($products)) {
                            foreach ($products as $id_product_combination => $product) {
                                if ($id_product_combination == 0 && isset($product['has_children']) && $product['has_children']) {
                                    continue;
                                }
                                if (!$product['export']) {
                                    continue;
                                }
                                if (!array_key_exists('jet_product', $product)) {
                                    continue;
                                }
                                $merchant_sku = trim($product['combination_reference']);
                                if (empty($merchant_sku)) {
                                    continue;
                                }
                                $jet_results[$merchant_sku] = $product['jet_product'];
                            }
                        }
                    }
                    if (Jet::$debug_mode) {
                        echo "<pre>\n";
                        printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                        printf('FILE_MERCHANT_SKU: %s'."\n", print_r($jet_results, true));
                        echo "</pre>";
                    }
                }

                break;

            case Jet::FILE_VARIATION:
                if (is_array(JetProducts::$products) && count(JetProducts::$products)) {
                    foreach (JetProducts::$products as $products) {
                        if (is_array($products) && count($products)) {
                            $product = reset($products);
                            if (array_key_exists('jet_variant', $product) && is_array($product['jet_variant']) && count($product['jet_variant'])) {
                                $jet_variant = reset($product['jet_variant']);
                                $parent_sku = key($product['jet_variant']);
                                $jet_results[$parent_sku] = $jet_variant;
                            }
                        }
                    }
                    if (Jet::$debug_mode) {
                        echo "<pre>\n";
                        printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                        printf('FILE_VARIATION: %s'."\n", print_r($jet_results, true));
                        echo "</pre>";
                    }
                }
                break;

            case Jet::FILE_PRICE:
                if (is_array(JetProducts::$products) && count(JetProducts::$products)) {
                    foreach (JetProducts::$products as $products) {
                        if (is_array($products) && count($products)) {
                            foreach ($products as $id_product_combination => $product) {
                                if ($id_product_combination == 0 && isset($product['has_children']) && $product['has_children']) {
                                    continue;
                                }
                                if (!$product['export']) {
                                    continue;
                                }
                                if (!array_key_exists('jet_price', $product)) {
                                    continue;
                                }
                                $merchant_sku = trim($product['combination_reference']);
                                if (empty($merchant_sku)) {
                                    continue;
                                }
                                $jet_results[$merchant_sku] = $product['jet_price'];
                            }
                        }
                    }
                    if (Jet::$debug_mode) {
                        echo "<pre>\n";
                        printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                        printf('FILE_PRICE: %s'."\n", print_r($jet_results, true));
                        echo "</pre>";
                    }
                }
                break;
            case Jet::FILE_INVENTORY:
                if (is_array(JetProducts::$products) && count(JetProducts::$products)) {
                    foreach (JetProducts::$products as $products) {
                        if (is_array($products) && count($products)) {
                            foreach ($products as $id_product_combination => $product) {
                                if ($id_product_combination == 0 && isset($product['has_children']) && $product['has_children']) {
                                    continue;
                                }
                                if (!$product['export']) {
                                    continue;
                                }
                                if (!array_key_exists('jet_quantity', $product)) {
                                    continue;
                                }
                                $merchant_sku = trim($product['combination_reference']);
                                if (empty($merchant_sku)) {
                                    continue;
                                }
                                $jet_results[$merchant_sku] = $product['jet_quantity'];
                            }
                        }
                    }
                    if (Jet::$debug_mode) {
                        echo "<pre>\n";
                        printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                        printf('FILE_INVENTORY: %s'."\n", print_r($jet_results, true));
                        echo "</pre>";
                    }
                }
                break;
        }

        if (count($jet_results)) {
            return (JetTools::jetJsonEncode($jet_results));
        }

        return (false);
    }

    /**
     * @param $profile
     * @param $product
     * @param $group_details
     *
     * @return int
     */
    private function convertForJet(&$profile, $product, &$group_details)
    {
        static $profiles = null, $models = null;
        static $features = null;
        static $features_values = null;

        if ($profiles === null) {
            $profiles = JetProfiles::getAll();
        }

        if ($models === null) {
            $models = JetModels::getAll();
        }

        if ($features_values === null) {
            $features = self::$features[$this->id_lang];
            $features_values = self::$features_values[$this->id_lang];
        }

        $profile_id = isset($profile['profile_id']) ? $profile['profile_id'] : -1;
        $model = false;
        $model_id = isset($profiles[$profile_id]['model']) ? $profiles[$profile_id]['model'] : -1;

        if (!isset($profiles[$profile_id]['model']) || is_null($profiles[$profile_id]['model'])) {
            self::$warnings[] = sprintf('%s: %s', $this->l('Missing model for profile'), $profiles[$profile_id]['name']);

            return (false);
        }

        if ($model_id === -1) {
            die($this->l('No Model found for profile id: '.$profile));
        }

        if (in_array(self::$product_feed_type, array(Jet::FILE_VARIATION, Jet::FILE_MERCHANT_SKU))) {
            $product_features = $product->getFeatures();

            $final_product_features = array();

            foreach ($product_features as $feature) {
                $id_feature = $feature['id_feature'];
                $id_feature_value = $feature['id_feature_value'];
                $feature_name = isset($features[$id_feature]) ? $features[$id_feature]['name'] : null;
                $feature_value
                    = isset($features_values[$id_feature][$id_feature_value]['value']) ? $features_values[$id_feature][$id_feature_value]['value'] : null;

                if (Tools::strlen($feature_name)) {
                    $feature['name'] = $feature_name;
                }

                if (Tools::strlen($feature_value)) {
                    $feature['value'] = $feature_value;
                    continue;
                }

                if ((bool)$feature['custom']) {
                    $custom_features_values = FeatureValue::getFeatureValuesWithLang($this->id_lang, $id_feature, true);

                    foreach ($custom_features_values as $custom_feature_value) {
                        if ($custom_feature_value['id_feature_value'] != $feature['id_feature_value']) {
                            continue;
                        }
                        $feature['value'] = $custom_feature_value['value'];
                        break;
                    }
                }
                $final_product_features[$id_feature] = $feature;
            }
            $group_details['features'] = $final_product_features;

            $models_list = &$models;

            if (isset($models_list) && isset($models_list[$model_id])) {
                $model = $models_list[$model_id];
            }

            if (!$model) {
                die($this->l('No Model Detailed Information found for profile id: '.$profile_id.' and Model Id: '.$model_id));
            }
        }

        switch (self::$product_feed_type) {
            case Jet::FILE_MERCHANT_SKU:
                return ($this->convertForJetProduct($profile, $product, $model, $group_details));
            case Jet::FILE_VARIATION:
                return ($this->convertForJetVariations($product, $model, $group_details));
            case Jet::FILE_PRICE:
                return ($this->convertForJetPrices($group_details));
            case Jet::FILE_INVENTORY:
                return ($this->convertForJetInventory($group_details));
            default:
                die('Unimplemented function');
        }
    }

    /**
     * @param $profile
     * @param $product
     * @param $model
     * @param $group_details
     *
     * @return int
     */
    private function convertForJetProduct(&$profile, $product, &$model, &$group_details)
    {
        $jet_product = array();

        // Export
        $index = 0;
        foreach ($group_details as $id_product_attribute => $combination) {
            $model['variant'] = array(); // filled by parseMapping

            if ($id_product_attribute == 0 && isset($combination['has_children']) && $combination['has_children']) {
                $is_parent = true;
            } else {
                $is_parent = false;
            }

            if ($is_parent) {
                continue;
            }

            if (!isset($combination['export']) || !(bool)$combination['export']) {
                continue;
            }

            $jet_product['product_title'] = $combination['name'];
            $jet_product['jet_browse_node_id'] = (int)$model['category'];
            $jet_product['jet_retail_sku'] = '';
            $jet_product['amazon_item_type_keyword'] = '';
            $jet_product['ASIN'] = '';
            $jet_product['category_path'] = JetTools::cPath($combination['id_category'], $this->id_lang);

            $standard_product_codes = array();

            if (!$is_parent) {
                if (Tools::strlen($combination['combination_ean'])) {
                    $standard_product_code = array('standard_product_code' => $combination['combination_ean'], 'standard_product_code_type' => 'EAN');
                    $standard_product_codes[] = $standard_product_code;
                }
                if (Tools::strlen($combination['combination_upc'])) {
                    $standard_product_code = array('standard_product_code' => $combination['combination_upc'], 'standard_product_code_type' => 'UPC');
                    $standard_product_codes[] = $standard_product_code;
                }
            }

            $jet_product['standard_product_codes'] = $standard_product_codes;
            $jet_product['multipack_quantity'] = 1;
            $jet_product['brand'] = $combination['manufacturer_name'];
            $jet_product['manufacturer'] = $combination['supplier_name']; //TODO: Manufacturer mapping
            $jet_product['mfr_part_number'] = $combination['supplier_reference'];

            if ((int)$product->unity) {
                $jet_product['number_units_for_price_per_unit'] = (int)$product->unity ? (int)$product->unity : 1;
            }

            $jet_product['type_of_unit_for_price_per_unit'] = 'each';

            if ((int)$combination['weight'] && is_numeric($combination['weight'])) {
                $jet_product['shipping_weight_pounds'] = (float)Tools::ps_round($combination['weight'], 2);
            }

            if ((int)$product->depth && is_numeric($product->depth)) {
                $jet_product['package_length_inches'] = (float)Tools::ps_round($product->depth, 2);
            }

            if ((int)$product->width && is_numeric($product->width)) {
                $jet_product['package_width_inches'] = (float)Tools::ps_round($product->width, 2);
            }

            if ((int)$product->height && is_numeric($product->height)) {
                $jet_product['package_height_inches'] = (float)Tools::ps_round($product->height, 2);
            }

            $jet_product['display_length_inches'] = '';
            $jet_product['display_width_inches'] = '';
            $jet_product['display_height_inches'] = '';
            $jet_product['prop_65'] = '';
            $jet_product['legal_disclaimer_description'] = '';
            $jet_product['cpsia_cautionary_statements'] = '';
            $jet_product['country_of_origin'] = '';
            $jet_product['safety_warning'] = '';

            //$jet_product['start_selling_date'] = date(sprintf('Y-m-d\TH:i:s%sP', Tools::substr(microtime(), 1, 8)));
            $jet_product['start_selling_date'] = jetTools::jetDate();

            // Restock Date > Pre-Sales
            //
            if (version_compare(_PS_VERSION_, '1.5', '>=') && Validate::isDate($product->available_date)) {
                $dateNow = time();
                $psDateRestock = strtotime($product->available_date);

                if ($psDateRestock && $psDateRestock > $dateNow) {
                    $jet_product['start_selling_date'] = jetTools::jetDate($psDateRestock);
                }
            }
            if (isset($profile['fulfillment_time']) && (int)$profile['fulfillment_time']) {
                $jet_product['fulfillment_time'] = (int)$profile['fulfillment_time'];
            }

            $product_tax_code = null;

            if (isset($profile['product_tax_code']) && Tools::strlen($profile['product_tax_code'])) {
                $product_tax_code = $profile['product_tax_code'];

                if (Tools::strlen($product_tax_code) && array_key_exists($product_tax_code, $this->config['product_tax_codes'])) {
                    $product_tax_code = $this->config['product_tax_codes'][$product_tax_code];
                }
            }

            $jet_product['msrp'] = '';
            $jet_product['map_price'] = '';
            $jet_product['map_implementation'] = '';
            $jet_product['product_tax_code'] = $product_tax_code;

            $jet_product['map_implementation'] = '';
            $jet_product['no_return_fee_adjustment'] = '';
            $jet_product['exclude_from_fee_adjustments'] = false;
            $jet_product['ships_alone'] = '';

            foreach ($jet_product as $key => $value) {
                if (is_string($value)) {
                    $jet_product[$key] = trim($value);
                }
            }

            if (is_array($combination['product_images']) && count($combination['product_images'])) {
                $main_image = reset($combination['product_images']);

                $jet_product['main_image_url'] = $main_image;
                $jet_product['swatch_image_url'] = $main_image;

                if (count($combination['product_images']) > 1) {
                    $jet_product['alternate_images'] = array();

                    foreach ($combination['product_images'] as $key => $image) {
                        if ($key == 0) {
                            continue;
                        }

                        $jet_product['alternate_images'][] = array('image_slot_id' => $key, 'image_url' => $image);
                    }
                }
            }

            $jet_product = array_filter($jet_product);

            if (!$is_parent) {
                $attributes_node_specific = $this->parseMappings($combination, $model);
            }

            $bullet_points = $this->generateBulletPoints($product, $combination, $profile);

            if (is_array($bullet_points) && count($bullet_points)) {
                $jet_product['bullets'] = $bullet_points;
            }

            if (Tools::strlen($product->description)) {
                $jet_product['product_description'] = $product->description;
            }

            // Combination & Mappign

            if (!$is_parent) {
                $jet_product['attributes_node_specific'] = $attributes_node_specific;
            }

            $group_details[$id_product_attribute]['jet_product'] = $jet_product;

            $index++;

            if (Jet::$debug_mode) {
                echo "<pre>\n";
                printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                printf('jet_product: %s'."\n", print_r($jet_product, true));
                echo "</pre>";
            }

            //$shipping_per_item_table = JetProduct::shippingPerItem($combination['weight'], $combination['price']);
        }

        return ($index);
    }

    /**
     * @param $product
     * @param $model
     * @param $group_details
     *
     * @return int
     */
    private function convertForJetVariations($product, &$model, &$group_details)
    {
        $index = 0;
        $jet_variant = array();
        $variation_refinements = array();
        $previous_variation_refinements = array();
        $skus = array();

        foreach ($group_details as $id_product_attribute => $combination) {
            $model['variant'] = array(); // filled by parseMapping

            if ($id_product_attribute == 0 && isset($combination['has_children']) && $combination['has_children']) {
                continue;
            }

            if (!isset($combination['export']) || !(bool)$combination['export']) {
                continue;
            }
            $sku = $combination['combination_reference'];

            $attributes_node_specific = $this->parseMappings($combination, $model);
            $possible_jet_attributes_mappings = array_values($model['variant']);

            $variation_refinements = array();

            if (is_array($attributes_node_specific) && count($attributes_node_specific)) {
                $jet_attributes_count = 0;

                foreach ($attributes_node_specific as $attribute_node) {
                    $jet_attribute_id = $attribute_node['attribute_id'];
                    $attribute_value = $attribute_node['attribute_value'];

                    if (!in_array($jet_attribute_id, $possible_jet_attributes_mappings)) {
                        continue;
                    }

                    if (!Tools::strlen($attribute_value)) {
                        continue;
                    }

                    $variation_refinements[] = (int)$jet_attribute_id;

                    $jet_attributes_count++;
                }
                if ($jet_attributes_count == count($possible_jet_attributes_mappings)) {
                    if (!$previous_variation_refinements || $previous_variation_refinements == $variation_refinements) {
                        $previous_variation_refinements = $variation_refinements;
                        $skus[] = $sku;
                    }
                }
            }
        }

        $parent_sku = null;

        if (is_array($skus) && count($skus) && is_array($variation_refinements) && count($variation_refinements)) {
            $jet_variant['relationship'] = 'Variation';
            $jet_variant['variation_refinements'] = array();
            $jet_variant['group_title'] = $product->name;
            $jet_variant['children_skus'] = array();
            $jet_variant['variation_refinements'] = $variation_refinements;


            foreach ($skus as $key => $sku) {
                if ($key == 0) {
                    $parent_sku = $sku;
                    continue;
                }

                $jet_variant['children_skus'][] = $sku;
                $index++;
            }
            if (!count($jet_variant['children_skus'])) {
                $index = 0;
            }
        }
        if ($parent_sku && $index) {
            $group_details[0]['jet_variant'][$parent_sku] = $jet_variant;
        } else {
            $group_details[0]['jet_variant'] = null;
        }

        if (Jet::$debug_mode) {
            echo "<pre>\n";
            printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
            printf('jet_variant: %s'."\n", print_r($jet_variant, true));
            echo "</pre>";
        }

        return ($index);
    }

    /**
     * @param $group_details
     *
     * @return int
     */
    private function convertForJetPrices(&$group_details)
    {
        $index = 0;
        $jet_price = array();

        foreach ($group_details as $id_product_attribute => $combination) {
            if ($id_product_attribute == 0 && isset($combination['has_children']) && $combination['has_children']) {
                continue;
            }

            if (!isset($combination['export']) || !(bool)$combination['export']) {
                continue;
            }

            $jet_price['price'] = (float)$combination['final_price'];

            $group_details[$id_product_attribute]['jet_price'] = $jet_price;

            $index++;
        }

        return ($index);
    }

    /**
     * @param $group_details
     *
     * @return int
     */
    private function convertForJetInventory(&$group_details)
    {
        $index = 0;
        $jet_quantity = array();

        foreach ($group_details as $id_product_attribute => $combination) {
            if ($id_product_attribute == 0 && isset($combination['has_children']) && $combination['has_children']) {
                continue;
            }

            if (!isset($combination['export']) || !(bool)$combination['export']) {
                continue;
            }

            $quantity = max(0, (int)$combination['quantity']);

            $jet_quantity['fulfillment_nodes'] = array();

            $i = 0;
            foreach (self::$fullfillment_nodes as $fullfillment_node) {
                $jet_quantity['fulfillment_nodes'][$i]['fulfillment_node_id']
                    = $fullfillment_node->jet_fulfillment_node_id;
                $jet_quantity['fulfillment_nodes'][$i]['quantity'] = (int)$quantity;

                $i++;
            }

            $group_details[$id_product_attribute]['jet_quantity'] = $jet_quantity;

            $index++;
        }

        return ($index);
    }

    /**
     * @param $combination
     * @param $model
     *
     * @return array
     */
    public function parseMappings(&$combination, &$model)
    {
        $mappings = array();

        if (array_key_exists('attributes', $combination) && is_array($combination['attributes']) && count($combination['attributes'])) {
            foreach ($combination['attributes'] as $id_attribute => $attribute) {
                $id_attribute_group = $attribute['id_attribute_group'];
                $default_value = $attribute['attribute_name'];
                $mapping = $this->parseMapping($combination, $model, $id_attribute_group, $id_attribute, JetMappings::ATTRIBUTE_TYPE, $default_value);
                if (is_array($mapping) && count($mapping)) {
                    $mappings = array_merge($mappings, $mapping);
                }
            }
        }
        if (array_key_exists('features', $combination) && is_array($combination['features']) && count($combination['features'])) {
            foreach ($combination['features'] as $id_feature => $feature) {
                $id_feature_value = $feature['id_feature_value'];
                $default_value = $feature['value'];
                $mapping = $this->parseMapping($combination, $model, $id_feature, $id_feature_value, JetMappings::FEATURE_TYPE, $default_value);
                if (is_array($mapping) && count($mapping)) {
                    $mappings = array_merge($mappings, $mapping);
                }
            }
        }

        return ($mappings);
    }

    /**
     * @param $combination
     * @param $model
     * @param $id_prestashop
     * @param $id_prestashop_value
     * @param $type
     * @param $default_value
     *
     * @return array
     */
    public function parseMapping(&$combination, &$model, $id_prestashop, $id_prestashop_value, $type, $default_value)
    {
        static $attributes_mappings = null;
        static $features_mappings = null;
        static $features_mapping_right = null;
        static $attributes_mapping_right = null;

        static $jet_attributes = array();
        static $jet_attributes_values = array();

        $jet_attribute = array();
        $jet_mappings = array();

        if ($attributes_mappings === null && $features_mappings === null) {
            if (is_array($this->config['attr_mapping_table']) && count($this->config['attr_mapping_table'])) {
                $attributes_mappings = &$this->config['attr_mapping_table'];
            } else {
                $attributes_mappings = array();
            }

            if (is_array($this->config['feat_mapping_table']) && count($this->config['feat_mapping_table'])) {
                $features_mappings = &$this->config['feat_mapping_table'];
            } else {
                $features_mappings = array();
            }

            if (is_array($this->config['attributes_mapping_right']) && count($this->config['attributes_mapping_right'])) {
                $attributes_mapping_right = &$this->config['attributes_mapping_right'];
            } else {
                $attributes_mapping_right = array();
            }

            if (is_array($this->config['features_mapping_right']) && count($this->config['features_mapping_right'])) {
                $features_mapping_right = &$this->config['features_mapping_right'];
            } else {
                $features_mapping_right = array();
            }
        }
        $possible_jet_attributes_ids = array_key_exists('mappings', $model) && is_array($model['mappings']) ? array_keys($model['mappings']) : array();

        if ($type == JetMappings::ATTRIBUTE_TYPE) {
            $target_mappings = &$attributes_mappings;
            $target_mapping_right = &$attributes_mapping_right;
        } elseif ($type == JetMappings::FEATURE_TYPE) {
            $target_mappings = &$features_mappings;
            $target_mapping_right = &$features_mapping_right;
        } else {
            return (false);
        }

        if (array_key_exists($id_prestashop, $target_mappings)) {
            $jet_attribute_list = array_values($target_mappings[$id_prestashop]);
            $search_jet_attributes_list = array();

            foreach ($jet_attribute_list as $jet_attribute_id) {
                if (!in_array($jet_attribute_id, $possible_jet_attributes_ids)) {
                    continue;
                }
                if (!array_key_exists($jet_attribute_id, $jet_attributes)) {
                    $search_jet_attributes_list[] = $jet_attribute_id;
                }
                if (count($search_jet_attributes_list)) {
                    $tmp_jet_attributes = JetMarketplaceAttributes::attributes($search_jet_attributes_list);
                    if (is_array($tmp_jet_attributes) && count($tmp_jet_attributes)) {
                        $jet_attributes = $jet_attributes + $tmp_jet_attributes;
                    }
                }
            }

            if (is_array($jet_attribute_list) && count($jet_attribute_list)) {
                foreach ($jet_attribute_list as $jet_attribute_id) {
                    if (!in_array($jet_attribute_id, $possible_jet_attributes_ids)) {
                        continue;
                    }
                    $has_jet_mapping = array_key_exists($jet_attribute_id, $target_mapping_right) && is_array($target_mapping_right[$jet_attribute_id]);
                    $has_prestashop_mapping = $has_jet_mapping && array_key_exists($id_prestashop, $target_mapping_right[$jet_attribute_id]) && is_array($target_mapping_right[$jet_attribute_id][$id_prestashop]);
                    $has_attribute_mapping = $has_prestashop_mapping && array_key_exists($id_prestashop_value, $target_mapping_right[$jet_attribute_id][$id_prestashop]);

                    if ($has_attribute_mapping) {
                        $mapping = $target_mapping_right[$jet_attribute_id][$id_prestashop][$id_prestashop_value];
                    } else {
                        $mapping = null;
                    }

                    $is_valid_mapping = Tools::strlen($mapping);
                    $allows_free_text = array_key_exists($jet_attribute_id, $jet_attributes) && (bool)$jet_attributes[$jet_attribute_id]['free_text'];
                    $allows_variant = array_key_exists($jet_attribute_id, $jet_attributes) && (bool)$jet_attributes[$jet_attribute_id]['variant'];

                    if ($is_valid_mapping && !array_key_exists($jet_attribute_id, $jet_attributes_values)) {
                        $jet_attr_values = JetMarketplaceAttributes::attributeValues($jet_attribute_id);

                        if (is_array($jet_attr_values) && count($jet_attr_values)) {
                            $jet_attributes_values[$jet_attribute_id] = $jet_attr_values;
                        } else {
                            $jet_attributes_values[$jet_attribute_id] = array();
                        }
                    }

                    if ($is_valid_mapping && array_key_exists($jet_attribute_id, $jet_attributes_values)) {
                        $valid_values = $jet_attributes_values[$jet_attribute_id];
                    } else {
                        $valid_values = array();
                    }

                    $mapping_key = $is_valid_mapping ? JetTools::toKey($mapping) : null;
                    $unit = null;

                    if ($is_valid_mapping && is_array($valid_values) && count($valid_values) == 1) {
                        // Special case of "Units"
                        //
                        $valid_value = reset($valid_values);

                        if ($valid_value['units']) {
                            $unit = $valid_value['units'];

                            if (!empty($mapping) || is_numeric($mapping)) {
                                $final_mapping = $mapping;
                            } else {
                                $final_mapping = $combination['attributes'][$id_prestashop_value]['attribute_name'];
                            }
                        }
                    } elseif (Tools::strlen($mapping_key) && array_key_exists($mapping_key, $valid_values)) {
                        $final_mapping = $valid_values[$mapping_key]['value'];
                    } elseif ($is_valid_mapping && $allows_free_text) {
                        $final_mapping = $mapping;
                    } elseif ($allows_free_text) {
                        $final_mapping = $default_value;
                    }

                    if (Tools::strlen($final_mapping) && is_numeric($jet_attribute_id)) {
                        $jet_attribute['attribute_id'] = (int)$jet_attribute_id;
                        $jet_attribute['attribute_value'] = $final_mapping;

                        if ($unit) {
                            $jet_attribute['attribute_value_unit'] = $unit;
                        }

                        if ($allows_variant) {
                            $has_mappings = array_key_exists('mappings', $model) && is_array($model['mappings']) && count($model['mappings']);
                            $has_this_attr = $has_mappings && array_key_exists($jet_attribute_id, $model['mappings']) && is_array($model['mappings'][$jet_attribute_id]) && count($model['mappings'][$jet_attribute_id]);
                            $has_variant = $has_this_attr && array_key_exists('variant', $model['mappings'][$jet_attribute_id]) && is_array($model['mappings'][$jet_attribute_id]['variant']);
                            $is_variant = $has_variant && (bool)$model['mappings'][$jet_attribute_id]['variant']['field_value'];
                            if ($is_variant) {
                                if (!array_key_exists('variant', $model)) {
                                    $model['variant'] = array();
                                }
                                $model['variant'][] = $jet_attribute_id;
                            }
                        }
                        $jet_mappings[] = $jet_attribute;
                    }
                }
            }
        }

        return ($jet_mappings);
    }


    /**
     * @param $product
     * @param $combination
     * @param $profile
     *
     * @return array
     */
    private function generateBulletPoints(&$product, &$combination, &$profile)
    {
        $p_bullet_point_labels = true;
        $generated_bullet_points = array();
        $p_bullet_point_strategy = $profile['bullet_point_strategy'];

        if ((int)$p_bullet_point_strategy) {
            switch ($p_bullet_point_strategy) {
                case Jet::BULLET_POINT_STRATEGY_DESC:
                case Jet::BULLET_POINT_STRATEGY_DESC_ATTRIBUTES_FEATURES:
                case Jet::BULLET_POINT_STRATEGY_DESC_FEATURES:
                    if (Tools::strlen($product->description_short) && Tools::strlen($product->description_short) < Jet::LENGTH_BULLET_POINT) {
                        $generated_bullet_points[] = JetTools::cleanStripTags($product->description_short);
                    }
                    return($generated_bullet_points);
            }

            switch ($p_bullet_point_strategy) {
                case Jet::BULLET_POINT_STRATEGY_ATTRIBUTES:
                case Jet::BULLET_POINT_STRATEGY_ATTRIBUTES_FEATURES:
                case Jet::BULLET_POINT_STRATEGY_DESC_ATTRIBUTES_FEATURES:
                    if (array_key_exists('attributes', $combination) && is_array($combination['attributes']) && count($combination['attributes'])) {
                        if (Jet::$debug_mode) {
                            echo "<pre>\n";
                            echo "Bullet Points Strategy: ".$p_bullet_point_strategy."\n";
                            echo "Bullet Points Data:\n".nl2br(print_r($combination['attributes'], true));
                            echo "</pre>\n";
                        }
                        foreach ($combination['attributes'] as $attribute) {
                            $attribute_group = $attribute['group_name'];
                            $attribute_name = $attribute['attribute_name'];
                            if (!Tools::strlen($attribute_name)) {
                                continue;
                            }
                            if ($p_bullet_point_labels) {
                                $generated_bullet_points[] = sprintf('%s: %s', $attribute_group, $attribute_name);
                            } else {
                                $generated_bullet_points[] = $attribute_name;
                            }
                        }
                    }
                    return($generated_bullet_points);
            }

            switch ($p_bullet_point_strategy) {
                case Jet::BULLET_POINT_STRATEGY_FEATURES:
                case Jet::BULLET_POINT_STRATEGY_ATTRIBUTES_FEATURES:
                case Jet::BULLET_POINT_STRATEGY_DESC_ATTRIBUTES_FEATURES:
                case Jet::BULLET_POINT_STRATEGY_DESC_FEATURES:
                    if (array_key_exists('features', $combination) && is_array($combination['features']) && count($combination['features'])) {
                        if (Jet::$debug_mode) {
                            echo "<pre>\n";
                            echo "Bullet Points Strategy: ".$p_bullet_point_strategy."\n";
                            echo "Bullet Points Data:\n".print_r($combination['features'], true);
                            echo "</pre>\n";
                        }
                        foreach ($combination['features'] as $feature) {
                            $feature_value = $feature['value'];
                            if (!Tools::strlen($feature_value)) {
                                continue;
                            }

                            if ($p_bullet_point_labels) {
                                $generated_bullet_points[] = sprintf('%s: %s', $feature['name'], $feature_value);
                            } else {
                                $generated_bullet_points[] = $feature_value;
                            }
                        }
                    }
                    return($generated_bullet_points);
            }

            if (!count($generated_bullet_points)) {
                $generated_bullet_points = array();
            }
        }

        return ($generated_bullet_points);
    }
}

$ProductsList = new JetProducts();
$ProductsList->dispatch();
