$(function(){
    $('input[name*="region"]').click(function(){
        if($('form').serializeArray().length > 1)
        {
            $('.btnNext').removeAttr('disabled');
        }
        else
        {
            $('.btnNext').attr('disabled',true);
        }
    });
    $('.btnNext').click(function(){
        showWaiting(true);
        $('#frmRegion').submit();
    });
});