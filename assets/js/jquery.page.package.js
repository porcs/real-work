var ajaxCheckbox;
var ajaxGift;
 
$(function(){
    
    $('.btnPrev').click(function(){
        showWaiting(true);
        window.location.href = siteurl+'service/region';
    });
    
    $('.btnNext').click(function(){
        showWaiting(true);
        $('#frmPackages').submit();
    });
    
//    $('input[type="checkbox"]').click(function(){
//        var isChecked = $(this).prop('checked');
//        var id = $(this).val();
//        var eleName = $(this).attr('name');
//        var pkt = $(this).attr('pkt');
//        
//        ajaxCheckbox = $.ajax({
//            type: 'POST',
//            url: siteurl+'service/ajaxPackage',
//            data: $.param({
//                checked: isChecked,
//                id: id,
//                pkt: pkt,
//                name: eleName
//            }),
//            success: function(xhr)
//            {
//                if(xhr != '')
//                {
//                    if($('.overview-display').html()==''){ 
//                        $('.overview-display').html(xhr).slideDown('slow');
//                    }else{
//                     $('.overview-display').fadeOut('slow',function(){ 
//                        $('.overview-display').html(xhr).fadeIn('slow');
//                     });
//                    }
//                    
//                }else{
//                    $('.overview-display').slideUp('slow',function(){
//                         $('.overview-display').empty();
//                     });
//                }
//                console.log(xhr);
//            }
//        }); 
        
        $('.checkboSettings').prop('checked', true).change(function(){
            var obj = $(this).find('input[type="checkbox"]');
            var isChecked = obj.prop('checked');
            var id = obj.val();
            var eleName = obj.attr('name');
            var pkt = obj.attr('pkt');
            var offer_pkg = $(this).parents('.tab-pane');
//            console.log(offer_pkg,offer_pkg.find('input[name="offer_pkgs[]"]:first'))
            if(offer_pkg.find('.cb-checkbox.checked').length > 0){
                offer_pkg.find('input[name="offer_pkgs[]"]:first').prop('checked',true);
            }else{
                offer_pkg.find('input[name="offer_pkgs[]"]:first').prop('checked',false);
            }
        
        ajaxCheckbox = $.ajax({
            type: 'POST',
            url: siteurl+'service/ajaxPackage',
            data: $.param({
                checked: isChecked,
                id: id,
                pkt: pkt,
                name: eleName
            }),
            beforeSend:function(){
                showWaiting(true);
            },
            success: function(xhr)
            {
                if(xhr != '')
                {
                    if($('.overview-display').html()==''){ 
                        $('.overview-display').html(xhr).slideDown('slow',function(){showWaiting(false);});
                    }else{
                     $('.overview-display').fadeOut('slow',function(){ 
                        $('.overview-display').html(xhr).fadeIn('slow',function(){showWaiting(false);});
                     });
                    }
                    
                }else{
                    $('.overview-display').slideUp('slow',function(){
                         $('.overview-display').empty();
                     });
                }
//                console.log(xhr);
                
            }
        });
        
        
        if($(this).hasClass('sub') && !isChecked){
            var mbtn = $(".main_pk[value="+pkt+"]");
            
            var num= $('.offer-item-'+pkt+ ' input[type="checkbox"]:checked').length;
             if(num==0){
                mbtn.removeAttr('checked' );
                //$('.offer-item-'+pkt).addClass('hide');
                $('.offer-item-'+pkt).slideUp('slow',function(){
                            //$(this).addClass('hide');
                }); 
                $('.offer-item-'+pkt).find('input[type=checkbox]:checked').removeAttr('checked');
            }
        }else if($(this).hasClass('main_pk')){
            var val = $(this).val();
            var def = $('.offer-item-'+val+ '').find(' input.default'); 
            if(isChecked)
            { 
                $('.offer-item-'+val).slideDown('slow',function(){
                            $(this).removeClass('hide');
                }  );
                 //(def).attr('checked', true);
                (def).prop('checked', true);
 
            }
            else
            {
                $('.offer-item-'+val).slideUp('slow',function(){
                            //$(this).addClass('hide');
                }); 
                $('.offer-item-'+val).find('input[type=checkbox]:checked').prop('checked', false);
            }
        }
    });
    
    $('.btn-region').click(function(e){
        e.preventDefault();
        showWaiting(true);
        var key = $(this).attr('data-key');
        window.location.href = siteurl+"service/packages/?region="+key;
    });
});

$(window).load(function(){
    $('input[type="checkbox"]').each(function(){
        if($(this).prop('checked'))
        {
            if($('.'+"offer-item-"+$(this).val()).html() != null)
            {
                if($('.'+"offer-item-"+$(this).val()).hasClass('hide'))
                    $('.'+"offer-item-"+$(this).val()).removeClass('hide');
            }
        }                
    });
});