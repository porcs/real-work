$( document ).ready(function() {
    $('.responsive-table').DataTable({
            columnDefs: [{
                    targets: "tableNoSort",
                    orderable: false
            }]
        });

        $('.dataTables_wrapper').find('select').addClass('search-select');

        $(".search-select").chosen({
                disable_search_threshold: 1,
                no_results_text: "Nothing found!"
        }).trigger("chosen:updated");	

        $('.chosen-single').find('div').addClass('button-select');
        $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');

        $(".dataTables_wrapper .chosen-search").remove();
});