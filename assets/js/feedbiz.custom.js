
(function($) {
    $.fn.countTo = function(options) {
        options = $.extend({}, $.fn.countTo.defaults, options || {});
        var loops = Math.ceil(options.speed / options.refreshInterval),increment = (options.to - options.from) / loops;
        return $(this).each(function() {
            var _this = this,loopCount = 0,value = options.from,interval = setInterval(updateTimer, options.refreshInterval);
            function updateTimer() {
                value += increment;loopCount++;
                var html = value.toFixed(options.decimals);
                if(options.addComma)html =addCommas(html)
                $(_this).html(html);
                if (typeof(options.onUpdate) == 'function') {options.onUpdate.call(_this, value);}
                if (loopCount >= loops) {clearInterval(interval);value = options.to;
                    if (typeof(options.onComplete) == 'function') {options.onComplete.call(_this, value);}
                }
            }
            function addCommas(NumberStr)
            {
                NumberStr+= '';
                NumberData = NumberStr.split('.');
                Number1 = NumberData[0];
                Number2 = NumberData.length > 1 ? '.' + NumberData[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(Number1)) {
                    Number1 = Number1.replace(rgx, '$1' + ',' + '$2');
                }
                return Number1 + Number2;
            }
        });
    };
    $.fn.countTo.defaults = {from: 0,to: 100,speed: 1000,refreshInterval: 100,decimals: 0,onUpdate: null,onComplete: null,addComma:true};   

})(jQuery);

$(document).ready(function(){

    // disabled for beginner mode 
//    if($('#notification_mode').length) {
//        $('.main').attr('style', 'opacity:0.5');
//        $('.main').find('form').removeAttr('action');
//        $('.main').find('select, input').removeAttr('name').attr('disabled', 'disabled');
//        $('.main').find('button').removeAttr('id').removeAttr('type').attr('disabled', 'disabled');
//    }
    
    $('form#mode').on('submit', function(){
        setTimeout(function(){ 
            parent.location.reload();
        }, 200);
        
    })

    //check id_shop  
    if($('#missing_id_shop').length > 0){
    if($("#f_step").length == 0){
    if($('#missing_id_shop_flag').length == 0){
      
        var $fb_wizard_title = $('#fb_wizard_title').val();
        var step = 0;

        /* == Colorbox Resize == */
        jQuery.colorbox.settings.innerWidth = '80%';
        jQuery.colorbox.settings.innerHeight = '90%';

        jQuery(window).resize(function () {
            // Resize Colorbox when resizing window or changing mobile device orientation
            resizeColorBox();
            window.addEventListener("orientationchange", resizeColorBox, false);
            console.log(cmd_obj, obj, step);
        });

        var resizeTimer;
        var WIDTH,HEIGHT,innerWidth,innerHeight;
        var checksize = function(){
            WIDTH = $(window).width(); 
            HEIGHT = $(window).height();
            innerWidth = '70%';
            innerHeight = '80%';

            if(WIDTH >= 1280 && HEIGHT >= 768){
                innerWidth = '70%';
                innerHeight = '80%';
            }else if(WIDTH >= 800 && HEIGHT >= 600){
                innerWidth = '95%';
                innerHeight = '95%';
            }else{
                innerWidth = '100%';
                innerHeight = '100%';
            }
        };
            
        checksize();
            
        function resizeColorBox() {
            if (resizeTimer) {
                clearTimeout(resizeTimer);
            }
            resizeTimer = setTimeout(function () {
                if (jQuery('#cboxOverlay').is(':visible')) {
                    //jQuery.colorbox.resize({innerWidth: '80%', innerHeight: '90%'});
                    checksize();
                    jQuery.colorbox.resize({innerWidth: innerWidth, innerHeight: innerHeight});
                }
            }, 300);
        }
        
        /* == Colorbox End Resize == */
        $.colorbox({
            iframe: true,
            href: base_url + '/my_shop/configuration/' + step + '/11-00002',
            innerWidth: innerWidth,
            innerHeight: innerHeight,
            onComplete: function () {
                $('#cboxTitle').text($fb_wizard_title);
            },
            onLoad: function () {
                $('body').css('overflow-y', 'hidden');
            },
            onClosed: function () {
                $('body').css('overflow-y', 'auto');
            }
        });
    }
    }
    }

    //nice scroll
    $('.sidebar').niceScroll({touchbehavior:false,cursoropacitymax:0.7,horizrailenabled:false,cursorborder:"",cursorcolor:"#919191",cursorwidth:"3px", autohidemode:true});

    $('.sidebar a').on('click', function() {
        $(".sidebar").getNiceScroll().remove(); 
        myScroll = $(".sidebar").niceScroll({
           touchbehavior:false,
           cursoropacitymax:0.7,
           horizrailenabled:false,
           cursorborder:"",
           cursorcolor:"#919191",
           cursorwidth:"3px", 
           autohidemode:true}); 
        $('.sidebar').getNiceScroll().resize(); 
    }); 
    
    $('.search_right').click(function(){
        $(this).parent().addClass('hidden');
        $('.search-right').removeClass('hidden').fadeIn("slow");
        $('.search-right .form-control').focus();
    });

    $('.search-right .form-control').focusout(function(){
        $(this).parents('.search-right').addClass('hidden');
        $('.search_right').parent().removeClass('hidden');
    });

    
    $(document).on('click', '.message_code_id' ,function() {
    //$('.message_code_id').bind('hover',function() {
        var e=$(this);
        e.unbind('click');
        $.getJSON( base_url + "/tasks/get_message_help/" + e.data('id'),function(d) {
            e.popover({
                html: true,
                title: 'Help <a class="close"><i class="close fa fa-remove"></i></a>',
                content: d.info.content
            }).popover('show');
            e.click(function (i) {
                i.stopPropagation();
            });
            $(document).click(function (i) {
                if (($('.popover').has(i.target).length == 0) || $(i.target).is('.close')) {
                    e.popover('hide');
                }
            });
            $(e).find('.fa-remove').click(function (i) {
                if (($('.popover').has(i.target).length == 0) || $(i.target).is('.close')) {
                    e.popover('hide');
                }
            });
        });
    });

    
    // header.tpl
    $('.language_translate').on('click', function(){

        console.log($(this).val());
        //window.location.href='{$base_url}users/language/';
        $.ajax({
            url: base_url + "/users/language/" + $(this).val(),  
            success:function(data){ 
                location.reload();             
                //notification_alert($('#language_translate_success').val(), 'good');
            },
            error:function(){
                notification_alert($('#language_translate_error').val(), 'bad');
            }
        }); 

    });

});   


(function($) {
    $.fn.fbError = function(){
        var _this = this ;
        _this.error_val = new Object();;
        this.init = function() {
           return _this; 
        }
        _this.append_icon_help = function(code){
            _this.remove_icon_help();
              _this.get_icon_help(code,function(msg){_this.after( msg);}); 
            
        }
        _this.remove_icon_help = function(){
            if(_this.parent().find('.message_code_id')){
                _this.parent().find('.message_code_id').remove();
            }
        }
        _this.get_message_code = function(code,callback){
           var ret_val = null;
            if(  typeof(_this.error_val[code]) != "undefined" && _this.error_val[code] !== null ){
                if( typeof callback !== 'undefined' && $.isFunction(callback)){
                        callback(_this.error_val[code]);
                }
                return _this.error_val[code];
            }else{
                $.ajax({url: base_url+"/tasks/ajax_get_message_code/"+code,  
                success:function(data){
                    _this.error_val[code] = data;
                    ret_val = data;
                    if( typeof callback !== 'undefined' && $.isFunction(callback)){
                        callback(data);
                    }
                }
                }); 
            }
            return ret_val;
        }
        
        _this.get_icon_help = function(code,callback){
            var ret_val = null;
            if(  typeof(_this.error_val[code]) != "undefined" && _this.error_val[code] !== null ){
                if( typeof callback !== 'undefined' && $.isFunction(callback)){
                        callback(_this.error_val[code]);
                }
                return _this.error_val[code];
            }else{
                $.ajax({url: base_url+"/tasks/ajax_get_icon_help/"+code,  
                success:function(data){
                    _this.error_val[code] = data;
                    ret_val = data;
                    if( typeof callback !== 'undefined' && $.isFunction(callback)){
                        callback(data);
                    }
                }
                }); 
            }
            return ret_val;
        }
        return _this;
    }
    
})(jQuery);

function flagUnreadNotification(marketplace, id_shop, id_country, identify){
     $.ajax({
        type: 'POST',
        url: base_url + "help/flagUnreadNotification/" + marketplace +'/'+ id_shop +'/'+ id_country +'/'+ identify,
        dataType: 'json',
        success: function (rdata) { 
            if(rdata)
                window.location.href = base_url + rdata;
        },
        error: function (rerror) {
        }
    });
}