var addActive = (function ($activeBlock) {
    $(document).on('click', $activeBlock, function () {
        $($activeBlock).removeClass('active');
        $(this).addClass('active');
    });
});

var addChartsActive = (function ($chart, $tabs) {
    $(document).on('click', $chart, function () {
        $(this).closest($tabs).find('li').removeClass('active');
        $(this).addClass('active');
    });
});

var disabledInput = (function () {
    var $disabled = $('.form-group').find('[disabled]');
    if ($disabled) {
        $disabled.closest('.form-group').addClass('disabled');
    }
});

var hideBlock = (function ($mainBlock, $hideBlock) {
    $mainBlock.on('click', function (e) {
        if ($hideBlock.css('display') != 'block') {
            $mainBlock.addClass('active')
            $hideBlock.slideDown('slow');
            var firstClick = true;
            $(document).bind('click.myEvent', function (e) {
                if (!firstClick && $(e.target).closest($hideBlock).length == 0) {
                    $hideBlock.slideUp('slow', function () {
                        $hideBlock.css('display', '');
                    });
                    $mainBlock.removeClass('active')
                    $(document).unbind('click.myEvent');
                }
                firstClick = false;
            });
        }
        e.preventDefault();
    });
});

var faRemove = (function ($removeBlock) {
    $('.fa-remove').on('click', function () {
        $(this).closest($removeBlock).slideUp();
    });
});

var uploadFoto = (function ($dad) {
    $('.fileSelect').on('click', function (e) {
        e.preventDefault();
        $(this).closest($dad).find('.uploadbtn').click();
    });
});

var sidebarItem = (function ($sidebarItem) {
    $(this).each(function () {
        var $this = $(this),
                $item = $sidebarItem.parent().find('ul');
        if ($item) {
            $item.parent().addClass('slideItem');
        }
    });
});

var showMore = (function ($hideBlock, $count) {
    $more = $('.see_more'),
            $see_less = $('#see_less').val(),
            $see_more = $('#see_more').val(),
            $mainContent = $('.see_more').parent();

    $hideBlock.slice($count).hide();
    $more.on('click', function () {
        //e.preventDefault();
        $mainContent.find($hideBlock).slice($count).slideToggle();
        if ($more.text() == $see_more) {
            console.log($more.html($see_less));
        } else {
            console.log($more.html($see_more));
        }
    });
});

var resizeSidebar = (function ($sidebar) {
    var $window = $(window).width(),
            $main = $('.main');
    if (($window > 975) === true) {
        $sidebar.show();
        $main.animate({
            'margin-left': '250px'
        }, 0);
    } else {
        $sidebar.hide();
        $main.animate({
            'margin-left': '0px'
        }, 0);
    }
});

var showSidebar = (function () {
    var $this = $(this),
            $sidebar = $('.sidebar'),
            $main = $('.main'),
            $window = $(window);
    $('.btn-menu').on('click', function () {
        if (($(window).width() > 975) === true) {
            if ($sidebar.hasClass('active')) {
                $sidebar.slideUp('slow');
                $sidebar.removeClass('active');
                $main.animate({
                    'margin-left': '0px'
                }, 400);
            } else {
                $sidebar.addClass('active');
                $sidebar.slideDown('slow');
                $main.animate({
                    'margin-left': '250px'
                }, 400);
            }
        } else {
            $sidebar.slideToggle('slow');
        }
    });
});

var showBlock = (function ($blockClick, $showBlock) {
    $showBlock.hide();
    $blockClick.on('click', function (e) {
        e.preventDefault();
        $showBlock.slideToggle();
    });
    $(document).mouseup(function (e) {
        if ($showBlock.has(e.target).length === 0) {
            $showBlock.slideUp();
        }
    });
});

var countDown = (function () {
    var note = $('#note'),
            ts = new Date(2015, 2, 28),
            newYear = true;

    if ((new Date()) > ts) {
        ts = (new Date()).getTime() + 10 * 24 * 60 * 60 * 1000;
        newYear = false;
    }
    $('.countdown').each(function () {
        $(this).countdown({
            timestamp: (new Date()).getTime() + $(this).attr('rel') * 1000,
            callback: function (days, hours, minutes, seconds) {}
        });
    });
});

var checkboTable = (function () {
    $('input[type="checkbox"]:not(input.amazon-input)').change(function () {
        var $this = $(this),
                $_tableCheckbo = $this.closest('.dropCheckbo'),
                $_tableRow = $this.closest('.row').find('.row');

        $this.each(function () {
            var $_this = $(this),
                    $tableRow = $_this.closest('.row'),
                    $t_Head = $tableRow.find('thead'),
                    $t_Foot = $tableRow.find('tfoot');
            if ($_this.is(':checked')) {
                switch ($this.attr('name')) {
                    case ('id'):
                        $tableRow.find('.tableId').show();
                        $t_Head.find('th:first').show();
                        $t_Foot.find('th:first').show();
                        break;
                    case ('username'):
                        $tableRow.find('.tableUserName').show();
                        $t_Head.find('th:nth-child(2)').show();
                        $t_Foot.find('th:nth-child(2)').show();
                        break;
                    case ('email'):
                        $tableRow.find('.tableEmail').show();
                        $t_Head.find('th:nth-child(3)').show();
                        $t_Foot.find('th:nth-child(3)').show();
                        break;
                    case ('status'):
                        $tableRow.find('.tableStatus').show();
                        $t_Head.find('th:nth-child(4)').show();
                        $t_Foot.find('th:nth-child(4)').show();
                        break;
                    case ('joined'):
                        $tableRow.find('.tableJoined').show();
                        $t_Head.find('th:nth-child(5)').show();
                        $t_Foot.find('th:nth-child(5)').show();
                        break;
                    case ('summary'):
                        $tableRow.find('.tableSummary').show();
                        $t_Head.find('th:nth-child(6)').show();
                        $t_Foot.find('th:nth-child(6)').show();
                        break;
                    case ('error'):
                        $tableRow.find('.tableError').show();
                        $t_Head.find('th:nth-child(7)').show();
                        $t_Foot.find('th:nth-child(7)').show();
                        break;
                    case ('date'):
                        $tableRow.find('.tableDate').show();
                        $t_Head.find('th:nth-child(8)').show();
                        $t_Foot.find('th:nth-child(8)').show();
                        break;
                    case ('download'):
                        $tableRow.find('.tableDownload').show();
                        $t_Head.find('th:nth-child(9)').show();
                        $t_Foot.find('th:nth-child(9)').show();
                        break;
                    case ('exported'):
                        $tableRow.find('.tableExported').show();
                        $t_Head.find('th:nth-child(10)').show();
                        $t_Foot.find('th:nth-child(10)').show();
                        break;
                }
            } else {
                switch ($_this.attr('name')) {
                    case ('id'):
                        $tableRow.find('.tableId').hide();
                        $t_Head.find('th:first').hide();
                        $t_Foot.find('th:first').hide();
                        break;
                    case ('username'):
                        $tableRow.find('.tableUserName').hide();
                        $t_Head.find('th:nth-child(2)').hide();
                        $t_Foot.find('th:nth-child(2)').hide();
                        break;
                    case ('email'):
                        $tableRow.find('.tableEmail').hide();
                        $t_Head.find('th:nth-child(3)').hide();
                        $t_Foot.find('th:nth-child(3)').hide();
                        break;
                    case ('status'):
                        $tableRow.find('.tableStatus').hide();
                        $t_Head.find('th:nth-child(4)').hide();
                        $t_Foot.find('th:nth-child(4)').hide();
                        break;
                    case ('joined'):
                        $tableRow.find('.tableJoined').hide();
                        $t_Head.find('th:nth-child(5)').hide();
                        $t_Foot.find('th:nth-child(5)').hide();
                        break;
                    case ('summary'):
                        $tableRow.find('.tableSummary').hide();
                        $t_Head.find('th:nth-child(6)').hide();
                        $t_Foot.find('th:nth-child(6)').hide();
                        break;
                    case ('error'):
                        $tableRow.find('.tableError').hide();
                        $t_Head.find('th:nth-child(7)').hide();
                        $t_Foot.find('th:nth-child(7)').hide();
                        break;
                    case ('date'):
                        $tableRow.find('.tableDate').hide();
                        $t_Head.find('th:nth-child(8)').hide();
                        $t_Foot.find('th:nth-child(8)').hide();
                        break;
                    case ('download'):
                        $tableRow.find('.tableDownload').hide();
                        $t_Head.find('th:nth-child(9)').hide();
                        $t_Foot.find('th:nth-child(9)').hide();
                        break;
                    case ('exported'):
                        $tableRow.find('.tableExported').hide();
                        $t_Head.find('th:nth-child(10)').hide();
                        $t_Foot.find('th:nth-child(10)').hide();
                        break;
                }
            }
            if ($this.closest('.dropCheckbo').find('.cb-checkbox.checked').length == 0) {
                $_tableRow.find('.tableEmpty').show();
                $_tableRow.find('.dataTables_filter').hide();
                $_tableRow.find('.dataTables_info').hide();
                $_tableRow.find('.dataTables_paginate').hide();
            } else {
                $_tableRow.find('.tableEmpty').hide();
                $_tableRow.find('.dataTables_filter').show();
                $_tableRow.find('.dataTables_info').show();
                $_tableRow.find('.dataTables_paginate').show();
            }
        });
    });
});

var hideEmpty = (function () {
    var $window = $(window).width();
    if (($window < 975) === true) {
        $('.responsive-table').find('td:empty').hide();
    } else {
        $('.responsive-table').find('td:empty').css('display', 'table-cell');
    }
});

var mainMenu = (function () {
    $('.sidebar_content a').on('click', function () {
        var $this = $(this);
        if ($this.next('ul').length > 0) {
            var $item = $this.closest('li'),
                    $parentItem = $item.parent(),
                    selfClick = $item.find('ul:first').is(':visible');
            if (!selfClick) {
                $parentItem.find('> li ul:visible').slideToggle();
                $item.find('.link').removeClass('active');
                $item.removeClass('active');
                $parentItem.find('li').removeClass('active');
                $parentItem.find('a').removeClass('active');
            }
            $item.find('ul:first').slideToggle();
            $item.find('.link').toggleClass('active');
            $item.toggleClass('active');
            return false;
        }

    });
});

var checkBox = (function ($checkbox) {
    if ($checkbox.hasClass('checked')) {
        $checkbox.find('input').prop('checked', true).change();
    } else {
        $checkbox.find('input').prop('checked', false).change();
    }
});

var checkBa = (function ($checkbox) {
    $($checkbox).each(function (i, elm) {
        if ($(elm).hasClass('checked')) {
            $(elm).find('input').prop('checked', true).change();
        } else {
            $(elm).find('input').prop('checked', false).change();
        }
    });
});

var treeSettings = (function ($_this) {
    $_this.parent().find('li').removeClass('active');
    $_this.addClass('active');
});

var addWithDrop = (function ($mainContainer) {
    $chosenContainer = $mainContainer.find('.chosen-container');
    $chosenContainer.on('click', function () {
        $(this).toggleClass('chosen-with-drop');
    });
});

var removeWithDrop = function ($parentContainer) {
    $(document).mouseup(function (e) {
        $container = $parentContainer.find('.chosen-container');
        if ($container.has(e.target).length === 0) {
            $container.removeClass('chosen-with-drop');
        }
    });
};

var tree = (function ($tree) {
    var $this = $(this),
            $tree_item = $tree.find('li'),
            $cb_plus = $tree.find('.cb-plus'),
            $propagate = $('.propagate'),
            $expand = $('.expandAll'),
            $collapse = $('.collapseAll'),
            $checked = $('.checkedAll'),
            $unchecked = $('.uncheckedAll'),
            $checkBa = $('.checkBaAll'),
            $uncheckBa = $('.uncheckBaAll'),
            $checkbox = $tree.find('input[type="checkbox"]'),
            $cb_checkbox = $tree.find('.cb-checkbox'),
            $Ba_checkbox = $tree.find('.checkBa'),
            $_window = $(window).width();
            
    if(!$.isFunction($($tree).on('keydown'))){
        $($tree).on('keydown', function(e) {
            var keyPressed = e.keyCode;
            if (keyPressed == 16) { //thats the keycode for shift

              $(this).css({'-moz-user-select':'-moz-none',
                 '-moz-user-select':'none',
                 '-o-user-select':'none',
                 '-khtml-user-select':'none',
                 '-webkit-user-select':'none',
                 '-ms-user-select':'none',
                 'user-select':'none'
              }); //or you could pass these css rules into a class and add that class to html instead

            }
          });
    }

    $propagate.on('click', function () {
        var $_this = $(this);
        treeSettings($_this);
        $tree.find('ul .cb-plus').addClass('active');
    });

    $expand.on('click', function () {
        var $_this = $(this);
        treeSettings($_this);
        $tree.find('ul.tree_child').slideDown();
        $tree.find('ul .cb-plus').addClass('active');
    });

    $collapse.on('click', function () {
        var $_this = $(this);
        treeSettings($_this);
        $tree.find('ul.tree_child').slideUp();
        $tree.find('ul .cb-plus').removeClass('active');
    });

    $checked.on('click', function () {
        var $_this = $(this);
        treeSettings($_this);
        $cb_checkbox.addClass('checked');
        checkBox($cb_checkbox);
    });

    $unchecked.on('click', function () {
        var $_this = $(this);
        treeSettings($_this);
        $cb_checkbox.removeClass('checked');
        checkBox($cb_checkbox);
    });

    $checkBa.on('click', function () {
        showWaiting(true);
        var time = 0;
        var num = $('body').find('select').length; // count selector in right tree.
        if (num > 200) {
            time = 200;
        }
        var $_this = $(this);
        setTimeout(function () {
            treeSettings($_this);
            $Ba_checkbox.addClass('checked');
            checkBa($Ba_checkbox);
            showWaiting(false);
        }, time);
    });

    $uncheckBa.on('click', function () {
        showWaiting(true);
        var time = 0;
        var num = $('body').find('select').length; // count selector in right tree.
        if (num > 200) {
            time = 200;
        }
        var $_this = $(this);
        setTimeout(function () {
            treeSettings($_this);
            $Ba_checkbox.removeClass('checked');
            checkBa($Ba_checkbox);
            showWaiting(false);
        }, time);
    });

    var lastChecked = null;
    var $chkboxes = $('.checkBa');
    $chkboxes.click(function (e) {
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        if (e.shiftKey) {
            var start = $chkboxes.index(this);
            var end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).each(function (i, elm) {
                $(elm).find('input[name^="category"]').prop('checked', $(lastChecked).find('input[name^="category"]').prop('checked')).change();
                if ($(lastChecked).find('input[name^="category"]').prop('checked')) {
                    $(elm).addClass('checked');
                } else {
                    $(elm).removeClass('checked');
                }
            });
            window.getSelection().removeAllRanges();
            return false;
        }

        lastChecked = this;
    });

    $checkbox.change(function () {
        var $_this = $(this);
        if ($_this.is(':checked')) {
            $_this.closest('li').find('.tree_show:first').show();
        } else {
            $_this.closest('li').find('.tree_show:first').hide();
        }
    });

    $tree_item.each(function () {
        var $_this = $(this);
        if ($_this.find('ul.tree_child').length) {
            $_this.find('.cb-plus').show();
        } else {
            $_this.find('.cb-plus').hide();
        }
    });

    $cb_plus.on('click', function () {
        var $_this = $(this);
        $_this.closest('li').find('ul.tree_child').slideToggle();
        $_this.toggleClass('active');
        if ($_this.not('.active')) {
            $_this.closest('li').find('ul.tree_child').slice(1).hide();
            $_this.closest('li').find('ul .cb-plus').removeClass('active');
        }
    });
});

var countBlock = (function ($countBlock, $selectBlock) {
    var $count = $selectBlock.length - 1;
    $countBlock.text($count);
    if ($count == 0) {
        $('.btnShow').addClass('hidden');
    } else {
        $('.btnShow').removeClass('hidden');
    }
});

var choSelect = (function () {
    $('.search-select').chosen({search_contains: true, allow_single_deselect: true});
    $('.chosen-single').find('div').addClass('button-select');
    $('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
});

var rulesAddSettings = (function () {
    var $setCheckbo = $('#main .setBlockMain').find('.checkBo'),
            $setCzn = $('#main .setBlockMain').find('.choZn'),
            $ruleCheckBo = $('#main .ruleSettings').find('.checkBo'),
            $ruleCzn = $('#main .ruleSettings .choZn'),
            $rule = '#main .ruleSettings';

    $setCheckbo.clone(true).appendTo($rule);
    $setCzn.clone(false).appendTo($rule);

    $setCheckbo.remove();
    $setCzn.remove();

    $('.ruleSettings .checkBo').clone(true).appendTo('.setBlockMain .addCheckBo');
    $('.ruleSettings .choZn').clone(true).appendTo('.setBlockMain .addSelect');
});

var rulesRemoveSettings = (function () {
    $('.ruleSettings .checkBo').remove();
    $('.ruleSettings .choZn').remove();
});

var countRule = (function () {
    var $this = $(this);
    if ($(this).closest('.profileBlock').find('.setBlockMain')) {
        $(this).closest('.profileBlock').find('.countRule').text($('.setBlock').length + $('.setBlockMain').length - 2);
    } else {
        $(this).closest('.profileBlock').find('.countRule').text($('.setBlock').length);
    }
});

var showBlockSet = (function ($block, $boolean) {
    $(document).on('click', $block, function (e) {
        e.preventDefault();

        $(this).closest('.row').next().clone($boolean).insertAfter($(this).closest('.row').next()).show();
        countBlock($('.countProfile'), $('.showSelectBlock'));
        countBlock($('.countBlock'), $('.amazonProfiles'));
        choSelect();
    });
});

var selectWizard = (function () {
    $selectWizard_point = $('.selectWizard li');
    if ($selectWizard_point.hasClass('active')) {
        $selectWizard_point.closest('.modal').find('.btn-save').show();
    } else {
        $selectWizard_point.closest('.modal').find('.btn-save').hide();
    }
});


var notification_alert = (function ($msg, $type, $error_code) {
    if (!$msg) {
        return;
    }
    if (!$type) {
        $type = 'good';
    }
    if ($error_code && typeof $error_code != typeof undefined) {
        var err_txt = 'Error';
        if ($('#error_txt').length != 0) {
            err_txt = $('#error_txt').val();
        }
        $msg = err_txt + ' ' + $error_code + ' : ' + $msg;
    } else {
        $error_code = '';
    }
    if ($type == 'bad') {
        $.ajax({url: base_url + "ajax_task/ajax_process_update.php", method: 'POST', data: {user_bad_notify: true, message: $msg, err_code: $error_code}});
    }

    $('.notification').each(function () {

        if ($(this).hasClass('ajax_notify')) {
            if ($(this).is(":visible")) {
                $(this).hide('swing', function () {
                    $(this).removeClass('good').removeClass('bad').removeClass('warning').addClass($type).show('slow').find('p').html($msg);
                });
            } else {
                $(this).removeClass('good').removeClass('bad').removeClass('warning').addClass($type).show('slow').find('p').html($msg);
            }

        } else {
            $(this).hide('swing');
        }

    });
});

var showWaiting = (function (status) {
    if (status) {
        $.blockUI();
    } else {
        $.unblockUI();
    }
});

var notificationslideshow = (function (status, running_txt) {
    switch (status) {
        case true :
            $('.notification.good.ajax_notify').show('slow').find('p').html(running_txt);
            break;
        case false :
            setTimeout(function () {
                $('.notification.good').hide('swing', function () { /*$(this).remove();*/
                });
            }, 5000);
            break;
    }
});