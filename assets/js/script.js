$(document).ready(function() {	
	$('#logNotification').popover({
			width:'700px', html: true,
                        'placement':'bottom',
                        content:$('#user_inbox'),
                        //template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div><div class="popover-footer text-center"><a href="users/notifications" class="link"><div class="ui_viewmore">View more</div></a></div></div>',
                        callback: function (obj) {
			var $box = $('.logNotification .popover');
			//console.log(obj,$box,$box.find('a'));
                            $(this).find('a').each(function(){
                            $(this).css({color:'black'});
                            //console.log($(this));
                            console.log($(obj));
                            
                            
			});
            }}).on('show.bs.popover', function () {
                        //Check Screen
            		if($('#logNotification').data('bs.popover') != undefined){
                        if($(window).width() < 975){
                            $('#logNotification').data('bs.popover').options.placement = 'left';
                        }else{
                            $('#logNotification').data('bs.popover').options.placement = 'bottom';
                        }
            		}

            }); 
            $(window).resize(function() {
        		if($('#logNotification').data('bs.popover') != undefined){
	                if($(window).width() < 975){
	                    $('#logNotification').data('bs.popover').options.placement = 'left';
	                }else{
	                    $('#logNotification').data('bs.popover').options.placement = 'bottom';
	                }
	                $('#logNotification').popover('hide');
        		}
            });
            
            $('[data-toggle=popover]').on('shown.bs.popover', function () {
                $('.popover').css('top', parseInt($('.popover').css('top')) + 5 + 'px');
                $('.popover').css('left', parseInt($('.popover').css('left')) + 7 + 'px');
            });

	showMore($('.search-more li'), 6);

	$('.custom-form').checkBo();
        
        /* Datatable Processing */
        if($('.dataTables_processing').length > 0){
            $('.dataTables_processing').html('');
            $('.dataTables_processing').append('<div class="feedbizloader"><div class="feedbizcolor"></div><p class="m-t10 text-info">'+$('#processing').val()+'</p></div>');
            if($('.dataTables_processing').find('.feedbizloader')){
                $('.dataTables_processing').find('.feedbizloader').find('.feedbizcolor').sprite({fps: 10, no_of_frames: 30});
            }
        }
        
	/*$('.responsive-table').dataTable({
		 columnDefs: [{
	        targets: "tableNoSort",
	        orderable: false
	    }]
	});	*/

	$('.dataTables_wrapper').find('select').addClass('search-select');


	$(".search-select").chosen({
                search_contains: true,
		disable_search_threshold: 1,
		no_results_text: "Nothing found!"
	}).trigger("chosen:updated");	

	$('.chosen-single').find('div').addClass('button-select');
	$('.button-select').find('b').addClass('fa').addClass('fa-angle-down');
	
	$(".dataTables_wrapper .chosen-search").remove();	
	
	//hideEmpty();

	$('.allCheckBo').checkBo({
		checkAllButton : '.checkAll', 
                checkAllTarget : '.checkboSettings'
	});

	$('.forCheckBo').on('click', function(){
		var $this = $(this),
			$checkbox = $this.parent().find('.cb-checkbox');
		$checkbox.toggleClass('checked');
		checkBox($checkbox);
		return false;
	});
        if($("#progressbar").length>0){
            if (typeof draggable !== 'undefined' && $.isFunction(draggable)) {
                $("#progressbar").draggable();
            }
        }

	$('.progressbar .fa-remove').on('click', function(){
		$(this).closest('.progressbar').remove();
	});

	hideBlock ($('.btn-more'), $('.loginBlock'));
	addActive('.form-group');
	addChartsActive('.chart .tabs li', '.tabs');
	disabledInput();	

	uploadFoto('.profile');

	mainMenu();	
	sidebarItem($('.sidebar ul li > ul li a'));	
	
	resizeSidebar($('.sidebar'));
	showSidebar();
	countDown();
	checkboTable();
	tree($('.tree'));

	$(".gallery-image").fancybox({
		"padding" : 0,
		"imageScale" : false, 
		"zoomOpacity" : false,
		"zoomSpeedIn" : 1000,	
		"zoomSpeedOut" : 1000,	
		"zoomSpeedChange" : 1000, 
		"frameWidth" : 700,	 
		"frameHeight" : 600, 
		"overlayShow" : true, 
		"overlayOpacity" : 0.9,	
		"hideOnContentClick" :false,
		"centerOnScroll" : false,
		helpers : {
			media : {}
		}
	});

	$('.countDiv').remove();

	showBlock($('.language'), $('.language_block'));

	$('.showColumns').on('click', function(event){
		event.preventDefault();
		$(this).find('.dropCheckbo').show();
		$(document).mouseup(function (e) {
			if ($('.dropCheckbo').has(e.target).length === 0){
				$('.dropCheckbo').hide();
			}
		});
	});
	
	$('.btn-MainSearch').on('click', function(){
		var $window = $(window).width();
		if(($window > 751) === true){
			$(this).animate({
			'left':'-30px'
			},400);
			$(this).parent().find('input').slideToggle(400);
		}
	});
        
	$('.inputDate').datepicker();

	/*$(document).on('click', '.withIcon .cb-plus.good', function(){
		$(this).closest('.row').prev().clone().insertAfter($(this).closest('.row')).show();				
		choSelect();
	})*/

        $(window).resize(function() {
                resizeSidebar($('.sidebar'));
                //hideEmpty();
        });
        
        $('.cb-radio a').click(function(){
            var ref = $(this).attr('href');
            window.location.href=ref;
        });

        $('.notification').on('click','.fa-remove',function(){ $(this).parents('.notification').animate({ height: 0, opacity: 0 }, { easing: 'swing',duration: 1000,complete: function(){ /*$(this).remove();*/ $(this).hide(); } }); });
        $('.validate').on('click','.fa-remove',function(){ $(this).parents('.row').animate({ height: 0, opacity: 0 }, { easing: 'swing',duration: 1000,complete: function(){ /*$(this).remove();*/ $(this).hide(); } }); });
	
        //setTimeout(function(){ $('.notification.good').animate({ height: 0, opacity: 0 }, { easing: 'swing',duration: 1000,complete: function(){ /*$(this).remove();*/ } }); }, 5000);
       
        notificationslideshow(false);
        
        $('#sidebar-tooltip').tooltipster({ position:'right', theme:'tooltipster-shadow'});

        $('.tooltips').tooltipster({ 
        	position:'right', 
        	theme:'tooltipster-shadow', 
        	contentAsHTML: true,
        	animation: 'fade',
                maxWidth: 400,
		    touchDevices: true,
		    //trigger: 'hover',
		    //hideOnClick: false,
		    interactive: true,
		    //autoClose: false,
        });
}); // close Ready
