var ajaxCheckbox;

$(function(){
    $('.paypal_btn').click(function(){
        if($(this).hasClass('acitve'))return;
        $('.paypal_btn.active').removeClass('active');
        $(this).addClass('active');
        
    });
    $('.btnNext').click(function(){
        if($('#frmPayment').validationEngine('validate'))
        {
            showWaiting(true);
            $('#frmPayment').submit();
        }
        return false;
    });
    
    $('input[type="radio"]').click(function(){
        if($(this).attr('id') == "pay_credit" && $(this).prop('checked'))
        {
            $('.layout-credit').removeClass('hide');
        }
        else
        {
            $('.layout-credit').addClass('hide');
        }
    });
    
    $('.btn-giftcode').click(function(){
        var code = $('input[name="gift_code"]').val();
        
        if(code != '')
        {
            ajaxCheckbox = $.ajax({
                type: 'POST',
                url: siteurl+'service/ajaxGiftCode',
                data: $.param({
                    param: 'giftcode',
                    code: code
                }),
                beforeSend: function(){
                    try{
                        ajaxCheckbox.abort();
                    }catch(Exception){}
                },
                success: function(xhr)
                {
                    if(xhr == 'true')
                    {
                        window.location.reload();
                    }
                    else
                    {
                        $.gritter.add({
                            title: 'Error',
                            text: xhr,
                            class_name: 'gritter-error'
                        });
                        $('input[name="gift_code"]').val('');
                    }
                }
            });
        }
        else
        {
            return false;
        }
    });
});

//$(window).load(function(){
//    if(err != '')
//    {
//        $.gritter.add({
//            title: 'Error',
//            text: err,
//            class_name: 'gritter-error'
//        });
//    }
//});
$('.cb-radio a').click(function(){
    var ref = $(this).attr('href');
    window.location.href=ref;
})
function checkCCValid(field, rules, i, options)
{
    if(!field.hasClass('valid'))
    {
       return 'Your CreditCard is invalid, please try again';  
    }
}