var ajaxLogDetail;
$(function(){
    var oTable1 = $('#history-datatable').dataTable({
        "aaSorting": [[0,"desc"]],
        "aoColumns": [
          null,null,null,null,
          { "bSortable": false }
        ]
    });
    
    $('.btn-detail').click(function(e){
        var id = $(this).attr('data-id');
        
        ajaxLogDetail = $.ajax({
            type: 'post',
            data: $.param({
                'id': id
            }),
            url: siteurl+"billing/logDetail/",
            beforeSend: function(){
                try
                {
                    ajaxLogDetail.abort();
                }
                catch(Exception)
                {}
            },
            success: function(xhr){
                if(xhr != '')
                {
                    $('.modal-detail').html(xhr);
                    $('#myModel').modal();
                }                
            }
        });                
    });
});