$(function(){

    $('#download-datatable').dataTable({
        "aaSorting": [[ 2, "desc" ]],
        "aoColumns": [
          null,null,null,null,
          { "bSortable": false }
        ]
    });
    
    $('.btn-app').click(function(e){
        e.preventDefault();
        
        var id = $(this).attr('data-id');
        window.open(base_url+'billing/printBilling/?id='+id,'_blank');
    });
});