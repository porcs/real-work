 !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
function goclicky(meh,w,h)
{
    if(!w)w=650;
    if(!h)h=300
    var x = screen.width/3 - w/2;
    var y = screen.height/3 - h/2;
    x = x<0?0:x;
    y = x<0?0:y;    
    
    window.open($(meh).attr('url'), 'sharegplus','height='+h+',width='+w+',left='+x+',top='+y);
}

 
 $(function(){
    $('#fb-shar-btn').click(function(){
         $('#fb_if').modal({show:true})
     });
    /*$('#tag_email').tagsInput({
        width: '99%',
        height: '200px',
        defaultText: tagPlaceholder
    });*/
    
    $('#form-field-1').click(function(){
        $(this).select();
    });
    
    $('.btn-save').click(function(){
        var chk = true;
        var emailItem = $('#tag_email').val();
        if(emailItem != '')
        {
            var arrItem = emailItem.split(',');
            for(var i=0;i<arrItem.length;i++)
            {
                if(!validateEmail(arrItem[i]))
                {
                    $.gritter.add({
                            title: 'Error',
                            text: 'Invalid email address(s), Please try again.',
                            class_name: 'gritter-error'
                    });
                    chk = false;
                    return;
                }
            }
        }
        else
        {
            chk = false;
        }
        
        if(chk){
            $('#frmEmail').submit();
            return false;
        }
        else
        {
            return false;
        }
    });
});

$(window).load(function(){
     var completeTxt = false;
    if($('#mess').val() !== ''){
        completeTxt = true;
    }
    if(completeTxt)
    {
        $.gritter.add({
            title: 'Success',
            text: 'Send Recommend a Friend via Email Complete.',
            class_name: 'gritter-success'
        });
    }
});

function validateEmail(email){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}