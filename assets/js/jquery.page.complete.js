$(function(){
    
    $('.btnPrev').click(function(){
        event.preventDefault();
        showWaiting(true);
        window.location.href = siteurl+"service/information";
    });
    
    $('.btnNext').click(function(){
        showWaiting(true);
        window.location.href = siteurl+"service/finish";
    });
    
});