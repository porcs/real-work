 
(function($) {
    $.easyPieChart = function(el, options) {
        var addScaleLine, animateLine, drawLine, easeInOutQuad, rAF, renderBackground, renderScale, renderTrack,
                _this = this;
        this.el = el;
        this.$el = $(el);
        this.$el.data("easyPieChart", this);
        this.init = function() {
            var percent, scaleBy;
            _this.options = $.extend({}, $.easyPieChart.defaultOptions, options);
            percent = parseInt(_this.$el.data('percent'), 10);
            _this.percentage = 0;
            _this.canvas = $("<canvas width='" + _this.options.size + "' height='" + _this.options.size + "'></canvas>").get(0);
            _this.$el.append(_this.canvas);
            if (typeof G_vmlCanvasManager !== "undefined" && G_vmlCanvasManager !== null) {
                G_vmlCanvasManager.initElement(_this.canvas);
            }
            _this.ctx = _this.canvas.getContext('2d');
            if (window.devicePixelRatio > 1) {
                scaleBy = window.devicePixelRatio;
                $(_this.canvas).css({
                    width: _this.options.size,
                    height: _this.options.size
                });
                _this.canvas.width *= scaleBy;
                _this.canvas.height *= scaleBy;
                _this.ctx.scale(scaleBy, scaleBy);
            }
            _this.ctx.translate(_this.options.size / 2, _this.options.size / 2);
            _this.ctx.rotate(_this.options.rotate * Math.PI / 180);
            _this.$el.addClass('easyPieChart');
            _this.$el.css({
                width: _this.options.size,
                height: _this.options.size,
                lineHeight: "" + _this.options.size + "px"
            });
            _this.update(percent);
            return _this;
        };
        this.update = function(percent) {
            percent = parseFloat(percent) || 0;
            if (_this.options.animate === false) {
                drawLine(percent);
            } else {
                animateLine(_this.percentage, percent);
            }
            return _this;
        };
        renderScale = function() {
            var i, _i, _results;
            _this.ctx.fillStyle = _this.options.scaleColor;
            _this.ctx.lineWidth = 1;
            _results = [];
            for (i = _i = 0; _i <= 24; i = ++_i) {
                _results.push(addScaleLine(i));
            }
            return _results;
        };
        addScaleLine = function(i) {
            var offset;
            offset = i % 6 === 0 ? 0 : _this.options.size * 0.017;
            _this.ctx.save();
            _this.ctx.rotate(i * Math.PI / 12);
            _this.ctx.fillRect(_this.options.size / 2 - offset, 0, -_this.options.size * 0.05 + offset, 1);
            _this.ctx.restore();
        };
        renderTrack = function() {
            var offset;
            offset = _this.options.size / 2 - _this.options.lineWidth / 2;
            if (_this.options.scaleColor !== false) {
                offset -= _this.options.size * 0.08;
            }
            _this.ctx.beginPath();
            _this.ctx.arc(0, 0, offset, 0, Math.PI * 2, true);
            _this.ctx.closePath();
            _this.ctx.strokeStyle = _this.options.trackColor;
            _this.ctx.lineWidth = _this.options.lineWidth;
            _this.ctx.stroke();
        };
        renderBackground = function() {
            if (_this.options.scaleColor !== false) {
                renderScale();
            }
            if (_this.options.trackColor !== false) {
                renderTrack();
            }
        };
        drawLine = function(percent) {
            var offset;
            renderBackground();
            _this.ctx.strokeStyle = $.isFunction(_this.options.barColor) ? _this.options.barColor(percent) : _this.options.barColor;
            _this.ctx.lineCap = _this.options.lineCap;
            _this.ctx.lineWidth = _this.options.lineWidth;
            offset = _this.options.size / 2 - _this.options.lineWidth / 2;
            if (_this.options.scaleColor !== false) {
                offset -= _this.options.size * 0.08;
            }
            _this.ctx.save();
            _this.ctx.rotate(-Math.PI / 2);
            _this.ctx.beginPath();
            _this.ctx.arc(0, 0, offset, 0, Math.PI * 2 * percent / 100, false);
            _this.ctx.stroke();
            _this.ctx.restore();
        };
        rAF = (function() {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
                return window.setTimeout(callback, 1000 / 60);
            };
        })();
        animateLine = function(from, to) {
            var anim, startTime;
            _this.options.onStart.call(_this);
            _this.percentage = to;
            Date.now || (Date.now = function() {
                return +(new Date);
            });
            startTime = Date.now();
            anim = function() {
                var currentValue, process;
                process = Date.now() - startTime;
                if (process < _this.options.animate) {
                    rAF(anim);
                }
                _this.ctx.clearRect(-_this.options.size / 2, -_this.options.size / 2, _this.options.size, _this.options.size);
                renderBackground.call(_this);
                currentValue = [easeInOutQuad(process, from, to - from, _this.options.animate)];
                _this.options.onStep.call(_this, currentValue);
                drawLine.call(_this, currentValue);
                if (process >= _this.options.animate) {
                    return _this.options.onStop.call(_this, currentValue, to);
                }
            };
            rAF(anim);
        };
        easeInOutQuad = function(t, b, c, d) {
            var easeIn, easing;
            easeIn = function(t) {
                return Math.pow(t, 2);
            };
            easing = function(t) {
                if (t < 1) {
                    return easeIn(t);
                } else {
                    return 2 - easeIn((t / 2) * -2 + 2);
                }
            };
            t /= d / 2;
            return c / 2 * easing(t) + b;
        };
        return this.init();
    };
    $.easyPieChart.defaultOptions = {
        barColor: '#ef1e25',
        trackColor: '#f2f2f2',
        scaleColor: '#dfe0e0',
        lineCap: 'round',
        rotate: 0,
        size: 110,
        lineWidth: 3,
        animate: false,
        onStart: $.noop,
        onStop: $.noop,
        onStep: $.noop
    };
    $.fn.easyPieChart = function(options) {
        return $.each(this, function(i, el) {
            var $el, instanceOptions;
            $el = $(el);
            if (!$el.data('easyPieChart')) {
                instanceOptions = $.extend({}, options, $el.data());
                return $el.data('easyPieChart', new $.easyPieChart(el, instanceOptions));
            }
        });
    };
    return void 0;
})(jQuery);
var alive = true;
var ShowTimeOutWarning = function(){
//    console.log('not alive');
    alive = false;
    
}
var timeoutTime = 900000;
var timeoutTimer = setTimeout(ShowTimeOutWarning, timeoutTime);
$(document).ready(function() {
    $('body').mousemove( function(event) {
        if(alive==false){
//            console.log('back to alive m'); 
            alive=true;
        }
        clearTimeout(timeoutTimer);
        timeoutTimer = setTimeout(ShowTimeOutWarning, timeoutTime);
    });
    $('body').bind('mousedown keydown touchstart', function(event) {
        if(alive==false){
//            console.log('back to alive');
            alive=true;
        }
        clearTimeout(timeoutTimer);
        timeoutTimer = setTimeout(ShowTimeOutWarning, timeoutTime);
    });
});

$(document).ready(function() {
    
    function initial_progress_bar() {
        //console.log('xxxx');
        /*if($('.progress_cv').length==0){
         var obj = $('<div class="progress_cv"><div class="progress_small"><div class="running_task" style="display:none;"><div class="running_icon" style="display: inline-block;position: absolute;left: 0;bottom: 0px;"><i class="icon_chk icon-spinner icon-spin orange bigger-300"  ></i></div><div class="running_text" style="width:165px;display: inline-block;margin: 0 0 10px 40px;float: left;"></div></div><div class="easy-pie-chart percentage" data-percent="0" data-size="46"  ><span class="percent">0</span>%</div></div><div class="progress_full"><div class="close_pfull active"></div><ul></ul></div></div>');
         $(obj).appendTo('body');
         
         }*/
    }

    initial_progress_bar();
    var timeout = 5000;
    var ref_timeout = 5000;
    var pfin = false;
    var pst = false;
    var delay = 2000;
    var sum_percent = 0;
    var charts = $('.progress_cv .progress_small .percentage');
    var running_box = $('.progress_cv .running_task');
    var oldie = /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase());
    var complete_count = 0
    var lost_count = 0;
    var max_lost = 10;
    var job_status = {'running':0,'error':0,'success':0};
    var glob_dp = true;
    var glob_just_change = false;
    var limit_display = 10;
    var last_action = '';
    function call_update(waiting) {
        if(alive==false){
            setTimeout(call_update, timeout);
            return;
        }
        online = window.navigator.onLine;
        if (!online) {
            console.log('Browser offline!');
            lost_count++;
            if (lost_count < max_lost) {
                setTimeout(call_update, timeout + (lost_count*1000) );
            }
            return;
        }
        job_status =  {'running':0,'error':0,'success':0};
//        try{
        //$.ajax({url: "ajax_process_update", dataType: "json", timeout: timeout+ (lost_count*1000),
        $.ajax({url: base_url+"ajax_task/ajax_process_update.php",data:{time:waiting}, dataType: "json", timeout: timeout+ (lost_count*1000),
            beforeSend: function(jqXHR, setting) {
                jqXHR.fail(function() {
                    jqXHR.abort();
                    lost_count++;
                    if (lost_count < max_lost) {
                        setTimeout(call_update, timeout + (lost_count*1000));
                    }
                })
            },
            success: function(r) {
                lost_count = 0;
                var running = 0;
                complete_count = 0;
                if ( r !==null && typeof r === 'object' &&  r.length != 0 && typeof r.token_error != 'undefined') {
                    var msg = r.msg;
                     
                    var type = r.case;
                    var display_msg ='';
                    if(type==1){
                        type='bad'; 
                        display_msg = $txt_other_login;
                        notification_alert(display_msg,type);
                    return;
                    }else{
                        type='good';
                        display_msg = $txt_admin_login;
                        $.ajax({url: base_url+"users/ajax_get_utk_ref" ,dataType: "json",
                             success: function(tk) {
                                 if(tk.utk && tk.utk !=''){
                                     $.ajaxSetup({
                                        headers: { 'X-Auth-Token': tk.utk } 
                                     });
                                     notification_alert(display_msg,type); 
                                 }else{
                                     type='bad'; 
                                     notification_alert(display_msg,type); 
                                 }
                             }
                        });
//                        setTimeout(function(){
//                            location.reload();
//                        },ref_timeout);
                        
                    }
                    
                }
                
                if ( r !==null && typeof r === 'object' &&  r.length != 0) {
                    var sum_progress = 0;
                    var i = 0;
                    if(typeof r.dp != 'undefined'){
                        if(glob_dp!=r.dp){
                            glob_just_change=true;
                        }else{
                            glob_just_change=false;
                        }
                        set_progress_display(r.dp);
                        glob_dp = r.dp;
                    }
                    
                    if(last_action !==''){
                        glob_dp = last_action;
                    }
                    
                    //var proc_id = []; 
                    if (r.proc) {
                        pst = true;
                        pfin = false;
                        var proc = r.proc;
                        $.each(proc, function(id, info) {
                            //proc_id.push(id);
                            add_process(info,i);
                            sum_progress += info.progress;
                            i++;
                            if (info.status == '1')
                                running++;
                        });
                    }
                    //console.log(i,complete_count,running,typeof callback_when_running_process,jQuery.isFunction(callback_when_running_process));
                    if (i == complete_count && complete_count != 0) {
                        if (typeof callback_after_done_process !== 'undefined' && jQuery.isFunction(callback_after_done_process)) { // if define this function they will run after all process complete
                            callback_after_done_process();
                        }
                    } else {

                        if (running > 0) {
                            if (typeof callback_when_running_process !== 'undefined' && jQuery.isFunction(callback_when_running_process)) { // if define this function they will run after all process complete
                                callback_when_running_process();
                            }
                        }
                    }
                    //console.log(i,complete_count,typeof callback_after_done_process);
                    if (i == 0)
                        i++;
                    sum_percent = sum_progress / i;
                    set_sum_prog(sum_percent);
                } 

                if (r!=null && !r.proc && !pst && pfin) {
                    console.log('fire');
                    //$.ajax({url: "/users/ajax_update_process", data: {clear_all_process: 'all'}});
                    $.ajax({url: base_url+"ajax_task/ajax_process_update.php", data: {clear_all_process: 'all'},success:function(){
                            pfin = false;
                            pst = false; 
                    } }); 
                    $('.progress_cv').slideUp("slow", function() {
                        $('.progress_cv .progress_full>ul>li').each(function() {
                            $(this).hide().remove();
                        });
                    });
                    pfin = false;
                    pst = false;
                }
                //console.log(running,pfin,pst,delay,timeout,r.length);
                if (running == 0) { //all process is done
                    if (pfin || pst) {
                        delay = timeout;
                        pfin = true;
                        pst = false;
                    }else if(!pfin && !pst){
                        delay=timeout
                    }

                }
                if(typeof r === 'object'){
                    if ( running != 0) { // process at running 
                        delay = r.delay;
                        timeout = r.timeout;

                    }else{
                        delay = r.timeout;
                        timeout = r.timeout; 
                    }
                }
                set_jobs_status();
                clear_jobs_done(r.proc);
                setTimeout(function(){call_update(delay);}, delay);
            },
            error: function(xhr, status, error) {
                if (xhr.responseText) {
                    console.log('Progress status error :', xhr.responseText);
                    lost_count++;
//                    if(xhr.responseText === 'terminate'){
//                        return;
//                    }
//                    if (lost_count < max_lost) {
//                        setTimeout(call_update, timeout);
//                    }
                }

                //setTimeout(call_update, delay);   
            }
        });
//        }catch(e){
//            console.log('Error',e);
//            lost_count++;
////                     
//            if(lost_count < 10){
//                setTimeout(call_update, timeout);
//            }
//        }
    }
    $(window).bind('beforeunload', function() {
        $.unloading = true;
        console.log('Event catch');
    });
//    window.onerror = function(message, filename, linenumber) {
//        // Perform error reporting here, like logging error message 
//        // with file name and line number on the server
//        // for later processing. 
//        console.log(message, filename, linenumber);
//        lost_count++;
////                     
//        if (lost_count < 100) {
//            setTimeout(call_update, timeout);
//        }
//        return true; // The exception is handled, don't show to the user.
//    }
    function clear_jobs_done(proc){
        var $speed = 'fast'; 
        if(typeof proc != 'undefined'){ 
            var arr_id = [];
            $.each(proc,function(k,info){
                arr_id.push(info.process);
            })
            var num = 0;
            var len = $('.progress_full>ul>li').length;
            $('.progress_full>ul>li').each(function(){
                var id= $(this).attr('data-act'); 
                if($.inArray(id,arr_id) == -1    ){
                    $(this).slideUp($speed,function(){
                        $(this).remove();
                    })
                }else{
                    //console.log(len,limit_display,num)
                    if(len > limit_display && (  len - num >= limit_display)){
                        $(this).slideUp($speed,function(){
                            $(this).remove();
                        })
                    }
                    num++;
                }
            })
//            if(num==0){
//                setTimeout(function(){
//                 $.ajax({url: base_url+"/ajax_task/ajax_process_update.php", data: {clear_all_process: 'all'}});
//                 },600);
//            }
        }
    }
    function set_jobs_status(){
        var run = job_status['running'];
        var suc = job_status['success'];
        var err = job_status['error'];
        var $speed = 'fast';
        var obj = $('.progress_small li.prun');
        var task = run;
        var sum =0;
        if(task>0){
            obj.show($speed);
            obj.find('span').html(task)
        }else{
            obj.hide($speed);
        }
        sum+=task;
        var obj = $('.progress_small li.psuc');
        var task = suc;
        if(task>0){
            obj.show($speed);
            obj.find('span').html(task)
        }else{
            obj.hide($speed);
        }sum+=task;
        var obj = $('.progress_small li.perr');
        var task = err;
        if(task>0){
            obj.show($speed);
            obj.find('span').html(task)
        }else{
            obj.hide($speed);
        }
        sum+=task;
        var obj = $('.progress_small li.pclose');
        var obj2 = $(".progress_small li.ptitle");
        if(sum==0){
            obj.show($speed);
            obj2.hide($speed);
        }else{
            obj2.show($speed);
            obj.hide($speed);
        }
    }
    function set_sum_prog(perc) {
        var o = $('.progress_cv .progress_small .percentage');
        if (o.data('easyPieChart'))
            o.data('easyPieChart').update(perc);
        o.find('span').text(perc);
    }
    function add_process(info,i) {
        var hide = true;
        if(i<limit_display){
            if (info.action === 'import') {
                c = 'progress-warning';
            } else {
                c = 'progress-warning';
            }
            if ($('.progress_cv').is(":visible")) {
                hide = false;
            }
    //          var old_obj = $('.progress_cv .progress_full ul li[data-act="'+info.process+'"]');
    //          if(old_obj.length>0){
    //            old_obj.each(function(){
    //              var o_bid = $(this).attr('data-pid');
    //              var type = $(this).attr('data-type');
    //              if(o_bid!=info.pid && type == 'import'){
    //                  $.ajax({url:"/users/ajax_update_process",data:{clear_process:o_bid},async:false});
    //
    //                  $(this).slideUp('slow',function(){
    //                      $(this).remove(); 
    //                  })
    //              }
    //            });
    //          }

            var obj = $('.progress_cv .progress_full>ul>li[data-act="' + info.process + '"]');
            var shop_name = '';
            if (info.name != '') {
                shop_name = ' (<span>' + info.name + '</span>)';
            }
            var priority = (obj).attr('data-priority');
            var new_line = false; 
            if (obj.length === 0) {
                new_line = true;
                //obj = '<li style="display:none;" data-bid="' + info.bid + '" data-pid="' + info.pid + '" data-sid="' + info.sid + '" data-act="' + info.process + '" data-type="' + info.action + '" data-stat="' + info.status + '" data-priority="' + info.priority + '"><div class="progress_row"><div class="progress_message ' + info.action + '" >' + info.msg + shop_name + '</div><div class="progress ' + c + ' progress-small progress-striped active" data-percent="' + info.progress + '%"><div class="bar" style="width: ' + info.progress + '%;"></div></div></div></li>';
                if(info.progress == 100){
                    obj = '<li class="conectimport" style="display:none;" data-bid="' + info.bid + '" data-pid="' + info.pid + '" data-sid="' + info.sid + '" data-act="' + info.process + '" data-type="' + info.action + '" data-stat="' + info.status + '" data-priority="' + info.priority + '"><div class="progress_row"><p class="progress_text" ><i class="cb-store"></i>' + info.msg +' '+ shop_name + '</p><div class="progress_text m-0"><i class="fa fa-check true-green"></i> '+$txt_complete+'</div></div></li>';
                }else{
                    obj = '<li class="conectimport" style="display:none;" data-bid="' + info.bid + '" data-pid="' + info.pid + '" data-sid="' + info.sid + '" data-act="' + info.process + '" data-type="' + info.action + '" data-stat="' + info.status + '" data-priority="' + info.priority + '"><div class="progress_row"><p class="progress_text" ><i class="cb-store"></i>' + info.msg +' '+ shop_name + '</p><div class="progressCondition"><div class="progressLine" data-value="' + info.progress + '"></div></div></div></li>';
                }
                $(obj).hide(); 
                $('.progress_full>ul').append(obj).each(function(){$(obj).slideDown('slow');});
                var obj = $('.progress_cv .progress_full>ul>li[data-act="' + info.process + '"]');
                info.nsig = true;
            } else {
    //                if(info.priority > priority ){
    //                    obj.find('.progress_message').html(info.msg+shop_name);
    //                     obj.attr('data-priority',info.priority);
    //                }
                 if(info.priority != 0 && info.priority > priority ){
                     obj.find('p.progress_text').html(info.msg + shop_name);
                     obj.attr('data-priority',info.priority);
                 }else if(info.priority==0){
                     obj.find('p .progress_text').html(info.msg + shop_name);
                 }


                //if(info.progress != 100)
                    //obj.find('.progress_text.m-0').html('').removeClass('progress_text').removeClass('m-0').addClass('progressCondition progressLine');
            }
    //            var priority = obj.attr('data-priority');
    //            if(info.priority < priority ) return; 
            var msg = '';
            var err_msg = ''; 
            if (info.err_msg != '') {
                //obj.parents('.progressbar').addClass('has-error');
                err_msg = info.err_msg;
            } else if (info.proc_msg != '') {
    //            obj.parents('.progressbar').addClass('has-error');
                msg = info.proc_msg;
            }
        
        

            if (typeof info.proc_msg === 'object' || info.popup_display === false)
            {
                //obj.parents('.progressbar').addClass('has-error');
                var ori_msg;
                if (info.err_msg != '')
                {
                    //obj.parents('.progressbar').addClass('has-error');
                    ori_msg = encodeURIComponent(JSON.stringify(info.err_msg));
                }
                else if (info.proc_msg != '')
                {
                    //obj.parents('.progressbar').addClass('has-error')
                    ori_msg = encodeURIComponent(JSON.stringify(info.proc_msg));
                }

                var mkp = info.marketplace;
                if(mkp==''){mkp="global";}
                var txt = $('.progress_cv .progress_full>ul>li[data-act="' + info.process + '"]').html();
                if((info.nsig || txt=='' || glob_just_change) && glob_dp){
                $.ajax({
                    url: base_url+"tasks/display_popup_template/" + mkp + "/" + ori_msg,
                    success: function(r) {

                        //Amazon
                        if ($('#actions').length > 0){
                            var div_status = $('#actions').find('.status');
                            $.each(div_status, function() {
                                if ($(this).show())
                                {
                                    $(this).hide();
                                    $('#actions').find('button').attr('disabled', 'disabled');
                                }
                            });
                        }
                        shop_name='';
                        if (info.name != '') {
                            shop_name = ' (<span>' + info.name + '</span>)';
                        }
                        if(mkp=='global'){
                        var $title = '<p class="progress_text"><i class="cb-store"></i>'+info.process+shop_name+'</p>';
                        }else{
                            $title = '';
                        }
                        obj = $('.progress_cv .progress_full>ul>li[data-act="' + info.process + '"]');
                        obj.parents('.conectimport').removeClass('has-error');
                        $(obj).html($title+r);
    //                    running_box.find('.running_text').html(info.msg);
    //                    running_box.show();
    //                    charts.hide();

                        if (hide) {
                            $(".progress_cv").delay(300).slideDown('slow');
    //                        console.log('hide');
                        }

                        var obj = $('.progress_cv .progress_full>ul>li[data-bid="' + info.bid + '"]');
                        $(obj).delay(500).slideDown('slow');
                    }
                });
                }

            } else {
                obj.parents('.conectimport').removeClass('has-error');
                msg = capitaliseFirstLetter(msg);
                obj.show();
                if (info.s_status === 'complete') {
                    //obj.find('.progress').removeClass(c).removeClass('active').addClass('progress-success');
                    //obj.find('.progress').attr('data-percent', 'Completed !!');
                    //obj.find('.progress .bar').css('width', '100%');
                    obj.find('.progressCondition .progressLine').attr('data-value', 100);
                    //setTimeout(function() {
                          // Do something after 3 seconds
                           obj.find('.progressCondition').delay( 3000 ).removeClass('progressCondition').addClass('progress_text m-0').html('<i class="fa fa-check true-green"></i> '+$txt_complete+'</div>');//.html('<div class="progressLine" data-value="0"></div>');
                           //Check remove if it's complete

                   // }, 3000);

                        if(obj.length > 2){
                            setTimeout(function(){
                                obj.each(function(){
                                    if($(this).attr('data-stat') == 9)
                                        $(this).slideDown("slow", function() { 
                                            $(this).remove();
                                            $.ajax({url: base_url+"/ajax_task/ajax_process_update.php", data: {clear_all_process: 'all'}});
                                        });
                                });
                            }, 5000);
                        }

                    complete_count++;
                } else if (info.status === 4 || info.status === 8 || info.s_status ==='error') {
                    obj.find('.progress').removeClass(c).removeClass('active').removeClass('progress-success').addClass('progress-pink');

                    if (err_msg != '') {
                        //obj.find('.progress').attr('data-percent', msg);
                        obj.addClass('has-error');
    //                    obj.find('.progress_row .progress_text.m-0').addClass('error').html(err_msg);
                        obj.find('.progressCondition .progressLine').attr('data-value', 0);
                        obj.find('.progressCondition').delay( 3000 ).removeClass('progressCondition').addClass('progress_text m-0').html(err_msg);
                        obj.find('.progress_text.m-0').html(err_msg);
                        obj.find('.error').show();

                    } else {
                        //obj.find('.progress').attr('data-percent', 'Error!! Please try again. ' + msg);
                         obj.addClass('has-error');
                         obj.find('.progress_text.m-0').addClass('error').html('').removeClass('progress_text').removeClass('m-0').addClass('progressCondition').html('<div class="progressLine" data-value="0"></div>');
                         obj.find('.progressCondition .progressLine').attr('data-value', info.progress);
                    }
                    //obj.find('.progress .bar').css('width', '100%');
                    obj.find('.progressCondition .progressLine').attr('data-value', 100);
                } else {
                    //obj.find('.progress').addClass(c).removeClass('progress-pink').removeClass('progress-success');
                        obj.find('.progress_text.m-0').removeClass('error').html('').removeClass('progress_text').removeClass('m-0').addClass('progressCondition').html('<div class="progressLine" data-value="0"></div>');
                    //if (!obj.find('.progress').hasClass('active'));
                    //    obj.find('.progress').addClass('active');
                    if (msg && msg != '' && info.progress * 1 == 0) {
                        //obj.find('.progress').attr('data-percent', msg + ' ');
                        //obj.find('.progress .bar').css('width', '100%');
                        obj.find('.progressCondition .progressLine').attr('data-value', 0);
                    } else if (msg != '') {
                        //obj.find('.progress').attr('data-percent', msg + ' : ' + info.progress + '%');
                        //obj.find('.progress .bar').css('width', info.progress + '%');
                        obj.find('.progressCondition .progressLine').attr('data-value', info.progress);
                    } else {
                        //obj.find('.progress').attr('data-percent', info.progress + '%');
                       // obj.find('.progress .bar').css('width', info.progress + '%');
                       obj.find('.progressCondition .progressLine').attr('data-value', info.progress);
                    }

                }

                //Add Separate class conectimport
                var counterboxim =  obj.parents('ul').find('li.conectimport').length;
                obj.parents('ul').find('li.conectimport:not(:last-child)').each(function(){
                    if(counterboxim >= 2){
                        $(this).addClass('b-Bottom');
                    }else{
                        $(this).removeClass('b-Bottom');
                    }
                });

                if (hide) {
                    $(".progress_cv").delay(300).slideDown('slow');
                }
                var obj = $('.progress_cv .progress_full>ul>li[data-bid="' + info.bid + '"]');
                $(obj).delay(500).slideDown('slow');
            }
        }
        
        var count_job=0;
        if (info.s_status === 'complete' || info.status === 9) {
            job_status['success']++;
            count_job++;
        }else if (info.status === 4  ) {
            job_status['error']++;
            count_job++;
        }else{
            job_status['running']++;
            count_job++;
        }
        if(count_job>0){
            $('div.progress_cv').show();
        }
    }

    function set_progress_display(status){
        $speed = 'fast';
        if(last_action===''){
            if(!status){
                $('.progress_cv .progress_full').slideUp($speed);
                $('.progress_cv .progress_small').slideDown($speed, 'linear');
            }else{
                $('.progress_cv .progress_full').slideDown($speed);
                $('.progress_cv .progress_small').slideUp($speed, 'linear');
            }
        }
    }
    var initCharts = function() {
//        charts.easyPieChart({
//            barColor: '#87B87F',
//            trackColor: '#EEEEEE',
//            scaleColor: false,
//            lineCap: 'butt',
//            lineWidth: 3,
//            animate: oldie ? false : 1000,
//            size: $(this).data('size'),
//            onStep: function(value) {
//                this.$el.find('span').text(~~value);
//            }
//        });

//        $('.progress_cv .progress_small').click(function() {
//            if (!$('.progress_cv .close_pfull').hasClass('active')) {
//                $('.progress_cv .close_pfull').addClass('active');
//                $('.progress_cv .progress_small').fadeOut('slow', 'linear');
//                $('.progress_cv .progress_full').slideDown('slow', 'swing');
//            }
//        });
        var $speed = 'slow';
        $('.progress_cv .progress_min').on('click',function(e){ 
            last_action = false;
            $('.progress_cv .progress_full').slideUp($speed,function(){
                $('#progressbar.progress_cv').animate({left:0,top:$(window).height() },$speed); 
            });
            $('.progress_cv .progress_small').slideDown($speed, 'linear');
            
            
            $.ajax({url: base_url+"/ajax_task/ajax_process_update.php", data: {progress_display: 'hide'}}); 
        });
        $('.progress_cv .progress_max').on('click',function(e){
            last_action = true;
            $('.progress_cv .progress_full').slideDown($speed);
            $('.progress_cv .progress_small').slideUp($speed, 'linear');
            $.ajax({url: base_url+"/ajax_task/ajax_process_update.php", data: {progress_display: 'show'}}); 
        });
        $('.progress_cv .close_pfull').on('click', function(e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                //Amazon
                if ($('#actions').length > 0){
                    $('#actions').find('button').removeAttr('disabled');
                }
//                if (pst && !pfin) {
//                    $(this).removeClass('active');
//                    $('.progress_cv .progress_small').show('fast', 'linear')
//                    $('.progress_cv .progress_full').hide('slow', 'linear')
//                    charts.each(function() {
//                        $(this).data('easyPieChart').update(sum_percent);
//                    });
//                } else if (!pst && pfin) {
                    //$.ajax({url: "/users/ajax_update_process", data: {clear_all_process: 'all'}});
                    $.ajax({url: base_url+"/ajax_task/ajax_process_update.php", data: {clear_all_process: 'all'}}); 
                    $('.progress_cv').slideUp("slow", function() {
                        $('.progress_cv .progress_full>ul>li').each(function() {
                            $(this).hide().remove();
                            pfin = false;
                            pst = false;
                            //console.log($(this));
                        });
                    });
//                }
            }
        });

    }
    initCharts();
    call_update();
    
    function capitaliseFirstLetter(string)
    {
        if (!string || string=='')
            return '';
        string = replaceAll('_', ' ', string.toString());
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    function replaceAll(find, replace_txt, str) {

        if (typeof str !== "undefined" && str && str !='') {
            return str.replace(new RegExp(find, 'g'), replace_txt);
        } else {
            return '';
        }
    }
}); 


