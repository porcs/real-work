<?php
	$_GET['id'] = isset($_GET['id']) ? $_GET['id'] : 'dashboard';
	$id = $_GET['id'];
?>
<?php require_once 'inc/config/all.php'; ?>
<?php require_once 'inc/modules/header.php';?>
<?php require_once 'inc/modules/nav-header-login.php';?>
<?php 
	if ($id == 'billing-packages-extend-wait'){
		echo '<div class="notification wait">
				<div class="container">
					<p>
					Please wait...
					</p>
				</div>
			</div>';
	}
?>
<div class="page-wrapper">
	<div class="wrapper">
		<?php require_once 'inc/modules/sidebar.php';?>
		<div class="main" id="content">
			<?php 
				require_once "inc/pages/" . $id . ".php"; 
			?>
		</div>	
	</div>
</div>
<div class="progressbar">
	<p class="progress_text">
		<i class="fa fa-remove"></i>
		<i class="cb-store"></i>
		Process Delete: Download file response from eBay
	</p>
	<div class="progressCondition">
		<div class="progressLine" data-value="60"></div>
	</div>
</div>

<div class="progressbar">
	<p class="progress_text">		
		<i class="fa fa-remove"></i>		
		Process Delete: Download file response from eBay
	</p>
	<p class="progress_text m-0">
		<i class="fa fa-check true-green"></i>
		COMPLETED!
	</p>	
</div>

<?php
	if ($id == 'billing-packages-extend-wait'){
		echo '<div class="shadow"></div>';
	}
?>
<?php require_once 'inc/modules/footer.php';?>