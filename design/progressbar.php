<?php require_once 'inc/modules/header.php';?>
<div class="container">
	<div class="progressbar-view">
		<div class="row">
			<div class="col-sm-6">
				<div class="progressbar">
					<p class="progress_text">
						<i class="fa fa-remove"></i>
						<i class="cb-store"></i>
						Process Delete: Download file response from eBay
					</p>
					<div class="progressCondition">
						<div class="progressLine" data-value="60"></div>
					</div>
				</div>
				<br/>
				<div class="progressbar">
					<p class="progress_text">		
						<i class="fa fa-remove"></i>		
						Process Delete: Download file response from eBay
					</p>
					<p class="progress_text m-0">
						<i class="fa fa-check true-green"></i>
						COMPLETED!
					</p>	
				</div>
				<div class="progressbar">
					<p class="progress_text">		
						<i class="fa fa-remove"></i>		
						Process Delete: Download file response from eBay
					</p>
					<p class="progress_text m-0 error">
						<i class="fa fa-exclamation-triangle"></i>
						Empty Products
					</p>	
				</div>
				<div class="progressbar has-error">
					<p class="progress_text">
						<i class="fa fa-remove"></i>
						Error processing!
					</p>
					<div class="progressCondition">
						<div class="progressLine" data-value="60"></div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="progressbar">
					<p class="progress_text">Amazon.com synchronization started on</p>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Bottom m-b10">
								<div class="row">
									<div class="col-sm-6 ">
										<div class="progress-date b-BlRight">
											<p class="progress-date_text"><span>Date:</span> 2015 - 02 - 03</p>
										</div>							
									</div>
									<div class="col-sm-6">
										<div class="progress-date">
											<p class="progress-date_text"><span>Time:</span> 02 : 22 : 17</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<p class="progress_text m-0 true-pink">
						<i class="fa fa-remove"></i>
						Preparing products...
					</p>	
				</div>
				<br/>				
				<div class="progressbar">
					<p class="progress_text">Amazon.com synchronization started on</p>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Bottom m-b10">
								<div class="row">
									<div class="col-sm-6 ">
										<div class="progress-date b-BlRight">
											<p class="progress-date_text"><span>Date:</span> 2015 - 02 - 03</p>
										</div>							
									</div>
									<div class="col-sm-6">
										<div class="progress-date">
											<p class="progress-date_text"><span>Time:</span> 02 : 22 : 17</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<p class="progress_text true-pink">
						<i class="fa fa-remove"></i>
						Preparing products...
					</p>	
					<p class="progress_small-text">Inventory, waiting.</p>
					<p class="progress_small-text">Price, waiting.</p>
					<p class="progress_small-text">Image, waiting </p>
				</div>
				<br/>
				<div class="progressbar">
					<p class="progress_text">
						<i class="fa fa-remove"></i>
						<i class="cb-store"></i>
						Importing your products (Presta Store Test)
					</p>
					<div class="progressCondition">
						<div class="progressLine" data-value="60"></div>
					</div>
				</div>
				<br/>
				<div class="progressbar">
					<p class="progress_text">Amazon.com synchronization started on</p>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Bottom m-b10">
								<div class="row">
									<div class="col-sm-6 ">
										<div class="progress-date b-BlRight">
											<p class="progress-date_text"><span>Date:</span> 2015 - 02 - 03</p>
										</div>							
									</div>
									<div class="col-sm-6">
										<div class="progress-date">
											<p class="progress-date_text"><span>Time:</span> 02 : 22 : 17</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<p class="progress_text error">
						<i class="fa fa-remove"></i>
						Errors:
					</p>	
					<ol class="progress-list_error">
						<li><p>Missing items for order #028 - 785965-354654</p></li>
						<li><p>Missing items for order #028 - 785965-354654</p></li>
						<li><p>Missing items for order #028 - 785965-354654</p></li>
					</ol>
					<p class="progress_text warning">
						<i class="fa fa-remove"></i>
						Warnings:
					</p>	
					<ol class="progress-list_warning">
						<li><p>Order ID (028-25648-23534) Cold not import product</p>
    						<p>SKU : 9708 - Being - T39</p></li>
						<li><p>Missing items for order #028 - 785965-354654</p></li>
						<li><p>Missing items for order #028 - 785965-354654</p></li>
					</ol>
					<div class="b-Bottom p-b10 text-right">
						<a href="" class="link text-uc p-size">SEE ALL MESSAGES <i class="fa fa-long-arrow-right m-r10"></i></a>
					</div>
					<p class="progress_text m-t10">
						<i class="fa fa-remove"></i>
						<i class="cb-store"></i>
						Importing your products (Presta Store Test)
					</p>
					<div class="progressCondition">
						<div class="progressLine" data-value="60"></div>
					</div>
				</div>
				<br/>
			</div>
		</div>
	</div>
</div>

<?php require_once 'inc/modules/footer.php';?>