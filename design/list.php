<?php require_once 'inc/modules/header.php';?>
<div class="container-fluid p-rl40">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="text-center">Feed.biz list pages</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h4 class="text-center">Pages number: <span></span></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<p class="text-left"><a href="guidline.php">Guidline</a></p>
		</div>
		<div class="col-sm-6">
			<p class="text-right"><a href="progressbar.php">Progressbar</a></p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-2">
			<h2 class="text-center">Sign In</h2>
			<ul>
				<li>
					<a href="index.php?id=sign-in">Sign in</a>
					<ul>
						<li>
							<a href="index.php?id=sign-in-alert">Alert</a></li>
					</ul>
				</li>
				<li>
					<a href="index.php?id=forgot-password">Forgot Password</a>
					<ul>
						<li><a href="index.php?id=forgot-password-alert">Alert</a></li>
					</ul>
				</li>
				<li>
					<a href="index.php?id=create-account">Create account</a>
					<ul>
						<li><a href="index.php?id=create-account-alert">Alert</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=reset-password">Reset password</a>
					<ul>
						<li><a href="index.php?id=reset-password-alert">Alert</a></li>
					</ul>
				</li>	
			</ul>
		</div>
		<div class="col-sm-3">
			<h2 class="text-center">Main site</h2>
			<ul>
				<li><a href="index.php?id=404">404</a></li>
				<li><a href="index.php?id=dashboard">Dashboard</a>
					<ul>
						<li>Registration Wizard 
							<ul>
								<li>Amazon
									<ul>
										<li><a href="index.php?id=wizard-1">Step 1</a></li>
										<li><a href="index.php?id=wizard-2">Step 2</a></li>
										<li><a href="index.php?id=wizard-3">Step 3</a></li>
										<li><a href="index.php?id=wizard-4">Step 4</a></li>
										<li><a href="index.php?id=wizard-5">Step 5</a></li>
									</ul>
								</li>
								<li>Ebay
									<ul>
										<li><a href="index.php?id=wizard-1">Step 1</a></li>
										<li><a href="index.php?id=wizard-2">Step 2</a></li>
										<li><a href="index.php?id=wizard-3">Step 3</a>
											<ul>
												<li><a href="index.php?id=wizart-3-alert">Alert</a></li>
											</ul>
										</li>
										<li><a href="index.php?id=wizard-4">Step 4</a></li>
										<li><a href="index.php?id=wizard-5">Step 5</a>
											<ul>
												<li><a href="index.php?id=wizard-5-alert">Alert</a></li>
											</ul>
										</li>
										<li><a href="index.php?id=wizard-6">Step 6</a>
											<ul>
												<li><a href="index.php?id=wizard-6-alert">Alert</a></li>
											</ul>
										</li>
										<li><a href="index.php?id=wizard-7">Step 7</a></li>
										<li><a href="index.php?id=wizard-8">Step 8</a></li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="index.php?id=affiliate">Affiliate Lists</a>
					<ul>
						<li><a href="index.php?id=affiliate-empty">Empty</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=edit-billing-information">Edit billing information</a>
					<ul>
						<li><a href="index.php?id=edit-billing-information-alert">Alert</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=edit-profile">Edit profile</a>
					<ul>
						<li><a href="index.php?id=edit-profile-alert">Alert</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=invitations">Invitations</a>
					<ul>
						<li><a href="index.php?id=invitations-alert">Alert</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=statistics">Statistics</a></li>
				<li><a href="index.php?id=billing-packages">Billing packages</a>
					<ul>
						<li><a href="index.php?id=billing-packages-subTotal">Sub Total</a></li>
						<li><a href="index.php?id=billing-packages-2step">2 step</a>						
							<ul>
								<li><a href="index.php?id=billing-packages-2step-alert">Alert</a></li>
							</ul>
						</li>
						<li><a href="index.php?id=billing-packages-extend">Extend your packages</a></li>
						<li><a href="index.php?id=billing-packages-extend-wait">Wait</a></li>	
						<li><a href="index.php?id=billing-packages-settings">Settings</a></li>	
						<li><a href="index.php?id=billing-packages-profile-save">Profile has been saved</a></li>
						<li><a href="index.php?id=billing-packages-download-billing">Download Billing</a></li>	
						<li><a href="index.php?id=billing-packages-payments">Payments</a></li>				
					</ul>
				</li>				
			</ul>
		</div>
		<div class="col-sm-3">
			<h2 class="text-center">My shop, My feed</h2>
			<ul>
				<li><a href="index.php?id=my-products">My products</a></li>
				<li><a href="index.php?id=my-shop">My Shop</a>
					<ul>
						<li><a href="index.php?id=my-shop-loading">Loading</a></li>
						<li><a href="index.php?id=my-shop-success">Success verifying</a></li>
						<li><a href="index.php?id=my-shop-error">Error verifying</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=carriers">Carriers</a></li>
				<li><a href="index.php?id=category">Category</a></li>
				<li><a href="index.php?id=filters">Filters</a>
					<ul>
						<li><a href="index.php?id=filters-empty">Empty</a></li>
					</ul>			
				</li>
				<li>Configuration
					<ul>
						<li><a href="index.php?id=config-general">General</a>
							<ul>
								<li><a href="index.php?id=config-general-mode">Select mode</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="index.php?id=log-batches">Log Batches</a></li>
				<li><a href="index.php?id=import-history">Import history</a></li>						
				<li><a href="index.php?id=messages-log">Messages log</a></li>
				<li><a href="index.php?id=manufactures">Manufacturers</a></li>
				<li><a href="index.php?id=profiles">Profiles</a>
					<ul>
						<li><a href="index.php?id=profiles-alert">Alert</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=rules">Rules</a>
					<ul>
						<li><a href="index.php?id=rules-alert">Alert</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=configuration">Configuration</a></li>	
				<li>Marketplace popap
					<ul>
						<li><a href="index.php?id=marketplace-popap1">First</a></li>
						<li><a href="index.php?id=marketplace-popap2">Second</a></li>
						<li><a href="index.php?id=marketplace-popap3">Third</a></li>
					</ul>
				</li>				
				<li><a href="index.php?id=stock-report">Stock report</a></li>				
			</ul>
		</div>
		
		<div class="col-sm-2">
			<h2 class="text-center">Amazon</h2>
				<ul>
					<li><a href="index.php?id=amazon-actions">Actions</a>
						<ul>
							<li><a href="index.php?id=amazon-actions-alert">Alert</a></li>
							<li><a href="index.php?id=amazon-actions-success">Success</a></li>
							<li><a href="index.php?id=amazon-actions-update">Update</a></li>
						</ul>
					</li>
					<li><a href="index.php?id=amazon-carriers">Carriers</a></li>
					<li><a href="index.php?id=amazon-category">Category</a></li>
					<li><a href="index.php?id=amazon-condition">Condition</a></li>
					<li><a href="index.php?id=amazon-logs">Logs</a>
						<ul>
							<li><a href="index.php?id=amazon-logs-empty">Empty</a></li>
						</ul>
					</li>
					<li><a href="index.php?id=amazon-mapping">Mappings</a></li>
					<li><a href="index.php?id=amazon-models">Models</a></li>
					<li><a href="index.php?id=amazon-orders">Orders</a></li>
					<li><a href="index.php?id=amazon-parameters">Parameters</a>
						<ul>
							<li><a href="index.php?id=amazon-parameters-small-alert">Small alert</a></li>
							<li><a href="index.php?id=amazon-parameters-big-alert">Big alert</a></li>
							<li><a href="index.php?id=amazon-parameters-success">Success</a></li>
						</ul>
					</li>
					<li><a href="index.php?id=amazon-profile">Profile</a>
						<ul>
							<li><a href="index.php?id=amazon-profile-all">All profile</a></li>
							<li><a href="index.php?id=amazon-profile-alert">Alert</a></li>
						</ul>
					</li>
					<li><a href="index.php?id=amazon-report">Report</a></li>
					<li><a href="index.php?id=amazon-status">Status</a> <span class="true-pink">New page</span></li>				
				</ul>
			</li>	
		</div>
		<div class="col-sm-2">
			<h2 class="text-center">Ebay</h2>
			<ul>
				<li><a href="index.php?id=ebay-actions">Actions</a>
					<ul>
						<li>Delete
							<ul>
								<li><a href="index.php?id=ebay-actions-alert">Alert</a>
								<ul>
									<li><a href="index.php?id=ebay-actions-alert-delete">Delete success</a></li>
								</ul>
								</li>
							</ul>
						</li>
						<li>Order
							<ul>
								<li><a href="index.php?id=ebay-actions-order">Import Order</a>
									<ul>
										<li><a href="index.php?id=ebay-actions-order-alert">Alert</a></li>
									</ul>
								</li>
							</ul>
						</li>
						<li><a href="index.php?id=ebay-actions-offer">Offer</a>
							<ul>
								<li><a href="index.php?id=ebay-actions-offer-alert">Alert</a></li>
							</ul>
						</li>
						<li class="">Products
							<ul>
								<li><a href="index.php?id=ebay-actions-products-alert">Alert</a></li>
								<li>
									<a href="index.php?id=ebay-actions-products-send">Send</a>
									<ul>
										<li><a href="index.php?id=ebay-actions-products-send-success">Success</a></li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="index.php?id=ebay-auth">Autentification</a>
					<ul>
						<li>Alert
							<ul>
								<li><a href="index.php?id=ebay-auth-alert-username">Username</a></li>
								<li><a href="index.php?id=ebay-auth-alert-userid">UserID</a></li>
								<li><a href="index.php?id=ebay-auth-alert-incorrect">Incorrect</a></li>
							</ul>
						</li>						
					</ul>
					
				</li>
				<li><a href="index.php?id=ebay-batch-log">Batch Log</a></li>
				<li><a href="index.php?id=ebay-stat">Statistics</a></li>
				<li>Mapping
					<ul>
						<li><a href="index.php?id=ebay-mapping-carriers">Carriers</a>
							<ul>
								<li><a href="index.php?id=ebay-mapping-carriers-2">2 step</a></li>
								<li><a href="index.php?id=ebay-mapping-carriers-alert">Alert</a></li>
							</ul>
						</li>
						<li><a href="index.php?id=ebay-mapping-settings">Settings</a></li>
						<li><a href="index.php?id=ebay-mapping-categories">Categories</a>
							<ul>
								<li><a href="index.php?id=ebay-mapping-categories-alert">Alert</a></li>
							</ul>
						</li>
						<li><a href="index.php?id=ebay-mapping-condition">Condition</a></li>
						<li><a href="index.php?id=ebay-mapping-orders">Orders</a>
							<ul>
								<li><a href="index.php?id=ebay-mapping-orders-sending">Sending</a></li>
							</ul>
						</li>
						<li><a href="index.php?id=ebay-mapping-template">Templates</a></li>
						<li><a href="index.php?id=ebay-mapping-price">Price</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=ebay-settings">Settings</a></li>
				<li><a href="index.php?id=ebay-tax">Tax</a></li>
				<li><a href="index.php?id=ebay-templates">Templates</a></li>				
				<li><a href="index.php?id=ebay-universe">Universe</a>
					<ul>
						<li><a href="index.php?id=ebay-universe-alert">Alert</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<script>
	var $count = $('body').find('li a').length;
		$('body').find('h4 span').text($count - 5);

		//console.log($('a').length - 4);
</script>