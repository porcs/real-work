$(document).ready(function() {
    $('#miniFirst').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        tooltip: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0,
			enabled: false
        },
		credits: {
			enabled: false
		},
		exporting:{
			buttons: {
				contextButton: {
					enabled:false
				}
			}
		},
		xAxis: {
		   lineWidth: 0,
		   minorGridLineWidth: 0,
		   lineColor: 'transparent',
		   labels: {
			   enabled: false
		   },
		   minorTickLength: 0,
		   tickLength: 0
		},
		yAxis: {
		   lineWidth: 0,
		   minorGridLineWidth: 0,
		   lineColor: 'transparent',
		   labels: {
			   enabled: false
		   },
			title: {
				text: ''
			},
			minorTickLength: 0,
		   tickLength: 0
		},
        series: [{
            name: 'Goodies (FOXCHIP)',
			data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }],
    });

    $('#miniSecond').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        tooltip: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0,
			enabled: false
        },
		credits: {
			enabled: false
		},
		exporting:{
			buttons: {
				contextButton: {
					enabled:false
				}
			}
		},
		xAxis: {
		   lineWidth: 0,
		   minorGridLineWidth: 0,
		   lineColor: 'transparent',
		   labels: {
			   enabled: false
		   },
		   minorTickLength: 0,
		   tickLength: 0
		},
		yAxis: {
		   lineWidth: 0,
		   minorGridLineWidth: 0,
		   lineColor: 'transparent',
		   labels: {
			   enabled: false
		   },
			title: {
				text: ''
			},
			minorTickLength: 0,
		   tickLength: 0
		},
        series: [{
            name: 'Goodies (FOXCHIP)',
			data: [7.0, 4.0, 5.5, 2.5, 8.2, 6.5, 10.2, 9.5, 14.3, 18.3, 13.9, 9.6]
        }],
    });

    $('#miniThird').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        tooltip: {
            enabled: false
        },
        legend: {
			enabled: false
        },
		credits: {
			enabled: false
		},
		exporting:{
			buttons: {
				contextButton: {
					enabled:false
				}
			}
		},
		xAxis: {
		   lineWidth: 0,
		   minorGridLineWidth: 0,
		   lineColor: 'transparent',
		   labels: {
			   enabled: false
		   },
		   minorTickLength: 0,
		   tickLength: 0
		},
		yAxis: {
		   lineWidth: 0,
		   minorGridLineWidth: 0,
		   lineColor: 'transparent',
		   labels: {
			   enabled: false
		   },
			title: {
				text: ''
			},
			minorTickLength: 0,
		   tickLength: 0
		},
        series: [{
            name: 'Goodies (FOXCHIP)',
			data: [22.0, 18.0, 20.5, 15.5, 13.2, 14.5, 15.2, 8.5, 19.3, 18.3, 13.9, 18.6]
        }],
    });

    $('#product').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        exporting:{
            buttons: {
                contextButton: {
                    enabled:false
                }
            }
        },
       
        scrollbar:{
            enabled:false
        },
        navigator:{
            enabled:false
        },        
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: "Value: {point.y:,.1f} "
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Browser share',
             data: [
                ['Bananas', 8],
                ['Kiwi', 3],
                ['Mixed nuts', 1],
                ['Oranges', 6],
                ['Apples', 8],
                ['Pears', 4],
                ['Clementines', 4],
                ['Reddish (bag)', 1],
                ['Grapes (bunch)', 1]
            ]
        }] 
    });
    $('#order').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        exporting:{
            buttons: {
                contextButton: {
                    enabled:false
                }
            }
        },
        scrollbar:{
            enabled:false
        },
        navigator:{
            enabled:false
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: "Value: {point.y:,.1f} "
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        xAxis: {
          gridLineWidth: 1,
          lineColor: 'red',
          tickColor: 'red',
          labels: {
             style: {
                fill: 'red',
                font: '11px Trebuchet MS, Verdana, sans-serif'
             }
          },
          title: {
             style: {
                fill: 'red',
                fontWeight: 'bold',
                fontSize: '12px',
                fontFamily: 'Trebuchet MS, Verdana, sans-serif'

             }
          }
       },
        yAxis: {
            labels:{
                style: {
                    fill: 'red',
                    'font-family':'Open Sans',
                    fontSize:'12px',
                    fontWeight:'bold'
                }
            }
        },       
        series: [{
            name: 'Browser share',
             data: [
                ['Bear', 8],
                ['Racoon', 9],
                ['Wolf', 3],
                ['Mouse', 6],
                ['Horse', 7],
                ['Man', 4],
                ['Hightingale', 7],
                ['Cow', 3]
            ]
        }] 
    });

    /*$.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=new-intraday.json&callback=?', function (data) {        
        $('#sale').highcharts('StockChart', {
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            exporting:{
                buttons: {
                    contextButton: {
                        enabled:false
                    }
                }
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                 labels: {
                    format: '{value:%Y-%m-%d}'
                 }
            },
            scrollbar:{
                enabled:false
            },
            navigator:{
                enabled:false
            },
            rangeSelector : {
                allButtonsEnabled:true,
                width : 848,
                height : 20,
                buttons : [{
                    type : 'day',
                    count : 1,
                    text : 'DAY',
                    id: 'button1'
                }, {
                    type : 'week',
                    count : 1,
                    text : 'WEEK',
                    id: 'button2'
                }, {
                    type : 'month',
                    count : 1,
                    text : 'MONTH',
                    id: 'button3'
                }],
                selected : 0,
                inputEnabled : false
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: "Value: {point.y:,.1f} "
            },
            series : [{
                name : 'AAPL',
                type: 'area',
                data : data,
                gapSize: 5,
                tooltip: {
                    valueDecimals: 2
                },
                 fillColor : {
                    linearGradient : {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops : [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                pointStart: Date.UTC(2015, 0, 1),
                pointInterval: 365 * 36e5,
                threshold: null
            }],            
        });
    });*/
    $('#column').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        exporting:{
            buttons: {
                contextButton: {
                    enabled:false
                }
            }
        },
        legend: {
            enabled: false
        },
        scrollbar:{
            enabled:false
        },
        navigator:{
            enabled:false
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: "Value: {point.y:,.1f} "
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        xAxis: {
            enabled: false
          /*gridLineWidth: 1,
          lineColor: '#e2e2e2',
          labels: {
             style: {
                fill: '#e2e2e2',
                font: '11px Trebuchet MS, Verdana, sans-serif'
             }
          },
          title: {
             style: {
                fill: 'red',
                fontWeight: 'bold',
                fontSize: '12px',
                fontFamily: 'Trebuchet MS, Verdana, sans-serif'

             }
          }*/
       },
        yAxis: {
            enabled: false
            /*labels:{
                style: {
                    fill: '#e2e2e2',
                    'font-family':'Open Sans',
                    fontSize:'12px',
                    fontWeight:'bold'
                }
            }*/
        },       
        series: [{
            name: 'Browser share',
             data: [
                ['Bear', 8],
                ['Racoon', 9],
                ['Wolf', 3],
                ['Mouse', 6],
                ['Horse', 7],
                ['Man', 4],
                ['Hightingale', 7],
                ['Cow', 3]
            ]
        }] 
    });
});