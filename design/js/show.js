$(document).ready(function() {	
	$(document).on('click', '.showProfile', function(e){
		e.preventDefault();

		$(this).closest('.row').next().clone(true).insertAfter($(this).closest('.row').next()).show();
		countBlock($('.countProfile'), $('.showSelectBlock'));			
		choSelect();		
	});

	$(document).on('click', '.showAmazonProfile', function(e){
		e.preventDefault();

		$(this).closest('.row').next().next().clone(true).insertAfter($(this).closest('.row').next()).show();
		countBlock($('.countProfile'), $('.showSelectBlock'));			
		choSelect();		
	});

	$(document).on('click', '.showSet', function(e){
		e.preventDefault();
		var $this 				= $(this),
			$showSelectBlock 	= $this.closest('.showSelectBlock'),
			$ruleCheckBo 		= $showSelectBlock.find('.ruleSettings').find('.checkBo'),
			$addCheckBo 		= $showSelectBlock.find('.setBlock').last().find('.addCheckBo'),
			$rowCount 			= $ruleCheckBo.length,
			$choZN 				= $showSelectBlock.find('.ruleSettings').find('.choZn'),
			$addSelect 			= $showSelectBlock.find('.setBlock').last().find('.addSelect'),

		$templateBlock 	= '<div class="setBlock clearfix">'+
						'<div class="m-b10 clearfix">'+
							'<a href="" class="pull-right removeSet">Remove set <i class="cb-remove"></i></a>'+
						'</div>'+
						'<div class="setBlock_content">'+
							'<div class="setBlock_text">'+									
								
							'</div>'+
						'</div>'+
					'</div>',
		$templateText 	= '<div class="row addSettings">'+
							'<div class="col-sm-6 addCheckBo">'+

							'</div>'+
							'<div class="col-sm-6 addSelect">'+

							'</div>'+
						'</div>';
		choSelect();

		$showSelectBlock.find('.setBlockMain').removeClass('hidden');

		$($templateBlock).insertBefore($this.parent());

		$($templateText).appendTo($showSelectBlock.find('.setBlock').last().find('.setBlock_text'));
		
		$showSelectBlock.find('.setBlock').removeClass('hidden');

		for (var i=0; i < $rowCount; i++){
			$($templateText).appendTo($showSelectBlock.find('.setBlock').last().find('.setBlock_text'));
			
			$ruleCheckBo.eq(i).clone(true).appendTo($addCheckBo.eq(i));
			$choZN.eq(i).clone().appendTo($addSelect.eq(i));

			choSelect();
		}

		$showSelectBlock.find('.setSettings').show();

		$showSelectBlock.find('.setBlock').last().addClass('hidden');

		if($this.closest('.profileBlock').find('.setBlockMain')){
			$this.closest('.profileBlock').find('.countRule').text($('.setBlock').length + $('.setBlockMain').length - 2);
		}
		else{
			$this.closest('.profileBlock').find('.countRule').text($('.setBlock').length);
		}			
	});

	$(document).on('click', '.removeSet', function(e){
		e.preventDefault();
		var $this 				= $(this),
			$showSelectBlock 	= $this.closest('.showSelectBlock'),
			$countRule			= $this.closest('.profileBlock').find('.countRule');

		if($this.closest('.profileBlock').find('.setBlockMain')){
			$countRule.text($('.setBlock').length + $('.setBlockMain').length - 1);
		}
		else{
			$countRule.text($('.setBlock').length);
		}
		$showSelectBlock.find('.setSettings').hide();
		$this.closest('.setBlock').remove();

		$this.closest('.setBlockMain').remove();	
	});

	$(document).on('click', '.removeProfile', function(e){
		e.preventDefault();	
		var $ruleCheckBo = 	$('.ruleSettings').find('.checkBo').length;

		$(this).closest('.showSelectBlock').remove();

		countBlock($('.countProfile'), $('.showSelectBlock'));
		
		$('.setBlockMain').addClass('hidden');
		
		$('.setBlock').remove();			
	})
}) // close Ready