(function() {
  var $;
  $ = jQuery;
  $.fn.sieve = function(options) {
    var compact;
    compact = function(array) {
      var item, _i, _len, _results;
      _results = [];
      for (_i = 0, _len = array.length; _i < _len; _i++) {
        item = array[_i];
        if (item) {
          _results.push(item);
        }
      }
      return _results;
    };
    return this.each(function() {
      var container, searchBar, settings;
      container = $(this);
      settings = $.extend({
        searchInput: null,
        itemSelector: "li p",
        textSelector: null,
        toggle: function(item, match) {
          return item.toggle(match);
        },
        complete: function() {		
        /*var visible = $('.chosen-results>li:visible').size();
            if(visible){
                $(".noresult").hide();
            }
            else{$(".noresult").show();}*/
		    }
      }, options);
      if (!settings.searchInput) {
        searchBar = $(settings.searchTemplate);
        settings.searchInput = searchBar.find("input");
        container.before(searchBar);
      }
      return settings.searchInput.on("keyup.sieve change.sieve", function() {
        var items, query;
        query = compact($(this).val().toLowerCase().split(/\s+/));
        items = container.find(settings.itemSelector);
        items.each(function() {
          var cells, item, match, q, text, _i, _len;
          item = $(this);
          if (settings.textSelector) {
            cells = item.find(settings.textSelector);
            text = cells.text().toLowerCase();
          } else {
            text = item.text().toLowerCase();
          }
          match = true;
          for (_i = 0, _len = query.length; _i < _len; _i++) {
            q = query[_i];
            match && (match = text.indexOf(q) >= 0);           
          }
          return settings.toggle(item, match);
        });
        return settings.complete();
      });
    });
  };
}).call(this);

var HeaderSearch = function (){
  var searchTemplateHeader = '<div class="search-keyword">'+
                                '<div class="sm-show">'+
                                    '<input type="text" name="keywords" placeholder="Enter keyword"/>' + 
                                    '<input type="text" name="location"  placeholder="City, state or zip"/>'+
                                    '<div class="search-button">' +
                                    '<button class="btn btn-info btn-search" type="submit">' +
                                  '</div>'+
                                '</div>'+
                                '<div class="form-group iSearch m-t0">'+
                                    '<input type="text" placeholder="Enter keywords, city, state or zip">'+                  
                                    '<button class="btn btn-info btn-search" type="submit"></button>'+
                                  '</div>'+ 
                              '</div>';
  $('.main-menu').find('.resultSearch').sieve({ 
    itemSelector: "li",
    searchTemplate: searchTemplateHeader
  })
}
var HomeSearch = function (){
  var searchTemplateHome = '<div class="search-keyword-main">'+
                              '<div class="form-group sm-show">'+
                                '<p class="black bold">'+
                                 'What you search'+      
                                '</p>'+
                                '<input type="text" placeholder="enter keyword">'+        
                              '</div>'+
                              '<div class="form-group sm-show">'+
                                '<p class="black bold">'+
                                  'Where'+
                                '</p>'+
                                '<input type="text" placeholder="city, state or zip">'+                  
                              '</div>'+ 
                              '<div class="form-group iSearch">'+
                                '<input type="text" placeholder="Enter keywords, city, state or zip">'+                  
                                '<button class="btn btn-info btn-search" type="submit"></button>'+
                              '</div>'+      
                            '</div>'+    
                            '<div class="search-button">'+
                              '<button class="btn btn-info btn-search" type="submit"></button>'+
                            '</div>';
    $('.banner-intro').find('.resultSearch').sieve({ 
      itemSelector: "li",
    searchTemplate: searchTemplateHome
  })
}

$(document).ready(function() { 

HeaderSearch();
HomeSearch();

  $(".search-keyword input").keypress(function (e) {
    $('.main-menu').find('.resultSearch').css('display','block');
  })
  $(".search-keyword-main input").keypress(function (e) {
    $('.banner-intro').find('.resultSearch').css('display','block');
  })
})