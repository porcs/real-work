$(document).ready(function() {	
	/*---------------------[ Plugins ]--------------------------*/
	$(function() {
		$('.custom-form').checkBo();
	});

	
	$('.responsive-table').dataTable({
		 columnDefs: [{
	        targets: "tableNoSort",
	        orderable: false
	    }]
	});	

	$('.dataTables_wrapper').find('select').addClass('search-select');
	
	$(".search-select").chosen({
		disable_search_threshold: 1,
		no_results_text: "Nothing found!"
	});	

	$(function() {
		$('.allCheckBo').checkBo({
			checkAllButton : '.checkAll', 
	    	checkAllTarget : '.checkboSettings', 
		});
	});

	$(function() {
		$(".gallery-image").fancybox({
			"padding" : 0,
			"imageScale" : false, 
			"zoomOpacity" : false,
			"zoomSpeedIn" : 1000,	
			"zoomSpeedOut" : 1000,	
			"zoomSpeedChange" : 1000, 
			"frameWidth" : 700,	 
			"frameHeight" : 600, 
			"overlayShow" : true, 
			"overlayOpacity" : 0.9,	
			"hideOnContentClick" :false,
			"centerOnScroll" : false,
			helpers : {
				media : {}
			}
		});
	});

	$(function() {	
		$('.inputDate').datepicker();
	});

	$(function () {
	    $('[id*="dp-"]').datepicker();
	});

	/*---------------------[ Actions ]--------------------------*/

	$('.chosen-single').find('div').addClass('button-select');

	$('.button-select').find('b').addClass('fa').addClass('fa-angle-down');	

	$(".dataTables_wrapper .chosen-search").remove();

	$('.forCheckBo').on('click', function(){
		var $this = $(this),
			$checkbox = $this.parent().find('.cb-checkbox');
		$checkbox.toggleClass('checked');
		checkBox($checkbox);
		return false;
	});

	/*$('.headSettings_point-item').each(function(){
		var $this 		= $(this);
			$pointCount = $this.closest('.modal').find('.headSettings_point-item').length;
		$this.width(100/$pointCount + '%');
	});*/	

	$(".progressbar").draggable();

	$('.countDiv').remove();

	$('.showColumns').on('click', function(event){
		event.preventDefault();
		$(this).find('.dropCheckbo').show();
		$(document).mouseup(function (e) {
			if ($('.dropCheckbo').has(e.target).length === 0){
				$('.dropCheckbo').hide();
			}
		});
	});

	$('#marketplace').modal();
	
	$('.btn-MainSearch').on('click', function(){
		var $window = $(window).width();
		if(($window > 751) === true){
			$(this).animate({
			'left':'-30px'
			},400);
			$(this).parent().find('input').slideToggle(400);
		}
	});

	var modalBlockWidth = function($modalBlock){
		$($modalBlock).each(function(){
			var $this 		= $(this);
				$pointCount = $this.closest('.modal').find($modalBlock).length;
			$this.width(100/($pointCount) + '%');
		});	
	}

	modalBlockWidth('.headSettings_point-item');

	$('.marketplaceSelect li').on('click', function(){		
		$('.marketplaceSelect li').removeClass('active');
		$(this).addClass('active');	
		selectWizard($('.marketplaceSelect li'));		
	})

	$('.selectWizard li').on('click', function(){		
		$('.selectWizard li').removeClass('active');
		$(this).addClass('active');	
		selectWizard($('.selectWizard li'));		
	})

	
	$(document).on('click', '.withIcon .cb-plus.good', function(){
		$(this).closest('.row').prev().clone().insertAfter($(this).closest('.row')).show().removeClass('hidden');				
		choSelect();
	});

	$(document).on('click', '.withIcon .cb-plus.bad', function(){
		$(this).closest('.row').remove();
	});

	$(document).on('click', '.withIcon .cb-plus', function(){
		countBlock($('.count'), $('.showSelectBlock'));
	});

	$(document).on('click', '.choZn .cb-plus.good', function(){
		$(this).closest('.row').clone(true).insertBefore($(this).closest('.row'));
		$(this).removeClass('good').addClass('bad');
	});

	$(document).on('click', '.choZn .cb-plus.bad', function(){
		$(this).closest('.row').remove();
	});	


	/*---------------------[ Functions ]--------------------------*/

	faRemove('.progressbar');
	faRemove('.notification');
	faRemove('.validate');	

	showMore($('.list-circle-blue li'), 3);

	hideEmpty();

	hideBlock ($('.btn-more'), $('.loginBlock'));
	showBlock($('.language'), $('.language_block'));
	
	addActive('.form-group');
	addChartsActive('.wizardSettings li', '.wizardSettings');
	addChartsActive('.chart .tabs li', '.tabs');
	disabledInput();	

	uploadFoto('.profile');

	mainMenu();	
	sidebarItem($('.sidebar ul li > ul li a'));	
	
	resizeSidebar($('.sidebar'));
	showSidebar();
	countDown();
	checkboTable();
	tree($('.tree'));

	showBlockSet('.showModel', false);

	countBlock($('.countProfile'), $('.showSelectBlock'));

	rulesAddSettings();

	selectWizard($('.selectWizard li'));
	selectWizard($('.marketplaceSelect li'));
	
}) // close Ready

$(window).resize(function() {
	resizeSidebar($('.sidebar'));
	hideEmpty();
})