<?php require_once 'inc/modules/header.php' ?>
<div class="doc-navbar clearfix" role="navigation">
    <h1 class="montserrat white pull-left"> 
      	<img class="m-r10 pull-left" src="images/logo.png" alt="">
      	Feed.biz Guideline
    </h1>
</div>

<div class="container-fluid guide">
    <div class="row">
        <!-- [ Sidebar - Navigation ] -->
        <div class="col-sm-2 sidebar-guide">
            <?php require_once 'inc/modules/nav-guide.php' ?> 
        </div>
        <div class="col-sm-10 col-sm-offset-2 main">
         	<h2 id="grid" class="sub-header">Grid system</h2>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>
                            Extra small devices
                            <small>Phones (< 768px)</small>
                        </th>
                        <th>
                            Small devices
                            <small>Tablets (≥ 768px)</small>
                        </th>
                        <th>
                            Medium devices
                            <small>Desktops (>992px)</small>
                        </th>
                        <th>
                            Large devices
                            <small>Desktops (≥ 1200px)</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th class="text-nowrap">Grid behavior</th>
                        <td>Horizontal at all times</td>
                        <td colspan="3">Collapsed to start, horizontal above breakpoints</td>
                    </tr>
                    <tr>
                        <th class="text-nowrap">Container width</th>
                        <td>None (auto)</td>
                        <td>750px</td>
                        <td>970px</td>
                        <td>1170px</td>
                    </tr>
                    <tr>
                        <th class="text-nowrap">Class prefix</th>
                        <td><code>.col-xs-*</code></td>
                        <td><code>.col-sm-*</code></td>
                        <td><code>.col-md-*</code></td>
                        <td><code>.col-lg-*</code></td>
                    </tr>
                    <tr>
                        <th class="text-nowrap"># of columns</th>
                        <td colspan="4">12</td>
                    </tr>
                    <tr>
                        <th class="text-nowrap">Gutter width</th>
                        <td colspan="4">30px (15px on each side of a column)</td>
                    </tr>
                    <tr>
                        <th class="text-nowrap">Nestable</th>
                        <td colspan="4">Yes</td>
                    </tr>
                    <tr>
                    </tbody>
                </table>
            </div>
            <div class="container-fluid p-0">
                <div class="row m-b15">
                    <div class="col-sm-12">
                        <div class="bscol">col-sm-12</div>
                    </div>
                </div>
                <div class="row m-b15">
                    <div class="col-sm-6"><div class="bscol">col-sm-6</div></div>
                    <div class="col-sm-6"><div class="bscol">col-sm-6</div></div>
                </div>
                <div class="row m-b15">
                    <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
                    <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
                    <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
                </div>
                <div class="row m-b15">
                    <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
                    <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
                    <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
                    <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
                </div>
                <div class="row m-b15">
                    <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                    <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                    <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                    <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                    <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                    <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                </div>
                <div class="row m-b15">
                    <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
                    <div class="col-sm-8"><div class="bscol">col-sm-8</div></div>
                </div>
            </div>
            <pre class="prettyprint prettyprinted"><span class="tag">&lt;div</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"container"</span><span class="tag">&gt;</span><span class="pln">
  </span><span class="tag">&lt;div</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"row"</span><span class="tag">&gt;</span><span class="pln">
    </span><span class="tag">&lt;div</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"col-*-*"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/div&gt;</span><span class="pln">
  </span><span class="tag">&lt;/div&gt;</span><span class="pln">
</span><span class="tag">&lt;/div&gt;</span></pre> 
<h2 id="type" class="sub-header">Typography</h2> 
            <p>HTML headings, <code>&lt;h1&gt;</code> through <code>&lt;h6&gt;</code></p>
            <div class="demo">
              	<h4>Headings</h4>
              	<table class="table">
	                <thead>
		                <tr>
		                    <th>Tag</th>
		                    <th>Font size</th>
		                </tr>
	                </thead>
	                <tbody>
						<tr>
							<td><h1>h1. heading</h1></td>
							<td class="type-info">20px</td>
						</tr>
						<tr>
							<td><h2>h2. heading</h2></td>
							<td class="type-info">16px</td>
						</tr>
						<tr>
							<td><h3>h3. heading</h3></td>
							<td class="type-info">13px</td>
						</tr>
						<tr>
							<td><h4>h4. heading</h4></td>
							<td class="type-info">12px</td>
						</tr>
						<tr>
							<td><h5>h5. heading</h5></td>
							<td class="type-info">11px</td>
						</tr>
						<tr>
							<td><h6>h6. heading</h6></td>
							<td class="type-info">10px</td>
						</tr>
                  	</tbody>
              </table>
            </div>
<pre class="prettyprint prettyprinted"><span class="tag">&lt;h1&gt;</span><span class="pln">...</span><span class="tag">&lt;/h1&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h1"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h2&gt;</span><span class="pln">...</span><span class="tag">&lt;/h2&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h3&gt;</span><span class="pln">...</span><span class="tag">&lt;/h3&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h4&gt;</span><span class="pln">...</span><span class="tag">&lt;/h4&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h5&gt;</span><span class="pln">...</span><span class="tag">&lt;/h5&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h6&gt;</span><span class="pln">...</span><span class="tag">&lt;/h6&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span></pre>
            
            <h2 id="header" class="sub-header">Head menu</h2> 

            <h4>For Sign In / Sign Up</h4>
            <?php require_once 'inc/modules/nav-forSignIn.php';?>

            <h4>When Login</h4>
            <?php require_once 'inc/modules/nav-header-login.php';?>

            <h4>When not Login</h4>
            <?php require_once 'inc/modules/nav-header-notLogin.php';?>

            <h2 id="forms" class="sub-header">Forms</h2> 
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<input type="text" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group has-error">
						<input type="text" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<input type="text" class="form-control" disabled>
					</div>
				</div>
			</div>  
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group has-error">
						<input type="text" class="form-control">
						<div class="error text-left">
							<p>
								<i class="fa fa-exclamation-circle"></i>
								Please enter company name
							</p>
						</div>
					</div>
				</div>
			</div>

            <h2 id="dropdown" class="sub-header">Dropdown</h2>
			<h4 class="sub-header">Main</h4>
			<div class="row">
				<div class="col-sm-6">
					<select class="search-select" data-placeholder="Choose">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack & Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce & Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
			<h4 class="sub-header">Per Page</h4>
			<div class="row">
				<div class="col-sm-6">
					<div class="per-page">
						<select class="search-select">
							<option value="">5</option>
							<option value="">10</option>
							<option value="">15</option>
							<option value="">20</option>
							<option value="">25</option>
							<option value="">30</option>
							<option value="">50</option>
							<option value="">100</option>
						</select>
					</div>
				</div>
			</div>

            <h2 id="checkbo" class="sub-header">Checkbox, Radio & Switcher</h2>
			<div class="custom-form">
				<div class="row">
					<div class="col-sm-6">
						<h4 class="sub-header">Checkbox</h4>
						<div class="row ">
							<div class="col-xs-12">
								<label class="cb-checkbox">
									<input type="checkbox"/>
									Bank
								</label>
							</div>
							<div class="col-xs-12">
								<div class="p-b10">
									<label class="cb-checkbox">
										<input type="checkbox"/>
										Credit card
									</label>
								</div>
							</div>
						</div>
						  <pre class="prettyprint prettyprinted"><span class="tag">&lt;label</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"cb-checkbox"</span><span class="tag">&gt;</span><span class="pln">
  </span><span class="tag">&lt;input</span><span class="pln"> </span><span class="atn">type</span><span class="pun">=</span><span class="atv">"checkbox"</span><span class="tag">&gt;</span><span class="pln">
  Bank
</span><span class="tag">&lt;/label&gt;</span></pre> 
					</div>
					<div class="col-sm-6">
						<h4 class="sub-header">Radio</h4>
						<div class="row ">
							<div class="col-xs-12">
								<label class="cb-radio">
									<input type="radio"/>
									Bank
								</label>
							</div>
							<div class="col-xs-12">
								<div class="p-b10">
									<label class="cb-radio">
										<input type="radio"/>
										Credit card
									</label>
								</div>
							</div>
						</div>
												  <pre class="prettyprint prettyprinted"><span class="tag">&lt;label</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"cb-radio"</span><span class="tag">&gt;</span><span class="pln">
  </span><span class="tag">&lt;input</span><span class="pln"> </span><span class="atn">type</span><span class="pun">=</span><span class="atv">"radio"</span><span class="tag">&gt;</span><span class="pln">
  Bank
</span><span class="tag">&lt;/label&gt;</span></pre> 
					</div>		
				</div>	
				<div class="row">
					<div class="col-sm-6">
						<h4 class="sub-header">Switcher</h4>
						<div class="cb-switcher">
							<label class="inner-switcher">
								<input type="checkbox" data-state-on="ON" data-state-off="OFF"/>
							</label>
							<span class="cb-state">ON</span>
						</div>
					</div>
				</div>			
			</div>

			<h2 id="buttons" class="sub-header">Buttons</h2>
			<div class="row">
				<div class="col-sm-12">
					<p>
						<button class="btn btn-save">Save</button>
						<button class="btn btn-verify">Verify</button>
						<button class="btn btn-rule">+ Add Rule Items(s)</button>
					</p>
					<p>
						<button class="btn btn-facebook"><i class="fa fa-facebook"></i>Sharer on your Facebook Account</button>
						<button class="btn btn-google"><i class="fa fa-google-plus"></i>Sharer on your Google Account</button>
						<button class="btn btn-twitter"><i class="fa fa-twitter"></i>Sharer on your Twitter Account</button>
					</p>
					<p>
						<button class="btn btn-wizard"><i class="icon-wizard"></i>Amazon Synchronize Wizard</button>
						<button class="btn btn-amazon"><i class="icon-amazon"></i>Update Product offers to Amazon</button>
					</p>
					<p>
						<button class="btn btn-success"><i class="fa fa-check"></i>Verified</button>
						<button class="btn btn-catalog">Import my catalog</button>
					</p>
					<p>
						<button class="btn btn-error">Please check URL and verify again</button>
					</p>
				</div>
			</div>

			<h2 id="donut" class="sub-header">Donut chart</h2>
			<h4>Documentation <a class="link" href="http://api.highcharts.com/highcharts">Highcharts donut 3D</a></h4>
			<div class="row">
				<div class="col-xs-12">
					<div class="mainChart">
						<div class="row">
							<div class="col-sm-6">
								<div class="chart">
									<h4 class="text-uc dark-gray bold m-t20">Total products</h4>
									<ul class="tabs">
										<li class="active">30 days</li>
										<li>7 Days</li>
										<li>1 Day</li>
									</ul>
									<div id="product" data-highcharts-chart="3"><div class="highcharts-container" id="highcharts-12" style="position: relative; overflow: hidden; width: 470px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="470" height="400"><desc>Created with Highstock 2.1.1</desc><defs><clipPath id="highcharts-13"><rect x="0" y="0" width="470" height="400"></rect></clipPath></defs><rect x="0" y="0" width="470" height="400" strokeWidth="0" fill="#FFFFFF" class=" highcharts-background"></rect><g class="highcharts-series-group" zIndex="3"><path fill="rgba(67,67,72,0.25)" d="M 0 0"></path><g class="highcharts-series highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(10,10) scale(1 1)" style=""><path fill="rgba(219,66,66,1)" d="M 226.44893051211463 96.99033921202829 L 226.44893051211463 120.85519307707426 L 226.48005098129477 176.0095176197445 L 226.48005098129477 152.14466375469854 Z" zIndex="-126.71998991401652" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(219,66,66,1)" d="M 204.29117850894718 98.36311580501999 C 212.00269708908823 97.40243751163999 218.6186486130754 96.9925483048649 226.44893051211463 96.99033921202829 L 226.44893051211463 120.85519307707426 C 218.6186486130754 120.85740216991087 212.00269708908823 121.26729137668596 204.29117850894718 122.22796967006596 Z" zIndex="-126.0585905362207" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(219,66,66,1)" d="M 217.8246791050575 152.68090461133593 C 220.8369910504251 152.30563965298438 223.4213471144826 152.14552668158785 226.48005098129477 152.14466375469854 L 226.48005098129477 176.0095176197445 C 223.4213471144826 176.01038054663383 220.8369910504251 176.17049351803036 217.8246791050575 176.5457584763819 Z" zIndex="-126.0585905362207" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(99,156,211,1)" d="M 226.47392982992702 96.99033388541798 L 226.47392982992702 120.85518775046395 L 226.48981633981523 176.00951553903738 L 226.48981633981523 152.1446616739914 Z" zIndex="-126.71999737165142" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(219,66,66,1)" d="M 204.29117850894718 98.36311580501999 L 204.29117850894718 122.22796967006596 L 217.8246791050575 176.5457584763819 L 217.8246791050575 152.68090461133593 Z" zIndex="-124.7980046308585" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(18,119,118,1)" d="M 204.28991792415235 98.36327284955962 L 204.28991792415235 122.2281267146056 L 217.824186689122 176.54581982190518 L 217.824186689122 152.6809659568592 Z" zIndex="-124.79778475728584" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(18,119,118,1)" d="M 182.68255765402301 102.4587401720289 C 190.0820357439322 100.55245253441164 196.53409138900784 99.32952744190554 204.28991792415235 98.36327284955962 L 204.28991792415235 122.2281267146056 C 196.53409138900784 123.19438130695151 190.0820357439322 124.41730639945762 182.68255765402301 126.32359403707487 Z" zIndex="-120.26650301001098" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(18,119,118,1)" d="M 209.38381158360272 154.2807578796988 C 212.2742327124735 153.53611427125455 214.79456694883118 153.05840915699434 217.824186689122 152.6809659568592 L 217.824186689122 176.54581982190518 C 214.79456694883118 176.92326302204032 212.2742327124735 177.40096813630052 209.38381158360272 178.14561174474477 Z" zIndex="-120.26650301001098" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(18,119,118,1)" d="M 182.68255765402301 102.4587401720289 L 182.68255765402301 126.32359403707487 L 209.38381158360272 178.14561174474477 L 209.38381158360272 154.2807578796988 Z" zIndex="-119.06383797991087" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(203,186,59,1)" d="M 182.68135499118384 102.45905001238712 L 182.68135499118384 126.3239038774331 L 209.38334179343119 178.1457327761347 L 209.38334179343119 154.28087891108873 Z" zIndex="-119.06340418127847" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(203,186,59,1)" d="M 115.63649887870827 142.26017175580046 L 115.63649887870827 166.12502562084643 L 183.1939448744954 193.69304595715553 L 183.1939448744954 169.82819209210956 Z" zIndex="-63.338990875752614" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(203,186,59,1)" d="M 115.63649887870827 142.26017175580046 C 131.3375352666472 123.02190392016946 153.16685190746355 110.06294868370682 182.68135499118384 102.45905001238712 L 182.68135499118384 126.3239038774331 C 153.16685190746355 133.9278025487528 131.3375352666472 146.88675778521545 115.63649887870827 166.12502562084643 Z" zIndex="-63.97877866237638" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(203,186,59,1)" d="M 183.1939448744954 169.82819209210956 C 189.32716221353405 162.3132437188162 197.85423902635296 157.251151829573 209.38334179343119 154.28087891108873 L 209.38334179343119 178.1457327761347 C 197.85423902635296 181.11600569461896 189.32716221353405 186.17809758386218 183.1939448744954 193.69304595715553 Z" zIndex="-63.97877866237638" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(216,67,103,1)" d="M 115.63585909646483 142.26095568139675 L 115.63585909646483 166.12580954644272 L 183.19369495955658 193.69335217809157 L 183.19369495955658 169.8284983130456 Z" zIndex="-63.337893323924575" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(42,42,47,1)" d="M 352.54499697983147 171.7415302717194 L 352.54499697983147 195.60638413676537 L 275.7363269452467 205.20920162743636 L 275.7363269452467 181.34434776239038 Z" zIndex="-22.06298319586048" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(99,156,211,1)" d="M 352.5447741151131 171.74063899978634 L 352.5447741151131 195.6054928648323 L 275.73623988871606 205.2088534743375 L 275.73623988871606 181.34399960929153 Z" zIndex="-22.06423104022741" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(99,156,211,1)" d="M 226.47392982992702 96.99033388541798 C 289.29935584028925 96.9812858488308 341.6057373139377 127.99494326306294 352.5447741151131 171.74063899978634 L 352.5447741151131 195.6054928648323 C 341.6057373139377 151.8597971281089 289.29935584028925 120.84613971387677 226.47392982992702 120.85518775046395 Z" zIndex="-22.287102060835768" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(99,156,211,1)" d="M 226.48981633981523 152.1446616739914 C 251.03099837511297 152.14112728469954 271.4631786382569 164.25583721213394 275.73623988871606 181.34399960929153 L 275.73623988871606 205.2088534743375 C 271.4631786382569 188.1206910771799 251.03099837511297 176.0059811497455 226.48981633981523 176.00951553903738 Z" zIndex="-22.287102060835768" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(103,108,208,1)" d="M 100.44615777149329 203.22305408358596 L 100.44615777149329 227.08790794863194 L 177.26021787948957 217.50667186644674 L 177.26021787948957 193.64181800140076 Z" zIndex="22.013398763663613" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(216,67,103,1)" d="M 177.2601310235284 193.64146982325525 C 175.12865871444149 185.0968200694617 177.06058389824372 177.3434900543589 183.19369495955658 169.8284983130456 L 183.19369495955658 193.69335217809157 C 177.06058389824372 201.20834391940488 175.12865871444149 208.96167393450767 177.2601310235284 217.50632368830122 Z" zIndex="22.234495787398885" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(216,67,103,1)" d="M 100.44593542023271 203.22216274753347 C 99.08104927482745 197.75060488746945 98.5 193.05602398401842 98.5 187.5 L 98.5 211.36485386504597 C 98.5 216.9208778490644 99.08104927482745 221.61545875251542 100.44593542023271 227.08701661257945 Z" zIndex="22.234495787398885" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(216,67,103,1)" d="M 100.44593542023271 203.22216274753347 L 100.44593542023271 227.08701661257945 L 177.2601310235284 217.50632368830122 L 177.2601310235284 193.64146982325525 Z" zIndex="22.012150829524895" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(42,42,47,1)" d="M 275.7363269452467 181.34434776239038 C 277.3401455510125 187.75846394262965 276.6358322229423 193.468765960414 273.48592802235083 199.58971810834456 L 273.48592802235083 223.45457197339053 C 276.6358322229423 217.33361982545998 277.3401455510125 211.62331780767562 275.7363269452467 205.20920162743636 Z" zIndex="43.769454884066526" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(42,42,47,1)" d="M 354.5 187.5 C 354.5 198.60583275020426 352.1546312295537 208.01332119277484 346.78397573721804 218.4496783573621 L 346.78397573721804 242.31453222240808 C 352.1546312295537 231.8781750578208 354.5 222.47068661525023 354.5 211.36485386504597 Z" zIndex="43.769454884066526" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(42,42,47,1)" d="M 346.78397573721804 218.4496783573621 L 346.78397573721804 242.31453222240808 L 273.48592802235083 223.45457197339053 L 273.48592802235083 199.58971810834456 Z" zIndex="43.33176033522586" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(119,212,100,1)" d="M 346.78353803665505 218.45052889196373 L 346.78353803665505 242.3153827570097 L 273.4857570455684 223.4549042134693 L 273.4857570455684 199.59005034842332 Z" zIndex="43.332951144419056" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(119,212,100,1)" d="M 337.32620484008396 232.78549613697385 L 337.32620484008396 256.6503500020198 L 269.7914862656578 229.05450079355137 L 269.7914862656578 205.1896469285054 Z" zIndex="63.402929187545766" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(222,138,67,1)" d="M 337.3255644009145 232.78627979431928 L 337.3255644009145 256.65113365936526 L 269.79123609410726 229.05480690970194 L 269.79123609410726 205.18995304465597 Z" zIndex="63.40402636380352" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(119,212,100,1)" d="M 273.4857570455684 199.59005034842332 C 272.4337963521353 201.6341762262606 271.3306703644175 203.30624112789556 269.7914862656578 205.1896469285054 L 269.7914862656578 229.05450079355137 C 271.3306703644175 227.17109499294153 272.4337963521353 225.49903009130657 273.4857570455684 223.4549042134693 Z" zIndex="64.04336281570279" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(119,212,100,1)" d="M 346.78353803665505 218.45052889196373 C 344.0905186614663 223.68349113922716 341.2665161329088 227.96397728741263 337.32620484008396 232.78549613697385 L 337.32620484008396 256.6503500020198 C 341.2665161329088 251.8288311524586 344.0905186614663 247.54834500427313 346.78353803665505 242.3153827570097 Z" zIndex="64.04336281570279" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(222,138,67,1)" d="M 226.47520982990176 278.00966629440046 L 226.47520982990176 301.87452015944643 L 226.49031633980536 246.72019226129615 L 226.49031633980536 222.85533839625018 Z" zIndex="126.7199976234101" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(103,108,208,1)" d="M 226.47392982992702 278.00966611458205 L 226.47392982992702 301.874519979628 L 226.48981633981523 246.72019219105457 L 226.48981633981523 222.8553383260086 Z" zIndex="126.71999737165142" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(222,138,67,1)" d="M 269.79123609410726 205.18995304465597 C 260.5819198677136 216.45857207421693 244.89615895342308 222.85785903292842 226.49031633980536 222.85533839625018 L 226.49031633980536 246.72019226129615 C 244.89615895342308 246.7227128979744 260.5819198677136 240.3234259392629 269.79123609410726 229.05480690970194 Z" zIndex="127.99999759940414" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(222,138,67,1)" d="M 337.3255644009145 232.78627979431928 C 313.7497148613467 261.6339445099954 273.59416692076314 278.0161191242967 226.47520982990176 278.00966629440046 L 226.47520982990176 301.87452015944643 C 273.59416692076314 301.8809729893427 313.7497148613467 285.49879837504136 337.3255644009145 256.65113365936526 Z" zIndex="127.99999759940414" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(103,108,208,1)" d="M 226.48981633981523 222.8553383260086 C 201.9486343045175 222.85180393671672 181.52343471566292 210.7312097822864 177.26021787948957 193.64181800140076 L 177.26021787948957 217.50667186644674 C 181.52343471566292 234.5960636473324 201.9486343045175 246.7166578017627 226.48981633981523 246.72019219105457 Z" zIndex="127.99999734510244" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(103,108,208,1)" d="M 226.47392982992702 278.00966611458205 C 163.64850381956478 278.0006180779949 111.35999287209707 246.97189704265324 100.44615777149329 203.22305408358596 L 100.44615777149329 227.08790794863194 C 111.35999287209707 270.83675090769924 163.64850381956478 301.86547194304086 226.47392982992702 301.874519979628 Z" zIndex="127.99999734510244" transform="translate(0,0)" visibility="visible"></path><g zIndex="12800" stroke="#7cb5ec" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#7cb5ec" d="M 226.47392982992702 96.99033388541798 C 289.29935584028925 96.9812858488308 341.6057373139377 127.99494326306294 352.5447741151131 171.74063899978634 L 275.73623988871606 181.34399960929153 C 271.4631786382569 164.25583721213394 251.03099837511297 152.14112728469954 226.48981633981523 152.1446616739914 Z" zIndex="128" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="12800" stroke="#434348" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#434348" d="M 352.54499697983147 171.7415302717194 C 356.65077261059196 188.16166769313193 354.8477304907323 202.7800408586599 346.78397573721804 218.4496783573621 L 273.48592802235083 199.58971810834456 C 276.6358322229423 193.468765960414 277.3401455510125 187.75846394262965 275.7363269452467 181.34434776239038 Z" zIndex="128" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="12800" stroke="#90ed7d" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#90ed7d" d="M 346.78353803665505 218.45052889196373 C 344.0905186614663 223.68349113922716 341.2665161329088 227.96397728741263 337.32620484008396 232.78549613697385 L 269.7914862656578 205.1896469285054 C 271.3306703644175 203.30624112789556 272.4337963521353 201.6341762262606 273.4857570455684 199.59005034842332 Z" zIndex="128" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="12800" stroke="#f7a35c" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#f7a35c" d="M 337.3255644009145 232.78627979431928 C 313.7497148613467 261.6339445099954 273.59416692076314 278.0161191242967 226.47520982990176 278.00966629440046 L 226.49031633980536 222.85533839625018 C 244.89615895342308 222.85785903292842 260.5819198677136 216.45857207421693 269.79123609410726 205.18995304465597 Z" zIndex="128" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="12800" stroke="#8085e9" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#8085e9" d="M 226.47392982992702 278.00966611458205 C 163.64850381956478 278.0006180779949 111.35999287209707 246.97189704265324 100.44615777149329 203.22305408358596 L 177.26021787948957 193.64181800140076 C 181.52343471566292 210.7312097822864 201.9486343045175 222.85180393671672 226.48981633981523 222.8553383260086 Z" zIndex="128" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="12800" stroke="#f15c80" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#f15c80" d="M 100.44593542023271 203.22216274753347 C 94.9893663089702 181.34785937782198 99.93509477950387 161.4993345391588 115.63585909646483 142.26095568139675 L 183.19369495955658 169.8284983130456 C 177.06058389824372 177.3434900543589 175.12865871444149 185.0968200694617 177.2601310235284 193.64146982325525 Z" zIndex="128" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="12800" stroke="#e4d354" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#e4d354" d="M 115.63649887870827 142.26017175580046 C 131.3375352666472 123.02190392016946 153.16685190746355 110.06294868370682 182.68135499118384 102.45905001238712 L 209.38334179343119 154.28087891108873 C 197.85423902635296 157.251151829573 189.32716221353405 162.3132437188162 183.1939448744954 169.82819209210956 Z" zIndex="128" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="12800" stroke="#2b908f" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#2b908f" d="M 182.68255765402301 102.4587401720289 C 190.0820357439322 100.55245253441164 196.53409138900784 99.32952744190554 204.28991792415235 98.36327284955962 L 217.824186689122 152.6809659568592 C 214.79456694883118 153.05840915699434 212.2742327124735 153.53611427125455 209.38381158360272 154.2807578796988 Z" zIndex="128" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="12800" stroke="#f45b5b" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#f45b5b" d="M 204.29117850894718 98.36311580501999 C 212.00269708908823 97.40243751163999 218.6186486130754 96.9925483048649 226.44893051211463 96.99033921202829 L 226.48005098129477 152.14466375469854 C 223.4213471144826 152.14552668158785 220.8369910504251 152.30563965298438 217.8246791050575 152.68090461133593 Z" zIndex="128" transform="translate(0,0)" visibility="visible"></path></g></g><g class="highcharts-markers" visibility="visible" zIndex="0.1" transform="translate(10,10) scale(1 1)"></g></g><g class="highcharts-data-labels highcharts-tracker" visibility="visible" zIndex="6" transform="translate(10,10) scale(1 1)" opacity="1" style=""><path fill="none" d="M 359.7314001974921 95.19231882778868 C 354.7314001974921 95.19231882778868 322.91814145298093 101.3206743727405 315.847477746429 109.74716324704926 L 308.776814039877 118.17365212135802" stroke="#7cb5ec" stroke-width="1"></path><path fill="none" d="M 385.71465214266175 221.87791536026918 C 380.71465214266175 221.87791536026918 375.9292047137618 221.18066941828795 364.9710630347526 220.2219562480637 L 354.0129213557434 219.26324307783943" stroke="#434348" stroke-width="1"></path><path fill="none" d="M 370.67846076386684 262.28874286549274 C 365.67846076386684 262.28874286549274 362.44616805549754 258.9077967715672 352.4767823980944 254.25899589241948 L 342.5073967406912 249.61019501327178" stroke="#90ed7d" stroke-width="1"></path><path fill="none" d="M 323.8184885631987 315.7236197796902 C 318.8184885631987 315.7236197796902 301.50000000000006 308.7954165494147 296.00000000000006 299.26913710778587 L 290.50000000000006 289.742857666157" stroke="#f7a35c" stroke-width="1"></path><path fill="none" d="M 114.4221704515716 303.6823498608877 C 119.4221704515716 303.6823498608877 130.08185854701918 297.5539943159358 137.1525222535711 289.1275054416271 L 144.22318596012303 280.70101656731833" stroke="#8085e9" stroke-width="1"></path><path fill="none" d="M 64.89253990836849 166.56907924509662 C 69.89253990836849 166.56907924509662 78.77883704816877 167.9582646664321 89.61172233130306 169.8683946207683 L 100.44460761443734 171.7785245751045" stroke="#f15c80" stroke-width="1"></path><path fill="none" d="M 87.05670340251248 104.5 C 92.05670340251248 104.5 130.08185854701895 101.30639308117827 137.1525222535709 109.73288195548702 L 144.22318596012286 118.15937082979576" stroke="#e4d354" stroke-width="1"></path><path fill="none" d="M 106.17437405329204 79.5 C 111.17437405329204 79.5 187.6771432346219 78.8231364982553 190.52415273074962 89.44832058743505 L 193.37116222687735 100.0735046766148" stroke="#2b908f" stroke-width="1"></path><path fill="none" d="M 136.2063894538401 54.5 C 141.2063894538401 54.5 213.4266385878513 75.41806465553934 214.38535175807553 86.37620633454854 L 215.34406492829976 97.33434801355774" stroke="#f45b5b" stroke-width="1"></path><g zIndex="1" style="" transform="translate(365,85)"><text x="5" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Bananas</tspan></text></g><g zIndex="1" style="" transform="translate(391,212)"><text x="5" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Kiwi</tspan></text></g><g zIndex="1" style="" transform="translate(376,252)"><text x="5" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Mixed nuts</tspan></text></g><g zIndex="1" style="" transform="translate(329,306)"><text x="5" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Oranges</tspan></text></g><g zIndex="1" style="" transform="translate(64,294)"><text x="0" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Apples</tspan></text></g><g zIndex="1" style="" transform="translate(21,157)"><text x="0" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Pears</tspan></text></g><g zIndex="1" style="" transform="translate(5,95)"><text x="0" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Clementines</tspan></text></g><g zIndex="1" style="" transform="translate(16,70)"><text x="0" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Reddish (bag)</tspan></text></g><g zIndex="1" style="" transform="translate(37,45)"><text x="0" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Grapes (bunch)</tspan></text></g></g><g class="highcharts-legend" zIndex="7"><g zIndex="1"><g></g></g></g><g class="highcharts-tooltip" zIndex="8" style="cursor:default;padding:0;white-space:nowrap;" transform="translate(281,-9999)" opacity="0" visibility="visible"><path fill="none" d="M 3.5 0.5 L 72.5 0.5 C 75.5 0.5 75.5 0.5 75.5 3.5 L 75.5 44.5 C 75.5 47.5 75.5 47.5 72.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)" width="75" height="47"></path><path fill="none" d="M 3.5 0.5 L 72.5 0.5 C 75.5 0.5 75.5 0.5 75.5 3.5 L 75.5 44.5 C 75.5 47.5 75.5 47.5 72.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)" width="75" height="47"></path><path fill="none" d="M 3.5 0.5 L 72.5 0.5 C 75.5 0.5 75.5 0.5 75.5 3.5 L 75.5 44.5 C 75.5 47.5 75.5 47.5 72.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)" width="75" height="47"></path><path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 72.5 0.5 C 75.5 0.5 75.5 0.5 75.5 3.5 L 75.5 44.5 C 75.5 47.5 75.5 47.5 72.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#434348" stroke-width="1"></path><text x="8" zIndex="1" style="font-size:12px;color:#333333;fill:#333333;" y="20"><tspan style="font-size: 10px">Kiwi</tspan><tspan x="8" dy="15">Value: 3.0 </tspan></text></g></svg></div></div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="chart">
									<h4 class="text-uc dark-gray bold m-t20">Total orders</h4>
									<ul class="tabs">
										<li class="active">30 days</li>
										<li>7 Days</li>
										<li>1 Day</li>
									</ul>
									<div id="order" data-highcharts-chart="4"><div class="highcharts-container" id="highcharts-14" style="position: relative; overflow: hidden; width: 470px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="470" height="400"><desc>Created with Highstock 2.1.1</desc><defs><clipPath id="highcharts-15"><rect x="0" y="0" width="470" height="400"></rect></clipPath></defs><rect x="0" y="0" width="470" height="400" strokeWidth="0" fill="#FFFFFF" class=" highcharts-background"></rect><g class="highcharts-series-group" zIndex="3"><path fill="rgba(228,211,84,0.25)" d="M 0 0"></path><g class="highcharts-series highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(10,10) scale(1 1)" style=""><path fill="rgba(18,119,118,1)" d="M 225.94673612005707 91.10125222504513 L 225.94673612005707 114.9661060900911 L 225.98005098129477 174.0095176197445 L 225.98005098129477 150.14466375469854 Z" zIndex="-132.16498948063443" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(99,156,211,1)" d="M 225.9728096273067 91.10124666955703 L 225.9728096273067 114.966100534603 L 225.98981633981523 174.00951553903738 L 225.98981633981523 150.1446616739914 Z" zIndex="-132.16499725871458" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(18,119,118,1)" d="M 173.84191019565077 98.60413223874957 C 191.16761213555347 93.40437107065976 207.12507509536272 91.10656222500157 225.94673612005707 91.10125222504513 L 225.94673612005707 114.9661060900911 C 207.12507509536272 114.97141609004754 191.16761213555347 117.26922493570574 173.84191019565077 122.46898610379554 Z" zIndex="-122.88931470213934" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(18,119,118,1)" d="M 206.4651349047381 152.95473117556165 C 212.9541618485219 151.00725508264412 218.93073973609089 150.14665251872717 225.98005098129477 150.14466375469854 L 225.98005098129477 174.0095176197445 C 218.93073973609089 174.01150638377314 212.9541618485219 174.8721089476901 206.4651349047381 176.81958504060762 Z" zIndex="-122.88931470213934" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(18,119,118,1)" d="M 173.84191019565077 98.60413223874957 L 173.84191019565077 122.46898610379554 L 206.4651349047381 176.81958504060762 L 206.4651349047381 152.95473117556165 Z" zIndex="-121.66042155511795" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(203,186,59,1)" d="M 173.8406813051117 98.6045010564843 L 173.8406813051117 122.46935492153027 L 206.46467464610924 176.81972317459065 L 206.46467464610924 152.95486930954468 Z" zIndex="-121.6599051839459" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(99,156,211,1)" d="M 225.9728096273067 91.10124666955703 C 276.1959829359133 91.0940135931025 318.9697930683458 109.01067127157506 343.092568924796 140.15917787141538 L 343.092568924796 164.02403173646135 C 318.9697930683458 132.87552513662104 276.1959829359133 114.95886745814848 225.9728096273067 114.966100534603 Z" zIndex="-64.12160558339052" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(99,156,211,1)" d="M 225.98981633981523 150.1446616739914 C 244.7999936089563 150.1419526565927 260.82014721660886 156.85231133766857 269.854894728388 168.51841867843274 L 269.854894728388 192.3832725434787 C 260.82014721660886 180.71716520271454 244.7999936089563 174.00680652163868 225.98981633981523 174.00951553903738 Z" zIndex="-64.12160558339052" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(42,42,47,1)" d="M 343.0932101349972 140.16000584317752 L 343.0932101349972 164.0248597082235 L 269.8551348820214 192.38358264526227 L 269.8551348820214 168.5187287802163 Z" zIndex="-63.479230307950274" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(99,156,211,1)" d="M 343.092568924796 140.15917787141538 L 343.092568924796 164.02403173646135 L 269.854894728388 192.3832725434787 L 269.854894728388 168.51841867843274 Z" zIndex="-63.48038952755661" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(203,186,59,1)" d="M 96.15093587055128 163.57432390202857 C 106.34431393525028 133.39052030791098 133.44244369118204 110.72909079954152 173.8406813051117 98.6045010564843 L 173.8406813051117 122.46935492153027 C 133.44244369118204 134.5939446645875 106.34431393525028 157.25537417295695 96.15093587055128 187.43917776707454 Z" zIndex="-31.0075885019508" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(203,186,59,1)" d="M 177.36739171181696 177.2881362928946 C 181.18513630533718 165.9833409392925 191.3342485734764 157.49591415713166 206.46467464610924 152.95486930954468 L 206.46467464610924 176.81972317459065 C 191.3342485734764 181.36076802217764 181.18513630533718 189.84819480433848 177.36739171181696 201.15299015794056 Z" zIndex="-31.0075885019508" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(203,186,59,1)" d="M 96.15093587055128 163.57432390202857 L 96.15093587055128 187.43917776707454 L 177.36739171181696 201.15299015794056 L 177.36739171181696 177.2881362928946 Z" zIndex="-30.69751261693129" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(216,67,103,1)" d="M 96.1506258011587 163.5752420746626 L 96.1506258011587 187.44009593970858 L 177.3672755809583 201.15333404282222 L 177.3672755809583 177.28848017777625 Z" zIndex="-30.696227109661542" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(103,108,208,1)" d="M 98.48637771837477 213.45108345502024 L 98.48637771837477 237.3159373200662 L 178.24208903309915 219.83342444745054 L 178.24208903309915 195.96857058240457 Z" zIndex="39.13351329206071" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(216,67,103,1)" d="M 178.2419409875421 195.96823288245415 C 175.45724869996215 189.61608634085937 175.1828996528508 183.75696316503843 177.3672755809583 177.28848017777625 L 177.3672755809583 201.15333404282222 C 175.1828996528508 207.6218170300844 175.45724869996215 213.48094020590534 178.2419409875421 219.83308674750012 Z" zIndex="39.527526166912594" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(216,67,103,1)" d="M 98.48598243673742 213.4501817961526 C 94.30843710337976 203.92080569237316 92.5 195.47672047485605 92.5 185.5 L 92.5 209.36485386504597 C 92.5 219.34157433990202 94.30843710337976 227.78565955741914 98.48598243673742 237.31503566119858 Z" zIndex="39.527526166912594" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(216,67,103,1)" d="M 98.48598243673742 213.4501817961526 L 98.48598243673742 237.31503566119858 L 178.2419409875421 219.83308674750012 L 178.2419409875421 195.96823288245415 Z" zIndex="39.13225090524347" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(42,42,47,1)" d="M 327.9350838477845 246.45690560119152 L 327.9350838477845 270.3217594662375 L 264.17793402538746 232.19515558833868 L 264.17793402538746 208.3303017232927 Z" zIndex="85.34402179528651" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(119,212,100,1)" d="M 327.93422178186154 246.45762638803396 L 327.93422178186154 270.32248025307996 L 264.17761115425526 232.19542554595756 L 264.17761115425526 208.3305716809116 Z" zIndex="85.34503094834936" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(42,42,47,1)" d="M 269.8551348820214 168.5187287802163 C 280.0127180696623 181.6350077384832 277.83419603026704 196.91197102896442 264.17793402538746 208.3303017232927 L 264.17793402538746 232.19515558833868 C 277.83419603026704 220.7768248940104 280.0127180696623 205.49986160352918 269.8551348820214 192.38358264526227 Z" zIndex="86.20608262150152" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(42,42,47,1)" d="M 359.5 185.5 C 359.5 208.7991655734264 349.2121630454756 228.6666254790145 327.9350838477845 246.45690560119152 L 327.9350838477845 270.3217594662375 C 349.2121630454756 252.53147934406047 359.5 232.66401943847237 359.5 209.36485386504597 Z" zIndex="86.20608262150152" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(119,212,100,1)" d="M 286.1990732704486 269.7565177816505 L 286.1990732704486 293.62137164669645 L 248.5464693896811 240.92160209787386 L 248.5464693896811 217.05674823282789 Z" zIndex="117.96514306348854" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(222,138,67,1)" d="M 286.197881700337 269.75694344916695 L 286.197881700337 293.6217973142129 L 248.54602310874046 240.92176152391 L 248.54602310874046 217.05690765886402 Z" zIndex="117.96573902841565" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(119,212,100,1)" d="M 264.17761115425526 208.3305716809116 C 259.62554567344483 212.13658644707687 254.83840050402043 214.80903830998074 248.5464693896811 217.05674823282789 L 248.5464693896811 240.92160209787386 C 254.83840050402043 238.6738921750267 259.62554567344483 236.00144031212284 264.17761115425526 232.19542554595756 Z" zIndex="119.15671016513994" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(119,212,100,1)" d="M 327.93422178186154 246.45762638803396 C 315.78020694809766 256.6196858136953 302.9985293457346 263.75513228764856 286.1990732704486 269.7565177816505 L 286.1990732704486 293.62137164669645 C 302.9985293457346 287.61998615269454 315.78020694809766 280.48453967874127 327.93422178186154 270.32248025307996 Z" zIndex="119.15671016513994" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(222,138,67,1)" d="M 182.21099077449188 274.6761534017037 L 182.21099077449188 298.54100726674966 L 209.5996220129183 242.76416225519716 L 209.5996220129183 218.8993083901512 Z" zIndex="124.8529843251434" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(103,108,208,1)" d="M 182.20972963542553 274.6758437621912 L 182.20972963542553 298.5406976272372 L 209.59914967618934 242.76404628534232 L 209.59914967618934 218.89919242029634 Z" zIndex="124.8525508077094" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(103,108,208,1)" d="M 209.59914967618934 218.89919242029634 C 194.05505808006097 215.08268816954114 183.11419545907805 207.08191328194437 178.24208903309915 195.96857058240457 L 178.24208903309915 219.83342444745054 C 183.11419545907805 230.94676714699034 194.05505808006097 238.94754203458712 209.59914967618934 242.76404628534232 Z" zIndex="126.11368768455496" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(103,108,208,1)" d="M 182.20972963542553 274.6758437621912 C 140.70700507376282 264.4857774126748 111.49490187573844 243.12370846279148 98.48637771837477 213.45108345502024 L 98.48637771837477 237.3159373200662 C 111.49490187573844 266.9885623278375 140.70700507376282 288.35063127772077 182.20972963542553 298.5406976272372 Z" zIndex="126.11368768455496" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(222,138,67,1)" d="M 248.54602310874046 217.05690765886402 C 235.96194039513534 221.55229457589527 222.91840672726306 222.16933274353318 209.5996220129183 218.8993083901512 L 209.5996220129183 242.76416225519716 C 222.91840672726306 246.03418660857915 235.96194039513534 245.41714844094125 248.54602310874046 240.92176152391 Z" zIndex="133.20224439831196" transform="translate(0,0)" visibility="visible"></path><path fill="rgba(222,138,67,1)" d="M 286.197881700337 269.75694344916695 C 252.59838085501138 281.7596265176404 217.7721459617924 283.4071184252336 182.21099077449188 274.6761534017037 L 182.21099077449188 298.54100726674966 C 217.7721459617924 307.27197229027956 252.59838085501138 305.6244803826864 286.197881700337 293.6217973142129 Z" zIndex="133.20224439831196" transform="translate(0,0)" visibility="visible"></path><g zIndex="13350" stroke="#7cb5ec" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#7cb5ec" d="M 225.9728096273067 91.10124666955703 C 276.1959829359133 91.0940135931025 318.9697930683458 109.01067127157506 343.092568924796 140.15917787141538 L 269.854894728388 168.51841867843274 C 260.82014721660886 156.85231133766857 244.7999936089563 150.1419526565927 225.98981633981523 150.1446616739914 Z" zIndex="133.5" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="13350" stroke="#434348" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#434348" d="M 343.0932101349972 140.16000584317752 C 370.2139572459985 175.18047066175023 364.397303400813 215.969962647335 327.9350838477845 246.45690560119152 L 264.17793402538746 208.3303017232927 C 277.83419603026704 196.91197102896442 280.0127180696623 181.6350077384832 269.8551348820214 168.5187287802163 Z" zIndex="133.5" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="13350" stroke="#90ed7d" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#90ed7d" d="M 327.93422178186154 246.45762638803396 C 315.78020694809766 256.6196858136953 302.9985293457346 263.75513228764856 286.1990732704486 269.7565177816505 L 248.5464693896811 217.05674823282789 C 254.83840050402043 214.80903830998074 259.62554567344483 212.13658644707687 264.17761115425526 208.3305716809116 Z" zIndex="133.5" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="13350" stroke="#f7a35c" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#f7a35c" d="M 286.197881700337 269.75694344916695 C 252.59838085501138 281.7596265176404 217.7721459617924 283.4071184252336 182.21099077449188 274.6761534017037 L 209.5996220129183 218.8993083901512 C 222.91840672726306 222.16933274353318 235.96194039513534 221.55229457589527 248.54602310874046 217.05690765886402 Z" zIndex="133.5" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="13350" stroke="#8085e9" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#8085e9" d="M 182.20972963542553 274.6758437621912 C 140.70700507376282 264.4857774126748 111.49490187573844 243.12370846279148 98.48637771837477 213.45108345502024 L 178.24208903309915 195.96857058240457 C 183.11419545907805 207.08191328194437 194.05505808006097 215.08268816954114 209.59914967618934 218.89919242029634 Z" zIndex="133.5" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="13350" stroke="#f15c80" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#f15c80" d="M 98.48598243673742 213.4501817961526 C 91.05085402889895 196.48995053009457 90.31834207311165 180.84609165065262 96.1506258011587 163.5752420746626 L 177.3672755809583 177.28848017777625 C 175.1828996528508 183.75696316503843 175.45724869996215 189.61608634085937 178.2419409875421 195.96823288245415 Z" zIndex="133.5" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="13350" stroke="#e4d354" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#e4d354" d="M 96.15093587055128 163.57432390202857 C 106.34431393525028 133.39052030791098 133.44244369118204 110.72909079954152 173.8406813051117 98.6045010564843 L 206.46467464610924 152.95486930954468 C 191.3342485734764 157.49591415713166 181.18513630533718 165.9833409392925 177.36739171181696 177.2881362928946 Z" zIndex="133.5" transform="translate(0,0)" visibility="visible"></path></g><g zIndex="13350" stroke="#2b908f" stroke-width="1" stroke-linejoin="round" _args="NaN"><path fill="#2b908f" d="M 173.84191019565077 98.60413223874957 C 191.16761213555347 93.40437107065976 207.12507509536272 91.10656222500157 225.94673612005707 91.10125222504513 L 225.98005098129477 150.14466375469854 C 218.93073973609089 150.14665251872717 212.9541618485219 151.00725508264412 206.4651349047381 152.95473117556165 Z" zIndex="133.5" transform="translate(0,0)" visibility="visible"></path></g></g><g class="highcharts-markers" visibility="visible" zIndex="0.1" transform="translate(10,10) scale(1 1)"></g></g><g class="highcharts-data-labels highcharts-tracker" visibility="visible" zIndex="6" transform="translate(10,10) scale(1 1)" opacity="1" style=""><path fill="none" d="M 354.5959854512358 78.46597559496364 C 349.5959854512358 78.46597559496364 305.24538792304315 85.34918822904476 299.6396048545321 94.81360560090629 L 294.033821786021 104.27802297276781" stroke="#7cb5ec" stroke-width="1"></path><path fill="none" d="M 390.4177533455447 221.80743612896782 C 385.4177533455447 221.80743612896782 380.71905024626244 221.00667059597504 369.77429428028887 219.90561798810995 L 358.8295383143153 218.80456538024487" stroke="#434348" stroke-width="1"></path><path fill="none" d="M 350.58260743101397 297 C 345.58260743101397 297 322.3711077333181 300.7114325636941 315.5538589547554 292.0786393256288 L 308.7366101761927 283.4458460875635" stroke="#90ed7d" stroke-width="1"></path><path fill="none" d="M 300.5160783859799 333.4856913550574 C 295.5160783859799 333.4856913550574 236.38625454736643 325.50355632607216 235.6515355761701 314.5281206612175 L 234.91681660497375 303.5526849963628" stroke="#f7a35c" stroke-width="1"></path><path fill="none" d="M 100.354081163438 295.848594318558 C 105.354081163438 295.848594318558 114.22291715698196 290.2870548971609 122.12997767963918 282.6399381927398 L 130.0370382022964 274.9928214883187" stroke="#8085e9" stroke-width="1"></path><path fill="none" d="M 59.92139117354586 213.53447482180053 C 64.92139117354586 213.53447482180053 70.58683683994542 213.26715500573914 81.58069404097822 212.8995902586547 L 92.57455124201101 212.53202551157028" stroke="#f15c80" stroke-width="1"></path><path fill="none" d="M 78.58511092211677 105.18655549078836 C 83.58511092211677 105.18655549078836 107.25208343688988 110.35157830325878 115.65225759891052 117.4534846704056 L 124.05243176093117 124.55539103755243" stroke="#e4d354" stroke-width="1"></path><path fill="none" d="M 112.04824207381269 63.59116338512011 C 117.04824207381269 63.59116338512011 195.0265793163534 71.43085702541282 197.2176251524956 82.21043578081526 L 199.4086709886378 92.9900145362177" stroke="#2b908f" stroke-width="1"></path><g zIndex="1" style="" transform="translate(360,68)"><text x="5" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Bear</tspan></text></g><g zIndex="1" style="" transform="translate(395,212)"><text x="5" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Racoon</tspan></text></g><g zIndex="1" style="" transform="translate(356,287)"><text x="5" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Wolf</tspan></text></g><g zIndex="1" style="" transform="translate(306,323)"><text x="5" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Mouse</tspan></text></g><g zIndex="1" style="" transform="translate(56,286)"><text x="0" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Horse</tspan></text></g><g zIndex="1" style="" transform="translate(25,204)"><text x="0" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Man</tspan></text></g><g zIndex="1" style="" transform="translate(5,95)"><text x="0" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Hightingale</tspan></text></g><g zIndex="1" style="" transform="translate(77,54)"><text x="0" zIndex="1" style="font-size: 11px; font-weight: bold; color: rgb(0, 0, 0); fill: rgb(0, 0, 0); text-shadow: rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px;" y="16"><tspan>Cow</tspan></text></g></g><g class="highcharts-legend" zIndex="7"><g zIndex="1"><g></g></g></g><g class="highcharts-tooltip" zIndex="8" style="cursor:default;padding:0;white-space:nowrap;" transform="translate(148,-9999)" opacity="0" visibility="visible"><path fill="none" d="M 3.5 0.5 L 72.5 0.5 C 75.5 0.5 75.5 0.5 75.5 3.5 L 75.5 44.5 C 75.5 47.5 75.5 47.5 72.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)" width="75" height="47"></path><path fill="none" d="M 3.5 0.5 L 72.5 0.5 C 75.5 0.5 75.5 0.5 75.5 3.5 L 75.5 44.5 C 75.5 47.5 75.5 47.5 72.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)" width="75" height="47"></path><path fill="none" d="M 3.5 0.5 L 72.5 0.5 C 75.5 0.5 75.5 0.5 75.5 3.5 L 75.5 44.5 C 75.5 47.5 75.5 47.5 72.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)" width="75" height="47"></path><path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 72.5 0.5 C 75.5 0.5 75.5 0.5 75.5 3.5 L 75.5 44.5 C 75.5 47.5 75.5 47.5 72.5 47.5 L 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#e4d354" stroke-width="1"></path><text x="8" zIndex="1" style="font-size:12px;color:#333333;fill:#333333;" y="20"><tspan style="font-size: 10px">Hightingale</tspan><tspan x="8" dy="15">Value: 7.0 </tspan></text></g></svg></div></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<h2 id="lineChart" class="sub-header">Line chart</h2>
			<h4>Documentation <a class="link" href="http://api.highcharts.com/highstock">Highstock</a></h4>
			<div class="row">
				<div class="col-xs-12">	
					<div class="p-rl40">
						<div class="chart lineChart">
							<h4 class="text-uc dark-gray bold m-t20">Total sale</h4>
							<ul class="tabs">
								<li class="active">30 days</li>
								<li>7 Days</li>
								<li>1 Day</li>
							</ul>
							<div id="sale"></div>
						</div>
					</div>
				</div>
			</div>

			<h2 class="sub-header">Column Chart</h2>
			<div class="row">
				<div class="col-xs-12">
					<div id="column"></div>
				</div>
			</div>

			<h2 id="countdown" class="sub-header">Countdown</h2>
			<div class="row">
				<div class="col-sm-6 col-md-3">
					<div class="pay">
						<img class="pay_picture" src="images/pay/amazon.png" alt="">
						<a class="pay_link" href="">
							Configure Amazon
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="pay">
						<img class="pay_picture" src="images/pay/ebay.png" alt="">
						<a class="pay_link" href="">
							Configure Ebay
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="pay">
						<img class="pay_picture" src="images/logo_count.png" alt="">
						<ul class="pay_list">
							<li class="pay_item">Days</li>
							<li class="pay_item">Hours</li>
							<li class="pay_item">Minutes</li>
						</ul>
						<div id="countdown"></div>
						<div id="note"></div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="support dSTable">
						<div class="dRow">
							<img src="images/icons/enot.jpg" alt="" class="dCell icon-circle icon-user support_picture">							
							<div class="dCell">
								<p class="tooLight-gray">Your support</p>
								<p class="dark-gray h1 m-t0">Zoye</p>
								<a href="" class="link regRoboto h4">praew@common-services.com</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<h2 id="notification" class="sub-header">Notification</h2>
			<h4 class="">For page <a href="index.php?id=edit-profile" class="link">Edit profile</a></h4>
			<div class="notification good">
				<div class="container">
					<p><i class="fa fa-check"></i>
					Profile edited successfully
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>

			<h4 class="">For page <a href="index.php?id=invitations" class="link">Invitations</a></h4>
			<div class="notification good">
				<div class="container">
					<p><i class="fa fa-check"></i>
					Sent mail success
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>
			<br/>
			<div class="notification good">
				<div class="container">
					<p><i class="fa fa-check"></i>
					Save successful. Your data has been successfully saved.
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>
			<br/>
			<h4 class="">For page <a href="index.php?id=billing-packages-subTotal" class="link">Billing packages Sub Total</a></h4>
			<div class="notification wait">
				<div class="container">
					<p>Please wait...</p>
				</div>
			</div>
			<br/>
			<h4 class="">For page <a href="index.php?id=my-shop-success" class="link">Shop's verifying success</a></h4>
			<div class="notification good">
				<div class="container">
					<p><i class="fa fa-check"></i>
					Save successful. Your data has been successfully saved.
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>

			<h4 class="">For page <a href="index.php?id=rules" class="link">Rules</a></h4>
			<div class="notification good">
				<div class="container">
					<p><i class="fa fa-check"></i>
					Save successful. Your data has been successfully saved.
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>

			<h4 class="">For page <a href="index.php?id=sign-in" class="link">Sign In</a></h4>
			<div class="notification bad">
				<div class="container">
					<p><i class="fa fa-exclamation-triangle"></i>
						Error login mismatch		
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>

			<h4 class="">For page <a href="index.php?id=ebay-auth" class="link">Ebay autentification</a></h4>
			<div class="notification good">
				<div class="container">
					<p><i class="fa fa-check"></i>
					Save successful. Your data has been successfully saved.
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>

			<h4 class="">For page <a href="index.php?id=ebay-auth" class="link">Ebay autentification</a></h4>
			<div class="notification good">
				<div class="container">
					<p>
						Update Auth Security Success.
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>

			<h4 class="">For page <a href="index.php?id=ebay-auth" class="link">Ebay autentification</a></h4>
			<div class="notification warning">
				<div class="container">
					<p>
						Authentication is processing....
					</p>
				</div>
			</div>

			<h4 class="">For page <a href="index.php?id=ebay-auth" class="link">Ebay autentification</a></h4>
			<div class="notification bad">
				<div class="container">
					<p>
						<i class="fa fa-exclamation-triangle"></i>
						Warning : Update Auth Security is fail						
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>

			<h4 class="">For page <a href="index.php?id=amazon-parameters" class="link">Amazon parameters</a></h4>
			<div class="notification good">
				<div class="container">
					<p><i class="fa fa-check"></i>
					Save successful. Your data has been successfully saved.
					</p>
					<i class="fa fa-remove"></i>
				</div>
			</div>

			<h2 id="responsiveTable" class="sub-header">Table with sorting</h2>
			<div class="row">
				<div class="col-xs-12">
					<div class="headSettings clearfix">
						<div class="pull-md-left clearfix">
							<h4 class="headSettings_head">Affiliate Lists</h4>						
						</div>
						<div class="pull-md-right clearfix">
							<div class="showColumns clearfix">
								<a href="" class="showColumns_link">Show / Hide Columns</a>
								<div class="dropCheckbo">
									<ul class="custom-form">
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="id" checked/>
												ID
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="username" checked/>
												Username
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="email" checked/>
												Email
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="status" checked/>
												Status
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="joined" checked/>
												Joined
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="summary" checked/>
												Summary
											</label>
										</li>
									</ul>
								</div>
							</div>	
						</div>			
					</div>
					<div class="row">
						<div class="col-xs-12">
							<table class="responsive-table">
								<thead class="table-head">
									<tr>
										<th>ID</th>
										<th>User Name</th>
										<th>Email</th>
										<th>Status</th>
										<th>Joined</th>
										<th>Summary</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="tableId" data-th="ID:" data-name="">01326564682</td>
										<td class="tableUserName" data-th="User Name:">hmkhmhmhm</td>
										<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
										<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
									</tr>
									<tr>
										<td class="tableId" data-th="ID:">01326564682</td>
										<td class="tableUserName" data-th="User Name:">fffffffffedrs</td>
										<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
										<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">01326564682</td>
										<td class="tableUserName" data-th="User Name:">mn.n.</td>
										<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
										<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
									</tr>
									<tr>
										<td class="tableId" data-th="ID:">01326564682</td>
										<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
										<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
										<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
									</tr>
									<tr>
										<td class="tableId" data-th="ID:">1111111111</td>
										<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
										<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
										<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
										<td class="tableJoined" data-th="Joined:">aaaaaa</td>
										<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">2222222222222</td>
										<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
										<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
										<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
										<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
										<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">333333333333</td>
										<td class="tableUserName" data-th="User Name:">cccccccccc</td>
										<td class="tableEmail" data-th="Email:">ccccccccccc</td>
										<td class="tableStatus" data-th="Status:">cccccccccccc</td>
										<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
										<td class="tableSummary" data-th="Summary:">cccccccccc</td>
									</tr>
									<tr>
										<td class="tableId" data-th="ID:">01326564682</td>
										<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
										<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
										<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
									</tr>
									<tr>
										<td class="tableId" data-th="ID:">1111111111</td>
										<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
										<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
										<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
										<td class="tableJoined" data-th="Joined:">aaaaaa</td>
										<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">2222222222222</td>
										<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
										<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
										<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
										<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
										<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">333333333333</td>
										<td class="tableUserName" data-th="User Name:">cccccccccc</td>
										<td class="tableEmail" data-th="Email:">ccccccccccc</td>
										<td class="tableStatus" data-th="Status:">cccccccccccc</td>
										<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
										<td class="tableSummary" data-th="Summary:">cccccccccc</td>
									</tr>	
									<tr>
										<td class="tableId" data-th="ID:">01326564682</td>
										<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
										<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
										<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
									</tr>
									<tr>
										<td class="tableId" data-th="ID:">1111111111</td>
										<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
										<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
										<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
										<td class="tableJoined" data-th="Joined:">aaaaaa</td>
										<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">2222222222222</td>
										<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
										<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
										<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
										<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
										<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">333333333333</td>
										<td class="tableUserName" data-th="User Name:">cccccccccc</td>
										<td class="tableEmail" data-th="Email:">ccccccccccc</td>
										<td class="tableStatus" data-th="Status:">cccccccccccc</td>
										<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
										<td class="tableSummary" data-th="Summary:">cccccccccc</td>
									</tr>	
									<tr>
										<td class="tableId" data-th="ID:">01326564682</td>
										<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
										<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
										<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
									</tr>
									<tr>
										<td class="tableId" data-th="ID:">1111111111</td>
										<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
										<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
										<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
										<td class="tableJoined" data-th="Joined:">aaaaaa</td>
										<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">2222222222222</td>
										<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
										<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
										<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
										<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
										<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">333333333333</td>
										<td class="tableUserName" data-th="User Name:">cccccccccc</td>
										<td class="tableEmail" data-th="Email:">ccccccccccc</td>
										<td class="tableStatus" data-th="Status:">cccccccccccc</td>
										<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
										<td class="tableSummary" data-th="Summary:">cccccccccc</td>
									</tr>	
									<tr>
										<td class="tableId" data-th="ID:">01326564682</td>
										<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
										<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
										<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
									</tr>
									<tr>
										<td class="tableId" data-th="ID:">1111111111</td>
										<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
										<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
										<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
										<td class="tableJoined" data-th="Joined:">aaaaaa</td>
										<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">2222222222222</td>
										<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
										<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
										<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
										<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
										<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">333333333333</td>
										<td class="tableUserName" data-th="User Name:">cccccccccc</td>
										<td class="tableEmail" data-th="Email:">ccccccccccc</td>
										<td class="tableStatus" data-th="Status:">cccccccccccc</td>
										<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
										<td class="tableSummary" data-th="Summary:">cccccccccc</td>
									</tr>	
									<tr>
										<td class="tableId" data-th="ID:">01326564682</td>
										<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
										<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
										<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
									</tr>
									<tr>
										<td class="tableId" data-th="ID:">1111111111</td>
										<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
										<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
										<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
										<td class="tableJoined" data-th="Joined:">aaaaaa</td>
										<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">2222222222222</td>
										<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
										<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
										<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
										<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
										<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
									</tr>
										<tr>
										<td class="tableId" data-th="ID:">333333333333</td>
										<td class="tableUserName" data-th="User Name:">cccccccccc</td>
										<td class="tableEmail" data-th="Email:">ccccccccccc</td>
										<td class="tableStatus" data-th="Status:">cccccccccccc</td>
										<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
										<td class="tableSummary" data-th="Summary:">cccccccccc</td>
									</tr>								
								</tbody>
							</table>
							<table class="table tableEmpty">
								<tr>
									<td>No data available in table</td>
								</tr>
							</table>
						</div>
					</div>
				</div>			
			</div>

			<h4 class="sub-header">Empty sorting table</h4>
			<div class="row">
				<div class="col-xs-12">
					<div class="headSettings clearfix">
						<div class="pull-md-left clearfix">
							<h4 class="headSettings_head">Affiliate Lists</h4>						
						</div>
						<div class="pull-md-right clearfix">
							<div class="showColumns clearfix">
								<a href="" class="showColumns_link">Show / Hide Columns</a>
								<div class="dropCheckbo">
									<ul class="custom-form">
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="id" checked/>
												ID
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="username" checked/>
												Username
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="email" checked/>
												Email
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="status" checked/>
												Status
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="joined" checked/>
												Joined
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="summary" checked/>
												Summary
											</label>
										</li>
									</ul>
								</div>
							</div>	
						</div>			
					</div>
					<div class="row">
						<div class="col-xs-12">
							<table class="responsive-table">
								<thead class="table-head">
									<tr>
										<th>ID</th>
										<th>User Name</th>
										<th>Email</th>
										<th>Status</th>
										<th>Joined</th>
										<th>Summary</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>			
			</div>

			<h4 class="sub-header m-b30">Statistics</h4>
			<div class="row">
				<div class="col-xs-12 statistics">
					<table class="responsive-table">
						<thead class="table-head">
							<tr>
								<th>ID</th>
								<th>User Name</th>
								<th>Email</th>
								<th>Status</th>
								<th>Joined</th>
								<th>Summary</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="tableId" data-th="ID:" data-name="">01326564682</td>
								<td class="tableUserName" data-th="User Name:">hmkhmhmhm</td>
								<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
								<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
								<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
							</tr>
							<tr>
								<td class="tableId" data-th="ID:">01326564682</td>
								<td class="tableUserName" data-th="User Name:">fffffffffedrs</td>
								<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
								<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
								<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">01326564682</td>
								<td class="tableUserName" data-th="User Name:">mn.n.</td>
								<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
								<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
								<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
							</tr>
							<tr>
								<td class="tableId" data-th="ID:">01326564682</td>
								<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
								<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
								<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
								<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
							</tr>
							<tr>
								<td class="tableId" data-th="ID:">1111111111</td>
								<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
								<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
								<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
								<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">2222222222222</td>
								<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
								<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
								<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
								<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">333333333333</td>
								<td class="tableUserName" data-th="User Name:">cccccccccc</td>
								<td class="tableEmail" data-th="Email:">ccccccccccc</td>
								<td class="tableStatus" data-th="Status:">cccccccccccc</td>
								<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								<td class="tableSummary" data-th="Summary:">cccccccccc</td>
							</tr>
							<tr>
								<td class="tableId" data-th="ID:">01326564682</td>
								<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
								<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
								<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
								<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
							</tr>
							<tr>
								<td class="tableId" data-th="ID:">1111111111</td>
								<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
								<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
								<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
								<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">2222222222222</td>
								<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
								<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
								<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
								<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">333333333333</td>
								<td class="tableUserName" data-th="User Name:">cccccccccc</td>
								<td class="tableEmail" data-th="Email:">ccccccccccc</td>
								<td class="tableStatus" data-th="Status:">cccccccccccc</td>
								<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								<td class="tableSummary" data-th="Summary:">cccccccccc</td>
							</tr>	
							<tr>
								<td class="tableId" data-th="ID:">01326564682</td>
								<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
								<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
								<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
								<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
							</tr>
							<tr>
								<td class="tableId" data-th="ID:">1111111111</td>
								<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
								<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
								<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
								<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">2222222222222</td>
								<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
								<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
								<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
								<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">333333333333</td>
								<td class="tableUserName" data-th="User Name:">cccccccccc</td>
								<td class="tableEmail" data-th="Email:">ccccccccccc</td>
								<td class="tableStatus" data-th="Status:">cccccccccccc</td>
								<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								<td class="tableSummary" data-th="Summary:">cccccccccc</td>
							</tr>	
							<tr>
								<td class="tableId" data-th="ID:">01326564682</td>
								<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
								<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
								<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
								<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
							</tr>
							<tr>
								<td class="tableId" data-th="ID:">1111111111</td>
								<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
								<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
								<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
								<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">2222222222222</td>
								<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
								<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
								<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
								<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">333333333333</td>
								<td class="tableUserName" data-th="User Name:">cccccccccc</td>
								<td class="tableEmail" data-th="Email:">ccccccccccc</td>
								<td class="tableStatus" data-th="Status:">cccccccccccc</td>
								<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								<td class="tableSummary" data-th="Summary:">cccccccccc</td>
							</tr>	
							<tr>
								<td class="tableId" data-th="ID:">01326564682</td>
								<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
								<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
								<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
								<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
							</tr>
							<tr>
								<td class="tableId" data-th="ID:">1111111111</td>
								<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
								<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
								<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
								<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">2222222222222</td>
								<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
								<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
								<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
								<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">333333333333</td>
								<td class="tableUserName" data-th="User Name:">cccccccccc</td>
								<td class="tableEmail" data-th="Email:">ccccccccccc</td>
								<td class="tableStatus" data-th="Status:">cccccccccccc</td>
								<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								<td class="tableSummary" data-th="Summary:">cccccccccc</td>
							</tr>	
							<tr>
								<td class="tableId" data-th="ID:">01326564682</td>
								<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
								<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
								<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
								<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
							</tr>
							<tr>
								<td class="tableId" data-th="ID:">1111111111</td>
								<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
								<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
								<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
								<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">2222222222222</td>
								<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
								<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
								<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
								<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
							</tr>
								<tr>
								<td class="tableId" data-th="ID:">333333333333</td>
								<td class="tableUserName" data-th="User Name:">cccccccccc</td>
								<td class="tableEmail" data-th="Email:">ccccccccccc</td>
								<td class="tableStatus" data-th="Status:">cccccccccccc</td>
								<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								<td class="tableSummary" data-th="Summary:">cccccccccc</td>
							</tr>								
						</tbody>
					</table>
					<table class="table tableEmpty">
						<tr>
							<td>No data available in table</td>
						</tr>
					</table>
				</div>
			</div>

			<h2 id="tags" class="sub-header">Tags</h2>
			<h4>Documentation <a class="link" href="http://timschlechter.github.io/bootstrap-tagsinput/examples/">Bootstrap Tag Input</a></h4>
			<div class="row">
				<div class="col-xs-12">					
					<div class="form-group">
						<input class="form-control tags-input" type="text" data-role="tagsinput"/>					
					</div>
				</div>
			</div>

			<h2 id="header" class="sub-header">Head Title</h2>
			<div class="row">
				<div class="col-xs-12">
					<div class="headSettings clearfix p-b10 b-Bottom m-b10">
						<h4 class="headSettings_head">Information</h4>						
					</div>
				</div>
			</div>

			<h2 id="treeFiles" class="sub-header">Tree of files</h2>
			<div class="tree clearfix custom-form">	
				<i class="icon-folder"></i>
				<p class="treeHead treeRight">Profiles Mapping</p>
				<ul class="tree_point">
					<li>
						<span class="tree_item">
							<p>
								<i class="cb-plus good"></i>									
								<label class="cb-checkbox ">											
									<input type="checkbox"/>
									women
								</label>
							</p>
						</span>
						<div class="treeRight selectTree">
							<div class="tree_show">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<i class="icon-more"></i>
							</div>
						</div>
						<ul class="tree_child">
							<li>
								<span class="tree_item">
									<p>
										<i class="cb-plus good"></i>
										<label class="cb-checkbox">
											<input type="checkbox"/>
											dresses
										</label>
									</p>
								</span>
								<div class="treeRight selectTree">
									<div class="tree_show">
										<select class="search-select">
											<option value="">Home</option>
											<option value="">Apple</option>
											<option value="">Iphone 6</option>
											<option value="">Iphone 5</option>
											<option value="">Iphone 4</option>
											<option value="">Iphone 3</option>
											<option value="">Men shirt</option>
											<option value="">Jack &amp; Jones</option>
											<option value="">Adidas</option>
											<option value="">Armani</option>
											<option value="">Dolce &amp; Gabbana</option>
											<option value="">Mersedes</option>
											<option value="">Audi</option>
										</select>
										<i class="icon-more"></i>
									</div>
								</div>
								<ul class="tree_child">
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>
												<label class="cb-checkbox">
													<input type="checkbox"/>
													small
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
									</li>
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>
												<label class="cb-checkbox">
													<input type="checkbox"/>
													big
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
									</li>
								</ul>
							</li>
							<li>
								<span class="tree_item">
									<p>
										<i class="cb-plus good"></i>
										<label class="cb-checkbox">
											<input type="checkbox"/>
											blouses
										</label>
									</p>
								</span>
								<div class="treeRight selectTree">
									<div class="tree_show">
										<select class="search-select">
											<option value="">Home</option>
											<option value="">Apple</option>
											<option value="">Iphone 6</option>
											<option value="">Iphone 5</option>
											<option value="">Iphone 4</option>
											<option value="">Iphone 3</option>
											<option value="">Men shirt</option>
											<option value="">Jack &amp; Jones</option>
											<option value="">Adidas</option>
											<option value="">Armani</option>
											<option value="">Dolce &amp; Gabbana</option>
											<option value="">Mersedes</option>
											<option value="">Audi</option>
										</select>
										<i class="icon-more"></i>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li>
						<span class="tree_item">
							<p>
								<i class="cb-plus good"></i>
								<label class="cb-checkbox">
									<input type="checkbox"/>
									men
								</label>
							</p>
						</span>
						<div class="treeRight selectTree">
							<div class="tree_show">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<i class="icon-more"></i>
							</div>
						</div>
					</li>
				</ul>
			</div>

			<h2 id="manufacturers" class="sub-header">Clone block</h2>
			<div class="row">
				<div class="col-xs-12">
					<div class="b-Top p-t10">
						<p class="pull-right montserrat poor-gray"><span class="dark-gray count">0</span> Records</p>
					</div>
				</div>
			</div>
			<div class="row showSelectBlock">
				<div class="col-sm-6">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
					<i class="blockRight noBorder"></i>
				</div>
				<div class="col-sm-6">
					<div class="form-group withIcon clearfix">
						<input type="text" class="form-control">
						<i class="cb-plus bad"></i>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 m-b20">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
					<i class="blockRight noBorder"></i>
				</div>
				<div class="col-sm-6">
					<div class="form-group withIcon clearfix">
						<input type="text" class="form-control">
						<i class="cb-plus good"></i>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 m-b20">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
					<i class="blockRight noBorder"></i>
				</div>
				<div class="col-sm-6">
					<div class="form-group withIcon clearfix">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
						<i class="cb-plus good"></i>
					</div>
				</div>
			</div>

			<h2 id="anotherTables" class="sub-header">Another Tables</h2>
			<div class="row">
				<div class="col-xs-12">
					<div class="headSettings clearfix">
						<div class="pull-sm-left clearfix">
							<h4 class="headSettings_head">Profile mapping</h4>						
						</div>
						<div class="pull-sm-right clearfix">
							<div class="showColumns clearfix">
								<a href="" class="showColumns_link">Show / Hide Columns</a>
								<div class="dropCheckbo">
									<ul class="custom-form">
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="id" checked/>
												Select
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="username" checked/>
												Picture
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="email" checked/>
												ID
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="status" checked/>
												Contents
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="joined" checked/>
												Informations
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="summary" checked/>
												Details
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="error" checked/>
												Active
											</label>
										</li>
									</ul>
								</div>
							</div>	
						</div>			
					</div>
					<div class="row">
						<div class="col-xs-12">
							<table class="responsive-table allCheckBo">
								<thead class="table-head">
									<tr>
										<th class="tableId tableNoSort ">
											<label class="cb-checkbox checkAll">
												<input type="checkbox">												
											</label>
										</th>
										<th class="tableUserName tableNoSort tablePicture">Picture</th>
										<th class="tableEmail">ID</th>
										<th class="tableStatus">Contents</th>
										<th class="tableJoined tableNoSort">Informations</th>
										<th class="tableSummary tableNoSort">Details</th>
										<th class="tableError tableNoSort">Active</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="tableId checkboSettings">
											<label class="cb-checkbox">
												<input type="checkbox">												
											</label>
										</td>										
										<td class="tableUserName tablePicture" data-th=""><i class="fa fa-ban faError"></i></td>
										<td class="tableEmail" data-th="ID:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Contents:"><span class="true-green">Category</span>: Women</td>
										<td class="tableJoined" data-th="Informations:">Home <span class="true-blue smallText">>></span> Women</td>
										<td class="tableSummary" data-th="Details:"></td>
										<td class="tableError" data-th="Active:"></td>
									</tr>
									<tr>
										<td class="tableId tableNoSort checkboSettings">
											<label class="cb-checkbox">
												<input type="checkbox">												
											</label>
										</td>	
										<td class="tableUserName tablePicture" data-th="User Name:"><i class="fa fa-plus-square-o faGood"></i></td>
										<td class="tableEmail" data-th="ID:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Contents:"><span class="true-green">Category</span>: Women</td>
										<td class="tableJoined" data-th="Informations:">Home <span class="true-blue smallText">>></span> Women <span class="true-blue smallText">>></span> Tops</td>
										<td class="tableSummary" data-th="Details:"></td>
										<td class="tableError" data-th="Active:"></td>
									</tr>
										<tr>
										<td class="tableId tableNoSort checkboSettings">
											<label class="cb-checkbox">
												<input type="checkbox">												
											</label>
										</td>	
										<td class="tableUserName tablePicture" data-th="User Name:"><i class="fa fa-plus-square-o faGood"></i></td>
										<td class="tableEmail" data-th="ID:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Contents:"><span class="true-green">Category</span>: Women</td>
										<td class="tableJoined" data-th="Informations:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Details:"></td>
										<td class="tableError" data-th="Active:"></td>
									</tr>
									<tr>
										<td class="tableId tableNoSort checkboSettings">
											<label class="cb-checkbox">
												<input type="checkbox">												
											</label>
										</td>	
										<td class="tableUserName tablePicture" data-th="User Name:"><i class="fa fa-plus-square-o faGood"></i></td>
										<td class="tableEmail" data-th="ID:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Contents:"><span class="true-green">Category</span>: Women</td>
										<td class="tableJoined" data-th="Informations:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Details:"></td>
										<td class="tableError" data-th="Active:"></td>
									</tr>														
								</tbody>
							</table>
							<table class="table tableEmpty">
								<tr>
									<td>No data available in table</td>
								</tr>
							</table>
						</div>
					</div>
				</div>			
			</div>

			<br/>
			<br/>

			<div class="row">
				<div class="col-xs-12">
					<div class="headSettings clearfix">
						<div class="pull-sm-left clearfix">
							<h4 class="headSettings_head">Profile mapping</h4>						
						</div>
						<div class="pull-sm-right clearfix">
							<div class="showColumns clearfix">
								<a href="" class="showColumns_link">Show / Hide Columns</a>
								<div class="dropCheckbo">
									<ul class="custom-form">
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="id" checked/>
												Select
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="username" checked/>
												Picture
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="email" checked/>
												ID
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="status" checked/>
												Contents
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="joined" checked/>
												Informations
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="summary" checked/>
												Details
											</label>
										</li>
										<li>
											<label class="cb-checkbox checked">
												<input type="checkbox" name="error" checked/>
												Active
											</label>
										</li>
									</ul>
								</div>
							</div>	
						</div>			
					</div>
					<div class="row">
						<div class="col-xs-12">
							<table class="responsive-table allCheckBo">
								<thead class="table-head">
									<tr>
										<th class="tableId tableNoSort ">
											<label class="cb-checkbox checkAll">
												<input type="checkbox">												
											</label>
										</th>
										<th class="tableUserName tableNoSort tablePicture">Picture</th>
										<th class="tableEmail">ID</th>
										<th class="tableStatus">Contents</th>
										<th class="tableJoined tableNoSort">Informations</th>
										<th class="tableSummary tableNoSort">Details</th>
										<th class="tableError tableNoSort">Active</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="tableId tableNoSort checkboSettings">
											<label class="cb-checkbox">
												<input type="checkbox">												
											</label>
										</td>									
										<td class="tableUserName tablePicture" data-th=""><i class="fa fa-ban faError"></i></td>
										<td class="tableEmail" data-th="ID:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Contents:"><span class="true-green">Category</span>: Women</td>
										<td class="tableJoined" data-th="Informations:">Home <span class="true-blue smallText">>></span> Women</td>
										<td class="tableSummary" data-th="Details:"></td>
										<td class="tableError" data-th="Active:"></td>
									</tr>
									<tr>
										<td class="tableId checkboSettings">&nbsp;</td>										
										<td class="tableUserName tablePicture" data-th=""><i class="fa fa-minus-square-o faError"></i></td>
										<td class="tableEmail" data-th="ID:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Contents:"><span class="true-green">Category</span>: Women</td>
										<td class="tableJoined" data-th="Informations:">Home <span class="true-blue smallText">>></span> Women</td>
										<td class="tableSummary" data-th="Details:"></td>
										<td class="tableError" data-th="Active:"></td>
									</tr>
									<tr>
										<td class="tableId tableNoSort checkboSettings">
											<label class="cb-checkbox">
												<input type="checkbox">												
											</label>
										</td>	
										<td class="tableUserName tablePicture" data-th="User Name:">
											<a href="images/profile/enot.jpg" class="gallery-image">
												<img class="image-table" src="images/profile/enot.jpg" alt="">												
											</a>
										</td>
										<td class="tableEmail" data-th="ID:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Contents:"><span class="true-green">Category</span>: Women</td>
										<td class="tableJoined" data-th="Informations:">Home <span class="true-blue smallText">>></span> Women <span class="true-blue smallText">>></span> Tops</td>
										<td class="tableSummary" data-th="Details:"></td>
										<td class="tableError" data-th="Active:"></td>
									</tr>
										<tr>
										<td class="tableId tableNoSort checkboSettings">
											<label class="cb-checkbox">
												<input type="checkbox">												
											</label>
										</td>	
										<td class="tableUserName tablePicture" data-th="User Name:"><i class="fa fa-plus-square-o faGood"></i></td>
										<td class="tableEmail" data-th="ID:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Contents:"><span class="true-green">Category</span>: Women</td>
										<td class="tableJoined" data-th="Informations:">Home <span class="true-blue smallText">>></span> Women</td>
										<td class="tableSummary" data-th="Details:"></td>
										<td class="tableError" data-th="Active:"></td>
									</tr>
									<tr>
										<td class="tableId tableNoSort checkboSettings">
											<label class="cb-checkbox">
												<input type="checkbox">												
											</label>
										</td>	
										<td class="tableUserName tablePicture" data-th="User Name:"><i class="fa fa-plus-square-o faGood"></i></td>
										<td class="tableEmail" data-th="ID:">Ali Nickerman@gmail.com</td>
										<td class="tableStatus" data-th="Contents:"><span class="true-green">Category</span>: Women</td>
										<td class="tableJoined" data-th="Informations:">Lorem Ipsum</td>
										<td class="tableSummary" data-th="Details:"></td>
										<td class="tableError" data-th="Active:"></td>
									</tr>														
								</tbody>
							</table>
							<table class="table tableEmpty">
								<tr>
									<td>No data available in table</td>
								</tr>
							</table>
						</div>
					</div>
				</div>			
			</div>

			<h2 id="calendar" class="sub-header">Calendar</h2>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group calendar firstDate">
						<input type="text" class="form-control inputDate">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group calendar lastDate">
						<input type="text" class="form-control inputDate">
					</div>
				</div>
			</div>

			<h2 id="notice" class="sub-header">Notice</h2>
			<div class="row">
				<div class="col-xs-12">
					<div class="validate blue">
						<div class="validateRow">
							<div class="validateCell">
								<i class="note"></i>
							</div>
							<div class="validateCell">
								<p class="pull-left">Security token used between your Shop and Feed.biz</p>
								<i class="fa fa-remove pull-right"></i>
							</div>
						</div>
					</div>
				</div>
			</div>

			<br/>

			<div class="row">
				<div class="col-xs-12">
					<div class="validate blue">
						<div class="validateRow">
							<div class="validateCell">
								<i class="note"></i>
							</div>
							<div class="validateCell">
								<p class="pull-left">Associated carrier, usefull to display your preferate carrier on the order page and on the invoice</p>
								<i class="fa fa-remove pull-right"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br/>

			<div class="row">
				<div class="col-xs-12">
					<div class="validate yellow">
						<div class="validateRow">
							<div class="validateCell">
								<i class="note"></i>
							</div>
							<div class="validateCell">
								<p class=""><span class="bold">Missing API Keys:</span> Missing API connection keys. API connection doesn't exits. To setting API key please go to Configurations > Parameters Tab.</p>
								<p class="pull-left"><span class="bold">Profiles doesn't exits:</span> Missing Profiles to map with Category. To create a new profile please go to Configurations > Profiles. </p>
								<i class="fa fa-remove pull-right"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br/>

			<div class="row">
				<div class="col-xs-12">
					<div class="validate pink">
						<div class="validateRow">
							<div class="validateCell">
								<i class="note"></i>
							</div>
							<div class="validateCell">
								<div class="pull-left">
									<p>
										Update Categories Selected Failed : Please, try again.
									</p>								
								</div>
								<i class="fa fa-remove pull-right"></i>
							</div>
						</div>
					</div>
				</div>
			</div>

			<h2 id="flag" class="sub-header">Flag</h2>
			<div class="row">
				<div class="col-xs-12 b-Bottom p-b10 m-b10">
					<div class="row">
						<div class="col-xs-2">
							<p class="bold black">Country</p>
						</div>
						<div class="col-xs-1">
							<p class="bold black">Flag</p>
						</div>
						<div class="col-xs-2">
							<p class="bold black">Country</p>
						</div>
						<div class="col-xs-1">
							<p class="bold black">Flag</p>
						</div>
						<div class="col-xs-2">
							<p class="bold black">Country</p>
						</div>
						<div class="col-xs-1">
							<p class="bold black">Flag</p>
						</div>
						<div class="col-xs-2">
							<p class="bold black">Country</p>
						</div>
						<div class="col-xs-1">
							<p class="bold black">Flag</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php						
					if ($handle = opendir('./flags')) {  				    
					    while (false !== ($file = readdir($handle))) {
					    	$file_name = substr($file, 0, strlen($file)-3);
					        echo '<div class="col-xs-2">
					        	<p class="bold black">'.$file_name.'</p>
					        </div>
					        <div class="col-xs-1">
					        	<img src="flags/' . $file . '" alt=""><p class="bold black">
					        </div>';
					    }
					    closedir($handle); 
					}					
				?>
			</div>
			<h2 id="progresBar" class="sub-header">Progress Bar</h2>
			<code>data-value="#"</code>
			<div class="row progressbar">
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="5"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="10"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="15"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="20"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="25"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="30"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="35"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="40"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="45"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="50"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="55"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="60"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="65"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="70"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="75"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="80"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="85"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="90"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="95"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="relative">
						<div class="progressCondition">
							<div class="progressLine" data-value="100"></div>
						</div>
					</div>
				</div>
			</div>									
				
            <h2 id="cssHelper" class="sub-header">Css Helper</h2>
            <h3>Margin classes</h3>
            <div class="row">
                <div class="col-xs-12">
                    <div class="well well-default-o">
                        <dl class="togglable togglable-list">
                            <dt class="togglable-heading"><strong>All sides:</strong> <code class="small">m-*</code></dt>
                            <dd class="togglable-content">
                                      <span class="label label-default" style="display: inline-block;">m-0</span>
                                      <span class="label label-default" style="display: inline-block;">m-1</span>
                                      <span class="label label-default" style="display: inline-block;">m-2</span>
                                      <span class="label label-default" style="display: inline-block;">m-3</span>
                                      <span class="label label-default" style="display: inline-block;">m-4</span>
                                      <span class="label label-default" style="display: inline-block;">m-5</span>
                                      <span class="label label-default" style="display: inline-block;">m-6</span>
                                      <span class="label label-default" style="display: inline-block;">m-7</span>
                                      <span class="label label-default" style="display: inline-block;">m-8</span>
                                      <span class="label label-default" style="display: inline-block;">m-9</span>
                                      <span class="label label-default" style="display: inline-block;">m-10</span>
                                      <span class="label label-default" style="display: inline-block;">m-11</span>
                                      <span class="label label-default" style="display: inline-block;">m-12</span>
                                      <span class="label label-default" style="display: inline-block;">m-13</span>
                                      <span class="label label-default" style="display: inline-block;">m-14</span>
                                      <span class="label label-default" style="display: inline-block;">m-15</span>
                                      <span class="label label-default" style="display: inline-block;">m-16</span>
                                      <span class="label label-default" style="display: inline-block;">m-17</span>
                                      <span class="label label-default" style="display: inline-block;">m-18</span>
                                      <span class="label label-default" style="display: inline-block;">m-19</span>
                                      <span class="label label-default" style="display: inline-block;">m-20</span>
                                      <span class="label label-default" style="display: inline-block;">m-21</span>
                                      <span class="label label-default" style="display: inline-block;">m-22</span>
                                      <span class="label label-default" style="display: inline-block;">m-23</span>
                                      <span class="label label-default" style="display: inline-block;">m-24</span>
                                      <span class="label label-default" style="display: inline-block;">m-25</span>
                                      <span class="label label-default" style="display: inline-block;">m-26</span>
                                      <span class="label label-default" style="display: inline-block;">m-27</span>
                                      <span class="label label-default" style="display: inline-block;">m-28</span>
                                      <span class="label label-default" style="display: inline-block;">m-29</span>
                                      <span class="label label-default" style="display: inline-block;">m-30</span>
                                      <span class="label label-default" style="display: inline-block;">m-31</span>
                                      <span class="label label-default" style="display: inline-block;">m-32</span>
                                      <span class="label label-default" style="display: inline-block;">m-33</span>
                                      <span class="label label-default" style="display: inline-block;">m-34</span>
                                      <span class="label label-default" style="display: inline-block;">m-35</span>
                                      <span class="label label-default" style="display: inline-block;">m-36</span>
                                      <span class="label label-default" style="display: inline-block;">m-37</span>
                                      <span class="label label-default" style="display: inline-block;">m-38</span>
                                      <span class="label label-default" style="display: inline-block;">m-39</span>
                                      <span class="label label-default" style="display: inline-block;">m-40</span>
                            </dd>
                            <dt class="togglable-heading"><strong>Negative all sides:</strong> <code class="small">m--*</code></dt>
                            <dd class="togglable-content">
                                      <span class="label label-warning" style="display: inline-block;">m--0</span>
                                      <span class="label label-warning" style="display: inline-block;">m--1</span>
                                      <span class="label label-warning" style="display: inline-block;">m--2</span>
                                      <span class="label label-warning" style="display: inline-block;">m--3</span>
                                      <span class="label label-warning" style="display: inline-block;">m--4</span>
                                      <span class="label label-warning" style="display: inline-block;">m--5</span>
                                      <span class="label label-warning" style="display: inline-block;">m--6</span>
                                      <span class="label label-warning" style="display: inline-block;">m--7</span>
                                      <span class="label label-warning" style="display: inline-block;">m--8</span>
                                      <span class="label label-warning" style="display: inline-block;">m--9</span>
                                      <span class="label label-warning" style="display: inline-block;">m--10</span>
                                      <span class="label label-warning" style="display: inline-block;">m--11</span>
                                      <span class="label label-warning" style="display: inline-block;">m--12</span>
                                      <span class="label label-warning" style="display: inline-block;">m--13</span>
                                      <span class="label label-warning" style="display: inline-block;">m--14</span>
                                      <span class="label label-warning" style="display: inline-block;">m--15</span>
                                      <span class="label label-warning" style="display: inline-block;">m--16</span>
                                      <span class="label label-warning" style="display: inline-block;">m--17</span>
                                      <span class="label label-warning" style="display: inline-block;">m--18</span>
                                      <span class="label label-warning" style="display: inline-block;">m--19</span>
                                      <span class="label label-warning" style="display: inline-block;">m--20</span>
                                      <span class="label label-warning" style="display: inline-block;">m--21</span>
                                      <span class="label label-warning" style="display: inline-block;">m--22</span>
                                      <span class="label label-warning" style="display: inline-block;">m--23</span>
                                      <span class="label label-warning" style="display: inline-block;">m--24</span>
                                      <span class="label label-warning" style="display: inline-block;">m--25</span>
                                      <span class="label label-warning" style="display: inline-block;">m--26</span>
                                      <span class="label label-warning" style="display: inline-block;">m--27</span>
                                      <span class="label label-warning" style="display: inline-block;">m--28</span>
                                      <span class="label label-warning" style="display: inline-block;">m--29</span>
                                      <span class="label label-warning" style="display: inline-block;">m--30</span>
                                      <span class="label label-warning" style="display: inline-block;">m--31</span>
                                      <span class="label label-warning" style="display: inline-block;">m--32</span>
                                      <span class="label label-warning" style="display: inline-block;">m--33</span>
                                      <span class="label label-warning" style="display: inline-block;">m--34</span>
                                      <span class="label label-warning" style="display: inline-block;">m--35</span>
                                      <span class="label label-warning" style="display: inline-block;">m--36</span>
                                      <span class="label label-warning" style="display: inline-block;">m--37</span>
                                      <span class="label label-warning" style="display: inline-block;">m--38</span>
                                      <span class="label label-warning" style="display: inline-block;">m--39</span>
                                      <span class="label label-warning" style="display: inline-block;">m--40</span>
                            </dd>
                            <hr class="hr-md">
                            <dt class="togglable-heading"><strong>Top:</strong> <code class="small">m-t*</code></dt>
                            <dd class="togglable-content">
                                    <p>Top only:
                                      <span class="label label-default" style="display: inline-block;">m-t0</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t1</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t2</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t3</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t4</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t5</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t6</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t7</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t8</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t9</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t10</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t11</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t12</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t13</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t14</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t15</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t16</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t17</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t18</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t19</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t20</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t21</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t22</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t23</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t24</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t25</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t26</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t27</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t28</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t29</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t30</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t31</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t32</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t33</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t34</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t35</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t36</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t37</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t38</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t39</span> 
                                      <span class="label label-default" style="display: inline-block;">m-t40</span> 
                                    </p>
                                    <p>Top and right:
                                      <span class="label label-info" style="display: inline-block;">m-tr0</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr1</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr2</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr3</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr4</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr5</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr6</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr7</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr8</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr9</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr10</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr11</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr12</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr13</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr14</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr15</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr16</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr17</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr18</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr19</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr20</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr21</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr22</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr23</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr24</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr25</span>
                                      <span class="label label-info" style="display: inline-block;">m-tr26</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr27</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr28</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr29</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr30</span>
                                      <span class="label label-info" style="display: inline-block;">m-tr31</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr32</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr33</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr34</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr35</span>
                                      <span class="label label-info" style="display: inline-block;">m-tr36</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr37</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr38</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr39</span> 
                                      <span class="label label-info" style="display: inline-block;">m-tr40</span>
                                    </p>
                                    <p>Top and bottom:
                                      <span class="label label-primary" style="display: inline-block;">m-tb0</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb1</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb2</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb3</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb4</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb5</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb6</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb7</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb8</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb9</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb10</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb11</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb12</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb13</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb14</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb15</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb16</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb17</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb18</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb19</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb20</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb21</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb22</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb23</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb24</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb25</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb26</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb27</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb28</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb29</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb30</span>
                                      <span class="label label-primary" style="display: inline-block;">m-tb31</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb32</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb33</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb34</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb35</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb36</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb37</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb38</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb39</span> 
                                      <span class="label label-primary" style="display: inline-block;">m-tb40</span>
                                    </p>
                                    <p>Top and left:
                                      <span class="label label-success" style="display: inline-block;">m-tl0</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl1</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl2</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl3</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl4</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl5</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl6</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl7</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl8</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl9</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl10</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl11</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl12</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl13</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl14</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl15</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl16</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl17</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl18</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl19</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl20</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl21</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl22</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl23</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl24</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl25</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl26</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl27</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl28</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl29</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl30</span>
                                      <span class="label label-success" style="display: inline-block;">m-tl31</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl32</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl33</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl34</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl35</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl36</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl37</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl38</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl39</span> 
                                      <span class="label label-success" style="display: inline-block;">m-tl40</span>
                                   </p>
                            </dd>
                            <dt class="togglable-heading"><strong>Negative top:</strong> <code class="small">m--t*</code></dt>
                            <dd class="togglable-content">
                                <p>Negative top only:
                                  <span class="label label-default" style="display: inline-block;">m--t0</span>
                                  <span class="label label-default" style="display: inline-block;">m--t1</span>
                                  <span class="label label-default" style="display: inline-block;">m--t2</span>
                                  <span class="label label-default" style="display: inline-block;">m--t3</span>
                                  <span class="label label-default" style="display: inline-block;">m--t4</span>
                                  <span class="label label-default" style="display: inline-block;">m--t5</span>
                                  <span class="label label-default" style="display: inline-block;">m--t6</span>
                                  <span class="label label-default" style="display: inline-block;">m--t7</span>
                                  <span class="label label-default" style="display: inline-block;">m--t8</span>
                                  <span class="label label-default" style="display: inline-block;">m--t9</span>
                                  <span class="label label-default" style="display: inline-block;">m--t10</span>
                                  <span class="label label-default" style="display: inline-block;">m--t11</span>
                                  <span class="label label-default" style="display: inline-block;">m--t12</span>
                                  <span class="label label-default" style="display: inline-block;">m--t13</span>
                                  <span class="label label-default" style="display: inline-block;">m--t14</span>
                                  <span class="label label-default" style="display: inline-block;">m--t15</span>
                                  <span class="label label-default" style="display: inline-block;">m--t16</span>
                                  <span class="label label-default" style="display: inline-block;">m--t17</span>
                                  <span class="label label-default" style="display: inline-block;">m--t18</span>
                                  <span class="label label-default" style="display: inline-block;">m--t19</span>
                                  <span class="label label-default" style="display: inline-block;">m--t20</span>
                                  <span class="label label-default" style="display: inline-block;">m--t21</span>
                                  <span class="label label-default" style="display: inline-block;">m--t22</span>
                                  <span class="label label-default" style="display: inline-block;">m--t23</span>
                                  <span class="label label-default" style="display: inline-block;">m--t24</span>
                                  <span class="label label-default" style="display: inline-block;">m--t25</span>
                                  <span class="label label-default" style="display: inline-block;">m--t26</span>
                                  <span class="label label-default" style="display: inline-block;">m--t27</span>
                                  <span class="label label-default" style="display: inline-block;">m--t28</span>
                                  <span class="label label-default" style="display: inline-block;">m--t29</span>
                                  <span class="label label-default" style="display: inline-block;">m--t30</span>
                                  <span class="label label-default" style="display: inline-block;">m--t31</span>
                                  <span class="label label-default" style="display: inline-block;">m--t32</span>
                                  <span class="label label-default" style="display: inline-block;">m--t33</span>
                                  <span class="label label-default" style="display: inline-block;">m--t34</span>
                                  <span class="label label-default" style="display: inline-block;">m--t35</span>
                                  <span class="label label-default" style="display: inline-block;">m--t36</span>
                                  <span class="label label-default" style="display: inline-block;">m--t37</span>
                                  <span class="label label-default" style="display: inline-block;">m--t38</span>
                                  <span class="label label-default" style="display: inline-block;">m--t39</span>
                                  <span class="label label-default" style="display: inline-block;">m--t40</span>
                                </p>
                                <p>Negative top and right:
                                  <span class="label label-info" style="display: inline-block;">m--tr0</span>
                                  <span class="label label-info" style="display: inline-block;">m--tr1</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr2</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr3</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr4</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr5</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr6</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr7</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr8</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr9</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr10</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr11</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr12</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr13</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr14</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr15</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr16</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr17</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr18</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr19</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr20</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr21</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr22</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr23</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr24</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr25</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr26</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr27</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr28</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr29</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr30</span>
                                  <span class="label label-info" style="display: inline-block;">m--tr31</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr32</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr33</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr34</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr35</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr36</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr37</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr38</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr39</span> 
                                  <span class="label label-info" style="display: inline-block;">m--tr40</span>  
                                </p>
                                <p>Negative top and bottom:
                                  <span class="label label-primary" style="display: inline-block;">m--tb0</span>
                                  <span class="label label-primary" style="display: inline-block;">m--tb1</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb2</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb3</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb4</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb5</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb6</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb7</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb8</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb9</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb10</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb11</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb12</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb13</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb14</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb15</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb16</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb17</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb18</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb19</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb20</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb21</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb22</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb23</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb24</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb25</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb26</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb27</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb28</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb29</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb30</span>
                                  <span class="label label-primary" style="display: inline-block;">m--tb31</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb32</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb33</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb34</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb35</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb36</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb37</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb38</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb39</span> 
                                  <span class="label label-primary" style="display: inline-block;">m--tb40</span>
                                 </p>
                                <p>Negative top and left:
                                  <span class="label label-success" style="display: inline-block;">m--tl0</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl1</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl2</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl3</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl4</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl5</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl6</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl7</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl8</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl9</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl10</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl11</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl12</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl13</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl14</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl15</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl16</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl17</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl18</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl19</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl20</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl21</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl22</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl23</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl24</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl25</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl26</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl27</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl28</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl29</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl30</span>
                                  <span class="label label-success" style="display: inline-block;">m--tl31</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl32</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl33</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl34</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl35</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl36</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl37</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl38</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl39</span> 
                                  <span class="label label-success" style="display: inline-block;">m--tl40</span>  
                                </p>
                            </dd>
                            <hr class="hr-md">
                            <dt class="togglable-heading"><strong>Right:</strong> <code class="small">m-r*</code></dt>
                            <dd class="togglable-content">
                                <p>Right only:
                                    <span class="label label-default" style="display: inline-block;">m-r0</span>
                                    <span class="label label-default" style="display: inline-block;">m-r1</span>
                                    <span class="label label-default" style="display: inline-block;">m-r2</span>
                                    <span class="label label-default" style="display: inline-block;">m-r3</span>
                                    <span class="label label-default" style="display: inline-block;">m-r4</span>
                                    <span class="label label-default" style="display: inline-block;">m-r5</span>
                                    <span class="label label-default" style="display: inline-block;">m-r6</span>
                                    <span class="label label-default" style="display: inline-block;">m-r7</span>
                                    <span class="label label-default" style="display: inline-block;">m-r8</span>
                                    <span class="label label-default" style="display: inline-block;">m-r9</span>
                                    <span class="label label-default" style="display: inline-block;">m-r10</span>
                                    <span class="label label-default" style="display: inline-block;">m-r11</span>
                                    <span class="label label-default" style="display: inline-block;">m-r12</span>
                                    <span class="label label-default" style="display: inline-block;">m-r13</span>
                                    <span class="label label-default" style="display: inline-block;">m-r14</span>
                                    <span class="label label-default" style="display: inline-block;">m-r15</span>
                                    <span class="label label-default" style="display: inline-block;">m-r16</span>
                                    <span class="label label-default" style="display: inline-block;">m-r17</span>
                                    <span class="label label-default" style="display: inline-block;">m-r18</span>
                                    <span class="label label-default" style="display: inline-block;">m-r19</span>
                                    <span class="label label-default" style="display: inline-block;">m-r20</span>
                                    <span class="label label-default" style="display: inline-block;">m-r21</span>
                                    <span class="label label-default" style="display: inline-block;">m-r22</span>
                                    <span class="label label-default" style="display: inline-block;">m-r23</span>
                                    <span class="label label-default" style="display: inline-block;">m-r24</span>
                                    <span class="label label-default" style="display: inline-block;">m-r25</span>
                                    <span class="label label-default" style="display: inline-block;">m-r26</span>
                                    <span class="label label-default" style="display: inline-block;">m-r27</span>
                                    <span class="label label-default" style="display: inline-block;">m-r28</span>
                                    <span class="label label-default" style="display: inline-block;">m-r29</span>
                                    <span class="label label-default" style="display: inline-block;">m-r30</span>
                                    <span class="label label-default" style="display: inline-block;">m-r31</span>
                                    <span class="label label-default" style="display: inline-block;">m-r32</span>
                                    <span class="label label-default" style="display: inline-block;">m-r33</span>
                                    <span class="label label-default" style="display: inline-block;">m-r34</span>
                                    <span class="label label-default" style="display: inline-block;">m-r35</span>
                                    <span class="label label-default" style="display: inline-block;">m-r36</span>
                                    <span class="label label-default" style="display: inline-block;">m-r37</span>
                                    <span class="label label-default" style="display: inline-block;">m-r38</span>
                                    <span class="label label-default" style="display: inline-block;">m-r39</span>
                                    <span class="label label-default" style="display: inline-block;">m-r40</span>
                                  </p>
                                <p>Right and bottom:
                                    <span class="label label-info" style="display: inline-block;">m-rb0</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb1</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb2</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb3</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb4</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb5</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb6</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb7</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb8</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb9</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb10</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb11</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb12</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb13</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb14</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb15</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb16</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb17</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb18</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb19</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb20</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb21</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb22</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb23</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb24</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb25</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb26</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb27</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb28</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb29</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb30</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb31</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb32</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb33</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb34</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb35</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb36</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb37</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb38</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb39</span>
                                    <span class="label label-info" style="display: inline-block;">m-rb40</span>
                                  </p>
                                <p>Right and left: 
                                    <span class="label label-primary" style="display: inline-block;">m-rl0</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl1</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl2</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl3</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl4</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl5</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl6</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl7</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl8</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl9</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl10</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl11</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl12</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl13</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl14</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl15</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl16</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl17</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl18</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl19</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl20</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl21</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl22</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl23</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl24</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl25</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl26</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl27</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl28</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl29</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl30</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl31</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl32</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl33</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl34</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl35</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl36</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl37</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl38</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl39</span>
                                    <span class="label label-primary" style="display: inline-block;">m-rl40</span>
                                </p>
                            </dd>
                            <dt class="togglable-heading"><strong>Negative right:</strong> <code class="small">m--r*</code></dt>
                            <dd class="togglable-content">
                                <p>Negative right only:
                                    <span class="label label-default" style="display: inline-block;">m--r0</span>
                                    <span class="label label-default" style="display: inline-block;">m--r1</span>
                                    <span class="label label-default" style="display: inline-block;">m--r2</span>
                                    <span class="label label-default" style="display: inline-block;">m--r3</span>
                                    <span class="label label-default" style="display: inline-block;">m--r4</span>
                                    <span class="label label-default" style="display: inline-block;">m--r5</span>
                                    <span class="label label-default" style="display: inline-block;">m--r6</span>
                                    <span class="label label-default" style="display: inline-block;">m--r7</span>
                                    <span class="label label-default" style="display: inline-block;">m--r8</span>
                                    <span class="label label-default" style="display: inline-block;">m--r9</span>
                                    <span class="label label-default" style="display: inline-block;">m--r10</span>
                                    <span class="label label-default" style="display: inline-block;">m--r11</span>
                                    <span class="label label-default" style="display: inline-block;">m--r12</span>
                                    <span class="label label-default" style="display: inline-block;">m--r13</span>
                                    <span class="label label-default" style="display: inline-block;">m--r14</span>
                                    <span class="label label-default" style="display: inline-block;">m--r15</span>
                                    <span class="label label-default" style="display: inline-block;">m--r16</span>
                                    <span class="label label-default" style="display: inline-block;">m--r17</span>
                                    <span class="label label-default" style="display: inline-block;">m--r18</span>
                                    <span class="label label-default" style="display: inline-block;">m--r19</span>
                                    <span class="label label-default" style="display: inline-block;">m--r20</span>
                                    <span class="label label-default" style="display: inline-block;">m--r21</span>
                                    <span class="label label-default" style="display: inline-block;">m--r22</span>
                                    <span class="label label-default" style="display: inline-block;">m--r23</span>
                                    <span class="label label-default" style="display: inline-block;">m--r24</span>
                                    <span class="label label-default" style="display: inline-block;">m--r25</span>
                                    <span class="label label-default" style="display: inline-block;">m--r26</span>
                                    <span class="label label-default" style="display: inline-block;">m--r27</span>
                                    <span class="label label-default" style="display: inline-block;">m--r28</span>
                                    <span class="label label-default" style="display: inline-block;">m--r29</span>
                                    <span class="label label-default" style="display: inline-block;">m--r30</span>
                                    <span class="label label-default" style="display: inline-block;">m--r31</span>
                                    <span class="label label-default" style="display: inline-block;">m--r32</span>
                                    <span class="label label-default" style="display: inline-block;">m--r33</span>
                                    <span class="label label-default" style="display: inline-block;">m--r34</span>
                                    <span class="label label-default" style="display: inline-block;">m--r35</span>
                                    <span class="label label-default" style="display: inline-block;">m--r36</span>
                                    <span class="label label-default" style="display: inline-block;">m--r37</span>
                                    <span class="label label-default" style="display: inline-block;">m--r38</span>
                                    <span class="label label-default" style="display: inline-block;">m--r39</span>
                                    <span class="label label-default" style="display: inline-block;">m--r40</span>
                                </p>
                                <p>Negative right and bottom:
                                    <span class="label label-info" style="display: inline-block;">m--rb0</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb1</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb2</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb3</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb4</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb5</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb6</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb7</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb8</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb9</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb10</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb11</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb12</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb13</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb14</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb15</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb16</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb17</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb18</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb19</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb20</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb21</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb22</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb23</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb24</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb25</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb26</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb27</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb28</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb29</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb30</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb31</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb32</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb33</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb34</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb35</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb36</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb37</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb38</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb39</span>
                                    <span class="label label-info" style="display: inline-block;">m--rb40</span>
                                </p>
                                <p>Negative right and left:         <span class="label label-primary" style="display: inline-block;">m--rl0</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl1</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl2</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl3</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl4</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl5</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl6</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl7</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl8</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl9</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl10</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl11</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl12</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl13</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl14</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl15</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl16</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl17</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl18</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl19</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl20</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl21</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl22</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl23</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl24</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl25</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl26</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl27</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl28</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl29</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl30</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl31</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl32</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl33</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl34</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl35</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl36</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl37</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl38</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl39</span>
                                          <span class="label label-primary" style="display: inline-block;">m--rl40</span>
                                  </p>
                            </dd>
                            <hr class="hr-md">
                            <dt class="togglable-heading"><strong>Bottom:</strong> <code class="small">m-b*</code></dt>
                            <dd class="togglable-content">
                                <span class="label label-info" style="display: inline-block;">m-b0</span>
                                <span class="label label-info" style="display: inline-block;">m-b1</span>
                                <span class="label label-info" style="display: inline-block;">m-b2</span>
                                <span class="label label-info" style="display: inline-block;">m-b3</span>
                                <span class="label label-info" style="display: inline-block;">m-b4</span>
                                <span class="label label-info" style="display: inline-block;">m-b5</span>
                                <span class="label label-info" style="display: inline-block;">m-b6</span>
                                <span class="label label-info" style="display: inline-block;">m-b7</span>
                                <span class="label label-info" style="display: inline-block;">m-b8</span>
                                <span class="label label-info" style="display: inline-block;">m-b9</span>
                                <span class="label label-info" style="display: inline-block;">m-b10</span>
                                <span class="label label-info" style="display: inline-block;">m-b11</span>
                                <span class="label label-info" style="display: inline-block;">m-b12</span>
                                <span class="label label-info" style="display: inline-block;">m-b13</span>
                                <span class="label label-info" style="display: inline-block;">m-b14</span>
                                <span class="label label-info" style="display: inline-block;">m-b15</span>
                                <span class="label label-info" style="display: inline-block;">m-b16</span>
                                <span class="label label-info" style="display: inline-block;">m-b17</span>
                                <span class="label label-info" style="display: inline-block;">m-b18</span>
                                <span class="label label-info" style="display: inline-block;">m-b19</span>
                                <span class="label label-info" style="display: inline-block;">m-b20</span>
                                <span class="label label-info" style="display: inline-block;">m-b21</span>
                                <span class="label label-info" style="display: inline-block;">m-b22</span>
                                <span class="label label-info" style="display: inline-block;">m-b23</span>
                                <span class="label label-info" style="display: inline-block;">m-b24</span>
                                <span class="label label-info" style="display: inline-block;">m-b25</span>
                                <span class="label label-info" style="display: inline-block;">m-b26</span>
                                <span class="label label-info" style="display: inline-block;">m-b27</span>
                                <span class="label label-info" style="display: inline-block;">m-b28</span>
                                <span class="label label-info" style="display: inline-block;">m-b29</span>
                                <span class="label label-info" style="display: inline-block;">m-b30</span>
                                <span class="label label-info" style="display: inline-block;">m-b31</span>
                                <span class="label label-info" style="display: inline-block;">m-b32</span>
                                <span class="label label-info" style="display: inline-block;">m-b33</span>
                                <span class="label label-info" style="display: inline-block;">m-b34</span>
                                <span class="label label-info" style="display: inline-block;">m-b35</span>
                                <span class="label label-info" style="display: inline-block;">m-b36</span>
                                <span class="label label-info" style="display: inline-block;">m-b37</span>
                                <span class="label label-info" style="display: inline-block;">m-b38</span>
                                <span class="label label-info" style="display: inline-block;">m-b39</span>
                                <span class="label label-info" style="display: inline-block;">m-b40</span>
                            </dd>
                            <dt class="togglable-heading"><strong>Negative bottom:</strong> <code class="small">m--b*</code></dt>
                            <dd class="togglable-content">
                                <span class="label label-primary" style="display: inline-block;">m--b0</span>
                                <span class="label label-primary" style="display: inline-block;">m--b1</span>
                                <span class="label label-primary" style="display: inline-block;">m--b2</span>
                                <span class="label label-primary" style="display: inline-block;">m--b3</span>
                                <span class="label label-primary" style="display: inline-block;">m--b4</span>
                                <span class="label label-primary" style="display: inline-block;">m--b5</span>
                                <span class="label label-primary" style="display: inline-block;">m--b6</span>
                                <span class="label label-primary" style="display: inline-block;">m--b7</span>
                                <span class="label label-primary" style="display: inline-block;">m--b8</span>
                                <span class="label label-primary" style="display: inline-block;">m--b9</span>
                                <span class="label label-primary" style="display: inline-block;">m--b10</span>
                                <span class="label label-primary" style="display: inline-block;">m--b11</span>
                                <span class="label label-primary" style="display: inline-block;">m--b12</span>
                                <span class="label label-primary" style="display: inline-block;">m--b13</span>
                                <span class="label label-primary" style="display: inline-block;">m--b14</span>
                                <span class="label label-primary" style="display: inline-block;">m--b15</span>
                                <span class="label label-primary" style="display: inline-block;">m--b16</span>
                                <span class="label label-primary" style="display: inline-block;">m--b17</span>
                                <span class="label label-primary" style="display: inline-block;">m--b18</span>
                                <span class="label label-primary" style="display: inline-block;">m--b19</span>
                                <span class="label label-primary" style="display: inline-block;">m--b20</span>
                                <span class="label label-primary" style="display: inline-block;">m--b21</span>
                                <span class="label label-primary" style="display: inline-block;">m--b22</span>
                                <span class="label label-primary" style="display: inline-block;">m--b23</span>
                                <span class="label label-primary" style="display: inline-block;">m--b24</span>
                                <span class="label label-primary" style="display: inline-block;">m--b25</span>
                                <span class="label label-primary" style="display: inline-block;">m--b26</span>
                                <span class="label label-primary" style="display: inline-block;">m--b27</span>
                                <span class="label label-primary" style="display: inline-block;">m--b28</span>
                                <span class="label label-primary" style="display: inline-block;">m--b29</span>
                                <span class="label label-primary" style="display: inline-block;">m--b30</span>
                                <span class="label label-primary" style="display: inline-block;">m--b31</span>
                                <span class="label label-primary" style="display: inline-block;">m--b32</span>
                                <span class="label label-primary" style="display: inline-block;">m--b33</span>
                                <span class="label label-primary" style="display: inline-block;">m--b34</span>
                                <span class="label label-primary" style="display: inline-block;">m--b35</span>
                                <span class="label label-primary" style="display: inline-block;">m--b36</span>
                                <span class="label label-primary" style="display: inline-block;">m--b37</span>
                                <span class="label label-primary" style="display: inline-block;">m--b38</span>
                                <span class="label label-primary" style="display: inline-block;">m--b39</span>
                                <span class="label label-primary" style="display: inline-block;">m--b40</span>
                            </dd>
                            <hr class="hr-md">
                            <dt class="togglable-heading"><strong>Left:</strong> <code class="small">m-l*</code></dt>
                            <dd class="togglable-content">
                                <span class="label label-warning" style="display: inline-block;">m-l0</span>
                                <span class="label label-warning" style="display: inline-block;">m-l1</span>
                                <span class="label label-warning" style="display: inline-block;">m-l2</span>
                                <span class="label label-warning" style="display: inline-block;">m-l3</span>
                                <span class="label label-warning" style="display: inline-block;">m-l4</span>
                                <span class="label label-warning" style="display: inline-block;">m-l5</span>
                                <span class="label label-warning" style="display: inline-block;">m-l6</span>
                                <span class="label label-warning" style="display: inline-block;">m-l7</span>
                                <span class="label label-warning" style="display: inline-block;">m-l8</span>
                                <span class="label label-warning" style="display: inline-block;">m-l9</span>
                                <span class="label label-warning" style="display: inline-block;">m-l10</span>
                                <span class="label label-warning" style="display: inline-block;">m-l11</span>
                                <span class="label label-warning" style="display: inline-block;">m-l12</span>
                                <span class="label label-warning" style="display: inline-block;">m-l13</span>
                                <span class="label label-warning" style="display: inline-block;">m-l14</span>
                                <span class="label label-warning" style="display: inline-block;">m-l15</span>
                                <span class="label label-warning" style="display: inline-block;">m-l16</span>
                                <span class="label label-warning" style="display: inline-block;">m-l17</span>
                                <span class="label label-warning" style="display: inline-block;">m-l18</span>
                                <span class="label label-warning" style="display: inline-block;">m-l19</span>
                                <span class="label label-warning" style="display: inline-block;">m-l20</span>
                                <span class="label label-warning" style="display: inline-block;">m-l21</span>
                                <span class="label label-warning" style="display: inline-block;">m-l22</span>
                                <span class="label label-warning" style="display: inline-block;">m-l23</span>
                                <span class="label label-warning" style="display: inline-block;">m-l24</span>
                                <span class="label label-warning" style="display: inline-block;">m-l25</span>
                                <span class="label label-warning" style="display: inline-block;">m-l26</span>
                                <span class="label label-warning" style="display: inline-block;">m-l27</span>
                                <span class="label label-warning" style="display: inline-block;">m-l28</span>
                                <span class="label label-warning" style="display: inline-block;">m-l29</span>
                                <span class="label label-warning" style="display: inline-block;">m-l30</span>
                                <span class="label label-warning" style="display: inline-block;">m-l31</span>
                                <span class="label label-warning" style="display: inline-block;">m-l32</span>
                                <span class="label label-warning" style="display: inline-block;">m-l33</span>
                                <span class="label label-warning" style="display: inline-block;">m-l34</span>
                                <span class="label label-warning" style="display: inline-block;">m-l35</span>
                                <span class="label label-warning" style="display: inline-block;">m-l36</span>
                                <span class="label label-warning" style="display: inline-block;">m-l37</span>
                                <span class="label label-warning" style="display: inline-block;">m-l38</span>
                                <span class="label label-warning" style="display: inline-block;">m-l39</span>
                                <span class="label label-warning" style="display: inline-block;">m-l40</span>
                            </dd>
                            <dt class="togglable-heading"><strong>Negative left:</strong> <code class="small">m--l*</code></dt>
                            <dd class="togglable-content">
                                <span class="label label-info" style="display: inline-block;">m--l0</span>
                                <span class="label label-info" style="display: inline-block;">m--l1</span>
                                <span class="label label-info" style="display: inline-block;">m--l2</span>
                                <span class="label label-info" style="display: inline-block;">m--l3</span>
                                <span class="label label-info" style="display: inline-block;">m--l4</span>
                                <span class="label label-info" style="display: inline-block;">m--l5</span>
                                <span class="label label-info" style="display: inline-block;">m--l6</span>
                                <span class="label label-info" style="display: inline-block;">m--l7</span>
                                <span class="label label-info" style="display: inline-block;">m--l8</span>
                                <span class="label label-info" style="display: inline-block;">m--l9</span>
                                <span class="label label-info" style="display: inline-block;">m--l10</span>
                                <span class="label label-info" style="display: inline-block;">m--l11</span>
                                <span class="label label-info" style="display: inline-block;">m--l12</span>
                                <span class="label label-info" style="display: inline-block;">m--l13</span>
                                <span class="label label-info" style="display: inline-block;">m--l14</span>
                                <span class="label label-info" style="display: inline-block;">m--l15</span>
                                <span class="label label-info" style="display: inline-block;">m--l16</span>
                                <span class="label label-info" style="display: inline-block;">m--l17</span>
                                <span class="label label-info" style="display: inline-block;">m--l18</span>
                                <span class="label label-info" style="display: inline-block;">m--l19</span>
                                <span class="label label-info" style="display: inline-block;">m--l20</span>
                                <span class="label label-info" style="display: inline-block;">m--l21</span>
                                <span class="label label-info" style="display: inline-block;">m--l22</span>
                                <span class="label label-info" style="display: inline-block;">m--l23</span>
                                <span class="label label-info" style="display: inline-block;">m--l24</span>
                                <span class="label label-info" style="display: inline-block;">m--l25</span>
                                <span class="label label-info" style="display: inline-block;">m--l26</span>
                                <span class="label label-info" style="display: inline-block;">m--l27</span>
                                <span class="label label-info" style="display: inline-block;">m--l28</span>
                                <span class="label label-info" style="display: inline-block;">m--l29</span>
                                <span class="label label-info" style="display: inline-block;">m--l30</span>
                                <span class="label label-info" style="display: inline-block;">m--l31</span>
                                <span class="label label-info" style="display: inline-block;">m--l32</span>
                                <span class="label label-info" style="display: inline-block;">m--l33</span>
                                <span class="label label-info" style="display: inline-block;">m--l34</span>
                                <span class="label label-info" style="display: inline-block;">m--l35</span>
                                <span class="label label-info" style="display: inline-block;">m--l36</span>
                                <span class="label label-info" style="display: inline-block;">m--l37</span>
                                <span class="label label-info" style="display: inline-block;">m--l38</span>
                                <span class="label label-info" style="display: inline-block;">m--l39</span>
                                <span class="label label-info" style="display: inline-block;">m--l40</span>
                             </dd>
                        </dl>
                    </div>
                </div>
            </div>

            <h3>Padding classes</h3>
            <div class="row">
                <div class="col-xs-12">
                    <div class="well well-default-o">
                        <dl class="togglable togglable-list">
                            <dt class="togglable-heading"><strong>All sides:</strong> <code class="small">p-*</code></dt>
                            <dd class="togglable-content">
                                      <span class="label label-default" style="display: inline-block;">p-0</span>
                                      <span class="label label-default" style="display: inline-block;">p-1</span>
                                      <span class="label label-default" style="display: inline-block;">p-2</span>
                                      <span class="label label-default" style="display: inline-block;">p-3</span>
                                      <span class="label label-default" style="display: inline-block;">p-4</span>
                                      <span class="label label-default" style="display: inline-block;">p-5</span>
                                      <span class="label label-default" style="display: inline-block;">p-6</span>
                                      <span class="label label-default" style="display: inline-block;">p-7</span>
                                      <span class="label label-default" style="display: inline-block;">p-8</span>
                                      <span class="label label-default" style="display: inline-block;">p-9</span>
                                      <span class="label label-default" style="display: inline-block;">p-10</span>
                                      <span class="label label-default" style="display: inline-block;">p-11</span>
                                      <span class="label label-default" style="display: inline-block;">p-12</span>
                                      <span class="label label-default" style="display: inline-block;">p-13</span>
                                      <span class="label label-default" style="display: inline-block;">p-14</span>
                                      <span class="label label-default" style="display: inline-block;">p-15</span>
                                      <span class="label label-default" style="display: inline-block;">p-16</span>
                                      <span class="label label-default" style="display: inline-block;">p-17</span>
                                      <span class="label label-default" style="display: inline-block;">p-18</span>
                                      <span class="label label-default" style="display: inline-block;">p-19</span>
                                      <span class="label label-default" style="display: inline-block;">p-20</span>
                                      <span class="label label-default" style="display: inline-block;">p-21</span>
                                      <span class="label label-default" style="display: inline-block;">p-22</span>
                                      <span class="label label-default" style="display: inline-block;">p-23</span>
                                      <span class="label label-default" style="display: inline-block;">p-24</span>
                                      <span class="label label-default" style="display: inline-block;">p-25</span>
                                      <span class="label label-default" style="display: inline-block;">p-26</span>
                                      <span class="label label-default" style="display: inline-block;">p-27</span>
                                      <span class="label label-default" style="display: inline-block;">p-28</span>
                                      <span class="label label-default" style="display: inline-block;">p-29</span>
                                      <span class="label label-default" style="display: inline-block;">p-30</span>
                                      <span class="label label-default" style="display: inline-block;">p-31</span>
                                      <span class="label label-default" style="display: inline-block;">p-32</span>
                                      <span class="label label-default" style="display: inline-block;">p-33</span>
                                      <span class="label label-default" style="display: inline-block;">p-34</span>
                                      <span class="label label-default" style="display: inline-block;">p-35</span>
                                      <span class="label label-default" style="display: inline-block;">p-36</span>
                                      <span class="label label-default" style="display: inline-block;">p-37</span>
                                      <span class="label label-default" style="display: inline-block;">p-38</span>
                                      <span class="label label-default" style="display: inline-block;">p-39</span>
                                      <span class="label label-default" style="display: inline-block;">p-40</span>
                            </dd>               
                            <hr class="hr-md">
                            <dt class="togglable-heading"><strong>Top:</strong> <code class="small">p-t*</code></dt>
                            <dd class="togglable-content">
                                    <p>Top only:
                                      <span class="label label-default" style="display: inline-block;">p-t0</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t1</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t2</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t3</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t4</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t5</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t6</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t7</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t8</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t9</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t10</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t11</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t12</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t13</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t14</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t15</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t16</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t17</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t18</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t19</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t20</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t21</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t22</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t23</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t24</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t25</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t26</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t27</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t28</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t29</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t30</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t31</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t32</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t33</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t34</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t35</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t36</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t37</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t38</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t39</span> 
                                      <span class="label label-default" style="display: inline-block;">p-t40</span> 
                                    </p>
                                    <p>Top and right:
                                      <span class="label label-info" style="display: inline-block;">p-tr0</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr1</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr2</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr3</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr4</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr5</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr6</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr7</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr8</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr9</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr10</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr11</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr12</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr13</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr14</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr15</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr16</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr17</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr18</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr19</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr20</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr21</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr22</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr23</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr24</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr25</span>
                                      <span class="label label-info" style="display: inline-block;">p-tr26</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr27</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr28</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr29</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr30</span>
                                      <span class="label label-info" style="display: inline-block;">p-tr31</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr32</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr33</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr34</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr35</span>
                                      <span class="label label-info" style="display: inline-block;">p-tr36</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr37</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr38</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr39</span> 
                                      <span class="label label-info" style="display: inline-block;">p-tr40</span>
                                    </p>
                                    <p>Top and bottom:
                                      <span class="label label-primary" style="display: inline-block;">p-tb0</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb1</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb2</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb3</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb4</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb5</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb6</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb7</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb8</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb9</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb10</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb11</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb12</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb13</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb14</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb15</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb16</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb17</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb18</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb19</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb20</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb21</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb22</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb23</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb24</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb25</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb26</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb27</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb28</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb29</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb30</span>
                                      <span class="label label-primary" style="display: inline-block;">p-tb31</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb32</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb33</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb34</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb35</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb36</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb37</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb38</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb39</span> 
                                      <span class="label label-primary" style="display: inline-block;">p-tb40</span>
                                    </p>
                                    <p>Top and left:
                                      <span class="label label-success" style="display: inline-block;">p-tl0</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl1</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl2</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl3</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl4</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl5</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl6</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl7</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl8</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl9</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl10</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl11</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl12</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl13</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl14</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl15</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl16</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl17</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl18</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl19</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl20</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl21</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl22</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl23</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl24</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl25</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl26</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl27</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl28</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl29</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl30</span>
                                      <span class="label label-success" style="display: inline-block;">p-tl31</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl32</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl33</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl34</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl35</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl36</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl37</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl38</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl39</span> 
                                      <span class="label label-success" style="display: inline-block;">p-tl40</span>
                                   </p>
                            </dd>
                            <hr class="hr-md">
                            <dt class="togglable-heading"><strong>Right:</strong> <code class="small">p-r*</code></dt>
                            <dd class="togglable-content">
                                <p>Right only:
                                    <span class="label label-default" style="display: inline-block;">p-r0</span>
                                    <span class="label label-default" style="display: inline-block;">p-r1</span>
                                    <span class="label label-default" style="display: inline-block;">p-r2</span>
                                    <span class="label label-default" style="display: inline-block;">p-r3</span>
                                    <span class="label label-default" style="display: inline-block;">p-r4</span>
                                    <span class="label label-default" style="display: inline-block;">p-r5</span>
                                    <span class="label label-default" style="display: inline-block;">p-r6</span>
                                    <span class="label label-default" style="display: inline-block;">p-r7</span>
                                    <span class="label label-default" style="display: inline-block;">p-r8</span>
                                    <span class="label label-default" style="display: inline-block;">p-r9</span>
                                    <span class="label label-default" style="display: inline-block;">p-r10</span>
                                    <span class="label label-default" style="display: inline-block;">p-r11</span>
                                    <span class="label label-default" style="display: inline-block;">p-r12</span>
                                    <span class="label label-default" style="display: inline-block;">p-r13</span>
                                    <span class="label label-default" style="display: inline-block;">p-r14</span>
                                    <span class="label label-default" style="display: inline-block;">p-r15</span>
                                    <span class="label label-default" style="display: inline-block;">p-r16</span>
                                    <span class="label label-default" style="display: inline-block;">p-r17</span>
                                    <span class="label label-default" style="display: inline-block;">p-r18</span>
                                    <span class="label label-default" style="display: inline-block;">p-r19</span>
                                    <span class="label label-default" style="display: inline-block;">p-r20</span>
                                    <span class="label label-default" style="display: inline-block;">p-r21</span>
                                    <span class="label label-default" style="display: inline-block;">p-r22</span>
                                    <span class="label label-default" style="display: inline-block;">p-r23</span>
                                    <span class="label label-default" style="display: inline-block;">p-r24</span>
                                    <span class="label label-default" style="display: inline-block;">p-r25</span>
                                    <span class="label label-default" style="display: inline-block;">p-r26</span>
                                    <span class="label label-default" style="display: inline-block;">p-r27</span>
                                    <span class="label label-default" style="display: inline-block;">p-r28</span>
                                    <span class="label label-default" style="display: inline-block;">p-r29</span>
                                    <span class="label label-default" style="display: inline-block;">p-r30</span>
                                    <span class="label label-default" style="display: inline-block;">p-r31</span>
                                    <span class="label label-default" style="display: inline-block;">p-r32</span>
                                    <span class="label label-default" style="display: inline-block;">p-r33</span>
                                    <span class="label label-default" style="display: inline-block;">p-r34</span>
                                    <span class="label label-default" style="display: inline-block;">p-r35</span>
                                    <span class="label label-default" style="display: inline-block;">p-r36</span>
                                    <span class="label label-default" style="display: inline-block;">p-r37</span>
                                    <span class="label label-default" style="display: inline-block;">p-r38</span>
                                    <span class="label label-default" style="display: inline-block;">p-r39</span>
                                    <span class="label label-default" style="display: inline-block;">p-r40</span>
                                  </p>
                                <p>Right and bottom:
                                    <span class="label label-info" style="display: inline-block;">p-rb0</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb1</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb2</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb3</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb4</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb5</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb6</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb7</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb8</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb9</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb10</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb11</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb12</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb13</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb14</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb15</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb16</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb17</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb18</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb19</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb20</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb21</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb22</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb23</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb24</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb25</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb26</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb27</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb28</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb29</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb30</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb31</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb32</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb33</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb34</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb35</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb36</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb37</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb38</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb39</span>
                                    <span class="label label-info" style="display: inline-block;">p-rb40</span>
                                  </p>
                                <p>Right and left: 
                                    <span class="label label-primary" style="display: inline-block;">p-rl0</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl1</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl2</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl3</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl4</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl5</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl6</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl7</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl8</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl9</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl10</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl11</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl12</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl13</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl14</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl15</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl16</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl17</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl18</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl19</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl20</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl21</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl22</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl23</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl24</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl25</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl26</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl27</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl28</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl29</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl30</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl31</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl32</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl33</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl34</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl35</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl36</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl37</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl38</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl39</span>
                                    <span class="label label-primary" style="display: inline-block;">p-rl40</span>
                                </p>
                            </dd>
                            <hr class="hr-md">
                            <dt class="togglable-heading"><strong>Bottom:</strong> <code class="small">p-b*</code></dt>
                            <dd class="togglable-content">
                                <span class="label label-info" style="display: inline-block;">p-b0</span>
                                <span class="label label-info" style="display: inline-block;">p-b1</span>
                                <span class="label label-info" style="display: inline-block;">p-b2</span>
                                <span class="label label-info" style="display: inline-block;">p-b3</span>
                                <span class="label label-info" style="display: inline-block;">p-b4</span>
                                <span class="label label-info" style="display: inline-block;">p-b5</span>
                                <span class="label label-info" style="display: inline-block;">p-b6</span>
                                <span class="label label-info" style="display: inline-block;">p-b7</span>
                                <span class="label label-info" style="display: inline-block;">p-b8</span>
                                <span class="label label-info" style="display: inline-block;">p-b9</span>
                                <span class="label label-info" style="display: inline-block;">p-b10</span>
                                <span class="label label-info" style="display: inline-block;">p-b11</span>
                                <span class="label label-info" style="display: inline-block;">p-b12</span>
                                <span class="label label-info" style="display: inline-block;">p-b13</span>
                                <span class="label label-info" style="display: inline-block;">p-b14</span>
                                <span class="label label-info" style="display: inline-block;">p-b15</span>
                                <span class="label label-info" style="display: inline-block;">p-b16</span>
                                <span class="label label-info" style="display: inline-block;">p-b17</span>
                                <span class="label label-info" style="display: inline-block;">p-b18</span>
                                <span class="label label-info" style="display: inline-block;">p-b19</span>
                                <span class="label label-info" style="display: inline-block;">p-b20</span>
                                <span class="label label-info" style="display: inline-block;">p-b21</span>
                                <span class="label label-info" style="display: inline-block;">p-b22</span>
                                <span class="label label-info" style="display: inline-block;">p-b23</span>
                                <span class="label label-info" style="display: inline-block;">p-b24</span>
                                <span class="label label-info" style="display: inline-block;">p-b25</span>
                                <span class="label label-info" style="display: inline-block;">p-b26</span>
                                <span class="label label-info" style="display: inline-block;">p-b27</span>
                                <span class="label label-info" style="display: inline-block;">p-b28</span>
                                <span class="label label-info" style="display: inline-block;">p-b29</span>
                                <span class="label label-info" style="display: inline-block;">p-b30</span>
                                <span class="label label-info" style="display: inline-block;">p-b31</span>
                                <span class="label label-info" style="display: inline-block;">p-b32</span>
                                <span class="label label-info" style="display: inline-block;">p-b33</span>
                                <span class="label label-info" style="display: inline-block;">p-b34</span>
                                <span class="label label-info" style="display: inline-block;">p-b35</span>
                                <span class="label label-info" style="display: inline-block;">p-b36</span>
                                <span class="label label-info" style="display: inline-block;">p-b37</span>
                                <span class="label label-info" style="display: inline-block;">p-b38</span>
                                <span class="label label-info" style="display: inline-block;">p-b39</span>
                                <span class="label label-info" style="display: inline-block;">p-b40</span>
                            </dd>
                            <hr class="hr-md">
                            <dt class="togglable-heading"><strong>Left:</strong> <code class="small">p-l*</code></dt>
                            <dd class="togglable-content">
                                <span class="label label-info" style="display: inline-block;">p-l0</span>
                                <span class="label label-info" style="display: inline-block;">p-l1</span>
                                <span class="label label-info" style="display: inline-block;">p-l2</span>
                                <span class="label label-info" style="display: inline-block;">p-l3</span>
                                <span class="label label-info" style="display: inline-block;">p-l4</span>
                                <span class="label label-info" style="display: inline-block;">p-l5</span>
                                <span class="label label-info" style="display: inline-block;">p-l6</span>
                                <span class="label label-info" style="display: inline-block;">p-l7</span>
                                <span class="label label-info" style="display: inline-block;">p-l8</span>
                                <span class="label label-info" style="display: inline-block;">p-l9</span>
                                <span class="label label-info" style="display: inline-block;">p-l10</span>
                                <span class="label label-info" style="display: inline-block;">p-l11</span>
                                <span class="label label-info" style="display: inline-block;">p-l12</span>
                                <span class="label label-info" style="display: inline-block;">p-l13</span>
                                <span class="label label-info" style="display: inline-block;">p-l14</span>
                                <span class="label label-info" style="display: inline-block;">p-l15</span>
                                <span class="label label-info" style="display: inline-block;">p-l16</span>
                                <span class="label label-info" style="display: inline-block;">p-l17</span>
                                <span class="label label-info" style="display: inline-block;">p-l18</span>
                                <span class="label label-info" style="display: inline-block;">p-l19</span>
                                <span class="label label-info" style="display: inline-block;">p-l20</span>
                                <span class="label label-info" style="display: inline-block;">p-l21</span>
                                <span class="label label-info" style="display: inline-block;">p-l22</span>
                                <span class="label label-info" style="display: inline-block;">p-l23</span>
                                <span class="label label-info" style="display: inline-block;">p-l24</span>
                                <span class="label label-info" style="display: inline-block;">p-l25</span>
                                <span class="label label-info" style="display: inline-block;">p-l26</span>
                                <span class="label label-info" style="display: inline-block;">p-l27</span>
                                <span class="label label-info" style="display: inline-block;">p-l28</span>
                                <span class="label label-info" style="display: inline-block;">p-l29</span>
                                <span class="label label-info" style="display: inline-block;">p-l30</span>
                                <span class="label label-info" style="display: inline-block;">p-l31</span>
                                <span class="label label-info" style="display: inline-block;">p-l32</span>
                                <span class="label label-info" style="display: inline-block;">p-l33</span>
                                <span class="label label-info" style="display: inline-block;">p-l34</span>
                                <span class="label label-info" style="display: inline-block;">p-l35</span>
                                <span class="label label-info" style="display: inline-block;">p-l36</span>
                                <span class="label label-info" style="display: inline-block;">p-l37</span>
                                <span class="label label-info" style="display: inline-block;">p-l38</span>
                                <span class="label label-info" style="display: inline-block;">p-l39</span>
                                <span class="label label-info" style="display: inline-block;">p-l40</span>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div> 
        </div> 
    </div>
</div>
<script>
    $('.nav-sidebar li').on('click', function(){
        $('.nav-sidebar li').removeClass('active');
        $(this).addClass('active');
    });
</script>
<script src="js/prettify.js"></script>
<?php require_once 'inc/modules/footer.php' ?>