<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Orders</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>				
			<li class="bread_item active"><a class="bread_link" href="">Orders</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t10 b-Bottom clearfix">
					<p class="head_text pull-sm-left b-None clearfix m-0">
						<img src="flags/France.png" class="flag m-r10" alt="">
						<span class="p-t10">France</span>					
					</p>
					<div class="pull-sm-right">
						<button class="btn btn-save m-b10" data-toggle="modal" data-target="#importOrder">Import orders</button>  
						<button class="btn btn-save m-b10">Send orders</button>  
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 m-t20 m-b10">
				<div class="validate yellow">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class=""><span class="bold">Missing API Keys:</span> Missing API connection keys. API connection doesn't exits. To setting API key please go to Configurations > Parameters Tab.</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Orders</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											ID Order Ref.
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Buyer
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Payment
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Amount
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											Status
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="error" checked/>
											Order Date
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="date" checked/>
											Shipping Date
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="download" checked/>
											Tracking No.
										</label>
									</li>
									<li>
									<label class="cb-checkbox checked">
											<input type="checkbox" name="exported" checked/>
											Exported
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">ID</th>
									<th class="tableUserName">ID Order Ref.</th>
									<th class="tableEmail">Buyer</th>
									<th class="tableStatus">Payment</th>
									<th class="tableJoined">Amount</th>
									<th class="tableSummary">Status</th>
									<th class="tableError">Order Date</th>
									<th class="tableDate">Shipping Date</th>
									<th class="tableDownload">Tracking No.</th>
									<th class="tableExported">Exported</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order Ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Payment:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Amount:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Status:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">sfbnsfnbsfn</td>
									<td class="tableDate" data-th="Shipping Date:">sdnsnsfnx</td>
									<td class="tableDownload" data-th="Tracking No.">Lorem Ipsum</td>
									<td class="tableExported" data-th="Exported">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order Ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Payment:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Amount:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Status:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">sfbnsfnbsfn</td>
									<td class="tableDate" data-th="Shipping Date:">sdnsnsfnx</td>
									<td class="tableDownload" data-th="Tracking No.">Lorem Ipsum</td>
									<td class="tableExported" data-th="Exported">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order Ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Payment:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Amount:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Status:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">sfbnsfnbsfn</td>
									<td class="tableDate" data-th="Shipping Date:">sdnsnsfnx</td>
									<td class="tableDownload" data-th="Tracking No.">Lorem Ipsum</td>
									<td class="tableExported" data-th="Exported">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order Ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Payment:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Amount:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Status:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">sfbnsfnbsfn</td>
									<td class="tableDate" data-th="Shipping Date:">sdnsnsfnx</td>
									<td class="tableDownload" data-th="Tracking No.">Lorem Ipsum</td>
									<td class="tableExported" data-th="Exported">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order Ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Payment:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Amount:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Status:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">sfbnsfnbsfn</td>
									<td class="tableDate" data-th="Shipping Date:">sdnsnsfnx</td>
									<td class="tableDownload" data-th="Tracking No.">Lorem Ipsum</td>
									<td class="tableExported" data-th="Exported">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order Ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Payment:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Amount:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Status:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">sfbnsfnbsfn</td>
									<td class="tableDate" data-th="Shipping Date:">sdnsnsfnx</td>
									<td class="tableDownload" data-th="Tracking No.">Lorem Ipsum</td>
									<td class="tableExported" data-th="Exported">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order Ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Payment:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Amount:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Status:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">sfbnsfnbsfn</td>
									<td class="tableDate" data-th="Shipping Date:">sdnsnsfnx</td>
									<td class="tableDownload" data-th="Tracking No.">Lorem Ipsum</td>
									<td class="tableExported" data-th="Exported">Lorem Ipsum</td>
								</tr>																
							</tbody>
							<tfoot class="table-foot">
                                <tr>
                                	<th>
                                		<input class="form-control" type="text" placeholder="ID">
                                	</th>
                                	<th >
                                		<input class="form-control" type="text" placeholder="ID Order ref.">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Buyer">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Payment">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Amount">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Status">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Order Date">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Shipping Date">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Tracking No.">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Exported">
                                	</th>
                                </tr>
                            </tfoot>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>
	<div class="modal fade" id="importOrder">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">parameters</h1>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-3">
							<p class="poor-gray">Order date Range</p>
							<div class="form-group calendar firstDate">
								<input type="text" id="dp-firstDate" class="form-control inputDate">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group calendar lastDate m-t25">
								<input type="text" id="dp-lastDate" class="form-control inputDate">
							</div>
						</div>
						<div class="col-sm-6">
							<p class="poor-gray">Order status</p>
							<div class="form-group ">
								<select class="search-select" data-placeholder="Choose">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack & Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce & Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<ul class="selectRange">
								<li class="active today">Today</li>
								<li class="yesterday">Yesterday</li>
								<li class="last_7Days">last 7 days</li>
								<li class="last_30Days">last 30 days</li>
								<li class="thisMonth">this month</li>
								<li class="lastMonth">last month</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-save" data-dismiss="modal">Start importing orders</button>
				</div>
			</div>
		</div>
	</div>