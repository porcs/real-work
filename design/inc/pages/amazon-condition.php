<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Conditions</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>				
			<li class="bread_item active"><a class="bread_link" href="">Conditions</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix m-0">
					<img src="flags/France.png" class="flag m-r10" alt="">
					<span class="p-t10">France</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t20 p-b20">
					Condition Mappings
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class="pull-left">Amazon conditions side / Feed.biz conditions side, please associate the parameters wished</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t20 p-b20">
					Amazon - Conditions
				</p>
			</div>
		</div>
		<div class="row conditions">
			<div class="col-xs-12">
				<div class="b-Bottom p-t5 m-b10">
					<div class="row">
						<div class="col-sm-6">
							<p class="poor-gray p-t10">New</p>
							<i class="blockRight noBorder"></i>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row conditions">
			<div class="col-xs-12">
				<div class="b-Bottom p-t5 m-b10">
					<div class="row">
						<div class="col-sm-6">
							<p class="poor-gray p-t10">Used Like New</p>
							<i class="blockRight noBorder"></i>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row conditions">
			<div class="col-xs-12">
				<div class="b-Bottom p-t5 m-b10">
					<div class="row">
						<div class="col-sm-6">
							<p class="poor-gray p-t10">Used Very Good</p>
							<i class="blockRight noBorder"></i>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row conditions">
			<div class="col-xs-12">
				<div class="b-Bottom p-t5 m-b10">
					<div class="row">
						<div class="col-sm-6">
							<p class="poor-gray p-t10">Used Good</p>
							<i class="blockRight noBorder"></i>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Save
						</a>
						<button class="btn btn-save btnNext" type="button" >Save & Continue</button>             
					</div>
				</div>		
			</div>
		</div>
	</div>