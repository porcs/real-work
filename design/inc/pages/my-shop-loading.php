<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Data source</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My shop</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Connect</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-b10">
					<h4 class="headSettings_head">Module/extension</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="engine clearfix">
					<div class="engine_item">
						<img class="engine_item-picture" src="images/engine/prestashop.png" alt="">
						<p class="engine_item-text">Prestashop</p>
						<button class="btn btn-save">Download</button>
					</div>
					<div class="engine_item">
						<img class="engine_item-picture" src="images/engine/shopify.png" alt="">
						<p class="engine_item-text">Shopify</p>
						<button class="btn btn-save">Download</button>
					</div>
					<div class="engine_item">
						<img class="engine_item-picture" src="images/engine/opencart.png" alt="">
						<p class="engine_item-text">Opencart</p>
						<button class="btn btn-save">Download</button>
					</div>
					<div class="engine_item">
						<img class="engine_item-picture" src="images/engine/magento.png" alt="">
						<p class="engine_item-text">magento</p>
						<button class="btn btn-save">Download</button>
					</div>
					<div class="engine_item">
						<img class="engine_item-picture" src="images/engine/woo.png" alt="">
						<p class="engine_item-text">Woo commerce</p>
						<button class="btn btn-save">Download</button>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 m-t20 b-Bottom m-b10">
					<h4 class="headSettings_head">Feed Data</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="regRoboto poor-gray">URL provided by your Feed.biz module</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
				<button class="btn btn-verify"><i class="fa fa-spinner fa-spin"></i> Please wait while verifying...</button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 m-t20 b-Bottom m-b10">
					<h4 class="headSettings_head">Import Data</h4>						
				</div>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-sm-4">
				<label class="cb-radio m-t10">
					<input type="radio" name="id"/>
					<p class="text-uc dark-gray montserrat m-b0 bold">Auto importing
						<span class="recommended">Recommended</span>
					</p>  
					<p class="light-gray m-l25">Our system will often update your products.</p>
				</label>
			</div>
			<div class="col-sm-5">
				<label class="cb-radio m-t10">
					<input type="radio" name="id"/>
					<p class="text-uc dark-gray montserrat m-b0 bold">Manual import</p>  
					<p class="light-gray m-l25">Your products will be updated after clicking save button.</p>
				</label>
			</div>
			<div class="col-sm-3">
				<div class="pull-right">
					<button class="m-t10 btn btn-save">Save</button>
				</div>
			</div>
		</div>
	</div>