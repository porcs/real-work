<div class="p-rl40 p-xs-rl20">
	<div class="notFound">
		<div class="headSettings clearfix b-Bottom p-b10">
			<h4 class="headSettings_head"><span>Oops...</span> the page you are looking for cannot be found</h4>	
		</div>	
		<div class="notFound_reason">
			<ul class="main_reason">
				<li>Errors can be caused due to various reasons, such as:</li>
			</ul>
			<div class="notFound_reason-point">
				<ul class="pull-sm-left m-r30">
					<li>Page requested does not exist.</li>
					<li>Server is down.</li>
					<li>Internet connection is down.</li>
				</ul>
				<ul>
					<li>Broken links</li>
					<li>Incorrect URL</li>
					<li>Page has been moved to a different address</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="b-Top m-t20 p-t20">
				<a href="" class="link p-size"><i class="fa fa-long-arrow-left m-r10"></i>Back</a>
			</div>
		</div>
	</div>
</div>