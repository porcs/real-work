<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Stock</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Stock</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Report</a></li>
		</ul>
		<div class="csv">
			<button class="btn btn-success">Download CSV</button>
		</div>		
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Stock Movement</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Date
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Detail
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Quantity
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Movement Quantity
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Balance Quantity
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">Date</th>
									<th class="tableUserName">Detail</th>
									<th class="tableEmail">Quantity</th>
									<th class="tableStatus">Movement Quantity</th>
									<th class="tableJoined">Balance Quantity</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Date:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Detail:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Quantity:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Movement Quantity:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Balance Quantity:">Lorem Ipsum</td>
								</tr>		
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>