<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Batch Log</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>
			<li class="bread_item active"><a class="bread_link" href="">LOG</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Batch Log</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Batch Log
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Source
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Type
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Total
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Success
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											Warning
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="error" checked/>
											Error
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="date" checked/>
											Export Date
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">Batch Log</th>
									<th class="tableUserName">Source</th>
									<th class="tableEmail">Type</th>
									<th class="tableStatus">Total</th>
									<th class="tableJoined">Success</th>
									<th class="tableSummary">Warning</th>
									<th class="tableError">Error</th>
									<th class="tableDate">Export Date</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Total:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Success:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Export Date:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Total:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Success:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Export Date:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Total:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Success:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Export Date:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Total:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Success:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Export Date:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Total:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Success:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Export Date:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Total:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Success:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Export Date:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Total:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Success:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Export Date:">Lorem Ipsum</td>
								</tr>														
							</tbody>
							<tfoot class="table-foot">
                                <tr>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Batch Log">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Source">
                                	</th>
                                	
                                	<th>
                                		<input class="form-control" type="text" placeholder="Type">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Total">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Success">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Warning">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Error">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Export Date">
                                	</th>
                                </tr>
                            </tfoot>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Previous
						</a>
						<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
					</div>
				</div>		
			</div>
		</div>	
	</div>