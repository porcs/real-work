<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Carriers</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Carriers</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix">
					<img src="flags/France.png" class="flag m-r10" alt="">
					<span class="p-t10">France</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 p-b20">
					Carriers Mapping
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class="pull-left">Associated carrier, usefull to display your preferate carrier on the order page and on the invoice</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="poor-gray m-t10">
					<i class="fa fa-arrow-left true-pink m-r10 "></i>
					For Outgoing Orders
				</p>				
			</div>
		</div>
		<div class="row showSelectBlock">
			<div class="col-sm-6">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group withIcon clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
					<i class="cb-plus bad"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group withIcon clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
					<i class="cb-plus good"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t20">
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray">
								<i class="fa fa-arrow-right true-green m-r10"></i>
								For incoming orders
							</p>				
						</div>
					</div>
					<div class="row showSelectBlock">
						<div class="col-sm-6">
							<select class="search-select" data-placeholder="Choose Amazon carrier side">
								<option value="">Home</option>
								<option value="">Apple</option>
								<option value="">Iphone 6</option>
								<option value="">Iphone 5</option>
								<option value="">Iphone 4</option>
								<option value="">Iphone 3</option>
								<option value="">Men shirt</option>
								<option value="">Jack &amp; Jones</option>
								<option value="">Adidas</option>
								<option value="">Armani</option>
								<option value="">Dolce &amp; Gabbana</option>
								<option value="">Mersedes</option>
								<option value="">Audi</option>
							</select>
							<i class="blockRight noBorder"></i>
						</div>
						<div class="col-sm-6">
							<div class="form-group withIcon clearfix">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<i class="cb-plus bad"></i>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 m-b20">
							<select class="search-select" data-placeholder="Choose Feed.biz carrier side">
								<option value="">Home</option>
								<option value="">Apple</option>
								<option value="">Iphone 6</option>
								<option value="">Iphone 5</option>
								<option value="">Iphone 4</option>
								<option value="">Iphone 3</option>
								<option value="">Men shirt</option>
								<option value="">Jack &amp; Jones</option>
								<option value="">Adidas</option>
								<option value="">Armani</option>
								<option value="">Dolce &amp; Gabbana</option>
								<option value="">Mersedes</option>
								<option value="">Audi</option>
							</select>
							<i class="blockRight noBorder"></i>
						</div>
						<div class="col-sm-6">
							<div class="form-group withIcon clearfix">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<i class="cb-plus good"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Save
						</a>
						<button type="button" class="btn btn-save btnNext">Save & Continue</button>                   
					</div>
				</div>		
			</div>
		</div>
	</div>