<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Report</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Report</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix m-0">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings b-Top-None clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Feed Report</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Batch ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Action Type
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Feed Type
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Submission ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Processed
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											Successful
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="error" checked/>
											Error
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="date" checked/>
											Warning
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="download" checked/>
											Send Date
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">Batch ID</th>
									<th class="tableUserName">Action Type</th>
									<th class="tableEmail">Feed Type</th>
									<th class="tableStatus">Submission ID</th>
									<th class="tableJoined">Processed</th>
									<th class="tableSummary">Successful</th>
									<th class="tableError">Error</th>
									<th class="tableDate">Warning</th>
									<th class="tableDownload">Send Date</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Type:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Feed Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Submission ID:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Processed:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Successful:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Send Date:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Type:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Feed Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Submission ID:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Processed:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Successful:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Send Date:">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Type:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Feed Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Submission ID:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Processed:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Successful:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Send Date:">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Type:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Feed Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Submission ID:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Processed:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Successful:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Send Date:">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Type:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Feed Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Submission ID:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Processed:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Successful:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Send Date:">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Type:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Feed Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Submission ID:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Processed:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Successful:">Lorem Ipsum</td>
									<td class="tableError" data-th="Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Warning:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Send Date:">Lorem Ipsum</td>
								</tr>							
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>