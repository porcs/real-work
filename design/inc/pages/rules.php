<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Rules</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>
			<li class="bread_item"><a class="bread_link" href="">Parameters</a></li>
			<li class="bread_item"><a class="bread_link" href="">Offers</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Rules</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top b-Bottom p-t10 p-b10">
					<a href="" class="link showProfile p-size">+ Add a rule to the list</a>
					<p class="pull-right montserrat poor-gray"><span class="dark-gray countProfile">0</span> Rules</p>
				</div>
			</div>
		</div>
		<div class="row showSelectBlock countBlock custom-form">
			<div class="col-md-10 col-lg-9 m-t10">
				<a href="" class="removeProfile">Remove a rule from list</a>
				<div class="profileBlock clearfix">
					<p class="regRoboto poor-gray pull-left">Rule Name</p>
					<p class="pull-right montserrat poor-gray"><span class="dark-gray countRule">0</span> Rules</p>
					<div class="form-group">
						<input type="text" class="form-control">						
					</div>
					<div class="ruleSettings"></div>
					<div class="setBlockMain clearfix hidden">
						<div class="m-b10 clearfix">
							<a href="" class="pull-right removeSet">Remove set <i class="cb-remove"></i></a>
						</div>
						<div class="setBlock_content">
							<div class="setBlock_text">	
								<div class="row addSettings">
									<div class="col-sm-6 addCheckBo">
										<div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
											  <input type="checkbox">
											  Manufacturer
											</label>
										</div>
										<div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
												<input type="checkbox">
												  supplier
											</label>
										</div>
										<div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
												<input type="checkbox">
												  action
											</label>
										</div>
										<div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
											  <input type="checkbox">
											  Price Range
											</label>
										</div>
									</div>
									<div class="col-sm-6 addSelect">
										<div class="choZn">
											<select class="search-select" data-placeholder="Choose">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack & Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce & Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
										</div>
										<div class="choZn">
											<select class="search-select" data-placeholder="Choose">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack & Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce & Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
										</div>
										<div class="choZn">
											<select class="search-select" data-placeholder="Choose">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack & Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce & Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
										</div>
										<div class="choZn">
											<div class="row">
												<div class="col-xs-5">
													<div class="form-group">
														<input type="text" class="form-control"/>
													</div>
												</div>
												<div class="col-xs-1">
													<p class="m-t10">to</p>
												</div>
												<div class="col-xs-4">
													<div class="form-group">
														<input type="text" class="form-control"/>
													</div>
												</div>
												<div class="col-xs-1">
													<i class="cb-plus good"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="pull-right m-b10">
						<a href="" class="showSet">+ Add set <i class="cb-add"></i></a>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-3 m-t65">
				<div class="headSettings clearfix p-b10 b-Bottom ">
					<h4 class="headSettings_head">Summary</h4>						
				</div>
				<div class="setSettings">
					<div class="clearfix p-t10">
						<p class="pull-sm-left poor-gray">Rule Name</p>						
						<p class="pull-sm-right p-text">New rule</p>
					</div>					
					<div class="row">
						<div class="col-xs-12">
							<p class="head_text b-Top p-t10 m-0">					
								Item1
							</p>	
						</div>
						<div class="col-xs-12">
							<div class="summaryBlock">
								<div class="row">
									<div class="col-sm-6">
										<p class="text-left poor-gray">Manufactory</p>
									</div>
									<div class="col-sm-6">
										<p class="text-right p-text">Fashion Manufacturer</p>
									</div>
								</div>
							</div>
							<div class="summaryBlock">
								<div class="row">
									<div class="col-sm-6">
										<p class="text-left poor-gray">Supplier</p>
									</div>
									<div class="col-sm-6">
										<p class="text-right p-text">Fashion Manufacturer</p>
									</div>
								</div>
							</div>
							<div class="summaryBlock">
								<div class="row">
									<div class="col-sm-6">
										<p class="text-left poor-gray">Action</p>
									</div>
									<div class="col-sm-6">
										<p class="text-right p-text">Drop</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12">
							<p class="head_text b-Top p-t10 m-0">					
								Item2
							</p>
						</div>
						<div class="col-xs-12">
							<div class="summaryBlock">
								<div class="row">
									<div class="col-sm-6">
										<p class="text-left poor-gray">Supplier</p>
									</div>
									<div class="col-sm-6">
										<p class="text-right p-text">Fashion supplier</p>
									</div>
								</div>
							</div>
							<div class="summaryBlock">
								<div class="row">
									<div class="col-sm-6">
										<p class="text-left poor-gray">Price</p>
									</div>
									<div class="col-sm-6">
										<p class="text-right p-text">123 to 124</p>
									</div>
									<div class="col-sm-12">
										<p class="text-right p-text">125 to 126</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row btnShow">
			<div class="col-md-10 col-lg-9">
				<div class="pull-right m-t20">
					<a href="" class="pull-left link p-size m-tr10">
						Reset
					</a>
					<button type="button" class="btn btn-save btnNext">Save</button>                   
				</div>			
			</div>
		</div>
	</div>