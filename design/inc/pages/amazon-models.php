<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Models</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Models</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix m-0">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Bottom p-t10 p-b10">
					<a href="" class="link showProfile p-size">+ Add a model</a>
					<p class="pull-right montserrat poor-gray"><span class="dark-gray countProfile">0</span> Item(s)</p>
				</div>
			</div>
		</div>
		
		<div class="row showSelectBlock">
			<div class="col-md-10 col-lg-9 custom-form m-t10">
				<a href="" class="removeProfile">Remove a profile from list</a>
				<div class="profileBlock clearfix">
					<div class="validate blue">
						<div class="validateRow">
							<div class="validateCell">
								<i class="note"></i>
							</div>
							<div class="validateCell">
								<p class="pull-left">Please give a friendly name to remind this model. Do not forget to click on the save button at the bottom of the page!</p>
								<i class="fa fa-remove pull-right"></i>
							</div>
						</div>
					</div>
					<p class="poor-gray">Model name</p>
					<div class="form-group">
						<input type="text" class="form-control">
					</div>
					<p class="poor-gray">Product universe</p>
					<div class="form-group m-b10">
						<select class="search-select" data-placeholder="Choose">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack & Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce & Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
					<p class="poor-gray text-right">Choose the main universe for this model.</p>
					<p class="poor-gray">Product type</p>
					<div class="form-group m-b10">
						<select class="search-select" data-placeholder="Choose">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack & Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce & Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
					<p class="poor-gray text-right">Please select the main category for this model.</p>
					<p class="head_text p-t20 p-b20 b-Top clearfix m-0">
						Specific
					</p>
					<div class="row hidden">
						<div class="col-xs-12">
							<div class="form-group withIcon m-b10">
								<select class="search-select" data-placeholder="Choose">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack & Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce & Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<i class="cb-plus bad"></i>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group withIcon m-b10 m-t20">
								<select class="search-select" data-placeholder="Choose">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack & Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce & Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<i class="cb-plus good"></i>
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>

		<div class="row btnShow m-t20">
			<div class="col-md-10 col-lg-9">
					<div class="pull-right">
						<a href="" class="pull-left link p-size m-tr10">
							Save
						</a>
						<button class="btn btn-save btnNext" type="button" >Save & Continue</button>             
					</div>
				</div>		
			</div>
		</div>
	</div>	