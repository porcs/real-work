<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">log</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>	
			<li class="bread_item active"><a class="bread_link" href="">Batches</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Last Import Log</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Batch ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Source
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Shop
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Type
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											No. Total
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											No. Success
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="error" checked/>
											No. Error
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="date" checked/>
											Import Date
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="download" checked/>
											Download
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">Batch ID</th>
									<th class="tableUserName">Source</th>
									<th class="tableEmail">Shop</th>
									<th class="tableStatus">Type</th>
									<th class="tableJoined">No. Total</th>
									<th class="tableSummary">No. Success</th>
									<th class="tableError">No. Error</th>
									<th class="tableDate">Import Date</th>
									<th class="tableDownload">Download</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="No. Total:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="No. Success:">Lorem Ipsum</td>
									<td class="tableError" data-th="No. Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Import Date:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Download:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="No. Total:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="No. Success:">Lorem Ipsum</td>
									<td class="tableError" data-th="No. Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Import Date:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Download:">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="No. Total:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="No. Success:">Lorem Ipsum</td>
									<td class="tableError" data-th="No. Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Import Date:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Download:">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="No. Total:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="No. Success:">Lorem Ipsum</td>
									<td class="tableError" data-th="No. Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Import Date:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Download:">Lorem Ipsum</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Source:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="No. Total:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="No. Success:">Lorem Ipsum</td>
									<td class="tableError" data-th="No. Error:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Import Date:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Download:">Lorem Ipsum</td>
								</tr>							
								
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>