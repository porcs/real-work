	<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Dashboard</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item active"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Dashboard</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="p-rl40 p-xs-rl20">
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Activities</h4>
					<ul class="list-circle-blue">
						<li>Import Product from "FOXCHIP"</li>
						<li>Export Product to ebay.fr</li>
						<li>Import Product from "FOXCHIP"</li>
						<li>Import Product from "FOXCHIP"</li>					
					</ul>
					<a href="" class="more m-b30">+3 more</a>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Best Categories</h4>
					<ul class="list-circle-unstyled m-b30">
						<li>
							<div id="miniFirst"></div>
							<p>Goodies (FOXCHIP)</p>
							<p><span>5,538</span></p>
						</li>
						<li>
							<div id="miniSecond"></div>
							<p>Jeux-Video (FOXCHIP)</p>
							<p><span>966</span></p>
						</li>
						<li>
							<div id="miniThird"></div>
							<p>J Telephone/Tablette (FOX)</p>
							<p><span>464</span></p>
						</li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Configuration Checklist</h4>
					<ul class="list-circle-green m-b30">
						<li>Data source completed.</li>
						<li>Market place configuration complete.</li>
						<li>Category complete.</li>				
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Errors - Last Entries</h4>
					<ul class="list-circle-red m-b30">
						<li>Out of stock</li>
						<li>Product are Inactive</li>		
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="mainChart p-xs-rl20">
					<div class="row">
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total products</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="product"></div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total orders</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="order"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<div class="p-rl40 p-xs-rl20">
					<div class="chart lineChart">
						<h4 class="text-uc dark-gray bold m-t20">Total sale</h4>
						<ul class="tabs">
							<li class="active">30 days</li>
							<li>7 Days</li>
							<li>1 Day</li>
						</ul>
						<div id="sale"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row p-rl40 p-xs-rl20">
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/amazon.png" alt="">
					<a class="pay_link" href="" >
						Configure Amazon
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/ebay.png" alt="">
					<a class="pay_link" href="" data-toggle="modal" data-target="#ebayConfigurate">
						Configure Ebay
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/logo_count.png" alt="">
					<ul class="pay_list">
						<li class="pay_item">Days</li>
						<li class="pay_item">Hours</li>
						<li class="pay_item">Minutes</li>
					</ul>
					<div id="countdown"></div>
					<div id="note"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="support dSTable">
					<div class="dRow">
						<img src="images/icons/enot.jpg" alt="" class="dCell icon-circle icon-user support_picture">							
						<div class="dCell">
							<p class="tooLight-gray">Your support</p>
							<p class="dark-gray h1 m-t0">Zoye</p>
							<a href="" class="link regRoboto h4">praew@common-services.com</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	

	<div class="modal fade" id="ebayConfigurate">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="lightRoboto text-uc head-size light-gray">Ebay wizard</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix m-b10">
								<ul class="headSettings_point clearfix">
									<li class="headSettings_point-item active">
										<span class="step">1</span>
										<span class="title">Authorization</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">2</span>
										<span class="title">Choose Site</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">3</span>
										<span class="title">Shippings</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">4</span>
										<span class="title">Tax</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">5</span>
										<span class="title">Select Univers</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">6</span>
										<span class="title">Categories</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">7</span>
										<span class="title">Export Products</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">8</span>
										<span class="title">Finish</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="head_text p-t10 b-Top m-b0 clearfix">
								<img src="flags/Belarus.png" class="flag m-r10" alt="">
								<span class="p-t10">Belarus</span>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Top-None b-Bottom p-0">
								<h4 class="headSettings_head">Selected Main Categories</h4>																		
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-xs-12">
							<div class="validate blue m-t20">
								<div class="validateRow">
									<div class="validateCell">
										<i class="note"></i>
									</div>
									<div class="validateCell">
										<div class="pull-left">
											<p>
												Congratulation your product have been sent to eBay successfully. 
												You now can publish your offer in Action Tab.
											</p>
										</div>
										<i class="fa fa-remove pull-right"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row m-t20">
						<div class="col-sm-2 b-Right">
							<p class="montserrat dark-gray text-uc">processed : </p>
							<p class="poor-gray"><span class="true-blue">19</span> Product(s)</p>
						</div>
						<div class="col-sm-2 b-Right">
							<p class="montserrat dark-gray text-uc">Success : </p>
							<p class="poor-gray"><span class="true-green">1</span> Product(s)</p>
						</div>
						<div class="col-sm-2 b-Right">
							<p class="montserrat dark-gray text-uc">warning : </p>
							<p class="poor-gray"><span class="warning-text">0</span> Product(s)</p>
						</div>
						<div class="col-sm-2 b-Right">
							<p class="montserrat dark-gray text-uc">error : </p>
							<p class="poor-gray"><span class="true-pink">18</span> Product(s)</p>
						</div>
						<div class="col-sm-2 p-t20">
							<a href="" class="link p-size">See more detail</a>							
						</div>
					</div>
										
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top m-t10 p-t20">
								<div class="pull-right">
									<a href="" class="pull-left link p-size m-tr10">
										Previuos
									</a>
									<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
								</div>
							</div>		
						</div>
					</div>																			
				</div>
			</div>
		</div>
	</div>