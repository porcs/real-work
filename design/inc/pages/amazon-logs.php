<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Logs</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Logs</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix m-0">
					<img src="flags/France.png" class="flag m-r10" alt="">
					<span class="p-t10">France</span>
				</p>
			</div>
		</div>		
		<div class="clearfix">
			<div class="amazonLogs">
				<div class="logs m-t20">
					<h4 class="logs_head">Latest updates</h4>
				</div>
				<div class="logs">
					<h5 class="logs_text">Normal update</h5>
					<div class="row">
						<div class="col-xs-1">
							<i class="icon-log_date"></i>
						</div>
						<div class="col-xs-5 logsContent">
							<p class="logs_text-content"><span>Date</span></p>
							<p class="logs_text-content">2015 - 02 - 17</p>							
						</div>
						<div class="col-xs-5">
							<p class="logs_text-content"><span>Time</span></p>
							<p class="logs_text-content">09 : 40 : 28</p>
						</div>
					</div>
				</div>
				<div class="logs drown">
					<h5 class="logs_text">Cron update</h5>
					<div class="row">
						<div class="col-xs-1">
							<i class="icon-log_clock"></i>
						</div>
						<div class="col-xs-5 logsContent">
							<p class="logs_text-content"><span>Date</span></p>
							<p class="logs_text-content">2015 - 02 - 17</p>							
						</div>
						<div class="col-xs-5">
							<p class="logs_text-content"><span>Time</span></p>
							<p class="logs_text-content">09 : 40 : 28</p>
						</div>
					</div>
				</div>
			</div>			
			<div class="amazonTableLogs">
				<p class="head_text p-t20 b-None m-0">
					<span class="p-t10">History</span>
				</p>
				<table class="table tableLog">
					<thead class="table-head">
						<tr>
							<th class="tableId">Date / Time</th>
							<th class="tableUserName">Action</th>
							<th class="tableStatus">Status</th>
							<th class="tableJoined">Result</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="tableId" data-th="Date / Time:" data-name="">
								<p class="logRow">2015 - 02 - 18</p>
								<p class="logRow">09: 39: 02</p>
							</td>
							<td class="tableUserName" data-th="Action:">
								<p class="logRow"><span>syncronize (cron)</span></p>
								<p class="logRow">fd3df51g65r4h3d5</p>
							</td>							
							<td class="tableStatus" data-th="Status:">
								<p class="logRow logSuccess">
									<i class="fa fa-check"></i>
									Success
								</p>
							</td>
							<td class="tableJoined" data-th="Joined:">
								<p class="logRow">
									64 items to send , 0 skipped items
								</p>
							</td>
						</tr>
						<tr>
							<td class="tableId" data-th="Date / Time:" data-name="">
								<p class="logRow">2015 - 02 - 18</p>
								<p class="logRow">09: 39: 02</p>
							</td>
							<td class="tableUserName" data-th="Action:">
								<p class="logRow"><span>syncronize (cron)</span></p>
								<p class="logRow">fd3df51g65r4h3d5</p>
							</td>							
							<td class="tableStatus" data-th="Status:">
								<p class="logRow logSuccess">
									<i class="fa fa-check"></i>
									Success
								</p>
							</td>
							<td class="tableJoined" data-th="Joined:">
								<p class="logRow">
									64 items to send , 0 skipped items
								</p>
							</td>
						</tr>
						<tr>
							<td class="tableId" data-th="Date / Time:" data-name="">
								<p class="logRow">2015 - 02 - 18</p>
								<p class="logRow">09: 39: 02</p>
							</td>
							<td class="tableUserName" data-th="Action:">
								<p class="logRow"><span>syncronize (cron)</span></p>
								<p class="logRow">fd3df51g65r4h3d5</p>
							</td>							
							<td class="tableStatus" data-th="Status:">
								<p class="logRow logSuccess">
									<i class="fa fa-check"></i>
									Success
								</p>
							</td>
							<td class="tableJoined" data-th="Joined:">
								<p class="logRow">
									64 items to send , 0 skipped items
								</p>
							</td>
						</tr>
						<tr>
							<td class="tableId" data-th="Date / Time:" data-name="">
								<p class="logRow">2015 - 02 - 18</p>
								<p class="logRow">09: 39: 02</p>
							</td>
							<td class="tableUserName" data-th="Action:">
								<p class="logRow"><span>syncronize (cron)</span></p>
								<p class="logRow">fd3df51g65r4h3d5</p>
							</td>							
							<td class="tableStatus" data-th="Status:">
								<p class="logRow logSuccess">
									<i class="fa fa-check"></i>
									Success
								</p>
							</td>
							<td class="tableJoined" data-th="Joined:">
								<p class="logRow">
									64 items to send , 0 skipped items
								</p>
							</td>
						</tr>
						<tr>
							<td class="tableId" data-th="Date / Time:" data-name="">
								<p class="logRow">2015 - 02 - 18</p>
								<p class="logRow">09: 39: 02</p>
							</td>
							<td class="tableUserName" data-th="Action:">
								<p class="logRow"><span>syncronize (cron)</span></p>
								<p class="logRow">fd3df51g65r4h3d5</p>
							</td>							
							<td class="tableStatus" data-th="Status:">
								<p class="logRow logSuccess">
									<i class="fa fa-check"></i>
									Success
								</p>
							</td>
							<td class="tableJoined" data-th="Joined:">
								<p class="logRow">
									64 items to send , 0 skipped items
								</p>
							</td>
						</tr>											
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-t10">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Last Logs</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Batch ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Action Process
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Action Type
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Messages
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Date / Time
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">Batch ID</th>
									<th class="tableUserName">Action Process</th>
									<th class="tableEmail">Action Type</th>
									<th class="tableStatus">Messages</th>
									<th class="tableJoined">Date / Time</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Process:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Action Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Messages:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Date / Time:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Process:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Action Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Messages:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Date / Time:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Process:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Action Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Messages:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Date / Time:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Process:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Action Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Messages:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Date / Time:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Process:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Action Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Messages:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Date / Time:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Process:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Action Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Messages:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Date / Time:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Action Process:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Action Type:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Messages:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Date / Time:">Lorem Ipsum</td>
								</tr>											
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>