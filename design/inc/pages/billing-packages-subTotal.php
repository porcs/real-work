<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">CHOOSE YOUR PACKAGES</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Billing</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Billing information</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-b10">
					<ul class="headSettings_point clearfix">
						<li class="headSettings_point-item active">
							<span class="step">1</span>
							<span class="title">Packages</span>
						</li>
						<li class="headSettings_point-item">
							<span class="step">2</span>
							<span class="title">Information</span>
						</li>
						<li class="headSettings_point-item">
							<span class="step">3</span>
							<span class="title">Complete</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-sm-6">
						<div class="headSettings clearfix b-Bottom p-b10">
							<div class="pull-md-left clearfix">
								<h4 class="headSettings_head">Packages</h4>						
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#amazon" data-toggle="tab">Amazon</a></li>
									<li><a href="#ebay" data-toggle="tab">Ebay</a></li>
									<li><a href="#play" data-toggle="tab">Play.com</a></li>								
								</ul>
							</div>
						</div>
						<div class="tab-content">
							<div class="tab-pane active" id="amazon">
								<div class="row">
									<div class="col-sm-6">
										<p class="m-t10"><span class="true-pink">&euro; 10.00</span> per/month</p>
									</div>
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-5"><p class="m-t10 light-gray">Currency</p></div>
											<div class="col-sm-7">
												<select class="search-select">
													<option value="">Euro</option>
													<option value="">Dollar</option>
													<option value="">Russian ruble</option>
													<option value="">Bellorusian ruble</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="allCheckBo">
									<div class="mainCheckbo">
										<label class="cb-checkbox checkAll">
											<input type="checkbox"/>
											Europe
										</label>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.fr (France) 
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.co.uk (United Kingdom)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.de (Germany)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.it (Italy)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.es (Spain)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
								</div>
								<div class="allCheckBo">
									<div class="mainCheckbo">
										<label class="cb-checkbox checkAll">
											<input type="checkbox"/>
											USa
										</label>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.fr (France) 
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
								</div>
								<div class="allCheckBo">
									<div class="mainCheckbo">
										<label class="cb-checkbox checkAll">
											<input type="checkbox"/>
											International
										</label>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.fr (France) 
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.co.uk (United Kingdom)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.de (Germany)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.it (Italy)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											amazon.es (Spain)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="ebay">
								<div class="row">
									<div class="col-sm-6">
										<p class="m-t10"><span class="true-pink">&euro; 10.00</span> per/month</p>
									</div>
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-5"><p class="m-t10 light-gray">Currency</p></div>
											<div class="col-sm-7">
												<select class="search-select">
													<option value="">Euro</option>
													<option value="">Dollar</option>
													<option value="">Russian ruble</option>
													<option value="">Bellorusian ruble</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="allCheckBo">
									<div class="mainCheckbo">
										<label class="cb-checkbox checkAll">
											<input type="checkbox"/>
											Europe
										</label>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.fr (France) 
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.co.uk (United Kingdom)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.de (Germany)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.it (Italy)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.es (Spain)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
								</div>
								<div class="allCheckBo">
									<div class="mainCheckbo">
										<label class="cb-checkbox checkAll">
											<input type="checkbox"/>
											USa
										</label>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.fr (France) 
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
								</div>
								<div class="allCheckBo">
									<div class="mainCheckbo">
										<label class="cb-checkbox checkAll">
											<input type="checkbox"/>
											International
										</label>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.fr (France) 
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.co.uk (United Kingdom)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.de (Germany)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.it (Italy)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											ebay.es (Spain)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="play">
								<div class="row">
									<div class="col-sm-6">
										<p class="m-t10"><span class="true-pink">&euro; 10.00</span> per/month</p>
									</div>
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-5"><p class="m-t10 light-gray">Currency</p></div>
											<div class="col-sm-7">
												<select class="search-select">
													<option value="">Euro</option>
													<option value="">Dollar</option>
													<option value="">Russian ruble</option>
													<option value="">Bellorusian ruble</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="allCheckBo">
									<div class="mainCheckbo">
										<label class="cb-checkbox checkAll">
											<input type="checkbox"/>
											Europe
										</label>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.fr (France) 
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.co.uk (United Kingdom)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.de (Germany)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.it (Italy)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.es (Spain)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
								</div>
								<div class="allCheckBo">
									<div class="mainCheckbo">
										<label class="cb-checkbox checkAll">
											<input type="checkbox"/>
											USa
										</label>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.fr (France) 
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
								</div>
								<div class="allCheckBo">
									<div class="mainCheckbo">
										<label class="cb-checkbox checkAll">
											<input type="checkbox"/>
											International
										</label>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.fr (France) 
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.co.uk (United Kingdom)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.de (Germany)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.it (Italy)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox"/>
											play.es (Spain)
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="headSettings clearfix b-Bottom p-b10">
							<div class="pull-md-left clearfix">
								<h4 class="headSettings_head">Packages Summary</h4>						
							</div>
						</div>
						<div class="allCheckBo">
							<div class="checkboSettings head clearfix">
								<p class="pull-left text-uc strong-text dark-gray">									
									Amazon
								</p>
								<p class="pull-right strong-text"><span class="true-pink">&euro; 10.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<p class="pull-left">
									play.it (Italy)
								</p>
								<p class="pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<p class="pull-left">
									play.es (Spain)
								</p>
								<p class="pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings head clearfix b-Bottom">
								<p class="pull-left text-uc strong-text dark-gray">									
									Sub Total
								</p>
								<p class="pull-right strong-text"><span class="true-green">&euro; 10.00</span> per/month</p>
							</div>
						</div>
						<div class="pull-right m-t20">
							<p href="" class="poor-gray p-size pull-left m-tr10">
								You can add packages from other tabs such as eBay and Play.com
							</p>
							<button class="btn btn-save">Next <i class="m-l5 fa fa-angle-right"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>		