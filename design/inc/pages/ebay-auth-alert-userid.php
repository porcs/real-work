<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">EBAY AUTHENTICATION & SECURITY</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Auth & security</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 custom-form">
				<div class="headSettings clearfix b-Bottom b-Top-None p-t0">
					<h4 class="headSettings_head pull-sm-left">eBay Authentication & Security</h4>										
					<div class="pull-sm-right p-t7">
						<p class="pull-left poor-gray p-t3 m-r10">
							Production Keys  |  Mode
						</p>
						<div class="cb-switcher">
							<label class="inner-switcher">
								<input type="checkbox" data-state-on="ON" data-state-off="OFF"/>
							</label>
							<span class="cb-state">ON</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text m-t20 b-None">Link to your eBay account ID</p>
			</div>
		</div>
		<div class="row m-t10">
			<div class="col-sm-5">
				<p class="poor-gray ">
					eBay UserID :
				</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error text-left">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter eBay UserID
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-7 m-t25">
				<a href="" class="pull-left link p-size m-tr10">
					Reset
				</a>
				<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button> 
			</div>
		</div>
	</div>