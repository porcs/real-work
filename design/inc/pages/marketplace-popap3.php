<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Configuration</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Marketplace</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Configuration</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#amazon" data-toggle="tab">Amazon</a></li>
					<li><a href="#ebay" data-toggle="tab">Ebay</a></li>
					<li><a href="#play" data-toggle="tab">Play.com</a></li>								
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 configuration">
				<div class="tab-content">
					<div class="tab-pane active" id="amazon">								
						<div class="allCheckBo">
							<div class="mainCheckbo b-Top-None">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									Europe
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.fr (France) 
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.co.uk (United Kingdom)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.de (Germany)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.it (Italy)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.es (Spain)
								</label>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									USa
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.fr (France) 
								</label>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									International
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.fr (France) 
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.co.uk (United Kingdom)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.de (Germany)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.it (Italy)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.es (Spain)
								</label>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="ebay">
						<div class="allCheckBo">
							<div class="mainCheckbo b-Top-None">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									Europe
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.fr (France) 
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.co.uk (United Kingdom)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.de (Germany)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.it (Italy)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.es (Spain)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									USa
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.fr (France) 
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									International
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.fr (France) 
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.co.uk (United Kingdom)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.de (Germany)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.it (Italy)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.es (Spain)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="play">
						<div class="allCheckBo">
							<div class="mainCheckbo b-Top-None">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									Europe
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.fr (France) 
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.co.uk (United Kingdom)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.de (Germany)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.it (Italy)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.es (Spain)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									USa
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.fr (France) 
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									International
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.fr (France) 
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.co.uk (United Kingdom)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.de (Germany)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.it (Italy)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.es (Spain)
								</label>
								<p class="m-t10 pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t20 p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Reset
						</a>
						<button type="button" class="btn btn-save btnNext">Save</button>                   
					</div>
				</div>		
			</div>
		</div>
	</div>

	<div class="modal fade" id="marketplace">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="lightRoboto text-uc head-size light-gray">Marketplace</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix m-b10">
								<ul class="headSettings_point clearfix">
									<li class="headSettings_point-item active">
										<span class="step">1</span>
										<span class="title">Edit your profile</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">2</span>
										<span class="title">Verify your web store</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">3</span>
										<span class="title">Category Settings</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">4</span>
										<span class="title">Marketplace configuration</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix p-b10">
								<h4 class="headSettings_head">Congatulations, you have successfully configured your first login.</h4>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Bottom p-b10 text-center">	
								<p class="light-gray regRoboto">And, we recommend to configuration your marketplace account and send your items to marketplaces.</p>
							</div>
						</div>
					</div>
					<div class="row m-t20">
						<div class="col-sm-6">
							<button class="btn btn-save pull-sm-right">Amazon Wizard</button>			
						</div>
						<div class="col-sm-6">
							<button class="btn btn-save">eBay Wizard</button>
						</div>
					</div>																		
				</div>
			</div>
		</div>
	</div>		