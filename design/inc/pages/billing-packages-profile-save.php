	<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">complete payment</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Billing</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Billing information</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-b10">
					<ul class="headSettings_point clearfix">
						<li class="headSettings_point-item active">
							<span class="step">1</span>
							<span class="title">Packages</span>
						</li>
						<li class="headSettings_point-item active">
							<span class="step">2</span>
							<span class="title">Information</span>
						</li>
						<li class="headSettings_point-item active">
							<span class="step">3</span>
							<span class="title">Complete</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10">
					<h4 class="headSettings_head">Billing Information</h4>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Bottom p-b10 text-center">
					<h4 class="headSettings_head pull-none">Your Packages profile has been saved.</h4>	
					<p class="light-gray regRoboto">You just complete package profile.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 m-t20">
				<a href="" class="link p-size pull-right m-t8">Previous</a>				
			</div>
			<div class="col-xs-6 m-t20">
				<button class="btn btn-save ">Finish</button>
			</div>
		</div>
	</div>