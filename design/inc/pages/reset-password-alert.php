<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
			<div class="signIn">
				<h4 class="montserrat text-center text-uc p-b10">Reset password</h4>				
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">Enter your new password</p>
						<div class="form-group has-error">
							<input type="text" class="form-control">
							<div class="error text-right">
								<p>
									<i class="fa fa-exclamation-circle"></i>
									Please enter password
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">Confirm password</p>
						<div class="form-group has-error">
							<input type="text" class="form-control">
							<div class="error text-right">
								<p>
									<i class="fa fa-exclamation-circle"></i>
									Please confirm password
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">					
					<div class="col-xs-6 m-t5">
						<a class="sign_link" href=""><i class="fa fa-long-arrow-left"></i> Back to login</a>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-save pull-right">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>