<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Import history</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>	
			<li class="bread_item active"><a class="bread_link" href="">Import history</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Import history</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Shop name
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Task
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Running by
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Time
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">Shop name</th>
									<th class="tableUserName">Task</th>
									<th class="tableEmail">Running by</th>
									<th class="tableStatus">Time</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Shop name:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Task:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Running by:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Time:">Lorem Ipsum</td>									
								</tr>
								<tr>
									<td class="tableId" data-th="Shop name:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Task:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Running by:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Time:">Lorem Ipsum</td>									
								</tr>	
								<tr>
									<td class="tableId" data-th="Shop name:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Task:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Running by:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Time:">Lorem Ipsum</td>									
								</tr>	
								<tr>
									<td class="tableId" data-th="Shop name:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Task:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Running by:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Time:">Lorem Ipsum</td>									
								</tr>	
								<tr>
									<td class="tableId" data-th="Shop name:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Task:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Running by:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Time:">Lorem Ipsum</td>									
								</tr>	
								<tr>
									<td class="tableId" data-th="Shop name:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Task:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Running by:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Time:">Lorem Ipsum</td>									
								</tr>			
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>