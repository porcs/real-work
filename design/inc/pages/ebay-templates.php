<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">SETTING CONFIGURATION</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Setting configuration</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top m-b0 clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-tb20 m-b20">
					Upload Templates
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">				
				<p class="poor-gray">
					<i class="fa fa-check light-blue"></i>
					Default Templates
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<button class="btn btn-success m-l15">Download</button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-tb20 b-Top m-tb20">
					Add / Update Template
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="poor-gray">File</p>
				<button class="btn btn-save">Upload Template</button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t20 p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Previous
						</a>
						<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
					</div>
				</div>		
			</div>
		</div>
	</div>