<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">PAYPAL PRE-APPROVAL AGREEMENT</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Billing</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Paypal pre-approval agreement</a></li>
		</ul>
		<div class="row">
			<div class="col-sm-8">
				<div class="payments">
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>Title</p>
						</div>
						<div class="payments_content">
							<p>Feed.biz pre-approval payment</p>
						</div>
					</div>
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>Start date</p>
						</div>
						<div class="payments_content">
							<p>2015-03-12</p>
						</div>
					</div>
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>End date</p>
						</div>
						<div class="payments_content">
							<p>2016-03-12</p>
						</div>
					</div>	
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>Max amount per payment</p>
						</div>
						<div class="payments_content">
							<p>€200.00</p>
						</div>
					</div>	
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>Total amount</p>
						</div>
						<div class="payments_content">
							<p>€2,400.00</p>
						</div>
					</div>
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>Max number of payments</p>
						</div>
						<div class="payments_content">
							<p>40</p>
						</div>
					</div>					
				</div>
			</div>
			<div class="col-sm-4">
				<div class="clearfix pull-sm-right checkIn">
					<a href=""><img class="pull-left m-rb10" src="images/pay/pay-pal.png" alt=""></a>
					<a href=""><img class="pull-left m-rb10" src="images/pay/mastercard.png" alt=""></a>
					<a href=""><img class="pull-left m-rb10" src="images/pay/visa.png" alt=""></a>
					<a href=""><img class="pull-left m-rb10" src="images/pay/express.png" alt=""></a>
					<a href=""><img class="pull-left m-rb10" src="images/pay/discover.png" alt=""></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Disagree
						</a>
						<button type="button" class="btn btn-save btnNext">Agree & Continue</button>                   
					</div>
				</div>		
			</div>
		</div>
	</div>