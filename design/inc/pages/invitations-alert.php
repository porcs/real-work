<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Invitations</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Users</a></li>
			<li class="bread_item"><a class="bread_link" href="">Affiliation</a></li>	
			<li class="bread_item active"><a class="bread_link" href="">Invitations</a></li>		
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">Connect & Share</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="regRoboto poor-gray">With Friend on Facebook, Twitter, or Google.</p>
				<button class="btn btn-facebook"><i class="fa fa-facebook"></i>Sharer on your Facebook Account</button>
				<button class="btn btn-google"><i class="fa fa-google-plus"></i>Sharer on your Google Account</button>
				<button class="btn btn-twitter"><i class="fa fa-twitter"></i>Sharer on your Twitter Account</button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">Referrer link</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="regRoboto poor-gray">Copy + paste your personal link into your website, blog, email or IM.</p>
				<div class="form-group">
					<input type="text" class="form-control">					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 m-tb20 b-Bottom">
					<h4 class="headSettings_head">Invite Friends Via Email</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="regRoboto poor-gray">Send invitation email to your friends and family.  Email of your friends</p>
				<div class="form-group ">
					<input class="form-control tags-input" type="text" data-role="tagsinput"/>					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<button class="btn btn-save pull-right">Sent Invite</button>
			</div>
		</div>
	</div>