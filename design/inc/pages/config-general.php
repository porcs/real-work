<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">general</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>
			<li class="bread_item"><a class="bread_link" href="">Configuration</a></li>			
			<li class="bread_item active"><a class="bread_link" href="">General</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">mode</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="montserrat dark-gray text-uc m-t10">General Mode</p>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-sm-4">
				<label class="cb-radio w-100">
					<input type="radio" name="radio-group" />
					<p class="text-uc dark-gray montserrat m-b0 bold ">
						Beginner
					</p>
					<p class="light-gray m-l25">Choose Beginner mode.</p>
				</label>
			</div>
			<div class="col-sm-4">
				<label class="cb-radio w-100">
					<input type="radio" name="radio-group" />					
					<p class="text-uc dark-gray montserrat m-b0 bold">
						<span class="recommended m-l0 m-r10">Default mode</span>
						Expert
					</p>
					<p class="light-gray m-l25">Choose Expert mode.</p>
				</label>
			</div>
			<div class="col-sm-4">
				<label class="cb-radio w-100">
					<input type="radio" name="radio-group" />
					<p class="text-uc dark-gray montserrat m-b0 bold">
						advanced
					</p>
					<p class="light-gray m-l25">Choose Advanced mode.</p>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">Authentification</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class="pull-left">Security token used between your Shop and Feed.biz</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Username</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Token</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="m-t25">
					<a href="" class="link p-size pull-left m-tr10">Save</a>
					<button class="btn btn-save">Save & Continue</button>
				</div>
			</div>
		</div>
	</div>