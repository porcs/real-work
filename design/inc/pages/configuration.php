<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Configuration</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Marketplace</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Configuration</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#amazon" data-toggle="tab">Amazon</a></li>
					<li><a href="#ebay" data-toggle="tab">Ebay</a></li>
					<li><a href="#play" data-toggle="tab">Play.com</a></li>								
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 configuration">
				<div class="tab-content">
					<div class="tab-pane active" id="amazon">								
						<div class="allCheckBo">
							<div class="mainCheckbo b-Top-None">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									Europe
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.fr (France) 
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.co.uk (United Kingdom)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.de (Germany)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.it (Italy)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.es (Spain)
								</label>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									USa
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.fr (France) 
								</label>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									International
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.fr (France) 
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.co.uk (United Kingdom)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.de (Germany)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.it (Italy)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									amazon.es (Spain)
								</label>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="ebay">
						<div class="allCheckBo">
							<div class="mainCheckbo b-Top-None">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									Europe
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.fr (France) 
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.co.uk (United Kingdom)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.de (Germany)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.it (Italy)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.es (Spain)
								</label>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									USA
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.fr (France) 
								</label>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									International
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.fr (France) 
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.co.uk (United Kingdom)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.de (Germany)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.it (Italy)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									ebay.es (Spain)
								</label>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="play">
						<div class="allCheckBo">
							<div class="mainCheckbo b-Top-None">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									Europe
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.fr (France) 
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.co.uk (United Kingdom)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.de (Germany)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.it (Italy)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.es (Spain)
								</label>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									USa
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.fr (France) 
								</label>
							</div>
						</div>
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									International
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.fr (France) 
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.co.uk (United Kingdom)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.de (Germany)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.it (Italy)
								</label>
							</div>
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox"/>
									play.es (Spain)
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t20 p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Reset
						</a>
						<button type="button" class="btn btn-save btnNext">Save</button>                   
					</div>
				</div>		
			</div>
		</div>
	</div>		