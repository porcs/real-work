<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Mapping carriers</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>
			<li class="bread_item"><a class="bread_link" href="">Mapping</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Carriers</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 p-b20">
					Choose Shipping
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 p-b20 m-b20">
					Carrier & shipping time
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="poor-gray">
					Dispatch time 
				</p>
				<select class="search-select" data-placeholder="Choose">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack & Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce & Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text b-None p-t20">Shipping Rules</p>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-xs-12">
				<label class="cb-radio">
					<input type="radio" name="radio-group">
					Weight
				</label>
			</div>
			<div class="col-xs-12">
				<label class="cb-radio">
					<input type="radio" name="radio-group">
					Price
				</label>
			</div>
			<div class="col-xs-12">
				<label class="cb-radio">
					<input type="radio" name="radio-group">
					Item
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings m-t20 p-b10 b-Bottom">
					<h4 class="headSettings_head">Batch Log</h4>
				</div>
			</div>
		</div>
		<div class="row showSelectBlock">
			<div class="col-sm-4">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
			</div>
			<div class="col-sm-4">
				<div class="form-group form-carriers withIcon clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
					<i class="cb-plus bad"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 m-t20">
				<p class="poor-gray">Choose a Carrier</p>
			</div>
			<div class="col-sm-4 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
			</div>
			<div class="col-sm-4">
				<div class="form-group form-carriers withIcon clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
					<i class="cb-plus good"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings m-t10 p-b10 b-Bottom">
					<h4 class="headSettings_head">Mapping International Shipping</h4>
				</div>
			</div>
		</div>
		<div class="row showSelectBlock">	
			<div class="col-sm-4">
				<div class="form-group form-carriers withIcon clearfix">	
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>				
					<i class="cb-plus bad"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 m-t20">
				<p class="poor-gray">Country</p>
			</div>		
			<div class="col-sm-4">
				<div class="form-group form-carriers withIcon clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>					
					<i class="cb-plus good"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<p class="poor-gray">Choose a Carrier</p>
			</div>
			<div class="col-sm-4 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
			</div>
			<div class="col-sm-4 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings m-t10 p-b10 b-Bottom">
					<h4 class="headSettings_head">Set Shipping Item Rules</h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="poor-gray m-t20">
					All
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="input-group">
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group">
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="poor-gray">
					Choose an Associate Carrier 
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="input-group">
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group">
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="poor-gray">
					Choose a Country 
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="input-group">
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group">
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Previous
						</a>
						<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
					</div>
				</div>		
			</div>
		</div>	
	</div>