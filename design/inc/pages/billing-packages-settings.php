<div class="p-rl40 p-xs-rl20 m-t20">
	<div class="row">
		<div class="col-xs-12">
			<div class="headSettings clearfix b-Bottom p-b10">
				<div class="pull-md-left clearfix">
					<h4 class="headSettings_head">Settings</h4>						
				</div>
			</div>
		</div>
	</div>
	<div class="row m-t10">
		<div class="col-sm-4">
			<p class="regRoboto poor-gray">User Name</p>
			<div class="form-group">
				<input type="text" class="form-control">
			</div>
		</div>
		<div class="col-sm-4">
			<p class="regRoboto poor-gray">Email</p>
			<div class="form-group">
				<input type="text" class="form-control">
			</div>
		</div>
		<div class="col-sm-4">
			<p class="regRoboto poor-gray">Joined</p>
			<div class="form-group">
				<input type="text" class="form-control">
			</div>
		</div>
	</div>
	<div class="row custom-form">
		<div class="col-xs-12">
			<p class="pull-left text-uc montserrat dark-gray m-t2">									
				PayPal pre-approval:
			</p>
			<div class="cb-switcher m-l10">
				<label class="inner-switcher">
					<input type="checkbox" data-state-on="ON" data-state-off="OFF"/>
				</label>
				<span class="cb-state">ON</span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<p class="m-t10 regRoboto light-gray">Pre-approved payments, in which a sender logs into PayPal and sets up pre-approved payments. 
				The sender logs into Paypal.com once to set up the pre-approval. Once the sender agrees to the pre-approval, specific approval is no longer necessary. 
				<a href="" class="link">See details.</a>
			</p>
		</div>
		<div class="col-sm-6">
			<div class="pull-sm-right clearfix">
				<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/pay-pal.png" alt=""></a>
				<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/mastercard.png" alt=""></a>
				<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/visa.png" alt=""></a>
				<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/express.png" alt=""></a>
				<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/discover.png" alt=""></a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 b-Top m-t20 p-t20">
			<div class="pull-right">
				<button class="btn btn-save">Next <i class="m-l5 fa fa-angle-right"></i></button>
			</div>
		</div>
	</div>
</div>