<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Filters</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>
			<li class="bread_item"><a class="bread_link" href="">Parameters</a></li>			
			<li class="bread_item"><a class="bread_link" href="">Products</a></li>						
			<li class="bread_item active"><a class="bread_link" href="">Filters</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">Manufacturers</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 relative">
				<p class="head_text">
					<i class="fa fa-remove true-pink"></i>
					Excluded Manufacturers
				</p>
				<ul class="includeBlock">					
				</ul>
				<i class="blockRight"></i>
			</div>
			<div class="col-sm-6 relative">
				<p class="head_text">
					<i class="fa fa-check true-green"></i>
					Included  Manufacturers
				</p>
				<ul class="includeBlock">
				</ul>
				<i class="blockLeft"></i>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">Suppliers</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 relative">
				<p class="head_text">
					<i class="fa fa-remove true-pink"></i>
					Excluded suppliers
				</p>
				<ul class="includeBlock">
				</ul>
				<i class="blockRight"></i>
			</div>
			<div class="col-sm-6 relative">
				<p class="head_text">
					<i class="fa fa-check true-green"></i>
					Included suppliers
				</p>
				<ul class="includeBlock">					
				</ul>
				<i class="blockLeft"></i>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t10 m-t10">
					<button class="btn btn-save pull-right">Save</button>
				</div>
			</div>
		</div>
	</div>