	<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Dashboard</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item active"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Dashboard</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="p-rl40 p-xs-rl20">
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Activities</h4>
					<ul class="list-circle-blue">
						<li>Import Product from "FOXCHIP"</li>
						<li>Export Product to ebay.fr</li>
						<li>Import Product from "FOXCHIP"</li>
						<li>Import Product from "FOXCHIP"</li>					
					</ul>
					<a href="" class="more m-b30">+3 more</a>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Best Categories</h4>
					<ul class="list-circle-unstyled m-b30">
						<li>
							<div id="miniFirst"></div>
							<p>Goodies (FOXCHIP)</p>
							<p><span>5,538</span></p>
						</li>
						<li>
							<div id="miniSecond"></div>
							<p>Jeux-Video (FOXCHIP)</p>
							<p><span>966</span></p>
						</li>
						<li>
							<div id="miniThird"></div>
							<p>J Telephone/Tablette (FOX)</p>
							<p><span>464</span></p>
						</li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Configuration Checklist</h4>
					<ul class="list-circle-green m-b30">
						<li>Data source completed.</li>
						<li>Market place configuration complete.</li>
						<li>Category complete.</li>				
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Errors - Last Entries</h4>
					<ul class="list-circle-red m-b30">
						<li>Out of stock</li>
						<li>Product are Inactive</li>		
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="mainChart p-xs-rl20">
					<div class="row">
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total products</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="product"></div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total orders</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="order"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<div class="p-rl40 p-xs-rl20">
					<div class="chart lineChart">
						<h4 class="text-uc dark-gray bold m-t20">Total sale</h4>
						<ul class="tabs">
							<li class="active">30 days</li>
							<li>7 Days</li>
							<li>1 Day</li>
						</ul>
						<div id="sale"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row p-rl40 p-xs-rl20">
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/amazon.png" alt="">
					<a class="pay_link" href="">
						Configure Amazon
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/ebay.png" alt="">
					<a class="pay_link" href="">
						Configure Ebay
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/logo_count.png" alt="">
					<ul class="pay_list">
						<li class="pay_item">Days</li>
						<li class="pay_item">Hours</li>
						<li class="pay_item">Minutes</li>
					</ul>
					<div id="countdown"></div>
					<div id="note"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="support dSTable">
					<div class="dRow">
						<img src="images/icons/enot.jpg" alt="" class="dCell icon-circle icon-user support_picture">							
						<div class="dCell">
							<p class="tooLight-gray">Your support</p>
							<p class="dark-gray h1 m-t0">Zoye</p>
							<a href="" class="link regRoboto h4">praew@common-services.com</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>