<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Orders</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Orders</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12 ">
				<div class="b-Top p-t10 clearfix">
					<p class="pull-sm-left montserrat dark-gray text-uc bold">
						<img src="flags/Belarus.png" class="flag m-r10" alt="">
						<span class="p-t10">Belarus</span>
					</p>
					<div class="pull-sm-right">
						<button class="btn btn-save m-b10">Import orders</button>  
						<button class="btn btn-save m-b10">Send orders</button>  
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-md-left clearfix">
						<h4 class="headSettings_head">Orders report</h4>						
					</div>
					<div class="pull-md-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											ID Order
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											ID Order ref.
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											ID Buyer
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Country Name
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Payment By
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											Paid Amount
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="error" checked/>
											Order Date
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="date" checked/>
											ID Invoice
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="download" checked/>
											Exported
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th>ID Order</th>
									<th>ID Order ref.</th>
									<th>ID Buyer</th>
									<th>Country Name</th>
									<th>Payment By</th>
									<th>Paid Amount</th>
									<th>Order Date</th>
									<th>ID Invoice</th>
									<th>Exported</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="ID Order:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="ID Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Country Name:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payment By:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Paid Amount:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">Lorem Ipsum</td>
									<td class="tableDate" data-th="ID Invoice:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Exported:"><span class="errorText">Lorem Ipsum</span></td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID Order:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="ID Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Country Name:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payment By:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Paid Amount:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">Lorem Ipsum</td>
									<td class="tableDate" data-th="ID Invoice:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Exported:"><span class="errorText">Lorem Ipsum</span></td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID Order:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="ID Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Country Name:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payment By:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Paid Amount:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">Lorem Ipsum</td>
									<td class="tableDate" data-th="ID Invoice:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Exported:"><span class="errorText">Lorem Ipsum</span></td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID Order:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="ID Order ref.:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="ID Buyer:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Country Name:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payment By:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Paid Amount:">Lorem Ipsum</td>
									<td class="tableError" data-th="Order Date:">Lorem Ipsum</td>
									<td class="tableDate" data-th="ID Invoice:">Lorem Ipsum</td>
									<td class="tableDownload" data-th="Exported:"><span class="errorText">Lorem Ipsum</span></td>
								</tr>
							</tbody>
							<tfoot class="table-foot">
                                <tr>
                                	<th>
                                		<input class="form-control" type="text" placeholder="ID Order">
                                	</th>
                                	<th >
                                		<input class="form-control" type="text" placeholder="ID Order ref.">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="ID Buyer">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Country Name">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Payment By">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Paid Amount">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Order Date">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="ID Invoice">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Exported">
                                	</th>
                                </tr>
                            </tfoot>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>