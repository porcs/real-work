<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Affiliation</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Users</a></li>
			<li class="bread_item"><a class="bread_link" href="">Affiliation</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Referrals List</a></li>
		</ul>		
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Affiliate Lists</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Username
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Email
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Status
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Joined
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											Summary
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">ID</th>
									<th class="tableUserName">User Name</th>
									<th class="tableEmail">Email</th>
									<th class="tableStatus">Status</th>
									<th class="tableJoined">Joined</th>
									<th class="tableSummary">Summary</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="User Name:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">fffffffffedrs</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">mn.n.</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
									<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
									<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
									<td class="tableSummary" data-th="Summary:">cccccccccc</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
									<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
									<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
									<td class="tableSummary" data-th="Summary:">cccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
									<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
									<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
									<td class="tableSummary" data-th="Summary:">cccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
									<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
									<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
									<td class="tableSummary" data-th="Summary:">cccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
									<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
									<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
									<td class="tableSummary" data-th="Summary:">cccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Summary:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
									<td class="tableSummary" data-th="Summary:">aaaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
									<td class="tableSummary" data-th="Summary:">bbbbbbbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
									<td class="tableSummary" data-th="Summary:">cccccccccc</td>
								</tr>								
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-md-left clearfix">
						<h4 class="headSettings_head">Affiliate activity</h4>						
					</div>
					<div class="pull-md-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											IP address
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Username
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Clicks
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Joined
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Last activity
										</label>
									</li>
								</ul>
							</div>
						</div>
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th>IP address</th>
									<th>User Name</th>
									<th>Clicks</th>
									<th>Joined</th>
									<th>Last activity</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Joined:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">fffffffffedrs</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">mn.n.</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">fffffffffedrs</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">mn.n.</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>

									<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Clicks:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Last activity:">Lorem Ipsum</td>
								</tr>
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>