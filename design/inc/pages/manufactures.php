<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Manufacturers</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>
			<li class="bread_item"><a class="bread_link" href="">Parameters</a></li>
			<li class="bread_item"><a class="bread_link" href="">Products</a></li>
			<li class="bread_item"><a class="bread_link" href="">Mapping</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Manufacturers</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t10">
					<p class="pull-right montserrat poor-gray"><span class="dark-gray count">0</span> Records</p>
				</div>
			</div>
		</div>
		<div class="row showSelectBlock">
			<div class="col-sm-6">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group withIcon clearfix">
					<input type="text" class="form-control">
					<i class="cb-plus bad"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group withIcon clearfix">
					<input type="text" class="form-control">
					<i class="cb-plus good"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="pull-right">			
					<button class="btn btn-save m-t10">Save</button>
				</div>	
			</div>
		</div>
	</div>