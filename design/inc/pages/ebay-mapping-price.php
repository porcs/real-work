<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Mapping templates</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>
			<li class="bread_item"><a class="bread_link" href="">Mapping</a></li>						
			<li class="bread_item active"><a class="bread_link" href="">Templates</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top m-b0 clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-tb20 m-b20">
					Price rules
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<div class="pull-left">								
								<ol>
									<li>
										<p>You could configure a price rule in values or percentages for one or several price ranges.</p>
									</li>
									<li>										
										<p>Formula to be applied on all the exported product prices (multiplication, division, addition, subtraction, percentages).</p>
									</li>
									<li>										
										<p>Apply a specific price formula for eBay selected categories which will override the main setting.</p>
									</li>
									<li>
										<p>Rounding mode to be applied to the price (ie: price will be equal to 10.17 or 10.20)</p>
									</li>
								</ol>
							</div>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row showSelectBlock">			
			<div class="col-sm-3">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
			</div>
			<div class="col-sm-3">
				<div class="input-group">					
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">					
				</div>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-3">
				<div class="input-group">					
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">					
					<i class="iconToNext"></i>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group withIcon clearfix">
					<div class="input-group">
						<span class="input-group-addon">
							<strong>$</strong>
						</span>
						<input type="text" class="form-control m-b0">
					</div>
					<i class="cb-plus bad"></i>
				</div>				
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="poor-gray">Default Price Rule</p>				
			</div>
			<div class="col-sm-3 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
			</div>
			<div class="col-sm-3">
				<div class="input-group">					
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">					
				</div>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-3">
				<div class="input-group">					
					<span class="input-group-addon">
						<strong>$</strong>
					</span>
					<input type="text" class="form-control">					
					<i class="iconToNext"></i>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group withIcon clearfix">
					<div class="input-group">
						<span class="input-group-addon">
							<strong>$</strong>
						</span>
						<input type="text" class="form-control m-b0">
					</div>
					<i class="cb-plus good"></i>
				</div>				
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text b-None">Rounding</p>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-xs-12">
				<label class="cb-radio">
					<input type="radio" name="radio-group">
					One Digit
				</label>
			</div>
			<div class="col-xs-12">
				<label class="cb-radio">
					<input type="radio" name="radio-group">
					Two digits
				</label>
			</div>
			<div class="col-xs-12">
				<label class="cb-radio">
					<input type="radio" name="radio-group">
					None
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t20 p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Previous
						</a>
						<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
					</div>
				</div>		
			</div>
		</div>
	</div>