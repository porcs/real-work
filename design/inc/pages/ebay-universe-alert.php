<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">SETTING CONFIGURATION</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Setting configuration</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top m-b0 clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Top-None b-Bottom p-0">
					<h4 class="headSettings_head pull-sm-left p-t10">Selected Main Categories</h4>										
					<ul class="pull-sm-right clearfix treeSettings">						
						<li class="checkedAll"><i class="cb-checked"></i>Check all</li>
						<li class="uncheckedAll"><i class="cb-unchecked"></i>Uncheck all</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="validate pink m-t20">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<div class="pull-left">
								<p>
									Update Categories Selected Failed : Please, try again.
								</p>								
							</div>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row custom-form">
			<div class="col-sm-4 col-lg-3">
				<div class="tree universe">
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Accessoires animaux
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Art du XXeme, contemporain
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Auto: pieces, accessoires
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Bijoux, montres
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Collections
					</label>

					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Image, son
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Informatique, Reseaux
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Jeux, jouets, figurines
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Livres, BD, revues
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Maison
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Musique, CD, vinyles
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						PME, artisans, agriculteurs
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Vacances
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Timbres
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Vins
					</label>								
				</div>
			</div>
			<div class="col-sm-4 col-lg-3">
				<div class="tree universe">
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Art, antiquites
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Auto, moto
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Beaute, bien-etre, parfums
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Ceramiques, verres
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						DVD, cinema
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Immobilier
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Jardin, terrasse
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Jeux video, consoles
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Loisirs creatifs
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Monnaies
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Photo, camescopes
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Sports, vacances
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Telephonie, mobilite
					</label>
					<label class="cb-checkbox w-100">											
						<input type="checkbox"/>
						Vetements, accessoires
					</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t20 p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Previous
						</a>
						<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
					</div>
				</div>		
			</div>
		</div>
	</div>
