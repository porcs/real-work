	<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Dashboard</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item active"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Dashboard</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="p-rl40 p-xs-rl20">
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Activities</h4>
					<ul class="list-circle-blue">
						<li>Import Product from "FOXCHIP"</li>
						<li>Export Product to ebay.fr</li>
						<li>Import Product from "FOXCHIP"</li>
						<li>Import Product from "FOXCHIP"</li>					
					</ul>
					<a href="" class="more m-b30">+3 more</a>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Best Categories</h4>
					<ul class="list-circle-unstyled m-b30">
						<li>
							<div id="miniFirst"></div>
							<p>Goodies (FOXCHIP)</p>
							<p><span>5,538</span></p>
						</li>
						<li>
							<div id="miniSecond"></div>
							<p>Jeux-Video (FOXCHIP)</p>
							<p><span>966</span></p>
						</li>
						<li>
							<div id="miniThird"></div>
							<p>J Telephone/Tablette (FOX)</p>
							<p><span>464</span></p>
						</li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Configuration Checklist</h4>
					<ul class="list-circle-green m-b30">
						<li>Data source completed.</li>
						<li>Market place configuration complete.</li>
						<li>Category complete.</li>				
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Errors - Last Entries</h4>
					<ul class="list-circle-red m-b30">
						<li>Out of stock</li>
						<li>Product are Inactive</li>		
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="mainChart p-xs-rl20">
					<div class="row">
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total products</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="product"></div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total orders</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="order"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<div class="p-rl40 p-xs-rl20">
					<div class="chart lineChart">
						<h4 class="text-uc dark-gray bold m-t20">Total sale</h4>
						<ul class="tabs">
							<li class="active">30 days</li>
							<li>7 Days</li>
							<li>1 Day</li>
						</ul>
						<div id="sale"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row p-rl40 p-xs-rl20">
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/amazon.png" alt="">
					<a class="pay_link" href="" data-toggle="modal" data-target="#amazonConfigurate">
						Configure Amazon
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/ebay.png" alt="">
					<a class="pay_link" href="" data-toggle="modal" data-target="#ebayConfigurate">
						Configure Ebay
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/logo_count.png" alt="">
					<ul class="pay_list">
						<li class="pay_item">Days</li>
						<li class="pay_item">Hours</li>
						<li class="pay_item">Minutes</li>
					</ul>
					<div id="countdown"></div>
					<div id="note"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="support dSTable">
					<div class="dRow">
						<img src="images/icons/enot.jpg" alt="" class="dCell icon-circle icon-user support_picture">							
						<div class="dCell">
							<p class="tooLight-gray">Your support</p>
							<p class="dark-gray h1 m-t0">Zoye</p>
							<a href="" class="link regRoboto h4">praew@common-services.com</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="amazonConfigurate">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="lightRoboto text-uc head-size light-gray">Amazon wizard</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix m-b10">
								<ul class="headSettings_point clearfix">
									<li class="headSettings_point-item active">
										<span class="step">1</span>
										<span class="title">Choose Amazon site</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">2</span>
										<span class="title">Config API setting</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">3</span>
										<span class="title">Choose category</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">4</span>
										<span class="title">Sync & match</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">5</span>
										<span class="title">Send to Amazon</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top p-t10 clearfix">
								<p class="head_text b-None clearfix m-0">
									<img src="flags/France.png" class="flag m-r10" alt="">
									<span class="p-t10">France</span>					
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Bottom p-0">
								<h4 class="headSettings_head pull-sm-left p-t10">Choose categories</h4>										
								<ul class="pull-sm-right clearfix treeSettings">
									<li class="expandAll"><i class="cb-plus"></i>Expand all</li>
									<li class="collapseAll"><i class="cb-minus"></i>Collapse all</li>
									<li class="checkedAll"><i class="cb-checked"></i>Check all</li>
									<li class="uncheckedAll"><i class="cb-unchecked"></i>Uncheck all</li>
								</ul>
							</div>						
						</div>
						<div class="col-xs-12">
							<div class="tree clearfix custom-form">	
								<i class="icon-folder"></i>
								<ul class="tree_point">
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>									
												<label class="cb-checkbox ">											
													<input type="checkbox"/>
													women
												</label>
											</p>
										</span>
										<ul class="tree_child">
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															dresses
														</label>
													</p>
												</span>											
												<ul class="tree_child">
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	small
																</label>
															</p>
														</span>
													</li>
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	big
																</label>
															</p>
														</span>
													</li>
												</ul>
											</li>
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															blouses
														</label>
													</p>
												</span>
											</li>
										</ul>
									</li>
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>
												<label class="cb-checkbox">
													<input type="checkbox"/>
													men
												</label>
											</p>
										</span>
									</li>
								</ul>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top m-t5 p-t20">
								<div class="pull-right">
									<a href="" class="pull-left link p-size m-tr10">
										Previous
									</a>
									<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
								</div>
							</div>		
						</div>
					</div>		
				</div>				
			</div>
		</div>
	</div>

	<div class="modal fade" id="ebayConfigurate">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="lightRoboto text-uc head-size light-gray">Ebay wizard</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix m-b10">
								<ul class="headSettings_point clearfix">
									<li class="headSettings_point-item active">
										<span class="step">1</span>
										<span class="title">Authorization</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">2</span>
										<span class="title">Choose Site</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">3</span>
										<span class="title">Shippings</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">4</span>
										<span class="title">Tax</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">5</span>
										<span class="title">Select Univers</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">6</span>
										<span class="title">Categories</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">7</span>
										<span class="title">Export Products</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">8</span>
										<span class="title">Finish</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Bottom p-b10">
								<div class="pull-md-left clearfix">
									<h4 class="headSettings_head">Choose shippings</h4>						
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="validate blue m-t10">
								<div class="validateRow">
									<div class="validateCell">
										<i class="note"></i>
									</div>
									<div class="validateCell">
										<p class="pull-left">
											If you are an newbie user, I would recommend you sign up for the ebay account. 
											This wizard will cover the basic concepts of the eBay Platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account.</p>
										<i class="fa fa-remove pull-right"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="montserrat dark-gray text-uc">Payment & Location</p>
						</div>
					</div>	
					<div class="row">
						<div class="col-sm-4">
							<p class="poor-gray">Postcode :</p>
							<div class="form-group has-error">
								<input type="text" class="form-control">
								<div class="error">
									<p>
										<i class="fa fa-exclamation-circle"></i>
										Please enter postcode
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Bottom p-b10">
								<div class="pull-md-left clearfix">
									<h4 class="headSettings_head">Carrier & Shipping time</h4>						
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<p class="poor-gray m-t10">Dispatch time</p>
							<div class="form-group">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row custom-form">
						<div class="col-sm-4">
							<p class="montserrat dark-gray text-uc">Shipping Rules</p>
							<label class="cb-radio w-100">
								<input type="radio" name="radio-group">
								Weight
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="radio-group">
								Price
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="radio-group">
								Item
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Bottom p-b10">
								<div class="pull-md-left clearfix">
									<h4 class="headSettings_head">Mapping Domestic Shipping</h4>						
								</div>
							</div>
						</div>
					</div>
					<div class="row showSelectBlock">
						<div class="col-sm-4">
							<div class="form-group">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group withIcon clearfix">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<i class="cb-plus bad"></i>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray m-t10">Choose a Carrier</p>
						</div>
						<div class="col-sm-4 m-b20">
							<div class="form-group has-error">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<div class="error">
									<p>
										<i class="fa fa-exclamation-circle"></i>
										Please select item
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group withIcon has-error clearfix">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<div class="error">
									<p>
										<i class="fa fa-exclamation-circle"></i>
										Please select item
									</p>
								</div>
								<i class="cb-plus good"></i>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Bottom p-b10">
								<div class="pull-md-left clearfix">
									<h4 class="headSettings_head">Mapping International Shipping</h4>						
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray m-t10">Country</p>
						</div>
					</div>
					<div class="row showSelectBlock">
						<div class="col-sm-4">
							<div class="form-group withIcon form-carriers clearfix">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<i class="cb-plus bad"></i>
							</div>
						</div>
					</div>	
					<div class="row">						
						<div class="col-sm-4">
							<div class="form-group withIcon form-carriers clearfix">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
								<i class="cb-plus good"></i>
							</div>
						</div>
					</div>	
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray m-t10">Choose a Carrier</p>
						</div>
						<div class="col-sm-4 m-b20">
							<select class="search-select">
								<option value="">Home</option>
								<option value="">Apple</option>
								<option value="">Iphone 6</option>
								<option value="">Iphone 5</option>
								<option value="">Iphone 4</option>
								<option value="">Iphone 3</option>
								<option value="">Men shirt</option>
								<option value="">Jack &amp; Jones</option>
								<option value="">Adidas</option>
								<option value="">Armani</option>
								<option value="">Dolce &amp; Gabbana</option>
								<option value="">Mersedes</option>
								<option value="">Audi</option>
							</select>
						</div>
						<div class="col-sm-4">
							<div class="form-group withIcon clearfix w-xs-100">
								<select class="search-select">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack &amp; Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce &amp; Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Bottom p-b10">
								<div class="pull-md-left clearfix">
									<h4 class="headSettings_head">Set Shipping Item Rules</h4>						
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray m-t10">All</p>
						</div>
						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<strong>$</strong>
								</span>
								<input type="text" class="form-control" placeholder="Cost (USD)">
							</div>	
						</div>
						<div class="col-sm-4">
							<div class="form-group xs-form-group withIcon clearfix">
								<div class="input-group w-xs-100">
									<span class="input-group-addon">
										<strong>$</strong>
									</span>
									<input type="text" class="form-control m-b0" placeholder="Additional (USD)">
								</div>					
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray">Presta Store Test </p>
						</div>
						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<strong>$</strong>
								</span>
								<input type="text" class="form-control" placeholder="0.00">
							</div>	
						</div>
						<div class="col-sm-4">
							<div class="form-group xs-form-group withIcon clearfix">
								<div class="input-group w-xs-100">
									<span class="input-group-addon">
										<strong>$</strong>
									</span>
									<input type="text" class="form-control m-b0" placeholder="0.00">
								</div>					
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray">Choose a Country</p>
						</div>
						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">
									<strong>$</strong>
								</span>
								<input type="text" class="form-control" placeholder="0.00">
							</div>	
						</div>
						<div class="col-sm-4">
							<div class="form-group xs-form-group withIcon clearfix">
								<div class="input-group w-xs-100">
									<span class="input-group-addon">
										<strong>$</strong>
									</span>
									<input type="text" class="form-control m-b0" placeholder="0.00">
								</div>					
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top m-t5 p-t20">
								<div class="pull-right">
									<a href="" class="pull-left link p-size m-tr10">
										Reset
									</a>
									<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
								</div>
							</div>		
						</div>
					</div>																			
				</div>
			</div>
		</div>
	</div>