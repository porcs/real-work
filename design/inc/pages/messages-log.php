<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Messages log</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Messages</a></li>
		</ul>		
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Messages product</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Batch ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Product ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Shop
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Type
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Message
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											Date
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">Batch ID</th>
									<th class="tableUserName">Product ID</th>
									<th class="tableEmail">Shop</th>
									<th class="tableStatus">Type</th>
									<th class="tableJoined">Message</th>
									<th class="tableSummary"><i class="fa fa-remove smallText m-r5"></i>Date</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:"><span class="errorText">Error</span></td>
									<td class="tableJoined" data-th="Message:">Lorem Ipsum</td>	
									<td class="tableSummary" data-th="Date:">Lorem Ipsum</td>										
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:"><span class="errorText">Error</span></td>
									<td class="tableJoined" data-th="Message:">Lorem Ipsum</td>	
									<td class="tableSummary" data-th="Date:">Lorem Ipsum</td>										
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:"><span class="errorText">Error</span></td>
									<td class="tableJoined" data-th="Message:">Lorem Ipsum</td>	
									<td class="tableSummary" data-th="Date:">Lorem Ipsum</td>										
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:"><span class="errorText">Error</span></td>
									<td class="tableJoined" data-th="Message:">Lorem Ipsum</td>	
									<td class="tableSummary" data-th="Date:">Lorem Ipsum</td>										
								</tr>					
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-md-left clearfix">
						<h4 class="headSettings_head">Messages offer</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Batch ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Product ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Shop
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Type
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Message
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											Date
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">Batch ID</th>
									<th class="tableUserName">Product ID</th>
									<th class="tableEmail">Shop</th>
									<th class="tableStatus">Type</th>
									<th class="tableJoined">Message</th>
									<th class="tableSummary"><i class="fa fa-clock-o p-size m-r5"></i>Date</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:"><span class="errorWarning">Warning</span></td>
									<td class="tableJoined" data-th="Message:">Lorem Ipsum</td>	
									<td class="tableSummary" data-th="Date:">Lorem Ipsum</td>										
								</tr>
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:"><span class="errorText">Error</span></td>
									<td class="tableJoined" data-th="Message:">Lorem Ipsum</td>	
									<td class="tableSummary" data-th="Date:">Lorem Ipsum</td>										
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:"><span class="errorText">Error</span></td>
									<td class="tableJoined" data-th="Message:">Lorem Ipsum</td>	
									<td class="tableSummary" data-th="Date:">Lorem Ipsum</td>										
								</tr>	
								<tr>
									<td class="tableId" data-th="Batch ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Shop:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Type:"><span class="errorWarning">Warning</span></td>
									<td class="tableJoined" data-th="Message:">Lorem Ipsum</td>	
									<td class="tableSummary" data-th="Date:">Lorem Ipsum</td>										
								</tr>					
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>