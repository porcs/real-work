<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Extend YOUR PACKAGES</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Billing</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Payment</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings extend clearfix m-b10">
					<ul class="headSettings_point clearfix">
						<li class="headSettings_point-item active">
							<span class="step">1</span>
							<span class="title">Payment</span>
						</li>
						<li class="headSettings_point-item">
							<span class="step">2</span>
							<span class="title">complete</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-sm-6">
						<div class="headSettings clearfix b-Bottom p-b10">
							<div class="pull-md-left clearfix">
								<h4 class="headSettings_head">Packages overview</h4>						
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<p class="m-t10 m-b0 dark-gray montserrat">Period:</p>
								<p class="light-gray">3 February 2015 - 21 February 2015</small>
							</div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-5"><p class="m-t20 light-gray">Currency</p></div>
									<div class="col-sm-7 m-t10 m-b10">
										<select class="search-select">
											<option value="">Euro</option>
											<option value="">Dollar</option>
											<option value="">Russian ruble</option>
											<option value="">Bellorusian ruble</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="allCheckBo b-Top">							
							<div class="checkboSettings clearfix">
								<p class="pull-left">
									Amazon Offer Packages
								</p>
								<p class="pull-right dCell"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<p class="pull-left">
									Amazon United State (amazon.com) Package
								</p>
								<p class="pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<p class="pull-left">
									Amazon France (amazon.fr) Package
								</p>
								<p class="pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings clearfix">
								<p class="pull-left">
									Amazon Germany (amazon.de) Package
								</p>
								<p class="pull-right"><span class="true-pink">+ &euro; 5.00</span> per/month</p>
							</div>
							<div class="checkboSettings head clearfix b-Bottom">
								<p class="pull-left text-uc strong-text dark-gray">									
									Sub Total
								</p>
								<p class="pull-right strong-text"><span class="true-green">&euro; 10.00</span> per/month</p>
							</div>
						</div>					
					</div>
					<div class="col-sm-6 custom-form">
						<div class="headSettings clearfix b-Bottom p-b10">
							<div class="pull-md-left clearfix">
								<h4 class="headSettings_head">choose your payment method</h4>						
							</div>
						</div>
						<label class="cb-radio m-t10 pull-left">
							<input type="radio" name="id"/>
							<p class="text-uc dark-gray montserrat m-b0 ">pay only this bill</p>  
							<p class="light-gray m-l25">We will notify you when your packages expire</p>
						</label>
						<label class="cb-radio m-t10">
							<input type="radio" name="id"/>
							<p class="text-uc dark-gray montserrat m-b0">pay automatically each month</p>  
							<p class="light-gray m-b0 m-l25">Your pre-approval key is  inactive. <a href="" class="link">Change it now.</a></p>
							<p class="light-gray m-l25">We will automatically charge you every <span class="dark-gray">21st</span> of each month</p>
						</label>
						<div class="clearfix">
							<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/pay-pal.png" alt=""></a>
							<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/mastercard.png" alt=""></a>
							<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/visa.png" alt=""></a>
							<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/express.png" alt=""></a>
							<a href=""><img class="pull-left m-r10 m-t10" src="images/pay/discover.png" alt=""></a>
						</div>
						<div class="row">
							<div class="col-xs-12 b-Top m-t20 p-t20">
								<div class="pull-right">
									<button class="btn btn-save">Next <i class="m-l5 fa fa-angle-right"></i></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		