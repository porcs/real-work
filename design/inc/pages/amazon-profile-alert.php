<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Profile</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>				
			<li class="bread_item active"><a class="bread_link" href="">Profile</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t10 clearfix">
					<p class="head_text">
						<img src="flags/France.png" class="flag m-r10" alt="">
						<span class="p-t10">France</span>					
					</p>
				</div>
			</div>
		</div>

		<div class="ruleSettings"></div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Bottom p-b10">
					<a href="" class="link showProfile p-size">+ Add a profile to the list</a>
					<p class="pull-right montserrat poor-gray"><span class="dark-gray countProfile">0</span> Item(s)</p>
				</div>
			</div>
		</div>
		<div class="row showSelectBlock">
			<div class="col-md-10 col-lg-9 custom-form m-t10">
				<a href="" class="removeProfile">Remove a profile from list</a>
				<div class="profileBlock clearfix">
					<p class="regRoboto poor-gray">Profile Name</p>
					<div class="form-group has-error">
						<input type="text" class="form-control">
						<div class="error">
							<p>
								<i class="fa fa-exclamation-circle"></i>
								Please enter profile name first
							</p>
						</div>						
					</div>
					<p class="regRoboto poor-gray">Model Name</p>
					<div class="row">
						<div class="col-xs-12">
							<div class="validate yellow">
								<div class="validateRow">
									<div class="validateCell">
										<i class="note"></i>
									</div>
									<div class="validateCell">
										<p class=""><span class="bold">Models doesn't exits.</span> To create product on Amazon please set a Model go to Configurations > Models Tab.</p>
										<i class="fa fa-remove pull-right"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<p class="regRoboto poor-gray">Out of Stock</p>
					<div class="form-group">
						<input type="text" class="form-control">						
					</div>
					<p class="regRoboto poor-gray text-right">Minimum of quantity in stock to export the product. </p>
					<p class="regRoboto poor-gray">Synchronization Field</p>
					<div class="m-b10 synch">
						<div class="choZn">
							<select class="search-select" data-placeholder="Choose">
								<option value="">Home</option>
								<option value="">Apple</option>
								<option value="">Iphone 6</option>
								<option value="">Iphone 5</option>
								<option value="">Iphone 4</option>
								<option value="">Iphone 3</option>
								<option value="">Men shirt</option>
								<option value="">Jack & Jones</option>
								<option value="">Adidas</option>
								<option value="">Armani</option>
								<option value="">Dolce & Gabbana</option>
								<option value="">Mersedes</option>
								<option value="">Audi</option>
							</select>
						</div>
					</div>					
					<p class="regRoboto poor-gray">Choose the field which will be used for dealing with Amazon.</p>
					<div class="row m-t20">
						<div class="col-sm-4 col-md-5 col-lg-4">
							<p class="montserrat dark-gray text-uc">Title Format</p>
							<label class="cb-radio w-100">
								<input type="radio" name="radio-group">
								Title Format
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="radio-group">
								Manufacturer, Title, Attributes
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="radio-group">
								Manufacturer, Title, Reference, Attributes
							</label>
						</div>
						<div class="col-sm-4 col-md-5 col-lg-4">
							<p class="montserrat dark-gray text-uc">HTML Descriptions</p>
							<label class="cb-checkbox w-100">
								<input type="checkbox">
								Yes
							</label>
							<label class="cb-checkbox w-100">
								<input type="checkbox">
								Unconditionnaly
							</label>
							<p class="poor-gray">Export HTML Descriptions instead of Text Only.</p>
						</div>
						<div class="col-xs-12">
							<p class="poor-gray m-t10">
								Type of product name, Amazon recommend to format the title like Manufacturer, Title, Attributes. 
							</p>
							<p class="poor-gray">
								Please choose the first choice if your products titles are already formated for Amazon.
							</p>
						</div>
					</div>
					<div class="row m-t20">
						<div class="col-sm-4 col-md-5 col-lg-4">
							<p class="montserrat dark-gray text-uc">Description Field</p>
							<label class="cb-radio w-100">
								<input type="radio" name="description">
								Description
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="description">
								Short Description
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="description">
								Both
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="description">
								None
							</label>
						</div>
						<div class="col-sm-4 col-md-5 col-lg-4">
							<p class="montserrat dark-gray text-uc">SKU as Supplier Reference</p>
							<label class="cb-checkbox w-100">
								<input type="checkbox">
								Yes
							</label>
							<label class="cb-checkbox w-100">
								<input type="checkbox">
								Unconditionnaly
							</label>
						</div>
						<div class="col-xs-12">
							<p class="poor-gray m-t10">
								Description field to send to Amazon, short or long. 
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray m-t10">Default condition note</p>
							<div class="form-group">
								<input type="text" class="form-control">						
							</div>
							<p class="poor-gray">Short text about product condition/state which will appear on the product sheet on Amazon.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top p-t20 clearfix">
								<p class="head_text p-b20">
									Code Exemption			
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<label class="cb-checkbox">
								<input type="checkbox">
								Use EAN/UPC exemption for this profile
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top p-t20 m-t10 clearfix">
								<p class="head_text p-b20">
									Recommended Browse Node		
								</p>
							</div>
						</div>
					</div>
					<div class="row hidden">
						<div class="col-xs-12">
							<div class="form-group withIcon clearfix">
								<input type="text" class="form-control">
								<i class="cb-plus bad"></i>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray">Browse Node ID</p>
							<div class="form-group withIcon clearfix">
								<input type="text" class="form-control">
								<i class="cb-plus good"></i>						
							</div>	
						</div>
					</div>
					<p class="poor-gray m-b0">Amazon Browse Node ID - You can use the product classifier to get browse node IDs:</p>
					<p class="poor-gray m-b0"><a class="link p-size" href="https://sellercentral.amazon.com/hz/inventory/classify?ref=ag_pclasshm_cont_invfile">Amazon.com</a></p>
					<p class="poor-gray m-b0"><a class="link p-size" href="https://sellercentral.amazon.fr/hz/inventory/classify?ref=pt_pclasshm_cont_invfile">Amazon.co.uk</a></p>
					<p class="poor-gray">This option is mandatory for Canada, Europe, Japan.</p>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top p-t20 m-t10 clearfix">
								<p class="head_text p-b20">
									Key Product Features
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<label class="cb-radio w-100">
								<input type="radio" name="radio-group">
								Use Product Features (Recommended)
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="radio-group">
								Explode Description into Bullets Points
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="radio-group">
								Do not use key product features (Europe, Japan, Canada only)
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top p-t20 m-t10 clearfix">
								<p class="head_text p-b20">
									Price Rules
								</p>
							</div>
						</div>
					</div>
					<div class="row hidden">
						<div class="col-xs-12">
							<p class="poor-gray">Default Price Rule</p>
						</div>
						<div class="col-sm-3">
							<div class="form-group clearfix m-b0">
								<select class="search-select" data-placeholder="Choose">
									<option value="">Home</option>
									<option value="">Apple</option>
									<option value="">Iphone 6</option>
									<option value="">Iphone 5</option>
									<option value="">Iphone 4</option>
									<option value="">Iphone 3</option>
									<option value="">Men shirt</option>
									<option value="">Jack & Jones</option>
									<option value="">Adidas</option>
									<option value="">Armani</option>
									<option value="">Dolce & Gabbana</option>
									<option value="">Mersedes</option>
									<option value="">Audi</option>
								</select>	
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group">
								<span class="input-group-addon">
									<strong>&euro;</strong>
								</span>
								<input type="text" class="form-control">								
							</div>
							<i class="blockRight noBorder"></i>
						</div>
						<div class="col-sm-3">
							<div class="input-group">
								<span class="input-group-addon">
									<strong>$</strong>
								</span>
								<input type="text" class="form-control">								
							</div>
							<i class="blockRight noBorder"></i>
							<i class="blockRightDouble noBorder"></i>
						</div>
						<div class="col-sm-3">
							<div class="form-group withIcon clearfix">
								<div class="input-group">
									<span class="input-group-addon">
										<strong>$</strong>
									</span>
									<input type="text" class="form-control m-b0">
								</div>
								<i class="cb-plus bad"></i>
							</div>							
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray">Default Price Rule</p>
						</div>
						<div class="col-sm-3">
							<div class="form-group priceRule">
								<div class="choZn ">
									<select class="search-select" data-placeholder="Choose">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack & Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce & Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>	
								</div>
							</div>							
						</div>
						<div class="col-sm-3">
							<div class="input-group">
								<span class="input-group-addon">
									<strong>&euro;</strong>
								</span>
								<input type="text" class="form-control">
							</div>
							<i class="blockRight noBorder"></i>
						</div>
						<div class="col-sm-3">
							<div class="input-group">
								<span class="input-group-addon">
									<strong>$</strong>
								</span>
								<input type="text" class="form-control">
							</div>
							<i class="blockRight noBorder"></i>
							<i class="blockRightDouble noBorder"></i>
						</div>
						<div class="col-sm-3">
							<div class="form-group withIcon clearfix">
								<div class="input-group">
									<span class="input-group-addon">
										<strong>$</strong>
									</span>
									<input type="text" class="form-control m-b0">
								</div>
								<i class="cb-plus good"></i>
							</div>							
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray">You should configure a price rule in value or percentage for one or several prices ranges.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="montserrat dark-gray text-uc m-t20">Preference</p>
							<label class="cb-radio w-100">
								<input type="radio" name="preference">
								One Digit
							</label>
							<label class="cb-radio w-100">
								<input type="radio" name="preference">
								Two Digits
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray m-t10">Rounding mode to be applied to the price (ie: price will be equal to 10.17 or 10.20).</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row btnShow">
			<div class="col-md-10 col-lg-9">
				<div class="pull-right m-t20">
					<a href="" class="pull-left link p-size m-tr10">
						Save
					</a>
					<button type="button" class="btn btn-save btnNext">Save & Continue</button>                   
				</div>			
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 m-t20">
				<div class="validate yellow">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class=""><span class="bold">Missing API Keys:</span> Missing API connection keys. API connection doesn't exits. To setting API key please go to Configurations > Parameters Tab.</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>