<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">SETTING CONFIGURATION</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Setting configuration</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top m-b0 clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-tb20 m-b20">
					Product Configuration
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-b20 m-b20">
					Payment & Location
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9 col-md-6">
				<p class="poor-gray">
					Postcode :
				</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-sm-4 col-lg-3">
				<p class="montserrat dark-gray text-uc">Merchant credit cards :</p>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Visa / MasterCard
				</label>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Discover
				</label>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					American Express
				</label>
			</div>
			<div class="col-sm-4 col-lg-3">
				<p class="montserrat dark-gray text-uc">Other payment :</p>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Money order / Cashier's check
				</label>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Personal check
				</label>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Pay on pickup
				</label>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Bottom m-t10 p-t0 m-b20">
					<h4 class="headSettings_head p-t10">Returns Policy</h4>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 col-lg-2">
				<p class="poor-gray">
					Returns Policy :
				</p>
				<div class="form-group">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
			<div class="col-sm-3 col-lg-2">
				<p class="poor-gray">
					Returns Within :
				</p>
				<div class="form-group">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
			<div class="col-sm-3 col-lg-2">
				<p class="poor-gray">
					Who pays :
				</p>
				<div class="form-group">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9 col-md-6">
				<div class="form-group">
					<textarea class="form-control" rows="5"></textarea>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Bottom m-t10 p-t0 m-b20">
					<h4 class="headSettings_head p-t10">Listing Duration</h4>	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2  col-md-3 col-lg-2 col-xs-6">
				<p class="poor-gray">
					Listing Duration :
				</p>
				<div class="form-group">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Bottom m-t10 p-t0 m-b20">
					<h4 class="headSettings_head p-t10">Other option</h4>	
				</div>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-sm-6 col-lg-4">
				<p class="montserrat dark-gray text-uc">Merchant credit cards :</p>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Can export products to eBay account if there is no image for the product.
				</label>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Calculate discount price from Feed.biz if there is discount price.
				</label>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Synchronize with products in ebay account. Before export products to eBay account.
				</label>
			</div>
			<div class="col-sm-6 col-lg-4">
				<p class="montserrat dark-gray text-uc">Other payment :</p>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					<p class="m-b0">Can import orders from eBay account.</p>
					<p class="m-l25">Even there is not to export products from Feed.biz to eBay account.</p>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t20 p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Previous
						</a>
						<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
					</div>
				</div>		
			</div>
		</div>
	</div>