<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">STATISTICS</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Users</a></li>
			<li class="bread_item"><a class="bread_link" href="">Affiliation</a></li>	
			<li class="bread_item active"><a class="bread_link" href="">Statistics</a></li>		
		</ul>

		<div id="column"></div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings statistics clearfix">					
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Username
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Email
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Status
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Joined
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											Summary
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12 statistics">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th>Date</th>
									<th>Number of commision bills</th>
									<th>Sales Commision</th>
									<th>Tier Commision</th>
									<th>Payout</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Date:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Number of commision bills:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Sales Commision:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Tier Commision:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payout:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Date:">01326564682</td>
									<td class="tableUserName" data-th="Number of commision bills:">fffffffffedrs</td>
									<td class="tableEmail" data-th="Sales Commision:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Tier Commision:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payout:">Lorem Ipsum</td>
								</tr>
									<tr>
									<td class="tableId" data-th="Date:">01326564682</td>
									<td class="tableUserName" data-th="Number of commision bills:">mn.n.</td>
									<td class="tableEmail" data-th="Sales Commision:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Tier Commision:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payout:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Date:">01326564682</td>
									<td class="tableUserName" data-th="Number of commision bills:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Sales Commision:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Tier Commision:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payout:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Date:">1111111111</td>
									<td class="tableUserName" data-th="Number of commision bills:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Sales Commision:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Tier Commision:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Payout:">aaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="Date:">2222222222222</td>
									<td class="tableUserName" data-th="Number of commision bills:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Sales Commision:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Tier Commision:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Payout:">bbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="Date:">333333333333</td>
									<td class="tableUserName" data-th="Number of commision bills:">cccccccccc</td>
									<td class="tableEmail" data-th="Sales Commision:">ccccccccccc</td>
									<td class="tableStatus" data-th="Tier Commision:">cccccccccccc</td>
									<td class="tableJoined" data-th="Payout:">ccccccccccccccc</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Date:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Sales Commision:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Tier Commision:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payout:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Date:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Sales Commision:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Tier Commision:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Payout:">aaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="Date:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Sales Commision:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Tier Commision:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Payout:">bbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="Date:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Sales Commision:">ccccccccccc</td>
									<td class="tableStatus" data-th="Tier Commision:">cccccccccccc</td>
									<td class="tableJoined" data-th="Payout:">ccccccccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="Date:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Sales Commision:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Tier Commision:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payout:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Date:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Sales Commision:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Tier Commision:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Payout:">aaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="Date:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Sales Commision:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Tier Commision:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Payout:">bbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="Date:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Sales Commision:">ccccccccccc</td>
									<td class="tableStatus" data-th="Tier Commision:">cccccccccccc</td>
									<td class="tableJoined" data-th="Payout:">ccccccccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="Date:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Sales Commision:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Tier Commision:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Payout:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Date:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Payout:">aaaaaa</td>
								</tr>						
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>