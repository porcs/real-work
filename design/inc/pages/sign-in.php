<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
			<div class="signIn">
				<h4 class="montserrat text-center text-uc p-b10">Login to your Account</h4>
				<div class="row">
					<div class="col-sm-6">
						<button class="btn btn-facebook pull-width"><i class="fa fa-facebook"></i>Facebook Account</button>
					</div>
					<div class="col-sm-6">
						<button class="btn btn-google pull-width"><i class="fa fa-google-plus"></i>Sharer on your Google Account</button>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">Email</p>
						<div class="form-group">
							<input type="text" class="form-control">
							<div class="error text-right">
								<p>
									<i class="fa fa-exclamation-circle"></i>
									Please enter email
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">Password</p>
						<div class="form-group">
							<input type="text" class="form-control">
							<div class="error text-right">
								<p>
									<i class="fa fa-exclamation-circle"></i>
									Please enter password
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row custom-form">
					<div class="col-xs-6">
						<label class="cb-checkbox">
							<input type="checkbox"/>
							Remember me
						</label>
					</div>
					<div class="col-xs-6">
						<button class="btn btn-save pull-right">Save</button>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 xs-center">
						<a class="sign_link" href="">Create a new account</a>
					</div>
					<div class="col-sm-6 xs-center sm-right">
						<a class="sign_link" href="" >Forgot Password?</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>