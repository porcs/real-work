<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">My products</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My shop</a></li>
			<li class="bread_item active"><a class="bread_link" href="">My products</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings statistics clearfix">	
					<div class="row m-b30 m-t10">
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-4 col-md-5">
									<p class="m-t10 light-gray">Filter by conditions</p>
								</div>
								<div class="col-sm-8 col-md-7">
									<select class="search-select">
										<option value="">All conditions</option>
										<option value="">New</option>
										<option value="">Used</option>
										<option value="">Refurbished</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-4 col-md-5">
									<p class="m-t10 light-gray">Filter by category</p>
								</div>
								<div class="col-sm-8 col-md-7">
									<select class="search-select">
										<option value="">All categories</option>
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">iPhone5</option>
										<option value="">iPhone5s</option>
										<option value="">iPhone6</option>
										<option value="">Jack and Jones</option>
										<option value="">Addidas</option>
										<option value="">Armani</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 statistics">
						<table class="responsive-table custom-form">
							<thead class="table-head">
								<tr>
									<th class="tableNoSort">#</th>
									<th class="tableNoSort">Product Name > SKU</th>
									<th class="tableNoSort">Variants</th>
									<th class="tableNoSort">Price</th>
									<th class="tableNoSort">Quantity</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableCheckbo" data-th="#: " data-name="">										
										<label class="cb-checkbox">
											<input type="checkbox"/>
										</label>
										<a href="images/profile/enot.jpg" class=" gallery-image">
											<img class="image-table" src="images/profile/enot.jpg" alt="">												
										</a>										
									</td>
									<td data-th="Product Name > SKU:" class="forCheckBo">
										<p class="">Faded Short Sleeve T-shirts (EAN: 0000000001007)</p>
									</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>	
								<tr>
									<td>&nbsp;</td>
									<td data-th="Product Name > SKU:"><i class="fa fa-reply lb"></i> EAN: 0000010000014</td>
									<td data-th="Variants:">Color: Orange | Size: S</td>
									<td data-th="Price:">$1,775.70</td>
									<td data-th="Quantity:">256</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td data-th="Product Name > SKU:"><i class="fa fa-reply lb"></i> EAN: 0000010000014</td>
									<td data-th="Variants:">Color: Orange | Size: S</td>
									<td data-th="Price:">$1,775.70</td>
									<td data-th="Quantity:">256</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td data-th="Product Name > SKU:"><i class="fa fa-reply lb"></i> EAN: 0000010000014</td>
									<td data-th="Variants:">Color: Orange | Size: S</td>
									<td data-th="Price:">$1,775.70</td>
									<td data-th="Quantity:">256</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td data-th="Product Name > SKU:"><i class="fa fa-reply lb"></i> EAN: 0000010000014</td>
									<td data-th="Variants:">Color: Orange | Size: S</td>
									<td data-th="Price:">$1,775.70</td>
									<td data-th="Quantity:">256</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td data-th="Product Name > SKU:"><i class="fa fa-reply lb"></i> EAN: 0000010000014</td>
									<td data-th="Variants:">Color: Orange | Size: S</td>
									<td data-th="Price:">$1,775.70</td>
									<td data-th="Quantity:">256</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td data-th="Product Name > SKU:"><i class="fa fa-reply lb"></i> EAN: 0000010000014</td>
									<td data-th="Variants:">Color: Orange | Size: S</td>
									<td data-th="Price:">$1,775.70</td>
									<td data-th="Quantity:">256</td>
								</tr>
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>