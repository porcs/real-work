<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">SETTING CONFIGURATION</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Setting configuration</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top m-b0 clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-tb20 m-b20">
					Tax Configuration
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p>Formula to be applied on all exported product prices (multiplication, division, addition, subtraction, percentages).</p>
							<p>Apply a specific price formula for eBay selected categories which will override the main setting.</p>
							<p>Rounding mode to be applied to the price (ie: price will be equal to 10.17 or 10.20)</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-4 m-t10">
				<p class="poor-gray">
					Selected Tax :
				</p>
				<div class="form-group">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text b-None m-t10">Rounding</p>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-xs-12">
				<label class="cb-radio">
					<input type="radio" name="radio-group">
					One Digit
				</label>
			</div>
			<div class="col-xs-12">
				<label class="cb-radio">
					<input type="radio" name="radio-group">
					Two digits
				</label>
			</div>
			<div class="col-xs-12">
				<label class="cb-radio">
					<input type="radio" name="radio-group">
					None
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t20 p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Previous
						</a>
						<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
					</div>
				</div>		
			</div>
		</div>
	</div>