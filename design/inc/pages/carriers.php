<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Carriers</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>
			<li class="bread_item"><a class="bread_link" href="">Parameters</a></li>			
			<li class="bread_item"><a class="bread_link" href="">Products</a></li>						
			<li class="bread_item active"><a class="bread_link" href="">Carriers</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-t20">
					<h4 class="headSettings_head">Choose carriers</h4>						
				</div>				
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-xs-12 ">
				<label class="cb-checkbox">
					<input type="checkbox"/>
					Standart Carrier
				</label>
			</div>
			<div class="col-xs-12 ">
				<label class="cb-checkbox">
					<input type="checkbox"/>
					My carrier
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t10 p-t10">
					<button class="btn btn-save">Save</button>
				</div>
			</div>
		</div>
	</div>