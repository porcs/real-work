<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">mappings</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Mappings</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix m-0">
					<img src="flags/France.png" class="flag m-r10" alt="">
					<span class="p-t10">France</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate yellow m-t20">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class="">
								<span class="bold">Missing API Keys:</span> 
								Missing API connection keys. API connection doesn't exits. 
								To setting API key please go to Configurations > Parameters Tab. 
							</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue m-t10">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class="pull-left">
								<span class="bold">No attribute to map:</span>
								In category that you choose, has no attribute to map. 
							</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="p-t20">
					<div class="pull-right">
						<a href="" class="pull-left link p-size m-tr10">
							Save
						</a>
						<button class="btn btn-save btnNext" type="button" >Save & Continue</button>             
					</div>
				</div>		
			</div>
		</div>
	</div>	