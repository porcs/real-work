<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">STATISTICS</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Product statistics</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">Batch Log</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Batch Log
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Product ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Response
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Export date
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											User
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											Type
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="error" checked/>
											Name
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="date" checked/>
											Message
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">Batch Log</th>
									<th class="tableUserName">Product ID</th>
									<th class="tableEmail">Response</th>
									<th class="tableStatus">Export date</th>
									<th class="tableJoined">User</th>
									<th class="tableSummary">Type</th>
									<th class="tableError">Name</th>
									<th class="tableDate">Message</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Response:"><span class="errorText">Failure</span></td>
									<td class="tableStatus" data-th="Export date:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="User:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Type:">Lorem Ipsum</td>
									<td class="tableError" data-th="Name:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Message:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Response:"><span class="errorText">Failure</span></td>
									<td class="tableStatus" data-th="Export date:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="User:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Type:">Lorem Ipsum</td>
									<td class="tableError" data-th="Name:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Message:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Response:"><span class="errorText">Failure</span></td>
									<td class="tableStatus" data-th="Export date:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="User:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Type:">Lorem Ipsum</td>
									<td class="tableError" data-th="Name:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Message:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="Batch Log:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="Product ID:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Response:"><span class="errorText">Failure</span></td>
									<td class="tableStatus" data-th="Export date:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="User:">Lorem Ipsum</td>
									<td class="tableSummary" data-th="Type:">Lorem Ipsum</td>
									<td class="tableError" data-th="Name:">Lorem Ipsum</td>
									<td class="tableDate" data-th="Message:">Lorem Ipsum</td>
								</tr>
							</tbody>
							<tfoot class="table-foot">
                                <tr>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Batch ID">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Product ID">
                                	</th>
                                	
                                	<th>
                                		<input class="form-control" type="text" placeholder="Response">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Export Date" >
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="User">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Type">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Name">
                                	</th>
                                	<th>
                                		<input class="form-control" type="text" placeholder="Message">
                                	</th>
                                </tr>
                            </tfoot>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="b-Top p-t20">
							<div class="pull-right ">
								<a href="" class="pull-left link p-size m-tr10">
									Previous
								</a>
								<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
							</div>
						</div>		
					</div>
				</div>
			</div>			
		</div>
	</div>