<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
			<div class="signIn">
				<h4 class="montserrat text-center text-uc p-b10">Create a new Account</h4>
				<div class="row">
					<div class="col-sm-6">
						<button class="btn btn-facebook pull-width"><i class="fa fa-facebook"></i>Facebook Account</button>
					</div>
					<div class="col-sm-6">
						<button class="btn btn-google pull-width"><i class="fa fa-google-plus"></i>Sharer on your Google Account</button>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">Email</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">Password</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">Confirm password</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<img src="images/captcha/captcha.png" alt="">
					</div>
				</div>
				<div class="row">					
					<div class="col-xs-12">
						<button class="btn btn-save pull-right">Create account</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
						<h4 class="montserrat text-uc p-b10">Support information</h4>					
				</div>
			</div>
			<div class="row dTable">
				<div class="col-xs-12">
					<div class="pull-left">
						<img class="icon-small-circle m-r20" src="images/profile/enot.jpg" alt="">
					</div>
					<p class="poor-gray regRoboto p-b40"> Hello,<br/>
						I'm Edward and I'm now in charge of your support. I'll now be your 
						prefered contact at Feed.biz. You're going to receive an email 
						summarizing the creation of your account and how you can 
						contact me immediately after you confirm your email address.
						Access to your back office will then be activated 
						and you can begin using Feed.biz right away.<br/>
						Thank you,
					</p>												
				</div>
			</div>
		</div>
	</div>
</div>