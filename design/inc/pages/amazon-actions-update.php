<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Actions</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Actions</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix">
					<img src="flags/France.png" class="flag m-r10" alt="">
					<span class="p-t10">France</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#synchronization" data-toggle="tab"><i class="icon-syn"></i>Synchronization</a></li>
					<li><a href="#creation" data-toggle="tab"><i class="icon-creation-up"></i>creation</a></li>
					<li><a href="#orders" data-toggle="tab"><i class="icon-creation-down"></i>inport orders</a></li>
					<li><a href="#products" data-toggle="tab"><i class="icon-garbage"></i>delete products</a></li>								
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="tab-content">
					<div class="tab-pane active" id="synchronization">
						<div class="row">
							<div class="col-sm-10">
								<p class="head_text montserrat black p-t20 b-None">
									Synchronization
								</p>
								<ul class="p-l20">
									<li>
										<p class="dark-gray">Amazon Synchronize Wizard</p>
										<p class="poor-gray">
											This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. 
											After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. 
											The goal is to create your offers on Amazon.
										</p>
									</li>
									<li>
										<p class="dark-gray">Update product offers</p>
										<p class="poor-gray">
											This will synchronize Amazon depending your stocks moves, within the selected categories in your configuration.
										</p>
									</li>
								</ul>
								<div class="clearfix m-t20 xs-center">
									<div class="pull-sm-left">
										<button class="btn btn-wizard"><i class="icon-wizard"></i>Amazon Synchronize Wizard</button>
									</div>
									<div class="pull-sm-right form-group has-update">
										<button class="btn btn-amazon update"><i class="icon-amazon"></i>Update Product offers to Amazon</button>
										<div class="update">
											<p>Starting update</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="creation">
						<div class="row">
							<div class="col-sm-10">
								<p class="head_text montserrat black p-t20 b-None">
									Creation
								</p>
								<ul class="p-l20">
									<li>
										<p class="dark-gray">Amazon Creation Wizard</p>
										<p class="poor-gray">
											This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. 
											After that, the wizard will mark as to be created the unknown products on Amazon but present in your database. 
											The goal is to create the unknown items on Amazon.
										</p>
									</li>
								</ul>
								<div class="clearfix m-t20">
									<button class="btn btn-wizard"><i class="icon-wizard"></i>Amazon Synchronize Wizard</button>									
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="orders">
						<div class="row">
							<div class="col-sm-12">
								<p class="head_text montserrat black p-t20 b-None">
									Import Orders
								</p>
								<ul class="p-l20">
									<li>
										<p class="poor-gray">
											This will import orders from Amazon. 
											For every order you import there will be an order summary and a link to the order in your modules.
										</p>
									</li>
								</ul>
								<p class="head_text montserrat black p-t20 b-None">
									Parameters
								</p>
								<div class="row">
									<div class="col-sm-5 col-lg-6">
										<p class="poor-gray">Order Date Range</p>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group calendar firstDate">
													<input type="text" class="form-control inputDate">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group calendar lastDate">
													<input type="text" class="form-control inputDate">
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<p class="poor-gray">Order Status</p>
										<select class="search-select">
											<option value="">Home</option>
											<option value="">Apple</option>
											<option value="">Iphone 6</option>
											<option value="">Iphone 5</option>
											<option value="">Iphone 4</option>
											<option value="">Iphone 3</option>
											<option value="">Men shirt</option>
											<option value="">Jack &amp; Jones</option>
											<option value="">Adidas</option>
											<option value="">Armani</option>
											<option value="">Dolce &amp; Gabbana</option>
											<option value="">Mersedes</option>
											<option value="">Audi</option>
										</select>
									</div>
									<div class="col-sm-4 col-lg-3 sm-right">
										<button class="btn btn-save m-t25">Import orders from Amazon</button>
									</div>
								</div>
								<p class="head_text montserrat black p-t20 b-None">
									Import Orders
								</p>
								<div class="row">
									<div class="col-sm-8 col-lg-9">
										<ul class="p-l20">
											<li>
												<p class="poor-gray">
													This will import orders from Amazon. 
													For every order you import there will be an order summary and a link to the order in your modules.
												</p>
											</li>
										</ul>
									</div>
									<div class="col-sm-4 col-lg-3 sm-right">
										<button class="btn btn-rule">Confirm order Shipment to Amazon</button>
									</div>
								</div>
							</div>
						</div>						
					</div>
					<div class="tab-pane" id="products">
						<div class="row">
							<div class="col-sm-12">
								<p class="head_text montserrat black p-t20 b-None">
									Delete Products
								</p>
								<ul class="p-l20">
									<li>
										<p class="poor-gray">
											This will send all the products having a profile and within the selected categories in your configuration.
										</p>
									</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<p class="head_text montserrat black p-t20 b-None">
									Parameters
								</p>
							</div>
						</div>
						<div class="row custom-form">
							<div class="col-sm-3">
								<label class="cb-radio">
 									<input type="radio" name="group"/>
 									<p class="poor-gray">Delete out of stock products</p>
 									<p class="m-l25 poor-gray">
 										This will delete only out of stock products on Amazon.
 									</p>
 								</label>
							</div>
							<div class="col-sm-3">
								<label class="cb-radio">
 									<input type="radio" name="group"/>
 									<p class="poor-gray">Delete all sent products</p>
 									<p class="m-l25 poor-gray">
 										This will delete all sent products on Amazon.
 									</p>
 								</label>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<button class="btn btn-save">Delete products from Amazon</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>