<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">CHOOSE YOUR PACKAGES</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Billing</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Billing information</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-b10">
					<ul class="headSettings_point clearfix">
						<li class="headSettings_point-item active">
							<span class="step">1</span>
							<span class="title">Packages</span>
						</li>
						<li class="headSettings_point-item active">
							<span class="step">2</span>
							<span class="title">Information</span>
						</li>
						<li class="headSettings_point-item">
							<span class="step">3</span>
							<span class="title">Complete</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Bottom p-b10">
					<div class="pull-md-left clearfix">
						<h4 class="headSettings_head">Billing Information</h4>						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h5 class="montserrat text-uc dark-gray">Enter the following information</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">First Name *</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter first name
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Last Name *</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter last name
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Address 1 *</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error has-error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter address 1
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Address 2 *</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Zip Code *</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error has-error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter zip code
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">City *</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error has-error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter city
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Floor/Office *</p>
				<div class="form-group">
					<input type="text" class="form-control">					
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">State/Region *</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter state/region
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4 country form-group has-error">
				<p class="regRoboto poor-gray">Country *</p>
				<select class="search-select">
					<option value="">Belarus</option>
					<option value="">Russian Federation</option>
					<option value="">Poland</option>
					<option value="">Chech republic</option>
				</select>
				<div class="error">
					<p>
						<i class="fa fa-exclamation-circle"></i>
						Please enter country
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="pull-right tooSmall regRoboto poor-gray">* fields are required</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="pull-right">
					<a href="" class="link p-size pull-left m-tr10">Previous</a>
					<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>
				</div>				
			</div>
		</div>
	</div>
		


		