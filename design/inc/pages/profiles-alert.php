<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Profiles</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>
			<li class="bread_item"><a class="bread_link" href="">Parameters</a></li>
			<li class="bread_item"><a class="bread_link" href="">Profiles</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Configuration</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top b-Bottom p-t10 p-b10">
					<a href="" class="link showProfile p-size">+ Add a profile to the list</a>
					<p class="pull-right montserrat poor-gray"><span class="dark-gray countProfile">0</span> Profiles</p>
				</div>
			</div>
		</div>
		<div class="row showSelectBlock profiles">
			<div class="col-sm-8 custom-form m-t10">
				<a href="" class="removeProfile">Remove a profile from list</a>
				<div class="profileBlock clearfix">
					<p class="regRoboto poor-gray">Profile Name</p>
					<div class="form-group has-error">
						<input type="text" class="form-control">
						<div class="error text-left">
							<p>
								<i class="fa fa-exclamation-circle"></i>
								Please enter rule profile name first
							</p>
						</div>							
					</div>
					<div class="pull-right">
						<label class="cb-checkbox p-t10 m-r10">
						  <input type="checkbox">
						  Default Profile
						</label>
						<button class="btn btn-rule m-b10 m--t5">+ Add Rule Items(s)</button>
					</div>
				</div>
			</div>
		</div>
		<div class="row btnShow">
			<div class="col-sm-8">
				<button class="btn btn-save m-t10 pull-right">Save</button>
			</div>
		</div>
	</div>