	<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Dashboard</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item active"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Dashboard</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="p-rl40 p-xs-rl20">
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Activities</h4>
					<ul class="list-circle-blue">
						<li>Import Product from "FOXCHIP"</li>
						<li>Export Product to ebay.fr</li>
						<li>Import Product from "FOXCHIP"</li>
						<li>Import Product from "FOXCHIP"</li>					
					</ul>
					<a href="" class="more m-b30">+3 more</a>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Best Categories</h4>
					<ul class="list-circle-unstyled m-b30">
						<li>
							<div id="miniFirst"></div>
							<p>Goodies (FOXCHIP)</p>
							<p><span>5,538</span></p>
						</li>
						<li>
							<div id="miniSecond"></div>
							<p>Jeux-Video (FOXCHIP)</p>
							<p><span>966</span></p>
						</li>
						<li>
							<div id="miniThird"></div>
							<p>J Telephone/Tablette (FOX)</p>
							<p><span>464</span></p>
						</li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Configuration Checklist</h4>
					<ul class="list-circle-green m-b30">
						<li>Data source completed.</li>
						<li>Market place configuration complete.</li>
						<li>Category complete.</li>				
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Errors - Last Entries</h4>
					<ul class="list-circle-red m-b30">
						<li>Out of stock</li>
						<li>Product are Inactive</li>		
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="mainChart p-xs-rl20">
					<div class="row">
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total products</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="product"></div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total orders</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="order"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<div class="p-rl40 p-xs-rl20">
					<div class="chart lineChart">
						<h4 class="text-uc dark-gray bold m-t20">Total sale</h4>
						<ul class="tabs">
							<li class="active">30 days</li>
							<li>7 Days</li>
							<li>1 Day</li>
						</ul>
						<div id="sale"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row p-rl40 p-xs-rl20">
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/amazon.png" alt="">
					<a class="pay_link" href="" data-toggle="modal" data-target="#amazonConfigurate">
						Configure Amazon
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/ebay.png" alt="">
					<a class="pay_link" href="" data-toggle="modal" data-target="#ebayConfigurate">
						Configure Ebay
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/logo_count.png" alt="">
					<ul class="pay_list">
						<li class="pay_item">Days</li>
						<li class="pay_item">Hours</li>
						<li class="pay_item">Minutes</li>
					</ul>
					<div id="countdown"></div>
					<div id="note"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="support dSTable">
					<div class="dRow">
						<img src="images/icons/enot.jpg" alt="" class="dCell icon-circle icon-user support_picture">							
						<div class="dCell">
							<p class="tooLight-gray">Your support</p>
							<p class="dark-gray h1 m-t0">Zoye</p>
							<a href="" class="link regRoboto h4">praew@common-services.com</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="amazonConfigurate">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="lightRoboto text-uc head-size light-gray">Amazon wizard</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix m-b10">
								<ul class="headSettings_point clearfix">
									<li class="headSettings_point-item active">
										<span class="step">1</span>
										<span class="title">Choose Amazon site</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">2</span>
										<span class="title">Config API setting</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">3</span>
										<span class="title">Choose category</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">4</span>
										<span class="title">Sync & match</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">5</span>
										<span class="title">Send to Amazon</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Bottom p-b10">
								<div class="pull-md-left clearfix">
									<h4 class="headSettings_head">Amazon site</h4>						
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="validate blue m-t10">
								<div class="validateRow">
									<div class="validateCell">
										<i class="note"></i>
									</div>
									<div class="validateCell">
										<ul>
											<li>
												<p>
													This wizard will download your inventory from Amazon. 
													The inventory will be compared to your local inventory.
													After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. 
													The goal is to create your offers on Amazon.
												</p>
											</li>
											<li>
												<p>
													This operation is automatic but could take one hour, please be patient and wait till the process is completed.
												</p>
											</li>
										</ul>
										<i class="fa fa-remove pull-right"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<ul class="selectWizard clearfix">
								<li class="selectWizard_point">
									<img src="flags/Belarus.png" alt="">
									<p class="poor-gray">Belarus</p>
								</li>
								<li class="selectWizard_point">
									<img src="flags/Belarus.png" alt="">
									<p class="poor-gray">Belarus</p>
								</li>
								<li class="selectWizard_point">
									<img src="flags/Belarus.png" alt="">
									<p class="poor-gray">Belarus</p>
								</li>
								<li class="selectWizard_point">
									<img src="flags/Belarus.png" alt="">
									<p class="poor-gray">Belarus</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="ebayConfigurate">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="lightRoboto text-uc head-size light-gray">Ebay wizard</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix m-b10">
								<ul class="headSettings_point clearfix">
									<li class="headSettings_point-item active">
										<span class="step">1</span>
										<span class="title">Authorization</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">2</span>
										<span class="title">Choose Site</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">3</span>
										<span class="title">Shippings</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">4</span>
										<span class="title">Tax</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">5</span>
										<span class="title">Select Univers</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">6</span>
										<span class="title">Categories</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">7</span>
										<span class="title">Export Products</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">8</span>
										<span class="title">Finish</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Bottom p-b10">
								<div class="learfix">
									<h4 class="headSettings_head">Wizard Export Products</h4>						
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<div class="validate blue m-t20 m-b0">
								<div class="validateRow">
									<div class="validateCell">
										<i class="note"></i>
									</div>
									<div class="validateCell">
										<p>
											If you are an newbie user, I would recommend you sign up for the ebay account. 
											This wizard will cover the basic concepts of the eBay Platform and get you familiarized with some of our popular program. 
											At the end of the wizard, you will can export product to your ebay account.
										</p>
										<i class="fa fa-remove pull-right"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<p class="poor-gray m-t20">
								 Send to eBay Account
							</p>
							<button class="btn btn-success">Send products to eBay</button>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top m-t20 p-t20">
								<div class="pull-right">
									<a href="" class="pull-left link p-size m-tr10">
										Reset
									</a>
									<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
								</div>
							</div>		
						</div>
					</div>													
				</div>
			</div>
		</div>
	</div>