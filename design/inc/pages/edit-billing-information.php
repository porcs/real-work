<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Edit Profile</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Users</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Edit Billing Information</a></li>			
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">Information</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Company Name *</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">First Name *</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Last Name *</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="pull-right tooSmall regRoboto poor-gray">* fields are required</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">Address</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Address 1 *</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Address 2</p>
				<div class="form-group">
					<input type="text" class="form-control">					
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">State/Region *</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Zip Code *</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">City *</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4 country form-group">
				<p class="regRoboto poor-gray">Country *</p>
				<select class="search-select">
					<option value="">Belarus</option>
					<option value="">Russian Federation</option>
					<option value="">Poland</option>
					<option value="">Chech republic</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="pull-right tooSmall regRoboto poor-gray">* fields are required</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<button class="btn btn-save pull-right m-t20">Save</button>
			</div>
		</div>
	</div> 