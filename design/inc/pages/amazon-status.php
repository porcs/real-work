<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Status</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>				
			<li class="bread_item active"><a class="bread_link" href="">Status</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t10 clearfix">
					<p class="head_text">
						<img src="flags/France.png" class="flag m-r10" alt="">
						<span class="p-t10">France</span>					
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 m-t10">
				<div class="validate yellow">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class=""><span class="bold">Missing API Keys:</span> Missing API connection keys. API connection doesn't exits. To setting API key please go to Configurations > Parameters Tab.</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 m-t10">
				<div class="validate blue">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class="">Cron tasks are used to automate certain tasks, for example update your stock levels on Amazon every hours.</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings p-t10 b-Bottom m-t10">
					<h4 class="headSettings_head">Scheduled Tasks</h4>
				</div>
			</div>			
		</div>
		<div class="row custom-form m-t20">
			<div class="col-sm-6">
				<p class="montserrat dark-gray text-uc">
					Synchronization
				</p>				
				<label class="cb-checkbox w-100">
					<input type="checkbox"/>
					<p class="text-uc dark-gray montserrat m-b0">Allow automatic update offer.</p>
					<p class="m-l23">Automatic update your stock levels on Amazon every hours</p>
				</label>
			</div>
			<div class="col-sm-6">
				<p class="montserrat dark-gray text-uc">
					Orders
				</p>	
				<label class="cb-checkbox w-100">
					<input type="checkbox"/>
					<p class="text-uc dark-gray montserrat m-b0">Allow automatic export orders to shop.</p>
					<p class="m-l23">Automatic import your orders every hours</p>
				</label>
				<label class="cb-checkbox w-100">
					<input type="checkbox"/>
					<p class="text-uc dark-gray montserrat m-b0">Allow send statuses to Amazon.</p>
					<p class="m-l23">Automatic update your orders status on Amazon every hours</p>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t10 p-t20">
					<div class="pull-right">
						<button class="btn btn-save">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>