<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">MAPPING CATEGORIES</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>
			<li class="bread_item"><a class="bread_link" href="">Mapping</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Categories</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top m-b0 clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Top-None b-Bottom p-0">
					<h4 class="headSettings_head pull-sm-left p-t10">Choose categories</h4>										
					<ul class="pull-sm-right clearfix treeSettings">
						<li class="expandAll"><i class="cb-plus"></i>Expand all</li>
						<li class="collapseAll"><i class="cb-minus"></i>Collapse all</li>
						<li class="checkedAll"><i class="cb-checked"></i>Check all</li>
						<li class="uncheckedAll"><i class="cb-unchecked"></i>Uncheck all</li>
						<li class="link"><i class="cb-store"></i>Get Store Categories</li>						
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate pink m-t20">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<div class="pull-left">
								<p>
									Update Categories Selected Failed : Please, try again.
								</p>								
							</div>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue m-t15">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<div class="pull-left">
								<p>
									You can select multiple categories at once by checking the first category then pressing 'Shift' and clicking on the last element you wish to include in the same profile.
								</p>
								<ol>
									<li>
										<p>Primary eBay Category - This is the main category where Buyers will find your item on eBay (not in your store, but on general eBay shopping).</p>
									</li>
									<li>
										<p>Secondary eBay Category (optional) - This field is optional.</p>
										<p>If you add a secondary category to a listing, you may later change it to a different one, but you will not be able to remove the use of a secondary category entirely.</p>
									</li>
									<li>
										<p>Primary eBay Store Category (optional) - if you have imported Store Categories, you will also be able to choose eBay Store categories when doing your Product Mapping.</p>
										<p>Store Categories are optional and will only apply if you have an eBay Store and have imported them for use in Feed.biz.</p>
									</li>
									<li>
										<p>Secondary eBay Store Category (optional) - you may choose a second category from your eBay Store category structure as well, if desired.</p>
									</li>
								</ol>
							</div>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#primary" data-toggle="tab">Primary Categories</a></li>
					<li><a href="#secondary" data-toggle="tab">secondary categories</a></li>
					<li><a href="#prim_store" data-toggle="tab">Primary Store Categories</a></li>	
					<li><a href="#sec_store" data-toggle="tab">secondary Store Categories</a></li>							
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane active" id="primary">
				<div class="tree clearfix custom-form">	
					<i class="icon-folder"></i>
					<p class="treeHead treeRight">Choose eBay Categories</p>
					<ul class="tree_point">
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>									
									<label class="cb-checkbox ">											
										<input type="checkbox"/>
										women
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
							<ul class="tree_child">
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												dresses
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
									<ul class="tree_child">
										<li>
											<span class="tree_item">
												<p>
													<i class="cb-plus good"></i>
													<label class="cb-checkbox">
														<input type="checkbox"/>
														small
													</label>
												</p>
											</span>
											<div class="treeRight selectTree">
												<div class="tree_show">
													<select class="search-select">
														<option value="">Home</option>
														<option value="">Apple</option>
														<option value="">Iphone 6</option>
														<option value="">Iphone 5</option>
														<option value="">Iphone 4</option>
														<option value="">Iphone 3</option>
														<option value="">Men shirt</option>
														<option value="">Jack &amp; Jones</option>
														<option value="">Adidas</option>
														<option value="">Armani</option>
														<option value="">Dolce &amp; Gabbana</option>
														<option value="">Mersedes</option>
														<option value="">Audi</option>
													</select>
													<i class="icon-more"></i>
												</div>
											</div>
										</li>
										<li>
											<span class="tree_item">
												<p>
													<i class="cb-plus good"></i>
													<label class="cb-checkbox">
														<input type="checkbox"/>
														big
													</label>
												</p>
											</span>
											<div class="treeRight selectTree">
												<div class="tree_show">
													<select class="search-select">
														<option value="">Home</option>
														<option value="">Apple</option>
														<option value="">Iphone 6</option>
														<option value="">Iphone 5</option>
														<option value="">Iphone 4</option>
														<option value="">Iphone 3</option>
														<option value="">Men shirt</option>
														<option value="">Jack &amp; Jones</option>
														<option value="">Adidas</option>
														<option value="">Armani</option>
														<option value="">Dolce &amp; Gabbana</option>
														<option value="">Mersedes</option>
														<option value="">Audi</option>
													</select>
													<i class="icon-more"></i>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												blouses
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>
									<label class="cb-checkbox">
										<input type="checkbox"/>
										men
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
						</li>
					</ul>
				</div>				
			</div>
			<div class="tab-pane" id="secondary">
				<div class="tree clearfix custom-form">	
					<i class="icon-folder"></i>
					<p class="treeHead treeRight">Choose eBay Categories</p>
					<ul class="tree_point">
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>									
									<label class="cb-checkbox ">											
										<input type="checkbox"/>
										women
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
							<ul class="tree_child">
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												dresses
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
									<ul class="tree_child">
										<li>
											<span class="tree_item">
												<p>
													<i class="cb-plus good"></i>
													<label class="cb-checkbox">
														<input type="checkbox"/>
														small
													</label>
												</p>
											</span>
											<div class="treeRight selectTree">
												<div class="tree_show">
													<select class="search-select">
														<option value="">Home</option>
														<option value="">Apple</option>
														<option value="">Iphone 6</option>
														<option value="">Iphone 5</option>
														<option value="">Iphone 4</option>
														<option value="">Iphone 3</option>
														<option value="">Men shirt</option>
														<option value="">Jack &amp; Jones</option>
														<option value="">Adidas</option>
														<option value="">Armani</option>
														<option value="">Dolce &amp; Gabbana</option>
														<option value="">Mersedes</option>
														<option value="">Audi</option>
													</select>
													<i class="icon-more"></i>
												</div>
											</div>
										</li>
										<li>
											<span class="tree_item">
												<p>
													<i class="cb-plus good"></i>
													<label class="cb-checkbox">
														<input type="checkbox"/>
														big
													</label>
												</p>
											</span>
											<div class="treeRight selectTree">
												<div class="tree_show">
													<select class="search-select">
														<option value="">Home</option>
														<option value="">Apple</option>
														<option value="">Iphone 6</option>
														<option value="">Iphone 5</option>
														<option value="">Iphone 4</option>
														<option value="">Iphone 3</option>
														<option value="">Men shirt</option>
														<option value="">Jack &amp; Jones</option>
														<option value="">Adidas</option>
														<option value="">Armani</option>
														<option value="">Dolce &amp; Gabbana</option>
														<option value="">Mersedes</option>
														<option value="">Audi</option>
													</select>
													<i class="icon-more"></i>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												blouses
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>
									<label class="cb-checkbox">
										<input type="checkbox"/>
										men
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="tab-pane" id="prim_store">
				<div class="tree clearfix custom-form">	
					<i class="icon-folder"></i>
					<p class="treeHead treeRight">Choose eBay Categories</p>
					<ul class="tree_point">
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>									
									<label class="cb-checkbox ">											
										<input type="checkbox"/>
										women
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
							<ul class="tree_child">
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												dresses
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
									<ul class="tree_child">
										<li>
											<span class="tree_item">
												<p>
													<i class="cb-plus good"></i>
													<label class="cb-checkbox">
														<input type="checkbox"/>
														small
													</label>
												</p>
											</span>
											<div class="treeRight selectTree">
												<div class="tree_show">
													<select class="search-select">
														<option value="">Home</option>
														<option value="">Apple</option>
														<option value="">Iphone 6</option>
														<option value="">Iphone 5</option>
														<option value="">Iphone 4</option>
														<option value="">Iphone 3</option>
														<option value="">Men shirt</option>
														<option value="">Jack &amp; Jones</option>
														<option value="">Adidas</option>
														<option value="">Armani</option>
														<option value="">Dolce &amp; Gabbana</option>
														<option value="">Mersedes</option>
														<option value="">Audi</option>
													</select>
													<i class="icon-more"></i>
												</div>
											</div>
										</li>
										<li>
											<span class="tree_item">
												<p>
													<i class="cb-plus good"></i>
													<label class="cb-checkbox">
														<input type="checkbox"/>
														big
													</label>
												</p>
											</span>
											<div class="treeRight selectTree">
												<div class="tree_show">
													<select class="search-select">
														<option value="">Home</option>
														<option value="">Apple</option>
														<option value="">Iphone 6</option>
														<option value="">Iphone 5</option>
														<option value="">Iphone 4</option>
														<option value="">Iphone 3</option>
														<option value="">Men shirt</option>
														<option value="">Jack &amp; Jones</option>
														<option value="">Adidas</option>
														<option value="">Armani</option>
														<option value="">Dolce &amp; Gabbana</option>
														<option value="">Mersedes</option>
														<option value="">Audi</option>
													</select>
													<i class="icon-more"></i>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												blouses
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>
									<label class="cb-checkbox">
										<input type="checkbox"/>
										men
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="tab-pane" id="sec_store">
				<div class="tree clearfix custom-form">	
					<i class="icon-folder"></i>
					<p class="treeHead treeRight">Choose eBay Categories</p>
					<ul class="tree_point">
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>									
									<label class="cb-checkbox ">											
										<input type="checkbox"/>
										women
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
							<ul class="tree_child">
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												dresses
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
									<ul class="tree_child">
										<li>
											<span class="tree_item">
												<p>
													<i class="cb-plus good"></i>
													<label class="cb-checkbox">
														<input type="checkbox"/>
														small
													</label>
												</p>
											</span>
											<div class="treeRight selectTree">
												<div class="tree_show">
													<select class="search-select">
														<option value="">Home</option>
														<option value="">Apple</option>
														<option value="">Iphone 6</option>
														<option value="">Iphone 5</option>
														<option value="">Iphone 4</option>
														<option value="">Iphone 3</option>
														<option value="">Men shirt</option>
														<option value="">Jack &amp; Jones</option>
														<option value="">Adidas</option>
														<option value="">Armani</option>
														<option value="">Dolce &amp; Gabbana</option>
														<option value="">Mersedes</option>
														<option value="">Audi</option>
													</select>
													<i class="icon-more"></i>
												</div>
											</div>
										</li>
										<li>
											<span class="tree_item">
												<p>
													<i class="cb-plus good"></i>
													<label class="cb-checkbox">
														<input type="checkbox"/>
														big
													</label>
												</p>
											</span>
											<div class="treeRight selectTree">
												<div class="tree_show">
													<select class="search-select">
														<option value="">Home</option>
														<option value="">Apple</option>
														<option value="">Iphone 6</option>
														<option value="">Iphone 5</option>
														<option value="">Iphone 4</option>
														<option value="">Iphone 3</option>
														<option value="">Men shirt</option>
														<option value="">Jack &amp; Jones</option>
														<option value="">Adidas</option>
														<option value="">Armani</option>
														<option value="">Dolce &amp; Gabbana</option>
														<option value="">Mersedes</option>
														<option value="">Audi</option>
													</select>
													<i class="icon-more"></i>
												</div>
											</div>
										</li>
									</ul>
								</li>
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												blouses
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>
									<label class="cb-checkbox">
										<input type="checkbox"/>
										men
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">			
			<div class="col-xs-12">
				<div class="b-Top m-t20 p-t20">
					<div class="pull-right">
						<a href="" class="link p-size pull-left m-tr10">Save</a>
						<button class="btn btn-save">Save & Continue</button>
					</div>
				</div>
			</div>
		</div>
	</div>