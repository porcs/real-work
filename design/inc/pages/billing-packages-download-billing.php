	<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Download billing</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Billing</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Billing information</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings statistics clearfix">					
					<div class="pull-md-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">Show / Hide Columns</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											Billing ID
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											Billing detail
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											Date / Time
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											Payment Method
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											Print
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12 statistics">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th>Billing ID</th>
									<th>Billing detail</th>
									<th>Date / Time</th>
									<th>Payment Method</th>
									<th>Print</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tableId" data-th="ID:" data-name="">01326564682</td>
									<td class="tableUserName" data-th="User Name:">hmkhmhmhm</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>									
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">fffffffffedrs</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">mn.n.</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								</tr>	
								<tr>
									<td class="tableId" data-th="ID:">01326564682</td>
									<td class="tableUserName" data-th="User Name:">Ali Nickerman</td>
									<td class="tableEmail" data-th="Email:">Ali Nickerman@gmail.com</td>
									<td class="tableStatus" data-th="Status:">Lorem Ipsum</td>
									<td class="tableJoined" data-th="Joined:">Lorem Ipsum</td>
								</tr>
								<tr>
									<td class="tableId" data-th="ID:">1111111111</td>
									<td class="tableUserName" data-th="User Name:">aaaaaaaa</td>
									<td class="tableEmail" data-th="Email:">aaaaaaaaaa</td>
									<td class="tableStatus" data-th="Status:">aaaaaaaa</td>
									<td class="tableJoined" data-th="Joined:">aaaaaa</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">2222222222222</td>
									<td class="tableUserName" data-th="User Name:">bbbbbbbbbbb</td>
									<td class="tableEmail" data-th="Email:">bbbbbbbbb</td>
									<td class="tableStatus" data-th="Status:">bbbbbbbb</td>
									<td class="tableJoined" data-th="Joined:">bbbbbbbbb</td>
								</tr>
									<tr>
									<td class="tableId" data-th="ID:">333333333333</td>
									<td class="tableUserName" data-th="User Name:">cccccccccc</td>
									<td class="tableEmail" data-th="Email:">ccccccccccc</td>
									<td class="tableStatus" data-th="Status:">cccccccccccc</td>
									<td class="tableJoined" data-th="Joined:">ccccccccccccccc</td>
								</tr>								
							</tbody>
						</table>
						<table class="table tableEmpty">
							<tr>
								<td>No data available in table</td>
							</tr>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>