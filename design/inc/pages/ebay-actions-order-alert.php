<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Actions</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Actions</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t20 p-b20">
					Product Actions
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p>This will send all products with a profile within the selected categories in your module configuration.</p>
							<p>If you want to add a product to the list, please go to the mapping and setting configuration in the eBay options tab.</p>
							<p>Then, do not forget to generate and save a report (For support you will need the XML and the Report)</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix m-t20 actions">
			<div class="pull-left">
				<p class="poor-gray">Send to eBay Account</p>
				<button class="btn btn-send m-b15">Send products to eBay</button>
				<button class="btn btn-send m-r10 m-b15">Send offers to eBay</button>
			</div>
			<div class="pull-left">
				<p class="poor-gray m-r10">Import order from eBay Account</p>
				<button class="btn btn-save">Get orders from eBay</button>
				<p class="true-pink m-b0"><i class="fa fa-exclamation-circle m-r5"></i>Get orders failed: Please, try again.</p>
			</div>
			<div class="pull-left">
				<p class="poor-gray">Delete on eBay Account</p>
				<button class="btn btn-error m-b15">Delete products on eBay</button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t20 p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Previous
						</a>
						<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
					</div>
				</div>		
			</div>
		</div>
	</div>
		