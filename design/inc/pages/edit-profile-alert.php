<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Edit Billing Information</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Users</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Edit Billing Information</a></li>			
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">Image</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 dSTable">
				<div class="profile dRow">
					<div class="profile dRow">
						<div class="profile_picture dCell">
							<img class="icon-big-circle" src="images/profile/enot.jpg" alt="">						
						</div>
						<div class="dCell p-l20">
							<p class="profile_text">Olivier Baquet</p>
							<a href="" class="profile_link">Remove Image</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-tb20 p-b10 b-Bottom">
					<h4 class="headSettings_head">Basic info</h4>						
				</div>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Email</p>
				<div class="form-group">
					<input type="text" class="form-control" disabled>
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">First Name</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter first name
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Last Name</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter last name
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-sm-6">
				<label class="cb-checkbox">
					<input type="checkbox"/>
					Join our newsletter.
				</label>
				<p class="m-l25 poor-gray p-size regRoboto">Read Feedbiz's <a href="" class="link">terms and conditions</a></p>
			</div>
			<div class="col-sm-6">
				<button class="btn btn-save pull-right">Save</button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">Password</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Current Password</p>
				<div class="form-group">
					<input type="text" class="form-control">					
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">New password</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter new password
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">Confirm password</p>
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<div class="error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							Please enter confirm password
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<button class="btn btn-save pull-right m-t20">Save</button>
			</div>
		</div>
	</div>