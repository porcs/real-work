<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Category</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">My feeds</a></li>
			<li class="bread_item"><a class="bread_link" href="">Parameters</a></li>			
			<li class="bread_item"><a class="bread_link" href="">Products</a></li>						
			<li class="bread_item active"><a class="bread_link" href="">Category</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Bottom m-t10 p-0">
					<h4 class="headSettings_head pull-sm-left p-t10">Choose categories</h4>										
					<ul class="pull-sm-right clearfix treeSettings">
						<li class="expandAll"><i class="cb-plus"></i>Expand all</li>
						<li class="collapseAll"><i class="cb-minus"></i>Collapse all</li>
						<li class="checkedAll"><i class="cb-checked"></i>Check all</li>
						<li class="uncheckedAll"><i class="cb-unchecked"></i>Uncheck all</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="tree clearfix custom-form">	
			<i class="icon-folder"></i>
			<p class="treeHead treeRight">Profiles Mapping</p>
			<ul class="tree_point">
				<li>
					<span class="tree_item">
						<p>
							<i class="cb-plus good"></i>									
							<label class="cb-checkbox ">											
								<input type="checkbox"/>
								women
							</label>
						</p>
					</span>
					<div class="treeRight selectTree">
						<div class="tree_show">
							<select class="search-select">
								<option value="">Home</option>
								<option value="">Apple</option>
								<option value="">Iphone 6</option>
								<option value="">Iphone 5</option>
								<option value="">Iphone 4</option>
								<option value="">Iphone 3</option>
								<option value="">Men shirt</option>
								<option value="">Jack &amp; Jones</option>
								<option value="">Adidas</option>
								<option value="">Armani</option>
								<option value="">Dolce &amp; Gabbana</option>
								<option value="">Mersedes</option>
								<option value="">Audi</option>
							</select>
							<i class="icon-more"></i>
						</div>
					</div>
					<ul class="tree_child">
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>
									<label class="cb-checkbox">
										<input type="checkbox"/>
										dresses
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
							<ul class="tree_child">
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												small
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
								</li>
								<li>
									<span class="tree_item">
										<p>
											<i class="cb-plus good"></i>
											<label class="cb-checkbox">
												<input type="checkbox"/>
												big
											</label>
										</p>
									</span>
									<div class="treeRight selectTree">
										<div class="tree_show">
											<select class="search-select">
												<option value="">Home</option>
												<option value="">Apple</option>
												<option value="">Iphone 6</option>
												<option value="">Iphone 5</option>
												<option value="">Iphone 4</option>
												<option value="">Iphone 3</option>
												<option value="">Men shirt</option>
												<option value="">Jack &amp; Jones</option>
												<option value="">Adidas</option>
												<option value="">Armani</option>
												<option value="">Dolce &amp; Gabbana</option>
												<option value="">Mersedes</option>
												<option value="">Audi</option>
											</select>
											<i class="icon-more"></i>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<span class="tree_item">
								<p>
									<i class="cb-plus good"></i>
									<label class="cb-checkbox">
										<input type="checkbox"/>
										blouses
									</label>
								</p>
							</span>
							<div class="treeRight selectTree">
								<div class="tree_show">
									<select class="search-select">
										<option value="">Home</option>
										<option value="">Apple</option>
										<option value="">Iphone 6</option>
										<option value="">Iphone 5</option>
										<option value="">Iphone 4</option>
										<option value="">Iphone 3</option>
										<option value="">Men shirt</option>
										<option value="">Jack &amp; Jones</option>
										<option value="">Adidas</option>
										<option value="">Armani</option>
										<option value="">Dolce &amp; Gabbana</option>
										<option value="">Mersedes</option>
										<option value="">Audi</option>
									</select>
									<i class="icon-more"></i>
								</div>
							</div>
						</li>
					</ul>
				</li>
				<li>
					<span class="tree_item">
						<p>
							<i class="cb-plus good"></i>
							<label class="cb-checkbox">
								<input type="checkbox"/>
								men
							</label>
						</p>
					</span>
					<div class="treeRight selectTree">
						<div class="tree_show">
							<select class="search-select">
								<option value="">Home</option>
								<option value="">Apple</option>
								<option value="">Iphone 6</option>
								<option value="">Iphone 5</option>
								<option value="">Iphone 4</option>
								<option value="">Iphone 3</option>
								<option value="">Men shirt</option>
								<option value="">Jack &amp; Jones</option>
								<option value="">Adidas</option>
								<option value="">Armani</option>
								<option value="">Dolce &amp; Gabbana</option>
								<option value="">Mersedes</option>
								<option value="">Audi</option>
							</select>
							<i class="icon-more"></i>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>