<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">MAPPINGS</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>
			<li class="bread_item active"><a class="bread_link" href="">Mapping</a></li>			
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 p-b20">
					Shoes
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 p-b20 m-b20">
					Size
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">Color</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="p-t8 xs-center">
					<i class="cb-color pink"></i>
					<p class="poor-gray m-t3">					
						Pink
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="p-t8 xs-center">
					<i class="cb-color black"></i>
					<p class="poor-gray m-t3">					
						Black
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="p-t8 xs-center">
					<i class="cb-color yellow"></i>
					<p class="poor-gray m-t3">					
						Yellow
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="p-t8 xs-center">
					<i class="cb-color green"></i>
					<p class="poor-gray m-t3">					
						Green
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="p-t8 xs-center">
					<i class="cb-color blue"></i>
					<p class="poor-gray m-t3">					
						Blue
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">Collection namer</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">Hell type</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">Inner material</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">outer material type</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">shoe closure type</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b20">
					<h4 class="headSettings_head">department</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack &amp; Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce &amp; Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group clearfix">
					<select class="search-select">
						<option value="">Home</option>
						<option value="">Apple</option>
						<option value="">Iphone 6</option>
						<option value="">Iphone 5</option>
						<option value="">Iphone 4</option>
						<option value="">Iphone 3</option>
						<option value="">Men shirt</option>
						<option value="">Jack &amp; Jones</option>
						<option value="">Adidas</option>
						<option value="">Armani</option>
						<option value="">Dolce &amp; Gabbana</option>
						<option value="">Mersedes</option>
						<option value="">Audi</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">			
			<div class="col-xs-12">
				<div class="b-Top p-t20">
					<div class="pull-right">
						<a href="" class="link p-size pull-left m-tr10">Save</a>
						<button class="btn btn-save">Save & Continue</button>
					</div>
				</div>
			</div>
		</div>
	</div>