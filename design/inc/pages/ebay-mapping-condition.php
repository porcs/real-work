<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">MAPPING conditions</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Ebay</a></li>
			<li class="bread_item"><a class="bread_link" href="">Mapping</a></li>					
			<li class="bread_item active"><a class="bread_link" href="">Selected condition</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-t10 b-Top m-b0 clearfix">
					<img src="flags/Belarus.png" class="flag m-r10" alt="">
					<span class="p-t10">Belarus</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-tb20 m-b20">
					Choose conditions
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="New">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="New other (see details)">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="New with defects">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="Manufacturer refurbished">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="Seller refurbished">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="Used">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="Very good">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="Good">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="Acceptable">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" disabled placeholder="For parts or not working">
						</div>
						<i class="blockRight noBorder"></i>
					</div>
					<div class="col-sm-6 m-b20">
						<select class="search-select">
							<option value="">Home</option>
							<option value="">Apple</option>
							<option value="">Iphone 6</option>
							<option value="">Iphone 5</option>
							<option value="">Iphone 4</option>
							<option value="">Iphone 3</option>
							<option value="">Men shirt</option>
							<option value="">Jack &amp; Jones</option>
							<option value="">Adidas</option>
							<option value="">Armani</option>
							<option value="">Dolce &amp; Gabbana</option>
							<option value="">Mersedes</option>
							<option value="">Audi</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10">
							Previous
						</a>
						<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
					</div>
				</div>		
			</div>
		</div>
	</div>