	<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Dashboard</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item active"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Dashboard</a></li>
		</ul>
	</div>
	<div class="content">
		<div class="p-rl40 p-xs-rl20">
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Activities</h4>
					<ul class="list-circle-blue">
						<li>Import Product from "FOXCHIP"</li>
						<li>Export Product to ebay.fr</li>
						<li>Import Product from "FOXCHIP"</li>
						<li>Import Product from "FOXCHIP"</li>					
					</ul>
					<a href="" class="more m-b30">+3 more</a>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Best Categories</h4>
					<ul class="list-circle-unstyled m-b30">
						<li>
							<div id="miniFirst"></div>
							<p>Goodies (FOXCHIP)</p>
							<p><span>5,538</span></p>
						</li>
						<li>
							<div id="miniSecond"></div>
							<p>Jeux-Video (FOXCHIP)</p>
							<p><span>966</span></p>
						</li>
						<li>
							<div id="miniThird"></div>
							<p>J Telephone/Tablette (FOX)</p>
							<p><span>464</span></p>
						</li>
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Configuration Checklist</h4>
					<ul class="list-circle-green m-b30">
						<li>Data source completed.</li>
						<li>Market place configuration complete.</li>
						<li>Category complete.</li>				
					</ul>
				</div>
				<div class="col-lg-3 col-sm-6">
					<h4 class="text-uc dark-gray bold">Errors - Last Entries</h4>
					<ul class="list-circle-red m-b30">
						<li>Out of stock</li>
						<li>Product are Inactive</li>		
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="mainChart p-xs-rl20">
					<div class="row">
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total products</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="product"></div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="chart">
								<h4 class="text-uc dark-gray bold m-t20">Total orders</h4>
								<ul class="tabs">
									<li class="active">30 days</li>
									<li>7 Days</li>
									<li>1 Day</li>
								</ul>
								<div id="order"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<div class="p-rl40 p-xs-rl20">
					<div class="chart lineChart">
						<h4 class="text-uc dark-gray bold m-t20">Total sale</h4>
						<ul class="tabs">
							<li class="active">30 days</li>
							<li>7 Days</li>
							<li>1 Day</li>
						</ul>
						<div id="sale"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row p-rl40 p-xs-rl20">
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/amazon.png" alt="">
					<a class="pay_link" href="" >
						Configure Amazon
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/pay/ebay.png" alt="">
					<a class="pay_link" href="" data-toggle="modal" data-target="#ebayConfigurate">
						Configure Ebay
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="pay">
					<img class="pay_picture" src="images/logo_count.png" alt="">
					<ul class="pay_list">
						<li class="pay_item">Days</li>
						<li class="pay_item">Hours</li>
						<li class="pay_item">Minutes</li>
					</ul>
					<div id="countdown"></div>
					<div id="note"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="support dSTable">
					<div class="dRow">
						<img src="images/icons/enot.jpg" alt="" class="dCell icon-circle icon-user support_picture">							
						<div class="dCell">
							<p class="tooLight-gray">Your support</p>
							<p class="dark-gray h1 m-t0">Zoye</p>
							<a href="" class="link regRoboto h4">praew@common-services.com</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	

	<div class="modal fade" id="ebayConfigurate">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<h1 class="lightRoboto text-uc head-size light-gray">Ebay wizard</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix m-b10">
								<ul class="headSettings_point clearfix">
									<li class="headSettings_point-item active">
										<span class="step">1</span>
										<span class="title">Authorization</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">2</span>
										<span class="title">Choose Site</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">3</span>
										<span class="title">Shippings</span>
									</li>
									<li class="headSettings_point-item active">
										<span class="step">4</span>
										<span class="title">Tax</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">5</span>
										<span class="title">Select Univers</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">6</span>
										<span class="title">Categories</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">7</span>
										<span class="title">Export Products</span>
									</li>
									<li class="headSettings_point-item">
										<span class="step">8</span>
										<span class="title">Finish</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="head_text p-t10 b-Top m-b0 clearfix">
								<img src="flags/Belarus.png" class="flag m-r10" alt="">
								<span class="p-t10">Belarus</span>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="headSettings clearfix b-Top-None b-Bottom p-0">
								<h4 class="headSettings_head pull-sm-left p-t10">Selected Main Categories</h4>										
								<ul class="pull-sm-right clearfix treeSettings">
									<li class="expandAll"><i class="cb-plus"></i>Expand all</li>
									<li class="collapseAll"><i class="cb-minus"></i>Collapse all</li>
									<li class="checkedAll"><i class="cb-checked"></i>Check all</li>
									<li class="uncheckedAll"><i class="cb-unchecked"></i>Uncheck all</li>
									<li class="link"><i class="cb-store"></i>Get Store Categories</li>						
								</ul>
							</div>
						</div>
					</div>		
					<div class="row">
						<div class="col-xs-12">
							<div class="validate pink m-t20 m-b0">
								<div class="validateRow">
									<div class="validateCell">
										<i class="note"></i>
									</div>
									<div class="validateCell">
										<p>
											Update Categories Selected Failed : Please, try again.
										</p>
										<i class="fa fa-remove pull-right"></i>
									</div>
								</div>
							</div>
						</div>
					</div>			
					<div class="row">
						<div class="col-xs-12">
							<div class="validate blue m-t20">
								<div class="validateRow">
									<div class="validateCell">
										<i class="note"></i>
									</div>
									<div class="validateCell">
										<div class="pull-left">
											<p>
												You can select multiple categories at once by checking the first category then pressing 'Shift' and clicking on the last element you wish to include in the same profile.
											</p>
											<ol>
												<li>
													<p>Primary eBay Category - This is the main category where Buyers will find your item on eBay (not in your store, but on general eBay shopping).</p>
												</li>
												<li>
													<p>Secondary eBay Category (optional) - This field is optional.</p>
													<p>If you add a secondary category to a listing, you may later change it to a different one, but you will not be able to remove the use of a secondary category entirely.</p>
												</li>
												<li>
													<p>Primary eBay Store Category (optional) - if you have imported Store Categories, you will also be able to choose eBay Store categories when doing your Product Mapping.</p>
													<p>Store Categories are optional and will only apply if you have an eBay Store and have imported them for use in Feed.biz.</p>
												</li>
												<li>
													<p>Secondary eBay Store Category (optional) - you may choose a second category from your eBay Store category structure as well, if desired.</p>
												</li>
											</ol>
										</div>
										<i class="fa fa-remove pull-right"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#primary" data-toggle="tab">Primary Categories</a></li>
								<li><a href="#secondary" data-toggle="tab">secondary categories</a></li>
								<li><a href="#prim_store" data-toggle="tab">Primary Store Categories</a></li>	
								<li><a href="#sec_store" data-toggle="tab">secondary Store Categories</a></li>							
							</ul>
						</div>
					</div>
					<div class="tab-content">
						<div class="tab-pane active" id="primary">
							<div class="tree clearfix custom-form">	
								<i class="icon-folder"></i>
								<p class="treeHead treeRight">Choose eBay Categories</p>
								<ul class="tree_point">
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>									
												<label class="cb-checkbox ">											
													<input type="checkbox"/>
													women
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
										<ul class="tree_child">
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															dresses
														</label>
													</p>
												</span>
												<div class="treeRight selectTree">
													<div class="tree_show">
														<select class="search-select">
															<option value="">Home</option>
															<option value="">Apple</option>
															<option value="">Iphone 6</option>
															<option value="">Iphone 5</option>
															<option value="">Iphone 4</option>
															<option value="">Iphone 3</option>
															<option value="">Men shirt</option>
															<option value="">Jack &amp; Jones</option>
															<option value="">Adidas</option>
															<option value="">Armani</option>
															<option value="">Dolce &amp; Gabbana</option>
															<option value="">Mersedes</option>
															<option value="">Audi</option>
														</select>
														<i class="icon-more"></i>
													</div>
												</div>
												<ul class="tree_child">
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	small
																</label>
															</p>
														</span>
														<div class="treeRight selectTree">
															<div class="tree_show">
																<select class="search-select">
																	<option value="">Home</option>
																	<option value="">Apple</option>
																	<option value="">Iphone 6</option>
																	<option value="">Iphone 5</option>
																	<option value="">Iphone 4</option>
																	<option value="">Iphone 3</option>
																	<option value="">Men shirt</option>
																	<option value="">Jack &amp; Jones</option>
																	<option value="">Adidas</option>
																	<option value="">Armani</option>
																	<option value="">Dolce &amp; Gabbana</option>
																	<option value="">Mersedes</option>
																	<option value="">Audi</option>
																</select>
																<i class="icon-more"></i>
															</div>
														</div>
													</li>
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	big
																</label>
															</p>
														</span>
														<div class="treeRight selectTree">
															<div class="tree_show">
																<select class="search-select">
																	<option value="">Home</option>
																	<option value="">Apple</option>
																	<option value="">Iphone 6</option>
																	<option value="">Iphone 5</option>
																	<option value="">Iphone 4</option>
																	<option value="">Iphone 3</option>
																	<option value="">Men shirt</option>
																	<option value="">Jack &amp; Jones</option>
																	<option value="">Adidas</option>
																	<option value="">Armani</option>
																	<option value="">Dolce &amp; Gabbana</option>
																	<option value="">Mersedes</option>
																	<option value="">Audi</option>
																</select>
																<i class="icon-more"></i>
															</div>
														</div>
													</li>
												</ul>
											</li>
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															blouses
														</label>
													</p>
												</span>
												<div class="treeRight selectTree">
													<div class="tree_show">
														<select class="search-select">
															<option value="">Home</option>
															<option value="">Apple</option>
															<option value="">Iphone 6</option>
															<option value="">Iphone 5</option>
															<option value="">Iphone 4</option>
															<option value="">Iphone 3</option>
															<option value="">Men shirt</option>
															<option value="">Jack &amp; Jones</option>
															<option value="">Adidas</option>
															<option value="">Armani</option>
															<option value="">Dolce &amp; Gabbana</option>
															<option value="">Mersedes</option>
															<option value="">Audi</option>
														</select>
														<i class="icon-more"></i>
													</div>
												</div>
											</li>
										</ul>
									</li>
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>
												<label class="cb-checkbox">
													<input type="checkbox"/>
													men
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
									</li>
								</ul>
							</div>				
						</div>
						<div class="tab-pane" id="secondary">
							<div class="tree clearfix custom-form">	
								<i class="icon-folder"></i>
								<p class="treeHead treeRight">Choose eBay Categories</p>
								<ul class="tree_point">
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>									
												<label class="cb-checkbox ">											
													<input type="checkbox"/>
													women
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
										<ul class="tree_child">
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															dresses
														</label>
													</p>
												</span>
												<div class="treeRight selectTree">
													<div class="tree_show">
														<select class="search-select">
															<option value="">Home</option>
															<option value="">Apple</option>
															<option value="">Iphone 6</option>
															<option value="">Iphone 5</option>
															<option value="">Iphone 4</option>
															<option value="">Iphone 3</option>
															<option value="">Men shirt</option>
															<option value="">Jack &amp; Jones</option>
															<option value="">Adidas</option>
															<option value="">Armani</option>
															<option value="">Dolce &amp; Gabbana</option>
															<option value="">Mersedes</option>
															<option value="">Audi</option>
														</select>
														<i class="icon-more"></i>
													</div>
												</div>
												<ul class="tree_child">
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	small
																</label>
															</p>
														</span>
														<div class="treeRight selectTree">
															<div class="tree_show">
																<select class="search-select">
																	<option value="">Home</option>
																	<option value="">Apple</option>
																	<option value="">Iphone 6</option>
																	<option value="">Iphone 5</option>
																	<option value="">Iphone 4</option>
																	<option value="">Iphone 3</option>
																	<option value="">Men shirt</option>
																	<option value="">Jack &amp; Jones</option>
																	<option value="">Adidas</option>
																	<option value="">Armani</option>
																	<option value="">Dolce &amp; Gabbana</option>
																	<option value="">Mersedes</option>
																	<option value="">Audi</option>
																</select>
																<i class="icon-more"></i>
															</div>
														</div>
													</li>
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	big
																</label>
															</p>
														</span>
														<div class="treeRight selectTree">
															<div class="tree_show">
																<select class="search-select">
																	<option value="">Home</option>
																	<option value="">Apple</option>
																	<option value="">Iphone 6</option>
																	<option value="">Iphone 5</option>
																	<option value="">Iphone 4</option>
																	<option value="">Iphone 3</option>
																	<option value="">Men shirt</option>
																	<option value="">Jack &amp; Jones</option>
																	<option value="">Adidas</option>
																	<option value="">Armani</option>
																	<option value="">Dolce &amp; Gabbana</option>
																	<option value="">Mersedes</option>
																	<option value="">Audi</option>
																</select>
																<i class="icon-more"></i>
															</div>
														</div>
													</li>
												</ul>
											</li>
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															blouses
														</label>
													</p>
												</span>
												<div class="treeRight selectTree">
													<div class="tree_show">
														<select class="search-select">
															<option value="">Home</option>
															<option value="">Apple</option>
															<option value="">Iphone 6</option>
															<option value="">Iphone 5</option>
															<option value="">Iphone 4</option>
															<option value="">Iphone 3</option>
															<option value="">Men shirt</option>
															<option value="">Jack &amp; Jones</option>
															<option value="">Adidas</option>
															<option value="">Armani</option>
															<option value="">Dolce &amp; Gabbana</option>
															<option value="">Mersedes</option>
															<option value="">Audi</option>
														</select>
														<i class="icon-more"></i>
													</div>
												</div>
											</li>
										</ul>
									</li>
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>
												<label class="cb-checkbox">
													<input type="checkbox"/>
													men
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="tab-pane" id="prim_store">
							<div class="tree clearfix custom-form">	
								<i class="icon-folder"></i>
								<p class="treeHead treeRight">Choose eBay Categories</p>
								<ul class="tree_point">
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>									
												<label class="cb-checkbox ">											
													<input type="checkbox"/>
													women
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
										<ul class="tree_child">
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															dresses
														</label>
													</p>
												</span>
												<div class="treeRight selectTree">
													<div class="tree_show">
														<select class="search-select">
															<option value="">Home</option>
															<option value="">Apple</option>
															<option value="">Iphone 6</option>
															<option value="">Iphone 5</option>
															<option value="">Iphone 4</option>
															<option value="">Iphone 3</option>
															<option value="">Men shirt</option>
															<option value="">Jack &amp; Jones</option>
															<option value="">Adidas</option>
															<option value="">Armani</option>
															<option value="">Dolce &amp; Gabbana</option>
															<option value="">Mersedes</option>
															<option value="">Audi</option>
														</select>
														<i class="icon-more"></i>
													</div>
												</div>
												<ul class="tree_child">
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	small
																</label>
															</p>
														</span>
														<div class="treeRight selectTree">
															<div class="tree_show">
																<select class="search-select">
																	<option value="">Home</option>
																	<option value="">Apple</option>
																	<option value="">Iphone 6</option>
																	<option value="">Iphone 5</option>
																	<option value="">Iphone 4</option>
																	<option value="">Iphone 3</option>
																	<option value="">Men shirt</option>
																	<option value="">Jack &amp; Jones</option>
																	<option value="">Adidas</option>
																	<option value="">Armani</option>
																	<option value="">Dolce &amp; Gabbana</option>
																	<option value="">Mersedes</option>
																	<option value="">Audi</option>
																</select>
																<i class="icon-more"></i>
															</div>
														</div>
													</li>
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	big
																</label>
															</p>
														</span>
														<div class="treeRight selectTree">
															<div class="tree_show">
																<select class="search-select">
																	<option value="">Home</option>
																	<option value="">Apple</option>
																	<option value="">Iphone 6</option>
																	<option value="">Iphone 5</option>
																	<option value="">Iphone 4</option>
																	<option value="">Iphone 3</option>
																	<option value="">Men shirt</option>
																	<option value="">Jack &amp; Jones</option>
																	<option value="">Adidas</option>
																	<option value="">Armani</option>
																	<option value="">Dolce &amp; Gabbana</option>
																	<option value="">Mersedes</option>
																	<option value="">Audi</option>
																</select>
																<i class="icon-more"></i>
															</div>
														</div>
													</li>
												</ul>
											</li>
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															blouses
														</label>
													</p>
												</span>
												<div class="treeRight selectTree">
													<div class="tree_show">
														<select class="search-select">
															<option value="">Home</option>
															<option value="">Apple</option>
															<option value="">Iphone 6</option>
															<option value="">Iphone 5</option>
															<option value="">Iphone 4</option>
															<option value="">Iphone 3</option>
															<option value="">Men shirt</option>
															<option value="">Jack &amp; Jones</option>
															<option value="">Adidas</option>
															<option value="">Armani</option>
															<option value="">Dolce &amp; Gabbana</option>
															<option value="">Mersedes</option>
															<option value="">Audi</option>
														</select>
														<i class="icon-more"></i>
													</div>
												</div>
											</li>
										</ul>
									</li>
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>
												<label class="cb-checkbox">
													<input type="checkbox"/>
													men
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="tab-pane" id="sec_store">
							<div class="tree clearfix custom-form">	
								<i class="icon-folder"></i>
								<p class="treeHead treeRight">Choose eBay Categories</p>
								<ul class="tree_point">
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>									
												<label class="cb-checkbox ">											
													<input type="checkbox"/>
													women
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
										<ul class="tree_child">
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															dresses
														</label>
													</p>
												</span>
												<div class="treeRight selectTree">
													<div class="tree_show">
														<select class="search-select">
															<option value="">Home</option>
															<option value="">Apple</option>
															<option value="">Iphone 6</option>
															<option value="">Iphone 5</option>
															<option value="">Iphone 4</option>
															<option value="">Iphone 3</option>
															<option value="">Men shirt</option>
															<option value="">Jack &amp; Jones</option>
															<option value="">Adidas</option>
															<option value="">Armani</option>
															<option value="">Dolce &amp; Gabbana</option>
															<option value="">Mersedes</option>
															<option value="">Audi</option>
														</select>
														<i class="icon-more"></i>
													</div>
												</div>
												<ul class="tree_child">
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	small
																</label>
															</p>
														</span>
														<div class="treeRight selectTree">
															<div class="tree_show">
																<select class="search-select">
																	<option value="">Home</option>
																	<option value="">Apple</option>
																	<option value="">Iphone 6</option>
																	<option value="">Iphone 5</option>
																	<option value="">Iphone 4</option>
																	<option value="">Iphone 3</option>
																	<option value="">Men shirt</option>
																	<option value="">Jack &amp; Jones</option>
																	<option value="">Adidas</option>
																	<option value="">Armani</option>
																	<option value="">Dolce &amp; Gabbana</option>
																	<option value="">Mersedes</option>
																	<option value="">Audi</option>
																</select>
																<i class="icon-more"></i>
															</div>
														</div>
													</li>
													<li>
														<span class="tree_item">
															<p>
																<i class="cb-plus good"></i>
																<label class="cb-checkbox">
																	<input type="checkbox"/>
																	big
																</label>
															</p>
														</span>
														<div class="treeRight selectTree">
															<div class="tree_show">
																<select class="search-select">
																	<option value="">Home</option>
																	<option value="">Apple</option>
																	<option value="">Iphone 6</option>
																	<option value="">Iphone 5</option>
																	<option value="">Iphone 4</option>
																	<option value="">Iphone 3</option>
																	<option value="">Men shirt</option>
																	<option value="">Jack &amp; Jones</option>
																	<option value="">Adidas</option>
																	<option value="">Armani</option>
																	<option value="">Dolce &amp; Gabbana</option>
																	<option value="">Mersedes</option>
																	<option value="">Audi</option>
																</select>
																<i class="icon-more"></i>
															</div>
														</div>
													</li>
												</ul>
											</li>
											<li>
												<span class="tree_item">
													<p>
														<i class="cb-plus good"></i>
														<label class="cb-checkbox">
															<input type="checkbox"/>
															blouses
														</label>
													</p>
												</span>
												<div class="treeRight selectTree">
													<div class="tree_show">
														<select class="search-select">
															<option value="">Home</option>
															<option value="">Apple</option>
															<option value="">Iphone 6</option>
															<option value="">Iphone 5</option>
															<option value="">Iphone 4</option>
															<option value="">Iphone 3</option>
															<option value="">Men shirt</option>
															<option value="">Jack &amp; Jones</option>
															<option value="">Adidas</option>
															<option value="">Armani</option>
															<option value="">Dolce &amp; Gabbana</option>
															<option value="">Mersedes</option>
															<option value="">Audi</option>
														</select>
														<i class="icon-more"></i>
													</div>
												</div>
											</li>
										</ul>
									</li>
									<li>
										<span class="tree_item">
											<p>
												<i class="cb-plus good"></i>
												<label class="cb-checkbox">
													<input type="checkbox"/>
													men
												</label>
											</p>
										</span>
										<div class="treeRight selectTree">
											<div class="tree_show">
												<select class="search-select">
													<option value="">Home</option>
													<option value="">Apple</option>
													<option value="">Iphone 6</option>
													<option value="">Iphone 5</option>
													<option value="">Iphone 4</option>
													<option value="">Iphone 3</option>
													<option value="">Men shirt</option>
													<option value="">Jack &amp; Jones</option>
													<option value="">Adidas</option>
													<option value="">Armani</option>
													<option value="">Dolce &amp; Gabbana</option>
													<option value="">Mersedes</option>
													<option value="">Audi</option>
												</select>
												<i class="icon-more"></i>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top m-t30 p-t20">
								<div class="pull-right">
									<a href="" class="pull-left link p-size m-tr10">
										Previuos
									</a>
									<button class="btn btn-save ">Next <i class="m-l5 fa fa-angle-right"></i></button>                  
								</div>
							</div>		
						</div>
					</div>																			
				</div>
			</div>
		</div>
	</div>