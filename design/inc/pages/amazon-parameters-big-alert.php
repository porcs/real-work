<div class="p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">Parameters</h1>
		<ul class="breadcrumbs menubar clearfix">
			<li class="bread_item"><a class="bread_link" href="">Expert</a></li>
			<li class="bread_item"><a class="bread_link" href="">Amazon</a></li>				
			<li class="bread_item active"><a class="bread_link" href="">Parameters</a></li>
		</ul>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t10 clearfix">
					<p class="head_text">
						<img src="flags/France.png" class="flag m-r10" alt="">
						<span class="p-t10">France</span>					
					</p>
				</div>
			</div>
		</div>
		<div class="row m-t20 custom-form">
			<div class="col-sm-6 col-md-4 col-lg-3">
				<p class="montserrat dark-gray text-uc">Preference</p>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Allow automatic offer creation
				</label>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Synchronize Quantity
				</label>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Synchronize Price
				</label>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3">
				<p class="montserrat dark-gray text-uc">Mode</p>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Creation Mode
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="head_text p-tb20 m-b20">
					API Settings
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 m-b10">
				<p class="poor-gray">
					Enter API credentials (key pairs)
				</p>
				<select class="search-select" data-placeholder="Choose">
					<option value="">Home</option>
					<option value="">Apple</option>
					<option value="">Iphone 6</option>
					<option value="">Iphone 5</option>
					<option value="">Iphone 4</option>
					<option value="">Iphone 3</option>
					<option value="">Men shirt</option>
					<option value="">Jack & Jones</option>
					<option value="">Adidas</option>
					<option value="">Armani</option>
					<option value="">Dolce & Gabbana</option>
					<option value="">Mersedes</option>
					<option value="">Audi</option>
				</select>
			</div>
			<div class="col-sm-4">
				<p class="poor-gray">AWS Key ID *</p>
				<div class="form-group">
					<input type="text" class="form-control"/>
				</div>
			</div>
			<div class="col-sm-4 xs-center sm-right col-lg-3">
				<div class="form-group has-error">
					<button class="btn btn-check">
						<i class="icon-check"></i>
						Check connectivity
					</button>
					<div class="error">
						<p class="m-t0">
							<i class="fa fa-exclamation-circle"></i>
							Connection to Amazon failed!
						</p>
					</div>
				</div>				
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-sm-4">
				<p class="poor-gray">Merchant ID *</p>
				<div class="form-group has-error">
					<input type="text" class="form-control"/>
				</div>
			</div>
			<div class="col-sm-4 m-b10">
				<p class="poor-gray">AWS Secret Key *</p>
				<div class="form-group">
					<input type="text" class="form-control"/>
				</div>
				<p class="pull-right tooSmall regRoboto poor-gray">* fields are required</p>
				<p class="poor-gray m-b0">MWS URL for Keypairs : </p>
				<a href="https://sellercentral.amazon.fr/gp/mws/index.html" class="p-size link">
					https://sellercentral.amazon.fr/gp/mws/index.html
				</a>
			</div>
			<div class="col-sm-4 col-lg-3">
				<div class="pull-right">
					<p class="pull-left poor-gray m-t3 m-r10">Debug mode :</p>
					<div class="cb-switcher pull-left">
						<label class="inner-switcher">
							<input type="checkbox" data-state-on="ON" data-state-off="OFF"/>
						</label>
						<span class="cb-state">ON</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate pink m-t10">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<div class="pull-left">
								<p>
									Warning : Connection to Amazon failed ! invalid seller id : a2f51hrhydf2h1e5rh
								</p>								
							</div>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t10 m-t10">
					<div class="pull-right m-t20">
						<a href="" class="pull-left link p-size m-tr10">
							Save
						</a>
						<button type="button" class="btn btn-save btnNext">Save & Continue</button>                   
					</div>
				</div>							
			</div>
		</div>
	</div>