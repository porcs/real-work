<header class="navbar-fixed-top">
	<div class="logo">
		<a href=""><img src="images/main-logo.png" alt="" class="logo-picture"></a>
		<button class="btn btn-menu">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<nav class="nav">
		<ul class="loginBlock profile">
			<li class="search">
				<input class="form-control"/>
				<i class="btn-MainSearch"></i>
			</li>
			<li><a href=""><i class="btn-ring"><span class="icon-circle">1</span></i></a></li>
			<li>
				<a href=""><img class="icon-avatar icon-circle" src="images/icons/enot.jpg" alt=""></a>
			</li>
			<li class="language">
				<a href="">EN</a>
				<ul class="language_block">
					<li><a href="">English</a></li>
					<li><a href="">France</a></li>
				</ul>
			</li>
			<li class="logOut">
				<a href="">Log out <i class="icon-logOut"></i></a>
			</li>			
		</ul>
		<i class="btn btn-more"></i>
	</nav>
</header>
<!--<div class="notification bad">
	<div class="container">
		<p><i class="fa fa-exclamation-triangle"></i>
			Error login mismatch		
		</p>
		<i class="fa fa-remove"></i>
	</div>
</div>-->