<div id="menu">
	<div class="sidebar active">
		<ul class="sidebar_content">
			<li class="sidebar-item">
				<a class="sidebar-item_link link" href="">
					<i class="icon-clock"></i>
					Dashboard
				</a>
			</li>
			<li class="sidebar-item">
				<a class="sidebar-item_link link" href="">
					<i class="icon-men"></i>
					My account
				</a>
				<ul class="sidebar_down">
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link link" href="">Edit Profile</a>					
					</li>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link" href="">Edit Billing Info</a>
					</li>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link link" href="">Affiliation</a>
						<ul>
							<li><a class="sidebar-item_Twice-down-link link" href="">Affiliate List</a></li>
							<li><a class="sidebar-item_Twice-down-link link" href="">Statistics</a></li>
							<li><a class="sidebar-item_Twice-down-link link" href="">Invitations</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="sidebar-item">
				<a class="sidebar-item_link link" href="">
					<i class="icon-money"></i>
					Billing
					<i class="icon-warning"></i>
				</a>
				<ul>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link" href="">Packages</a>
					</li>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link" href="">Download Billing</a>
					</li>
				</ul>
			</li>
			<li class="sidebar-item">
				<a class="sidebar-item_link link" href="">
					<i class="icon-cart"></i>
					My shop
				</a>
				<ul>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link" href="">Connect</a>
					</li>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link" href="">My products</a>
					</li>
				</ul>			
			</li>
			<li class="sidebar-item">
				<a class="sidebar-item_link link" href="">
					<i class="icon-globe"></i>
					Marketplace
				</a>
				<ul>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link link" href="">Configuration</a>				
					</li>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link link" href="">Ebay</a>
						<ul>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.fr</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.co.uk</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.de</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.it</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.es</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.at</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.at</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Benl.ebay.be</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Befr.ebay.be</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.ie</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.nl</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurates</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.pl</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Eim.ebay.se</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.ch</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.ca</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Cafr.ebay.ca</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Ebay.com</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Auth & Security</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Settings</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Templates</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Price Modifier</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Tax</a></li>
											<li class="sidebar-item_Fouth-down">
												<a class="sidebar-item_Fouth-down-link link" href="">Mapping</a>
												<ul>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Carriers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Templates</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Univers</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Categories</a></li>
													<li><a class="sidebar-item_Fifth-down-link link" href="">Condition</a></li>
												</ul>
											</li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Advanced Settings</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Batch log details</a></li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link link" href="">Amazon</a>
						<ul>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">amazon.co.jp</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Parameters</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Profiles</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Category</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Carriers</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Conditions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Status</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>									
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Logs</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>									
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">amazon.cn</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Parameters</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Profiles</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Category</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Carriers</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Conditions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Status</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>									
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Logs</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>									
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">amazon.ca</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Parameters</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Profiles</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Category</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Carriers</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Conditions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Status</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>									
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Logs</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>									
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">amazon.fr</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Parameters</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Profiles</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Category</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Carriers</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Conditions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Status</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>									
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Logs</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>									
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">amazon.com.uk</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Parameters</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Profiles</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Category</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Carriers</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Conditions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Status</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>									
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Logs</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>									
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">amazon.de</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Parameters</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Profiles</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Category</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Carriers</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Conditions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Status</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>									
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Logs</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>									
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">amazon.it</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Parameters</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Profiles</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Category</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Carriers</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Conditions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Status</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>									
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Logs</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>									
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">amazon.es</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Parameters</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Profiles</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Category</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Carriers</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Conditions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Status</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>									
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Logs</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>									
									</li>
								</ul>
							</li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">amazon.com</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Configurations</a>
										<ul>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Parameters</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Profiles</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Category</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Carriers</a></li>
											<li class="sidebar-item_Fouth-down"><a class="sidebar-item_Fouth-down-link link" href="">Conditions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Task</a>									
										<ul>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Status</a></li>
											<li><a class="sidebar-item_Fouth-down-link link" href="">Actions</a></li>
										</ul>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Report</a>									
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Logs</a>
									</li>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Orders</a>									
									</li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="sidebar-item">
				<a class="sidebar-item_link link" href="">
					<i class="icon-feed"></i>
					My feed
				</a>
				<ul>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link link" href="">Configuration</a>
						<ul>
							<li><a class="sidebar-item_Twice-down-link link" href="">General</a></li>
						</ul>
					</li>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link link" href="">Parameters</a>
						<ul>
							<li><a class="sidebar-item_Twice-down-link link" href="">Carriers</a></li>
							<li><a class="sidebar-item_Twice-down-link link" href="">Filters</a></li>
							<li>
								<a class="sidebar-item_Twice-down-link link" href="">Mapping</a>
								<ul>
									<li>
										<a class="sidebar-item_Third-down-link link" href="">Manufactures</a>
									</li>
								</ul>
							</li>
							<li><a class="sidebar-item_Twice-down-link link" href="">Rules</a></li>
							<li><a class="sidebar-item_Twice-down-link link" href="">Profiles</a></li>
							<li><a class="sidebar-item_Twice-down-link link" href="">Category</a></li>
						</ul>
					</li>
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link link" href="">Log</a>
						<ul>
							<li><a class="sidebar-item_Twice-down-link link" href="">Batches</a></li>
							<li><a class="sidebar-item_Twice-down-link link" href="">Messages</a></li>
							<li><a class="sidebar-item_Twice-down-link link" href="">Import history</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="sidebar-item">
				<a class="sidebar-item_link link" href="">
					<i class="icon-list"></i>
					Stock
				</a>
				<ul class="sidebar_down">
					<li class="sidebar-item_Once-down">
						<a class="sidebar-item_Once-down-link link" href="">Report</a>					
					</li>					
				</ul>
			</li>			
			<li class="sidebar-item">
				<a class="sidebar-item_link link" href="">
					<i class="icon-help"></i>
					Help
				</a>
			</li>
		</ul>
	</div>
</div>