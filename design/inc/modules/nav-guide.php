 <ul class="nav nav-sidebar">              
                <li class="active"><a href="#grid">Grid system</a></li>
                <li><a href="#type">Typography</a></li>
                <li><a href="#forms">Forms</a></li>
                <li><a href="#header">Head Menu</a></li>
                <li><a href="#dropdown">Dropdown</a></li>
                <li><a href="#checkbo">Checkbox, Radio & Switcher</a></li>
                <li><a href="#buttons">Buttons</a></li>
                <li><a href="#donut">Donut chart</a></li>
                <li><a href="#lineChart">Line chart</a></li>
                <li><a href="#responsiveTable">Table</a></li>
                <li><a href="#countdown">Countdown</a></li>
                <li><a href="#notification">Notification</a></li>
                <li><a href="#responsiveTable">Table with sorting</a></li>
                <li><a href="#header">Head Title</a></li>
                <li><a href="#treeFiles">Tree of files</a></li>
                <li><a href="#tags">Tags</a></li>
                <li><a href="#manufacturers"> Clone block</a></li>
                <li><a href="#anotherTables">Another Tables</a></li>
                <li><a href="#calendar">Calendar</a></li>
                <li><a href="#notice">Notice</a></li>
                <li><a href="#flag">Flag</a></li>
                <li><a href="#progresBar">Progress Bar</a></li>
                <li><a href="#cssHelper">Css Helper</a></li>
            </ul>