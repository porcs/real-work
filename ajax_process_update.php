<?php 
ini_set('display_errors',1);
error_reporting(E_ALL);
$system_path = 'system';
define('BASEPATH', str_replace("\\", "/", $system_path));
include('../application/config/config.php');
include('../application/config/database.php');
include('./apu_lib.php');
define("SESSON_ENCRYPT" , $config['sess_encrypt_cookie']);
define("COOKIE_NAME" , $config['sess_cookie_name']);
define('encryption_key',($config['encryption_key']));
$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.');
define('_hash_type','sha1');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$session = isset($_COOKIE[COOKIE_NAME])?$_COOKIE[COOKIE_NAME]:null;
if($session){
    if (SESSON_ENCRYPT == TRUE)
    { 

            $_mcrypt_exists = ( ! function_exists('mcrypt_encrypt')) ? FALSE : TRUE;
            $key = encryption_key;
            $dec = base64_decode($session);
            $session = mcrypt_decode($dec,$key);
            $session =  _unserialize($session);
    }
    else
    {
            // encryption was not used, so we need to check the md5 hash
            $hash	 = substr($session, strlen($session)-32); // get last 32 chars
            $session = substr($session, 0, strlen($session)-32);

            // Does the md5 hash match? This is to prevent manipulation of session data in userspace
            if ($hash !==  md5($session. encryption_key))
            {
                    die($hash.' '.$session.'<br>The session cookie data did not match what was expected. This could be a possible hacking attempt.');
            } 
            $session =  _unserialize($session);
    }

    if(isset($session['session_id'])){
        $ses_id = $session['session_id'];

        $sql = "select * from ".$config['sess_table_name']." where  session_id = '$ses_id' ";
        $q = mysqli_query($dbx,$sql);
        $data = mysqli_fetch_assoc($q);

        if(!empty($data) && isset($data['user_data'])){
            $user_data = $data['user_data'];
            $custom_data =  _unserialize($user_data);

            if (is_array($custom_data))
            {
                    foreach ($custom_data as $key => $val)
                    {
                            $session[$key] = $val;
                    }
            } 
            $userdata = $session;
            ajax_update_process();

        }else{
            //$out = json_encode(array('delay'=>1000,'timeout'=>5000));
            $out = 'Session Timeout';
            die($out);
        }
    }else{
        //$out = json_encode(array('delay'=>1000,'timeout'=>5000));
        $out = 'Session Timeout';
        die($out);
    }

}else{
    $out = 'Session Timeout';
    die($out);
}

?>