<?php 
$target_dir = '..';
$target_name = '/sync/src.tgz';
$target_path = $target_dir.$target_name;
$extract_target = realpath(__DIR__.'/../../');
$main_domain = 'https://client.feed.biz/';
$gen_url = $main_domain."script/sync/gen_tgz.php";
$src_url = $main_domain."script".$target_name;

$exclude_path = 
    array('../../application/libraries/MPDF','../../application/libraries/Opauth',
        '../../application/views/templates/ebay/bak/','../../application/libraries/Swift',
        '../../application/libraries/PayPal','../../application/libraries/Smarty',
        '../../application/logs' ,'../../assets/apps/node_modules',
        '*.csv','*old','*.log','*.sql','*.txt','*.*gz','*.7z','*.out',
        '*.*-*.*','*.bak','*bak.*','*2015*.*','*2016*.*','*backup*','_*.*',
        '../../assets/apps/users/*/xml_log/','../../assets/apps/users/*/amazon/',
        '../../assets/apps/users/*/mirakl/','../../assets/apps/users/cron'
    );
$include_path = array('../../application/libraries/' ,'../../application/models/' ,
    '../../application/views/templates/*' ,'../../assets/apps/*.php',
    '../../assets/apps/*.js','../../assets/apps/FeedbizImport',
    '../../assets/apps/ebay','../../assets/apps/cronman',
    '../../assets/apps/tpl','../../assets/apps/templates_c',
    '../../script','../../libraries',
    '../../application/helpers/','../../application/language','../../assets/apps/users/*/template');

//$include_path = array('../../application/helpers/');