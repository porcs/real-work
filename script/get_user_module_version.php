<?php 
ini_set('display_errors',1);
error_reporting(E_ALL);
$system_path = 'system';
define('BASEPATH', str_replace("\\", "/", $system_path));
include(dirname(__FILE__) . '/../libraries/feedbiz_override_tools.php');
include(dirname(__FILE__) . '/../application/config/config.php');
include(dirname(__FILE__) . '/../application/config/database.php');  
$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.');
  
$sql = "select value from configuration where name = 'FEED_BIZ_CURRENT_MODULE_VERSION' and id_customer=0";
$q = mysqli_query($dbx,$sql);  
$d = mysqli_fetch_array($q);
$current_ver = $d['value'];
 
$sql = "select * from configuration ,users where name like 'FEED_BIZ%' and id=id_customer and (user_module_ver <> '$current_ver' or user_module_ver is null)  ";
//echo $sql;
//echo '<pre>';
$q = mysqli_query($dbx,$sql); 
$ver_list=array();
while($d = mysqli_fetch_assoc($q)){  
    $val = unserialize(base64_decode($d['value']));
     $url =trim($val['base_url']);
     $key = 'functions/connector.php';
     if(($url)!='' && strpos($url,$key)!==false){
            if(isset($ver_list[$url])){
                $ver = $ver_list[$url];
                $sql = "update users set user_module_ver = '{$ver}' where id = '{$d['id_customer']}'";
                mysqli_query($dbx,$sql);
                continue;
            }
             $config_url =  str_replace($key,'config.xml',$url);
             $config_us_url=  str_replace($key,'config_us.xml',$url);
             $config_fr_url=  str_replace($key,'config_fr.xml',$url);
//             echo $config_url."\n";
             $xmlstr = curl_get_contents($config_us_url);
             if( !isset($xmlstr) || empty($xmlstr) ){
                 $xmlstr = curl_get_contents($config_fr_url);
             }
             if( !isset($xmlstr) || empty($xmlstr) ){
                 $xmlstr = curl_get_contents($config_url);
             }
         
             if( !isset($xmlstr) || empty($xmlstr) ){
                 $ver_list[$url]='';
//                 echo  "Empty file \n";
                 continue;
             }
            try{
            $oxml = @new SimpleXMLElement($xmlstr);
            if(!empty($oxml)){
                
                if(isset($oxml->version)&& !empty($oxml->version)){
//                    echo $oxml->version."\n";
                    $ver_list[$url] = $oxml->version;
                    $sql = "update users set user_module_ver = '{$oxml->version}' where id = '{$d['id_customer']}'";
                    mysqli_query($dbx,$sql);
                }
                
            }
            }catch(Exception $e){
                echo $e->getMessage()."\n";
                echo $config_url."\n";
            }
     }
} 
//echo '</pre>';
?>
