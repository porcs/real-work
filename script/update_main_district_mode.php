<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
$system_path = 'system';
define('BASEPATH', str_replace("\\", "/", $system_path));
include(dirname(__FILE__) . '/../application/config/config.php');
include (dirname(__FILE__).'/../application/libraries/db_mysqli.php');
include(dirname(__FILE__) . '/../application/config/database.php');
$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.');
$aync = false;

$sql = "select * from restrict_mode where status = 1 ";
$q = mysqli_query($dbx,$sql);
$list =array();
while($d = mysqli_fetch_assoc($q)){
    $list[$d['controller']][$d['method']][$d['additional']] =
        array('free'=>$d['free_mode'],'pro'=>$d['pro_mode'],'adv'=>$d['advance_mode'],'man'=>$d['manager_mode'],'url'=>str_replace($config['base_url'],'',$d['url']));
}


file_put_content(USERDATA_PATH.'/json/restrict_mode.json',json_encode($list));
