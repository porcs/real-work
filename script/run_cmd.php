<?php define('_SKIP_',true);
include('./admin_login_prove.php');
?>
<?php
function get_mfa_status_users(){
        $out = array();
    $target_file = '../assets/apps/users/mfa_activate_users.json';
    if(file_exists($target_file)){
        $out = json_decode(file_get_contents($target_file),true);
    }
    return $out;
}

    if(!empty($_POST['submit']) && !empty($_POST['otp'])){
        $key_list =  get_mfa_status_users();
        $mfa_mail='admin@demo.com';
        $otp_pass=false;
        if(isset($key_list[$mfa_mail])){
            include(dirname(__FILE__).'/../application/libraries/mfa/config.php');
            $key = base64_decode($key_list[$mfa_mail]);
            $otp=$_POST['otp'];
            if(TokenAuth6238::verify($key,$otp)){
                $otp_pass=true;
            }
         }else{
             $otp_pass=true;
         }
        if($otp_pass){
            echo file_get_contents('https://127.0.0.1:3001/run_cli/'.str_replace(array(' ','/','|','&','*'),array('%20','%2F','%7C','%26','%2A'),$_POST['submit']));
        }
        
    }
    if (isset($_POST['submit'])){
        exit;
    }
?>
<pre style="width: 100%; word-break: break-word; white-space: pre-wrap;" >
<?php
echo "Current SERV time : ".date('r').'<br>';
?>
<script src="../assets/js/jquery/jquery.min.js"></script>
Command <input id="cmd" type="text" style="width:500px;">
<br>Admin OTP <input id="otp" type="text" style="width:60px;">
<button type="button" id="cmd_btn">Run</button>
<div id="result">
    <p></p>
</div>

<script>
    $('#cmd_btn').click(function(){
        var cmd = $('#cmd').val();
        var otp = $('#otp').val();
        if(jQuery.trim(cmd)!=''){

            $(this).attr('disabled',true);
            $.ajax({url:'run_cmd.php',method:'POST',data:{submit:cmd,otp:otp},
                success:function(out){
                    $('#result p').html(out);
                    $('#cmd_btn').attr('disabled',false);
                }
            });
        }
    })
</script>