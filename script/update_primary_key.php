<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
$system_path = 'system';
define('BASEPATH', str_replace("\\", "/", $system_path));
include(dirname(__FILE__) . '/../application/config/config.php');
$backend_host = '127.0.0.1:3001';
$create_profiles_product_url = (@$config['ssl_protocol'] ? 'https://' : 'http://') . $backend_host.'/createuser/product/';
$create_profiles_offer_url = (@$config['ssl_protocol'] ? 'https://' : 'http://') . $backend_host.'/createuser/offer/';
$create_profiles_order_url = (@$config['ssl_protocol'] ? 'https://' : 'http://') . $backend_host.'/createuser/order/';
$type = isset($_REQUEST['type'])?$_REQUEST['type']:'products';
include (dirname(__FILE__).'/../application/libraries/db.php'); 
include(dirname(__FILE__) . '/../application/config/database.php');  
require_once(dirname(__FILE__) . '/../assets/apps/FeedbizImport/config/'.$type.'.php'); 

function update_prim_key($def,$db){
     global $prefix;
            $tb = $prefix.$def['table'];
            $field = $def['fields'];
            $pri = '';
            $all_pri = array();
            
            $sql = "select date_add from $tb group by date_add order by date_add desc limit 2";
            
            $arr = $db->db_query_str($sql);
            if(sizeof($arr)>1){
                echo $sql.'<br>';
                $last_date = $arr[0]['date_add'];
                $sql = "delete from $tb where date_add < '{$last_date}';";
                 echo $sql.'<br>';
                if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
            }
            
            
            
            foreach($field as $name=>$data){
                if(isset($data['primary_key'])){
                    $all_pri[$name] = $name;
                    $pri = $name;
                    $typec = $data['type'];
                    $sql = "ALTER TABLE {$tb} MODIFY COLUMN {$name} {$typec};";
                    echo $sql.'<br>';
                    if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                    break;
                }
            }
             
            if(isset($def['primary']) && !empty($def['primary'])){
                foreach($def['primary'] as $v){
                    $all_pri[$v] = $v;
                }
            }
            if(isset($def['unique']) && !empty($def['unique'])){
                foreach($def['unique'] as $v){
                    if(is_array($v)){
                        foreach($v as $vx){
                            $all_pri[$vx] = $vx;
                        }
                    } 
                    
                }
            }
            
            if(!empty($all_pri)){
                $sql = "ALTER TABLE {$tb} DROP PRIMARY KEY;";
                echo $sql.'<br>';
                if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
            }
            
            if(!empty($all_pri)){
                $sql = "ALTER TABLE {$tb} ADD PRIMARY KEY (".implode(',',$all_pri).");";
                echo $sql.'<br>';
                if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                
                $sql = "ALTER TABLE {$tb} ADD INDEX (".implode(',',$all_pri).");";
                echo $sql.'<br>';
                if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
            }
            
            foreach($field as $name=>$data){
                if(isset($data['primary_key'])){
                    $pri = $name;
                    $typec = $data['type'];
                    $sql = "ALTER TABLE {$tb} MODIFY COLUMN {$name} {$typec} AUTO_INCREMENT;";
                    echo $sql.'<br>';
                    if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                    
                    
                    break;
                }
            }
            
            
            //lang
            if(isset($def['lang'])&& !empty($all_pri)){
                echo '##lang table<br>';
                $tb = $tb.'_lang';
                $field = $def['lang']['fields'];
                $pri = '';
                $all_pri = array();

                $sql = "select date_add from $tb group by date_add order by date_add desc limit 2";

                $arr = $db->db_query_str($sql);
                if(sizeof($arr)>1){
                    echo $sql.'<br>';
                    $last_date = $arr[0]['date_add'];
                    $sql = "delete from $tb where date_add < '{$last_date}'";
                     echo $sql.'<br>';
                    if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                }



                foreach($field as $name=>$data){
                    if(isset($data['primary_key'])){
                        $all_pri[$name] = $name;
                        $pri = $name;
                        $typec = $data['type'];
                        $sql = "ALTER TABLE {$tb} MODIFY COLUMN {$name} {$typec};";
                        echo $sql.'<br>';
                        if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                        break;
                    }
                }

                if(isset($def['primary']) && !empty($def['primary'])){
                    foreach($def['primary'] as $v){
                        $all_pri[$v] = $v;
                    }
                }
                if(isset($def['unique']) && !empty($def['unique'])){
                    foreach($def['unique'] as $v){
                        if(is_array($v)){
                            foreach($v as $vx){
                                $all_pri[$vx] = $vx;
                            }
                        } 

                    }
                }
                $all_pri['id_lang'] = 'id_lang';

                if(!empty($all_pri)){
                    $sql = "ALTER TABLE {$tb} DROP PRIMARY KEY;";
                    echo $sql.'<br>';
                    if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                }

                if(!empty($all_pri)){
                    $sql = "ALTER TABLE {$tb} ADD PRIMARY KEY (".implode(',',$all_pri).");";
                    echo $sql.'<br>';
                    if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 

                    $sql = "ALTER TABLE {$tb} ADD INDEX (".implode(',',$all_pri).");";
                    echo $sql.'<br>';
                    if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                }

                foreach($field as $name=>$data){
                    if(isset($data['primary_key'])){
                        $pri = $name;
                        $typec = $data['type'];
                        $sql = "ALTER TABLE {$tb} MODIFY COLUMN {$name} {$typec} AUTO_INCREMENT;";
                        echo $sql.'<br>';
                        if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 


                        break;
                    }
                }
                $sql = "ALTER TABLE products_carrier_selected DROP PRIMARY KEY;";
                echo $sql.'<br>';
                if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                $sql = "ALTER TABLE products_carrier_selected ADD PRIMARY KEY (id_carrier,id_shop);";
                echo $sql.'<br>';
                if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                $sql = "ALTER TABLE products_carrier_selected ADD INDEX (id_carrier,id_shop);";
                echo $sql.'<br>';
                if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                
                $sql = "ALTER TABLE products_exclude_manufacturer DROP PRIMARY KEY;";
                echo $sql.'<br>'; if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                $sql = "ALTER TABLE products_exclude_manufacturer ADD PRIMARY KEY (id_manufacturer,id_shop);";
                echo $sql.'<br>'; if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                $sql = "ALTER TABLE products_exclude_manufacturer ADD INDEX (id_manufacturer,id_shop);";
                echo $sql.'<br>'; if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                
                
                 $sql = "ALTER TABLE products_exclude_supplier DROP PRIMARY KEY;";
                echo $sql.'<br>'; if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                $sql = "ALTER TABLE products_exclude_supplier ADD PRIMARY KEY (id_supplier,id_shop);";
                echo $sql.'<br>'; if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                $sql = "ALTER TABLE products_exclude_supplier ADD INDEX (id_supplier,id_shop);";
                echo $sql.'<br>'; if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
                
                
                
                


                
                
            }
} 


$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.');
$aync = false; 
$sql = "select * from configuration ,users where name like 'FEED_BIZ%' and id=id_customer ";
$q = mysqli_query($dbx,$sql); 
$arr_table_name=array();
$arr_table_name['products'] = array(
            'Attribute',
            'AttributeGroup',
            'Carrier',
            'Category',
            'Conditions',
            'Currency',
            'Feature',
            'FeatureValue',
            'Manufacturer',
            'Mapping',
            'Language',
            'Product',
            'ProductAttribute',
            'ProductAttributeCombination',
            'ProductAttributeImage',
            'ProductAttributeUnit',
            'ProductCarrier',
            'ProductCategory',
            'ProductFeature',
            'ProductImage',
            'ProductSupplier',
            'ProductSale',        
            'ProductTag',
            'Supplier',
            'Tag',
            'Tax',
            'Unit',
            'OTP',
            'Shop',
            'Log',
            'MarketplaceProductOption',
        );
$arr_table_name['offers'] = array(
            'Product',
            'ProductAttribute',
            'ProductSale',    
            'Category', 
            'Carrier', 
            'Conditions', 
            'Currency',
            'Manufacturer',
            'Profile', 
            'Price',
            'Rule',
            'Supplier',
            'Tax',
            'OTP',
            'Shop',
            'Log',
        );  

$arr_table_name['orders'] = array(
            'OrderLog',
            'OrderBuyers',
            'OrderInvoices',
            'OrderItems',
            'OrderItemsAttribute',
            'OrderPayments',
            'OrderSeller',
            'OrderShippings',
            'OrderStatus',
            'OrderTaxes',
            'Orders'
        );
if(!isset($arr_table_name[$type])){echo 'Not found this type';exit;}

$prefix = $type.'_';
echo '<pre>';
while($d = mysqli_fetch_assoc($q)){  
    $id_user = $d['id_customer'];
    $user_name = $d['user_name'];
    
//    echo $id_user.' '.$user_name.'<br>';
    
    $db = new Db($user_name,'products',true,false);
    if(!$db->exist_db){
//        echo '- Not found database : '.$db->database.'<br>';
        continue;
    }else{
        echo '# Found database : '.$db->database.'<br>';
    }
    
    
    $target = $arr_table_name[$type];
    foreach($target as $table_name){
        
        if($table_name=='Mapping'){
            $map = new $table_name($user_name); 
            update_prim_key($map->attributes,$db);
            update_prim_key($map->characteristics,$db);
            update_prim_key($map->manufacturers,$db);
            update_prim_key($map->conditions,$db);
        }else{
            $cond = isset($table_name::$definition);
            if($cond){ 
                update_prim_key($table_name::$definition,$db);
            }
        }
        echo '<br>';
    }
    

   
    
    
    
    
//    $sql = "ALTER TABLE  offers_category_selected MODIFY COLUMN date_add text;";  
//    echo $sql.'<br>';
//    if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
//    $qx =explode(';',$sql);
//    foreach($qx as $sql){
//        if(isset($_REQUEST['real']))$db->db_exec($sql,false,false); 
//    }
//    
    $db->db_close();
    echo '<br>-------------------------------------------------<br>';
    
}
 
?>
