<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
$system_path = 'system';
if(!defined('BASEPATH'))
define('BASEPATH', str_replace("\\", "/", $system_path));
include(dirname(__FILE__) . '/../../application/config/config.php');   
include(dirname(__FILE__) . '/../../application/config/database.php');  
$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.'); 
mysqli_set_charset($dbx,"utf8");
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");
$path_taxonomy = 'http://www.google.com/basepages/producttype/taxonomy-with-ids.%s.txt';
$all_taxonomy = array(
 'en' =>  'en-US' ,
  'br' =>  'pt-BR' ,
  'cs' =>  'cs-CZ' ,
  'da' =>  'da-DK' ,
  'de' =>  'de-DE' ,
  'es' =>  'es-ES' ,
  'fr' =>  'fr-FR' ,
  'it' =>  'it-IT' ,
  'ja' =>  'ja-JP' ,
  'nl' =>  'nl-NL' ,
  'nn' =>  'no-NO' ,
  'pl' =>  'pl-PL' ,
  'ru' =>  'ru-RU' ,
  'sv' =>  'sv-SE' ,
  'tr' =>  'tr-TR' ,
  'cn' =>  'zh-CN' ,
    );
$file_ver_format = "./taxonomy_files/bak/taxo_%s_ver_%s.txt";
$file_last_ver_format = "./taxonomy_files/taxo_%s_lastver.txt";


function insert_into_record($value_list , $sql){
    global $dbx;
    if(empty($value_list)){
        die('empty value of '.$sql);
    }
    if(empty($sql))return;
    $out = implode(",",$value_list);
    $sql_r = $sql.' '.$out;
    echo $sql_r.'<br>';
    if(isset($_REQUEST['real'])){
        $o = mysqli_query($dbx,$sql_r);
        if($o==false){
            die(mysqli_error($dbx));
        }else{
            echo ' SQL:success<br>';
        }
    }
    
}