<?php
include('./config.php');


$taxonomy_word = array();
$taxonomy_index = array();
$taxonomy_list=array();
$taxonomy_parent_list=array();
$taxonomy_parent_index_list=array();
$gtxCat=array();
$size_of_tax=array();
$glue_cat= '>';
echo '<pre>';
$sql = "truncate google_taxonomy_parent_list ";
if(isset($_REQUEST['real'])){$q = mysqli_query($dbx, $sql);}
$sql = "truncate google_taxonomy_deep ";
if(isset($_REQUEST['real']))$q = mysqli_query($dbx, $sql);
$sql = "select * from google_taxonomy_words where 1 order by iso asc ,gt_index asc";
$q = mysqli_query($dbx, $sql);
while($d=  mysqli_fetch_assoc($q)){
    $taxonomy_word[$d['iso']][$d['words']] = $d['gt_index'];
    $taxonomy_index[$d['iso']][$d['gt_index']] = $d['words'];
}


foreach($all_taxonomy as $iso=>$lang){
        if(empty($size_of_tax[$iso])){
            $size_of_tax[$iso]=0;
        }
      $file_lastv_path = sprintf($file_last_ver_format,$iso);
      if(!file_exists($file_lastv_path))continue;
      $data = file_get_contents($file_lastv_path);
      $array = preg_split ('/$\R?^/m', $data );
      foreach($array as $no=>$line){
          if($no==0)continue;
//          $tmp = explode('-',$line);
//          if(sizeof($tmp)>2){
//              echo 'Something wrong @ '.$file_lastv_path.'<br>'.$no.' # '.$line.'<br>';die();
//          }
          $gtxID =  strtok($line, "-");  
          
          $taxoTxt = str_replace($gtxID.'-','',$line);
          $gtxID = trim($gtxID);
          $cat = explode('>',$taxoTxt);
          $gtxCat[$gtxID] = $taxoTxt;
          $index_list = array();
          $parent_list=array();
          foreach($cat as $i=>$c){ 
              $c=trim($c);
              if(!isset($taxonomy_word[$iso][$c])){
                  $size_of_tax[$iso]++;
                  $taxonomy_word[$iso][$c]=$size_of_tax[$iso];
                  $taxonomy_index[$iso][$size_of_tax[$iso]] = $c; 
                  $index = $size_of_tax[$iso];
              }else{ 
                  $index = $taxonomy_word[$iso][$c];
              }
              if(!empty($index_list)){ 
                  $inx = $taxonomy_parent_list[$iso][$gtxID][$index] = implode($glue_cat,$index_list);
                  $taxonomy_parent_index_list[$iso][$index][$inx]=array('gtxID'=>$gtxID,'parent'=>$inx);
              }else{
                  $taxonomy_parent_list[$iso][$gtxID][$index]=0;
                  $taxonomy_parent_index_list[$iso][$index][0]=array('gtxID'=>$gtxID,'parent'=>0);
              }
              $index_list[] = $index;
              if(sizeof($taxonomy_parent_index_list[$iso][$index])>1){
                  echo 'Something Dup @ '.$file_lastv_path.'<br>'.$no.' # '.$line.'<br>'.$index.'<br>'; 
                  
                  print_r($taxonomy_parent_index_list[$iso][$index]); 
              }
          }
          if(empty($index_list))continue;
          if(isset($taxonomy_list[$iso][$gtxID])){
              echo 'Something wrong gtxID @ '.$file_lastv_path.'<br>'.$no.' # '.$line.'<br>'; 
              echo "Last record: ".print_r($taxonomy_list[$iso][$gtxID],true);
              die();
          }
          $taxonomy_list[$iso][$gtxID] = implode($glue_cat,$index_list);
      }
       
       
}

if(!empty($taxonomy_word)){ 
    $insert_list = array();
    $sql= "replace into google_taxonomy_words (gt_index,iso,words) values ";
    foreach($taxonomy_word as $iso => $txt){
        foreach($txt as $word => $id){
//             $word = mb_convert_encoding ($word, 'UTF-8','ISO-8859-1'  );
             $word = str_replace("'","\'",$word);
            $insert_list[] = "('{$id}','{$iso}','{$word}')";
            if(sizeof($insert_list)>1000){
                insert_into_record($insert_list,$sql);
                unset($insert_list);
                $insert_list=array();
            }
        }
       
    }
    if(sizeof($insert_list)>0){
        insert_into_record($insert_list,$sql);
        unset($insert_list);
        $insert_list=array();
    }
}

if(!empty($taxonomy_list)){
    $insert_list = array();
    $sql= "replace into google_taxonomy_deep (gtx_id,iso,gt_index,deep) values ";
    foreach($taxonomy_list as $iso => $gtxList){
        foreach($gtxList as $gtx_id => $word){
            $tmp = explode($glue_cat,$word);
            foreach($tmp as $k=>$v){
                $insert_list[] = "('{$gtx_id}','{$iso}','{$v}','{$k}')";
            }
            if(sizeof($insert_list)>2000){
                insert_into_record($insert_list,$sql);
                unset($insert_list);
                $insert_list=array();
            }
        }
        
    }
    if(sizeof($insert_list)>0){
        insert_into_record($insert_list,$sql);
        unset($insert_list);
        $insert_list=array();
    }
}

if(!empty($taxonomy_parent_index_list)){
    $insert_list = array();
    $sql= "replace into google_taxonomy_parent_list (iso,gt_index,parent) values ";
    foreach($taxonomy_parent_index_list as $iso => $gtxList){
        foreach($gtxList as $gt_index => $list){
             
            foreach($list as $v){
                $gtx_id = $v['gtxID'];
                $parent = $v['parent'];
                $insert_list[] = "('{$iso}','{$gt_index}','{$parent}')";
            }
            if(sizeof($insert_list)>2000){
                insert_into_record($insert_list,$sql);
                unset($insert_list);
                $insert_list=array();
            }
        }
        
    }
    if(sizeof($insert_list)>0){
        insert_into_record($insert_list,$sql);
        unset($insert_list);
        $insert_list=array();
    }
}


echo 'done';
?>