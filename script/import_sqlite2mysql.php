<?php
ini_set('display_errors',1);
ini_set('memory_limit','2000M'); 
ini_set('max_execution_time', 0);
error_reporting(E_ALL);
$system_path = 'system';
define('BASEPATH', str_replace("\\", "/", $system_path));
include(dirname(__FILE__) . '/../application/config/config.php');
include (dirname(__FILE__).'/../application/libraries/db_mysqli.php'); 
include(dirname(__FILE__) . '/../application/config/database.php');  
require('./export_sqlite2arr.php');
$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.');
$aync = false;
$sql = "select * from configuration ,users where name like 'FEED_BIZ%' and id=id_customer ";
$q = mysqli_query($dbx,$sql); 
$user_list = array();
include(dirname(__FILE__) . '/target_id.php'); 
while($d = mysqli_fetch_assoc($q)){  
    $id_user = $d['id_customer'];
    $user_name = $d['user_name'];
    $user_list[$user_name] = $id_user;
}
 
define("PAGE", basename(__FILE__));
define("FORCETYPE", false);  
$debug = true;
     ini_set('display_errors',1); 
     $dirx  = dirname(__FILE__).'/../assets/apps/users';
     $dir = array_diff(scandir($dirx,1), array('..', '.'));
     $output = array();
     echo 'start<br><pre style="word-wrap: break-word;">';
     $proto=array();
     
     foreach($dir as $k=>$d){
         $d = $dirx.'/'.$d; 
         if(is_dir($d)){
             $d_user = $d;
             $list = array_diff(scandir($d_user), array('..', '.'));
             $user_name = basename($d_user);
             if(!isset($user_list[$user_name])){ continue;  }
             
             $id_user = $user_list[$user_name];
             echo 'Import ID '.$id_user.'<br>';
             if(!in_array($id_user, $u_list)){continue;}
             foreach($list as $file_name){
                 
                 $path = $d_user.'/'.$file_name;
                 $ext = pathinfo($path, PATHINFO_EXTENSION);
                 
                 if($ext == 'db'){ $db_name = basename($file_name,'.'.$ext);
                     if(!in_array($db_name,array( 'orders')))continue;
                     
                      $db_conn = new Db($user_name,$db_name,true,false);
                      if(!$db_conn->exist_db) continue;
                      
                      echo $user_name.' '.$db_name.' '.$path.'<br>';    
                      
                      $obj =  new export_sqlite2arr(array('path'=>$path, "name"=>$path, "writable"=>1, "readable"=>1));
                      $out = array();
                      $obj->export_all_table_sql($out,false);
                      $can_not_create = array();
                      foreach($out as $tbn => $data){
                          if($tbn == 'process_log') continue; 
                          if(!$db_conn->db_table_exists($tbn)){
                          //create table first
//                              echo 'Create Table '.$tbn.'<br>';
                              foreach($data['create'] as $cr_sql){
                                  if(trim($cr_sql)==';') continue;
                                  $type='table';
                                  echo $cr_sql.'<br>';
                                  if(strpos($cr_sql,'INDEX')!==false){
                                      $type = 'index';
                                  }
                                  $cr_sql = add_prefix_table($cr_sql,$tbn,$db_conn->prefix_table,$type);
                                  $cr_sql = replace_quote($cr_sql);
                                  $cr_sql = str_replace('enum(0,1)', "enum('0','1')",$cr_sql);
                                  $t1 = "enum('0','1') NOT NULL DEFAULT 0";
                                  $t2 = "enum('0','1') NOT NULL DEFAULT '0'";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql);
                                  $t1 = "decimal int(11)";
                                  $t2 = "decimal_value int(11)";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql);
                                  $t1 = "int(1";
                                  $t2 = "bigint(1";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql); 
                                  $t1 = "date NOT NULL ";
                                  $t2 = "datetime NOT NULL ";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql);
                                  $t1 = "date,";
                                  $t2 = "datetime,";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql);
                                  $t1 = "date)";
                                  $t2 = "datetime)";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql);
                                  $t1 = "INTEGER NOT NULL";
                                  $t2 = "bigint(11) NOT NULL";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql);
                                  $t1 = " force";
                                  $t2 = " c_force";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql); 
                                  $t1 = " disable";
                                  $t2 = " c_disable";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql); 
                                  $t1 = ",force";
                                  $t2 = ",c_force";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql); 
                                  $t1 = ",disable";
                                  $t2 = ",c_disable";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql); 
                                  $t1 = "`0`";
                                  $t2 = "'0'";
                                  $cr_sql = str_replace($t1,$t2,$cr_sql); 
                                  $result=false;
                                  $cr_sql = str_replace('`','',$cr_sql);
                                  if(isset($_REQUEST['real']))$result = $db_conn->db_exec($cr_sql,false,false);
                                  echo 'Run : '.$cr_sql;
                                  if($result === false){ 
                                      $can_not_create[] = $cr_sql.'<br>';
                                      echo 'Result error X : '.$db_conn->_error().'<br>------------------------------------------------<br>';
                                  } 
                              }
                              
                          }else{
//                              echo 'skip '.$tbn.'<br>';
                          }
                         
                          
                          $update = "ALTER TABLE products_amazon_category_selected MODIFY COLUMN id_category_selected bigint auto_increment;
                                    ALTER TABLE products_amazon_model MODIFY COLUMN id_model bigint auto_increment;
                                    ALTER TABLE products_amazon_mapping_carrier MODIFY COLUMN id_mapping bigint auto_increment;
                                    ALTER TABLE products_amazon_status MODIFY COLUMN id_status bigint auto_increment;
                                    ALTER TABLE products_amazon_log MODIFY COLUMN id_log bigint auto_increment;
                                    ALTER TABLE products_amazon_request_report_log MODIFY COLUMN id_log bigint auto_increment;
                                    ALTER TABLE products_amazon_validation_log MODIFY COLUMN id_log bigint auto_increment;
                                    ALTER TABLE products_amazon_submission_list_log MODIFY COLUMN id_log bigint auto_increment;
                                    ALTER TABLE products_amazon_profile MODIFY COLUMN id_profile bigint auto_increment;
                                    ALTER TABLE products_amazon_log_display MODIFY COLUMN field_id bigint auto_increment PRIMARY KEY;
                                    ALTER TABLE products_amazon_repricing_log MODIFY COLUMN id_log bigint auto_increment  ;";
                          $db_conn->db_exec($update,false);
                          if($tbn=='currency'){
                          $update = "ALTER TABLE  products_currency MODIFY COLUMN is_default tinyint(1);";
                          $db_conn->db_exec($update,false,false);
                           $update = "ALTER TABLE  offers_currency MODIFY COLUMN is_default tinyint(1);";
                          $db_conn->db_exec($update,false,false);
                          }
                          
                          if($tbn=='category_selected'){
                           
                           $update = "ALTER TABLE  offers_category_selected MODIFY COLUMN date_add varchar(32);";
                          $db_conn->db_exec($update,false,false);
                          }
                          if($tbn == 'ebay_product_details'){
                            $update = "ALTER TABLE  ebay_product_details MODIFY COLUMN currency_rate_from float default '1';";
                            $db_conn->db_exec($update,false,false);  
                            $update = "ALTER TABLE  ebay_product_details MODIFY COLUMN currency_rate_to float default '1';";
                            $db_conn->db_exec($update,false,false);  
                          }
//                          if($tbn == 'amazon_product_option'){
//                              $update = "ALTER TABLE  products_amazon_product_option change `force` c_force tinyint(4);";
//                            $db_conn->db_exec($update,false,false);  
//                            $update = "ALTER TABLE  products_amazon_product_option change `disable` c_disable tinyint(4);";
//                            $db_conn->db_exec($update,false,false);  
//                          }
//                          if($tbn!='orders')continue;
                          if(!in_array($tbn, array('flag_update_log','dev_log','ebay_statistics_combination'))){
                          if(isset($_REQUEST['real']))
                          $obj->run_export_insert_sql(array(0=>$tbn),$db_conn);
                          }
//                          foreach($data['insert'] as $k => $ins_sql){
//                            $ins_sql = replace_quote($ins_sql);
//                            $ins_sql = add_prefix_table($ins_sql,$tbn,$db_conn->prefix_table);
//                            $t1 = "decimal int(11)";
//                            $t2 = "decimal_value int(11)";
//                            $ins_sql = str_replace($t1,$t2,$ins_sql);
//                              unset($data['insert'][$k]);
//                              if(trim($ins_sql)==';') continue;
////                              echo $ins_sql.'<br>';
////                              exit;
//                                $result = $db_conn->db_query_result_str($ins_sql);
//                                  if($result === false){
//                                      echo $ins_sql.'<br>';
//                                      echo 'Result error: '.$db_conn->_error().'<br>------------------------------------------------<br>';
//                                      exit;
//                                  } 
//                          }
                          
                          unset($out[$tbn]);
                      }
                       foreach($can_not_create as $v)echo $v;
                      
                 }
                 
             } 
         }
     }
     
     
     echo '</pre>';
     
     function replace_quote($str,$replace_to ='`'){
         $str =  str_replace('"', $replace_to, $str);
         $str =  str_replace("'", $replace_to, $str);
         $str =  str_replace("``","''", $str);
         return $str;
     }
     
     function add_prefix_table($sql ,$tbn,$prefix,$type){
         $sql = str_replace("  ", " ", $sql);
         $sql = str_replace("  ", " ", $sql);
         $sql = str_replace("CREATE TABLE ".$tbn, "CREATE TABLE ".$prefix.$tbn, $sql);
         $sql = str_replace("CREATE TABLE '".$tbn."'", "CREATE TABLE '".$prefix.$tbn."'", $sql);
         $sql = str_replace('CREATE TABLE "'.$tbn.'"', 'CREATE TABLE "'.$prefix.$tbn.'"', $sql);
         $sql = str_replace('ON "'.$tbn.'"', 'ON "'.$prefix.$tbn.'"', $sql);
         $sql = str_replace("ON '".$tbn."'", "ON '".$prefix.$tbn."'", $sql);
         $sql = str_replace("CONSTRAINT '".$tbn."_uniques' UNIQUE", " PRIMARY KEY ", $sql);
         $sql = str_replace('CONSTRAINT "'.$tbn.'_uniques" UNIQUE', ' PRIMARY KEY ', $sql); 
         if($type!=='index'){
         $sql = str_replace(';'," ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;",$sql);
         }
         if($tbn == 'amazon_inventory'&&$type=='table'){
             $sql = str_replace('("sku", "id_country", "id_shop")' , '(`id_product`,`id_product_attribute`,`sku`, `id_country`, `id_shop`)',$sql);
         }
         if($tbn == 'amazon_inventory'&&$type=='index'){
             $sql = str_replace('("sku" ASC, "id_country" ASC, "id_shop" ASC)' , '(`id_product`,`id_product_attribute`,`sku`, `id_country`, `id_shop`)',$sql);
         }
         if($tbn == 'ebay_statistics'&&$type=='table'){
             $sql = str_replace('PRIMARY KEY (batch_id, id_product, id_combination, id_site, id_shop, ebay_user, name_product, code)' ,
                     'PRIMARY KEY (batch_id, id_product, id_combination, sku_export, id_site, id_shop, ebay_user, name_product, code)',$sql);
         }
         
         
         if($tbn == 'ebay_synchronization_attribute'&&$type=='table'){
             $sql = str_replace('SKU varchar(255) NOT NULL,' , 'SKU varchar(16) NOT NULL,',$sql);
             $sql = str_replace('ebay_user varchar(255) NOT NULL,' , 'ebay_user varchar(50) NOT NULL,',$sql); 
         }
         
         
         
//         echo __LINE__.' '.$prefix.' '.$sql.'<br>';
         return $sql;
     }