<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
$system_path = 'system';
define('BASEPATH', str_replace("\\", "/", $system_path));
include(dirname(__FILE__) . '/../application/config/config.php');
$backend_host = '127.0.0.1:3001';
$create_profiles_product_url = (@$config['ssl_protocol'] ? 'https://' : 'http://') . $backend_host.'/createuser/product/';
$create_profiles_offer_url = (@$config['ssl_protocol'] ? 'https://' : 'http://') . $backend_host.'/createuser/offer/';
$create_profiles_order_url = (@$config['ssl_protocol'] ? 'https://' : 'http://') . $backend_host.'/createuser/order/';

include (dirname(__FILE__).'/../application/libraries/db_mysqli.php');
include(dirname(__FILE__) . '/../application/config/database.php');
$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.');
$aync = false;
include(dirname(__FILE__) . '/target_id.php');
$sql = "select * from configuration ,users where name like 'FEED_BIZ%' and id=id_customer ";
$q = mysqli_query($dbx,$sql);


while($d = mysqli_fetch_assoc($q)){
    $id_user = $d['id_customer'];
    $user_name =  $d['user_name'];
//    if(!in_array($id_user,array(9)))continue;
//    echo $id_user.' '.$user_name.'<br>';

    $db = new Db($user_name,'',true,false);
    if(!$db->exist_db){
//        echo '- Not found database : '.$db->database.'<br>';
        continue;
    }else{
//        echo '# Found database : '.$db->database.'<br>';
    }

    $sql = "select sku  ,count(sku) as num,id_country  from amazon_repricing_report group by sku,id_country order by count(sku) desc   ";
//    echo $sql.'<br>';
    $rows = $db->db_query_string_fetch($sql);
    foreach($rows as $r){
        if($r['num']<=8)continue;
        $sku = $r['sku'];
        $id_country = $r['id_country'];

//        echo 'Rows: '. $r['num'].'<br>'."\n";
        $sql = "select sku , date_upd from amazon_repricing_report where sku = '$sku' and id_country = '$id_country' order by date_upd desc limit 4";
//        echo $sql.'<br>';
        $rd = $db->db_query_string_fetch($sql);
        $dl = array();
        if(is_array($rd)){
            foreach($rd as $r){
                $dl[$r['date_upd']] = $r['date_upd'];
            }
            if(!empty($dl)){
                $date = implode("','",$dl);
                $sql = "delete from amazon_repricing_report where sku  = '$sku' and id_country = '$id_country' and date_upd not in ('$date') ";
//                echo $sql.'<br>'."\n";
                $out = $db->db_exec($sql,FALSE,FALSE);
            }
        }
    }

    $sql = "select id_country from amazon_fba_log group by id_country";
    $country = $db->db_query_string_fetch($sql);
    $dl = $id_l= array();
    $sku_list =$id_list= array();
    foreach($country as $c){
        $id_country = $c['id_country'];

        $sql = "SELECT * FROM ( SELECT
			t.sku,
			t.id_log,
			t.date_add,
			t.id_country,
			@ROW := CASE
		WHEN @prev1 = t.sku THEN
			@ROW
		ELSE
			0
		END + 1 rn,
		@prev1 := t.sku
	FROM
		amazon_fba_log t
	CROSS JOIN (SELECT @ROW := 0, @prev1 := NULL) c
	WHERE
		t.id_country = '$id_country'
	AND t.date_add < DATE_ADD(NOW(), INTERVAL - 8 DAY)
	ORDER BY
		sku ASC,
		date_add DESC ) src where rn <= 2";
//        echo $sql."\n";continue;
        $rows = $db->db_query_string_fetch($sql);
        $id_list= array();
        foreach($rows as $r){

            $id_list[$r['id_log']] = $r['id_log'];

        }
        if(!empty($id_list)){
            $id = implode("','",$id_list);
            $sql = "delete from amazon_fba_log where   id_log not in ('$id') and date_add < DATE_ADD(NOW( ), INTERVAL -8 DAY ) and id_country = '$id_country'";
//             echo $sql.";\n";
            $out = $db->db_exec($sql,FALSE,FALSE);

        }
    }
/*
    $sql = "select * from (SELECT
	sku,
	count(sku) AS num,
	id_country,
	process,
	type
FROM
	amazon_fba_log
WHERE
	date_add < DATE_ADD(NOW(), INTERVAL - 8 DAY)
GROUP BY
	sku,
	id_country,
	process,
	type

ORDER BY
	count(sku) DESC
  ) as list
where list.num > 5 ";

    $rows = $db->db_query_string_fetch($sql);
    foreach($rows as $r){
        if($r['num']<5)continue;
        $sku = $r['sku'];
        $id_country = $r['id_country'];
        $process = $r['process'];
        $type = $r['type'];

//        echo 'Rows: '. $r['num'].'<br>'."\n";
        $sql = "select sku , date_add,id_log from amazon_fba_log where sku = '$sku' and id_country = '$id_country' and process = '$process' and type = '$type' order by date_add desc limit 2";
//        echo $sql.'<br>'."\n";
        $rd = $db->db_query_string_fetch($sql);
        $dl = $id_l= array();
        if(is_array($rd)){
            foreach($rd as $r){
                $dl[$r['date_add']] = $r['date_add'];
                $id_l[$r['id_log']] = $r['id_log'];
            }
            if(!empty($dl)){
                $date = implode("','",$dl);
                $id = implode("','",$id_l);
                $sql = "delete from amazon_fba_log where sku  = '$sku' and id_country = '$id_country' and process = '$process' and type = '$type' and id_log not in ('$id') and date_add < DATE_ADD(NOW( ), INTERVAL -8 DAY )";// and date_add not in ('$date') ";
//                echo $sql.'<br>'."\n";
                $out = $db->db_exec($sql,FALSE,FALSE);
            }
        }
    }

*/
    $db->db_close();

}


?>
