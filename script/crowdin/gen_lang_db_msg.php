<?php
  include(dirname(__FILE__) . '/config.php');   
  
$sql = "select * from crowdin_approved";
$app_by_key=array();
$app_by_text=array();
$res = mysqli_query($dbx,$sql);
while($d = mysqli_fetch_assoc($res)){
    $app_by_key[$d['type']][$d['lang']][$d['id_key']] = $d;
    $app_by_text[$d['type']][$d['lang']][$d['base_text']] = $d;
}

$sql ="select * from crowdin_translate where found_using in (3) and skip != 2 order by lang asc,filename asc , line asc ";
$all_text = array();
$all_text_base = array();
$res = mysqli_query($dbx,$sql); 
while($d = mysqli_fetch_assoc($res)){
    $all_text[$d['id_key']] = $d; 
} 
$updated_base_text = array();
echo '<pre>'; 
foreach($app_by_key[2] as $lang=>$row){
    foreach($row as $id_key => $d){
        echo $d['base_text'].'<br>';
        if(isset($all_text[$d['id_key']])){
            $base_text = $d['base_text'];
            $t = $all_text[$d['id_key']];
            $lang = $d['lang']; 
            $msg_lang = isset($msgl_conv[$lang])?$msgl_conv[$lang]:'en';
            $tmp = $t['filename'];
            $tmp = explode('#',$tmp);
            $table =$tmp[0];
            $field = $tmp[1];
            $v = str_replace('{BR}','<br>',  $field);
            $v = str_replace('{B}','<b>',  $v);
            $v = str_replace('{/B}','</b>',  $v);
            $v = str_replace('{P}','<p>',  $v);
            $field = str_replace('{/P}','</p>',  $v);
            $tmp = $t['line'];
            $tmp = explode('#-#',$tmp);
            $row_id = $tmp[0];
            $type = $tmp[1];
            $text = mysqli_escape_string($dbx, $d['word']);
            $v = str_replace('{BR}','<br>',  $text);
            $v = str_replace('{B}','<b>',  $v);
            $v = str_replace('{/B}','</b>',  $v);
            $v = str_replace('{P}','<p>',  $v);
            $text = str_replace('{/P}','</p>',  $v);
            echo "#$table<br>"; 
            
            if($table=='message'){
                $sql_chk = "select * from {$table} where message_code = '{$row_id}' and message_language = '{$msg_lang}' and message_type = '{$type}' limit 1";
                $res = mysqli_query($dbx,$sql_chk);
                $data = mysqli_fetch_assoc($res); 
                $old_text = $data[$field]; 
                if(mysqli_num_rows($res)>0){
                    $sql = "update {$table} set {$field} = '$text' where message_code = '{$row_id}' and message_language = '{$msg_lang}' and message_type = '{$type}'; ";
                    echo $sql.'<br>';
                }else{
                    $sql = "replace into {$table} (message_code,message_language,message_type,{$field}) values ('{$row_id}','{$msg_lang}','{$type}','{$text}')";
                    echo $sql.'<br>';
                }
                 mysqli_query($dbx,$sql);

                if(!empty($old_text)){
                    $updated_base_text[]=$old_text;
                    $sql = "update {$table} set {$field} = '$text' where   message_language = '{$msg_lang}' and {$field} = '$old_text'; ";
                    echo $sql.'<br>';
                    mysqli_query($dbx,$sql);
                }
            }else if($table=='ebay_error_resolutions'){
                 
                $sql_chk = "select * from {$table} where error_code = '{$row_id}' and error_lang = '{$msg_lang}' /*and id_error = '{$type}'*/ limit 1";
                echo $sql_chk."\n";
                $res = mysqli_query($dbx,$sql_chk);
                $data = mysqli_fetch_assoc($res); 
                $old_text = $data[$field]; 
                $sql='';
                if(mysqli_num_rows($res)>0){
                    $sql = "update {$table} set {$field} = '$text' where error_code = '{$row_id}' and error_lang = '{$msg_lang}'  /*and id_error = '{$type}'*/; ";
                    echo $sql.'<br>';
                    mysqli_query($dbx,$sql);
                }
//                else{
//                    $sql = "replace into {$table} (error_code,error_lang,id_error,{$field}) values ('{$row_id}','{$msg_lang}','{$type}','{$text}')";
//                    echo $sql.'<br>';
//                }
                

                if(!empty($old_text)){
                    $updated_base_text[]=$old_text;
                    $all_field = array('error_details','error_resolution','other');
                    foreach($all_field as $f){
                        $sql = "update {$table} set {$f} = '$text' where   error_lang = '{$msg_lang}' and {$f} = '$old_text'; ";
                        echo $sql.'<br>';
                        mysqli_query($dbx,$sql);
                    }
                }
            }
        }
    }
}

if(!empty($updated_base_text)){
    foreach($updated_base_text as $k=>$v){
        $updated_base_text[$k] = mysqli_escape_string($dbx, $v);
    }
    $sql = "update crowdin_translate set skip = 9 where base_text in ('".implode("','",$updated_base_text)."')";
    echo $sql;
    mysqli_query($dbx,$sql);
}