<?php
include(dirname(__FILE__) . '/config.php');

$sql = "select * from crowdin_approved";
$app_by_key=array();
$app_by_text=array();
$res = mysqli_query($dbx,$sql);
while($d = mysqli_fetch_assoc($res)){
     
    $app_by_text[$d['type']][$d['lang']][$d['base_text']] = $d['word'];
}
//echo '<pre>';
//print_r($app_by_text);exit;
$sql = "select c.url,c.filename,c.translated,a.* from crowdin_approved a , crowdin_translate c where   a.type in (1) and a.id_key = c.id_key and c.skip in (0,9)   order by lang,base_text";
$res = mysqli_query($dbx,$sql);


//foreach($target_lang_list as $lang => $iso){
//$target_file = dirname(__FILE__) ."/upload/feed_biz_tran2_{$iso}.csv";
$target_file = dirname(__FILE__) ."/approved_feed_biz_all_lang.csv";
                             $fp = fopen($target_file, 'w'); 
                             
while($d = mysqli_fetch_assoc($res)){
     $text[$d['id_key']] = $d;
     $text_lang[$d['id_key']][$d['lang']] = $d;
     if(strpos('_lang.php',$d['filename'])!==false)continue;
}               
 $dup = array();
                            foreach($text as $k => $d){
                                if(isset($dup[$d['base_text']]))continue;
                                $dup[$d['base_text']] = true;
                                 
                                    $comm='FA '; 
                                if(isset($d['key_full'])){
                                    $comm .= 'Array key: '.$d['key_full'].' | ';
                                    if(!empty($d['url'])){
                                        if(strpos($d['url'],'index.php/admin') === false)
                                        $comm .= ' Contain in: https://'.$d['url'].' | ';
                                    }
    //                                if($d['type']=='translated') continue;
                                    if($d['comm']==1){
                                        $comm.="*NOTE: please leave %s in the same position*";
                                    }elseif($d['comm']==2){
                                        $comm.="*NOTE: please leave {SOME_TEXT} in the same position*";
                                    }
                                    elseif($d['comm']==3){
                                        $comm.="*NOTE: please leave _SOMETEXT_ in the same position*";
                                    }
                                }
                                
                                $base_text = $d['base_text'];
                                $ll = array();
                                foreach($lang_list as $lang=>$s){
                                    $word = isset($app_by_text[1][$lang][$d['base_text']])?$app_by_text[1][$lang][$d['base_text']]:'';
                                    $word = trim($word);
                                    if($word==''){ 
                                    $word = isset($text_lang[$d['id_key']][$lang]['translated'])?trim($text_lang[$d['id_key']][$lang]['translated'],'.'):'';
                                    $word = trim($word);
                                    $ll[]=$word;
                                    }else{
                                        $ll[] = $word;
//                                        if($lang=='english')
//                                        $base_text = $word;
                                    }
                                }
                                $out = array($d['id_key'],$base_text,$comm );
                                $out = array_merge($out,$ll);
                                foreach($out as $k=>$v){
                                    $out[$k] = iconv("ISO-8859-1//TRANSLIT","UTF-8",$v);
                                }
                                fputcsv($fp,$out );
                            }
                            
                            
                            if(!empty($fp))fclose($fp); 
 
//}
                            
                            
$sql = "select a.*,c.comm,c.url,c.line,c.filename  from crowdin_approved a , crowdin_translate c where   a.type in (2) and a.id_key = c.id_key and c.skip in (0,9)   order by lang,base_text";
$res = mysqli_query($dbx,$sql);
 
//foreach($target_lang_list as $lang => $iso){
//$target_file = dirname(__FILE__) ."/upload/feed_biz_tran2_{$iso}.csv";
$target_file = dirname(__FILE__) ."/approved_feed_biz_db_msg.csv";
                            $fp = fopen($target_file, 'w'); 
   $text = array();  
   $text_lang = array();
while($d = mysqli_fetch_assoc($res)){
     $text[$d['id_key'].$d['filename']] = $d;
     $text_lang[$d['id_key'].$d['filename']][$d['lang']] = $d;
}     
$dup = array();
                            foreach($text as $k => $d){
                                if(isset($dup[$d['base_text']]))continue;
                                $dup[$d['base_text']] = true;
                                $comm = '';
//                                if($d['type']=='translated') continue;
                                if($d['comm']==1){
                                    $comm="*NOTE: please leave %s in the same position*";
                                }elseif($d['comm']==2){
                                    $comm="*NOTE: please leave {SOME_TEXT} in the same position*";
                                }
                                elseif($d['comm']==3){
                                    $comm="*NOTE: please leave _SOMETEXT_ in the same position*";
                                }
                                
                                if(!empty($d['url'])){
                                    if(strpos($d['url'],'index.php/admin') === false)
                                    $comm .= ' Contain in: https://'.$d['url'].' | ';
                                }
								
								if(!empty($d['filename'])){
                                     
                                    $comm .= ' Field type: '.str_replace('message#','',$d['filename']).'   ';
                                }
								if(!empty($d['line'])){
                                     
                                    $comm .= ' | Group Index: https://crowdin.com/translate/feedbiz/7/en-enus#q='. md5($d['line']) .'   ['.$d['line'].']';
                                }
                                $lang='english';
                                $base_text = isset($text_lang[$d['id_key'].$d['filename']][$lang]['word'])?$text_lang[$d['id_key'].$d['filename']][$lang]['word']:$d['base_text'];
                                $ll = array();
                                foreach($lang_list as $lang=>$s){
                                     
                                    $ll[] = isset($text_lang[$d['id_key'].$d['filename']][$lang])?trim($text_lang[$d['id_key'].$d['filename']][$lang]['word'],'.'):'';
                                    
//                                    $word = isset($app_by_text[1][$lang][$d['base_text']])?$app_by_text[1][$lang][$d['base_text']]:'';
//                                    if($word==''){
//                                    $ll[] = isset($text_lang[$d['id_key']][$lang])?trim($text_lang[$d['id_key']][$lang]['translated'],'.'):'';
//                                    }else{
//                                        $ll[] = $word;
////                                        if($lang=='english')
////                                        $base_text = $word;
//                                    }
                                }
                                $out = array($d['id_key'],$base_text,$comm );
                                
//                                $out = array($d['id_key'],$d['base_text'],$comm );
//                                $ll = array();
//                                foreach($lang_list as $lang=>$s){
//                                    $ll[] = isset($text_lang[$d['id_key'].$d['filename']][$lang])?trim($text_lang[$d['id_key'].$d['filename']][$lang]['translated'],'.'):'';
//                                }
                                $out = array_merge($out,$ll);
                                foreach($out as $k=>$v){
                                    $v = str_replace(array('<br/>','<br>'), '{BR}', $v);
                                    $v = str_replace(array('<b>'), '{B}', $v);
                                    $v = str_replace(array('</b>'), '{/B}', $v);
                                    $out[$k] = iconv("ISO-8859-1//TRANSLIT","UTF-8",$v);
                                }
                                fputcsv($fp,$out );
                            } 