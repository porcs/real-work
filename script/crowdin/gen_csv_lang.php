<?php
include(dirname(__FILE__) . '/config.php');


$sql = "select * from crowdin_approved";
$app_by_key=array();
$app_by_text=array();
$res = mysqli_query($dbx,$sql);
$conf_key=array();
while($d = mysqli_fetch_assoc($res)){
     
    $app_by_text[$d['type']][$d['lang']][$d['base_text']] = $d['word'];
}

//foreach($target_lang_list as $lang => $iso){
//$target_file = dirname(__FILE__) ."/upload/feed_biz_tran2_{$iso}.csv";
$target_file = dirname(__FILE__) ."/feed_biz_all_lang.csv";
                             $fp = fopen($target_file, 'w'); 
$sql = "select a.* from crowdin_approved a , crowdin_translate c where   a.type in (1) and a.id_key = c.id_key and c.skip in (0,9) and confirm1 = '1'  order by lang,base_text";
$res = mysqli_query($dbx,$sql);         
$approved = array();
while($d = mysqli_fetch_assoc($res)){
    $approved[$d['id_key']][$d['lang']]=true;
     $text[$d['id_key']] = $d;
     $text_lang[$d['id_key']][$d['lang']] = $d; 
}
$sql = "select * from crowdin_translate where   skip in (0,9) and found_using in (1,2,4,9) and confirm1 = '1'  order by url,filename , line";
$res = mysqli_query($dbx,$sql);                
while($d = mysqli_fetch_assoc($res)){
    if(isset($text[$d['id_key']])){
        foreach($d as $k =>$v){
            if(!isset($text[$d['id_key']][$k]) && $d['lang'] =='english'){
                $text[$d['id_key']][$k] = $v;
            }
            if(!isset($text_lang[$d['id_key']][$d['lang']][$k])){
                $text_lang[$d['id_key']][$d['lang']][$k] = $v;
            }
        } 
    }else{
     if(strpos('_lang.php',$d['filename'])!==false)continue;
     if( $d['lang'] =='english'){
        $text[$d['id_key']] = $d;
     }
     $text_lang[$d['id_key']][$d['lang']] = $d;
     
    }
}               
 $dup = array();
                            foreach($text as $k => $d){
                                if(isset($d['avail_dup']) && $d['avail_dup']==0){
                                if(isset($dup[trim($d['base_text'],'.')]))continue;
                                }
                                $dup[trim($d['base_text'],'.')] = true;
                                
                                $comm='';
                                 
                                if(isset($d['key_full'])){
                                $comm .= 'Array key: '.$d['key_full'].' | ';
                                if(!empty($d['url'])){
                                    if(strpos($d['url'],'index.php/admin') === false)
                                    $comm .= ' Contain in: https://'.$d['url'].' | ';
                                }
                                if(!empty($d['filename'])){
                                    $comm .= ' File: '.str_replace('.php','', $d['filename']).' | ';
                                }
//                                if($d['type']=='translated') continue;
                                if($d['comm']==1){
                                    $comm.="*NOTE: please leave %s in the same position*";
                                }elseif($d['comm']==2){
                                    $comm.="*NOTE: please leave {SOME_TEXT} in the same position*";
                                }
                                elseif($d['comm']==3){
                                    $comm.="*NOTE: please leave _SOMETEXT_ in the same position*";
                                }
                                }
                                if(isset($approved[$d['id_key']])){
                                    $comm = trim($comm,'|');
                                    $comm = trim($comm);
                                    $comm .=" | Aprroved Lang: ";
                                    $lxx = array();
                                    foreach($approved[$d['id_key']] as $k =>$v){
                                        $lxx[$k]=$k;
                                    }
                                    $comm .=implode(",",$lxx);
                                }
                                
//                                $out = array($d['id_key'],$d['base_text'],$comm );
//                                $ll = array();
//                                foreach($lang_list as $lang=>$s){
//                                    $ll[] = isset($text_lang[$d['id_key']][$lang])?trim($text_lang[$d['id_key']][$lang]['translated'],'.'):'';
//                                }
//                                $out = array_merge($out,$ll);
                                $base_text = $d['base_text'];
                                $ll = array();
                                foreach($lang_list as $lang=>$s){
                                    $word = isset($app_by_text[1][$lang][$d['base_text']])?$app_by_text[1][$lang][$d['base_text']]:'';
                                    if($word==''){
                                    $ll[] = isset($text_lang[$d['id_key']][$lang])?trim($text_lang[$d['id_key']][$lang]['translated'],'.'):'';
                                    }else{
                                        $ll[] = $word;
//                                        if($lang=='english')
//                                        $base_text = $word;
                                    }
                                }
                                $out = array($d['id_key'],$base_text,$comm );
                                $out = array_merge($out,$ll);
                                foreach($out as $k=>$v){
                                    $out[$k] = iconv("ISO-8859-1//TRANSLIT","UTF-8",$v);
                                }
                                if(strpos(  strtolower(implode(' ',$out)),'debug')!==false){
                                    echo '#Skip : '.implode(' ',$out).'<br>';
                                    continue;
                                }
                                $conf_key[$out[0]]=$out[0];
                                fputcsv($fp,$out );
                            }
                            
                            
                            if(!empty($fp))fclose($fp); 
 
//}
$sql = "select a.*,c.comm,c.url,c.line,c.filename  from crowdin_approved a , crowdin_translate c where   a.type in (2) and a.id_key = c.id_key and c.skip in (0,9) and confirm1 = 1   order by lang,base_text";
$res = mysqli_query($dbx,$sql);         
$approved = array();
while($d = mysqli_fetch_assoc($res)){
    $approved[$d['filename'].$d['line']][$d['lang']]=true;
     $text[$d['filename'].$d['line']] = $d;
     $text_lang[$d['filename'].$d['line']][$d['lang']] = $d; 
}           
                            
$sql = "select * from crowdin_translate where   skip in (0,9) and found_using in (3) and confirm1 = 1 group by key_full,filename,lang order by filename , line";
$res = mysqli_query($dbx,$sql);
 
//foreach($target_lang_list as $lang => $iso){
//$target_file = dirname(__FILE__) ."/upload/feed_biz_tran2_{$iso}.csv";
$target_file = dirname(__FILE__) ."/feed_biz_db_msg.csv";
                            $fp = fopen($target_file, 'w'); 
   $text = array();  
   $text_lang = array();
while($d = mysqli_fetch_assoc($res)){
     if(isset($text[$d['filename'].$d['line']][$d['lang']]))continue;
     $text[$d['filename'].$d['line']] = $d;
     $text_lang[$d['filename'].$d['line']][$d['lang']] = $d;
     
}     
 $dup = array();
                            foreach($text as $k => $d){
                                if(isset($dup[$d['base_text']]))continue;
                                if(strpos($d['base_text'],'.vtt')!==false)continue; 
                                $dup[$d['base_text']] = true;
                                $comm = '';
//                                if($d['type']=='translated') continue;
                                if($d['comm']==1){
                                    $comm="*NOTE: please leave %s in the same position*";
                                }elseif($d['comm']==2){
                                    $comm="*NOTE: please leave {SOME_TEXT} in the same position*";
                                }
                                elseif($d['comm']==3){
                                    $comm="*NOTE: please leave _SOMETEXT_ in the same position*";
                                }
                                
                                if(!empty($d['url'])){
                                    if(strpos($d['url'],'index.php/admin') === false)
                                    $comm .= ' Contain in: https://'.$d['url'].' | ';
                                }
								
								if(!empty($d['filename'])){
                                     
                                    $comm .= ' Field type: '.str_replace('message#','',$d['filename']).'   ';
                                }
								if(!empty($d['line'])){
                                     
                                    $comm .= ' | Group Index: https://crowdin.com/translate/feedbiz/7/en-enus#q='. md5($d['line']) .'   ['.$d['line'].']';
                                }
                                
                                $base_text = $d['base_text'];
                                $ll = array();
                                foreach($lang_list as $lang=>$s){
                                    $word = isset($app_by_text[2][$lang][$d['base_text']])?$app_by_text[2][$lang][$d['base_text']]:'';
                                    if($word==''){
                                    $ll[] = isset($text_lang[$d['id_key']][$lang])?trim($text_lang[$d['id_key']][$lang]['translated'],'.'):'';
                                    }else{
                                        $ll[] = $word;
                                    }
//                                    $word = isset($app_by_text[1][$lang][$d['base_text']])?$app_by_text[1][$lang][$d['base_text']]:'';
//                                    if($word==''){
//                                    $ll[] = isset($text_lang[$d['id_key']][$lang])?trim($text_lang[$d['id_key']][$lang]['translated'],'.'):'';
//                                    }else{
//                                        $ll[] = $word;
////                                        if($lang=='english')
////                                        $base_text = $word;
//                                    }
                                }
                                $out = array($d['id_key'],$base_text,$comm );
                                
//                                $out = array($d['id_key'],$d['base_text'],$comm );
//                                $ll = array();
//                                foreach($lang_list as $lang=>$s){
//                                    $ll[] = isset($text_lang[$d['id_key'].$d['filename']][$lang])?trim($text_lang[$d['id_key'].$d['filename']][$lang]['translated'],'.'):'';
//                                }
                                $out = array_merge($out,$ll);
                                foreach($out as $k=>$v){
                                    $v = str_replace(array('<br/>','<br>'), '{BR}', $v);
                                    $v = str_replace(array('<b>'), '{B}', $v);
                                    $v = str_replace(array('</b>'), '{/B}', $v);
                                    $v = str_replace(array('<p>'), '{P}', $v);
                                    $v = str_replace(array('</p>'), '{/P}', $v);
                                    $out[$k] = iconv("ISO-8859-1//TRANSLIT","UTF-8",$v);
                                }
                                if(strpos(  strtolower(implode(' ',$out)),'debug')!==false){ 
                                    echo '#Skip : '.implode(' ',$out).'<br>';
                                    continue;
                                }
                                $conf_key[$out[0]]=$out[0];
                                fputcsv($fp,$out );
                            }
                            
                            
                            if(!empty($fp))fclose($fp); 
                    echo '---End---<br>';       

;

//$query = "update crowdin_translate set sent = 1 where   id_key in ('".implode("','",$conf_key)."')";
////echo "\n".$query.";\n";
//$res = mysqli_query($dbx,$query);
//if(!$res){
//    echo "\n".'<br>## Error:'.mysqli_error($dbx);exit;
//}
if(isset($_REQUEST['upload'])){
    echo '---Upload csv to crowdin---<br>';
exec('php upload_file2crowdin.php');
exec('php upload_filedb2crowdin.php');
}