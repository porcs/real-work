<?php

include(dirname(__FILE__) . '/config.php');  
mysqli_query($dbx,"SET CHARACTER SET utf8;");
$sql_list=array();
$line=0;
echo '<pre>';
foreach($project_group_file as $v){
    $path = "./translations/".$v;
    if(!file_exists($path)){
        continue;
    }
    $file = fopen($path,"r");
    while( $r = fgetcsv($file)){
        $line++;
        $id_key = $r[0];
        $base_text = mysqli_escape_string($dbx, $r[1]);
        $text = array();
        $text['english'] = $r[3];
        $text['french'] = $r[4];
        $text['german'] = $r[5];
        $text['spanish'] = $r[6];
        $text['italian'] = $r[7];
        foreach($text as $lang =>$v){
            if(trim($v)=='')continue;
            $v = mysqli_escape_string($dbx, $v);
            $sql_list[] = "('$id_key','$base_text','$lang','$v',now(),1)";
        }

    }
}
$file = fopen("./translations/feed_biz_db_msg.csv","r");
while( $r = fgetcsv($file)){
    $line++;
    $id_key = $r[0];
    $base_text = mysqli_escape_string($dbx, $r[1]);
    $text = array();
    $text['english'] = $r[3];
    $text['french'] = $r[4];
    $text['german'] = $r[5];
    $text['spanish'] = $r[6];
    $text['italian'] = $r[7];
    foreach($text as $lang =>$v){
        if(trim($v)=='')continue;
        $v = mysqli_escape_string($dbx, $v);
        $sql_list[] = "('$id_key','$base_text','$lang','$v',now(),2)";
    }
    
}
$sql_insert_list=array();
$b=0;
foreach($sql_list as $k=>$val){ 
        $sql_insert_list[$b][] = $val;
        if($k%100==0&&$k!=0){
            $b++;
        }
        
}
foreach($sql_insert_list as $b => $r){
    $sql = " replace into crowdin_approved (id_key,base_text,lang,word,last_update,type) values ".  implode(',', $r);
            echo $sql.'<br>';
            mysqli_query($dbx,$sql);
             
}
fclose($file);