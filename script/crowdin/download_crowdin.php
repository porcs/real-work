<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
include(dirname(__FILE__) . '/config.php');   

//built
$cmd = 'export';
$request_url = "https://api.crowdin.com/api/project/{$proj_name}/{$cmd}?key={$api_key}";
echo 'Build url:'.$request_url.'<br>';
$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL, $request_url); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
$result = curl_exec($ch); 
curl_close($ch);


//download
ini_set('auto_detect_line_endings', 1);
ini_set('default_socket_timeout', 5); // socket timeout, just in case
$cmd = 'download/all.zip';
$request_url = "https://api.crowdin.com/api/project/{$proj_name}/{$cmd}?key={$api_key}";
echo 'Download url:'.$request_url.'<br>';
file_put_contents("translations.zip", file_get_contents($request_url));

$zip = new ZipArchive;
$res = $zip->open('translations.zip');
if ($res === TRUE) {
  $zip->extractTo('./translations/');
  $zip->close();
  echo '--Extracted--';
} else {
  echo '--Can not extract--';
}