<?php
  
  include(dirname(__FILE__) . '/config.php');   
  $cmd = 'update-file';
//  $cmd = 'add-file';
  if(isset($_REQUEST['cmd'])){
      $cmd = $_REQUEST['cmd'];
  }
  $request_url = "https://api.crowdin.com/api/project/{$proj_name}/{$cmd}?key={$api_key}&type=csv";
foreach($project_group_file as $k=>$filename){
    $post_params = array();
  if(function_exists('curl_file_create')) {
    $post_params['files['.$filename.']'] = curl_file_create($filename);
  } else {
    $post_params['files['.$filename.']'] = $filename;
  }
  
  $post_params['export_patterns['.$filename.']'] = '/translations/%original_file_name%';
  $post_params['titles['.$filename.']'] = $project_group_name[$k];
  $post_params['scheme'] = 'identifier,source_phrase,context,'.implode(',',$lang_list);
  $post_params['first_line_contains_header'] = 'true';
  $post_params['import_eq_suggestions'] = '1'; 
  echo '<pre>';
  echo $filename.'<br>';
  print_r($post_params);
  
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $request_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);

  $result = curl_exec($ch);
  curl_close($ch);

  echo $result;
  echo '</pre>'; 
}