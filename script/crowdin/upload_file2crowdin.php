<?php
  $post_params = array();
  include(dirname(__FILE__) . '/config.php');   
  $cmd = 'update-file';
  if(isset($_REQUEST['cmd'])){
      $cmd = $_REQUEST['cmd'];
  }
  $request_url = "https://api.crowdin.com/api/project/{$proj_name}/{$cmd}?key={$api_key}&type=csv";

  if(function_exists('curl_file_create')) {
    $post_params['files[feed_biz_all_lang.csv]'] = curl_file_create('feed_biz_all_lang.csv');
  } else {
    $post_params['files[feed_biz_all_lang.csv]'] = 'feed_biz_all_lang.csv';
  }
  
  $post_params['export_patterns[feed_biz_all_lang.csv]'] = '/translations/%original_file_name%';
  $post_params['titles[feed_biz_all_lang.csv]'] = 'Feedbiz all language';
  $post_params['scheme'] = 'identifier,source_phrase,context,'.implode(',',$lang_list);
  $post_params['first_line_contains_header'] = '';
  $post_params['import_eq_suggestions'] = '1'; 
  echo '<pre>';
  echo $request_url.'<br>';
  print_r($post_params);
  
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $request_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);

  $result = curl_exec($ch);
  curl_close($ch);

  echo $result;
  echo '</pre>'; 