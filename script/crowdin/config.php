<?php
$lang_list = array('english'=>'en-US','french'=>'fr','german'=>'de','spanish'=>'es-ES','italian'=>'it');
$target_lang_list = array('english'=>'en-US','french'=>'fr','german'=>'de','spanish'=>'es-ES','italian'=>'it');//array('french'=>'fr');
$msgl = array('en'=>'english','fr'=>'french','de'=>'german','es'=>'spanish','it'=>'italian');
$msgl_conv = array('english'=>'en','french'=>'fr','german'=>'de','spanish'=>'es','italian'=>'it');
$proj_name = "feedbiz";
$api_key = "c8cb80064d7c31fc230d12b43fc60f3b";
$search_path = array(array('case'=>1,'path'=>dirname(__FILE__)."/../../application/views/templates"),
                     array('case'=>2,'path'=>dirname(__FILE__)."/../../application/controllers"),
                     array('case'=>3,'path'=>dirname(__FILE__)."/../../application/libraries/Amazon"),
                     array('case'=>4,'path'=>dirname(__FILE__)."/../../assets/apps"));
$search_case = array(1=>array("line='",'line="'),
                     2=>array("lang->line('",'lang->line("'),
                     3=>array("Amazon_Tools::l('",'Amazon_Tools::l("'),
                     4=>array(" l('",' l("'));
$skip_file = array('/var/www/backend/script/crowdin/../../application/views/templates/users/billing_information.tpl.r1402', '/var/www/backend/script/crowdin/../../application/views/templates/service/packages.tpl.r1489', '/var/www/backend/script/crowdin/../../application/views/templates/my_feeds/parameters/profiles/category_old.tpl', '/var/www/backend/script/crowdin/../../application/views/templates/my_feeds/parameters/products/carriers20150916.tpl', '/var/www/backend/script/crowdin/../../application/views/templates/ebay/wizard/wizard_finish20150616.tpl', '/var/www/backend/script/crowdin/../../application/views/templates/ebay/order/order_PAGINATION_ISSUE.tpl', '/var/www/backend/script/crowdin/../../application/views/templates/ebay/mapping/configuration_mapping_carriers11.tpl', '/var/www/backend/script/crowdin/../../application/views/templates/dashboard20150528.tpl', '/var/www/backend/script/crowdin/../../application/controllers/ebay.php_bakup_1_10_2015','/var/www/backend/script/crowdin/../../application/controllers/webservice20151021.php','/var/www/backend/script/crowdin/../../application/controllers/ebay1.php','/var/www/backend/script/crowdin/../../application/views/templates/amazon/Parameters20150918.tpl','/var/www/backend/script/crowdin/../../application/views/templates/breadcrumbs20150709.tpl','/var/www/backend/script/crowdin/../../application/views/templates/dashboard20150427.tpl','/var/www/backend/script/crowdin/../../application/views/templates/ebay/actions/configuration_actions_products20150623.tpl','/var/www/backend/script/crowdin/../../application/views/templates/ebay/auth_security2.tpl','/var/www/backend/script/crowdin/../../application/views/templates/ebay/configuration_auth_setting_backup.tpl','/var/www/backend/script/crowdin/../../application/views/templates/ebay/configuration_return_method20150615.tpl','/var/www/backend/script/crowdin/../../application/views/templates/ebay/mapping/backup/mapping_categories.tpl.r1613','/var/www/backend/script/crowdin/../../application/views/templates/ebay/mapping/configuration_mapping_attributes20150417.tpl','/var/www/backend/script/crowdin/../../application/views/templates/ebay/mapping/configuration_mapping_carriers20150528.tpl','/var/www/backend/script/crowdin/../../application/views/templates/ebay/mapping/configuration_mapping_categories20150622.tpl','/var/www/backend/script/crowdin/../../application/views/templates/ebay/mapping/configuration_mapping_conditions20150528.tpl','/var/www/backend/script/crowdin/var/www/backend/script/crowdin/../../application/views/templates/ebay/wizard/wizard_finish20150616.tpl/../../application/views/templates/ebay/order/order_PAGINATION_ISSUE.tpl','/var/www/backend/script/crowdin/../../application/views/templates/my_feeds/parameters/products/carriers.tpl.r1480','/var/www/backend/script/crowdin/../../application/views/templates/my_shop/connect20150213.tpl','/var/www/backend/script/crowdin/../../application/views/templates/my_shop/connect20150602.tpl','/var/www/backend/script/crowdin/../../application/views/templates/my_shop/my_products20150604.tpl','/var/www/backend/script/crowdin/../../application/views/templates/service/packages.tpl.mine','/var/www/backend/script/crowdin/../../application/views/templates/sidebar20150608.tpl','/var/www/backend/script/crowdin/../../application/views/templates/users/billing_information.tpl.mine','/var/www/backend/script/crowdin/../../application/views/templates/users/login.tpl.mine');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ini_set('display_errors',1);
error_reporting(E_ALL);
$system_path = 'system';
if(!defined('BASEPATH'))
define('BASEPATH', str_replace("\\", "/", $system_path));
include(dirname(__FILE__) . '/../../application/config/config.php');   
include(dirname(__FILE__) . '/../../application/config/database.php');  
$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.'); 
mysqli_set_charset('utf8mb4');
$ctrl2lang = array(
    'my_shop'=>'my_feeds_lang.php',
    'my_feeds'=>'my_feeds_lang.php',
    'amazon'=>'amazon_lang.php',
    'ebay'=>'ebay_lang.php',
    'billing'=>'paypal_lang.php',
    'dashboard'=>'dashboard_lang.php',
    'help'=>'my_feeds_lang.php',
    'marketplace'=>'marketplace_lang.php',
    'users'=>'user_lang.php',
    'service'=>'paypal_lang.php',
    'stock'=>'stock_lang.php',
    'tasks'=>'my_feeds_lang.php',
    'statistics'=>'my_feeds_lang.php',
    'mirakl'=>'mirakl_lang.php',
    'general'=>'mirakl_lang.php',
    ''=>'user_lang.php', 
    'offers'=>'offers_lang.php', 
    'profiles'=>'profiles_lang.php', 
    'webservice'=>'webservice_lang.php',
    'google_shopping'=>'google_shopping_lang.php',
    'messaging'=>'messaging_lang.php',
);

$project_group=array( 
    'amazon'=>array('amazon_feed_lang.php','amazon_lang.php','amazon_order_lang.php','amazon_report_lang.php','amazon_repricing_lang.php'),
    'ebay'=>array('ebay_lang.php'),
    'mirakl'=>array('mirakl_lang.php'),
    'google_shopping'=>array('google_shopping_lang.php')
);

$project_group_file=array( 
    'global'=>'feed_biz_all_lang.csv',
//    'common'=>'feed_biz_market_lang.csv',
    'amazon'=>'feed_biz_amazon_lang.csv',
    'ebay'=>'feed_biz_ebay_lang.csv',
    'mirakl'=>'feed_biz_mirakl_lang.csv',
    'google_shopping'=>'feed_biz_gmerc_lang.csv'
);

$project_group_name=array( 
    'global'=>'1. Global and Marketplace common',
//    'common'=>'Marketplace common',
    'amazon'=>'3. Amazon lang file',
    'ebay'=>'4. eBay lang file',
    'mirakl'=>'5. Mirakl lang file',
    'google_shopping'=>'6. GoogleShopping lang file' 
);
