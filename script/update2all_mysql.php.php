<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
$system_path = 'system';
define('BASEPATH', str_replace("\\", "/", $system_path));
include(dirname(__FILE__) . '/../application/config/config.php');
$backend_host = '127.0.0.1:3001';
$create_profiles_product_url = (@$config['ssl_protocol'] ? 'https://' : 'http://') . $backend_host.'/createuser/product/';
$create_profiles_offer_url = (@$config['ssl_protocol'] ? 'https://' : 'http://') . $backend_host.'/createuser/offer/';
$create_profiles_order_url = (@$config['ssl_protocol'] ? 'https://' : 'http://') . $backend_host.'/createuser/order/';

include (dirname(__FILE__).'/../application/libraries/db.php'); 
include(dirname(__FILE__) . '/../application/config/database.php');  
$dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
mysqli_select_db($dbx,$db['default']['database'] ) or die('Could not select database.');
$aync = false;
$u_list = array(331);
$sql = "select * from configuration ,users where name like 'FEED_BIZ%' and id=id_customer ";
$q = mysqli_query($dbx,$sql); 


while($d = mysqli_fetch_assoc($q)){  
    $id_user = $d['id_customer'];
    $user_name = $d['user_name'];
    
//    echo $id_user.' '.$user_name.'<br>';
    
    $db = new Db($user_name,'products',true,false);
    if(!$db->exist_db){
        echo '- Not found database : '.$db->database.'<br>';
        continue;
    }else{
        echo '# Found database : '.$db->database.'<br>';
    }
    $sql = "";
    $db->db_exec($sql);
    
}



  function do_post_request($url, $data = array(), $optional_headers = null, $getresponse = false) {
      global $aync;
        if ($aync) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);
            curl_exec($ch);
            curl_close($ch);
            // $params = array('http' => array(
            // 'method' => 'POST',
            // 'content' => $data
            // ));
            // if ($optional_headers !== null) {
            // $params['http']['header'] = $optional_headers;
            // }
            // $ctx = stream_context_create($params);
            // $fp = fopen($url, 'rb', false, $ctx);
            // if (!$fp) {
            // return false;
            // }
            // if ($getresponse){
            //
                    // $response = stream_get_contents($fp);
            // return $response;
            // }
            return true;
        } else {
            // $start_post = microtime();
            // echo 'start :'.$start_post."<br>\n\n";
//            echo $url."<br>\n\n";
//            echo $url;
            ob_start();
            $ctx = stream_context_create(array(
                'http' => array(
                    'timeout' => 600
                    )
                )
            );

            echo file_get_contents($url, 0, $ctx);
            // var_dump(file_get_contents($url)); exit;
            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL,$url);
            // //curl_setopt($ch, CURLOPT_POST, 1);
            // //curl_setopt($ch, CURLOPT_POSTFIELDS, is_array($data)?$data:array() );
            // //curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
            // curl_exec ($ch);
            // curl_close ($ch);
            $output = ob_get_contents();
            ob_end_clean();
            // echo 'end :'. (microtime()-$start_post) . ' ' .microtime()."<br>\n\n";
            return $output;
        }
    }
?>
