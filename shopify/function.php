<?php

function require_dir($dir) {
    if (file_exists($dir)) {
        $files = scandir($dir);
        foreach ($files as $file) {
            if (strpos($file, '.php') !== FALSE){
                require $dir . '/' . $file;
            }
        }
    }
}