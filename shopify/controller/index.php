<?php
class index extends Controller {
	var $metafield = null;
	var $meta = array ();
	var $data = array ();
	public function __construct() {
		parent::__construct ();
		
		$this->shopify_metafield = new shopify_metafield ( $this->api );
		$this->meta = $this->shopify_metafield->get ( constant::FEEDBIZ_NAMESPACE );
		
		foreach ( $this->meta as $meta ) {
			$this->data [$meta [shopify_metafield::FIELD_KEY]] = $meta [shopify_metafield::FIELD_VALUE];
		}
		
		// case save
		if ($this->isSave ()) {
			$this->_save ();
		}
		
		// if 1st times, initial store meta
		if (empty ( $this->meta )) {
			$this->_initial ();
		}
	}
	private function _initial() {
		foreach ( $this->input_conf as $input_conf_name => $input_conf_type ) {
			$value = '';
			switch ($input_conf_type) {
				case constant::INPUT_CHECKBOX :
				case constant::INPUT_INTEGER :
					$value = '0';
					break;
			}
			
			$this->shopify_metafield->add ( constant::FEEDBIZ_NAMESPACE, $input_conf_name, $value );
			$this->data [$input_conf_name] = $value;
		}
		
		// FEEDBIZ IP
		$this->shopify_metafield->add ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_IP, FEEDBIZ_IP );
	}
	private function _save() {
		foreach ( $this->input_conf as $input_conf_name => $input_conf_type ) {
			// skip input outside the form
			if (in_array ( $input_conf_name, $this->input_conf_noform ))
				continue;
				
				// retrieve meta id
			$meta_id = '';
			foreach ( $this->meta as $meta ) {
				if ($input_conf_name == $meta [shopify_metafield::FIELD_KEY]) {
					$meta_id = $meta [shopify_metafield::FIELD_ID];
					break;
				}
			}
			
			if ($meta_id) {
				$value = '';
				if ($input_conf_type == constant::INPUT_TEXT) {
					$value = $_POST [$input_conf_name];
				} else if ($input_conf_type == constant::INPUT_CHECKBOX) {
					$value = isset ( $_POST [$input_conf_name] ) && $_POST [$input_conf_name] ? 1 : 0;
				} else if ($input_conf_type == constant::INPUT_INTEGER) {
					$value = intval ( $_POST [$input_conf_name] );
				}
				$this->shopify_metafield->update ( $meta_id, $value );
				$this->data [$input_conf_name] = $value;
			}
		}
	}
}

?>