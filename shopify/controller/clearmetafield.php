<?php

class clearmetafield extends Controller {

    var $input_conf = array();
    var $meta = array();
    var $data = array();

    public function __construct() {
        parent::__construct();
        $this->_clear();
    }

    private function _clear() {
        $metas = $this->api->call(constant::METHOD_GET, constant::SHOPIFY_PATH_ADMIN_METAFIELDS);
        foreach ($metas as $meta) {
            $this->api->call(constant::METHOD_DELETE, sprintf(constant::SHOPIFY_PATH_ADMIN_METAFIELDS_ID, $meta[shopify_metafield::FIELD_ID]));
        }
        echo 'success';
    }
}

?>