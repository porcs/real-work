<?php
class verify extends Controller {
	var $shopify_metafield = null;
	var $metafield = null;
	var $meta = array ();
	var $data = array ();
	public function __construct() {
		parent::__construct ();
		$this->shopify_metafield = new shopify_metafield ( $this->api );
		$this->_debug = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DEBUG ) ) ? true : false;
		
		$response = service::verify($_GET[constant::FEEDBIZ_TOKEN], $this->_debug) ? 'Success' : 'Fail';
		echo $response;
	}
}