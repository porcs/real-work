<?php
require '../classes/feed.php';
class StockMovement extends Feed {
	var $shopify_shop = null;
	var $shopify_metafield = null;
	var $shopify_product = null;
	var $shopify_order = null;
	var $shopify_mapping_manufacturer = null;
	var $shopify_mapping_category = null;
	var $context = null;
	var $_debug = false;
	var $errors = array ();
	var $statusCode = '-1';
	var $status = 'Access denied';
	var $returnData = array ();
	var $_cr = "\n<br/>";
	public function __construct() {
		parent::__construct ();
		
		$this->shopify_shop = new shopify_shop ( $this->api );
		$this->shopify_metafield = new shopify_metafield ( $this->api );
		$this->shopify_product = new shopify_product ( $this->api );
		$this->shopify_order = new shopify_order ( $this->api );
		$this->shopify_mapping_manufacturer = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_MANUFACTURER );
		$this->shopify_mapping_category = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_CATEGORY );
		$this->_debug = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DEBUG ) ) ? true : false;
		// echo '<pre>'.print_r($this->shopify_shop, true).'</pre>';exit;
		
		if (! $this->_debug) {
			@ini_set ( 'display_errors', 1 );
			register_shutdown_function ( array (
					$this,
					'_FBShutdowFunction' 
			) );
		}
		$this->_ouput ();
	}
	function _ouput() {
		ob_start ();
		if ($this->_validate ()) {
			$this->statusCode = '0';
			$this->status = 'Fail';
			
			$productId = isset ( $_GET [constant::FEEDBIZ_PRODUCT] ) ? $_GET [constant::FEEDBIZ_PRODUCT] : '';
			$combinationId = isset ( $_GET [constant::FEEDBIZ_COMBINATION] ) ? $_GET [constant::FEEDBIZ_COMBINATION] : '';
			$qty = isset ( $_GET [constant::FEEDBIZ_SOLD] ) ? intval ( $_GET [constant::FEEDBIZ_SOLD] ) : 0;
			
			if ($this->_stockMovement ( $productId, $combinationId, $qty )) {
				$this->statusCode = '1';
				$this->status = 'Success';
			}
		} else {
			$this->statusCode = '-1';
			$this->status = 'Access denied';
		}
	}
	private function _validate() {
		$gettoken = isset ( $_GET [constant::FEEDBIZ_TOKEN] ) ? $_GET [constant::FEEDBIZ_TOKEN] : '';
		$fbtoken = $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TOKEN );
		
		return $gettoken == $fbtoken;
	}
	function _FBShutdowFunction() {
		$level_names = array (
				E_ERROR => 'E_ERROR',
				E_WARNING => 'E_WARNING',
				E_PARSE => 'E_PARSE',
				E_NOTICE => 'E_NOTICE',
				E_CORE_ERROR => 'E_CORE_ERROR',
				E_CORE_WARNING => 'E_CORE_WARNING',
				E_COMPILE_ERROR => 'E_COMPILE_ERROR',
				E_COMPILE_WARNING => 'E_COMPILE_WARNING',
				E_USER_ERROR => 'E_USER_ERROR',
				E_USER_WARNING => 'E_USER_WARNING',
				E_USER_NOTICE => 'E_USER_NOTICE' 
		);
		
		$outBuffer = ob_get_contents ();
		
		$Document = new DOMDocument ();
		$Document->preserveWhiteSpace = true;
		$Document->formatOutput = true;
		$Document->encoding = 'utf-8';
		$Document->version = '1.0';
		
		$OfferPackage = $Document->appendChild ( $Document->createElement ( 'Result' ) );
		$OfferPackage->setAttribute ( 'ShopName', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_NAME ) );
		$OfferPackage->appendChild ( $StatusDoc = $Document->createElement ( 'Status', '' ) );
		$StatusDoc->appendChild ( $Document->createElement ( 'Code', $this->statusCode ) );
		$StatusDoc->appendChild ( $Document->createElement ( 'Message', $this->status ) );
		$StatusDoc->appendChild ( $Document->createElement ( 'Output', implode ( ", ", $this->errors ) ) );
		$errorDoc = $StatusDoc->appendChild ( $Document->createElement ( 'Error' ) );
		$errorDoc->appendChild ( $Document->createCDATASection ( $outBuffer ) );
		
		if (isset ( $this->returnData ['InvoiceNumber'] ))
			$StatusDoc->appendChild ( $Document->createElement ( 'InvoiceNumber', $this->returnData ['InvoiceNumber'] ) );
		if (isset ( $this->returnData ['OrderNumber'] ))
			$StatusDoc->appendChild ( $Document->createElement ( 'OrderNumber', $this->returnData ['OrderNumber'] ) );
		
		ob_end_clean ();
		ob_start ();
		header ( "Content-Type: application/xml; charset=utf-8" );
		echo $Document->saveXML ();
		exit ( 1 );
	}
	function _stockMovement($productId, $combinationId, $qty) {
		if (! $productId) {
			echo 'ProductId is required';
			return false;
		}
		if(!$qty){
			echo 'Please specific stock movement quantity';
			return false;
		}
		
		$refId = $combinationId ? $combinationId : $productId;
		$varian = $this->api->call ( constant::METHOD_GET, sprintf ( constant::SHOPIFY_PATH_ADMIN_VARIANTS_Q_ID, $refId ) );
		
		if (isset ( $varian [constant::SHOPIFY_ERRORS] )) {
			echo $varian [constant::SHOPIFY_ERRORS];
			return false;
		}
		if ($varian [shopify_product::PROPERTY_INVENTORY_MANAGEMENT] != shopify_product::VALUE_INVENTORY_SHOPIFY_TRACKING) {
			echo 'Product inventory tracking is disabled.';
			return false;
		}
		
		$varianArr = array (
				shopify_product::PROPERTY_ID => $refId,
				shopify_product::PROPERTY_INVENTORY_QUANTITY => intval ( $varian [shopify_product::PROPERTY_INVENTORY_QUANTITY] ) - $qty 
		);
		$this->api->call ( constant::METHOD_PUT, sprintf ( constant::SHOPIFY_PATH_ADMIN_VARIANTS_Q_ID, $refId ), array (
				'variant' => $varianArr 
		) );
		
		return true;
	}
}

new StockMovement ();
