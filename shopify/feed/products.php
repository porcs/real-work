<?php
require '../classes/feed.php';
class Products extends Feed {
	var $shopify_shop = null;
	var $shopify_country = null;
	var $shopify_metafield = null;
	var $shopify_product = null;
	var $shopify_mapping_manufacturer = null;
	var $shopify_mapping_category = null;
	var $context = null;
	var $_debug = false;
	var $errors = array ();
	public function __construct() {
		parent::__construct ();
		
		$this->shopify_shop = new shopify_shop ( $this->api );
		$this->shopify_country = new shopify_country ( $this->api );
		$this->shopify_metafield = new shopify_metafield ( $this->api );
		$this->shopify_product = new shopify_product ( $this->api );
		$this->shopify_mapping_manufacturer = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_MANUFACTURER );
		$this->shopify_mapping_category = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_CATEGORY );
		$this->_debug = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DEBUG ) ) ? true : false;
		// echo '<pre>'.print_r($this->shopify_shop, true).'</pre>';exit;
		
		$this->_ouput ();
	}
	function _ouput() {
		if ($this->_validate ()) {
			$this->_xml ();
		} else {
			$this->_accessDeny ();
		}
	}
	private function _validate() {
		$gettoken = isset ( $_GET [constant::FEEDBIZ_TOKEN] ) ? $_GET [constant::FEEDBIZ_TOKEN] : '';
		$fbtoken = $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TOKEN );
		
		return $gettoken == $fbtoken;
	}
	private function _xml() {
		// Feedbiz login as customer to calculate cost
		$feedOBJs = array ();
		
		$error = false;
		$count = 0;
		$history_combination = array ();
		$history_ean = array ();
		$history_manufacturer = array ();
		$history_categories = array ();
		$ProductCombination = array ();
		$sku_history = array ();
		$ManufacturersElement = null;
		$CategoriesElement = null;
		$cr = "\n<br/>";
		
		ob_start ();
		
		// create DOMDocument();
		$Document = new DOMDocument ();
		$Document->preserveWhiteSpace = true;
		$Document->formatOutput = true;
		$Document->encoding = 'utf-8';
		$Document->version = '1.0';
		
		$shopifyCountries = $this->shopify_country->getCountries ();
		// echo '<pre>'.print_r($shopifyCountries, true).'</pre>';exit;
		$ExportData = $Document->appendChild ( $Document->createElement ( 'ExportData', '' ) );
		$ExportData->setAttribute ( 'ShopName', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_NAME ) );
		$Products = $ExportData->appendChild ( $Document->createElement ( 'Products', '' ) );
		
		// Prices Parameters
		$countryCode = isset ( $_GET [constant::FEEDBIZ_COUNTRY] ) ? $_GET [constant::FEEDBIZ_COUNTRY] : '*';
		$countryShop = null;
		$useTaxes = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TAXES ) ) ? true : false;
		$useSpecials = floatval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DISCOUNT ) ) ? true : false;
		$description = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DESCRIPTION ) );
		
		// Delivery Fees
		$AdditionalShippingPrice = floatval ( isset ( $this->settingFeedbiz ['FEEDBIZ_ADDITIONAL_SP'] ) ? $this->settingFeedbiz ['FEEDBIZ_ADDITIONAL_SP'] : 0 );
		
		// Carrier Configuration
		$ShippingList = array ();
		$selected = isset ( $this->settingFeedbiz ['FEEDBIZ_CARRIER'] ) ? $this->settingFeedbiz ['FEEDBIZ_CARRIER'] : '';
		$country = $this->shopify_country->getCountry ( $countryCode );
		if ($country) {
			// shipping rate
			foreach ( $country [shopify_country::PROPERTY_WEIGHT_BASED_SHIPPING_RATES] as $shippingRate ) {
				$ShippingList [] = array (
						'id' => $shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__ID],
						'code' => $shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__ID],
						'name' => $shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__NAME] 
				);
			}
		}
		
		// Products
		if (! $error) {
			$products_no = 0;
			$products_determine_no = 0;
			
			// CONTEXT UPDATE
			$this->context = new Context ( $this->shopify_metafield, constant::FEEDBIZ_CONTEXT_PRODUCTS );
			$products_count = $this->shopify_product->count ( null );
			$this->context->maxProduct = intval ( $products_count );
			$this->context->minProduct = $this->context->maxProduct ? 1 : 0;
			
			$this->context->timestamp = date ( 'Y-m-d H:m:i' );
			$limit = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_PAGE_SIZE ) );
			
			if (! $limit || $limit > FEEDBIZ_PAGE_SIZE)
				$limit = intval ( FEEDBIZ_PAGE_SIZE );
				
				// echo '<pre>'.print_r($this->context, true).'</pre>';exit;
			$page = $this->context->status == constant::STATUS_COMPLETE || ! $this->context->currentProduct ? 1 : floor ( $this->context->currentProduct / $limit ) + 1;
			$products = $this->shopify_product->getProducts ( $limit, $page );
			
			$AttributesDom = $ExportData->appendChild ( $Document->createElement ( 'Attributes', '' ) );
			$AttributeDom = $AttributesDom->appendChild ( $Document->createElement ( 'Attribute', '' ) );
			$AttributeDom->setAttribute ( 'type', constant::FEEDBIZ_ATTRIBUTE_MODEL_TYPE );
			$AttributeDom->setAttribute ( 'ID', constant::FEEDBIZ_ATTRIBUTE_MODEL_ID );
			
			$AttributeLangDom = $AttributeDom->appendChild ( $Document->createElement ( 'Name', '' ) );
			$AttributeLangDom->setAttribute ( 'lang', 1 );
			$AttributeLangDom->appendChild ( $Document->createCDATASection ( constant::FEEDBIZ_ATTRIBUTE_MODEL_NAME ) );
			
			$AttributeValuesDom = null;
			
			if ($products) {
				// echo '<pre>' . print_r ( $products, true ) . '</pre>';
				// exit ();
				foreach ( $products as $details ) {
					$products_determine_no ++;
					$varians = isset ( $details [shopify_product::PROPERTY_VARIANTS] ) ? $details [shopify_product::PROPERTY_VARIANTS] : array ();
					$id_product = $details [shopify_product::PROPERTY_ID];
					if (sizeof ( $varians ) == 1)
						$id_product = $varians [0] [shopify_product::PROPERTY_ID];
					
					$strikedPrice = $standardPrice = isset ( $details [shopify_product::PROPERTY_PRICE] ) ? floatval ( $details [shopify_product::PROPERTY_PRICE] ) : 0;
					$special_diff = 0;
					$idx = 0;
					$jan = $upc = $ean13 = isset ( $details [shopify_product::PROPERTY_BARCODE] ) ? trim ( $details [shopify_product::PROPERTY_BARCODE] ) : '';
					$id_manufacturer = isset ( $details [shopify_product::PROPERTY_VENDOR] ) && $details [shopify_product::PROPERTY_VENDOR] ? $this->shopify_mapping_manufacturer->getId ( $details [shopify_product::PROPERTY_VENDOR] ) : 0;
					
					// Feed.Biz Product Option
					$reference = $id_product;
					$tagsText = $details [shopify_product::PROPERTY_TAGS];
					$vendor = $details [shopify_product::PROPERTY_VENDOR];
					$product_type = $details [shopify_product::PROPERTY_PRODUCT_TYPE];
					$sku_product = '';
					$upc_product = '';
					if (sizeof ( $varians ) == 1) {
						$sku_product = $varians [0] [shopify_product::PROPERTY_SKU];
						$upc_product = $varians [0] [shopify_product::PROPERTY_BARCODE];
					}
					
					if (empty ( $reference )) {
						$this->errors [] = sprintf ( 'Skipping product without reference: %d', $id_product ) . $cr;
						continue;
					}
					
					if (( int ) $ean13) {
						if (! Tool::EAN_UPC_Check ( $ean13 )) {
							$this->errors [] = sprintf ( 'Inconsistency for product %s(%d) - Product EAN(%s) seems to be wrong - Skipping product' . $cr, $reference, $id_product, $ean13 );
							continue;
						}
						if ($ean13 && isset ( $history_ean [$ean13] )) {
							if ($this->_debug)
								$this->errors [] = sprintf ( 'Duplicate record for product %s(%d/%d)' . $cr, $reference, $id_product );
							continue;
						}
						$history_ean [$ean13] = true;
					}
					
					if ($reference && isset ( $sku_history [$reference] )) {
						if ($this->_debug)
							$this->errors [] = sprintf ( 'Duplicate record for product %s(%d/%d)' . $cr, $reference, $id_product );
						continue;
					}
					$sku_history [$reference] = true;
					
					$products_no ++;
					$ProductDetails = $Products->appendChild ( $product_no = $Document->createElement ( 'Product', '' ) );
					$product_no->setAttribute ( 'ID', $products_no );
					
					$ProductData = $ProductDetails->appendChild ( $Document->createElement ( 'ProductData', '' ) );
					$count ++;
					
					// Product Name
					$ProductNames = $ProductData->appendChild ( $p_name = $Document->createElement ( 'Names', '' ) );
					$ProductName = $ProductNames->appendChild ( $p_name = $Document->createElement ( 'Name', '' ) );
					$ProductName->appendChild ( $Document->createCDATASection ( $details [shopify_product::PROPERTY_TITLE] ) );
					$p_name->setAttribute ( 'lang', 1 );
					
					// Product Description
					$ProductDescriptions = $ProductData->appendChild ( $p_name = $Document->createElement ( 'Descriptions', '' ) );
					$ProductDescription = $ProductDescriptions->appendChild ( $Document->createElement ( 'Description', '' ) );
					$ProductDescription->appendChild ( $Document->createCDATASection ( Tool::description ( $details [shopify_product::PROPERTY_BODY_HTML] ) ) );
					$ProductDescription->setAttribute ( 'lang', 1 );
					
					$ProductSortDescriptions = $ProductData->appendChild ( $p_name = $Document->createElement ( 'ShortDescriptions', '' ) );
					$ProductSortDescription = $ProductSortDescriptions->appendChild ( $Document->createElement ( 'ShortDescription', '' ) );
					$productShortDescriptionText = $details [shopify_product::PROPERTY_BODY_HTML];
					
					$productMeta = $this->shopify_product->getMeta ( $id_product );
					foreach ( $productMeta as $productMetaElement ) {
						if ($productMetaElement [shopify_metafield::FIELD_KEY] == shopify_metafield::VALUE_DESCRIPTION_TAG && ! empty ( $productMetaElement [shopify_metafield::FIELD_VALUE] )) {
							$productShortDescriptionText = $productMetaElement [shopify_metafield::FIELD_VALUE];
							break;
						}
					}
					$ProductSortDescription->appendChild ( $Document->createCDATASection ( Tool::description ( $productShortDescriptionText ) ) );
					$ProductSortDescription->setAttribute ( 'lang', 1 );
					
					// Tags
					// $tagsArr = array ();
					// if ($tagsText) {
					// $product_desc_arr = explode ( ',', $tagsText );
					// foreach ( $product_desc_arr as $tags_lang => $product_desc_ele ) {
					// if (trim ( $product_desc_ele ) != '')
					// $tagsArr [] = array (
					// 'language_id' => 1,
					// 'tag' => trim ( $product_desc_ele )
					// );
					// }
					// }
					// if (sizeof ( $tagsArr ) > 0) {
					// $ProductTags = $ProductData->appendChild ( $p_name = $Document->createElement ( 'Tags', '' ) );
					// $ProductTag = array ();
					// $tag_no = 0;
					// foreach ( $tagsArr as $tagsEle ) {
					// $tag_key = $id_product;
					// if (! isset ( $ProductTag [$tag_key] )) {
					// $tag_no ++;
					// $ProductTag [$tag_key] = $ProductTags->appendChild ( $p_tags = $Document->createElement ( 'Tag', '' ) );
					// $p_tags->setAttribute ( 'ID', $tag_no );
					// }
					
					// $ProductTagLang = $ProductTag [$tag_key]->appendChild ( $pl_tags = $Document->createElement ( 'Name', '' ) );
					// $ProductTagLang->appendChild ( $Document->createCDATASection ( trim ( $tagsEle ['tag'] ) ) );
					// $pl_tags->setAttribute ( 'lang', $tagsEle ['language_id'] );
					// }
					// }
					
					$ProductTags = $ProductData->appendChild ( $Document->createElement ( 'Tags', '' ) );
					$ProductTagLang = $ProductTags->appendChild ( $Document->createElement ( 'Tag', '' ) );
					$ProductTagLang->appendChild ( $Document->createCDATASection ( trim ( $tagsText ) ) );
					$ProductTagLang->setAttribute ( 'lang', 1 );
					
					// Identifier
					$identifierElement = $ProductDetails->appendChild ( $Document->createElement ( 'Identifier', '' ) );
					$identifierElement->appendChild ( $i_id = $Document->createElement ( 'Reference', $id_product ) );
					$i_id->setAttribute ( 'type', 'ID' );
					
					if (isset ( $details [shopify_product::PROPERTY_ID] ) && $details [shopify_product::PROPERTY_ID]) {
						$identifierElement->appendChild ( $model_Ref = $Document->createElement ( 'Reference', '' ) );
						$model_Ref->setAttribute ( 'type', 'Reference' );
						$model_Ref->appendChild ( $Document->createCDATASection ( $id_product ) );
					}
					
					if ($sku_product) {
						$ProductSKU = $identifierElement->appendChild ( $i_sku = $Document->createElement ( 'Reference', '' ) );
						$ProductSKU->appendChild ( $Document->createCDATASection ( $sku_product ) );
						$i_sku->setAttribute ( 'type', 'SKU' );
					}
					
					// if (! empty ( $ean13 )) {
					// $identifierElement->appendChild ( $i_ean = $Document->createElement ( 'Code', $ean13 ) );
					// $i_ean->setAttribute ( 'type', 'EAN' );
					// }
					if ($upc_product) {
						$identifierElement->appendChild ( $i_upc = $Document->createElement ( 'Code', $upc_product ) );
						$i_upc->setAttribute ( 'type', 'UPC' );
					}
					// if (! empty ( $jan )) {
					// $identifierElement->appendChild ( $i_jan = $Document->createElement ( 'Code', $jan ) );
					// $i_jan->setAttribute ( 'type', 'JAN' );
					// }
					
					// Images Product
					$images_product = $details [shopify_product::PROPERTY_IMAGES];
					
					if ($images_product) {
						if ($this->_debug) {
							printf ( 'Products Images: %s' . "<br />\n", print_r ( $images_product, true ) );
						}
					}
					
					$imagesElement = $ProductDetails->appendChild ( $Document->createElement ( 'Images', '' ) );
					$image_product_no = 1;
					$arr_images = array ();
					
					$default_image = true;
					foreach ( $images_product as $image_product ) {
						if (is_array ( $image_product )) {
							array_push ( $arr_images, $image_product_no );
							$ProductImage = $imagesElement->appendChild ( $img_product = $Document->createElement ( 'Image', '' ) );
							$ProductImage->appendChild ( $Document->createCDATASection ( str_replace ( 'https://', 'http://', $image_product [shopify_product::PROPERTY_IMAGES_SRC] ) ) );
							$img_product->setAttribute ( 'id', $image_product_no );
							
							if ($default_image) {
								$default_image = false;
								$img_product->setAttribute ( 'type', 'default' );
							} else
								$img_product->setAttribute ( 'type', 'normal' );
							
							$md5_key = md5 ( $image_product [shopify_product::PROPERTY_IMAGES_UPDATE_AT] );
							$img_product->setAttribute ( 'availability', 1 );
							$img_product->setAttribute ( 'md5', $md5_key );
							$image_product_no ++;
						}
					}
					
					// Manufacturer
					$ProductDetails->appendChild ( $manufac_id = $Document->createElement ( 'Manufacturer', '' ) );
					if (isset ( $details [shopify_product::PROPERTY_VENDOR] ) && $details [shopify_product::PROPERTY_VENDOR])
						$manufac_id->setAttribute ( 'ID', $this->shopify_mapping_manufacturer->getId ( $details [shopify_product::PROPERTY_VENDOR] ) );
						
						// Product Carrier
					$resultArr = array ();
					$ProductCarrier = $ProductDetails->appendChild ( $Document->createElement ( 'Carriers', '' ) );
					foreach ( $ShippingList as $ShippingEle ) {
						// foreach ( $shopifyCountry [shopify_country::PROPERTY_WEIGHT_BASED_SHIPPING_RATES] as $shippingRate ) {.....
						// echo '<pre>'.print_r($shopifyCountry, true).'</pre>';exit;
						$ProductCarrier->appendChild ( $carrier_att = $Document->createElement ( 'Carrier', '' ) );
						$carrier_att->setAttribute ( 'ID', $ShippingEle [shopify_country::PROPERTY_SHIPPING_RATE__ID] );
						// }
					}
					
					// Pricing
					$priceElement = $ProductDetails->appendChild ( $Document->createElement ( 'Price', '' ) );
					if (sizeof ( $varians ) == 1) {
						$varianPriceInc = $varianPrice = floatval ( $varians [0] [shopify_product::PROPERTY_PRICE] );
						$varianCompareAtPriceInc = $varianCompareAtPrice = floatval ( $varians [0] [shopify_product::PROPERTY_COMPARE_AT_PRICE] );
						if ($useTaxes) {
							$taxWeight = floatval ( $country [shopify_country::PROPERTY_TAX] );
							$varianPriceInc = $varianPrice * (1 + $taxWeight);
							$varianCompareAtPriceInc = $varianCompareAtPrice * (1 + $taxWeight);
						}
						
						if ($useSpecials && $varianCompareAtPrice) {
							if ($varianCompareAtPrice) {
								$special_diff = $varianCompareAtPriceInc - $varianPriceInc;
								$startDate = '1970-01-01';
								$toDate = '1970-01-01'; // ISO 8601
								$priceElement->appendChild ( $p_sale = $Document->createElement ( 'Sale', '' ) );
								$p_sale->setAttribute ( 'startDate', $startDate );
								$p_sale->setAttribute ( 'endDate', $toDate );
								$p_sale->setAttribute ( 'value', round ( $special_diff, 2 ) );
								$p_sale->setAttribute ( 'type', 'amount' );
								$varianPriceInc = $varianCompareAtPriceInc;
							}
						}
					} else { // case varians
						$varianPriceInc = 0;
					}
					$priceElement->appendChild ( $Document->createElement ( 'Standard', round ( $varianPriceInc, 2 ) ) );
					
					// get TAX RATE which has highest PRIORITY
					if ($useTaxes) {
						if ($country) {
							$priceElement->appendChild ( $p_tax = $Document->createElement ( 'Tax', '' ) );
							$p_tax->setAttribute ( 'ID', $country [shopify_country::PROPERTY_ID] );
						}
					}
					
					$priceElement->appendChild ( $p_currency = $Document->createElement ( 'Currency', '' ) );
					$p_currency->setAttribute ( 'ID', 1 );
					
					// Category
					$ProductCategories = $ProductDetails->appendChild ( $category_att = $Document->createElement ( 'Categories', '' ) );
					if (trim ( $vendor )) {
						$ProductCategories->appendChild ( $category_att = $Document->createElement ( 'Category', '' ) );
						$category_att->setAttribute ( 'ID', $this->shopify_mapping_category->getId ( trim ( $product_type ), 2 ) );
						$category_att->setAttribute ( 'type', 'Default' );
					}
					
					// Condition
					$Condition = $ProductDetails->appendChild ( $Document->createElement ( 'Condition', '' ) );
					$Condition->setAttribute ( 'ID', 1 );
					
					// CHILD-PARENT ATTRIBUTE DOES NOT EXIST IN OPENCART
					// Children
					if (isset ( $varians ) && count ( $varians ) > 1) {
						foreach ( $varians as $varian ) {
							// echo '<pre>' . print_r ( $varian, true ) . '</pre>';
							// exit ();
							// Validation
							$combination_id = $varian [shopify_product::PROPERTY_ID];
							$combination_sku = $varian [shopify_product::PROPERTY_SKU];
							$combination_upc = $varian [shopify_product::PROPERTY_BARCODE];
							
							if (isset ( $history_combination [$combination_id] )) {
								if ($this->_debug)
									$this->errors [] = sprintf ( 'Duplicate record for product %s(%d/%d)' . $cr, $reference, $id_product, $combination_id );
								continue;
							}
							
							$idx ++;
							
							if (! isset ( $ProductCombination [$id_product] )) {
								$ProductCombination [$id_product] = $ProductDetails->appendChild ( $Document->createElement ( 'Combinations', '' ) );
							}
							
							$ProductCombinationID = $ProductCombination [$id_product]->appendChild ( $id_com = $Document->createElement ( 'Combination', '' ) );
							$id_com->setAttribute ( 'id', $combination_id );
							
							$ProductCombinationIdentifier = $ProductCombinationID->appendChild ( $id_com = $Document->createElement ( 'Identifier', '' ) );
							$ProductCombinationIdentifier->appendChild ( $id_com_Ref = $Document->createElement ( 'Reference', $combination_id ) );
							$id_com_Ref->setAttribute ( 'type', 'ID' );
							
							if ($combination_sku) {
								$ProductCombinationIdentifier->appendChild ( $model_Ref = $Document->createElement ( 'Reference', '' ) );
								$model_Ref->setAttribute ( 'type', 'Reference' );
								$model_Ref->appendChild ( $Document->createCDATASection ( $combination_id ) );
							}
							
							if ($combination_sku) {
								$ProductCombinationSKU = $ProductCombinationIdentifier->appendChild ( $sku_com_Ref = $Document->createElement ( 'Reference', '' ) );
								$ProductCombinationSKU->appendChild ( $Document->createCDATASection ( $combination_sku ) );
								$sku_com_Ref->setAttribute ( 'type', 'SKU' );
							}
							
							if (isset ( $combination_upc ) && ! empty ( $combination_upc )) {
								$ProductCombinationIdentifier->appendChild ( $upc_com_Ref = $Document->createElement ( 'Code', $combination_upc ) );
								$upc_com_Ref->setAttribute ( 'type', 'UPC' );
							}
							
							// if (isset ( $combination ['ean13'] ) && ! empty ( $combination ['ean13'] )) {
							// $ProductCombinationIdentifier->appendChild ( $ean13_com_Ref = $Document->createElement ( 'Code', $combination ['ean13'] ) );
							// $ean13_com_Ref->setAttribute ( 'type', 'EAN' );
							// }
							
							// $ProductCombinationAttributes = null;
							// if ($combination ['id_attribute_group']) {
							
							// $attribute_group_idArr = explode ( '_', $combination ['id_attribute_group'] );
							// $attribute_idArr = explode ( '_', $combination ['id_attribute'] );
							// foreach ( $attribute_group_idArr as $attribute_group_idArr_key => $attribute_group_idArr_val ) {
							// $ProductCombinationAttributes = $ProductCombinationAttributes == null ? $ProductCombinationID->appendChild ( $id_com = $Document->createElement ( 'Attributes', '' ) ) : $ProductCombinationAttributes;
							// $ProductCombinationAttributes->appendChild ( $attribute_name = $Document->createElement ( 'Attribute', '' ) );
							// $attribute_name->setAttribute ( 'ID', $attribute_group_idArr [$attribute_group_idArr_key] );
							// $attribute_name->setAttribute ( 'Option', $attribute_idArr [$attribute_group_idArr_key] );
							// }
							// }
							$ProductCombinationAttributes = $ProductCombinationID->appendChild ( $Document->createElement ( 'Attributes', '' ) );
							$ProductCombinationAttributes->appendChild ( $attribute_name = $Document->createElement ( 'Attribute', '' ) );
							$attribute_name->setAttribute ( 'ID', constant::FEEDBIZ_ATTRIBUTE_MODEL_ID );
							$attribute_name->setAttribute ( 'Option', $varian [shopify_product::PROPERTY_ID] );
							
							// Images
							if (isset ( $arr_images )) {
								foreach ( $arr_images as $comb_img ) {
									$ProductCombinationImage = $ProductCombinationID->appendChild ( $Document->createElement ( 'Images', '' ) );
									$ProductCombinationImage->appendChild ( $img = $Document->createElement ( 'Image', '' ) );
									$img->setAttribute ( 'id', $comb_img );
									break;
								}
							}
							
							// $product_tax_rules = $this->model_feedbiz_product->getTaxes($details ['tax_class_id']);
							// foreach ($product_tax_rules as $product_tax_rule) {
							// $carrier_tax_rate_priority = $product_tax_rule ['priority'];
							// $carrier_tax_rate = $this->model_localisation_tax_rate->getTaxRate($product_tax_rule ['tax_rate_id']);
							// $product_tax = $carrier_tax_rate ['rate'];
							// break;
							// }.....
							
							// Pricing
							$combinationPriceInc = $combinationPrice = floatval ( $varian [shopify_product::PROPERTY_PRICE] );
							$combinationCompareAtPriceInc = $combinationCompareAtPrice = floatval ( $varian [shopify_product::PROPERTY_COMPARE_AT_PRICE] );
							if ($useTaxes) {
								$taxWeight = floatval ( $country [shopify_country::PROPERTY_TAX] );
								$combinationPriceInc = $combinationPrice * (1 + $taxWeight);
								$combinationCompareAtPriceInc = $combinationCompareAtPrice * (1 + $taxWeight);
							}
							
							$ProductCombinationPriceOverride = $ProductCombinationID->appendChild ( $Document->createElement ( 'PriceOverride', '' ) );
							$ProductCombinationPriceOverride->appendChild ( $price_comb = $Document->createElement ( 'Operation', round ( $combinationPriceInc, 2 ) ) );
							$ProductCombinationPriceOverride->appendChild ( $Document->createElement ( 'Price', round ( $combinationPriceInc, 2 ) ) );
							$ProductCombinationPriceOverride->appendChild ( $Document->createElement ( 'Striked', round ( $combinationPriceInc, 2 ) ) );
							$price_comb->setAttribute ( 'type', 'Increase' );
							$price_comb->setAttribute ( 'value', '1' );
							
							$ProductCombinationTopology = $ProductCombinationID->appendChild ( $Document->createElement ( 'Topology', '' ) );
							
							if (isset ( $varian [shopify_product::PROPERTY_GRAMS] )) {
								$ProductCombinationTopology->appendChild ( $weight_unit = $Document->createElement ( 'Unit', $varian [shopify_product::PROPERTY_GRAMS] ) );
								$weight_unit->setAttribute ( 'ID', '1' );
								$weight_unit->setAttribute ( 'Name', 'Weight' );
							}
							
							$stockManagement = $varian [shopify_product::PROPERTY_INVENTORY_MANAGEMENT] == shopify_product::VALUE_INVENTORY_SHOPIFY_TRACKING;
							$quantity = $stockManagement ? intval ( $varian [shopify_product::PROPERTY_INVENTORY_QUANTITY] ) : 999;
							$ProductCombinationAvailabilities = $ProductCombinationID->appendChild ( $Document->createElement ( 'Availabilities', '' ) );
							$ProductCombinationAvailabilities->appendChild ( $Document->createElement ( 'Stock', $quantity ) );
							$ProductCombinationAvailabilities->appendChild ( $Document->createElement ( 'Active', 1 ) );
							$available_date = date ( 'c', strtotime ( $varian [shopify_product::PROPERTY_CREATED_AT] ) );
							$ProductCombinationAvailabilities->appendChild ( $Document->createElement ( 'Date', $available_date ) );
							
							$history_combination [$combination_id] = true;
						}
					} else {
						// echo '<pre>'.print_r($details, true).'</pre>';exit;
						// Topology
						$ProductCombinationTopology = $ProductDetails->appendChild ( $Document->createElement ( 'Topology', '' ) );
						$ProductCombinationTopology->appendChild ( $weight_unit = $Document->createElement ( 'Unit', $varians [0] [shopify_product::PROPERTY_GRAMS] ) );
						$weight_unit->setAttribute ( 'ID', '1' );
						$weight_unit->setAttribute ( 'Name', 'Weight' );
						
						// Availabilities
						$stockManagement = $varians [0] [shopify_product::PROPERTY_INVENTORY_MANAGEMENT] == shopify_product::VALUE_INVENTORY_SHOPIFY_TRACKING;
						$quantity = $stockManagement ? intval ( $varians [0] [shopify_product::PROPERTY_INVENTORY_QUANTITY] ) : 999;
						$ProductAvailabilities = $ProductDetails->appendChild ( $Document->createElement ( 'Availabilities', '' ) );
						$ProductAvailabilities->appendChild ( $Document->createElement ( 'Stock', $quantity ) );
						$ProductAvailabilities->appendChild ( $Document->createElement ( 'Active', 1 ) );
						$available_date = date ( 'c', strtotime ( $details [shopify_product::PROPERTY_CREATED_AT] ) );
						$ProductAvailabilities->appendChild ( $Document->createElement ( 'Date', $available_date ) );
					}
					
					if ($limit == $products_no) {
						break;
					}
					
					// Manufacturers
					if (! $ManufacturersElement) {
						$ManufacturersElement = $ExportData->appendChild ( $Document->createElement ( 'Manufacturers' ) );
					}
					if (trim ( $vendor )) {
						if (! in_array ( $vendor, $history_manufacturer )) {
							$history_manufacturer [] = $vendor;
							$ManufacturerElement = $ManufacturersElement->appendChild ( $manufacturer_id = $Document->createElement ( 'Manufacturer' ) );
							$ManufacturerElement->appendChild ( $Document->createCDATASection ( $vendor ) );
							$manufacturer_id->setAttribute ( 'ID', $this->shopify_mapping_manufacturer->getId ( $vendor ) );
						}
					}
					
					// Categories
					if (! $CategoriesElement) {
						$CategoriesElement = $ExportData->appendChild ( $Document->createElement ( 'Categories' ) );
						$CategoryElement = $CategoriesElement->appendChild ( $category_id = $Document->createElement ( 'Category' ) );
						$category_id->setAttribute ( 'ID', 1 );
						$category_id->setAttribute ( 'parent', 'root' );
						$CategoryNameElement = $CategoryElement->appendChild ( $cat_name = $Document->createElement ( 'Name' ) );
						$cat_name->setAttribute ( 'lang', 1 );
						$CategoryNameElement->appendChild ( $Document->createCDATASection ( 'Root' ) );
					}
					if (trim ( $product_type )) {
						if (! in_array ( $product_type, $history_categories )) {
							$history_categories [] = $product_type;
							$CategoryElement = $CategoriesElement->appendChild ( $category_id = $Document->createElement ( 'Category' ) );
							$category_id->setAttribute ( 'ID', $this->shopify_mapping_category->getId ( trim ( $product_type ), 2 ) );
							$category_id->setAttribute ( 'parent', 1 );
							$CategoryNameElement = $CategoryElement->appendChild ( $cat_name = $Document->createElement ( 'Name' ) );
							$cat_name->setAttribute ( 'lang', 1 );
							$CategoryNameElement->appendChild ( $Document->createCDATASection ( $product_type ) );
						}
					}
					
					// Attributes
					foreach ( $varians as $varian ) {
						// Attribute Values
						if (! $AttributeValuesDom) {
							$AttributeValuesDom = $AttributeDom->appendChild ( $Document->createElement ( 'Values', '' ) );
						}
						
						$AttributeValueDom = $AttributeValuesDom->appendChild ( $Document->createElement ( 'Value', '' ) );
						$AttributeValueDom->setAttribute ( 'Option', $varian [shopify_product::PROPERTY_ID] );
						
						// Attribute Value Language
						$AttributeValueLangDom = $AttributeValueDom->appendChild ( $Document->createElement ( 'Name', '' ) );
						$AttributeValueLangDom->setAttribute ( 'lang', 1 );
						$AttributeValueLangDom->appendChild ( $Document->createCDATASection ( $varian [shopify_product::PROPERTY_TITLE] ) );
					}
				} // end foreach products
				  // CONTEXT COUNT
				$this->context->currentProduct = (($limit * ($page - 1)) + $products_no);
				$this->context->currentProductCount = $products_no;
				$this->context->status = $this->context->maxProduct == (($limit * ($page - 1)) + $products_no) ? constant::STATUS_COMPLETE : constant::STATUS_INCOMPLETE; // previous + current
			}  // end if product
else {
				$products_no = 0;
				$this->context->currentProduct = 0;
				$this->context->currentProductCount = 0;
				$this->context->status = FeedbizContext::STATUS_COMPLETE;
			}
		}  // end if
else {
			$products_no = 0;
			$this->context->currentProduct = 0;
			$this->context->currentProductCount = 0;
			$this->context->status = FeedbizContext::STATUS_COMPLETE;
		}
		
		$this->context->save ();
		$this->shopify_mapping_category->save ();
		$this->shopify_mapping_manufacturer->save ();
		// CONTEXT SAVING
		$ExportData->appendChild ( $statusDoc = $Document->createElement ( 'Status', '' ) );
		$statusDoc->appendChild ( $Document->createElement ( 'Code', $this->context->status == constant::STATUS_INCOMPLETE ? '-1' : '1' ) );
		$statusDoc->appendChild ( $Document->createElement ( 'Message', $this->context->status ) );
		$statusDoc->appendChild ( $Document->createElement ( 'ExportTotal', $products_no ) );
		$statusDoc->appendChild ( $Document->createElement ( 'CurrentProductID', $this->context->currentProduct ) );
		$statusDoc->appendChild ( $Document->createElement ( 'MinProductID', $this->context->minProduct ) );
		$statusDoc->appendChild ( $Document->createElement ( 'MaxProductID', $this->context->maxProduct ) );
		
		// FULL EDIT SETTING
		
		if (! $this->_debug) {
			ob_end_clean ();
			header ( "Content-Type: application/xml; charset=utf-8" );
			echo $Document->saveXML ();
		} else {
			echo '<pre>' . print_r ( $this->errors, true ) . '</pre>';
		}
	}
	private function _accessDeny() {
		$Document = new DOMDocument ();
		$Document->preserveWhiteSpace = true;
		$Document->formatOutput = true;
		$Document->encoding = 'utf-8';
		$Document->version = '1.0';
		
		$ExportData = $Document->appendChild ( $Document->createElement ( 'ExportData', '' ) );
		$ExportData->setAttribute ( 'ShopName', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_NAME ) );
		$ExportData->appendChild ( $errorDoc = $Document->createElement ( 'Status', '' ) );
		$errorDoc->appendChild ( $Document->createElement ( 'Code', '0' ) );
		$errorDoc->appendChild ( $Document->createElement ( 'Message', 'Access denied' ) );
		
		header ( "Content-Type: application/xml; charset=utf-8" );
		echo $Document->saveXML ();
		exit ();
	}
}

new Products ();
