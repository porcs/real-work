<?php
require '../classes/feed.php';
class Products extends Feed {
	var $shopify_shop = null;
	var $shopify_country = null;
	var $shopify_metafield = null;
	var $shopify_product = null;
	var $shopify_mapping_manufacturer = null;
	var $shopify_mapping_category = null;
	var $context = null;
	var $_debug = false;
	var $errors = array ();
	public function __construct() {
		parent::__construct ();
		
		$this->shopify_shop = new shopify_shop ( $this->api );
		$this->shopify_country = new shopify_country ( $this->api );
		$this->shopify_metafield = new shopify_metafield ( $this->api );
		$this->shopify_product = new shopify_product ( $this->api );
		$this->shopify_mapping_manufacturer = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_MANUFACTURER );
		$this->shopify_mapping_category = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_CATEGORY );
		$this->_debug = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DEBUG ) ) ? true : false;
		// echo '<pre>'.print_r($this->shopify_shop, true).'</pre>';exit;
		
		$this->_ouput ();
	}
	function _ouput() {
		if ($this->_validate ()) {
			$this->_xml ();
		} else {
			$this->_accessDeny ();
		}
	}
	private function _validate() {
		$gettoken = isset ( $_GET [constant::FEEDBIZ_TOKEN] ) ? $_GET [constant::FEEDBIZ_TOKEN] : '';
		$fbtoken = $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TOKEN );
		
		return $gettoken == $fbtoken;
	}
	private function _xml() {
		$toFeedBiz = array ();
		$countryCode = isset ( $_GET [constant::FEEDBIZ_COUNTRY] ) ? $_GET [constant::FEEDBIZ_COUNTRY] : '*';
		$countryShop = null;
		$useTaxes = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TAXES ) ) ? true : false;
		$useSpecials = floatval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DISCOUNT ) ) ? true : false;
		$MinDeliveryTime = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_SHIPPING_DELAY ) );
		$country = $this->shopify_country->getCountry ( $countryCode );
		$ShippingList = array ();
		if ($country) {
			foreach ( $country [shopify_country::PROPERTY_WEIGHT_BASED_SHIPPING_RATES] as $shippingRate ) {
				$ShippingList [] = array (
						'id' => $shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__ID],
						'code' => $shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__ID],
						'name' => $shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__NAME] 
				);
			}
		}
		
		// Carrier Configuration
		$selected = isset ( $this->settingFeedbiz ['FEEDBIZ_CARRIER'] ) ? $this->settingFeedbiz ['FEEDBIZ_CARRIER'] : '';
		$products_no = 0;
		$products_determine_no = 0;
		
		// CONTEXT UPDATE
		$this->context = new Context ( $this->shopify_metafield, constant::FEEDBIZ_CONTEXT_OFFERS );
		$products_count = $this->shopify_product->count ( null );
		$this->context->maxProduct = intval ( $products_count );
		$this->context->minProduct = $this->context->maxProduct ? 1 : 0;
		$this->context->timestamp = date ( 'Y-m-d H:m:i' );
		
		$limit = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_PAGE_SIZE ) );
		if (! $limit || $limit > FEEDBIZ_PAGE_SIZE)
			$limit = intval ( FEEDBIZ_PAGE_SIZE );
			
			// echo '<pre>'.print_r($this->context, true).'</pre>';exit;
		$page = $this->context->status == constant::STATUS_COMPLETE || ! $this->context->currentProduct ? 1 : floor ( $this->context->currentProduct / $limit ) + 1;
		$products = $this->shopify_product->getProducts ( $limit, $page );
		
		if ($products)
			foreach ( $products as $details ) {
				$products_determine_no ++;
				$id_product = $details [shopify_product::PROPERTY_ID];
				$product_type = trim ( $details [shopify_product::PROPERTY_PRODUCT_TYPE] );
				$vendor = trim ( $details [shopify_product::PROPERTY_VENDOR] );
				$varians = $details [shopify_product::PROPERTY_VARIANTS];
				$combinationProduct = sizeof ( $varians ) > 1;
				$upc_product = '';
				$sku_product = '';
				
				if (! $combinationProduct) {
					$upc_product = $varians [0] [shopify_product::PROPERTY_BARCODE];
					$sku_product = $varians [0] [shopify_product::PROPERTY_SKU];
				}
				
				if ($details == null) {
					$this->errors [] = sprintf ( 'Could not load the product id: %d', $id_product );
					$error = true;
					continue;
				}
				if (sizeof ( $varians ) == 1)
					$id_product = $varians [0] [shopify_product::PROPERTY_ID];
				
				foreach ( $varians as $varian ) {
					// Pricing
					$varianPriceInc = $varianPrice = floatval ( $varians [0] [shopify_product::PROPERTY_PRICE] );
					$varianCompareAtPriceInc = $varianCompareAtPrice = floatval ( $varians [0] [shopify_product::PROPERTY_COMPARE_AT_PRICE] );
					
					if ($useTaxes) {
						$taxWeight = floatval ( $country [shopify_country::PROPERTY_TAX] );
						$varianPriceInc = $varianPrice * (1 + $taxWeight);
						$varianCompareAtPriceInc = $varianCompareAtPrice * (1 + $taxWeight);
					}
					// if ($useSpecials) {
					// if ($varianCompareAtPrice) {
					// $special_diff = $varianCompareAtPriceInc - $varianPriceInc;
					// $startDate = date ( 'c', strtotime ( '1970-01-01' ) ); // ISO 8601
					// $toDate = date ( 'c', strtotime ( '1970-01-01' ) ); // ISO 8601
					// $priceElement->appendChild ( $p_sale = $Document->createElement ( 'Sale', '' ) );
					// $p_sale->setAttribute ( 'startDate', $startDate );
					// $p_sale->setAttribute ( 'endDate', $toDate );
					// $p_sale->setAttribute ( 'value', round ( $special_diff, 2 ) );
					// $p_sale->setAttribute ( 'type', 'amount' );
					// }
					// }
					
					// Product level
					if (! isset ( $toFeedBiz ['Offers'] [$id_product] ) || empty ( $toFeedBiz ['Offers'] [$id_product] )) {
						$products_no ++;
						$toFeedBiz ['Offers'] [$id_product] ['ProductIdReference'] = intval ( $id_product );
						
						if ($id_product) {
							$toFeedBiz ['Offers'] [$id_product] ['ProductReference'] = $id_product;
						}
						
						$toFeedBiz ['Offers'] [$id_product] ['Vat'] = $country [shopify_country::PROPERTY_ID];
						$toFeedBiz ['Offers'] [$id_product] ['DefaultCategory'] = $this->shopify_mapping_category->getId ( trim ( $product_type ), 2 );
						$toFeedBiz ['Offers'] [$id_product] ['Manufacturer'] = $this->shopify_mapping_manufacturer->getId ( $vendor );
						$basePrice = 0;
						
						if (! $combinationProduct)
							if ($useSpecials && $varianCompareAtPriceInc)
								$basePrice = $varianCompareAtPriceInc;
							else
								$basePrice = $varianPriceInc;
						
						$toFeedBiz ['Offers'] [$id_product] ['BasePrice'] = $basePrice;
						$toFeedBiz ['Offers'] [$id_product] ['MinDeliveryTime'] = $MinDeliveryTime;
						$toFeedBiz ['Offers'] [$id_product] ['ProductCondition'] = 1;
						
						if ($upc_product)
							$toFeedBiz ['Offers'] [$id_product] ['ProductUpc'] = $upc_product;
						if ($sku_product)
							$toFeedBiz ['Offers'] [$id_product] ['ProductSku'] = $sku_product;
						
						foreach ( $ShippingList as $ShippingEle ) {
							$toFeedBiz ['Offers'] [$id_product] ['DefaultCarrier'] = $ShippingEle ['id'];
						}
						
						// Discount
						if ($useSpecials && ! $combinationProduct && $varianCompareAtPriceInc) {
							if ($varianCompareAtPriceInc) {
								$discountItem = array ();
								$special_diff = $varianCompareAtPriceInc - $varianPriceInc;
								$startDate = '1970-01-01';
								$toDate = '1970-01-01';
								$discountItem ['dateStart'] = $startDate;
								$discountItem ['dateEnd'] = $toDate;
								$discountItem ['type'] = 'amount';
								$discountItem ['value'] = sprintf ( '%.02f', round ( $special_diff, 2 ) );
								$toFeedBiz ['Offers'] [$id_product] ['Discount'] = $discountItem;
							}
						}
					}
					
					// Attribute
					if ($combinationProduct) {
						$id_product_attribute = $varian [shopify_product::PROPERTY_ID];
						$price_combinationInc = $price_combination = $varian [shopify_product::PROPERTY_PRICE];
						$sku_combination = $varian [shopify_product::PROPERTY_SKU];
						$upc_combination = $varian [shopify_product::PROPERTY_BARCODE];
						$stock_management = $varian [shopify_product::PROPERTY_INVENTORY_MANAGEMENT] == shopify_product::VALUE_INVENTORY_SHOPIFY_TRACKING;
						$qty_combination = $stock_management ? $varian [shopify_product::PROPERTY_INVENTORY_QUANTITY] : 999;
						
						if ($useTaxes) {
							$taxWeight = floatval ( $country [shopify_country::PROPERTY_TAX] );
							$price_combinationInc = $price_combination * (1 + $taxWeight);
						}
						
						if (isset ( $id_product_attribute ) && ! empty ( $id_product_attribute )) {
							$toFeedBiz ['Offers'] [$id_product] ['items'] [$id_product_attribute] ['AttributeIdReference'] = $id_product_attribute;
							$toFeedBiz ['Offers'] [$id_product] ['items'] [$id_product_attribute] ['ProductReference'] = $id_product_attribute;
							
							if ($upc_combination)
								$toFeedBiz ['Offers'] [$id_product] ['items'] [$id_product_attribute] ['ProductUpc'] = $upc_combination;
							if ($sku_combination)
								$toFeedBiz ['Offers'] [$id_product] ['items'] [$id_product_attribute] ['ProductSku'] = $sku_combination;
							
							$toFeedBiz ['Offers'] [$id_product] ['items'] [$id_product_attribute] ['Stock'] = $qty_combination;
							$toFeedBiz ['Offers'] [$id_product] ['items'] [$id_product_attribute] ['AdditionalPrice'] = sprintf ( '%.02f', round ( $price_combinationInc, 2 ) );
							$toFeedBiz ['Offers'] [$id_product] ['items'] [$id_product_attribute] ['SalesReferencePrice'] = sprintf ( '%.02f', round ( $price_combinationInc, 2 ) );
						}
						$history [$id_product] [$id_product_attribute] = true;
					}
					
					if ($this->_debug) {
						echo "Content:" . nl2br ( print_r ( $toFeedBiz ['Offers'] [$id_product], true ) ) . $cr;
						echo $cr;
						echo "Memory: " . number_format ( memory_get_usage () / 1024 ) . 'k' . $cr;
					}
					
					if ($this->_debug)
						printf ( "Exporting Product: %d id: %d reference: %s %s", $idx, $details ['product_id'], $reference, $cr );
					if ($limit == $products_no) {
						break;
					}
				} // end foreach varians
				  
				// Category
				if (trim ( $product_type )) {
					$categoryID = $this->shopify_mapping_category->getId ( trim ( $product_type ), 2 );
					$toFeedBiz ['Categories'] [$categoryID] = $product_type;
				}
				// Manufacturer
				if (trim ( $vendor )) {
					$manufacturerID = $this->shopify_mapping_manufacturer->getId ( trim ( $vendor ) );
					$toFeedBiz ['Manufacturers'] [$manufacturerID] = $vendor;
				}
			} // end foreach products
				  
		// CONTEXT COUNT
				  // CONTEXT COUNT
		$this->context->currentProduct = (($limit * ($page - 1)) + $products_no);
		$this->context->currentProductCount = $products_no;
		$this->context->status = $this->context->maxProduct == (($limit * ($page - 1)) + $products_no) ? constant::STATUS_COMPLETE : constant::STATUS_INCOMPLETE; // previous + current
		                                                                                                                                                          // Make XML
		$this->createOffers ( $toFeedBiz );
	}
	private function createOffers($offers) {
		$cr = "\n<br/>";
		
		if ($this->_debug)
			echo nl2br ( print_r ( $offers, true ) ) . $cr;
		
		$Document = new DOMDocument ();
		$Document->preserveWhiteSpace = true;
		$Document->formatOutput = true;
		$Document->encoding = 'utf-8';
		$Document->version = '1.0';
		$OfferPackage = $Document->appendChild ( $Document->createElement ( 'OfferPackage', '' ) );
		$OfferPackage->setAttribute ( 'ShopName', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_NAME ) );
		
		$Offers = $OfferPackage->appendChild ( $Document->createElement ( 'Offers', '' ) );
		
		$curr_id = $Offers->appendChild ( $Document->createElement ( 'Currency', '' ) );
		$curr_id->setAttribute ( 'ID', 1 );
		
		if (isset ( $offers ['Offers'] )) {
			foreach ( $offers ['Offers'] as $id => $Product ) {
				$Offer = $Offers->appendChild ( $Document->createElement ( 'Offer', '' ) );
				ksort ( $Product );
				
				foreach ( $Product as $attr => $value ) {
					if ($attr == 'items') {
						$items = $Offer->appendChild ( $Document->createElement ( 'Items', '' ) );
						
						foreach ( $value as $items_value ) {
							$items->appendChild ( $item = $Document->createElement ( 'Item', '' ) );
							
							foreach ( $items_value as $item_attr => $item_value )
								$item->setAttribute ( $item_attr, $item_value );
						}
					} elseif ($attr == 'Discount') {
						$discountItem = $value;
						$DiscountList = $Offer->appendChild ( $Document->createElement ( 'DiscountList', '' ) );
						$Discount = $DiscountList->appendChild ( $Document->createElement ( 'Discount', '' ) );
						
						$Discount->setAttribute ( 'DiscountValue', $discountItem ['value'] );
						$Discount->setAttribute ( 'DiscountType', $discountItem ['type'] );
						$Discount->setAttribute ( 'StartDate', $discountItem ['dateStart'] );
						$Discount->setAttribute ( 'EndDate', $discountItem ['dateEnd'] );
					} else { // Standard Offer
						$Offer->setAttribute ( $attr, $value );
					}
				}
				
				if ($this->_debug) {
					echo $cr;
					echo "Memory: " . number_format ( memory_get_usage () / 1024 ) . 'k' . $cr;
				}
			}
		}
		if (isset ( $offers ['Categories'] )) {
			$CategoriesDom = $OfferPackage->appendChild ( $Document->createElement ( 'Categories', '' ) );
			$CategoryDom = $CategoriesDom->appendChild ( $Document->createElement ( 'Category', '' ) );
			$CategoryDom->setAttribute ( 'ID', 1 );
			$CategoryDom->setAttribute ( 'parent', 'root' );
			$CategoryNameDom = $CategoryDom->appendChild ( $Document->createElement ( 'Name', '' ) );
			$CategoryNameDom->setAttribute ( 'lang', 1 );
			$CategoryNameDom->appendChild ( $Document->createCDATASection ( 'Root' ) );
			foreach ( $offers ['Categories'] as $id => $name ) {
				$CategoryDom = $CategoriesDom->appendChild ( $Document->createElement ( 'Category', '' ) );
				$CategoryDom->setAttribute ( 'ID', $id );
				$CategoryDom->setAttribute ( 'parent', 1 );
				$CategoryNameDom = $CategoryDom->appendChild ( $Document->createElement ( 'Name', '' ) );
				$CategoryNameDom->setAttribute ( 'lang', 1 );
				$CategoryNameDom->appendChild ( $Document->createCDATASection ( $name ) );
			}
		}
		if (isset ( $offers ['Manufacturers'] )) {
			$ManufacturersDom = $OfferPackage->appendChild ( $Document->createElement ( 'Manufacturers', '' ) );
			foreach ( $offers ['Manufacturers'] as $id => $name ) {
				$ManufacturerDom = $ManufacturersDom->appendChild ( $Document->createElement ( 'Manufacturer', '' ) );
				$ManufacturerDom->setAttribute ( 'ID', $id );
				$ManufacturerDom->appendChild ( $Document->createCDATASection ( $name ) );
			}
		}
		
		// CONTEXT SAVING
		$OfferPackage->appendChild ( $statusDoc = $Document->createElement ( 'Status', '' ) );
		$statusDoc->appendChild ( $Document->createElement ( 'Code', $this->context->status == constant::STATUS_INCOMPLETE ? '-1' : '1' ) );
		$statusDoc->appendChild ( $Document->createElement ( 'Message', $this->context->status ) );
		$statusDoc->appendChild ( $Document->createElement ( 'ExportTotal', count ( $offers ['Offers'] ) ) );
		$statusDoc->appendChild ( $Document->createElement ( 'CurrentProductID', $this->context->currentProduct ) );
		$statusDoc->appendChild ( $Document->createElement ( 'MinProductID', $this->context->minProduct ) );
		$statusDoc->appendChild ( $Document->createElement ( 'MaxProductID', $this->context->maxProduct ) );
		
		// FULL EDIT SETTING
		// $this->settingFeedbiz['FEEDBIZ_PRODUCT_UPDATE_CONTEXT'] = serialize($this->context);
		// $this->model_feedbiz_setting->editSetting(_SETTING_GROUP_FEEDBIZ_, $this->settingFeedbiz, $this->id_store);
		// PARTIAL EDIT SETTING : Full update not guarantee for transaction roll back in case of failure, so it lost some settings after clear(delete) old settings.
		$this->context->save ();
		
		if (! $this->_debug) {
			ob_end_clean ();
			header ( "Content-Type: application/xml; charset=utf-8" );
			echo $Document->saveXML ();
		}
	}
	private function _accessDeny() {
		$Document = new DOMDocument ();
		$Document->preserveWhiteSpace = true;
		$Document->formatOutput = true;
		$Document->encoding = 'utf-8';
		$Document->version = '1.0';
		
		$ExportData = $Document->appendChild ( $Document->createElement ( 'ExportData', '' ) );
		$ExportData->setAttribute ( 'ShopName', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_NAME ) );
		$ExportData->appendChild ( $errorDoc = $Document->createElement ( 'Status', '' ) );
		$errorDoc->appendChild ( $Document->createElement ( 'Code', '0' ) );
		$errorDoc->appendChild ( $Document->createElement ( 'Message', 'Access denied' ) );
		
		header ( "Content-Type: application/xml; charset=utf-8" );
		echo $Document->saveXML ();
		exit ();
	}
}

new Products ();
