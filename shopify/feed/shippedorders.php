<?php
require '../classes/feed.php';
class ShippedOrders extends Feed {
	var $shop = null;
	var $shopify_shop = null;
	var $shopify_metafield = null;
	var $shopify_order = null;
	var $orderModel = null;
	var $orders = null;
	var $_debug = false;
	var $errors = array ();
	var $statusCode = '-1';
	var $status = 'Access denied';
	var $returnData = array ();
	var $_cr = "\n<br/>";
	public function __construct() {
		parent::__construct ();
		
		@ini_set ( 'display_errors', 1 );
		$this->shop = isset ( $_GET ['shop'] ) ? $_GET ['shop'] : '';
		$this->shopify_shop = new shopify_shop ( $this->api );
		$this->shopify_metafield = new shopify_metafield ( $this->api );
		$this->shopify_order = new shopify_order ( $this->api );
		$this->_debug = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DEBUG ) ) ? true : false;
		$this->orderModel = new OrderModel ( $this->shop, $this->_debug );
		// echo '<pre>'.print_r($this->shopify_shop, true).'</pre>';exit;
		
		register_shutdown_function ( array (
				$this,
				'_FBShutdowFunction' 
		) );
		$this->_ouput ();
	}
	function _ouput() {
		ob_start ();
		if ($this->_validate ()) {
			$this->statusCode = '0';
			$this->status = 'Fail';
			
			if ($this->_shippedOrders ()) {
				$this->statusCode = '1';
				$this->status = 'Success';
			}
		} else {
			$this->statusCode = '-1';
			$this->status = 'Access denied';
		}
	}
	private function _validate() {
		$gettoken = isset ( $_GET [constant::FEEDBIZ_TOKEN] ) ? $_GET [constant::FEEDBIZ_TOKEN] : '';
		$fbtoken = $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TOKEN );
		
		return $gettoken == $fbtoken;
	}
	function _FBShutdowFunction() {
		if ($this->_debug) {
			echo '<pre>' . print_r ( $this->orders, true ) . '</pre>';
		} else {
			$level_names = array (
					E_ERROR => 'E_ERROR',
					E_WARNING => 'E_WARNING',
					E_PARSE => 'E_PARSE',
					E_NOTICE => 'E_NOTICE',
					E_CORE_ERROR => 'E_CORE_ERROR',
					E_CORE_WARNING => 'E_CORE_WARNING',
					E_COMPILE_ERROR => 'E_COMPILE_ERROR',
					E_COMPILE_WARNING => 'E_COMPILE_WARNING',
					E_USER_ERROR => 'E_USER_ERROR',
					E_USER_WARNING => 'E_USER_WARNING',
					E_USER_NOTICE => 'E_USER_NOTICE' 
			);
			
			$outBuffer = ob_get_contents ();
			
			$Document = new DOMDocument ();
			$Document->preserveWhiteSpace = true;
			$Document->formatOutput = true;
			$Document->encoding = 'utf-8';
			$Document->version = '1.0';
			
			$ShippedPackage = $Document->appendChild ( $Document->createElement ( 'Result' ) );
			$ShippedPackage->setAttribute ( 'ShopName', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_NAME ) );
			
			$OrdersDOM = $ShippedPackage->appendChild ( $Document->createElement ( 'Orders' ) );
			
			foreach ( $this->orders as $order ) {
				$OrderDOM = $OrdersDOM->appendChild ( $Document->createElement ( 'Order' ) );
				$OrderDOM->setAttribute ( 'ID', $order [shopify_order::PROPERTY_ORDER_NUMBER] );
				$OrderDOM->appendChild ( $Document->createElement ( 'MPOrderID', $order [OrderModel::COLUMN_MP_ORDER_ID] ) );
				$OrderDOM->appendChild ( $Document->createElement ( 'CarrierID', $order [shopify_order::PROPERTY_TRACKING_COMPANY] ) );
				$OrderDOM->appendChild ( $Document->createElement ( 'CarrierName', $order [shopify_order::PROPERTY_TRACKING_COMPANY] ) );
				$OrderDOM->appendChild ( $Document->createElement ( 'ShippingNumber', $order [shopify_order::PROPERTY_TRACKING_NUMBER] ) );
				$OrderDOM->appendChild ( $Document->createElement ( 'ShippingDate', $order [shopify_order::PROPERTY_CREATED_AT] ) );
			}
			
			$ShippedPackage->appendChild ( $StatusDoc = $Document->createElement ( 'Status', '' ) );
			$StatusDoc->appendChild ( $Document->createElement ( 'Code', $this->statusCode ) );
			$StatusDoc->appendChild ( $Document->createElement ( 'Message', $this->status ) );
			$StatusDoc->appendChild ( $Document->createElement ( 'Output', implode ( ", ", $this->errors ) ) );
			$errorDoc = $StatusDoc->appendChild ( $Document->createElement ( 'Error' ) );
			$errorDoc->appendChild ( $Document->createCDATASection ( $outBuffer ) );
			
			ob_end_clean ();
			ob_start ();
			header ( "Content-Type: application/xml; charset=utf-8" );
			echo $Document->saveXML ();
		}
		exit ( 1 );
	}
	function _shippedOrders() {
		$this->orders = array ();
		
		$orders = $this->shopify_order->getOrdersShipped ();
		
		if ($this->_debug) {
			echo '<pre>' . print_r ( $orders, true ) . '</pre>';
		}
		
		foreach ( $orders as $order ) {
			if ($order [shopify_order::PROPERTY_FULLFILLMENTS] && $order_log = $this->orderModel->exist (0, $order [shopify_order::PROPERTY_ORDER_NUMBER], $this->_debug )) {
				
				$orderEle = array ();
				$orderEle [shopify_order::PROPERTY_ORDER_NUMBER] = $order [shopify_order::PROPERTY_ORDER_NUMBER];
				foreach ( $order_log as $order_log_ele ) {
					$orderEle [OrderModel::COLUMN_MP_ORDER_ID] = $order_log_ele [OrderModel::COLUMN_MP_ORDER_ID];
				}
				
				foreach ( $order [shopify_order::PROPERTY_FULLFILLMENTS] as $fulfillment ) {
					if (isset ( $fulfillment [shopify_order::PROPERTY_TRACKING_NUMBER] ) && $fulfillment [shopify_order::PROPERTY_TRACKING_NUMBER] && isset ( $fulfillment [shopify_order::PROPERTY_CREATED_AT] ) && $fulfillment [shopify_order::PROPERTY_CREATED_AT]) {
						$orderEle [shopify_order::PROPERTY_TRACKING_NUMBER] = $fulfillment [shopify_order::PROPERTY_TRACKING_NUMBER];
						$orderEle [shopify_order::PROPERTY_CREATED_AT] = isset ( $fulfillment [shopify_order::PROPERTY_CREATED_AT] ) ? $fulfillment [shopify_order::PROPERTY_CREATED_AT] : '';
						$orderEle [shopify_order::PROPERTY_TRACKING_COMPANY] = isset ( $fulfillment [shopify_order::PROPERTY_TRACKING_COMPANY] ) ? $fulfillment [shopify_order::PROPERTY_TRACKING_COMPANY] : '';
					}
				}
				
				$this->orders [$order [shopify_order::PROPERTY_ORDER_NUMBER]] = $orderEle;
			}
		}
		return true;
	}
}

new ShippedOrders ();
