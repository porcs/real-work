<?php
require '../classes/feed.php';
class Products extends Feed {
	var $shop = '';
	var $shopify_shop = null;
	var $shopify_metafield = null;
	var $shopify_product = null;
	var $shopify_order = null;
	var $shopify_mapping_manufacturer = null;
	var $shopify_mapping_category = null;
	var $context = null;
	var $_debug = false;
	var $_usetax = false;
	var $errors = array ();
	var $statusCode = '0';
	var $status = 'Fail';
	var $returnData = array ();
	var $orderModel = null;
	var $_cr = "\n<br/>";
	public function __construct() {
		parent::__construct ();
		
		$this->shop = isset ( $_GET ['shop'] ) ? $_GET ['shop'] : '';
		$this->shopify_shop = new shopify_shop ( $this->api );
		$this->shopify_metafield = new shopify_metafield ( $this->api );
		$this->shopify_product = new shopify_product ( $this->api );
		$this->shopify_order = new shopify_order ( $this->api );
		$this->shopify_mapping_manufacturer = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_MANUFACTURER );
		$this->shopify_mapping_category = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_CATEGORY );
		$this->_debug = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DEBUG ) ) ? true : false;
		
		if ($this->_debug) {
			error_reporting ( E_ALL );
			ini_set ( 'display_errors', 1 );
		}
		
		$this->_usetax = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TAXES ) ) ? true : false;
		$this->orderModel = new OrderModel ( $this->shop, $this->_debug );
		// echo '<pre>'.print_r($this->shopify_shop, true).'</pre>';exit;
		
		@ini_set ( 'display_errors', 1 );
		register_shutdown_function ( array (
				$this,
				'_FBShutdowFunction' 
		) );
		
		$this->_ouput ();
	}
	function _ouput() {
		ob_start ();
		if ($this->_validate ()) {
			$order = $this->_getOrder ();
			
			if ($response = $this->_importOrder ( $order, $this->_usetax, $this->_debug )) {
				if ($this->_debug) {
					echo '<pre>' . print_r ( $this->errors, true ) . '</pre>';
				}
				
				$this->returnData ['InvoiceNumber'] = $response [shopify_order::PROPERTY_ID];
				$this->returnData ['OrderNumber'] = $response [shopify_order::PROPERTY_ORDER_NUMBER];
				
				if (isset ( $response [shopify_order::PROPERTY_ORDER_NUMBER] ) && $response [shopify_order::PROPERTY_ORDER_NUMBER]) {
					$this->orderModel->create ( intval ( $response [shopify_order::PROPERTY_ORDER_NUMBER] ), intval ( $order->id_orders ) );
				}
				
				$this->statusCode = '1';
				$this->status = 'Success';
			}
		} else {
			$this->statusCode = '-1';
			$this->status = 'Access denied';
		}
	}
	private function _validate() {
		$gettoken = isset ( $_GET [constant::FEEDBIZ_TOKEN] ) ? $_GET [constant::FEEDBIZ_TOKEN] : '';
		$fbtoken = $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TOKEN );
		
		return $gettoken == $fbtoken;
	}
	function _FBShutdowFunction() {
		$level_names = array (
				E_ERROR => 'E_ERROR',
				E_WARNING => 'E_WARNING',
				E_PARSE => 'E_PARSE',
				E_NOTICE => 'E_NOTICE',
				E_CORE_ERROR => 'E_CORE_ERROR',
				E_CORE_WARNING => 'E_CORE_WARNING',
				E_COMPILE_ERROR => 'E_COMPILE_ERROR',
				E_COMPILE_WARNING => 'E_COMPILE_WARNING',
				E_USER_ERROR => 'E_USER_ERROR',
				E_USER_WARNING => 'E_USER_WARNING',
				E_USER_NOTICE => 'E_USER_NOTICE' 
		);
		
		$outBuffer = ob_get_contents ();
		
		$Document = new DOMDocument ();
		$Document->preserveWhiteSpace = true;
		$Document->formatOutput = true;
		$Document->encoding = 'utf-8';
		$Document->version = '1.0';
		
		$OfferPackage = $Document->appendChild ( $Document->createElement ( 'Result' ) );
		$OfferPackage->setAttribute ( 'ShopName', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_NAME ) );
		$OfferPackage->appendChild ( $StatusDoc = $Document->createElement ( 'Status', '' ) );
		$StatusDoc->appendChild ( $Document->createElement ( 'Code', $this->statusCode ) );
		$StatusDoc->appendChild ( $Document->createElement ( 'Message', $this->status ) );
		$StatusDoc->appendChild ( $Document->createElement ( 'Output', implode ( ", ", $this->errors ) ) );
		$errorDoc = $StatusDoc->appendChild ( $Document->createElement ( 'Error' ) );
		$errorDoc->appendChild ( $Document->createCDATASection ( $outBuffer ) );
		
		if (isset ( $this->returnData ['InvoiceNumber'] ))
			$StatusDoc->appendChild ( $Document->createElement ( 'InvoiceNumber', $this->returnData ['InvoiceNumber'] ) );
		if (isset ( $this->returnData ['OrderNumber'] ))
			$StatusDoc->appendChild ( $Document->createElement ( 'OrderNumber', $this->returnData ['OrderNumber'] ) );
		
		if (! $this->_debug) {
			ob_end_clean ();
			ob_start ();
			header ( "Content-Type: application/xml; charset=utf-8" );
			echo $Document->saveXML ();
			exit ( 1 );
		}
	}
	private function _getOrder() {
		$params = array (
				'token' => $_GET [constant::FEEDBIZ_TOKEN],
				'id_order' => $_GET [constant::FEEDBIZ_ORDER],
				'otp' => md5 ( md5 ( $_GET [constant::FEEDBIZ_OTP] ) ) 
		);
		
		$order = service::getOrder ( $params, constant::METHOD_GET, true, $this->_debug );
		
		// $order = new SimpleXMLElement ( '<return>
		// <id_orders>15</id_orders>
		// <id_marketplace_order_ref>205-5713244-6050755</id_marketplace_order_ref>
		// <sales_channel>Amazon.co.uk</sales_channel>
		// <id_shop>2</id_shop>
		// <id_lang>2</id_lang>
		// <site>2</site>
		// <order_type>StandardOrder</order_type>
		// <order_status>Unshipped</order_status>
		// <payment_method>Other</payment_method>
		// <total_discount>0</total_discount>
		// <total_amount>0</total_amount>
		// <total_paid>32.74</total_paid>
		// <total_shipping>10</total_shipping>
		// <order_date>2014-08-05T22:54:21Z</order_date>
		// <shipping_date>2014-08-05T23:00:00Z</shipping_date>
		// <delivery_date>2014-08-06T23:00:00Z</delivery_date>
		// <purchase_date>2014-08-05T22:54:21Z</purchase_date>
		// <affiliate_id>0</affiliate_id>
		// <commission>0</commission>
		// <ip>205-5713244-6050755</ip>
		// <gift_message/>
		// <gift_amount>0</gift_amount>
		// <comment/>
		// <status>0</status>
		// <date_add>2014-11-07 17:54:58</date_add>
		// <id>15</id>
		// <user>Praew</user>
		// <buyer>
		// <id_buyer/>
		// <id_buyer_ref>205-5713244-6050755</id_buyer_ref>
		// <site>2</site>
		// <email>ekawit.ch@gmail.com</email>
		// <name>Daniel Jenkins</name>
		// <id_address_ref>Daniel Jenkins</id_address_ref>
		// <address1>9 Gendros Crescent</address1>
		// <address2>Gendros</address2>
		// <city>SWANSEA</city>
		// <district/>
		// <state_region>W Glam</state_region>
		// <country_code>UK</country_code>
		// <country_name>United Kingdom</country_name>
		// <postal_code>SA5 8EL</postal_code>
		// <phone>01792 578023</phone>
		// <security_code_token/>
		// </buyer>
		// <carrier>
		// <shipment_service>Exp UK Dom</shipment_service>
		// <weight>0</weight>
		// <tracking_number/>
		// <shipping_services_cost>10</shipping_services_cost>
		// <shipping_services_level>Expedited</shipping_services_level>
		// <id_carrier>0</id_carrier>
		// </carrier>
		// <payment>
		// <payment_method>Other</payment_method>
		// <payment_status>Unshipped</payment_status>
		// <id_currency>4</id_currency>
		// <Amount>32.74</Amount>
		// <external_transaction_id/>
		// </payment>
		// <invoices>
		// <id_invoice>15</id_invoice>
		// <total_discount_tax_excl/>
		// <total_discount_tax_incl>0</total_discount_tax_incl>
		// <total_paid_tax_excl/>
		// <total_paid_tax_incl>32.74</total_paid_tax_incl>
		// <total_products>32.74</total_products>
		// <total_shipping_tax_excl/>
		// <total_shipping_tax_incl>10</total_shipping_tax_incl>
		// <note/>
		// <date_add>2014-11-07 17:54:58</date_add>
		// <invoice_no>#FA195419</invoice_no>
		// </invoices>
		// <item>
		// <property_0>
		// <id_orders>15</id_orders>
		// <id_order_items>29</id_order_items>
		// <id_marketplace_order_item_ref>02854256258067</id_marketplace_order_item_ref>
		// <id_shop>2</id_shop>
		// <id_product>11683</id_product>
		// <reference>989455929</reference>
		// <id_marketplace_product>B00C1B28Y2</id_marketplace_product>
		// <id_product_attribute>71306</id_product_attribute>
		// <product_name>
		// Dragon Ball Kai Figuarts ZERO Super Saiyan Goku (japan import)
		// </product_name>
		// <quantity>1</quantity>
		// <quantity_in_stock>0</quantity_in_stock>
		// <quantity_refunded/>
		// <product_weight/>
		// <product_price>5.5</product_price>
		// <shipping_price>0</shipping_price>
		// <tax_rate>0</tax_rate>
		// <unit_price_tax_incl>22.74</unit_price_tax_incl>
		// <unit_price_tax_excl/>
		// <total_price_tax_incl>22.74</total_price_tax_incl>
		// <total_price_tax_excl>22.74</total_price_tax_excl>
		// <total_shipping_price_tax_incl>0</total_shipping_price_tax_incl>
		// <total_shipping_price_tax_excl/>
		// <id_condition>1</id_condition>
		// <promotion/>
		// <message/>
		// <id_status/>
		// <attributes>
		// <property_0>
		// <id_attribute_group>55</id_attribute_group>
		// <id_attribute>404</id_attribute>
		// </property_0>
		// </attributes>
		// </property_0>
		// </item>
		// </return>' );
		
		return $order;
	}
	private function _importOrder($order, $usetax = true, $debug = false) {
		if (empty ( $order ) || ! ( int ) $order->id_orders) {
			$this->errors [] = sprintf ( '%s/%s: %s', basename ( __FILE__ ), __LINE__, 'Unable to read orders.' ) . $this->_cr;
			return false;
		}
		
		if ($this->orderModel->exist ( strval ( $order->id_orders ), 0, $debug )) {
			$this->errors [] = sprintf ( '%s/%s: %s', basename ( __FILE__ ), __LINE__, 'Order has been imported already.' ) . $this->_cr;
			return true;
		}
		
		if (! isset ( $order->buyer->email ) || ! Tool::isEmail ( $order->buyer->email )) {
			$this->errors [] = sprintf ( '%s/%s: %s (%s)', basename ( __FILE__ ), __LINE__, 'Invalid customer email address', isset ( $order->buyer->email ) ? $order->buyer->email : null ) . $this->_cr;
			return false;
		}
		
		if (! isset ( $order->buyer->country_code )) {
			$this->errors [] = sprintf ( '%s/%s: %s', basename ( __FILE__ ), __LINE__, 'Require buyer country code.' ) . $this->_cr;
			return false;
		}
		if (! isset ( $order->buyer->name )) {
			$this->errors [] = sprintf ( '%s/%s: %s', basename ( __FILE__ ), __LINE__, 'Require buyer name.' ) . $this->_cr;
			return false;
		}
		if (! isset ( $order->buyer->address1 )) {
			$this->errors [] = sprintf ( '%s/%s: %s', basename ( __FILE__ ), __LINE__, 'Require buyer address1.' ) . $this->_cr;
			return false;
		}
		if (! isset ( $order->buyer->postal_code )) {
			$this->errors [] = sprintf ( '%s/%s: %s', basename ( __FILE__ ), __LINE__, 'Require buyer postal_code.' ) . $this->_cr;
			return false;
		}
		if (! isset ( $order->buyer->city )) {
			$this->errors [] = sprintf ( '%s/%s: %s', basename ( __FILE__ ), __LINE__, 'Require buyer city.' ) . $this->_cr;
			return false;
		}
		
		return $this->shopify_order->createOrder ( $order, $usetax, $debug );
	}
}

new Products ();
