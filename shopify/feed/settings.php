<?php
require '../classes/feed.php';
class Settings extends Feed {
	var $shopify_shop = null;
	var $shopify_country = null;
	var $shopify_metafield = null;
	var $shopify_product = null;
	var $shopify_mapping_maufacturer = null;
	var $shopify_mapping_category = null;
	var $debug = false;
	public function __construct() {
		parent::__construct ();
		
		$this->shopify_shop = new shopify_shop ( $this->api );
		$this->shopify_country = new shopify_country ( $this->api );
		$this->shopify_metafield = new shopify_metafield ( $this->api );
		$this->shopify_product = new shopify_product ( $this->api );
		$this->shopify_mapping_maufacturer = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_MANUFACTURER );
		$this->shopify_mapping_category = new shopify_mapping ( $this->shopify_metafield, constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_MAPPING_CATEGORY );
		$this->debug = intval ( $this->shopify_metafield->getValue ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_DEBUG ) ) ? true : false;
		$this->_ouput ();
	}
	function _ouput() {
		if ($this->_validate ()) {
			$this->_xml ();
		} else {
			$this->_accessDeny ();
		}
	}
	private function _validate() {
		$gettoken = isset ( $_GET [constant::FEEDBIZ_TOKEN] ) ? $_GET [constant::FEEDBIZ_TOKEN] : '';
		$fbtoken = $this->shopify_metafield->get ( constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TOKEN );
		
		return $gettoken == $fbtoken [shopify_metafield::FIELD_VALUE];
	}
	private function _xml() {
		$metafields = $this->shopify_metafield->get ( constant::FEEDBIZ_NAMESPACE );
		$useTaxes = ( int ) $metafields [constant::FEEDBIZ_TAXES] ? true : false;
		$id_currency = $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_CURRENCY );
		
		ob_start ();
		
		// create DOMDocument();
		$Document = new DOMDocument ();
		$Document->preserveWhiteSpace = true;
		$Document->formatOutput = true;
		$Document->encoding = 'utf-8';
		$Document->version = '1.0';
		
		$ExportData = $Document->appendChild ( $Document->createElement ( 'ExportData' ) );
		$ExportData->setAttribute ( 'ShopName', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_NAME ) );
		
		// Language
		$LanguageElement = $ExportData->appendChild ( $Document->createElement ( 'Language' ) );
		$LanguageNameElement = $LanguageElement->appendChild ( $language_id = $Document->createElement ( 'Name' ) );
		$LanguageNameElement->appendChild ( $Document->createCDATASection ( $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_COUNTRY_NAME ) ) );
		$language_id->setAttribute ( 'ID', 1 );
		$language_id->setAttribute ( 'iso_code', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_COUNTRY_CODE ) );
		$language_id->setAttribute ( 'is_default', 1 );
		
		// Currencies
		$CurrenciesElement = $ExportData->appendChild ( $Document->createElement ( 'Currencies' ) );
		$CurrenciesElement->appendChild ( $currency_id = $Document->createElement ( 'Currency', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_CURRENCY ) ) );
		$currency_id->setAttribute ( 'ID', 1 );
		$currency_id->setAttribute ( 'iso_code', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_CURRENCY ) );
		
		// Taxes
		$taxesElement = $ExportData->appendChild ( $Document->createElement ( 'Taxes' ) );
		$shopifyCountries = $this->shopify_country->getCountries ();
		foreach ( $shopifyCountries as $shopifyCountry ) {
			$taxesTaxElement = $taxesElement->appendChild ( $t_tax = $Document->createElement ( 'Tax' ) );
			$t_tax->setAttribute ( 'type', 'vat' );
			$t_tax->setAttribute ( 'id', $shopifyCountry [shopify_country::PROPERTY_ID] );
			$taxesTaxElement->appendChild ( $t_Rate = $Document->createElement ( 'Rate', $shopifyCountry [shopify_country::PROPERTY_TAX] ) );
			$t_Rate->setAttribute ( 'type', 'percent' );
			
			$taxesTaxName = $taxesTaxElement->appendChild ( $tax_name = $Document->createElement ( 'Name' ) );
			$taxesTaxName->appendChild ( $Document->createCDATASection ( $shopifyCountry [shopify_country::PROPERTY_NAME] ) );
			$tax_name->setAttribute ( 'lang', 1 );
		}
		
		// Carrier
		$carrierData = array ();
		$CarriersElement = $ExportData->appendChild ( $Document->createElement ( 'Carriers' ) );
		foreach ( $shopifyCountries as $shopifyCountry ) {
			foreach ( $shopifyCountry [shopify_country::PROPERTY_WEIGHT_BASED_SHIPPING_RATES] as $shippingRate ) {
				$carrierData [$shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__ID]] = $shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__NAME];
			}
			foreach ( $shopifyCountry [shopify_country::PROPERTY_PRICE_BASED_SHIPPING_RATES] as $shippingRate ) {
				$carrierData [$shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__ID]] = $shippingRate [shopify_country::PROPERTY_SHIPPING_RATE__NAME];
			}
		}
		
		foreach ( $carrierData as $carrierID => $carrierName ) {
			$CarrierElement = $CarriersElement->appendChild ( $carrier_id = $Document->createElement ( 'Carrier' ) );
			$carrier_id->setAttribute ( 'ID', $carrierID );
			$ProductCarrier = $CarrierElement->appendChild ( $Document->createElement ( 'Name' ) );
			$ProductCarrier->appendChild ( $Document->createCDATASection ( $carrierName ) );
		}
		
		// There is no feature
		$ExportData->appendChild ( $Document->createElement ( 'Features' ) );
		
		// Units
		$UnitsElement = $ExportData->appendChild ( $Document->createElement ( 'Units' ) );
		$UnitsElement->appendChild ( $weight_id = $Document->createElement ( 'Unit', 'g' ) );
		$weight_id->setAttribute ( 'ID', '1' );
		$weight_id->setAttribute ( 'Type', 'Weight' );
		
		// Conditions
		$ConditionsElement = $ExportData->appendChild ( $Document->createElement ( 'Conditions' ) );
		$ConditionElement = $ConditionsElement->appendChild ( $cond_new_id = $Document->createElement ( 'Condition' ) );
		$ConditionElement->appendChild ( $Document->createCDATASection ( 'new' ) );
		$cond_new_id->setAttribute ( 'ID', 1 );
		
		// There is no supplier
		$ExportData->appendChild ( $Document->createElement ( 'Suppliers' ) );
		
		if ($this->debug) {
			
		} else {
			$this->shopify_mapping_maufacturer->save ();
			$this->shopify_mapping_category->save ();
			ob_end_clean ();
			header ( "Content-Type: application/xml; charset=utf-8" );
			echo $Document->saveXML ();
		}
	}
	private function _accessDeny() {
		$Document = new DOMDocument ();
		$Document->preserveWhiteSpace = true;
		$Document->formatOutput = true;
		$Document->encoding = 'utf-8';
		$Document->version = '1.0';
		
		$ExportData = $Document->appendChild ( $Document->createElement ( 'ExportData', '' ) );
		$ExportData->setAttribute ( 'ShopName', $this->shopify_shop->getProperty ( shopify_shop::PROPERTY_NAME ) );
		$ExportData->appendChild ( $errorDoc = $Document->createElement ( 'Status', '' ) );
		$errorDoc->appendChild ( $Document->createElement ( 'Code', '0' ) );
		$errorDoc->appendChild ( $Document->createElement ( 'Message', 'Access denied' ) );
		
		header ( "Content-Type: application/xml; charset=utf-8" );
		echo $Document->saveXML ();
		exit ();
	}
}

new Settings ();
