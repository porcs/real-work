<?php

require '../classes/feed.php';

class Connector extends Feed {
    var $metafield = null;
    var $shop = null;
    
    public function __construct() {
        parent::__construct();
        $this->_ouput();
    }

    function _ouput() {
        $this->shopify_metafield = new shopify_metafield($this->api);
        $this->shopify_shop = new shopify_shop($this->api);
        
        if ($this->_validate()) {
            $Document = new DOMDocument ();
            $Document->preserveWhiteSpace = true;
            $Document->formatOutput = true;
            $Document->encoding = 'utf-8';
            $Document->version = '1.0';

            $DOMConnector = $Document->appendChild($Document->createElement('Connector', ''));
            $DOMConnector->setAttribute('ShopName', $this->shopify_shop->getProperty(shopify_shop::PROPERTY_NAME));
            $DOMSoftware = $DOMConnector->appendChild($Document->createElement('Software', ''));
            $DOMSoftware->setAttribute('Name', 'shopify');
            $DOMSoftware->setAttribute('Version', 'Official');
            
//            $settingUrl = sprintf(HTTPS_SERVER_FEED_SETTINGS, $this->shop, $this->token);
            //echo $settingUrl;
            $DOMUrl = $DOMConnector->appendChild($Document->createElement('Urls', ''));
            $settingUrl = $DOMUrl->appendChild($Document->createElement('Settings'));
            $settingUrl->appendChild($Document->createCDATASection(sprintf(HTTPS_SERVER_FEED_SETTINGS, $this->shop, $this->token)));
            $productsUrl = $DOMUrl->appendChild($Document->createElement('Products'));
            $productsUrl->appendChild($Document->createCDATASection(sprintf(HTTPS_SERVER_FEED_PRODUCTS, $this->shop, $this->token)));
            $offersUrl = $DOMUrl->appendChild($Document->createElement('Offers'));
            $offersUrl->appendChild($Document->createCDATASection(sprintf(HTTPS_SERVER_FEED_OFFERS, $this->shop, $this->token)));
            $orderUrl = $DOMUrl->appendChild($Document->createElement('StockMovement'));
            $orderUrl->appendChild($Document->createCDATASection(sprintf(HTTPS_SERVER_FEED_STOCK_MOVEMENT, $this->shop, $this->token)));
            $orderUrl = $DOMUrl->appendChild($Document->createElement('OrderImport'));
            $orderUrl->appendChild($Document->createCDATASection(sprintf(HTTPS_SERVER_FEED_ORDER_IMPORT, $this->shop, $this->token)));
            $orderUrl = $DOMUrl->appendChild($Document->createElement('ShippedOrders'));
            $orderUrl->appendChild($Document->createCDATASection(sprintf(HTTPS_SERVER_FEED_SHIPPED_ORDERS, $this->shop, $this->token)));

            header("Content-Type: application/xml; charset=utf-8");
            echo $Document->saveXML();
        }
    }

    private function _validate() {
        $result = false;
        $fbtoken = isset($_GET[constant::FEEDBIZ_TOKEN]) ? $_GET[constant::FEEDBIZ_TOKEN] : '';
        $allowed_ips = explode('|', FEEDBIZ_IP);
        
        if ( in_array($_SERVER['REMOTE_ADDR'], $allowed_ips) &&
                !empty($fbtoken)) {
                	$tokenMeta = $this->shopify_metafield->get(constant::FEEDBIZ_NAMESPACE, constant::FEEDBIZ_TOKEN);
            $this->shopify_metafield->update($tokenMeta[shopify_metafield::FIELD_ID], $fbtoken);
            $result = true;
        }
        return $result;
    }

}

new Connector();
