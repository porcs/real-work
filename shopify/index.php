<?php

session_start();
$page = isset($_GET['load']) ? $_GET['load'] : 'index';
require './config.php';
require './constant.php';
require './function.php';
require './classes/html.php';
require './classes/controller.php';
require './classes/service.php';
require './classes/sqlite.php';
require './classes/model.php';
require_dir('./classes/shopify');
require_dir('./classes/model');
require './controller/' . $page . '.php';
$controller = new $page();
$data = $controller->getData();

if (file_exists('./view/' . $page . '.php'))
    require './view/' . $page . '.php';