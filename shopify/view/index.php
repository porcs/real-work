<?php
$apiKey = SHOPIFY_API_KEY;
$shopOrigin = $controller->shop;
$subject = $_SERVER ['HTTP_REFERER'];
?>

<head>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo HTTPS_SERVER .'/assets/js/jquery.zclip.min.js'; ?>"></script>
<link data-turbolinks-track="true"
	href="//cdn.shopify.com/s/assets/admin/style-0c813d21e4e3347081ac5863484e18f6.css"
	media="all" rel="stylesheet">
<script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
<script type="text/javascript">
        ShopifyApp.init({
            apiKey: '<?php echo $apiKey; ?>',
            shopOrigin: 'https://<?php echo $shopOrigin; ?>'
        });

        ShopifyApp.ready(function() {
            ShopifyApp.Bar.initialize({
                title: 'Configuration',
                buttons: {
                    primary: {
                        label: 'Save',
                        message: 'save',
                        callback: function() {
                            $('#feedbiz-form').submit();
                        }
                    }
                }
            });

            $("#button-copy").hover(function(){
                //turn off this listening event for the element that triggered this
                $(this).unbind('mouseover');
                 //initialize zclip
                $('#button-copy').zclip({
                    path:'<?php echo HTTPS_SERVER; ?>/assets/js/ZeroClipboard.swf',
                    copy:$('#fbconnector').val()
                });
             });
        });
    </script>
</head>
<body class="next-ui">
	<form id="feedbiz-form" action="" method="post">
		<div id="wrapper" class="wrapper">
			<div class="settings-general">

				<div class="section description first-section">
					<div class="next-grid">
						<div class="next-grid__cell next-grid__cell--quarter">
							<div class="section-summary">
								<h1>Credentials</h1>
							</div>
						</div>
						<div class="next-grid__cell">
							<div class="next-card">
								<div class="section-content">
									<div class="next-card__section">
                                        <?php
												echo html::actionSave ();
												echo html::textbox ( constant::FEEDBIZ_TOKEN, 'Token', $data [constant::FEEDBIZ_TOKEN], '', '<input type="button" value="Verify" onclick="verify();"/>&nbsp;&nbsp;<strong id="verify-message" style="display:none;">Success</strong><img id="verify-loading" src="'.HTTPS_SERVER.'/assets/images/loader-connection.gif" style="padding-bottom:15px;display:none;"/>', 'style="width:300px;"' );
												echo html::checkbox ( constant::FEEDBIZ_PRE_PRODUCTION, 'Pre-Production', $data [constant::FEEDBIZ_PRE_PRODUCTION], '<strong>Yes</strong>', '', '<p class="note">Preproduction Mode, in most cases must be unchecked.</p>' );
												echo html::checkbox ( constant::FEEDBIZ_EXPERT_MODE, 'Expert Mode', $data [constant::FEEDBIZ_EXPERT_MODE], '<strong>Yes</strong>', '', '<p class="note">Expert Mode, in most cases must be unchecked.</p>' );
												echo html::checkbox ( constant::FEEDBIZ_DEBUG, 'Debug Mode', $data [constant::FEEDBIZ_DEBUG], '<strong>Yes</strong>', '', '<p class="note">Debug mode. Enable traces for debugging and developpment purpose.<strong>In exploitation this option must not be active !</strong></p>' );
												echo html::textbox ( 'fbconnector', 'Feed.biz XML URL', sprintf ( HTTPS_SERVER_FEED_CONNECTOR, $controller->shop, $controller->token ), '', '<input type="button" id="button-copy" value="Copy to Clipboard"/>', 'style="width:800px;"' );
												?>
                                </div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="section description first-section">
					<div class="next-grid">
						<div class="next-grid__cell next-grid__cell--quarter">
							<div class="section-summary">
								<h1>Settings</h1>
							</div>
						</div>
						<div class="next-grid__cell">
							<div class="next-card">
								<div class="section-content">
									<div class="next-card__section">
                                        <?php
												echo html::actionSave ();
												echo html::checkbox ( constant::FEEDBIZ_DISCOUNT, 'Discount/Specials', $data [constant::FEEDBIZ_DISCOUNT], '<strong>Yes</strong>', '', '<p class="note">Export specials prices if is sets Yes. If unsets the discounted prices will be ignorates</p>' );
												echo html::checkbox ( constant::FEEDBIZ_TAXES, 'Taxes', $data [constant::FEEDBIZ_TAXES], '<strong>Yes</strong>', '', '<p class="note">Add taxes to products and calculate order\'s taxes if sets to yes</p>' );
												echo html::radio ( constant::FEEDBIZ_DESCRIPTION, 'Description Field', $data [constant::FEEDBIZ_DESCRIPTION], constant::getDescriptionOpt (), '', '<p class="note">Field used as product description</p>' );
												echo html::textbox ( constant::FEEDBIZ_PAGE_SIZE, 'Export limit per page', $data [constant::FEEDBIZ_PAGE_SIZE], '', '<p class="note">Number of product/offer to export per page<br/>(Default limit : 250)</p>', 'style="width:100px;"' );
												?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="section description first-section">
					<div class="next-grid">
						<div class="next-grid__cell next-grid__cell--quarter">
							<div class="section-summary">
								<h1>Shipping</h1>
							</div>
						</div>
						<div class="next-grid__cell">
							<div class="next-card">
								<div class="section-content">
									<div class="next-card__section">
                                        <?php
												echo html::actionSave ();
												echo html::textbox ( constant::FEEDBIZ_SHIPPING_DELAY, 'Min delivery time', $data [constant::FEEDBIZ_SHIPPING_DELAY], '', '<p class="note">Minimum delay in days after the parcel is considered as shipped</p>', 'style="width:100px;"' );
												?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</form>
	<script type="text/javascript">
	function verify(){
			$('#verify-loading').show();
			$('#verify-message').hide();
			$.ajax({
				url: '<?php echo HTTPS_SERVER ?>',
				data: {
					load: 'verify',
					shop: '<?php echo $controller->shop; ?>',
					token: '<?php echo $controller->token; ?>',
					fbtoken: $('#<?php echo constant::FEEDBIZ_TOKEN; ?>').val()
				},
				success: function(html){
					$('#verify-loading').hide();
					$('#verify-message').html(html).show();
				}
			});
		}
	</script>
</body>