<?php
class Service {
	public static function getOrder($params, $method, $returnXML = true, $debug = false) {
		ksort ( $params );
		
		foreach ( $params as $param => $value ) {
			$param = str_replace ( "%7E", "~", rawurlencode ( $param ) );
			$value = str_replace ( "%7E", "~", rawurlencode ( $value ) );
			$canonicalized_query [] = $param . "=" . $value;
		}
		
		$canonicalized_query = implode ( "&", $canonicalized_query );
		
		// open connection
		$ch = curl_init ();
		
		$curlOptions = array (
				CURLOPT_CUSTOMREQUEST => $method,
				CURLOPT_RETURNTRANSFER => true 
		);
		
		$curlOptions [CURLOPT_URL] = FEEDBIZ_GETORDER_URL . "?" . $canonicalized_query;
		$curlOptions [CURLOPT_SSL_VERIFYPEER] = false;
		$curlOptions [CURLOPT_VERBOSE] = false;
		
		if (_SSL_HTPASSWD_USER_) {
			$curlOptions [CURLOPT_HTTPAUTH] = CURLAUTH_ANY;
			$curlOptions [CURLOPT_USERPWD] = _SSL_HTPASSWD_USER_ . ":" . _SSL_HTPASSWD_PASS_;
		}
		if (_SSL_VERSION_) {
			$curlOptions [CURLOPT_SSLVERSION] = _SSL_VERSION_;
		}
		
		$curlHandle = curl_init ();
		curl_setopt_array ( $curlHandle, $curlOptions );
		
		if (($result = curl_exec ( $curlHandle )) == false){
			$pass = false;
        	echo __FILE__.'('.__LINE__.')'.'CURL error:('.curl_errno($curlHandle).')'.curl_error($curlHandle);
		}
		else{
			$pass = true;
		}
		
		curl_close ( $curlHandle );
		
		if ($debug) {
			echo '<pre>' . print_r ( $curlOptions, true ) . '</pre>';
			echo $result;
		}
		
		if (! $pass)
			return (false);
		
		if ($returnXML) {
// 			try {
				$xmlDom = new SimpleXMLElement ( $result );
				if ($xmlDom->error)
					echo $xmlDom->error;
				
				return ($xmlDom->Order->return);
// 			} catch ( Exception $e ) {
// 				$this->errors [] = "Exception Caught: " . $e->getMessage () . "\n";
// 			}
		} else {
			echo "Result Caught: " . $result;
		}
	}
	public static function verify($token, $debug = false) {
		$params = array (
				'token' => $token 
		);
		foreach ( $params as $param => $value ) {
			$param = str_replace ( "%7E", "~", rawurlencode ( $param ) );
			$value = str_replace ( "%7E", "~", rawurlencode ( $value ) );
			$canonicalized_query [] = $param . "=" . $value;
		}
		
		$canonicalized_query = implode ( "&", $canonicalized_query );
		
		// open connection
		$ch = curl_init ();
		
		$curlOptions = array (
				CURLOPT_RETURNTRANSFER => true 
		);
		
		$curlOptions [CURLOPT_URL] = FEEDBIZ_VERIFY_URL . "?" . $canonicalized_query;
		$curlOptions [CURLOPT_SSL_VERIFYPEER] = false;
		$curlOptions [CURLOPT_VERBOSE] = false;
		
		if (_SSL_HTPASSWD_USER_) {
			$curlOptions [CURLOPT_HTTPAUTH] = CURLAUTH_ANY;
			$curlOptions [CURLOPT_USERPWD] = _SSL_HTPASSWD_USER_ . ":" . _SSL_HTPASSWD_PASS_;
		}
		if (_SSL_VERSION_) {
			$curlOptions [CURLOPT_SSLVERSION] = _SSL_VERSION_;
		}
		
		$curlHandle = curl_init ();
		curl_setopt_array ( $curlHandle, $curlOptions );
		
		if (! $result = curl_exec ( $curlHandle )){
			$pass = false;
        	echo __FILE__.'('.__LINE__.')'.'CURL error:('.curl_errno($curlHandle).')'.curl_error($curlHandle);
		}
		else{
			$pass = true;
		}
		
		curl_close ( $curlHandle );
		
		if ($debug) {
			echo '<pre>' . print_r ( $curlOptions, true ) . '</pre>';
			echo $result;
			exit ();
		}
		
		if (! $pass)
			return (false);
		
		try {
			$xmlDom = new SimpleXMLElement ( $result );
			return 'true' == $xmlDom->connection->return;
		} catch ( Exception $e ) {
			$this->errors [] = "Exception Caught: " . $e->getMessage () . "\n";
		}
	}
}