<?php
class model {
	const DEF_TABLE = 'table';
	const DEF_PK = 'primary';
	const DEF_FIELDS = 'fields';
	const DEF_TYPE = 'type';
	const DEF_REQUIRED = 'required';
	const DEF_SIZE = 'size';
	const TYPE_VARCHARE = 'varchar';
	const TYPE_INT = 'int';
	const TYPE_TINYINT = 'tynyint';
	protected $definition = null;
	protected $sqlite = null;
	public function __construct($shopname = null, $debug = false) {
		if ($shopname) {
			$this->sqlite = new Sqlite ( $shopname, $debug );
			
			if (! $this->table_exist ()) {
				$this->create_table ();
			}
		} else {
			throw new Exception ( "Please assign shopname" );
		}
	}
	public function create_table() {
		$sql = "CREATE TABLE " . $this->definition [self::DEF_TABLE];
		$columns = array ();
		foreach ( $this->definition [self::DEF_FIELDS] as $column_name => $column_def ) {
			$column_sql = $column_name . ' ' . $column_def [self::DEF_TYPE];
			if (isset ( $column_def [self::DEF_SIZE] ) && $column_def [self::DEF_SIZE]) {
				$column_sql .= ' (' . $column_def [self::DEF_SIZE] . ')';
			}
			$column_sql .= ' ' . ((isset ( $column_def [self::DEF_REQUIRED] ) && $column_def [self::DEF_REQUIRED]) ? 'NOT ' : '') . 'NULL';
			$columns [] = $column_sql;
		}
		
		if (isset ( $this->definition [self::DEF_PK] ) && is_array ( $this->definition [self::DEF_PK] )) {
			$columns [] = 'PRIMARY KEY (' . implode ( ', ', $this->definition [self::DEF_PK] ) . ')';
		}
		$sql .= " (" . implode ( ', ', $columns ) . ")";
		
		$this->sqlite->exec ( $sql );
	}
	public function validate_definition() {
		if (! isset ( $this->definition [self::DEF_TABLE] ) || ! $this->definition [self::DEF_TABLE]) {
			throw new Exception ( 'Please define table name' );
		}
		if (! isset ( $this->definition [self::DEF_FIELDS] ) || ! $this->definition [self::DEF_FIELDS]) {
			throw new Exception ( 'Please define fields' );
		}
	}
	public function table_exist() {
		$sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='" . $this->definition [self::DEF_TABLE] . "';";
		$result = $this->sqlite->query ( $sql );
		return $result;
	}
	public function insert_sql($row) {
		if (! $row || ! is_array ( $row )) {
			throw new Exception ( 'Require input array.' );
		}
		
		$keys = array_keys ( $row );
		$sql = "INSERT INTO " . $this->definition [self::DEF_TABLE] . "(" . implode ( ', ', $keys ) . ") VALUES('" . implode ( "', '", $row ) . "')";
		
		return $sql;
	}
	public function insert($row) {
		$sql = $this->insert_sql ( $row );
		
		$this->sqlite->exec ( $sql );
		return true;
	}
}