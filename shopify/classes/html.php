<?php

class html {

    public static function actionSave() {
        return '<input type="hidden" name="' . constant::FEEDBIZ_ACTION . '" value="' . constant::VALUE_SAVE . '"/>';
    }

    public static function textbox($name, $label, $value, $tophint, $bottomhint, $custom) {
        return '<label for="' . $name . '">' . $label . '</label>'.$tophint.
            '<input class="ssb feedbiz-input" id="' . $name . '" name="' . $name . '" size="30" type="text" value="' . $value . '" '.$custom.'>'.$bottomhint;
    }

    public static function checkbox($name, $label, $value, $value_label, $tophint, $bottomhint) {
        return '<label for="'.$name.'">'.$label.'</label>'.$tophint.'
            <input name="'.$name.'" type="checkbox" value="1" class="feedbiz-input" '.($value ? 'checked="checked"' : '').'>&nbsp;'.$value_label.$bottomhint;
    }

    public static function radio($name, $label, $value, $values, $tophint, $bottomhint) {
        $output = '<label for="'.$name.'">'.$label.'</label>'.$tophint;
        foreach($values as $k=>$v)
            $output .= '<input type="radio" name="'.$name.'" value="'.$k.'" '.($k == $value ? 'checked="checked"' : '').'>'.$v.'&nbsp;&nbsp;';
        $output .= $bottomhint;
        
        return $output;
    }

}
