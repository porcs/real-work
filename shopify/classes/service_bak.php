<?php
class Service {
	public static function getOrder($params, $method, $returnXML = true, $debug = false) {
		ksort ( $params );
		///////////////////////////////////////
/*$curlOptions = array ();
$curlOptions [CURLOPT_URL] = 'https://qa.feed.biz/webservice/WsFeedBiz/api/Order/getOrders?id_order=1&otp=875a3f903218b1e5d61c0bc42d6c34d2&token=48dbb6ec66b7c99d543c011103a07cf7';
$curlOptions [CURLOPT_VERBOSE] = true;


$curlOptions [CURLOPT_HTTPAUTH] = CURLAUTH_ANY;
$curlOptions [CURLOPT_USERPWD] = 'feedbiz:90/25s81';
$curlOptions [CURLOPT_SSLVERSION] = 3;


$curlOptions [CURLOPT_HEADER] = true;
$curlOptions [CURLOPT_RETURNTRANSFER] = true;
$curlOptions [CURLOPT_FOLLOWLOCATION] = true;
$curlOptions [CURLOPT_MAXREDIRS] = 3;
$curlOptions [CURLOPT_SSL_VERIFYPEER] = false;
$curlOptions [CURLOPT_SSL_VERIFYHOST] = false;
$curlOptions [CURLOPT_USERAGENT] = 'ohShopify-php-api-client';
$curlOptions [CURLOPT_CONNECTTIMEOUT] = 30;
$curlOptions [CURLOPT_TIMEOUT] = 30;
$curlOptions [CURLOPT_CAINFO] = 'D:\xampp\apache\conf\cacert.pem';

$ch = curl_init ();
curl_setopt_array ( $ch, $curlOptions );
$result = curl_exec($ch);*/

		$ch = curl_init('https://qa.feed.biz/webservice/WsFeedBiz/api/Order/getOrders?id_order=1&otp=875a3f903218b1e5d61c0bc42d6c34d2&token=48dbb6ec66b7c99d543c011103a07cf7');
		curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'ohShopify-php-api-client');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt ($ch, CURLOPT_CAINFO, 'D:\xampp\apache\conf\cacert.pem');

$result = curl_exec($ch);

echo '<pre>'.print_r($result, true).'</pre>';
echo '<pre>ERR'.curl_errno($ch).':'.print_r(curl_error($ch), true).'</pre>';

echo '<pre>'.print_r($curlOptions, true).'</pre>';
		/////////////////////////////////////////
		foreach ( $params as $param => $value ) {
			$param = str_replace ( "%7E", "~", rawurlencode ( $param ) );
			$value = str_replace ( "%7E", "~", rawurlencode ( $value ) );
			$canonicalized_query [] = $param . "=" . $value;
		}
		
		$canonicalized_query = implode ( "&", $canonicalized_query );
		
		// open connection
		$ch = curl_init ();
		
		$curlOptions = array (
				//CURLOPT_CUSTOMREQUEST => $method,
				CURLOPT_RETURNTRANSFER => true 
		);
		
		$curlOptions [CURLOPT_URL] = FEEDBIZ_GETORDER_URL . "?" . $canonicalized_query;
		$curlOptions [CURLOPT_SSL_VERIFYPEER] = false;
		$curlOptions [CURLOPT_VERBOSE] = false;
		
		if (_SSL_HTPASSWD_USER_) {
			$curlOptions [CURLOPT_HTTPAUTH] = CURLAUTH_ANY;
			$curlOptions [CURLOPT_USERPWD] = _SSL_HTPASSWD_USER_ . ":" . _SSL_HTPASSWD_PASS_;
		}
		if (_SSL_VERSION_) {
			$curlOptions [CURLOPT_SSLVERSION] = _SSL_VERSION_;
		}
		
		$curlHandle = curl_init ();
		curl_setopt_array ( $curlHandle, $curlOptions );
		
		if (($result = curl_exec ( $curlHandle )) == false){
			$pass = false;
        	echo __FILE__.'('.__LINE__.')'.'CURL error:('.curl_errno($curlHandle).')'.curl_error($curlHandle);
		}
		else{
			$pass = true;
		}
		
		curl_close ( $curlHandle );
		
		if ($debug) {
			echo '<pre>' . print_r ( $curlOptions, true ) . '</pre>';
			echo $result;
		}
		
		if (! $pass)
			return (false);
		
		if ($returnXML) {
// 			try {
				$xmlDom = new SimpleXMLElement ( $result );
				if ($xmlDom->error)
					echo $xmlDom->error;
				
				return ($xmlDom->Order->return);
// 			} catch ( Exception $e ) {
// 				$this->errors [] = "Exception Caught: " . $e->getMessage () . "\n";
// 			}
		} else {
			echo "Result Caught: " . $result;
		}
	}
	public static function verify($token, $debug = false) {
		$params = array (
				'token' => $token 
		);
		foreach ( $params as $param => $value ) {
			$param = str_replace ( "%7E", "~", rawurlencode ( $param ) );
			$value = str_replace ( "%7E", "~", rawurlencode ( $value ) );
			$canonicalized_query [] = $param . "=" . $value;
		}
		
		$canonicalized_query = implode ( "&", $canonicalized_query );
		
		// open connection
		$ch = curl_init ();
		
		$curlOptions = array (
				CURLOPT_RETURNTRANSFER => true 
		);
		
		$curlOptions [CURLOPT_URL] = FEEDBIZ_VERIFY_URL . "?" . $canonicalized_query;
		$curlOptions [CURLOPT_SSL_VERIFYPEER] = false;
		$curlOptions [CURLOPT_VERBOSE] = false;
		
		if (_SSL_HTPASSWD_USER_) {
			$curlOptions [CURLOPT_HTTPAUTH] = CURLAUTH_ANY;
			$curlOptions [CURLOPT_USERPWD] = _SSL_HTPASSWD_USER_ . ":" . _SSL_HTPASSWD_PASS_;
		}
		if (_SSL_VERSION_) {
			$curlOptions [CURLOPT_SSLVERSION] = _SSL_VERSION_;
		}
		
		$curlHandle = curl_init ();
		curl_setopt_array ( $curlHandle, $curlOptions );
		
		if (! $result = curl_exec ( $curlHandle )){
			$pass = false;
        	echo __FILE__.'('.__LINE__.')'.'CURL error:('.curl_errno($curlHandle).')'.curl_error($curlHandle);
		}
		else{
			$pass = true;
		}
		
		curl_close ( $curlHandle );
		
		if ($debug) {
			echo '<pre>' . print_r ( $curlOptions, true ) . '</pre>';
			echo $result;
			exit ();
		}
		
		if (! $pass)
			return (false);
		
		try {
			$xmlDom = new SimpleXMLElement ( $result );
			return 'true' == $xmlDom->connection->return;
		} catch ( Exception $e ) {
			$this->errors [] = "Exception Caught: " . $e->getMessage () . "\n";
		}
	}
}