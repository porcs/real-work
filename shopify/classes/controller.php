<?php

class Controller {

    var $shop = null;
    var $token = null;
    var $api = null;
    var $data = array();
    var $input_conf = array();
    var $input_conf_noform = array();

    public function __construct() {
        if (isset($_GET[constant::SHOPIFY_SHOP]) && isset($_GET[constant::SHOPIFY_TOKEN])) {
            $this->shop = $_GET[constant::SHOPIFY_SHOP];
            $this->token = $_GET[constant::SHOPIFY_TOKEN];
            $this->api = new ShopifyClient($this->shop, $this->token, SHOPIFY_API_KEY, SHOPIFY_SECRET);

            $this->input_conf = array(constant::FEEDBIZ_TOKEN => constant::INPUT_TEXT,
                constant::FEEDBIZ_PRE_PRODUCTION => constant::INPUT_CHECKBOX,
                constant::FEEDBIZ_EXPERT_MODE => constant::INPUT_CHECKBOX,
                constant::FEEDBIZ_DEBUG => constant::INPUT_CHECKBOX,
                constant::FEEDBIZ_DISCOUNT => constant::INPUT_CHECKBOX,
                constant::FEEDBIZ_TAXES => constant::INPUT_CHECKBOX,
                constant::FEEDBIZ_DESCRIPTION => constant::INPUT_TEXT,
                constant::FEEDBIZ_PAGE_SIZE => constant::INPUT_INTEGER,
                constant::FEEDBIZ_MAPPING_MANUFACTURER => constant::INPUT_TEXT,
                constant::FEEDBIZ_MAPPING_CATEGORY => constant::INPUT_TEXT,
            	constant::FEEDBIZ_CONTEXT_PRODUCTS => constant::INPUT_TEXT,
            	constant::FEEDBIZ_CONTEXT_OFFERS => constant::INPUT_TEXT,
            	constant::FEEDBIZ_SHIPPING_DELAY => constant::INPUT_INTEGER
            );
            
            $this->input_conf_noform = array(constant::FEEDBIZ_MAPPING_MANUFACTURER,
            		constant::FEEDBIZ_MAPPING_CATEGORY,
            		constant::FEEDBIZ_CONTEXT_PRODUCTS,
            		constant::FEEDBIZ_CONTEXT_OFFERS
            );
            
        } else if (isset($_GET[constant::SHOPIFY_CODE])) {
            $shopifyClient = new ShopifyClient($_GET[constant::SHOPIFY_SHOP], "", SHOPIFY_API_KEY, SHOPIFY_SECRET);            
            $token = $shopifyClient->getAccessToken($_GET[constant::SHOPIFY_CODE]);
            $shop = $_GET[constant::SHOPIFY_SHOP];
            $load = $_GET[constant::FEEDBIZ_LOAD];
            $redirectURL = HTTPS_SERVER.'?';
            if($load)
            	$redirectURL .= 'load='.$load.'&';
            $redirectURL .= constant::SHOPIFY_SHOP.'='.$shop.'&'.constant::SHOPIFY_TOKEN.'='.$token;

            header("Location: " . $redirectURL);
            exit;
        }
        else if (isset($_POST[constant::SHOPIFY_SHOP]) || isset($_GET[constant::SHOPIFY_SHOP])) {
            $shop = isset($_POST[constant::SHOPIFY_SHOP]) ? $_POST[constant::SHOPIFY_SHOP] : $_GET[constant::SHOPIFY_SHOP];
            $shopifyClient = new ShopifyClient($shop, "", SHOPIFY_API_KEY, SHOPIFY_SECRET);

            $pageURL = 'https://' . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            
            header("Location: " . $shopifyClient->getAuthorizeUrl(SHOPIFY_SCOPE, $pageURL));
            exit;
        }
    }

    public function getData() {
        return $this->data;
    }

    public function isSave() {
        return isset($_POST[constant::FEEDBIZ_ACTION]) && constant::VALUE_SAVE == $_POST[constant::FEEDBIZ_ACTION];
    }

}
