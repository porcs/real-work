<?php
// for checking imported order
class OrderModel extends model {
	const TABLE_NAME = 'orders';
	const COLUMN_ID = 'id';
	const COLUMN_MP_ORDER_ID = 'mp_order_id';
	protected $definition = array (
			model::DEF_TABLE => self::TABLE_NAME,
			model::DEF_PK => array (
					'id' 
			),
			model::DEF_FIELDS => array (
					'id' => array (
							model::DEF_TYPE => model::TYPE_VARCHARE,
							model::DEF_REQUIRED => true,
							model::DEF_SIZE => 20 
					),
					'mp_order_id' => array (
							model::DEF_TYPE => model::TYPE_INT,
							model::DEF_REQUIRED => true,
							model::DEF_SIZE => 10 
					) 
			) 
	);
	public function __construct($shopname = null, $debug = false) {
		parent::__construct ( $shopname, $debug );
	}
	public function exist($mp_order_id = 0, $shopify_order_id = 0, $debug = false) {
		if ($mp_order_id || $shopify_order_id) {
			$causes = array ();
			if ($mp_order_id) {
				$causes [] = self::COLUMN_MP_ORDER_ID . "='" . $mp_order_id . "'";
			}
			if ($shopify_order_id) {
				$causes [] = self::COLUMN_ID . "='" . $shopify_order_id . "' and " . self::COLUMN_MP_ORDER_ID . " > 0";
			}
			$sql = "select * from " . self::TABLE_NAME . " where " . implode ( ' AND ', $causes );
			if ($debug) {
				echo '<pre>' . $sql . '</pre>';
			}
			return $this->sqlite->query ( $sql );
		} else {
			throw new Exception ( 'Please assign mp_order_id' );
		}
	}
	public function create($id = 0, $mp_order_id = 0) {
		$this->insert ( array (
				self::COLUMN_ID => strval ( $id ),
				self::COLUMN_MP_ORDER_ID => intval ( $mp_order_id ) 
		) );
	}
}