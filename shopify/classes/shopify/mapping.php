<?php

class shopify_mapping {

    var $shopify_metafield = null;
    var $namespace = null;
    var $meta_id = null;
    var $meta_key = null;
    var $data = array();
    const FIELD_SEPARATE = '|';

    public function __construct($shopify_metafield, $namespace, $meta_key) {
        $this->shopify_metafield = $shopify_metafield;
        $this->namespace = $namespace;
        $this->meta_key = $meta_key;
        
        $meta = $this->shopify_metafield->get($namespace, $meta_key);
        $this->data = empty($meta[shopify_metafield::FIELD_VALUE]) ? array() : explode(self::FIELD_SEPARATE, $meta[shopify_metafield::FIELD_VALUE]);
        $this->meta_id = $meta[shopify_metafield::FIELD_ID];
    }

    public function save(){
        $this->shopify_metafield->update($this->meta_id, implode(self::FIELD_SEPARATE, $this->data));
    }
    
    public function getId($value, $offset = 1){
        if(!in_array($value, $this->data)){
            $this->data[] = $value;
        }
        return array_search($value, $this->data) + $offset;
    }
}
