<?php
class shopify_carrier {
	var $api = null;
	const PROPERTY_ID = 'id';
	const PROPERTY_NAME = 'name';
	const PROPERTY_TAX = 'tax';
	const PROPERTY_TAX_NAME = 'tax_name';
	public function __construct($api) {
		if (! $api) {
			throw new Exception ( 'Shopify API connection failed.' );
		}
		$this->api = $api;
	}
}
