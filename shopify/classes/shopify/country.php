<?php
class shopify_country {
	var $api = null;
	var $countires = null;
	const PROPERTY_ID = 'id';
	const PROPERTY_CODE = 'code';
	const PROPERTY_NAME = 'name';
	const PROPERTY_TAX = 'tax';
	const PROPERTY_TAX_NAME = 'tax_name';
	const PROPERTY_PROVINCES = 'provinces';
	const PROPERTY_PROVINCE_CODE = 'code';
	const PROPERTY_PROVINCE_NAME = 'name';
	const PROPERTY_WEIGHT_BASED_SHIPPING_RATES = 'weight_based_shipping_rates';
	const PROPERTY_PRICE_BASED_SHIPPING_RATES = 'price_based_shipping_rates';
	const PROPERTY_CARRIER_SHIPPING_RATE_PROVIDERS = 'carrier_shipping_rate_providers';
	const PROPERTY_SHIPPING_RATE__ID = 'id';
	const PROPERTY_SHIPPING_RATE__NAME = 'name';
	public function __construct($api) {
		if (! $api) {
			throw new Exception ( 'Shopify API connection failed.' );
		}
		$this->api = $api;
	}
	public function getCountries() {
		if (! $this->countires) {
			$this->countires = $this->api->call ( constant::METHOD_GET, constant::SHOPIFY_PATH_ADMIN_COUNTRIES );
		}
		return $this->countires;
	}
	public function getCountry($country_code = '') {
		$country = null;
		$this->getCountries();
		foreach ( $this->countires as $shopifyCountry ) {
			if ($shopifyCountry [shopify_country::PROPERTY_CODE] == '*') {
				$country = $shopifyCountry;
			} else if (strtolower($shopifyCountry [shopify_country::PROPERTY_CODE]) == strtolower($country_code)) {
				$country = $shopifyCountry;
				return $country;
			}
		}
		return $country;
	}
	public function getTax($country_code = '', $province_code = '') {
		$tax = array (
				shopify_country::PROPERTY_TAX => 0,
				shopify_country::PROPERTY_TAX_NAME => '0' 
		);
		$country = $this->getCountry ( $country_code );
		if($country){
			$tax[shopify_country::PROPERTY_TAX] = $country[shopify_country::PROPERTY_TAX];
			$tax[shopify_country::PROPERTY_TAX_NAME] = $country[shopify_country::PROPERTY_TAX_NAME];
			foreach ($country[shopify_country::PROPERTY_PROVINCES] as $province){
				if(strtolower($province[shopify_country::PROPERTY_PROVINCE_CODE]) == strtolower($province_code) ||
						strtolower($province[shopify_country::PROPERTY_PROVINCE_NAME]) == strtolower($province_code)){
					
				}
			}
		}
		return $tax;
	}
}
