<?php
class shopify_variant {
	var $api = null;
	const PROPERTY_TITLE = 'title';
	public function __construct($api) {
		if (! $api) {
			throw new Exception ( 'Shopify API connection failed.' );
		}
		$this->api = $api;
	}
	public function getVariant($id) {
		$data = $this->api->call ( constant::METHOD_GET, sprintf ( constant::SHOPIFY_PATH_ADMIN_VARIANTS_Q_ID, $id ) );
		return $data;
	}
}
