<?php
class shopify_metafield {
	var $api = null;
	const FIELD_ID = 'id';
	const FIELD_DESCRIPTION = 'description';
	const FIELD_KEY = 'key';
	const FIELD_NAMESPACE = 'namespace';
	const FIELD_VALUE = 'value';
	const FIELD_VALUE_TYPE = 'value_type';
	const FIELD_UPDATED_AT = 'updated_at';
	const VALUE_DESCRIPTION_TAG = 'description_tag';
	const VALUE_METAFIELD = 'metafield';
	public function __construct($api) {
		if (! $api) {
			throw new Exception ( 'Shopify API connection failed.' );
		}
		$this->api = $api;
	}
	public function get($namespace = null, $key = null) {
		if ($namespace && $key) {
			$metas = $this->api->call ( constant::METHOD_GET, sprintf ( constant::SHOPIFY_PATH_ADMIN_METAFIELDS_Q_NAMESPACE_KEY, $namespace, $key ) );
			
			foreach ( $metas as $meta ) {
				if ($meta [self::FIELD_VALUE_TYPE] == constant::SHOPIFY_VALUE_TYPE_STRING && $meta [self::FIELD_VALUE] == constant::VALUE_EMPTY_STRING)
					$meta [self::FIELD_VALUE] = '';
				return $meta;
			}
		} else if ($namespace) {
			$data = array ();
			$metas = $this->api->call ( constant::METHOD_GET, sprintf ( constant::SHOPIFY_PATH_ADMIN_METAFIELDS_Q_NAMESPACE, $namespace ) );
			
			foreach ( $metas as $meta ) {
				if ($meta [self::FIELD_VALUE_TYPE] == constant::SHOPIFY_VALUE_TYPE_STRING && $meta [self::FIELD_VALUE] == constant::VALUE_EMPTY_STRING)
					$meta [self::FIELD_VALUE] = '';
				$data [$meta [self::FIELD_KEY]] = $meta;
			}
			return $data;
		} else {
			throw new Exception ( 'Please specific criteria' );
		}
		return null;
	}
	public function getValue($namespace = null, $key = null) {
		$data = $this->get ( $namespace, $key );
		return $data [self::FIELD_VALUE];
	}
	public function add($namespace, $key, $value, $value_type = constant::SHOPIFY_VALUE_TYPE_STRING) {
		if ($value_type == constant::SHOPIFY_VALUE_TYPE_STRING && empty ( $value ))
			$value = constant::VALUE_EMPTY_STRING;
		else
			$value = '0';
		
		$metafield = array (
				self::FIELD_NAMESPACE => $namespace,
				self::FIELD_KEY => $key,
				self::FIELD_VALUE => $value,
				self::FIELD_VALUE_TYPE => $value_type 
		);
		$this->api->call ( constant::METHOD_POST, constant::SHOPIFY_PATH_ADMIN_METAFIELDS, array (
				self::VALUE_METAFIELD => $metafield 
		) );
	}
	public function update($meta_id, $value, $value_type = constant::SHOPIFY_VALUE_TYPE_STRING) {
		if ($value_type == constant::SHOPIFY_VALUE_TYPE_STRING && empty ( $value ))
			$value = constant::VALUE_EMPTY_STRING;
		
		$metafield = array (
				self::FIELD_ID => $meta_id,
				self::FIELD_VALUE => $value,
				self::FIELD_VALUE_TYPE => $value_type 
		);
		$this->api->call ( constant::METHOD_PUT, sprintf ( constant::SHOPIFY_PATH_ADMIN_METAFIELDS_ID, $meta_id ), array (
				self::VALUE_METAFIELD => $metafield 
		) );
	}
}
