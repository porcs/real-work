<?php
class shopify_order {
	var $api = null;
	var $shopify_variant = null;
	var $shopify_country = null;
	const PROPERTY_ID = 'id';
	const PROPERTY_ORDER_NUMBER = 'order_number';
	const PROPERTY_FULLFILLMENTS = 'fulfillments';
	const PROPERTY_TRACKING_NUMBER = 'tracking_number';
	const PROPERTY_CREATED_AT = 'created_at';
	const PROPERTY_TRACKING_COMPANY = 'tracking_company';
	const PROPERTY_METAFIELDS = 'metafields';
	const PROPERTY_META_KEY = 'key';
	const PROPERTY_META_VALUE = 'value';
	const PROPERTY_META_VALUE_TYPE = 'value_type';
	const PROPERTY_META_NAMESPACE = 'namespace';
	const META_KEY_MP_ORDER_ID = 'mp_order_id';
	public function __construct($api) {
		if (! $api) {
			throw new Exception ( 'Shopify API connection failed.' );
		}
		$this->api = $api;
		$this->shopify_variant = new shopify_variant ( $api );
		$this->shopify_country = new shopify_country ( $api );
	}
	public function createOrder($order, $usetax = true, $debug = false) {
		$fullname = Tool::splitFullName ( strval ( $order->buyer->name ) );
		
		$tax = $this->shopify_country->getTax ( strval ( $order->buyer->country_code ), strval ( $order->buyer->country_code ) );
		$tax_rate = $usetax && isset ( $tax [shopify_country::PROPERTY_TAX] ) ? floatval ( $tax [shopify_country::PROPERTY_TAX] ) : 0;
		$tax_name = $usetax && isset ( $tax [shopify_country::PROPERTY_TAX_NAME] ) ? $tax [shopify_country::PROPERTY_TAX_NAME] : '';
		
		$total_tax_incl = floatval ( $order->invoices->total_products );
		$total_tax_excl = number_format ( $total_tax_incl / (1 + $tax_rate), 2 );
		$total_tax = $total_tax_incl - $total_tax_excl;
		
		$ship_tax_incl = floatval ( $order->invoices->total_shipping_tax_incl );
		$ship_tax_excl = number_format ( $ship_tax_incl / (1 + $tax_rate), 2 );
		$ship_tax = $ship_tax_incl - $ship_tax_excl;
		
		$data = array (
				'note' => 'Order channel : Feed.biz - ' . strval ( $order->sales_channel ),
				'total_price' => $total_tax_incl,
				'subtotal_price' => $total_tax_incl - $ship_tax_incl,
				'total_weight' => floatval ( $order->carrier->weight ),
				'total_tax' => $total_tax,
				'taxes_included' => 'true',
				'processing_method' => strval ( $order->sales_channel ),
				'customer' => array (
						'first_name' => $fullname ['firstname'],
						'last_name' => $fullname ['lastname'],
						'email' => strval ( $order->buyer->email ) 
				),
				'billing_address' => array (
						'first_name' => $fullname ['firstname'],
						'last_name' => $fullname ['lastname'],
						'address1' => strval ( $order->buyer->address1 ),
						'address2' => strval ( $order->buyer->address2 ),
						'phone' => strval ( $order->buyer->phone ),
						'city' => strval ( $order->buyer->city ),
						'province' => strval ( $order->buyer->state_region ),
						'country' => strval ( $order->buyer->country_name ),
						'zip' => strval ( $order->buyer->postal_code ) 
				),
				'shipping_address' => array (
						'first_name' => $fullname ['firstname'],
						'last_name' => $fullname ['lastname'],
						'address1' => strval ( $order->buyer->address1 ),
						'address2' => strval ( $order->buyer->address2 ),
						'phone' => strval ( $order->buyer->phone ),
						'city' => strval ( $order->buyer->city ),
						'province' => strval ( $order->buyer->state_region ),
						'country' => strval ( $order->buyer->country_name ),
						'zip' => strval ( $order->buyer->postal_code ) 
				),
				'email' => strval ( $order->buyer->email ),
				'financial_status' => 'authorized',
				'shipping_lines' => array (
						array (
								'code' => strval ( $order->carrier->shipment_service ),
								'price' => $ship_tax_incl,
								'source' => strval ( $order->sales_channel ),
								'title' => strval ( $order->carrier->shipment_service ),
								'tax_lines' => array () 
						) 
				),
				self::PROPERTY_METAFIELDS => array (
						array (
								'key' => self::META_KEY_MP_ORDER_ID,
								'value' => strval ( $order->id_order ),
								'value_type' => 'string',
								'namespace' => 'global' 
						) 
				) 
		);
		
		if ($usetax) {
			$data ['tax_lines'] [] = array (
					'price' => $total_tax,
					'rate' => $tax_rate,
					'title' => $tax_name 
			);
		}
		
		foreach ( $order->item->children () as $item ) {
			$variant = $this->shopify_variant->getVariant ( strval ( $item->reference ) );
			$item_tax_incl = floatval ( $item->unit_price_tax_incl );
			$item_tax_excl = $item_tax_incl / (1 + $tax_rate);
			$item_data = array (
					'variant_id' => strval ( $item->reference ),
					'quantity' => intval ( $item->quantity ),
					'price' => $item_tax_incl / intval ( $item->quantity ),
					'variant_title' => $variant [shopify_variant::PROPERTY_TITLE] 
			);
			
			$data ['line_items'] [] = $item_data;
		}
		
		$dataJSON = Tool::prepareJSON ( $data );
		
		if ($debug) {
			echo '<pre>' . print_r ( $dataJSON, true ) . '</pre>';
			// $this->api->call ( constant::METHOD_POST, constant::SHOPIFY_PATH_ADMIN_ORDERS, array (
			// 'order' => $dataJSON
			// ), $debug );
			return false;
		} else {
			return $this->api->call ( constant::METHOD_POST, constant::SHOPIFY_PATH_ADMIN_ORDERS, array (
					'order' => $dataJSON 
			) );
		}
	}
	public function getOrdersShipped() {
		$date15daysbefore = date ( 'Y-m-d', strtotime ( date ( 'Y-m-d' ) . ' - 15 days' ) );
		$orders = $this->api->call ( constant::METHOD_GET, sprintf ( constant::SHOPIFY_PATH_ADMIN_ORDERS_SHIPPED, $date15daysbefore, 'shipped' ) );
		return $orders;
	}
}
