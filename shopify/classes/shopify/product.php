<?php
class shopify_product {
	var $api = null;
	const PROPERTY_ID = 'id';
	const PROPERTY_GRAMS = 'grams';
	const PROPERTY_SKU = 'sku';
	const PROPERTY_FIELDS = 'fields';
	const PROPERTY_VENDOR = 'vendor';
	const PROPERTY_PRODUCT_TYPE = 'product_type';
	const PROPERTY_BODY_HTML = 'body_html';
	const PROPERTY_PRICE = 'price';
	const PROPERTY_COMPARE_AT_PRICE = 'compare_at_price';
	const PROPERTY_BARCODE = 'barcode';
	const PROPERTY_CREATED_AT = 'created_at';
	const PROPERTY_IMAGES = 'images';
	const PROPERTY_IMAGES_SRC = 'src';
	const PROPERTY_IMAGES_UPDATE_AT = 'updated_at';
	const PROPERTY_TITLE = 'title';
	const PROPERTY_TAGS = 'tags';
	const PROPERTY_INVENTORY_MANAGEMENT = 'inventory_management';
	const PROPERTY_INVENTORY_QUANTITY = 'inventory_quantity';
	const PROPERTY_VARIANTS = 'variants';
	const PROPERTY_COUNT = 'count';
	const VALUE_INVENTORY_SHOPIFY_TRACKING = 'shopify';
	public function __construct($api) {
		if (! $api) {
			throw new Exception ( 'Shopify API connection failed.' );
		}
		$this->api = $api;
	}
	public function getProducts($limit, $page) {
		$data = $this->api->call ( constant::METHOD_GET, sprintf ( constant::SHOPIFY_PATH_ADMIN_PRODUCTS_Q_LIMIT_PAGE, ( string ) $limit, ( string ) $page ) );
		return $data;
	}
	public function getMeta($id_product) {
		$data = $this->api->call ( constant::METHOD_GET, sprintf ( constant::SHOPIFY_PATH_ADMIN_PRODUCTS_METAFIELDS, $id_product ) );
		return $data;
	}
	public function count($criterias) {
		$causes = '';
		if ($criterias) {
			foreach ( $criterias as $k => $v ) {
				$causes .= $k . '=' . $v;
			}
		}
		$data = $this->api->call ( constant::METHOD_GET, constant::SHOPIFY_PATH_ADMIN_PRODUCTS_COUNT . '?' . $causes );
		return $data;
	}
	public function query($criterias) {
		$causes = '';
		if ($criterias) {
			foreach ( $criterias as $k => $v ) {
				$causes .= $k . '=' . $v;
			}
		}
		$data = $this->api->call ( constant::METHOD_GET, constant::SHOPIFY_PATH_ADMIN_PRODUCTS . '?' . $causes );
		return $data;
	}
}
