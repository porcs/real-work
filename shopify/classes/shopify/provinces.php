<?php
class shopify_provinces {
	var $api = null;
	var $data = array ();
	const PROPERTY_NAME = 'name';
	const PROPERTY_CURRENCY = 'currency';
	const PROPERTY_COUNTRY_CODE = 'country_code';
	const PROPERTY_COUNTRY_NAME = 'country_name';
	public function __construct($api) {
		if (! $api) {
			throw new Exception ( 'Shopify API connection failed.' );
		}
		$this->api = $api;
	}
	public function getProvinces($country) {
		$provinces = $this->api->call ( constant::METHOD_GET, sprintf ( constant::SHOPIFY_PATH_ADMIN_COUNTRIES_PROVINCES, $country ) );
		return $provinces;
	}
}
