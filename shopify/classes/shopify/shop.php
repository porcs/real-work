<?php
class shopify_shop {
	var $api = null;
	var $data = array ();
	var $countires = null;
	const PROPERTY_NAME = 'name';
	const PROPERTY_CURRENCY = 'currency';
	const PROPERTY_COUNTRY_CODE = 'country_code';
	const PROPERTY_COUNTRY_NAME = 'country_name';
	public function __construct($api) {
		if (! $api) {
			throw new Exception ( 'Shopify API connection failed.' );
		}
		$this->api = $api;
		$this->data = $this->api->call ( constant::METHOD_GET, sprintf ( constant::SHOPIFY_PATH_ADMIN_SHOP ) );
	}
	public function getProperty($key) {
		return $this->data [$key];
	}
}
