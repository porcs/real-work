<?php
class Context {
	var $shopify_metafield = null;
	var $metafield = array ();
	public $timestamp = null;
	public $currentProduct = 0;
	public $maxProduct = 0;
	public $minProduct = 0;
	public $status = constant::STATUS_INCOMPLETE;
	public function __construct($shopify_metafield, $type = null) {
		$this->shopify_metafield = $shopify_metafield;
		return $this->restore ( $type );
	}
	public function restore($type) {
		if (! $type)
			$type = constant::FEEDBIZ_CONTEXT_PRODUCTS;
		$this->metafield = $this->shopify_metafield->get ( constant::FEEDBIZ_NAMESPACE, $type );
		
		$rawContext = null;
		if ($this->metafield && ! empty ( $this->metafield [shopify_metafield::FIELD_VALUE] )) {
			$rawContextArr = explode ( '|', $this->metafield [shopify_metafield::FIELD_VALUE] );
			$rawContext = array ();
			$rawContext ['timestamp'] = $this->metafield [shopify_metafield::FIELD_UPDATED_AT];
			$rawContext ['currentProduct'] = $rawContextArr [0];
			$rawContext ['maxProduct'] = $rawContextArr [1];
			$rawContext ['minProduct'] = $rawContextArr [2];
			$rawContext ['status'] = $rawContextArr [3];
		}
		
		$current = date ( 'Y-m-d H:m:i' );
		if ($rawContext && is_array ( $rawContext ) && self::min_diff ( $current, $rawContext ['timestamp'] ) < FEEDBIZ_CONTEXT_TIMEOUT && $rawContext ['status'] == constant::STATUS_INCOMPLETE) {
			$this->currentProduct = $rawContext ['currentProduct'];
			$this->maxProduct = $rawContext ['maxProduct'];
			$this->minProduct = $rawContext ['minProduct'];
			$this->status = $rawContext ['status'];
		}
		return $this;
	}
	public function save() {
		$dataArr = array (
				$this->currentProduct,
				$this->maxProduct,
				$this->minProduct,
				$this->status 
		);
		$this->shopify_metafield->update ( $this->metafield [shopify_metafield::FIELD_ID], implode ( '|', $dataArr ) );
	}
	static public function min_diff($dt01, $dt02) {
		$result = - 1;
		if ($dt01 instanceof DateTime) {
			$dt01 = $dt01->format ( 'Y-m-d H:i:s' );
		}
		if ($dt02 instanceof DateTime) {
			$dt02 = $dt02->format ( 'Y-m-d H:i:s' );
		}
		
		if (! $dt01 && ! $dt02) {
			$diff = strtotime ( $dt01 ) - strtotime ( $dt02 );
			$result = $diff / 60;
		}
		
		return $result;
	}
}
