<?php
require '../config.php';
require '../constant.php';
require '../function.php';
require '../classes/sqlite.php';
require '../classes/model.php';
require_dir ( '../classes/shopify' );
require_dir ( '../classes/models' );
require '../classes/context.php';
require '../classes/tool.php';
require '../classes/service.php';
class Feed {
	var $api = null;
	var $shop = null;
	var $token = null;
	public function __construct() {
		$this->shop = isset ( $_GET ['shop'] ) ? $_GET ['shop'] : '';
		$this->token = isset ( $_GET ['token'] ) ? $_GET ['token'] : '';
		
		if (! empty ( $this->shop ) && ! empty ( $this->token )) {
			
			$this->api = new ShopifyClient ( $this->shop, $this->token, SHOPIFY_API_KEY, SHOPIFY_SECRET );
		} else if (! empty ( $_GET ['code'] )) { // setp 2
			$this->_step2 ();
		} else if (! empty ( $this->shop )) { // step 1
			$this->_step1 ();
		}
	}
	private function _step1() {
		$this->api = new ShopifyClient ( $this->shop, "", SHOPIFY_API_KEY, SHOPIFY_SECRET );
		$pageURL = "https://" . $_SERVER ["SERVER_NAME"] . $_SERVER ["REQUEST_URI"];
		header ( "Location: " . $this->api->getAuthorizeUrl ( SHOPIFY_SCOPE, $pageURL ) );
		exit ();
	}
	private function _step2() {
		$this->api = new ShopifyClient ( $this->shop, "", SHOPIFY_API_KEY, SHOPIFY_SECRET );
		$this->token = $this->api->getAccessToken ( $_GET ['code'] );
		
		header ( "Location: " . sprintf ( HTTPS_SERVER_FEED_SETTINGS, $this->shop, $this->token ) );
		exit ();
	}
}
