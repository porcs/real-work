<?php
class Sqlite {
	private $database = '';
	private $conn = null;
	public function __construct($shopname, $debug = false) {
		$this->database = DIR_SERVER_SQLITE . '/' . md5 ( $shopname ) . '.db';
		
		if ($debug) {
			echo '<pre>Database file: ' . $this->database . '</pre>';
		}
		
		$result = $this->validate ();
		if ($result) {
			throw new Exception ( implode ( '<br/>', $result ) );
		}
		$this->open ();
	}
	public function open() {
		if(!file_exists($this->database)){
			throw new Exception(sprintf('Database file not found %s', $this->database));
		}
		$this->conn = sqlite_open ( $this->database, 0666, $error );
	}
	public function query($sql) {
		$exec = sqlite_array_query ( $this->conn, $sql );
		
		if (! $exec)
			return FALSE;
		
		return $exec ? $exec : array ();
	}
	public function exist($sql) {
		$result = $this->query ( $sql );
		return sizeof ( $result ) > 0;
	}
	public function exec($sql) {
		$exec = sqlite_exec ( $this->conn, $sql );
		
		if (! $exec)
			return FALSE;
		
		return $exec;
	}
	public function close() {
		return sqlite_close ( $this->conn );
	}
	public function validate() {
		$result = array ();
		$oldmask = umask ( 0 );
		if (! file_exists ( $this->database )) {
			if (! is_writable ( DIR_SERVER_SQLITE )) {
				$result ['error'] = sprintf ( 'Directory %s is not writeable', DIR_SERVER_SQLITE );
			} else {
				$error = '';
				if (! sqlite_open ( $this->database, 0777, $error ))
					$result ['error'] = $error;
			}
			umask ( $oldmask );
		}
		return $result;
	}
	public function __destruct() {
		$this->close ();
	}
}