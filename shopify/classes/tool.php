<?php
class Tool {
	public static function EAN_UPC_Check($code) {
		// first change digits to a string so that we can access individual numbers
		$digits = sprintf ( '%012s', substr ( sprintf ( '%013s', $code ), 0, 12 ) );
		// 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits {1} + $digits {3} + $digits {5} + $digits {7} + $digits {9} + $digits {11};
		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;
		// 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits {0} + $digits {2} + $digits {4} + $digits {6} + $digits {8} + $digits {10};
		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;
		// 5. The check character is the smallest number which, when added to the result in step 4, produces a multiple of 10.
		$next_ten = (ceil ( $total_sum / 10 )) * 10;
		$check_digit = $next_ten - $total_sum;
		return ($code == ($digits . $check_digit));
	}
	public static function cleanNonUnicodeSupport($pattern) {
		return preg_replace ( '/\\\[px]\{[a-z]\}{1,2}|(\/[a-z]*)u([a-z]*)$/i', "$1$2", $pattern );
	}
	public static function isEmail($email) {
		return ! empty ( $email ) && preg_match ( Tool::cleanNonUnicodeSupport ( '/^[a-z\p{L}0-9!#$%&\'*+\/=?^`{}|~_-]+[.a-z\p{L}0-9!#$%&\'*+\/=?^`{}|~_-]*@[a-z\p{L}0-9]+[._a-z\p{L}0-9-]*\.[a-z\p{L}0-9]+$/ui' ), $email );
	}
	public static function prepareJSON($data) {
		if (is_string ( $data )) {
			$data = str_replace ( '"', '', $data );
			return $data;
		} else if (is_array ( $data )) {
			foreach ( $data as $k => $v ) {
				$data [$k] = self::prepareJSON ( $v );
			}
			return $data;
		} else {
			return $data;
		}
	}
	public static function splitFullName($name) {
		$result = array (
				'firstname' => '',
				'lastname' => '' 
		);
		$firstnameArr = array ();
		$nameSplit = explode ( ' ', $name );
		for($i = 0; $i < sizeof ( $nameSplit ); $i ++) {
			if ($i == 0) {
				$firstnameArr [] = $nameSplit [$i];
				$result ['lastname'] = $nameSplit [$i];
			} else if ($i == (sizeof ( $nameSplit ) - 1)) {
				$result ['lastname'] = $nameSplit [$i];
			} else {
				$firstnameArr [] = $nameSplit [$i];
			}
		}
		$result ['firstname'] = implode ( ' ', $firstnameArr );
		return $result;
	}
	public static function description($html) {
		$text = $html;
		
		$text = str_replace ( '</p>', "\n</p>", $text );
		$text = str_replace ( '</li>', "\n</li>", $text );
		$text = str_replace ( '<br', "\n<br", $text );
		
		// $text = strip_tags($text);
		
		// $text = iconv("UTF-8", "UTF-8//TRANSLIT", $text);
		$text = str_replace ( '&#39;', "'", $text );
		$text = str_replace ( '"', "'", $text );
		
		$text = mb_convert_encoding ( $text, 'HTML-ENTITIES' );
		$text = str_replace ( '&nbsp;', ' ', $text );
		$text = html_entity_decode ( $text, ENT_NOQUOTES, 'UTF-8' );
		$text = str_replace ( '&', '&amp;', $text );
		
		$text = preg_replace ( '#\s+[\n|\r]+$#i', '', $text ); // empty
		$text = preg_replace ( '#[\n|\r]+#i', "\n", $text ); // multiple-return
		$text = preg_replace ( '#\n+#i', "\n", $text ); // multiple-return
		$text = preg_replace ( '#^[\n\r\s]#i', '', $text );
		
		$text = preg_replace ( '/[\x{0001}-\x{0009}]/u', '', $text );
		$text = preg_replace ( '/[\x{000b}-\x{001f}]/u', '', $text );
		$text = preg_replace ( '/[\x{0080}-\x{009F}]/u', '', $text );
		$text = preg_replace ( '/[\x{0600}-\x{FFFF}]/u', '', $text );
		
		$text = preg_replace ( '/\x{000a}/', "\n", $text );
		return htmlspecialchars ( trim ( $text ) );
	}
}
