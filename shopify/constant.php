<?php
class constant{
// 	private $date15daysbefore = 
	
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';
    
    const SHOPIFY_SHOP = 'shop';
    const SHOPIFY_CODE = 'code';
    const SHOPIFY_TOKEN = 'token';
    const SHOPIFY_ERRORS = 'errors';
    const SHOPIFY_PATH_ADMIN_PRODUCTS = '/admin/products.json';
    const SHOPIFY_PATH_ADMIN_PRODUCTS_METAFIELDS = '/admin/products/%s/metafields.json';
    const SHOPIFY_PATH_ADMIN_PRODUCTS_COUNT = '/admin/products/count.json';
    const SHOPIFY_PATH_ADMIN_PRODUCTS_Q_LIMIT_PAGE = '/admin/products.json?limit=%s&page=%s';
    const SHOPIFY_PATH_ADMIN_METAFIELDS = '/admin/metafields.json';
    const SHOPIFY_PATH_ADMIN_METAFIELDS_ID = '/admin/metafields/%s.json';
    const SHOPIFY_PATH_ADMIN_METAFIELDS_Q_NAMESPACE = '/admin/metafields.json?namepace=%s';
    const SHOPIFY_PATH_ADMIN_METAFIELDS_Q_NAMESPACE_KEY = '/admin/metafields.json?namepace=%s&key=%s';
    const SHOPIFY_PATH_ADMIN_COUNTRIES = '/admin/countries.json';
    const SHOPIFY_PATH_ADMIN_COUNTRIES_PROVINCES = '/admin/countries/%s/provinces.json';
    const SHOPIFY_PATH_ADMIN_SHOP = '/admin/shop.json';
    const SHOPIFY_PATH_ADMIN_ORDERS = '/admin/orders.json';
    const SHOPIFY_PATH_ADMIN_ORDERS_SHIPPED = '/admin/orders.json?updated_at_min=%s&fulfillment_status=%s ';
    const SHOPIFY_PATH_ADMIN_VARIANTS_Q_ID = '/admin/variants/%s.json';
    
    const SHOPIFY_VALUE_TYPE_STRING = 'string';
    const SHOPIFY_VALUE_TYPE_INTEGER = 'integer';
        
    const FEEDBIZ_NAMESPACE = 'feedbiz';
    const FEEDBIZ_ACTION = 'fbaction';
    const FEEDBIZ_LOAD = 'load';
    const FEEDBIZ_IP = 'fbip';
    const FEEDBIZ_ORDER = 'fborder';
    const FEEDBIZ_PRODUCT = 'fbproduct';
    const FEEDBIZ_COMBINATION = 'fbcombination';
    const FEEDBIZ_SOLD = 'fbsold';
    const FEEDBIZ_OTP = 'otp';
    const FEEDBIZ_TOKEN = 'fbtoken';
    const FEEDBIZ_PRE_PRODUCTION = 'fbpreprod';
    const FEEDBIZ_EXPERT_MODE = 'fbexpert';
    const FEEDBIZ_DEBUG = 'fbdebug';
    const FEEDBIZ_SHIPPING_DELAY = 'fbdelay';
    const FEEDBIZ_CARRIER = 'fbcarrier';
    const FEEDBIZ_DISCOUNT = 'fbdiscount';
    const FEEDBIZ_TAXES = 'fbtax';
    const FEEDBIZ_DESCRIPTION = 'fbdescription';
    const FEEDBIZ_PAGE_SIZE = 'fbpagesize';
    const FEEDBIZ_MAPPING_MANUFACTURER = 'fbmappingmanufacturer';
    const FEEDBIZ_MAPPING_CATEGORY = 'fbmappingcategory';
    const FEEDBIZ_CONTEXT_PRODUCTS = 'fbcontextproducts';
    const FEEDBIZ_CONTEXT_OFFERS = 'fbcontextoffers';
    const FEEDBIZ_COUNTRY = 'country';
    const FEEDBIZ_ATTRIBUTE_MODEL_NAME = 'model';
    const FEEDBIZ_ATTRIBUTE_MODEL_TYPE = 'select';
    const FEEDBIZ_ATTRIBUTE_MODEL_ID = '1';
    
    const VALUE_SAVE = 'save';
    const VALUE_EMPTY_STRING = 'EMPTY_STRING';
    const VALUE_DESCRIPTION_LONG = 1;
    const VALUE_DESCRIPTION_SHORT = 2;
    
    const STATUS_COMPLETE = 'complete';
    const STATUS_INCOMPLETE = 'incomplete';
    
    const INPUT_TEXT = 'text';
    const INPUT_INTEGER = 'int';
    const INPUT_CHECKBOX = 'checkbox';
    
    public static function getDescriptionOpt(){
        return array('1' => 'Long Description',
            '2' => 'Short Description');
    }
}