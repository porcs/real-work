<?php
define('SHOPIFY_API_KEY', 'b8ab6c1c5c5d4c0dc4ca088a6e33561e');
define('SHOPIFY_SECRET', '1efc22e111f2966d0f1f607da26d6f0f');
define('SHOPIFY_SCOPE', 'read_products, write_products, read_customers, read_orders, write_orders');


define('FEEDBIZ_PAGE_SIZE', 250);
define('FEEDBIZ_CONTEXT_TIMEOUT', 5);


define('_FEEDBIZ_SANDBOX_', false);
if (_FEEDBIZ_SANDBOX_) {
	define('DIR_SERVER', 'D:\B\SVN\shopify');
	define('DIR_SERVER_SQLITE', DIR_SERVER.'/assets/databases');
	define('HTTPS_SERVER', 'https://localhost/shopify');
	define('FEEDBIZ_IP', '192.168.1.10|180.183.102.248|127.0.0.1');
	define('FEEDBIZ_VERIFY_URL', 'http://feedbiz.com/webservice/WsFeedBiz/api/Connection/checkConnection');
	define('FEEDBIZ_GETORDER_URL', 'http://feedbiz.com/webservice/WsFeedBiz/api/Order/getOrders');
	define('_SSL_HTPASSWD_USER_', '');
	define('_SSL_HTPASSWD_PASS_', '');
	define('_SSL_VERSION_', '');
} else {
	define('DIR_SERVER', '/var/www/html/backend/shopify');
	define('DIR_SERVER_SQLITE', DIR_SERVER.'/assets/databases');
	define('HTTPS_SERVER', 'https://qa.feed.biz/shopify');
	define('FEEDBIZ_IP', '54.187.71.110|54.148.216.231');
	define('FEEDBIZ_VERIFY_URL', 'https://qa.feed.biz/webservice/WsFeedBiz/api/Connection/checkConnection');
	define('FEEDBIZ_GETORDER_URL', 'https://qa.feed.biz/webservice/WsFeedBiz/api/Order/getOrders');
	define('_SSL_HTPASSWD_USER_', 'feedbiz');
	define('_SSL_HTPASSWD_PASS_', '90/25s81');
	define('_SSL_VERSION_', '');
}

define('HTTPS_SERVER_FEED', HTTPS_SERVER.'/feed');
define('HTTPS_SERVER_FEED_CONNECTOR', HTTPS_SERVER_FEED.'/connector.php?shop=%s&token=%s');
define('HTTPS_SERVER_FEED_SETTINGS', HTTPS_SERVER_FEED.'/settings.php?shop=%s&token=%s');
define('HTTPS_SERVER_FEED_PRODUCTS', HTTPS_SERVER_FEED.'/products.php?shop=%s&token=%s');
define('HTTPS_SERVER_FEED_OFFERS', HTTPS_SERVER_FEED.'/offers.php?shop=%s&token=%s');
define('HTTPS_SERVER_FEED_STOCK_MOVEMENT', HTTPS_SERVER_FEED.'/stockmovement.php?shop=%s&token=%s');
define('HTTPS_SERVER_FEED_ORDER_IMPORT', HTTPS_SERVER_FEED.'/orderimport.php?shop=%s&token=%s');
define('HTTPS_SERVER_FEED_SHIPPED_ORDERS', HTTPS_SERVER_FEED.'/shippedorders.php?shop=%s&token=%s');