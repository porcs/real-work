Geachte {customer_name},

U vindt hierbij de factuur voor uw bestelling nr. {order_id} van {order_date} via {marketplace}.

Deze factuur is in het universele PDF-formaat; u kunt ze met een PDF-lezer op uw scherm bekijken.

Bewaar deze factuur en denk aan het milieu; druk ze alleen af wanneer dat nodig is.

Met vriendelijke groet,
Klantenservice
{shop_name}